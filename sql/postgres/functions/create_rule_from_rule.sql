-- DROP FUNCTION create_rule_from_rule(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_rule_id BIGINT, in_previous_rule_id BIGINT, in_src_rule_id BIGINT);
CREATE OR REPLACE FUNCTION create_rule_from_rule(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_rule_id BIGINT, in_previous_rule_id BIGINT, in_src_rule_id BIGINT, in_new_rule_id BIGINT)
RETURNS BIGINT AS $$
DECLARE

	tmp_project_version_record RECORD;
	tmp_src_rule_record RECORD;
	tmp_src_rule_content_record RECORD;
	
	tmp_child_rule_record RECORD;
	
	tmp_next_rule_id BIGINT;
	
	tmp_rule_content_id BIGINT;
	
	tmp_parent_rule_id BIGINT;
	tmp_previous_rule_id BIGINT;
	tmp_child_rule_id BIGINT;

	tmp_new_rule_id BIGINT;
	tmp_return BIGINT default 0;
	
BEGIN

	SELECT * INTO tmp_project_version_record FROM projects_versions_table WHERE project_id=in_project_id AND version=in_dst_version;
	IF FOUND THEN

		SELECT * INTO tmp_src_rule_record FROM rules_table WHERE rule_id=in_src_rule_id;
		IF FOUND THEN
		
			SELECT * INTO tmp_src_rule_content_record FROM rules_contents_table WHERE rule_content_id=tmp_src_rule_record.rule_content_id;
			IF FOUND THEN
			
				IF in_previous_rule_id IS NULL THEN
					SELECT rule_id INTO tmp_next_rule_id FROM rules_table WHERE parent_rule_id=in_parent_rule_id AND previous_rule_id IS NULL;
				ELSE
					SELECT rule_id INTO tmp_next_rule_id FROM rules_table WHERE parent_rule_id=in_parent_rule_id AND previous_rule_id=in_previous_rule_id;
				END IF;
				
				INSERT INTO rules_contents_table (short_name, description, project_id, project_version_id, version)
				VALUES (
					tmp_src_rule_content_record.short_name,
					tmp_src_rule_content_record.description,
					in_project_id,
					tmp_project_version_record.project_version_id,
					in_dst_version);
				SELECT currval('rules_contents_rule_content_id_seq') INTO tmp_rule_content_id;			
				
				INSERT INTO rules_table (rule_content_id, parent_rule_id, previous_rule_id, project_id, project_version_id, version)
				VALUES (tmp_rule_content_id, in_parent_rule_id, in_previous_rule_id, in_project_id, tmp_project_version_record.project_version_id, in_dst_version);
				SELECT currval('rules_rule_id_seq') INTO tmp_return;
				
				IF tmp_next_rule_id IS NOT NULL THEN
					UPDATE rules_table SET previous_rule_id=tmp_return WHERE rule_id=tmp_next_rule_id;
				END IF;
				
				tmp_parent_rule_id := tmp_src_rule_record.rule_id;
				tmp_child_rule_id := NULL;
				tmp_previous_rule_id := NULL;
				IF in_new_rule_id IS NULL THEN
					tmp_new_rule_id:=tmp_return;
				ELSE
					tmp_new_rule_id:=in_new_rule_id;
				END IF;
				
				SELECT * INTO tmp_child_rule_record FROM rules_table WHERE parent_rule_id=tmp_parent_rule_id AND previous_rule_id IS NULL;
				IF FOUND AND tmp_child_rule_record.rule_id=tmp_new_rule_id THEN
					SELECT * INTO tmp_child_rule_record FROM rules_table WHERE parent_rule_id=tmp_parent_rule_id AND previous_rule_id=tmp_new_rule_id;
				END IF;

				WHILE FOUND LOOP
				
					tmp_child_rule_id := create_rule_from_rule(in_project_id, in_dst_version, tmp_return, tmp_child_rule_id, tmp_child_rule_record.rule_id, tmp_return);
					tmp_previous_rule_id := tmp_child_rule_record.rule_id;
					SELECT * INTO tmp_child_rule_record FROM rules_table WHERE parent_rule_id=tmp_parent_rule_id AND previous_rule_id=tmp_previous_rule_id;
					IF FOUND AND tmp_child_rule_record.rule_id=tmp_new_rule_id THEN
						SELECT * INTO tmp_child_rule_record FROM rules_table WHERE parent_rule_id=tmp_parent_rule_id AND previous_rule_id=tmp_new_rule_id;
					END IF;
				END LOOP;
					
			ELSE
			
				RAISE NOTICE 'Pas de contenu de regle de gestion %', tmp_src_rule_record.rule_content_id;
				
			END IF;
			
		ELSE
		
			RAISE NOTICE 'Pas de regle de gestion %', in_src_rule_id;
			
		END IF;
	ELSE
	
		RAISE NOTICE 'Pas de version de projet %', in_dst_version;

	END IF;
	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;
