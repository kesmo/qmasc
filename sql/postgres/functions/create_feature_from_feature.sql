-- DROP FUNCTION create_feature_from_feature(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_feature_id BIGINT, in_previous_feature_id BIGINT, in_src_feature_id BIGINT);
CREATE OR REPLACE FUNCTION create_feature_from_feature(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_feature_id BIGINT, in_previous_feature_id BIGINT, in_src_feature_id BIGINT, in_new_feature_id BIGINT)
RETURNS BIGINT AS $$
DECLARE

	tmp_project_version_record RECORD;
	tmp_src_feature_record RECORD;
	tmp_src_feature_content_record RECORD;
	
	tmp_child_feature_record RECORD;
	
	tmp_next_feature_id BIGINT;
	
	tmp_feature_content_id BIGINT;
	
	tmp_parent_feature_id BIGINT;
	tmp_previous_feature_id BIGINT;
	tmp_child_feature_id BIGINT;

	tmp_new_feature_id BIGINT;
	tmp_return BIGINT default 0;
	
BEGIN

	SELECT * INTO tmp_project_version_record FROM projects_versions_table WHERE project_id=in_project_id AND version=in_dst_version;
	IF FOUND THEN

		SELECT * INTO tmp_src_feature_record FROM features_table WHERE feature_id=in_src_feature_id;
		IF FOUND THEN
		
			SELECT * INTO tmp_src_feature_content_record FROM features_contents_table WHERE feature_content_id=tmp_src_feature_record.feature_content_id;
			IF FOUND THEN
			
				IF in_previous_feature_id IS NULL THEN
					SELECT feature_id INTO tmp_next_feature_id FROM features_table WHERE parent_feature_id=in_parent_feature_id AND previous_feature_id IS NULL;
				ELSE
					SELECT feature_id INTO tmp_next_feature_id FROM features_table WHERE parent_feature_id=in_parent_feature_id AND previous_feature_id=in_previous_feature_id;
				END IF;
				
				INSERT INTO features_contents_table (short_name, description, project_id, project_version_id, version)
				VALUES (
					tmp_src_feature_content_record.short_name,
					tmp_src_feature_content_record.description,
					in_project_id,
					tmp_project_version_record.project_version_id,
					in_dst_version);
				SELECT currval('features_contents_feature_content_id_seq') INTO tmp_feature_content_id;			
				
				INSERT INTO features_table (feature_content_id, parent_feature_id, previous_feature_id, project_id, project_version_id, version)
				VALUES (tmp_feature_content_id, in_parent_feature_id, in_previous_feature_id, in_project_id, tmp_project_version_record.project_version_id, in_dst_version);
				SELECT currval('features_feature_id_seq') INTO tmp_return;
				
				IF tmp_next_feature_id IS NOT NULL THEN
					UPDATE features_table SET previous_feature_id=tmp_return WHERE feature_id=tmp_next_feature_id;
				END IF;
				
				tmp_parent_feature_id := tmp_src_feature_record.feature_id;
				tmp_child_feature_id := NULL;
				tmp_previous_feature_id := NULL;
				IF in_new_feature_id IS NULL THEN
					tmp_new_feature_id:=tmp_return;
				ELSE
					tmp_new_feature_id:=in_new_feature_id;
				END IF;
				
				SELECT * INTO tmp_child_feature_record FROM features_table WHERE parent_feature_id=tmp_parent_feature_id AND previous_feature_id IS NULL;
				IF FOUND AND tmp_child_feature_record.feature_id=tmp_new_feature_id THEN
					SELECT * INTO tmp_child_feature_record FROM features_table WHERE parent_feature_id=tmp_parent_feature_id AND previous_feature_id=tmp_new_feature_id;
				END IF;

				WHILE FOUND LOOP
				
					tmp_child_feature_id := create_feature_from_feature(in_project_id, in_dst_version, tmp_return, tmp_child_feature_id, tmp_child_feature_record.feature_id, tmp_return);
					tmp_previous_feature_id := tmp_child_feature_record.feature_id;
					SELECT * INTO tmp_child_feature_record FROM features_table WHERE parent_feature_id=tmp_parent_feature_id AND previous_feature_id=tmp_previous_feature_id;
					IF FOUND AND tmp_child_feature_record.feature_id=tmp_new_feature_id THEN
						SELECT * INTO tmp_child_feature_record FROM features_table WHERE parent_feature_id=tmp_parent_feature_id AND previous_feature_id=tmp_new_feature_id;
					END IF;
				END LOOP;
					
			ELSE
			
				RAISE NOTICE 'Pas de contenu de fonctionnalite %', tmp_src_feature_record.feature_content_id;
				
			END IF;
			
		ELSE
		
			RAISE NOTICE 'Pas de fonctionnalite %', in_src_feature_id;
			
		END IF;

	ELSE
	
		RAISE NOTICE 'Pas de version de projet %', in_dst_version;

	END IF;
	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;
