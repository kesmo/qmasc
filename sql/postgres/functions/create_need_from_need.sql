-- DROP FUNCTION create_need_from_need(in_project_id BIGINT, in_parent_need_id BIGINT, in_previous_need_id BIGINT, in_src_need_id BIGINT);
CREATE OR REPLACE FUNCTION create_need_from_need(in_project_id BIGINT, in_parent_need_id BIGINT, in_previous_need_id BIGINT, in_src_need_id BIGINT, in_new_need_id BIGINT)
RETURNS BIGINT AS $$
DECLARE

	tmp_src_need_record RECORD;
	
	tmp_child_need_record RECORD;
	
	tmp_next_need_id BIGINT;
		
	tmp_parent_need_id BIGINT;
	tmp_previous_need_id BIGINT;
	tmp_child_need_id BIGINT;

	tmp_new_need_id BIGINT;
	tmp_return BIGINT default 0;
	
BEGIN

	SELECT * INTO tmp_src_need_record FROM needs_table WHERE need_id=in_src_need_id;
	IF FOUND THEN
				
		IF in_previous_need_id IS NULL THEN
			SELECT need_id INTO tmp_next_need_id FROM needs_table WHERE parent_need_id=in_parent_need_id AND in_previous_need_id IS NULL;
		ELSE
			SELECT need_id INTO tmp_next_need_id FROM needs_table WHERE parent_need_id=in_parent_need_id AND in_previous_need_id=in_previous_need_id;
		END IF;
		
		INSERT INTO needs_table (short_name, description, parent_need_id, previous_need_id, project_id)
		VALUES (tmp_src_need_record.short_name, tmp_src_need_record.description, in_parent_need_id, in_previous_need_id, in_project_id);
		SELECT currval('needs_need_id_seq') INTO tmp_return;
		
		IF tmp_next_need_id IS NOT NULL THEN
			UPDATE needs_table SET previous_need_id=tmp_return WHERE need_id=tmp_next_need_id;
		END IF;
		
		tmp_parent_need_id := tmp_src_need_record.need_id;
		tmp_child_need_id := NULL;
		tmp_previous_need_id := NULL;
		IF in_new_need_id IS NULL THEN
			tmp_new_need_id:=tmp_return;
		ELSE
			tmp_new_need_id:=in_new_need_id;
		END IF;
		
		SELECT * INTO tmp_child_need_record FROM needs_table WHERE parent_need_id=tmp_parent_need_id AND previous_need_id IS NULL;
		IF FOUND AND tmp_child_need_record.need_id=tmp_new_need_id THEN
			SELECT * INTO tmp_child_need_record FROM needs_table WHERE parent_need_id=tmp_parent_need_id AND previous_need_id=tmp_new_need_id;
		END IF;

		WHILE FOUND LOOP
		
			tmp_child_need_id := create_need_from_need(in_project_id, tmp_return, tmp_child_need_id, tmp_child_need_record.need_id, tmp_return);
			tmp_previous_need_id := tmp_child_need_record.need_id;
			SELECT * INTO tmp_child_need_record FROM needs_table WHERE parent_need_id=tmp_parent_need_id AND previous_need_id=tmp_previous_need_id;
			IF FOUND AND tmp_child_need_record.need_id=tmp_new_need_id THEN
				SELECT * INTO tmp_child_need_record FROM needs_table WHERE parent_need_id=tmp_parent_need_id AND previous_need_id=tmp_new_need_id;
			END IF;
		END LOOP;
						
	ELSE
	
		RAISE NOTICE 'Pas de regle de gestion %', in_src_need_id;
		
	END IF;
	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;
