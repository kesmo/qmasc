-- PROJETS
DROP VIEW projects;
ALTER TABLE projects_table ALTER COLUMN description TYPE VARCHAR(16384);
ALTER TABLE projects_table ALTER COLUMN description_plain_text TYPE VARCHAR(16384);
CREATE OR REPLACE VIEW projects AS select * from projects_table;
GRANT SELECT ON projects TO admin_role, writer_role, reader_role;

-- VERSIONS DE PROJETS
DROP VIEW projects_versions;
ALTER TABLE projects_versions_table ALTER COLUMN description TYPE VARCHAR(16384);
ALTER TABLE projects_versions_table ALTER COLUMN description_plain_text TYPE VARCHAR(16384);
CREATE OR REPLACE VIEW projects_versions AS select * from projects_versions_table;
GRANT SELECT ON projects_versions TO admin_role, writer_role, reader_role;


-- CONTENUS DE TESTS
DROP VIEW tests_contents;
ALTER TABLE tests_contents_table ALTER COLUMN description TYPE VARCHAR(16384);
ALTER TABLE tests_contents_table ALTER COLUMN description_plain_text TYPE VARCHAR(16384);
CREATE OR REPLACE VIEW tests_contents AS select * from tests_contents_table;
GRANT SELECT ON tests_contents TO admin_role, writer_role, reader_role;

ALTER TABLE tests_contents_table DROP CONSTRAINT tests_contents_table_original_test_content_id_fkey;

CREATE SEQUENCE tests_contents_original_test_content_id_seq;
GRANT SELECT, UPDATE ON tests_contents_original_test_content_id_seq TO admin_role, writer_role;
ALTER TABLE tests_contents_table ALTER COLUMN original_test_content_id SET DEFAULT nextval('tests_contents_original_test_content_id_seq');

CREATE OR REPLACE FUNCTION correct_test_content_ids()
RETURNS VOID AS $$
DECLARE

	tmp_original_test_content_id BIGINT DEFAULT 0;
	tmp_count BIGINT DEFAULT 0;
	tmp_seq_num BIGINT DEFAULT 0;

BEGIN

	FOR tmp_count, tmp_original_test_content_id IN SELECT count(*), original_test_content_id FROM tests_contents_table GROUP BY original_test_content_id, version LOOP
		IF tmp_count > 1 THEN
			UPDATE tests_contents_table SET original_test_content_id=NULL WHERE original_test_content_id=tmp_original_test_content_id;
		END IF;
	END LOOP;

	SELECT MAX(test_content_id)+1 INTO tmp_seq_num FROM tests_contents_table;
	PERFORM setval('tests_contents_original_test_content_id_seq', tmp_seq_num , true);
	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION upgrade_test_content_ids(in_test_content_id BIGINT, in_original_test_content_id BIGINT)
RETURNS VOID AS $$
DECLARE

	tmp_test_content_record RECORD;
	tmp_original_test_content_id BIGINT DEFAULT 0;

BEGIN

	IF in_test_content_id IS NULL THEN
		FOR tmp_test_content_record IN SELECT * FROM tests_contents_table WHERE original_test_content_id IS NULL LOOP
			SELECT nextval('tests_contents_original_test_content_id_seq') INTO tmp_original_test_content_id;
			UPDATE tests_contents_table SET original_test_content_id=tmp_original_test_content_id WHERE test_content_id=tmp_test_content_record.test_content_id;
			PERFORM upgrade_test_content_ids(tmp_test_content_record.test_content_id, tmp_original_test_content_id );
		END LOOP;
	ELSE
		IF in_original_test_content_id IS NOT NULL THEN
			FOR tmp_test_content_record IN SELECT * FROM tests_contents_table WHERE original_test_content_id=in_test_content_id AND test_content_id!=in_test_content_id LOOP
				UPDATE tests_contents_table SET original_test_content_id=in_original_test_content_id WHERE test_content_id=tmp_test_content_record.test_content_id;
				PERFORM upgrade_test_content_ids(tmp_test_content_record.test_content_id, in_original_test_content_id);
			END LOOP;
		END IF;
	END IF;
	
END;
$$ LANGUAGE plpgsql;

SELECT correct_test_content_ids();
SELECT upgrade_test_content_ids(NULL, NULL);

ALTER TABLE tests_contents_table ALTER COLUMN original_test_content_id SET NOT NULL;
ALTER TABLE tests_contents_table ADD CONSTRAINT original_test_content_version_pk UNIQUE (original_test_content_id, version);

-- EXIGENCES DE TESTS
ALTER TABLE tests_requirements_table ADD COLUMN original_requirement_content_id BIGINT;

-- CONTENUS D'EXIGENCES
DROP VIEW requirements_contents;
ALTER TABLE requirements_contents_table ALTER COLUMN description TYPE VARCHAR(16384);
ALTER TABLE requirements_contents_table ALTER COLUMN description_plain_text TYPE VARCHAR(16384);
CREATE OR REPLACE VIEW requirements_contents AS select * from requirements_contents_table;
GRANT SELECT ON requirements_contents TO admin_role, writer_role, reader_role;

ALTER TABLE requirements_contents_table DROP CONSTRAINT requirements_contents_table_original_requirement_content_i_fkey;
ALTER TABLE requirements_contents_table DROP CONSTRAINT requirements_contents_table_requirement_content_id_fkey;

CREATE SEQUENCE requirements_contents_original_requirement_content_id_seq;
GRANT SELECT, UPDATE ON requirements_contents_original_requirement_content_id_seq TO admin_role, writer_role;
ALTER TABLE requirements_contents_table ALTER COLUMN original_requirement_content_id SET DEFAULT nextval('requirements_contents_original_requirement_content_id_seq');

CREATE OR REPLACE FUNCTION correct_requirement_content_ids()
RETURNS VOID AS $$
DECLARE

	tmp_original_requirement_content_id BIGINT DEFAULT 0;
	tmp_count BIGINT DEFAULT 0;
	tmp_seq_num BIGINT DEFAULT 0;

BEGIN

	FOR tmp_count, tmp_original_requirement_content_id IN SELECT count(*), original_requirement_content_id FROM requirements_contents_table GROUP BY original_requirement_content_id, version LOOP
		IF tmp_count > 1 THEN
			UPDATE requirements_contents_table SET original_requirement_content_id=NULL WHERE original_requirement_content_id=tmp_original_requirement_content_id;
		END IF;
	END LOOP;

	SELECT MAX(requirement_content_id)+1 INTO tmp_seq_num FROM requirements_contents_table;
	PERFORM setval('requirements_contents_original_requirement_content_id_seq', tmp_seq_num , true);
	
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION upgrade_requirement_content_ids(in_requirement_content_id BIGINT, in_original_requirement_content_id BIGINT)
RETURNS VOID AS $$
DECLARE

	tmp_requirement_content_record RECORD;
	tmp_original_requirement_content_id BIGINT DEFAULT 0;

BEGIN

	IF in_requirement_content_id IS NULL THEN
		FOR tmp_requirement_content_record IN SELECT * FROM requirements_contents_table WHERE original_requirement_content_id IS NULL LOOP
			SELECT nextval('requirements_contents_original_requirement_content_id_seq') INTO tmp_original_requirement_content_id;
			UPDATE requirements_contents_table SET original_requirement_content_id=tmp_original_requirement_content_id  WHERE requirement_content_id=tmp_requirement_content_record.requirement_content_id;
			PERFORM upgrade_requirement_content_ids(tmp_requirement_content_record.requirement_content_id, tmp_original_requirement_content_id );
		
		END LOOP;
	ELSE
		IF in_original_requirement_content_id IS NOT NULL THEN
			UPDATE tests_requirements_table SET original_requirement_content_id=in_original_requirement_content_id WHERE requirement_content_id=in_requirement_content_id;
			FOR tmp_requirement_content_record IN SELECT * FROM requirements_contents_table WHERE original_requirement_content_id=in_requirement_content_id AND requirement_content_id!=in_requirement_content_id LOOP
				UPDATE requirements_contents_table SET original_requirement_content_id=in_original_requirement_content_id WHERE requirement_content_id=tmp_requirement_content_record.requirement_content_id;
				PERFORM upgrade_requirement_content_ids(tmp_requirement_content_record.requirement_content_id, in_original_requirement_content_id);
			END LOOP;
		END IF;
	END IF;
	
END;
$$ LANGUAGE plpgsql;

SELECT correct_requirement_content_ids();
SELECT upgrade_requirement_content_ids(NULL, NULL);

ALTER TABLE requirements_contents_table ALTER COLUMN original_requirement_content_id SET NOT NULL;
ALTER TABLE requirements_contents_table ADD CONSTRAINT original_requirement_content_version_pk UNIQUE (original_requirement_content_id, version);

-- EXIGENCES DE TESTS
DROP VIEW tests_requirements;
ALTER TABLE tests_requirements_table DROP COLUMN requirement_content_id;
ALTER TABLE tests_requirements_table ADD PRIMARY KEY (test_content_id, original_requirement_content_id);
CREATE VIEW tests_requirements AS SELECT * FROM tests_requirements_table;
COMMENT ON COLUMN tests_requirements_table.original_requirement_content_id IS 'Identifiant original du contenu de l''exigence';

DROP FUNCTION create_test_from_requirement(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_test_id BIGINT, in_previous_test_id BIGINT, in_src_requirement_id BIGINT);
CREATE OR REPLACE FUNCTION create_test_from_requirement(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_test_id BIGINT, in_previous_test_id BIGINT, in_src_requirement_id BIGINT)
RETURNS BIGINT AS $$
DECLARE

	tmp_requirement_record RECORD;
	tmp_requirement_content_record RECORD;
	
	tmp_test_id BIGINT;
	
	tmp_test_content_id BIGINT;
	
	tmp_parent_requirement_id BIGINT;
	tmp_previous_requirement_id BIGINT;
	tmp_previous_test_id BIGINT;

	tmp_return BIGINT default 0;
	
BEGIN

	SELECT * INTO tmp_requirement_record FROM requirements_table WHERE requirement_id=in_src_requirement_id;
	IF FOUND THEN
	
		SELECT * INTO tmp_requirement_content_record FROM requirements_contents_table WHERE requirement_content_id=tmp_requirement_record.requirement_content_id;
		IF FOUND THEN
		
			IF in_previous_test_id IS NULL THEN
				SELECT test_id INTO tmp_test_id FROM tests_table WHERE parent_test_id=in_parent_test_id AND previous_test_id IS NULL;
			ELSE
				SELECT test_id INTO tmp_test_id FROM tests_table WHERE parent_test_id=in_parent_test_id AND previous_test_id=in_previous_test_id;
			END IF;
			
			INSERT INTO tests_contents_table (short_name, description, category_id, priority_level, project_id, version)
			VALUES (
				tmp_requirement_content_record.short_name,
				tmp_requirement_content_record.description,
				tmp_requirement_content_record.category_id,
				tmp_requirement_content_record.priority_level,
				in_project_id,
				in_dst_version);
			SELECT currval('tests_contents_test_content_id_seq') INTO tmp_test_content_id;
			
			INSERT INTO tests_table (test_content_id, parent_test_id, previous_test_id, project_id, version)
			VALUES (tmp_test_content_id, in_parent_test_id, in_previous_test_id, in_project_id, in_dst_version);
			SELECT currval('tests_test_id_seq') INTO tmp_return;
			
			IF tmp_test_id IS NOT NULL THEN
				UPDATE tests_table SET previous_test_id=tmp_return WHERE test_id=tmp_test_id;
			END IF;
			
			IF tmp_requirement_record.project_id=in_project_id THEN
				INSERT INTO tests_requirements_table (test_content_id, original_requirement_content_id)
				VALUES (tmp_test_content_id, tmp_requirement_content_record.original_requirement_content_id);
			END IF;
			
			tmp_parent_requirement_id := tmp_requirement_record.requirement_id;
			tmp_previous_requirement_id := NULL;
			tmp_previous_test_id := NULL;
			
			SELECT * INTO tmp_requirement_record
				FROM requirements_table
				WHERE parent_requirement_id = tmp_parent_requirement_id AND previous_requirement_id IS NULL;

			WHILE FOUND LOOP
			
				tmp_previous_test_id := create_test_from_requirement(in_project_id, in_dst_version, tmp_return, tmp_previous_test_id, tmp_requirement_record.requirement_id);
				tmp_previous_requirement_id := tmp_requirement_record.requirement_id;
				SELECT * INTO tmp_requirement_record FROM requirements_table WHERE parent_requirement_id=tmp_parent_requirement_id AND previous_requirement_id=tmp_previous_requirement_id;
				
			END LOOP;
			
		ELSE
		
			RAISE NOTICE 'Pas de contenu d''exigence %', tmp_requirement_record.requirement_content_id;
			
		END IF;
		
	ELSE
	
		RAISE NOTICE 'Pas d''exigence %', in_src_requirement_id;
		
	END IF;
	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;

-- ACTIONS
-- Ajout d'un lien vers un autre test
DROP VIEW actions;
ALTER TABLE actions_table ALTER COLUMN description TYPE VARCHAR(16384);
ALTER TABLE actions_table ALTER COLUMN description_plain_text TYPE VARCHAR(16384);
ALTER TABLE actions_table ALTER COLUMN wait_result TYPE VARCHAR(16384);
ALTER TABLE actions_table ALTER COLUMN wait_result_plain_text TYPE VARCHAR(16384);

ALTER TABLE actions_table ADD COLUMN link_original_test_content_id bigint;

COMMENT ON COLUMN actions_table.link_original_test_content_id IS 'Identifiant du test original lié';

CREATE OR REPLACE VIEW actions AS select * from actions_table;
GRANT SELECT ON actions TO admin_role, writer_role, reader_role;

-- CONTENUS D'EXIGENCES
-- Ajout d'un status (V)alide, (I)nvalide, En (C)ours de validation)
ALTER TABLE requirements_contents_table ADD COLUMN status CHAR(1) DEFAULT 'I';

COMMENT ON COLUMN requirements_contents_table.status IS 'Status de l''exigence';

CREATE OR REPLACE VIEW requirements_contents AS select * from requirements_contents_table;
GRANT SELECT ON requirements_contents TO admin_role, writer_role, reader_role;

CREATE OR REPLACE VIEW REQUIREMENTS_HIERARCHY AS 
SELECT
	requirements_contents_table.short_name AS short_name,
	requirements_contents_table.category_id AS category_id,
	requirements_contents_table.priority_level AS priority_level,
	requirements_contents_table.version AS content_version,
	requirements_table.requirement_id AS requirement_id,
	requirements_table.requirement_content_id AS requirement_content_id,
	requirements_table.parent_requirement_id AS parent_requirement_id,
	requirements_table.previous_requirement_id AS previous_requirement_id,
	requirements_table.project_id AS project_id,
	requirements_table.version AS version,
	requirements_contents_table.status AS status,
	requirements_contents_table.original_requirement_content_id AS original_requirement_content_id 
FROM 
	requirements_contents_table, requirements_table 
WHERE
	requirements_contents_table.requirement_content_id = requirements_table.requirement_content_id;
GRANT SELECT ON REQUIREMENTS_HIERARCHY TO admin_role, writer_role, reader_role;

-- CONTENUS DE TESTS
-- Ajout d'un status (V)alide, (I)nvalide, En (C)ours de validation)
ALTER TABLE tests_contents_table ADD COLUMN status CHAR(1) DEFAULT 'I';

COMMENT ON COLUMN tests_contents_table.status IS 'Status du test';

CREATE OR REPLACE VIEW tests_contents AS select * from tests_contents_table;
GRANT SELECT ON tests_contents TO admin_role, writer_role, reader_role;

CREATE OR REPLACE VIEW TESTS_HIERARCHY AS 
SELECT
	tests_contents_table.short_name AS short_name,
	tests_contents_table.category_id AS category_id,
	tests_contents_table.priority_level AS priority_level,
	tests_contents_table.version AS content_version,
	tests_table.test_id AS test_id,
	tests_table.test_content_id AS test_content_id,
	tests_table.original_test_id AS original_test_id,
	tests_table.parent_test_id AS parent_test_id,
	tests_table.previous_test_id AS previous_test_id,
	tests_table.project_id AS project_id,
	tests_table.version AS version,
	tests_contents_table.status AS status,
	tests_contents_table.original_test_content_id AS original_test_content_id 
FROM 
	tests_contents_table, tests_table 
WHERE
	tests_contents_table.test_content_id = tests_table.test_content_id;
GRANT SELECT ON TESTS_HIERARCHY TO admin_role, writer_role, reader_role;

-- STATUS DES CONTENUS D'EXIGENCES ET DES CONTENUS DE TESTS
CREATE TABLE status_table (
	status_id CHAR(1) PRIMARY KEY,
	status_label VARCHAR(64) NOT NULL
);


COMMENT ON TABLE status_table IS 'Status des contenus d''exigences et des contenus de tests';
COMMENT ON COLUMN status_table.status_id IS 'Identifiant du status';
COMMENT ON COLUMN status_table.status_label IS 'Libelle du status';

CREATE OR REPLACE VIEW status AS SELECT * FROM status_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE status_table TO admin_role;
GRANT SELECT ON status_table TO writer_role, reader_role;
GRANT SELECT ON status TO admin_role, writer_role, reader_role;

INSERT INTO status_table (status_id, status_label)
VALUES ('V', 'Valide');

INSERT INTO status_table (status_id, status_label)
VALUES ('I', 'Invalide');

INSERT INTO status_table (status_id, status_label)
VALUES ('C', 'En cours de validation');

-- CAMPAGNES
DROP VIEW campaigns;
ALTER TABLE campaigns_table ALTER COLUMN description TYPE VARCHAR(16384);
CREATE VIEW campaigns AS select * from campaigns_table;
GRANT SELECT ON campaigns TO admin_role, writer_role, reader_role;

-- EXECUTIONS DE TESTS
DROP VIEW executions_tests;
ALTER TABLE executions_tests_table ALTER COLUMN comments TYPE VARCHAR(16384);
CREATE VIEW executions_tests AS select * from executions_tests_table;
GRANT SELECT ON executions_tests TO admin_role, writer_role, reader_role;

-- EXECUTIONS D'ACTIONS
DROP VIEW executions_actions;
ALTER TABLE executions_actions_table ALTER COLUMN comments TYPE VARCHAR(16384);
CREATE VIEW executions_actions AS select * from executions_actions_table;
GRANT SELECT ON executions_actions TO admin_role, writer_role, reader_role;

-- EXECUTIONS D'EXIGENCES
DROP VIEW executions_requirements;
ALTER TABLE executions_requirements_table ALTER COLUMN comments TYPE VARCHAR(16384);
CREATE VIEW executions_requirements AS select * from executions_requirements_table;
GRANT SELECT ON executions_requirements TO admin_role, writer_role, reader_role;

-- Numeros de versions
CREATE OR REPLACE FUNCTION update_versions_numbers()
RETURNS VOID AS $$
DECLARE

	tmp_project_id BIGINT;
	tmp_project_version varchar;
	tmp_str varchar;

	tmp_new_project_version varchar default '';

	tmp_index int;
BEGIN

	FOR tmp_project_id, tmp_project_version IN SELECT project_id, version FROM projects_versions_table LOOP
		tmp_new_project_version := '';
		tmp_str := tmp_project_version;
		tmp_index := strpos(tmp_str, '.');
		IF tmp_index > 0 THEN
			IF tmp_index = 2 THEN
				tmp_new_project_version := tmp_new_project_version || '0' || substr(tmp_str, 1, 1);
			ELSE
				tmp_new_project_version := tmp_new_project_version || substr(tmp_str, 1, 2);
			END IF;
			tmp_str := substr(tmp_str, tmp_index + 1);

			tmp_index := strpos(tmp_str, '.');
			IF tmp_index > 0 THEN
				IF tmp_index = 2 THEN
					tmp_new_project_version := tmp_new_project_version || '.0' || substr(tmp_str, 1, 1);
				ELSE
					tmp_new_project_version := tmp_new_project_version || '.' || substr(tmp_str, 1, 2);
				END IF;
				tmp_str := substr(tmp_str, tmp_index + 1);

				tmp_index := strpos(tmp_str, '.');
				IF tmp_index > 0 THEN
					IF tmp_index = 2 THEN
						tmp_new_project_version := tmp_new_project_version || '.0' || substr(tmp_str, 1, 1);
					ELSE
						tmp_new_project_version := tmp_new_project_version || '.' || substr(tmp_str, 1, 2);
					END IF;
					tmp_str := substr(tmp_str, tmp_index + 1);


					IF length(tmp_str) = 1 THEN
						tmp_new_project_version := tmp_new_project_version || '.0' || tmp_str;
					ELSE
						tmp_new_project_version := tmp_new_project_version || '.' || tmp_str;
					END IF;

					-- Mise à jour de la base
					UPDATE projects_versions_table SET version=tmp_new_project_version WHERE project_id=tmp_project_id AND version=tmp_project_version;

				END IF;

			END IF;
		END IF;

	END LOOP;

END;
$$ LANGUAGE plpgsql;

ALTER TABLE campaigns_table DROP CONSTRAINT campaigns_table_project_id_fkey;
ALTER TABLE requirements_contents_table DROP CONSTRAINT requirements_contents_table_project_id_fkey;
ALTER TABLE requirements_table DROP CONSTRAINT requirements_table_project_id_fkey;
ALTER TABLE tests_contents_table DROP CONSTRAINT tests_contents_table_project_id_fkey;
ALTER TABLE tests_table DROP CONSTRAINT tests_table_project_id_fkey;

ALTER TABLE campaigns_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE requirements_contents_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE requirements_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE tests_contents_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE tests_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

SELECT update_versions_numbers();
