﻿DROP FUNCTION create_test_from_test(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_test_id BIGINT, in_previous_test_id BIGINT, in_src_test_id BIGINT, in_new_test_id BIGINT);
CREATE OR REPLACE FUNCTION create_test_from_test(in_project_id BIGINT, in_dst_version VARCHAR(12), in_parent_test_id BIGINT, in_previous_test_id BIGINT, in_src_test_id BIGINT, in_new_test_id BIGINT)
RETURNS BIGINT AS $$
DECLARE

	tmp_src_test_record RECORD;
	tmp_src_test_content_record RECORD;
	
	tmp_child_test_record RECORD;
	tmp_action_record RECORD;
	tmp_requirement_content_record RECORD;
	
	tmp_next_test_id BIGINT;
	
	tmp_test_content_id BIGINT;
	tmp_previous_action_id BIGINT;
	tmp_action_id BIGINT;
	
	tmp_parent_test_id BIGINT;
	tmp_previous_test_id BIGINT;
	tmp_original_test_id BIGINT;
	tmp_child_test_id BIGINT;

	tmp_new_test_id BIGINT;
	tmp_return BIGINT default 0;
	
BEGIN

	SELECT * INTO tmp_src_test_record FROM tests_table WHERE test_id=in_src_test_id;
	IF FOUND THEN
	
		SELECT * INTO tmp_src_test_content_record FROM tests_contents_table WHERE test_content_id=tmp_src_test_record.test_content_id;
		IF FOUND THEN
		
			IF in_previous_test_id IS NULL THEN
				SELECT test_id INTO tmp_next_test_id FROM tests_table WHERE parent_test_id=in_parent_test_id AND previous_test_id IS NULL;
			ELSE
				SELECT test_id INTO tmp_next_test_id FROM tests_table WHERE parent_test_id=in_parent_test_id AND previous_test_id=in_previous_test_id;
			END IF;
			
			IF tmp_src_test_record.project_id=in_project_id AND tmp_src_test_record.version=in_dst_version AND tmp_src_test_record.original_test_id IS NOT NULL THEN

				tmp_test_content_id := tmp_src_test_record.test_content_id;
				tmp_original_test_id := tmp_src_test_record.original_test_id;
				
			ELSE
			
				INSERT INTO tests_contents_table (short_name, description, category_id, priority_level, project_id, version)
				VALUES (
					tmp_src_test_content_record.short_name,
					tmp_src_test_content_record.description,
					tmp_src_test_content_record.category_id,
					tmp_src_test_content_record.priority_level,
					in_project_id,
					in_dst_version);
				SELECT currval('tests_contents_test_content_id_seq') INTO tmp_test_content_id;

				tmp_previous_action_id := NULL;
				
				SELECT * INTO tmp_action_record FROM actions_table
				WHERE test_content_id=tmp_src_test_record.test_content_id AND previous_action_id IS NULL;

				WHILE FOUND LOOP
					tmp_action_id := tmp_action_record.action_id;

					INSERT INTO actions_table (test_content_id, previous_action_id, short_name, description, wait_result, link_original_test_content_id)
					VALUES (tmp_test_content_id, tmp_previous_action_id, tmp_action_record.short_name, tmp_action_record.description, tmp_action_record.wait_result, tmp_action_record.link_original_test_content_id);
					IF FOUND THEN
						SELECT currval('actions_action_id_seq') INTO tmp_previous_action_id;
						SELECT * INTO tmp_action_record FROM actions_table WHERE test_content_id=tmp_src_test_record.test_content_id AND previous_action_id=tmp_action_id;
					END IF;
					
				END LOOP;

				IF tmp_src_test_record.project_id=in_project_id THEN
				
					FOR tmp_requirement_content_record IN SELECT * FROM tests_requirements_table WHERE test_content_id=tmp_src_test_record.test_content_id LOOP
						INSERT INTO tests_requirements_table (test_content_id, original_requirement_content_id)
						VALUES (tmp_test_content_id, tmp_requirement_content_record.original_requirement_content_id);
					END LOOP;
					
				END IF;

			END IF;
			
			INSERT INTO tests_table (test_content_id, original_test_id, parent_test_id, previous_test_id, project_id, version) VALUES (tmp_test_content_id, tmp_original_test_id, in_parent_test_id, in_previous_test_id, in_project_id, in_dst_version);
			SELECT currval('tests_test_id_seq') INTO tmp_return;
			
			IF tmp_next_test_id IS NOT NULL THEN
				UPDATE tests_table SET previous_test_id=tmp_return WHERE test_id=tmp_next_test_id;
			END IF;
			
			IF tmp_original_test_id IS NULL THEN
			
				tmp_parent_test_id := tmp_src_test_record.test_id;
				tmp_child_test_id := NULL;
				tmp_previous_test_id := NULL;
				IF in_new_test_id IS NULL THEN
					tmp_new_test_id:=tmp_return;
				ELSE
					tmp_new_test_id:=in_new_test_id;
				END IF;
				
				SELECT * INTO tmp_child_test_record FROM tests_table WHERE parent_test_id=tmp_parent_test_id AND previous_test_id IS NULL;
				IF FOUND AND tmp_child_test_record.test_id=tmp_new_test_id THEN
					SELECT * INTO tmp_child_test_record FROM tests_table WHERE parent_test_id=tmp_parent_test_id AND previous_test_id=tmp_new_test_id;
				END IF;
	
				WHILE FOUND LOOP
				
					tmp_child_test_id := create_test_from_test(in_project_id, in_dst_version, tmp_return, tmp_child_test_id, tmp_child_test_record.test_id, tmp_return);
					tmp_previous_test_id := tmp_child_test_record.test_id;
					SELECT * INTO tmp_child_test_record FROM tests_table WHERE parent_test_id=tmp_parent_test_id AND previous_test_id=tmp_previous_test_id;
					IF FOUND AND tmp_child_test_record.test_id=tmp_new_test_id THEN
						SELECT * INTO tmp_child_test_record FROM tests_table WHERE parent_test_id=tmp_parent_test_id AND previous_test_id=tmp_new_test_id;
					END IF;
					
				END LOOP;
				
			END IF;
			
		ELSE
		
			RAISE NOTICE 'Pas de contenu de test %', tmp_src_test_record.test_content_id;
			
		END IF;
		
	ELSE
	
		RAISE NOTICE 'Pas de test %', in_src_test_id;
		
	END IF;
	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;

DROP FUNCTION html_to_plain_text(in_html_string VARCHAR);

CREATE OR REPLACE FUNCTION html_to_plain_text(in_html_string VARCHAR)
RETURNS varchar AS $$
DECLARE

	tmp_return varchar default '';
	tmp_str varchar default '';
	tmp_start INTEGER DEFAULT 1;
	tmp_end INTEGER DEFAULT 1;

BEGIN

	tmp_start := strpos(in_html_string, '<body');
	IF tmp_start > 0 THEN
		tmp_str := substr(in_html_string, tmp_start);
	ELSE
		tmp_str := in_html_string;
	END IF;

	WHILE tmp_start > 0 AND tmp_end > 0 LOOP
		tmp_start := strpos(tmp_str, '<');
		IF tmp_start > 0 THEN
			tmp_return := tmp_return || substr(tmp_str, 1, tmp_start - 1);
			tmp_end := strpos(tmp_str, '>');
			IF tmp_end > 0 THEN
				tmp_str := substr(tmp_str, tmp_end + 1);
			END IF;
		END IF;
	END LOOP;
	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;

UPDATE tests_contents_table SET description_plain_text=html_to_plain_text(description);
UPDATE requirements_contents_table SET description_plain_text=html_to_plain_text(description);
UPDATE actions_table SET description_plain_text=html_to_plain_text(description), wait_result_plain_text=html_to_plain_text(wait_result);

-- VERSIONS DE PROJETS
DROP VIEW projects_versions;
ALTER TABLE projects_versions_table ALTER COLUMN bug_tracker_type TYPE VARCHAR(64);
CREATE VIEW projects_versions AS SELECT * FROM projects_versions_table;
GRANT SELECT ON projects_versions TO admin_role, writer_role, reader_role;

UPDATE projects_versions_table SET bug_tracker_type='Bugzilla' WHERE bug_tracker_type='B';
UPDATE projects_versions_table SET bug_tracker_type='Mantis' WHERE bug_tracker_type='M';

-- ANOMALIES
DROP VIEW bugs;
ALTER TABLE bugs_table ADD status CHAR(1) DEFAULT 'O';
COMMENT ON COLUMN bugs_table.status IS 'Status de l''anomalie';
CREATE VIEW bugs AS select * from bugs_table;
GRANT SELECT ON bugs TO admin_role, writer_role, reader_role;

-- Copie de version de projet : ajout de la copie des campagnes
DROP FUNCTION copy_project_version(in_project_id BIGINT, in_src_version  VARCHAR(12), in_dst_version VARCHAR(12));
CREATE OR REPLACE FUNCTION copy_project_version(in_project_id BIGINT, in_src_version VARCHAR(12), in_dst_version VARCHAR(12))
RETURNS INT AS $$
DECLARE

	tmp_test_record RECORD;
	tmp_requirement_record RECORD;
	tmp_campaign_record RECORD;
	tmp_test_campaign_record RECORD;
	tmp_id BIGINT;

	tmp_original_id BIGINT;
	tmp_parent_id BIGINT;
	tmp_previous_id BIGINT;

	tmp_return INT default 0;
BEGIN


	CREATE TEMPORARY TABLE TEST_ID_MATRIX
	(
		src_test_id BIGINT PRIMARY KEY,
		dst_test_id BIGINT
	)
	ON COMMIT DROP;

	CREATE TEMPORARY TABLE REQUIREMENT_ID_MATRIX
	(
		src_requirement_id BIGINT PRIMARY KEY,
		dst_requirement_id BIGINT
	)
	ON COMMIT DROP;

	-- Creer la version
	INSERT INTO projects_versions_table (project_id, version) VALUES (in_project_id, in_dst_version);

	-- Copier tous les tests
	FOR tmp_test_record IN SELECT * FROM tests_table WHERE project_id=in_project_id AND version=in_src_version LOOP
		INSERT INTO tests_table (original_test_id, parent_test_id, previous_test_id, test_content_id, project_id, version)
			VALUES (tmp_test_record.original_test_id, tmp_test_record.parent_test_id, tmp_test_record.previous_test_id, tmp_test_record.test_content_id, in_project_id, in_dst_version);
		SELECT currval('tests_test_id_seq') INTO tmp_id;
		INSERT INTO TEST_ID_MATRIX (src_test_id, dst_test_id) VALUES (tmp_test_record.test_id, tmp_id);
	END LOOP;

	FOR tmp_test_record IN SELECT * FROM tests_table WHERE project_id=in_project_id AND version=in_dst_version LOOP

		tmp_original_id := NULL;
		tmp_parent_id := NULL;
		tmp_previous_id := NULL;

		-- Identifiant du test original
		IF tmp_test_record.original_test_id IS NOT NULL THEN
			SELECT dst_test_id INTO tmp_original_id FROM TEST_ID_MATRIX WHERE src_test_id=tmp_test_record.original_test_id;
		END IF;

		-- Identifiant du test parent
		IF tmp_test_record.parent_test_id IS NOT NULL THEN
			SELECT dst_test_id INTO tmp_parent_id FROM TEST_ID_MATRIX WHERE src_test_id=tmp_test_record.parent_test_id;
		END IF;

		-- Identifiant du test précédent
		IF tmp_test_record.previous_test_id IS NOT NULL THEN
			SELECT dst_test_id INTO tmp_previous_id FROM TEST_ID_MATRIX WHERE src_test_id=tmp_test_record.previous_test_id;
		END IF;

		UPDATE tests_table SET original_test_id=tmp_original_id, parent_test_id=tmp_parent_id, previous_test_id=tmp_previous_id WHERE test_id=tmp_test_record.test_id;

		SELECT dst_test_id INTO tmp_original_id FROM TEST_ID_MATRIX WHERE src_test_id=tmp_test_record.original_test_id;
		
	END LOOP;

	-- Copier toutes les exigences
	FOR tmp_requirement_record IN SELECT * FROM requirements_table WHERE project_id=in_project_id AND version=in_src_version LOOP
		INSERT INTO requirements_table (parent_requirement_id, previous_requirement_id, requirement_content_id, project_id, version)
			VALUES (tmp_requirement_record.parent_requirement_id, tmp_requirement_record.previous_requirement_id, tmp_requirement_record.requirement_content_id, in_project_id, in_dst_version);
		SELECT currval('requirements_requirement_id_seq') INTO tmp_id;
		INSERT INTO REQUIREMENT_ID_MATRIX (src_requirement_id, dst_requirement_id) VALUES (tmp_requirement_record.requirement_id, tmp_id);
	END LOOP;

	FOR tmp_requirement_record IN SELECT * FROM requirements_table WHERE project_id=in_project_id AND version=in_dst_version LOOP

		tmp_parent_id := NULL;
		tmp_previous_id := NULL;

		-- Identifiant de l'exigence parente
		IF tmp_requirement_record.parent_requirement_id IS NOT NULL THEN
			SELECT dst_requirement_id INTO tmp_parent_id FROM REQUIREMENT_ID_MATRIX WHERE src_requirement_id=tmp_requirement_record.parent_requirement_id;
		END IF;

		-- Identifiant de l'exigence précédente
		IF tmp_requirement_record.previous_requirement_id IS NOT NULL THEN
			SELECT dst_requirement_id INTO tmp_previous_id FROM REQUIREMENT_ID_MATRIX WHERE src_requirement_id=tmp_requirement_record.previous_requirement_id;
		END IF;

		UPDATE requirements_table SET parent_requirement_id=tmp_parent_id, previous_requirement_id=tmp_previous_id WHERE requirement_id=tmp_requirement_record.requirement_id;

	END LOOP;

	-- Copier toutes les campagnes
	FOR tmp_campaign_record IN SELECT * FROM campaigns_table WHERE project_id=in_project_id AND version=in_src_version LOOP

		INSERT INTO campaigns_table (project_id, version, short_name, description)
			VALUES (in_project_id, in_dst_version, tmp_campaign_record.short_name , tmp_campaign_record.description);
		SELECT currval('campaigns_campaign_id_seq') INTO tmp_id;

		-- Creer les tests de campagnes
		tmp_previous_id := NULL;
		FOR tmp_test_campaign_record IN SELECT * FROM tests_campaigns_table WHERE campaign_id=tmp_campaign_record.campaign_id LOOP
			SELECT dst_test_id INTO tmp_original_id FROM TEST_ID_MATRIX WHERE src_test_id=tmp_test_campaign_record.test_id;
			IF tmp_original_id IS NOT NULL THEN
				INSERT INTO tests_campaigns_table (campaign_id, test_id, previous_test_campaign_id)
					VALUES (tmp_id, tmp_original_id, tmp_previous_id);
				SELECT currval('tests_campaigns_test_campaign_id_seq') INTO tmp_previous_id;
			END IF;
		END LOOP;

	END LOOP;


	return tmp_return;

END;
$$ LANGUAGE plpgsql;

-- ********************************************
-- DATABASE_VERSION 
-- ********************************************
CREATE TABLE database_version_table (
  database_version_number varchar(12) NOT NULL,
  upgrade_date timestamp with time zone NOT NULL DEFAULT now()
);

COMMENT ON TABLE database_version_table IS 'Version de la base de données';

GRANT SELECT ON database_version_table TO PUBLIC;

INSERT INTO database_version_table (database_version_number) VALUES ('01.04.00.00');

