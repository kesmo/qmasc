-- project version
DROP VIEW projects_versions;

ALTER TABLE projects_versions_table DROP CONSTRAINT projects_versions_table_pkey CASCADE;
ALTER TABLE projects_versions_table DROP CONSTRAINT projects_versions_table_project_version_id_key;

ALTER TABLE projects_versions_table ADD PRIMARY KEY (project_version_id);
ALTER TABLE projects_versions_table ADD UNIQUE (project_id, version);

CREATE VIEW projects_versions AS SELECT * FROM projects_versions_table;
GRANT SELECT ON projects_versions TO admin_role, writer_role, reader_role;

-- features
DROP VIEW features;
DROP VIEW features_hierarchy;

ALTER TABLE features_table DROP CONSTRAINT features_table_project_id_fkey;
ALTER TABLE features_table ADD COLUMN project_version_id BIGINT;

UPDATE features_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=features_table.project_id
and projects_versions_table.version=features_table.version);
ALTER TABLE features_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE features_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE features_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW features AS select * from features_table;
GRANT SELECT ON features TO admin_role, writer_role, reader_role;

-- feature contents
DROP VIEW features_contents;

ALTER TABLE features_contents_table DROP CONSTRAINT features_contents_table_project_id_fkey;
ALTER TABLE features_contents_table ADD COLUMN project_version_id BIGINT;

UPDATE features_contents_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=features_contents_table.project_id
and projects_versions_table.version=features_contents_table.version);
ALTER TABLE features_contents_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE features_contents_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE features_contents_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW features_contents AS select * from features_contents_table;
GRANT SELECT ON features_contents TO admin_role, writer_role, reader_role;

DROP VIEW features_hierarchy;
CREATE OR REPLACE VIEW features_hierarchy AS 
SELECT
	features_contents_table.short_name AS short_name,
	features_contents_table.version AS content_version,
	features_table.feature_id AS feature_id,
	features_table.feature_content_id AS feature_content_id,
	features_table.parent_feature_id AS parent_feature_id,
	features_table.previous_feature_id AS previous_feature_id,
	features_table.project_version_id AS project_version_id,
	features_table.project_id AS project_id,
	features_table.version AS version,
	features_contents_table.status AS status
FROM 
	features_contents_table, features_table 
WHERE
	features_contents_table.feature_content_id = features_table.feature_content_id;
GRANT SELECT ON features_hierarchy TO admin_role, writer_role, reader_role;

-- rules
DROP VIEW rules;
DROP VIEW rules_hierarchy;

ALTER TABLE rules_table DROP CONSTRAINT rules_table_project_id_fkey;
ALTER TABLE rules_table ADD COLUMN project_version_id BIGINT;

UPDATE rules_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=rules_table.project_id
and projects_versions_table.version=rules_table.version);
ALTER TABLE rules_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE rules_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE rules_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW rules AS select * from rules_table;
GRANT SELECT ON rules TO admin_role, writer_role, reader_role;

-- rule contents
DROP VIEW rules_contents;

ALTER TABLE rules_contents_table DROP CONSTRAINT rules_contents_table_project_id_fkey;
ALTER TABLE rules_contents_table ADD COLUMN project_version_id BIGINT;

UPDATE rules_contents_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=rules_contents_table.project_id
and projects_versions_table.version=rules_contents_table.version);
ALTER TABLE rules_contents_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE rules_contents_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE rules_contents_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW rules_contents AS select * from rules_contents_table;
GRANT SELECT ON rules_contents TO admin_role, writer_role, reader_role;

DROP VIEW rules_hierarchy;
CREATE OR REPLACE VIEW rules_hierarchy AS 
SELECT
	rules_contents_table.short_name AS short_name,
	rules_contents_table.version AS content_version,
	rules_table.rule_id AS rule_id,
	rules_table.rule_content_id AS rule_content_id,
	rules_table.parent_rule_id AS parent_rule_id,
	rules_table.previous_rule_id AS previous_rule_id,
	rules_table.project_version_id AS project_version_id,
	rules_table.project_id AS project_id,
	rules_table.version AS version,
	rules_contents_table.status AS status
FROM 
	rules_contents_table, rules_table 
WHERE
	rules_contents_table.rule_content_id = rules_table.rule_content_id;
GRANT SELECT ON rules_hierarchy TO admin_role, writer_role, reader_role;

-- tests
DROP VIEW tests;
DROP VIEW tests_hierarchy;

ALTER TABLE tests_table DROP CONSTRAINT tests_table_project_id_fkey;
ALTER TABLE tests_table ADD COLUMN project_version_id BIGINT;

UPDATE tests_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=tests_table.project_id
and projects_versions_table.version=tests_table.version);
ALTER TABLE tests_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE tests_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE tests_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW tests AS select * from tests_table;
GRANT SELECT ON tests TO admin_role, writer_role, reader_role;

-- test contents
DROP VIEW tests_contents;

ALTER TABLE tests_contents_table DROP CONSTRAINT tests_contents_table_project_id_fkey;
ALTER TABLE tests_contents_table ADD COLUMN project_version_id BIGINT;

UPDATE tests_contents_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=tests_contents_table.project_id
and projects_versions_table.version=tests_contents_table.version);
ALTER TABLE tests_contents_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE tests_contents_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE tests_contents_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE tests_contents_table ALTER COLUMN original_test_content_id DROP DEFAULT;
ALTER TABLE tests_contents_table ALTER COLUMN original_test_content_id DROP NOT NULL;
ALTER TABLE tests_contents_table DROP CONSTRAINT original_test_content_version_pk;

UPDATE actions_table SET link_original_test_content_id=
(SELECT test_content_id from tests_contents_table
where actions_table.link_original_test_content_id=tests_contents_table.original_test_content_id order by tests_contents_table.version asc limit 1);
ALTER TABLE actions_table ADD FOREIGN KEY (link_original_test_content_id) REFERENCES tests_contents_table(test_content_id) ON DELETE SET NULL;

UPDATE tests_contents_table AS rc1 SET original_test_content_id =
(select test_content_id from tests_contents_table as rc2
where rc2.original_test_content_id=rc1.original_test_content_id order by rc2.version asc limit 1);

UPDATE tests_contents_table SET original_test_content_id = null
where original_test_content_id=test_content_id;

ALTER TABLE tests_contents_table ADD FOREIGN KEY (original_test_content_id) REFERENCES tests_contents_table(test_content_id) ON DELETE SET NULL;

CREATE OR REPLACE VIEW tests_contents AS select * from tests_contents_table;
GRANT SELECT ON tests_contents TO admin_role, writer_role, reader_role;

DROP VIEW TESTS_HIERARCHY;
CREATE OR REPLACE VIEW TESTS_HIERARCHY AS 
SELECT
	tests_contents_table.short_name AS short_name,
	tests_contents_table.category_id AS category_id,
	tests_contents_table.priority_level AS priority_level,
	tests_contents_table.version AS content_version,
	tests_table.test_id AS test_id,
	tests_table.test_content_id AS test_content_id,
	tests_table.original_test_id AS original_test_id,
	tests_table.parent_test_id AS parent_test_id,
	tests_table.previous_test_id AS previous_test_id,
	tests_table.project_version_id AS project_version_id,
	tests_table.project_id AS project_id,
	tests_table.version AS version,
	tests_contents_table.status AS status,
	tests_contents_table.original_test_content_id AS original_test_content_id,
	tests_contents_table.automated AS content_automated,
	tests_contents_table.type AS content_type
FROM 
	tests_contents_table, tests_table 
WHERE
	tests_contents_table.test_content_id = tests_table.test_content_id;
GRANT SELECT ON TESTS_HIERARCHY TO admin_role, writer_role, reader_role;

-- requirements
DROP VIEW requirements;
DROP VIEW requirements_hierarchy;

ALTER TABLE requirements_table DROP CONSTRAINT requirements_table_project_id_fkey;
ALTER TABLE requirements_table ADD COLUMN project_version_id BIGINT;

UPDATE requirements_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=requirements_table.project_id
and projects_versions_table.version=requirements_table.version);
ALTER TABLE requirements_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE requirements_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE requirements_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW requirements AS select * from requirements_table;
GRANT SELECT ON requirements TO admin_role, writer_role, reader_role;

-- tests requirements
ALTER TABLE tests_requirements_table RENAME COLUMN original_requirement_content_id TO requirement_content_id;
UPDATE tests_requirements_table SET requirement_content_id =
(select requirement_content_id from requirements_contents_table
where requirements_contents_table.original_requirement_content_id=tests_requirements_table.requirement_content_id
order by requirements_contents_table.version asc limit 1);
ALTER TABLE tests_requirements_table ADD FOREIGN KEY(requirement_content_id) REFERENCES requirements_contents_table(requirement_content_id) ON DELETE CASCADE;

COMMENT ON COLUMN tests_requirements_table.requirement_content_id IS 'Identifiant du contenu de l''exigence';

DROP VIEW tests_requirements;
CREATE VIEW tests_requirements AS SELECT * FROM tests_requirements_table;
GRANT SELECT ON tests_requirements TO admin_role, writer_role, reader_role;

-- requirement contents
DROP VIEW requirements_contents;

ALTER TABLE requirements_contents_table DROP CONSTRAINT requirements_contents_table_project_id_fkey;
ALTER TABLE requirements_contents_table ADD COLUMN project_version_id BIGINT;

UPDATE requirements_contents_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=requirements_contents_table.project_id
and projects_versions_table.version=requirements_contents_table.version);
ALTER TABLE requirements_contents_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE requirements_contents_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE requirements_contents_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE requirements_contents_table ALTER COLUMN original_requirement_content_id DROP DEFAULT;
ALTER TABLE requirements_contents_table ALTER COLUMN original_requirement_content_id DROP NOT NULL;

UPDATE requirements_contents_table AS rc1 SET original_requirement_content_id =
(select requirement_content_id from requirements_contents_table as rc2
where rc2.original_requirement_content_id=rc1.original_requirement_content_id order by rc2.version asc limit 1);

UPDATE requirements_contents_table SET original_requirement_content_id = null
where original_requirement_content_id=requirement_content_id;

ALTER TABLE requirements_contents_table ADD FOREIGN KEY (original_requirement_content_id) REFERENCES requirements_contents_table(requirement_content_id) ON DELETE SET NULL;

CREATE VIEW requirements_contents AS select * from requirements_contents_table;
GRANT SELECT ON requirements_contents TO admin_role, writer_role, reader_role;

DROP VIEW REQUIREMENTS_HIERARCHY;
CREATE OR REPLACE VIEW REQUIREMENTS_HIERARCHY AS 
SELECT
	requirements_contents_table.short_name AS short_name,
	requirements_contents_table.category_id AS category_id,
	requirements_contents_table.priority_level AS priority_level,
	requirements_contents_table.version AS content_version,
	requirements_table.requirement_id AS requirement_id,
	requirements_table.requirement_content_id AS requirement_content_id,
	requirements_table.parent_requirement_id AS parent_requirement_id,
	requirements_table.previous_requirement_id AS previous_requirement_id,
	requirements_table.project_version_id AS project_version_id,
	requirements_table.project_id AS project_id,
	requirements_table.version AS version,
	requirements_contents_table.status AS status,
	requirements_contents_table.original_requirement_content_id AS original_requirement_content_id 
FROM 
	requirements_contents_table, requirements_table 
WHERE
	requirements_contents_table.requirement_content_id = requirements_table.requirement_content_id;
GRANT SELECT ON REQUIREMENTS_HIERARCHY TO admin_role, writer_role, reader_role;

-- campaigns
DROP VIEW campaigns;

ALTER TABLE campaigns_table DROP CONSTRAINT campaigns_table_project_id_fkey;
ALTER TABLE campaigns_table ADD COLUMN project_version_id BIGINT;

UPDATE campaigns_table SET project_version_id =
(select project_version_id from projects_versions_table
where projects_versions_table.project_id=campaigns_table.project_id
and projects_versions_table.version=campaigns_table.version);
ALTER TABLE campaigns_table ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE campaigns_table ADD FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE campaigns_table ADD FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE VIEW campaigns AS select * from campaigns_table;
GRANT SELECT ON campaigns TO admin_role, writer_role, reader_role;

-- Users
DROP VIEW users;
ALTER TABLE users_table DROP COLUMN password;
ALTER TABLE users_table ALTER COLUMN email DROP NOT NULL;
CREATE VIEW users AS SELECT * from users_table;
GRANT SELECT ON users TO admin_role, writer_role, reader_role;

-- Fix admin_role privileges
GRANT writer_role TO admin_role WITH ADMIN OPTION;
GRANT reader_role TO admin_role WITH ADMIN OPTION;

-- Mise à jour de la version logique de la base de données
UPDATE database_version_table set database_version_number='01.07.00.00', upgrade_date=now();
