-- Modification des tests
DROP VIEW tests_contents;
ALTER TABLE tests_contents_table ADD COLUMN automation_command_return_code_variable VARCHAR(256);
ALTER TABLE tests_contents_table ADD COLUMN automation_command_stdout_variable VARCHAR(256);
COMMENT ON COLUMN tests_contents_table.automation_command_return_code_variable IS 'Nom d''une variable de recuperation du code de retour';
COMMENT ON COLUMN tests_contents_table.automation_command_stdout_variable IS 'Nom d''une variable de recuperation de la sortie standard';

CREATE OR REPLACE VIEW tests_contents AS select * from tests_contents_table;
GRANT SELECT ON tests_contents TO admin_role, writer_role, reader_role;

-- Modification des executions de tests
DROP VIEW executions_tests;
ALTER TABLE executions_tests_table ADD COLUMN automation_command_return_code INT;
ALTER TABLE executions_tests_table ADD COLUMN automation_command_stdout VARCHAR(16384);
ALTER TABLE executions_tests_table ADD COLUMN user_id BIGINT;
ALTER TABLE executions_tests_table ADD FOREIGN KEY (user_id) REFERENCES users_table (user_id) ON DELETE SET NULL;
COMMENT ON COLUMN executions_tests_table.automation_command_return_code IS 'Code retour (automatisation)';
COMMENT ON COLUMN executions_tests_table.automation_command_stdout IS 'Sortie standard (automatisation)';
COMMENT ON COLUMN executions_tests_table.user_id IS 'Identifiant de l''utilisateur';

CREATE OR REPLACE VIEW executions_tests AS select * from executions_tests_table;
GRANT SELECT ON executions_tests TO admin_role, writer_role, reader_role;

-----------------------------------------------------------
-- PLAN DE TESTS
-----------------------------------------------------------
CREATE SEQUENCE tests_plans_id_seq;

CREATE TABLE tests_plans_table (
  test_plan_id bigint NOT NULL DEFAULT nextval('tests_plans_id_seq'),
  campaign_id bigint NOT NULL,
  PRIMARY KEY (test_plan_id),
  FOREIGN KEY (campaign_id) REFERENCES campaigns_table (campaign_id) ON DELETE CASCADE
);

CREATE VIEW tests_plans AS SELECT * FROM tests_plans_table;

GRANT SELECT, UPDATE ON tests_plans_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE tests_plans_table TO admin_role, writer_role;
GRANT SELECT ON TABLE tests_plans_table TO reader_role;
GRANT SELECT ON tests_plans TO admin_role, writer_role, reader_role;

-- Execution de campagne
DROP VIEW executions_campaigns;
ALTER TABLE executions_campaigns_table ADD COLUMN test_plan_id bigint;
COMMENT ON COLUMN executions_campaigns_table.test_plan_id IS 'Identifiant du plan de tests';

CREATE OR REPLACE VIEW executions_campaigns AS select * from executions_campaigns_table;
GRANT SELECT ON executions_campaigns TO admin_role, writer_role, reader_role;

ALTER TABLE executions_campaigns_table ADD FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table(test_plan_id) ON DELETE SET NULL;

-- Graphic item
CREATE SEQUENCE graphics_items_id_seq;

CREATE TABLE graphics_items_table (
  graphic_item_id bigint NOT NULL DEFAULT nextval('graphics_items_id_seq'),
  test_plan_id bigint NOT NULL,
  parent_graphic_item_id bigint,
  type CHAR NOT NULL,
  x real NOT NULL,
  y real NOT NULL,
  z_value integer,
  comments VARCHAR(128),
  data VARCHAR(1024),
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE,
  FOREIGN KEY (parent_graphic_item_id) REFERENCES graphics_items_table(graphic_item_id) ON DELETE CASCADE
);

CREATE VIEW graphics_items AS SELECT * FROM graphics_items_table;

GRANT SELECT, UPDATE ON graphics_items_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_items_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_items_table TO reader_role;
GRANT SELECT ON graphics_items TO admin_role, writer_role, reader_role;

-- Graphic point
CREATE TABLE graphics_points_table (
  graphic_item_id bigint NOT NULL,
  test_plan_id bigint NOT NULL,
  link_graphic_item_id bigint NOT NULL,
  connected_graphic_item_id bigint,
  previous_graphic_point_item_id bigint,
  type CHAR NOT NULL,
  data VARCHAR(256),
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (graphic_item_id) REFERENCES graphics_items_table(graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE,
  FOREIGN KEY (link_graphic_item_id) REFERENCES graphics_items_table(graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (connected_graphic_item_id) REFERENCES graphics_items_table(graphic_item_id) ON DELETE SET NULL,
  FOREIGN KEY (previous_graphic_point_item_id) REFERENCES graphics_points_table(graphic_item_id) ON DELETE SET NULL
);

CREATE VIEW graphics_points AS SELECT * FROM graphics_points_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_points_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_points_table TO reader_role;
GRANT SELECT ON graphics_points TO admin_role, writer_role, reader_role;

-- Graphics items specialization --
-- Graphic test
CREATE TABLE graphics_tests_table (
  graphic_item_id bigint NOT NULL,
  test_plan_id bigint NOT NULL,
  test_id bigint NOT NULL,
  return_code_var_name VARCHAR(128),
  output_var_name VARCHAR(128),
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (graphic_item_id) REFERENCES graphics_items_table (graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (test_id) REFERENCES tests_table (test_id) ON DELETE CASCADE,
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE
);

CREATE VIEW graphics_tests AS SELECT * FROM graphics_tests_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_tests_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_tests_table TO reader_role;
GRANT SELECT ON graphics_tests TO admin_role, writer_role, reader_role;

-- Graphic If
CREATE TABLE graphics_ifs_table (
  graphic_item_id bigint NOT NULL,
  test_plan_id bigint NOT NULL,
  condition VARCHAR(128),
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (graphic_item_id) REFERENCES graphics_items_table (graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE
);

CREATE VIEW graphics_ifs AS SELECT * FROM graphics_ifs_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_ifs_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_ifs_table TO reader_role;
GRANT SELECT ON graphics_ifs TO admin_role, writer_role, reader_role;

-- Graphic Switch
CREATE TABLE graphics_switches_table (
  graphic_item_id bigint NOT NULL,
  test_plan_id bigint NOT NULL,
  condition VARCHAR(128),
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (graphic_item_id) REFERENCES graphics_items_table (graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE
);

CREATE VIEW graphics_switches AS SELECT * FROM graphics_switches_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_switches_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_switches_table TO reader_role;
GRANT SELECT ON graphics_switches TO admin_role, writer_role, reader_role;

-- Graphic Loop
CREATE TABLE graphics_loops_table (
  graphic_item_id bigint NOT NULL,
  test_plan_id bigint NOT NULL,
  condition VARCHAR(128),
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (graphic_item_id) REFERENCES graphics_items_table (graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE
);

CREATE VIEW graphics_loops AS SELECT * FROM graphics_loops_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_loops_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_loops_table TO reader_role;
GRANT SELECT ON graphics_loops TO admin_role, writer_role, reader_role;

-- Graphic link
CREATE TABLE graphics_links_table (
  graphic_item_id bigint NOT NULL,
  test_plan_id bigint NOT NULL,
  PRIMARY KEY (graphic_item_id),
  FOREIGN KEY (graphic_item_id) REFERENCES graphics_items_table (graphic_item_id) ON DELETE CASCADE,
  FOREIGN KEY (test_plan_id) REFERENCES tests_plans_table (test_plan_id) ON DELETE CASCADE
);

CREATE VIEW graphics_links AS SELECT * FROM graphics_links_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE graphics_links_table TO admin_role, writer_role;
GRANT SELECT ON TABLE graphics_links_table TO reader_role;
GRANT SELECT ON graphics_links TO admin_role, writer_role, reader_role;

-----------------------------------------------------------
-- Expression des besoins
-----------------------------------------------------------
CREATE SEQUENCE needs_need_id_seq;

CREATE TABLE needs_table (
	need_id bigint NOT NULL DEFAULT nextval('needs_need_id_seq'),
	parent_need_id bigint,
	previous_need_id bigint,
	project_id bigint NOT NULL,
	creation_date timestamp with time zone NOT NULL DEFAULT now(),
	short_name VARCHAR(128),
	description VARCHAR(16384),
	description_plain_text VARCHAR(16384),
	status CHAR(1) NOT NULL DEFAULT 'C',
	PRIMARY KEY (need_id),
	FOREIGN KEY (parent_need_id) REFERENCES needs_table(need_id) ON DELETE CASCADE,
	FOREIGN KEY (previous_need_id) REFERENCES needs_table(need_id) ON DELETE SET NULL,
	FOREIGN KEY (project_id) REFERENCES projects_table(project_id) ON UPDATE CASCADE ON DELETE CASCADE
);

COMMENT ON TABLE needs_table IS 'Expression de besoins';
COMMENT ON COLUMN needs_table.need_id IS 'Identifiant unique de l''expression de besoins';
COMMENT ON COLUMN needs_table.parent_need_id IS 'Identifiant unique de l''expression de besoins parente';
COMMENT ON COLUMN needs_table.previous_need_id IS 'Identifiant de l''expression de besoins precedente';
COMMENT ON COLUMN needs_table.project_id IS 'Identifiant du projet';
COMMENT ON COLUMN needs_table.creation_date IS 'Date de création';
COMMENT ON COLUMN needs_table.short_name IS 'Nom abrege';
COMMENT ON COLUMN needs_table.description IS 'Description';
COMMENT ON COLUMN needs_table.description_plain_text IS 'Description (texte brut)';
COMMENT ON COLUMN needs_table.status IS 'Status';

CREATE OR REPLACE VIEW needs AS select * from needs_table;

GRANT SELECT, UPDATE ON needs_need_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE needs_table TO admin_role, writer_role;
GRANT SELECT ON TABLE needs_table TO reader_role;
GRANT SELECT ON needs TO admin_role, writer_role, reader_role;

CREATE OR REPLACE FUNCTION NEEDS_TABLE_TRIGGER_FUNCTION()
RETURNS trigger AS $$
BEGIN
	NEW.description_plain_text:=html_to_plain_text(NEW.description);
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER NEEDS_TABLE_TRIGGER BEFORE INSERT OR UPDATE
ON needs_table  FOR EACH ROW
EXECUTE PROCEDURE NEEDS_TABLE_TRIGGER_FUNCTION();

-- Specification / fonctionnalité
CREATE SEQUENCE features_contents_feature_content_id_seq;
CREATE TABLE features_contents_table (
	feature_content_id bigint DEFAULT nextval('features_contents_feature_content_id_seq'),
	project_id bigint NOT NULL,
	version VARCHAR(12) NOT NULL,
	need_id bigint,
	short_name VARCHAR(128),
	description VARCHAR(16384),
	description_plain_text VARCHAR(16384),
	status CHAR(1) DEFAULT 'I',
	PRIMARY KEY (feature_content_id),
	FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (need_id) REFERENCES needs_table(need_id) ON DELETE SET NULL
);
COMMENT ON TABLE features_contents_table IS 'Contenus des fonctionnalités';
COMMENT ON COLUMN features_contents_table.feature_content_id  IS 'Identifiant unique du contenu de la fonctionnalité';
COMMENT ON COLUMN features_contents_table.project_id IS 'Identifiant du projet associe';
COMMENT ON COLUMN features_contents_table.version IS 'Numéro de version';
COMMENT ON COLUMN features_contents_table.need_id IS 'Identifiant de l''expression de besoin associée';
COMMENT ON COLUMN features_contents_table.short_name IS 'Nom abrege de la fonctionnalité';
COMMENT ON COLUMN features_contents_table.description IS 'Description de la fonctionnalité';
COMMENT ON COLUMN features_contents_table.description_plain_text IS 'Description de la fonctionnalité (texte brut)';
COMMENT ON COLUMN features_contents_table.status IS 'Status de la fonctionnalité';
 
CREATE VIEW features_contents AS select * from features_contents_table;

GRANT SELECT, UPDATE ON features_contents_feature_content_id_seq TO admin_role, writer_role;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE features_contents_table TO admin_role, writer_role;
GRANT SELECT ON TABLE features_contents_table TO reader_role;
GRANT SELECT ON features_contents TO admin_role, writer_role, reader_role;

CREATE OR REPLACE FUNCTION FEATURES_CONTENTS_TABLE_TRIGGER_FUNCTION()
RETURNS trigger AS $$
BEGIN
	NEW.description_plain_text:=html_to_plain_text(NEW.description);
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER FEATURES_CONTENTS_TABLE_TRIGGER BEFORE INSERT OR UPDATE
ON features_contents_table  FOR EACH ROW
EXECUTE PROCEDURE FEATURES_CONTENTS_TABLE_TRIGGER_FUNCTION();

CREATE SEQUENCE features_feature_id_seq;

CREATE TABLE features_table (
  feature_id bigint NOT NULL DEFAULT nextval('features_feature_id_seq'),
  feature_content_id bigint NOT NULL,
  parent_feature_id bigint,
  previous_feature_id bigint,
  project_id bigint NOT NULL,
  version VARCHAR(12) NOT NULL,
  PRIMARY KEY (feature_id),
  FOREIGN KEY (feature_content_id) REFERENCES features_contents_table(feature_content_id) ON DELETE CASCADE,
  FOREIGN KEY (parent_feature_id) REFERENCES features_table(feature_id) ON DELETE CASCADE,
  FOREIGN KEY (previous_feature_id) REFERENCES features_table(feature_id) ON DELETE SET NULL,
  FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE
);

COMMENT ON TABLE features_table IS 'Fonctionnalités';
COMMENT ON COLUMN features_table.feature_id IS 'Identifiant unique de la fonctionnalité';
COMMENT ON COLUMN features_table.parent_feature_id IS 'Identifiant unique de la fonctionnalité parente';
COMMENT ON COLUMN features_table.previous_feature_id IS 'Identifiant de la fonctionnalité precedente';
COMMENT ON COLUMN features_table.project_id IS 'Identifiant du projet';
COMMENT ON COLUMN features_table.version IS 'Identifiant de la version du projet';

CREATE OR REPLACE VIEW features AS select * from features_table;

GRANT SELECT, UPDATE ON features_feature_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE features_table TO admin_role, writer_role;
GRANT SELECT ON TABLE features_table TO reader_role;
GRANT SELECT ON features TO admin_role, writer_role, reader_role;

CREATE OR REPLACE VIEW features_hierarchy AS 
SELECT
	features_contents_table.short_name AS short_name,
	features_contents_table.version AS content_version,
	features_table.feature_id AS feature_id,
	features_table.feature_content_id AS feature_content_id,
	features_table.parent_feature_id AS parent_feature_id,
	features_table.previous_feature_id AS previous_feature_id,
	features_table.project_id AS project_id,
	features_table.version AS version,
	features_contents_table.status AS status
FROM 
	features_contents_table, features_table 
WHERE
	features_contents_table.feature_content_id = features_table.feature_content_id;
GRANT SELECT ON features_hierarchy TO admin_role, writer_role, reader_role;


-- Règles de gestion
CREATE SEQUENCE rules_contents_rule_content_id_seq;
CREATE TABLE rules_contents_table (
	rule_content_id bigint DEFAULT nextval('rules_contents_rule_content_id_seq'),
	project_id bigint NOT NULL,
	version VARCHAR(12) NOT NULL,
	feature_content_id bigint,
	short_name VARCHAR(128),
	description VARCHAR(16384),
	description_plain_text VARCHAR(16384),
	status CHAR(1) DEFAULT 'C',
	PRIMARY KEY (rule_content_id),
	FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (feature_content_id) REFERENCES features_contents_table(feature_content_id) ON DELETE SET NULL
);
COMMENT ON TABLE rules_contents_table IS 'Contenus des règles de gestion';
COMMENT ON COLUMN rules_contents_table.rule_content_id  IS 'Identifiant unique du contenu de la règle de gestion';
COMMENT ON COLUMN rules_contents_table.project_id IS 'Identifiant du projet associe';
COMMENT ON COLUMN rules_contents_table.version IS 'Numéro de version';
COMMENT ON COLUMN rules_contents_table.feature_content_id IS 'Identifiant du contenu de la fonctionnalité associée';
COMMENT ON COLUMN rules_contents_table.short_name IS 'Nom abrege de la règle de gestion';
COMMENT ON COLUMN rules_contents_table.description IS 'Description de la règle de gestion';
COMMENT ON COLUMN rules_contents_table.description_plain_text IS 'Description de la règle de gestion (texte brut)';
COMMENT ON COLUMN rules_contents_table.status IS 'Status de la règle de gestion';
 
CREATE VIEW rules_contents AS select * from rules_contents_table;

GRANT SELECT, UPDATE ON rules_contents_rule_content_id_seq TO admin_role, writer_role;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE rules_contents_table TO admin_role, writer_role;
GRANT SELECT ON TABLE rules_contents_table TO reader_role;
GRANT SELECT ON rules_contents TO admin_role, writer_role, reader_role;

CREATE OR REPLACE FUNCTION RULES_CONTENTS_TABLE_TRIGGER_FUNCTION()
RETURNS trigger AS $$
BEGIN
	NEW.description_plain_text:=html_to_plain_text(NEW.description);
	return NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER RULES_CONTENTS_TABLE_TRIGGER BEFORE INSERT OR UPDATE
ON rules_contents_table  FOR EACH ROW
EXECUTE PROCEDURE RULES_CONTENTS_TABLE_TRIGGER_FUNCTION();

CREATE SEQUENCE rules_rule_id_seq;

CREATE TABLE rules_table (
  rule_id bigint NOT NULL DEFAULT nextval('rules_rule_id_seq'),
  rule_content_id bigint NOT NULL,
  parent_rule_id bigint,
  previous_rule_id bigint,
  project_id bigint NOT NULL,
  version VARCHAR(12) NOT NULL,
  PRIMARY KEY (rule_id),
  FOREIGN KEY (rule_content_id) REFERENCES rules_contents_table(rule_content_id) ON DELETE CASCADE,
  FOREIGN KEY (parent_rule_id) REFERENCES rules_table(rule_id) ON DELETE CASCADE,
  FOREIGN KEY (previous_rule_id) REFERENCES rules_table(rule_id) ON DELETE SET NULL,
  FOREIGN KEY (project_id,version) REFERENCES projects_versions_table(project_id,version) ON UPDATE CASCADE ON DELETE CASCADE
);

COMMENT ON TABLE rules_table IS 'Règles de gestion';
COMMENT ON COLUMN rules_table.rule_id IS 'Identifiant unique de la règle de gestion';
COMMENT ON COLUMN rules_table.parent_rule_id IS 'Identifiant unique de la règle de gestion parente';
COMMENT ON COLUMN rules_table.previous_rule_id IS 'Identifiant de la règle de gestion precedente';
COMMENT ON COLUMN rules_table.project_id IS 'Identifiant du projet';
COMMENT ON COLUMN rules_table.version IS 'Identifiant de la version du projet';

-- Regles de gestion
CREATE OR REPLACE VIEW rules AS select * from rules_table;

GRANT SELECT, UPDATE ON rules_rule_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE rules_table TO admin_role, writer_role;
GRANT SELECT ON TABLE rules_table TO reader_role;
GRANT SELECT ON rules TO admin_role, writer_role, reader_role;

CREATE OR REPLACE VIEW rules_hierarchy AS 
SELECT
	rules_contents_table.short_name AS short_name,
	rules_contents_table.version AS content_version,
	rules_table.rule_id AS rule_id,
	rules_table.rule_content_id AS rule_content_id,
	rules_table.parent_rule_id AS parent_rule_id,
	rules_table.previous_rule_id AS previous_rule_id,
	rules_table.project_id AS project_id,
	rules_table.version AS version,
	rules_contents_table.status AS status
FROM 
	rules_contents_table, rules_table 
WHERE
	rules_contents_table.rule_content_id = rules_table.rule_content_id;
GRANT SELECT ON rules_hierarchy TO admin_role, writer_role, reader_role;

-- Modification des exigences
DROP VIEW requirements_contents;
ALTER TABLE requirements_contents_table ADD COLUMN feature_content_id bigint;
ALTER TABLE requirements_contents_table ADD FOREIGN KEY (feature_content_id) REFERENCES features_contents_table(feature_content_id) ON DELETE SET NULL;

CREATE OR REPLACE VIEW requirements_contents AS select * from requirements_contents_table;
GRANT SELECT ON requirements_contents TO admin_role, writer_role, reader_role;


-- ********************************************
-- PARAMETRES DE VERSIONS DE PROJETS
-- ********************************************
CREATE SEQUENCE projects_versions_parameters_project_version_parameter_id_seq;

CREATE TABLE projects_versions_parameters_table (
  project_version_parameter_id bigint NOT NULL DEFAULT nextval('projects_versions_parameters_project_version_parameter_id_seq'),
  project_version_id bigint NOT NULL,
  parent_group_parameter_id bigint,
  is_group CHAR(1) NOT NULL DEFAULT 'N',
  parameter_name VARCHAR(256) NOT NULL,
  parameter_value VARCHAR(256),
  PRIMARY KEY (project_version_parameter_id),
  FOREIGN KEY (project_version_id) REFERENCES projects_versions_table(project_version_id) ON DELETE CASCADE,
  FOREIGN KEY (parent_group_parameter_id) REFERENCES projects_versions_parameters_table(project_version_parameter_id) ON DELETE CASCADE
);

COMMENT ON TABLE projects_versions_parameters_table IS 'Paramètres de versions de projets';
COMMENT ON COLUMN projects_versions_parameters_table.project_version_parameter_id IS 'Identifiant unique du paramètre';
COMMENT ON COLUMN projects_versions_parameters_table.project_version_id IS 'Identifiant de la version du projet associé';
COMMENT ON COLUMN projects_versions_parameters_table.parent_group_parameter_id IS 'Identifiant du groupe de paramètre';
COMMENT ON COLUMN projects_versions_parameters_table.is_group IS 'Indicateur de groupe de paramètre';
COMMENT ON COLUMN projects_versions_parameters_table.parameter_name IS 'Nom du paramètre';
COMMENT ON COLUMN projects_versions_parameters_table.parameter_value IS 'Valeur du paramètre';

CREATE VIEW projects_versions_parameters AS select * from projects_versions_parameters_table;

GRANT SELECT, UPDATE ON projects_versions_parameters_project_version_parameter_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE projects_versions_parameters_table TO admin_role, writer_role;
GRANT SELECT ON TABLE projects_versions_parameters_table TO reader_role;
GRANT SELECT ON projects_versions_parameters TO admin_role, writer_role, reader_role;

-- Populate projects_versions_parameters_table with projects_parameters_table
INSERT INTO projects_versions_parameters_table (
		project_version_id,
		parent_group_parameter_id,
		is_group,
		parameter_name,
		parameter_value
	)
	SELECT 
		project_version_id, 
		NULL,
		'N',
		parameter_name,
		parameter_value
	FROM
		projects_versions_table, projects_parameters_table
	WHERE
		projects_versions_table.project_id=projects_parameters_table.project_id;
  
-- Mise à jour projects_grants_table
CREATE SEQUENCE projects_grants_project_grant_id_seq;
ALTER TABLE projects_grants_table ADD COLUMN project_grant_id bigint DEFAULT nextval('projects_grants_project_grant_id_seq');

ALTER TABLE projects_grants_table ADD COLUMN table_signature_id bigint;
ALTER TABLE projects_grants_table ADD COLUMN rights CHAR(1);
ALTER TABLE projects_grants_table DROP CONSTRAINT projects_grants_table_pkey;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 200, 'W' FROM projects_grants_table WHERE table_signature_id IS NULL
AND manage_tests_indic='W' AND manage_requirements_indic='W' AND manage_campaigns_indic='W' AND manage_executions_indic='W';

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 203, manage_tests_indic FROM projects_grants_table WHERE table_signature_id IS NULL;
INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 217, manage_tests_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 206, manage_requirements_indic FROM projects_grants_table WHERE table_signature_id IS NULL;
INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 218, manage_tests_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 209, manage_campaigns_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 211, manage_executions_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 232, manage_executions_indic FROM projects_grants_table WHERE table_signature_id IS NULL;
INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 240, manage_executions_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 230, manage_executions_indic FROM projects_grants_table WHERE table_signature_id IS NULL;
INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 241, manage_executions_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

INSERT INTO projects_grants_table (project_id, username, table_signature_id, rights)
SELECT project_id, username, 238, manage_executions_indic FROM projects_grants_table WHERE table_signature_id IS NULL;

DELETE FROM projects_grants_table WHERE table_signature_id IS NULL;

ALTER TABLE projects_grants_table ALTER COLUMN table_signature_id SET NOT NULL;
ALTER TABLE projects_grants_table ALTER COLUMN rights SET NOT NULL;

ALTER TABLE projects_grants_table ADD PRIMARY KEY (project_grant_id);
ALTER TABLE projects_grants_table ADD UNIQUE (project_id, username, table_signature_id);

DROP VIEW projects_grants;
ALTER TABLE projects_grants_table DROP COLUMN manage_tests_indic;
ALTER TABLE projects_grants_table DROP COLUMN manage_requirements_indic;
ALTER TABLE projects_grants_table DROP COLUMN manage_campaigns_indic;
ALTER TABLE projects_grants_table DROP COLUMN manage_executions_indic;
CREATE VIEW projects_grants AS select * from projects_grants_table;
GRANT SELECT ON projects_grants TO admin_role, writer_role, reader_role;
GRANT SELECT, UPDATE ON projects_grants_project_grant_id_seq TO admin_role, writer_role;

COMMENT ON COLUMN projects_grants_table.project_grant_id IS 'Identifiant';
COMMENT ON COLUMN projects_grants_table.table_signature_id IS 'Signature de l''entitté consernée';
COMMENT ON COLUMN projects_grants_table.rights IS 'Droit';

-- Mise à jour de la version logique de la base de données
UPDATE database_version_table set database_version_number='01.06.00.00', upgrade_date=now();
