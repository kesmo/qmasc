-- EXECUTION DE CAMPAGNE
DROP VIEW executions_campaigns;
ALTER TABLE executions_campaigns_table ADD COLUMN revision VARCHAR(24);
COMMENT ON COLUMN executions_campaigns_table.revision IS 'Revision';
CREATE OR REPLACE VIEW executions_campaigns AS select * from executions_campaigns_table;
GRANT SELECT ON executions_campaigns TO admin_role, writer_role, reader_role;

-- PROJETS
DROP VIEW projects;
ALTER TABLE projects_table ALTER COLUMN short_name TYPE VARCHAR(128);
CREATE OR REPLACE VIEW projects AS select * from projects_table;
GRANT SELECT ON projects TO admin_role, writer_role, reader_role;

-- CONTENUS DE TESTS
DROP VIEW tests_contents;
DROP VIEW TESTS_HIERARCHY;
ALTER TABLE tests_contents_table ALTER COLUMN short_name TYPE VARCHAR(128);
CREATE OR REPLACE VIEW tests_contents AS SELECT * FROM tests_contents_table;
GRANT SELECT ON tests_contents TO admin_role, writer_role, reader_role;

CREATE OR REPLACE VIEW TESTS_HIERARCHY AS
SELECT
	tests_contents_table.short_name AS short_name,
	tests_contents_table.category_id AS category_id,
	tests_contents_table.priority_level AS priority_level,
	tests_contents_table.version AS content_version,
	tests_table.test_id AS test_id,
	tests_table.test_content_id AS test_content_id,
	tests_table.original_test_id AS original_test_id,
	tests_table.parent_test_id AS parent_test_id,
	tests_table.previous_test_id AS previous_test_id,
	tests_table.project_id AS project_id,
	tests_table.version AS version,
	tests_contents_table.status AS status,
	tests_contents_table.original_test_content_id AS original_test_content_id
FROM 
	tests_contents_table, tests_table
WHERE
	tests_contents_table.test_content_id = tests_table.test_content_id;
GRANT SELECT ON TESTS_HIERARCHY TO admin_role, writer_role, reader_role;


-- ACTIONS
DROP VIEW actions;
ALTER TABLE actions_table ALTER COLUMN short_name TYPE VARCHAR(128);
CREATE OR REPLACE VIEW actions AS select * from actions_table;
GRANT SELECT ON actions TO admin_role, writer_role, reader_role;

-- CONTENUS D'EXIGENCES
DROP VIEW requirements_contents;
DROP VIEW REQUIREMENTS_HIERARCHY;
ALTER TABLE requirements_contents_table ALTER COLUMN short_name TYPE VARCHAR(128);
CREATE OR REPLACE VIEW requirements_contents AS select * from requirements_contents_table;
GRANT SELECT ON requirements_contents TO admin_role, writer_role, reader_role;

CREATE OR REPLACE VIEW REQUIREMENTS_HIERARCHY AS 
SELECT
	requirements_contents_table.short_name AS short_name,
	requirements_contents_table.category_id AS category_id,
	requirements_contents_table.priority_level AS priority_level,
	requirements_contents_table.version AS content_version,
	requirements_table.requirement_id AS requirement_id,
	requirements_table.requirement_content_id AS requirement_content_id,
	requirements_table.parent_requirement_id AS parent_requirement_id,
	requirements_table.previous_requirement_id AS previous_requirement_id,
	requirements_table.project_id AS project_id,
	requirements_table.version AS version,
	requirements_contents_table.status AS status,
	requirements_contents_table.original_requirement_content_id AS original_requirement_content_id 
FROM 
	requirements_contents_table, requirements_table 
WHERE
	requirements_contents_table.requirement_content_id = requirements_table.requirement_content_id;
GRANT SELECT ON REQUIREMENTS_HIERARCHY TO admin_role, writer_role, reader_role;

-- CAMPAGNES
DROP VIEW campaigns;
ALTER TABLE campaigns_table ALTER COLUMN short_name TYPE VARCHAR(128);
CREATE OR REPLACE VIEW campaigns AS select * from campaigns_table;
GRANT SELECT ON campaigns TO admin_role, writer_role, reader_role;

-- ANOMALIES
DROP VIEW bugs;
ALTER TABLE bugs_table ALTER COLUMN short_name TYPE VARCHAR(128);
ALTER TABLE bugs_table ADD COLUMN reproducibility varchar(64);
COMMENT ON COLUMN bugs_table.reproducibility IS 'Reproductibilité de l''anomalie';
CREATE OR REPLACE VIEW bugs AS select * from bugs_table;
GRANT SELECT ON bugs TO admin_role, writer_role, reader_role;


-- ********************************************
-- PARAMETRES D'EXECUTIONS DE TESTS
-- ********************************************
CREATE SEQUENCE executions_tests_parameters_parameter_id_seq;

CREATE TABLE executions_tests_parameters_table (
  execution_test_parameter_id bigint NOT NULL DEFAULT nextval('executions_tests_parameters_parameter_id_seq'),
  execution_test_id bigint NOT NULL,
  parameter_name VARCHAR(256) NOT NULL,
  parameter_value VARCHAR(256),
  PRIMARY KEY (execution_test_parameter_id),
  FOREIGN KEY (execution_test_id) REFERENCES executions_tests_table(execution_test_id) ON DELETE CASCADE
);

COMMENT ON TABLE executions_tests_parameters_table IS 'Paramètres d''exécutions de tests';
COMMENT ON COLUMN executions_tests_parameters_table.execution_test_parameter_id IS 'Identifiant unique du paramètre';
COMMENT ON COLUMN executions_tests_parameters_table.execution_test_id IS 'Identifiant de l''exécution de test associée';
COMMENT ON COLUMN executions_tests_parameters_table.parameter_name IS 'Nom du paramètre';
COMMENT ON COLUMN executions_tests_parameters_table.parameter_value IS 'Valeur du paramètre';

CREATE VIEW executions_tests_parameters AS select * from executions_tests_parameters_table;

GRANT SELECT, UPDATE ON executions_tests_parameters_parameter_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE executions_tests_parameters_table TO admin_role, writer_role;
GRANT SELECT ON TABLE executions_tests_parameters_table TO reader_role;
GRANT SELECT ON executions_tests_parameters TO admin_role, writer_role, reader_role;


