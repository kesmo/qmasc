/*****************************************************************************
Copyright (C) 2010 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R (Requirements and Tests Management Repository).

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#if defined(_WINDOWS) || defined(WIN32)
#include <winsock2.h>
#include <winldap.h>
#else
#include <ldap.h>
#endif

#include "rtmrlibrary.h"

// Directives de base de donnees
#ifdef _MYSQL
	#include "../common/mysql/EJMysqlLibrary.h"
#else
	#ifdef _POSTGRES
		#include "../common/postgres/rtmrpostgreslibrary.h"
	#else

	#endif
#endif

#include "../common/ldap/EJLdapLibrary.h"

#include "../common/utilities.h"
#include "../common/entities.h"
#include "../common/constants.h"
#include "../common/errors.h"

#if defined (__APPLE__)
	#include <malloc/malloc.h>
#else
		#include <malloc.h>
#endif

#include <string.h>
#include <stdio.h>

static net_session *m_session = NULL;

DLLEXPORT int DLLCALL EJLib_init( const char* in_hostname, int in_port, const char* in_dbname, const char* in_username, const char* in_passwd, const char* in_log_trace_file_name, const char* in_log_error_file_name )
{
	if (m_session != NULL)
		EJLib_die();

	m_session = (net_session*)malloc(sizeof(net_session));
	memset(m_session, 0, sizeof(net_session));

	if (is_empty_string(in_log_trace_file_name) == FALSE)
		m_session->m_log_trace_file = fopen(in_log_trace_file_name, "a");

	if (is_empty_string(in_log_error_file_name) == FALSE)
	{
		if (is_empty_string(in_log_trace_file_name) == FALSE && strcmp(in_log_trace_file_name, in_log_error_file_name) == 0)
			m_session->m_log_error_file = m_session->m_log_trace_file;
		else
			m_session->m_log_error_file = fopen(in_log_error_file_name, "a");
	}
	else
		m_session->m_log_error_file = m_session->m_log_trace_file;

	// Verifier les paramètres
	if (in_dbname == NULL)
	{
		LOG_ERROR(m_session, "init : dbname is null\n");
		return EMPTY_OBJECT;
	}

	if (in_username == NULL)
	{
		LOG_ERROR(m_session, "init : username is null\n");
		return EMPTY_OBJECT;
	}

	if (in_passwd == NULL)
	{
		LOG_ERROR(m_session, "init : passwd is null\n");
		return EMPTY_OBJECT;
	}


	return db_start(m_session, in_hostname, in_port, in_dbname, in_username, in_passwd, NULL);
}

DLLEXPORT int DLLCALL EJLib_die()
{
	if (m_session == NULL) return NOERR;

	db_die(m_session);

	if (m_session->m_log_error_file && m_session->m_log_trace_file && m_session->m_log_trace_file == m_session->m_log_error_file)
	{
		fclose(m_session->m_log_error_file);
	}
	else
	{
		if (m_session->m_log_error_file)
			fclose(m_session->m_log_error_file);

		if (m_session->m_log_trace_file)
			fclose(m_session->m_log_trace_file);
	}

	free(m_session);
	m_session = NULL;

	return NOERR;
}

DLLEXPORT char*** DLLCALL EJLib_runSql( const char* in_statement, unsigned long *out_rows_count, unsigned long *out_columns_count )
{
	char ***tmp_returned_rows = NULL;

	if (in_statement != NULL)
	{
		if (m_session && m_session->m_db_connexion && is_empty_string(in_statement) == FALSE)
		{
			tmp_returned_rows = db_do_query(m_session, in_statement, out_rows_count, out_columns_count);
		}
		else
			LOG_ERROR(m_session, "runSql : mysql db is NULL\n");
	}
	else
		LOG_ERROR(m_session, "runSql : statement is empty\n");

	return tmp_returned_rows;
}


#ifdef _LDAP

DLLEXPORT char*** DLLCALL EJLib_loadLdapEntries( int in_entry_signature, const char* in_ldap_url, const char* in_distinguish_name, const char* in_credencial, const char* in_base_query, const char* in_filter_query )
{
	unsigned long	tmp_rows_count = 0;
	unsigned long	tmp_columns_count = 0;

	char			***tmp_returned_rows = NULL;
	char			**tmp_attributes_names = NULL;

	int				tmp_ldap_return_code = 0;

	/* Verifier les parametres */
	if (in_ldap_url == NULL || in_distinguish_name == NULL || in_credencial == NULL)
	{
		LOG_ERROR(m_session, "loadLdapEntries : ldap url, distinguish name or credencial is null\n");
		return NULL;
	}

	/* Chercher le definition de l'entites */
	if (get_ldap_entry_def(in_entry_signature, &tmp_attributes_names) < 0)
	{
		LOG_ERROR(m_session, "loadLdapEntries : ldap entry signature unknow\n");
		return NULL;
	}

	if (is_empty_string(in_distinguish_name) == TRUE || is_empty_string(in_credencial) == TRUE)
	{
		LOG_ERROR(m_session, "loadLdapEntries : distinguish name or credencial is empty\n");
		return NULL;
	}

	/* Executer la requête LDAP */
	LOG_DB_TRACE(m_session, "loadLdapEntries : calling ldap_query...\n");

	tmp_returned_rows = ldap_query(in_ldap_url, in_distinguish_name, in_credencial, in_base_query, in_filter_query, tmp_attributes_names, &tmp_rows_count, &tmp_columns_count, &tmp_ldap_return_code);

	LOG_DB_TRACE(m_session, "loadLdapEntries : end ldap_query\n");

	return tmp_returned_rows;
}


DLLEXPORT int DLLCALL EJLib_loadLdapEntry( int in_entry_signature, const char* in_ldap_url, const char* in_distinguish_name, const char* in_credencial, char** in_out_columns_record, const char* in_base_query )
{
	unsigned long	tmp_columns_index = 0;
	unsigned long	tmp_rows_count = 0;
	unsigned long	tmp_columns_count = 0;

	char						***tmp_returned_rows = NULL;
	char						**tmp_attributes_names = NULL;

	int							tmp_ldap_return_code = 0;

	/* Verifier les parametres */
	if (in_ldap_url == NULL || in_distinguish_name == NULL || in_credencial == NULL)
	{
		LOG_ERROR(m_session, "loadLdapEntry : ldap url, distinguish name or credencial is null\n");
		return EMPTY_OBJECT;
	}

	/* Chercher le definition de l'entites */
	if (get_ldap_entry_def(in_entry_signature, &tmp_attributes_names) < 0)
	{
		LOG_ERROR(m_session, "loadLdapEntry : ldap entry signature unknow\n");
		return UNKNOW_LDAP_TYPE_ENTRY;
	}

	if (is_empty_string(in_distinguish_name) == TRUE || is_empty_string(in_credencial) == TRUE)
	{
		LOG_ERROR(m_session, "loadLdapEntry : distinguish name or credencial is empty\n");
		return EMPTY_OBJECT;
	}

	/* Executer la requête LDAP */
	LOG_DB_TRACE(m_session, "loadLdapEntry : calling ldap_query...\n");
	tmp_returned_rows = ldap_query(in_ldap_url, in_distinguish_name, in_credencial, in_base_query, NULL, tmp_attributes_names, &tmp_rows_count, &tmp_columns_count, &tmp_ldap_return_code);
	if (tmp_returned_rows)
	{
		/* Il ne doit y avoir qu'un seul enregistrement correspondant �  la cle primaire */
		if (tmp_rows_count == 1)
		{
			/* Alimenter le tableau des colonnes */
			for(tmp_columns_index = 0; tmp_columns_index < tmp_columns_count; tmp_columns_index++)
			{
				if (tmp_returned_rows[0][tmp_columns_index])
				{
					strcpy(in_out_columns_record[tmp_columns_index], tmp_returned_rows[0][tmp_columns_index]);
				}
			}
			free(tmp_returned_rows[0]);
			free(tmp_returned_rows);

			return NOERR;
		}
		/* Plusieurs enregistrements ont ete trouves : liberer la memoire et renvoyer une erreur */
		else if (tmp_rows_count> 1)
		{
			LOG_ERROR(m_session, "loadLdapEntry : trop de lignes renvoyees : %li\n", tmp_rows_count);
			free_rows_and_columns(&tmp_returned_rows, tmp_rows_count, tmp_columns_count);

			return LDAP_TOO_MUCH_ENTRIES_FOUND;
		}
		/* Aucun enregistrement a ete trouve */
		else
		{
			free_rows_and_columns(&tmp_returned_rows, tmp_rows_count, tmp_columns_count);
			if (tmp_ldap_return_code != NOERR)
				return tmp_ldap_return_code;
		}
	}

	return LDAP_NO_ENTRY_FOUND;
}

#endif

DLLEXPORT int DLLCALL EJLib_callSqlProcedure( const char* in_procedure_name, char** in_out_parameters )
{
	int							tmp_param_count = sizeof(in_out_parameters) / sizeof(char*);

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	/* Verifier les parametres */
	if (in_procedure_name == NULL)
	{
		LOG_ERROR(m_session, "callSqlProcedure : procedure name is null\n");
		return EMPTY_OBJECT;
	}

	if (is_empty_string(in_procedure_name) == TRUE)
	{
		LOG_ERROR(m_session, "callSqlProcedure : procedure name is empty\n");
		return EMPTY_OBJECT;
	}

	// Execution de la procedure
	db_call_procedure(m_session, in_procedure_name, tmp_param_count, in_out_parameters);

	return NOERR;
}

DLLEXPORT int DLLCALL EJLib_loadRecord( int in_table_signature,char **in_out_columns_record, const char *in_primary_key_value )
{
	char *tmp_statement_ptr = m_session->m_last_query;

	unsigned long tmp_columns_index = 0;
	unsigned long tmp_rows_count = 0;
	unsigned long tmp_columns_count = 0;

	char ***tmp_returned_rows = NULL;
	const char **tmp_keys_columns = NULL;

	entity_def *tmp_entity_def = NULL;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	/* Verifier les parametres */
	if (in_primary_key_value == NULL)
	{
		LOG_ERROR(m_session, "loadRecord : primary key value is null\n");
		return EMPTY_OBJECT;
	}

	/* Chercher le definition de l'entites */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	if (is_empty_string(in_primary_key_value) == TRUE)
	{
		LOG_ERROR(m_session, "loadRecord : primary key value is empty\n");
		return EMPTY_OBJECT;
	}

	tmp_keys_columns = tmp_entity_def->m_entity_columns_names;
	if (tmp_keys_columns == NULL || is_empty_string(*tmp_keys_columns) == TRUE)
	{
		LOG_ERROR(m_session, "loadRecord : primary key name is unknow\n");
		return EMPTY_OBJECT;
	}

	/* Generer la requête */
	tmp_statement_ptr += sprintf(tmp_statement_ptr, "select %s", *tmp_keys_columns);
	while ((*(++tmp_keys_columns)) != NULL)
		tmp_statement_ptr += sprintf(tmp_statement_ptr, ",%s", *tmp_keys_columns);

	tmp_statement_ptr += sprintf(tmp_statement_ptr, " from %s where %s=%s;", tmp_entity_def->m_entity_name, *tmp_entity_def->m_entity_columns_names, in_primary_key_value);

	/* Executer la requête */
	tmp_returned_rows = db_do_query(m_session, m_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
	if (tmp_returned_rows)
	{
		/* Il ne doit y avoir qu'un seul enregistrement correspondant �  la cle primaire */
		if (tmp_rows_count == 1)
		{
			/* Alimenter le tableau des colonnes */
			for(tmp_columns_index = 0; tmp_columns_index < tmp_columns_count; tmp_columns_index++)
			{
				if (tmp_returned_rows[0][tmp_columns_index])
				{
					in_out_columns_record[tmp_columns_index] = tmp_returned_rows[0][tmp_columns_index];
				}
			}
			free(tmp_returned_rows[0]);
			free(tmp_returned_rows);

			return NOERR;
		}
		/* Plusieurs enregistrements ont ete trouves : liberer la memoire et renvoyer une erreur */
		else if (tmp_rows_count> 1)
		{
			LOG_ERROR(m_session, "loadRecord : trop de lignes renvoyees : %li\n", tmp_rows_count);
			free_rows_and_columns(&tmp_returned_rows, tmp_rows_count, tmp_columns_count);

			return DB_SQL_TOO_MUCH_ROWS_FOUND;
		}
		/* Aucun enregistrement a ete trouve */
		else
		{
			free_rows_and_columns(&tmp_returned_rows, tmp_rows_count, tmp_columns_count);
			return DB_SQL_NO_ROW_FOUND;
		}
	}

	return DB_SQL_NO_ROW_FOUND;
}

DLLEXPORT char*** DLLCALL EJLib_loadRecords( int in_table_signature, const char* in_where_clause, const char* in_order_by_clause, unsigned long *in_out_records_count )
{
	entity_def					*tmp_entity_def = NULL;

	char *tmp_statement_ptr = m_session->m_last_query;

	unsigned long tmp_rows_count = 0;
	unsigned long tmp_columns_count = 0;

	char ***tmp_returned_rows = NULL;
	const char **tmp_current_key = NULL;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return NULL;

	/* Chercher le definition de l'entites */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return NULL;

	tmp_current_key = tmp_entity_def->m_entity_columns_names;
	if (tmp_current_key == NULL || is_empty_string(*tmp_current_key) == TRUE)
	{
		m_session->m_last_error = EMPTY_OBJECT;
		return NULL;
	}

	/* Generer la requête */
	tmp_statement_ptr += sprintf(tmp_statement_ptr, "select %s", *tmp_current_key);
	while ( *(++tmp_current_key) != NULL)
		tmp_statement_ptr += sprintf(tmp_statement_ptr, ",%s", *tmp_current_key);

	tmp_statement_ptr += sprintf(tmp_statement_ptr, " from %s", tmp_entity_def->m_entity_name);

	if (is_empty_string(in_where_clause) == FALSE)
		tmp_statement_ptr += sprintf(tmp_statement_ptr, " where %s", in_where_clause);

	if (is_empty_string(in_order_by_clause) == FALSE)
		tmp_statement_ptr += sprintf(tmp_statement_ptr, " order by %s", in_order_by_clause);

	tmp_statement_ptr += sprintf(tmp_statement_ptr, ";");

	/* Executer la requête */
	tmp_returned_rows = db_do_query(m_session, m_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
	if (in_out_records_count != NULL)
		*in_out_records_count = tmp_rows_count;

	return tmp_returned_rows;
}

DLLEXPORT int DLLCALL EJLib_insertRecord( int in_table_signature, char** in_out_columns_record )
{
	entity_def					*tmp_entity_def = NULL;
	const char 				**tmp_current_key = NULL;
	const char 				**tmp_current_column_format = NULL;

	char 							*tmp_statement_ptr = m_session->m_last_query;

	char 							tmp_columns_names_str[1024];
	char 							*tmp_columns_names_ptr = tmp_columns_names_str;

	char 							tmp_columns_values_str[MAX_SQL_STATEMENT_LENGTH];
	char 							*tmp_columns_values_ptr = tmp_columns_values_str;

	int								tmp_index = 0;
	int								tmp_return_value = 0;

	unsigned long		tmp_rows_count = 0;
	unsigned long		tmp_pk_value = 0;

	char							tmp_pk_value_str[64];

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	/* Verifier les parametres */
	if (in_out_columns_record == NULL) return EMPTY_OBJECT;

	/* Chercher le definition de l'entites */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_current_column_format = tmp_entity_def->m_entity_columns_formats;
	tmp_current_key = tmp_entity_def->m_entity_columns_names;
	if (tmp_current_key == NULL || is_empty_string(*tmp_current_key) == TRUE)
	{
		m_session->m_last_error = EMPTY_OBJECT;
		return EMPTY_OBJECT;
	}

	/* Generer la requête */
	if (in_out_columns_record[0] != NULL)
	{
		tmp_columns_names_ptr += sprintf(tmp_columns_names_ptr, "%s", *tmp_current_key);
		if (tmp_current_column_format && *tmp_current_column_format)
			tmp_columns_values_ptr = print_formated_column(tmp_columns_values_ptr, in_out_columns_record[0], (*tmp_current_column_format));
		else
			tmp_columns_values_ptr = print_formated_column(tmp_columns_values_ptr, in_out_columns_record[0], NUMBER_FORMAT);
	}
	else if (*(++tmp_current_key) != NULL)
	{
		tmp_index++;
		tmp_columns_names_ptr += sprintf(tmp_columns_names_ptr, "%s", *tmp_current_key);
		if (tmp_current_column_format) tmp_current_column_format++;
		if (in_out_columns_record[tmp_index] != NULL)
		{
			if (tmp_current_column_format && *tmp_current_column_format)
				tmp_columns_values_ptr = print_formated_column(tmp_columns_values_ptr, in_out_columns_record[tmp_index], (*tmp_current_column_format));
			else
				tmp_columns_values_ptr = print_formated_column(tmp_columns_values_ptr, in_out_columns_record[tmp_index], STANDARD_FORMAT);
		}
		else
			tmp_columns_values_ptr += sprintf(tmp_columns_values_ptr, "null");
	}

	while (*(++tmp_current_key) != NULL)
	{
		tmp_index++;
		tmp_columns_names_ptr += sprintf(tmp_columns_names_ptr, ",%s", *tmp_current_key);
		if (tmp_current_column_format) tmp_current_column_format++;

		if (in_out_columns_record[tmp_index] != NULL)
		{
			tmp_columns_values_ptr += sprintf(tmp_columns_values_ptr, ",");
			if (tmp_current_column_format && *tmp_current_column_format)
				tmp_columns_values_ptr = print_formated_column(tmp_columns_values_ptr, in_out_columns_record[tmp_index], (*tmp_current_column_format));
			else
				tmp_columns_values_ptr = print_formated_column(tmp_columns_values_ptr, in_out_columns_record[tmp_index], STANDARD_FORMAT);
		}
		else
			tmp_columns_values_ptr += sprintf(tmp_columns_values_ptr, ",null");
	}
	tmp_statement_ptr += sprintf(tmp_statement_ptr, "insert into %s (%s) values (%s);", tmp_entity_def->m_entity_name, tmp_columns_names_str, tmp_columns_values_str);

	tmp_return_value = db_insert_rows(m_session, tmp_entity_def, m_session->m_last_query, &tmp_rows_count, &tmp_pk_value);
	if (tmp_return_value == NOERR)
	{
		if (in_out_columns_record[0] == NULL && tmp_pk_value > 0)
		{
			memset(tmp_pk_value_str, 0, sizeof(tmp_pk_value_str));
			sprintf(tmp_pk_value_str, "%li", tmp_pk_value);
			in_out_columns_record[0] = (char*)malloc(strlen(tmp_pk_value_str) + 1);
			strcpy(in_out_columns_record[0], tmp_pk_value_str);
		}
	}

	return tmp_return_value;
}


DLLEXPORT int DLLCALL EJLib_saveRecord( int in_table_signature, char** in_out_columns_record, char** in_originals_columns_record )
{
	entity_def					*tmp_entity_def = NULL;
	const char 				**tmp_current_key = NULL;
	const char 				**tmp_current_column_format = NULL;

	char 							tmp_where_clause[256];
	char 							*tmp_where_clause_ptr = tmp_where_clause;

	const char 				**tmp_current_pk_name = NULL;
	char 							*tmp_current_pk_value = NULL;

	char 							*tmp_statement_ptr = m_session->m_last_query;

	char							*tmp_column_value = NULL;

	char							*tmp_original_column_value = NULL;

	int								tmp_same_column = FALSE;

	int								tmp_column_index = 0;
	int								tmp_changed_columns_count = 0;

	unsigned long		tmp_rows_count = 0;

	int								tmp_return = NOERR;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	LOG_TRACE(m_session, "saveRecord\n");

	/* Verifier les parametres */
	if (in_out_columns_record == NULL) return EMPTY_OBJECT;

	/* Chercher le definition de l'entites */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_current_key = tmp_entity_def->m_entity_columns_names;
	tmp_current_column_format = tmp_entity_def->m_entity_columns_formats;

	if (tmp_entity_def->m_primary_key != NULL)
	{
		tmp_current_pk_name = tmp_entity_def->m_primary_key;
		tmp_current_pk_value = value_for_key(tmp_entity_def, (in_originals_columns_record != NULL ? in_originals_columns_record : in_out_columns_record), *tmp_current_pk_name);
		if (is_empty_string(tmp_current_pk_value) == FALSE)
		{
			tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, "%s='%s'", *tmp_current_pk_name, tmp_current_pk_value);
		}
		tmp_current_pk_name++;

		while (tmp_current_pk_name && *tmp_current_pk_name)
		{
			tmp_current_pk_value = value_for_key(tmp_entity_def, (in_originals_columns_record != NULL ? in_originals_columns_record : in_out_columns_record), *tmp_current_pk_name);
			if (is_empty_string(tmp_current_pk_value) == FALSE)
			{
				tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, " and %s='%s'", *tmp_current_pk_name, tmp_current_pk_value);
			}
			tmp_current_pk_name++;
		}
	}
	else if (in_originals_columns_record)
		tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, "%s=%s", *tmp_current_key, in_originals_columns_record[0]);
	else
		tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, "%s=%s", *tmp_current_key, in_out_columns_record[0]);

	/* Generer la requête */
	if (is_empty_string(tmp_where_clause) == FALSE)
	{
		tmp_statement_ptr += sprintf(tmp_statement_ptr, "update %s set ", tmp_entity_def->m_entity_name);

		do
		{
			tmp_current_key++;
			tmp_column_index++;

			if (tmp_current_key != NULL && (*tmp_current_key) != NULL)
			{
				if (tmp_current_column_format) tmp_current_column_format++;

				tmp_same_column = FALSE;

				// Recuperer la valeur de la colonne courante
				tmp_column_value = in_out_columns_record[tmp_column_index];

				// Comparer la valeur de la colonne courante avec son ancienne valeur
				if (in_originals_columns_record != NULL)
				{
					tmp_original_column_value = in_originals_columns_record[tmp_column_index];
					tmp_same_column = (compare_values(tmp_original_column_value, tmp_column_value) == 0);
				}

				// La valeur de la colonne courante a ete modifiee, on prepare l'instruction sql update en consequent
				if (tmp_same_column == FALSE)
				{
					// Incrementer le nombre de colonnes modifiees
					tmp_changed_columns_count++;

					if (tmp_column_index > 1 && tmp_changed_columns_count > 1)
						tmp_statement_ptr += sprintf(tmp_statement_ptr, ",");

					if (in_out_columns_record[tmp_column_index] != NULL)
					{
						tmp_statement_ptr += sprintf(tmp_statement_ptr, "%s=", *tmp_current_key);
						if (tmp_current_column_format && *tmp_current_column_format)
							tmp_statement_ptr = print_formated_column(tmp_statement_ptr, tmp_column_value, (*tmp_current_column_format));
						else
							tmp_statement_ptr = print_formated_column(tmp_statement_ptr, tmp_column_value, STANDARD_FORMAT);
					}
					else
						tmp_statement_ptr += sprintf(tmp_statement_ptr, "%s=null", *tmp_current_key);
				}
			}
		}
		while (tmp_current_key != NULL && (*tmp_current_key) != NULL);

		tmp_statement_ptr += sprintf(tmp_statement_ptr, " where %s;", tmp_where_clause);

		if (tmp_changed_columns_count > 0)
		{
			tmp_return = db_update_rows(m_session, m_session->m_last_query, &tmp_rows_count);
			if (tmp_return == NOERR && tmp_rows_count <= 0)
				tmp_return = DB_SQL_NO_ROW_FOUND;
		}

		return tmp_return;
	}

	return EMPTY_OBJECT;
}

DLLEXPORT int DLLCALL EJLib_deleteRecord( int in_table_signature, char** in_columns_record )
{
	entity_def			*tmp_entity_def = NULL;

	char 							tmp_where_clause[256];
	char 							*tmp_where_clause_ptr = tmp_where_clause;

	const char 				**tmp_current_pk_name = NULL;
	char 							*tmp_current_pk_value = NULL;

	unsigned long		tmp_rows_count = 0;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	if (tmp_entity_def->m_primary_key != NULL)
	{
		tmp_current_pk_name = tmp_entity_def->m_primary_key;
		tmp_current_pk_value = value_for_key(tmp_entity_def, in_columns_record, *tmp_current_pk_name);
		if (is_empty_string(tmp_current_pk_value) == FALSE)
		{
			tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, "%s='%s'", *tmp_current_pk_name, tmp_current_pk_value);
		}
		tmp_current_pk_name++;

		while (tmp_current_pk_name && *tmp_current_pk_name)
		{
			tmp_current_pk_value = value_for_key(tmp_entity_def, in_columns_record, *tmp_current_pk_name);
			if (is_empty_string(tmp_current_pk_value) == FALSE)
			{
				tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, " and %s='%s'", *tmp_current_pk_name, tmp_current_pk_value);
			}
			tmp_current_pk_name++;
		}
	}
	else
		tmp_where_clause_ptr += sprintf(tmp_where_clause_ptr, "%s=%s", *(tmp_entity_def->m_entity_columns_names), in_columns_record[0]);

	/* Generer la requête */
	sprintf(m_session->m_last_query, "delete from %s where %s;", tmp_entity_def->m_entity_name, tmp_where_clause);

	/* Executer la requête */
	return db_delete_rows(m_session, m_session->m_last_query, &tmp_rows_count);
}

/***********************************************************************************	*
*	Java_org_ej_EJNativeLibrary_deleteRecords																				*
*----------------------------------------------------------------------------------------------------------------------------	*
*	Supprimer plusieurs enregistrements d'une même table de la base de donnees					. 	*
*----------------------------------------------------------------------------------------------------------------------------	*
*					Arguments	|		in_java_env																									*
*											|		in_class																										*
*											|		in_table_signature																					*
*											|		in_where_clause																						*
*----------------------------------------------------------------------------------------------------------------------------	*
*							Retour	|																															*
* ************************************************************************************
*	Date			|	Auteur	|	Version	|	Description																				*
*----------------------------------------------------------------------------------------------------------------------------	*
*	20090209	|	EJO			|	1.0.0		|	Creation																					*
*************************************************************************************/
DLLEXPORT int DLLCALL EJLib_deleteRecords( int in_table_signature, const char* in_where_clause )
{
	entity_def			*tmp_entity_def = NULL;

	unsigned long		tmp_rows_count = 0;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	/* Verifier les parametres */
	if (in_where_clause == NULL)
	{
		LOG_ERROR(m_session, "deleteRecords : where clause is null\n");
		return EMPTY_OBJECT;
	}

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	if (is_empty_string(in_where_clause) == TRUE)
	{
		LOG_ERROR(m_session, "deleteRecords : where clause is empty\n");
		return EMPTY_OBJECT;
	}

	/* Generer la requête */
	sprintf(m_session->m_last_query, "delete from %s where %s;", tmp_entity_def->m_entity_name, in_where_clause);

	/* Executer la requête */
	return db_delete_rows(m_session, m_session->m_last_query, &tmp_rows_count);
}



DLLEXPORT char* DLLCALL EJLib_getErrorMessage( int in_error_id )
{
	char	*tmp_error_msg = NULL;
	char	*tmp_full_error_msg = NULL;

	switch (in_error_id)
	{
		case DB_SQL_ERROR:
			if (m_session == NULL) return NULL;

			tmp_error_msg = (char*)get_error_message(in_error_id);
			tmp_full_error_msg = (char*) malloc(strlen(tmp_error_msg) + strlen(m_session->m_last_error_msg) + 2);
			sprintf(tmp_full_error_msg, "%s\n%s", tmp_error_msg, m_session->m_last_error_msg);
			free(tmp_error_msg);
			break;

		default:
			tmp_full_error_msg = (char*)get_error_message(in_error_id);
	}

	return tmp_full_error_msg;
}


DLLEXPORT char* DLLCALL EJLib_getLastErrorMessageExt( )
{
	if (m_session == NULL) return NULL;

	return m_session->m_last_error_msg;
}


DLLEXPORT int DLLCALL EJLib_addUser(const char *in_username, const char *in_password, const char *in_rolename)
{
	return db_add_user(m_session, in_username, in_password, in_rolename);
}


DLLEXPORT int DLLCALL EJLib_removeUser(const char *in_username)
{
	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	if (is_empty_string(in_username))	return EMPTY_OBJECT;

	sprintf(m_session->m_last_query, "drop user %s;", in_username);

	return db_exec_cmd(m_session, m_session->m_last_query);
}


DLLEXPORT int DLLCALL EJLib_modifyUserPassword(const char *in_username, const char *in_new_password)
{
	return db_modify_user_password(m_session, in_username, in_new_password);
}


DLLEXPORT int DLLCALL EJLib_addRoleToUser(const char *in_rolename, const char *in_username)
{
	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	if (is_empty_string(in_username) || is_empty_string(in_rolename))	return EMPTY_OBJECT;

	sprintf(m_session->m_last_query, "grant %s to %s;", in_rolename, in_username);

	return db_exec_cmd(m_session, m_session->m_last_query);
}


DLLEXPORT int DLLCALL EJLib_removeRoleFromUser(const char *in_rolename, const char *in_username)
{
	if (is_empty_string(in_username) || is_empty_string(in_rolename))	return EMPTY_OBJECT;

	sprintf(m_session->m_last_query, "revoke %s from %s;", in_rolename, in_username);

	return db_exec_cmd(m_session, m_session->m_last_query);
}


DLLEXPORT int DLLCALL EJLib_hasUserThisRole(const char *in_username, const char *in_rolename)
{
	char							***tmp_returned_rows = NULL;
	unsigned long		tmp_rows_count = 0;
	unsigned long		tmp_columns_count = 0;
	int								tmp_return_value = FALSE;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return DB_CONNEXION_ERROR;

	sprintf(m_session->m_last_query, "select is_grantable from information_schema.applicable_roles where grantee='%s' and role_name='%s';", in_username, in_rolename);
	tmp_returned_rows = db_do_query(m_session, m_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
	if (tmp_returned_rows && tmp_rows_count == 1 && tmp_columns_count == 1)
	{
		tmp_return_value = TRUE;
	}

	free_rows_and_columns(&tmp_returned_rows, tmp_rows_count, tmp_columns_count);

	return tmp_return_value;
}


DLLEXPORT char* DLLCALL EJLib_currentUser()
{
	char							***tmp_returned_rows = NULL;
	unsigned long		tmp_rows_count = 0;
	unsigned long		tmp_columns_count = 0;
	char							*tmp_return_value = NULL;

	if (m_session == NULL || m_session->m_db_connexion == NULL) return NULL;

	sprintf(m_session->m_last_query, "select current_user;");
	tmp_returned_rows = db_do_query(m_session, m_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
	if (tmp_returned_rows && tmp_rows_count == 1 && tmp_columns_count == 1)
	{
		tmp_return_value = (char*)malloc(strlen(tmp_returned_rows[0][0]) + 1);
		strcpy(tmp_return_value, tmp_returned_rows[0][0]);
	}

	free_rows_and_columns(&tmp_returned_rows, tmp_rows_count, tmp_columns_count);

	return tmp_return_value;
}

