/*****************************************************************************
Copyright (C) 2010 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R (Requirements and Tests Management Repository).

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef EJNATIVECLIBRARY_H_
#define EJNATIVECLIBRARY_H_

#ifdef RTMR_LIBRARY_EXPORTS
    #if (defined(_WINDOWS) || defined(WIN32))
            #define DLLEXPORT   __declspec( dllexport )
            #define DLLCALL		__stdcall
    #else
            #define DLLEXPORT   __attribute__((visibility("default")))
            #define DLLCALL
    #endif
#else
    #ifdef RTMR_LIBRARY_IMPORTS
		#if (defined(_WINDOWS) || defined(WIN32))
                #define DLLEXPORT   __declspec( dllimport )
                #define DLLCALL		__stdcall
        #else
                #define DLLEXPORT
                #define DLLCALL
        #endif
    #else
        #define DLLEXPORT
        #define DLLCALL
    #endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif
	
	
        DLLEXPORT int DLLCALL EJLib_init( const char* in_hostname, int in_port, const char* in_dbname, const char* in_username, const char* in_passwd, const char* in_log_trace_file_name, const char* in_log_error_file_name );

        DLLEXPORT int DLLCALL EJLib_die();

        DLLEXPORT char*** DLLCALL EJLib_runSql( const char* in_statement, unsigned long *out_rows_count, unsigned long *out_columns_count );

        DLLEXPORT int DLLCALL EJLib_callSqlProcedure( const char* in_procedure_name, char** in_out_parameters );

        DLLEXPORT int DLLCALL EJLib_loadRecord( int in_table_signature, char **in_out_columns_record, const char *in_primary_key_value );

        DLLEXPORT char*** DLLCALL EJLib_loadRecords( int in_table_signature, const char *in_where_clause, const char *in_order_by_clause, unsigned long *in_out_records_count );

        DLLEXPORT int DLLCALL EJLib_insertRecord( int in_table_signature, char** in_out_columns_record );

        DLLEXPORT int DLLCALL EJLib_saveRecord( int in_table_signature, char** in_out_columns_record, char** in_originals_columns_record );

        DLLEXPORT int DLLCALL EJLib_deleteRecord( int in_table_signature, char** in_columns_record );

        DLLEXPORT int DLLCALL EJLib_deleteRecords( int in_table_signature, const char* in_where_clause );

#ifdef _LDAP
        DLLEXPORT char*** DLLCALL EJLib_loadLdapEntries( int in_entry_signature, const char* in_ldap_url, const char* in_distinguish_name, const char* in_credencial, const char* in_base_query, const char* in_filter_query );

        DLLEXPORT int DLLCALL EJLib_loadLdapEntry( int in_entry_signature, const char* in_ldap_url, const char* in_distinguish_name, const char* in_credencial, char** in_out_columns_record, const char* in_base_query );
#endif

        DLLEXPORT char* DLLCALL EJLib_getErrorMessage( int in_error_id );

        DLLEXPORT char* DLLCALL EJLib_getLastErrorMessageExt();
        
        DLLEXPORT int DLLCALL EJLib_addUser(const char *in_username, const char *in_password, const char *in_rolename);

        DLLEXPORT int DLLCALL EJLib_removeUser(const char *in_username);

		DLLEXPORT int DLLCALL EJLib_modifyUserPassword(const char *in_username, const char *in_new_password);

		DLLEXPORT int DLLCALL EJLib_addRoleToUser(const char *in_rolename, const char *in_username);

        DLLEXPORT int DLLCALL EJLib_removeRoleFromUser(const char *in_rolename, const char *in_username);
        
        DLLEXPORT int DLLCALL EJLib_hasUserThisRole(const char *in_username, const char *in_rolename);

        DLLEXPORT char* DLLCALL EJLib_currentUser();


 #ifdef __cplusplus
}
#endif
#endif
