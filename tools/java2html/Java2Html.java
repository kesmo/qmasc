
public class Java2Html
{
	/**
	 * 
	 * @param in_object
	 * @param in_method
	 * @return
	 */
	private Object _invokeMethod(WOComponent in_object, String in_method)
	{
		Method tmpMethod;
		Object tmp_method_return = null;

		if (in_object != null)
		{
			try
			{
				tmpMethod = in_object.getClass().getMethod(in_method, null);
				tmp_method_return = tmpMethod.invoke(in_object, null);
			}
			catch (SecurityException e1)
			{
				NSLog.out.appendln("La m�thode '" + in_method + "' de la classe '" + in_object.getClass() + "' n'est pas acc�ssible (private ou protected).");
			}
			catch (NoSuchMethodException e1)
			{
				NSLog.out.appendln("La classe '" + in_object.getClass() + "' n'a pas de m�thode '" + in_method + "'.");
			}
			catch (IllegalArgumentException e)
			{
				NSLog.out.appendln("Les arguments pass�s � la m�thode '" + in_method + "' de la classe '" + in_object.getClass() + "' sont incorrects.");
			}
			catch (IllegalAccessException e)
			{
				NSLog.out.appendln("La m�thode '" + in_method + "' de la classe '" + in_object.getClass() + "' n'est pas acc�ssible (private ou protected).");

			}
			catch (InvocationTargetException e)
			{
				NSLog.out.appendln("La m�thode '" + in_method + "' de la classe '" + in_object.getClass() + "' a renvoy�e une exception.");
			}
		}

		return tmp_method_return;
	}


	/**
	 * Action par defaut
	 */
	public WOActionResults defaultAction()
	{
		WOSession tmp_session;
		String tmp_session_id = getSessionIDForRequest(request());
		WOApplication tmp_app = WOApplication.application();

		if (tmp_session_id != null)
		{
			// Recuperer la session
			tmp_session = tmp_app.sessionStore().restoreSessionWithID(tmp_session_id, null);

			if (tmp_session != null)
			{
				String tmp_context_id = (String) request().formValueForKey("contextID");
				String tmp_element_id = (String) request().formValueForKey("elementID");
				String tmp_method_name = (String) request().formValueForKey("method");

				// Recuperer la page
				WOComponent tmp_page = tmp_session.restorePageForContextID(tmp_context_id);

				if (tmp_page != null)
				{
					WOComponent tmp_element;
					int tmp_dot_start_pos = 0;
					int tmp_dot_end_pos = 0;
					tmp_element = tmp_page;

					tmp_page._awakeInContext(context());

					// Recuperer l'element source
					while (tmp_dot_start_pos >= 0 && tmp_page != null && (tmp_element = tmp_page._subcomponentForElementWithID(tmp_element_id)) == null)
					{
						tmp_dot_end_pos = tmp_element_id.indexOf('.', tmp_dot_start_pos);
						if (tmp_dot_end_pos >= 0)
						{
							tmp_page = tmp_page._subcomponentForElementWithID(tmp_element_id.substring(tmp_dot_start_pos, tmp_dot_end_pos));
							tmp_dot_start_pos = tmp_dot_end_pos + 1;
						}
					}

					if (tmp_element != null)
					{
						return (WOActionResults) _invokeMethod(tmp_element, tmp_method_name);
					}
				}
			}
		}

		return null;
	}

}
