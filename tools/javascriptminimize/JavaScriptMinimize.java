import java.io.File;


public class JavaScriptMinimize
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		File		tmp_src_dir = new File(args[0]);
		File		tmp_dest_dir = new File(args[1]);
		File[]		tmp_src_files;
		String	tmp_basename;
		
		if (tmp_src_dir.exists() && tmp_dest_dir.exists())
		{
			tmp_src_files = tmp_src_dir.listFiles();
			
        	for (int tmp_index = 0; tmp_index < tmp_src_files.length; tmp_index++)
        	{
        		tmp_basename = tmp_src_files[tmp_index].getName();
        		if (tmp_basename.endsWith(".js"))
        		{
        			jsmin.main(new String[]{tmp_src_files[tmp_index].getAbsolutePath(), tmp_dest_dir.getAbsolutePath() + File.separator + tmp_basename});
        		}
			}        	
			
		}
	}

}
