//================================================================================
//CLASSE : 	MOGraphicImage
//FICHIER :	MOGraphicImage.java
//PROJET : 	
//CREE PAR : 	EJ
//
//RESUME : Classe abstraite de g�n�ration d'images
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			16 mai 03	EJ		Cr�ation du fichier.
//================================================================================
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Paint;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import com.mediware.MOFoundation.MOControls.MOImage;

/**
 * Sous-classer cette classe pour r�aliser des traitements d'images.<br>
 * MOImage regroupe des propri�t�s redondantes des images :
 * 		<ul><li>largeur et hauteur</li>
 * 		<li>couleur de fond</li>
 * 		<li>couleur de crayon</li>
 * 		<li>transparence</li>
 * 		<li>format d'image</li><ul><br>
 * MOImage offre les fonctionnalit�s de bases suivantes :
 * 		<ul><li>sauvegarder l'image sur disque</li>
 * 		<li>mettre en m�moire l'image dans le cache de l'application</li>
 * 		<li>optimiser le rendu soit pour la qualit� soit pour la vitesse de traitement</li>
 */
public abstract class MOGraphicImage extends MOImage{

	/**
	 * Classe de gestion d'un r�sultat
	 */
    private static class Result
    {
        private Double	_m_value;
        
        public Result()
        {
            super();
            
            _m_value = null;
        }
        
        public void setValue(Double in_value)
        {
            _m_value = in_value;
        }
        
        public Double getValue()
        {
            return _m_value;
        }
    }

    
    /**
	 * Classe de gestion d'une op�ration
	 */
	protected static class Op{
	    
	    private static final int		_OPERATOR_PLUS = 0;
	    private static final int		_OPERATOR_LESS = 1;
	    private static final int		_OPERATOR_MULT = 2;
	    private static final int		_OPERATOR_DIV = 3;
	    private static final int		_OPERATOR_POW = 4;
	    private static final int		_OPERATOR_ROOT = 5;

	    private static final char		_OPERATORS[] = {'+', '-', '*', '/', '^', 'V'};
	    
	    /**
	     * Op�rande
	     */
	    Object		m_value;
	    
	    /**
	     * Op�rateur
	     */
	    int			m_operator;
	    
	    /**
	     * Listes de sous-op�rations
	     */
	    Chain		m_sub_expression;
	    
	    
	    /**
	     * Constructeur
	     */
	    private Op()
	    {
	        super();
	        
	        m_operator = -1;
	    }
	    
	    
	    /**
	     * Constructeur
	     */
	    private Op(Object in_value, int in_operator)
	    {
	        this();
	        m_value = in_value;
	        m_operator = in_operator;
	    }

		/**
		 * Calculer le r�sultat d'une �quation
		 * @param in_expression	cha�ne d�finissant l'�quation (ex : "((x+5)*2)^3")
		 * @param in_value	valeur de l'inconnue 'x' dans l'�quation
		 * @return
		 */
		public static double compute(String in_expression, double in_value)
		{
		    Result				tmp_result = new Result();
					
			Chain eq_chain = _stringToChainEquation(in_expression);
			
			
			_operate(null, eq_chain, tmp_result, in_value);
			if (tmp_result.getValue() == null)	return 0.0;
			
			return tmp_result.getValue().doubleValue();
		}

		
		/**
		 * Transfomer une �quation en liste (Chain) d'op�rations
		 * @param in_expression	cha�ne � transformer
		 **/
		static Chain _stringToChainEquation(String in_expression)
		{
			int					tmp_cursor_index = 0;
			Chain				tmp_operators_list = new Chain();
			Chain				tmp_current_chain = tmp_operators_list;
			char[]				tmp_chars;
			
			if (in_expression == null || in_expression.length() == 0)	return null;
			
			tmp_chars = in_expression.replaceAll("\\s", "").toCharArray();
			
			/* G�n�rer la liste des op�rations */
			while (tmp_cursor_index < tmp_chars.length)
			{
			    tmp_cursor_index = _getOperation(tmp_current_chain, tmp_chars, tmp_cursor_index);
			    if (tmp_cursor_index < 0 || tmp_cursor_index >= tmp_chars.length) break;
			    		    
			    tmp_current_chain.add();
			    tmp_current_chain = tmp_current_chain.next();
			}
			
			if (tmp_cursor_index < 0)	return null;
		    
			return tmp_operators_list;
		}

		
		/**
		 * Renvoie la position de la parenth�se droite correspondante � la position de la premi�re parenth�se gauche<br>
		 * Exemples d'appels de la fonction :<br>
		 * <ul><li>_rigthParenthesisIndex("((2*X)^3)", 0) renvoie la valeur 8.</li>
		 * <li>_rigthParenthesisIndex("((2*X)^3)", 1) renvoie la valeur 5.</li></ul>
		 * @param in_chars_array	cha�ne ou chercher la parenth�se droite
		 * @param in_start_index	position de la premi�re parenth�se gauche
		 * @return
		 */
		private static int _rigthParenthesisIndex(char in_chars_array[], int in_start_index)
		{
		    int		tmp_lp_index, tmp_rp_index;
		    
		    tmp_rp_index = indexOfChar(')', in_chars_array, in_start_index + 1);
		    if (tmp_rp_index < 0)	return -1;
		    
		    tmp_lp_index = indexOfChar('(', in_chars_array, in_start_index + 1);
		    if (tmp_lp_index < 0 || tmp_rp_index < tmp_lp_index)	return tmp_rp_index;
		    
		    return _rigthParenthesisIndex(in_chars_array, tmp_rp_index + 1);
		}

		
		/**
		 * R�cup�rer l'op�ration courante
		 * @param in_operator_list	Liste des op�rations
		 * @param chars_array	�quation sous forme de cha�ne
		 * @param in_index	position courante du curseur dans la cha�ne
		 * @return
		 */
		private static int _getOperation(
		        Chain	in_operator_list,
		        char[]	in_chars_array,
		        int		in_index
		        )
		{
		    boolean tmp_operator_indic = false;
		    int		out_index = in_index;
		    int		tmp_index = -1;
		    Op		tmp_operande = new Op();
		    
		    if (in_index >= in_chars_array.length) return in_index;
		    
		    /* Chercher l'op�rande */
		    switch (in_chars_array[out_index])
		    {
		    	case '-':
		    	    in_operator_list.setData(new Op(new Double(-1), Op._OPERATOR_MULT));
		    	    return ++out_index;
		    	case '+':
		    	    return _getOperation(in_operator_list, in_chars_array, ++out_index);
		    	case 'x':
		    	case 'X':
		    	    tmp_operande.m_value = "x";
		    	    out_index++;
		    	    break;
		    	case '(':		    	    
		    	    
		    	    tmp_index = _rigthParenthesisIndex(in_chars_array, ++out_index);
		    	    if (tmp_index < 0) return -1;

		    	    tmp_operande.m_sub_expression = _stringToChainEquation(new String(subCharArray(in_chars_array, out_index, tmp_index)));

		    	    out_index = tmp_index + 1;
		    	    break;
	    		default:
	    		    while (out_index < in_chars_array.length && !tmp_operator_indic)
	    		    {
	    			    switch (in_chars_array[out_index]){
	    			    	case '.':
	    			    	case '0':
	    			    	case '1':
	    			    	case '2':
	    			    	case '3':
	    			    	case '4':
	    			    	case '5':
	    			    	case '6':
	    			    	case '7':
	    			    	case '8':
	    			    	case '9':
	    			    	    out_index++;
	    			    	    break;
	    			    	default :
	    			    	    tmp_operator_indic = true;
	    			    }
	    		    }
	    		
	    			if (out_index == in_index)	return -1;
	    			
		    	    tmp_operande.m_value = new Double(String.copyValueOf(in_chars_array, in_index, out_index - in_index));
		    	    break;
		    }

		    
		    in_operator_list.setData(tmp_operande);

		    /* Chercher l'op�rateur */
		    tmp_index = _getOperator(in_operator_list, in_chars_array, out_index);
		    if (tmp_index >= 0)	out_index = tmp_index;
		    	    
		    return out_index;
	    }
		
		
		/**
		 * R�cup�rer l'op�rateur courant
		 * @param in_operator_list	Liste des op�rations
		 * @param chars_array	�quation sous forme de cha�ne
		 * @param in_index	position courante du curseur dans la cha�ne
		 * @return
		 */
		private static int _getOperator(
		    Chain	in_operator_list,
	        char[]	in_chars_array,
	        int		in_index)
		{
		    int 	out_index;
		    Op		tmp_operande;

		    if (in_index >= in_chars_array.length) return in_index;

		    out_index = in_index;
		    tmp_operande = (Op)in_operator_list.getData();
		    
		    if ((tmp_operande.m_operator = indexOfChar(in_chars_array[out_index], Op._OPERATORS, 0)) < 0)
		    {
			    switch (in_chars_array[out_index]){
			    	case 'x':
			    	case 'X':
			    	case '(':
			    	    tmp_operande.m_operator = Op._OPERATOR_MULT;
			    	    return in_index;
			    	default :
			    	    return -1;
			    }
		    }
		    		    
		    return in_index + 1;
		}


		/**
		 * Calculer le r�sultat d'une �quation
		 * @param in_operations_list (doit �tre <code>null</code>)
		 * @param in_next_list	la liste d'op�rations (calcul�e � partir de {@link #_stringToChainEquation(String)})
		 * @param une variable r�sultat non initialis�e
		 * @param la valeur de l'inconnue 'x' dans l'�quation
		 */
		static Chain _operate(Chain in_operations_list, Chain in_next_list, Result in_result, double in_x_value)
		{
		    Op		tmp_current_operation, tmp_next_operation;
		    Double	tmp_current_value, tmp_next_value;
		    Double	tmp_result;
		    Result	tmp_sub_result;
		    Chain	tmp_chain = in_next_list;

		    if (in_next_list == null)	return null;
		    
	        /* Seconde op�ration */
	        tmp_next_operation = (Op)in_next_list.getData();

	        if (in_operations_list == null)
		    {
	            in_result.setValue(_operationValue(tmp_next_operation, in_x_value));
	            return _operate(in_next_list, in_next_list.next(), in_result, in_x_value);	            
		    }
	        
	        /* Premiere op�ration */
	        tmp_current_operation = (Op)in_operations_list.getData();

	        if (in_result.getValue() == null)
		    {
	            tmp_current_value = _operationValue(tmp_current_operation, in_x_value);
	            if (tmp_current_value == null)	return null;
		    }
	        else
	        {
		        tmp_current_value = in_result.getValue();
		    }
	        

	        if (tmp_current_operation.m_operator >= tmp_next_operation.m_operator)
	        {
		        /* Valeur du premier op�rande */
	            tmp_next_value = _operationValue(tmp_next_operation, in_x_value);
		        if (tmp_next_value == null)	return null;

		        tmp_result = _calculate(tmp_current_value, tmp_next_value, tmp_current_operation.m_operator);
                if (in_result.getValue() != null)
                {
                    in_result.setValue(tmp_result);
                    return _operate(in_next_list, in_next_list.next(), in_result, in_x_value);
                }
                else
                {
                    in_result.setValue(tmp_result);
                }
	        }
	        else
	        {
	            tmp_sub_result = new Result();
	            tmp_chain = _operate(in_next_list, in_next_list.next(), tmp_sub_result, in_x_value);
	            if (tmp_sub_result.getValue() == null)	return null;
	            
	            in_result.setValue(_calculate(tmp_current_value, tmp_sub_result.getValue(), tmp_current_operation.m_operator));
	            if (tmp_chain != null)
	                return _operate(tmp_chain, tmp_chain.next(), in_result, in_x_value);
	        }
	        
		    return in_next_list;
		}
		
		
		/**
		 * Valeur d'une op�ration
		 * @param in_operation
		 * @param in_x_value
		 * @return
		 */
		private static Double _operationValue(Op in_operation, double in_x_value)
		{
		    Result tmp_result;
		    
	        if (in_operation.m_value != null)
	        {
	            if (in_operation.m_value instanceof Double)
	                return (Double)in_operation.m_value;
	            else if (in_operation.m_value instanceof String)
	                return new Double(in_x_value);
	        }
	        else if (in_operation.m_sub_expression != null)
			{
	            tmp_result = new Result();
	            _operate(null, in_operation.m_sub_expression, tmp_result, in_x_value);
	            return tmp_result.getValue();
			}
	        
		    return null;
		}

		
		/**
		 * Calculer l'op�ration de deux Double
		 * @param in_first_number premier op�rande
		 * @param in_second_number second op�rande
		 * @param in_operator op�rateur
		 */
		private static Double _calculate(Double in_first_number, Double in_second_number, int in_operator)
		{
		    double tmp_result;
		    
		    if (in_first_number == null || in_second_number == null)	return null;
		    
		    switch (in_operator)
		    {
		    	/* Division */
			    case Op._OPERATOR_DIV :
			        tmp_result = in_first_number.doubleValue() / in_second_number.doubleValue();
			    	break;
			    /* Puissance */
			    case Op._OPERATOR_POW :
			        tmp_result = Math.pow(in_first_number.doubleValue(), in_second_number.doubleValue());
		    		break;
			    /* Soustraction */
			    case Op._OPERATOR_LESS :
			        tmp_result = in_first_number.doubleValue() - in_second_number.doubleValue();
		    		break;
			    /* Racine (carr�, cubique...etc.) */
			    case Op._OPERATOR_ROOT :
			        tmp_result = Math.pow(in_second_number.doubleValue(), 1/in_first_number.doubleValue());
		    		break;
			    /* Addition */
			    case Op._OPERATOR_PLUS :
			        tmp_result = in_first_number.doubleValue() + in_second_number.doubleValue();
			    	break;
			   	/* Multiplication par defaut */
			    default :
			        tmp_result = in_first_number.doubleValue() * in_second_number.doubleValue();
			    	break;
		    }
		    
		    return new Double(tmp_result);
		}
		
		
	}

	/**
     * Constructeur
     */
    private MOGraphicImage()
	{
        super();
    }

    
	/**
	* Constructeur secondaire.
	*/
	public MOGraphicImage(double in_width, double in_height)
	{
	    super(in_width, in_height);
	}

	/**
	 * Renvoie une couleur plus fonc�e que la couleur pass�e param�tre.<br>
	 * Modification de la m�thode <code>darker()</code> de la classe <code>java.awt.Color</code><br>
	 * pour prise en charge de la valeur alpha de la couleur d'origine
	 * @param in_color	couleur d'origine
	 * @return
	 */
	public static Color darker(Color in_color)
	{
	    double	tmp_factor = 0.5;
	    
	    return new Color(Math.max((int)(in_color.getRed() * tmp_factor), 0), 
				 Math.max((int)(in_color.getGreen() * tmp_factor), 0),
				 Math.max((int)(in_color.getBlue() * tmp_factor), 0),
				 in_color.getAlpha());
	}
	
	

	/**
	 * Dessiner un parallepip�de rectangle.
	 * @param x position horizontale
	 * @param y position verticale
	 * @param width largeur
	 * @param height hauteur
	 * @param raised en 3D
	 * @param in_x_incline inclinaison horizontale
	 * @param in_y_incline inclinaison verticale
	 */
	protected void fill3DRect(double x, double y, double width, double height,
			   boolean raised, double in_x_incline, double in_y_incline)
	{
		Paint p = _m_Graphics2D.getPaint();
		Color c = _m_Graphics2D.getColor();
		Color brighter = c.brighter();
		Color darker = darker(c);
		double arcw = (width <= 10 ? 0 : 10), arch = (height <= 10 ? 0 : 10);
		
		/* Dessiner en relief ? */
		if (raised)
		{
		    /* Dessiner un parallepipede rectangle */
			_m_Graphics2D.setColor(darker(darker));
			/* Plan de gauche */
			_m_Graphics2D.fill(new Rectangle3D(x, y+1, height-2, width/2, -in_x_incline, in_y_incline, 90));
			/* Plan arri�re */
			_m_Graphics2D.fill(new Rectangle2D.Double(x + 1 + width/2 * Math.sin(Math.toRadians(in_y_incline)), y + 1 - width/2 * Math.sin(Math.toRadians(in_x_incline)), width-2, height-2));
			/* D�grad� de couleurs sur le plan de face */
			_m_Graphics2D.setPaint(new GradientPaint(new Point2D.Double(x+1, y+1), c, new Point2D.Double(x+1+width-2, y+1+height-2), darker));
			_m_Graphics2D.fill(new Rectangle2D.Double(x+1, y+1, width-2, height-2));
			/* Plan de droite */
			_m_Graphics2D.setColor(darker);
			_m_Graphics2D.fill(new Rectangle3D(x + 1, y+1, width-2, width/2, -in_x_incline, in_y_incline, 0));
			/* Plan de dessus */
			_m_Graphics2D.setColor(darker(darker));
			_m_Graphics2D.fill(new Rectangle3D(x + width - 1, y+1, height-2, width/2, -in_x_incline, in_y_incline, 90));
		}
		else
		{
		    /* Desssiner un rectangle arrondi */
			_m_Graphics2D.fill(new RoundRectangle2D.Double(x+1, y+1, width-2, height-2, arcw, arch));
			_m_Graphics2D.fill(new Rectangle2D.Double(x+1, y+height-arch/2, width-2, arch/2));

			/* Dessiner les ombres claires */
			_m_Graphics2D.setColor(brighter);
			/* Ligne claire de gauche */
			_m_Graphics2D.draw(new Line2D.Double(x + 1, y + arch/2, x + 1, y + height - 1));
			/* Arc clair gauche sup�rieur */
			_m_Graphics2D.draw(new Arc2D.Double(x + 1, y + 1, arcw, arch, 90, 90, Arc2D.OPEN));
			/* Ligne claire sup�rieure */
			_m_Graphics2D.draw(new Line2D.Double(x + arcw/2, y, x + width - arcw/2, y));
			/* Arc clair droit sup�rieur*/
			_m_Graphics2D.draw(new Arc2D.Double(x + width - arcw - 1, y + 1, arcw, arch, 0, 90, Arc2D.OPEN));
			/* Dessiner les ombres sombres */
			_m_Graphics2D.setColor(darker);
			/* Ligne sombre inf�rieure */
			_m_Graphics2D.draw(new Line2D.Double(x + 1, y + height - 1, x + width - 1, y + height - 1));
			/* Ligne sombre de droite */
			_m_Graphics2D.draw(new Line2D.Double(x + width - 1, y + arch/2, x + width - 1, y + height - 1));
			_m_Graphics2D.setPaint(p);
		}
	}
	
	

	/**
	 * Dessiner la repr�sentation graphique d'une equation d'inconnue 'x'.
	 * @param in_equation	une equation (i.e : 2 * x + 5)
	 */
	public void drawXdependantEquation(String in_equation)
	{
		double		tmp_x = 0.0;
		double		tmp_prev_x = 0.0;
		double		tmp_y = 0.0;
		double		tmp_prev_y = 0.0;
		Chain		tmp_eq_chain = Op._stringToChainEquation(in_equation);
		Result		tmp_result = new Result();

		
		Op._operate(null, tmp_eq_chain, tmp_result, tmp_prev_x);
		if (tmp_result.getValue() != null)
	    {
		    tmp_prev_y = _m_height - tmp_result.getValue().doubleValue();
		    
			_m_Graphics2D.setColor(_m_pencil_Color);
			
			for (tmp_x = 1; tmp_x < _m_width; tmp_x++)
			{
				Op._operate(null, tmp_eq_chain, tmp_result, tmp_x);
				if (tmp_result.getValue() != null)
			    {
					tmp_y = _m_height - tmp_result.getValue().doubleValue();
					
					_m_Graphics2D.draw(new Line2D.Double( tmp_prev_x, tmp_prev_y, tmp_x, tmp_y));
			    }
				tmp_prev_x = tmp_x;
				tmp_prev_y = tmp_y;
			}
	    }
	}
	
	
	/**
	 * Dessiner la repr�sentation graphique d'une equation d'inconnue 'y'.
	 * @param in_equation	une equation (i.e : y + 5)
	 */
	public void drawYdependantEquation(String in_equation)
	{
		double		tmp_x = 0.0;
		double		tmp_prev_x = 0.0;
		double		tmp_y = 0.0;
		double		tmp_prev_y = 0.0;
		Chain		tmp_eq_chain;
		Result		tmp_result = new Result();
		String		tmp_equation_str = in_equation.replace('y', 'x');
		
		tmp_equation_str = tmp_equation_str.replace('Y', 'x');
		
		tmp_eq_chain = Op._stringToChainEquation(tmp_equation_str);
		
		Op._operate(null, tmp_eq_chain, tmp_result, tmp_prev_y);
		if (tmp_result.getValue() != null)
	    {
		    tmp_prev_x = tmp_result.getValue().doubleValue();
		    
			_m_Graphics2D.setColor(_m_pencil_Color);
			
			for (tmp_y = 1; tmp_y < _m_height; tmp_y++)
			{
			    
				Op._operate(null, tmp_eq_chain, tmp_result, tmp_y);
				if (tmp_result.getValue() != null)
			    {
					tmp_x = tmp_result.getValue().doubleValue();
					
					_m_Graphics2D.draw(new Line2D.Double( tmp_prev_x, tmp_prev_y, tmp_x, tmp_y));
			    }
				tmp_prev_x = tmp_x;
				tmp_prev_y = tmp_y;
			}
	    }
	}
}
