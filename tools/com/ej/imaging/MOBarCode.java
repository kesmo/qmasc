//================================================================================
//CLASSE : 	MOBarCode
//FICHIER :	MOBarCode.java
//PROJET : 	MOBarCode
//
//RESUME :		Cette classe g�n�re des images de codes � barres
//
//================================================================================
//	VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//	1.0			15-FE-05	EJ		Cr�ation du fichier.
//
//================================================================================
package com.mediware.MOFoundation.MOControls;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.text.NumberFormat;
import java.util.Locale;


/**
 * Cette classe g�n�re des images de codes � barres
 */
public class MOBarCode extends MOImage {

	// --------------------------------------------------------------
	// CONSTANTES
	// --------------------------------------------------------------

    /**
     * Indentifiant du Code39
     */
    public static final int CODE39 = 0;
    
    /**
     * Indentifiant du code EAN13
     */
    public static final int EAN13 = 1;

    /**
     * Caract�res encodables en Code39
     */
    public static final char CODE39_CHARS[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 
            'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
            '-', '.', ' ', '$', '/', '+', '%', '*'};
    
    /**
     * Correspondances Code39 Caract�re / code.<br>
     * Un '1' correspond � une barre.<br>
     * Un '0' correspond � un blanc.
     */
    public static final String	CODE39_CODES[] =  {"101001101101", "110100101011", "101100101011",
            "110110010101", "101001101011", "110100110101", "101100110101", "101001011011", 
            "110100101101", "101100101101", "110101001011", "101101001011", "110110100101", 
            "101011001011", "110101100101", "101101100101", "101010011011", "110101001101", 
            "101101001101", "101011001101", "110101010011", "101101010011", "110110101001", 
            "101011010011", "110101101001", "101101101001", "101010110011", "110101011001", 
            "101101011001", "101011011001", "110010101011", "100110101011", "110011010101", 
            "100101101011", "110010110101", "100110110101", "100101011011", "110010101101",
            "100110101101", "100100100101", "100100101001", "100101001001", "101001001001",
            "100101101101"};

    /**
     * Caract�res encodables en EAN13
     */
    public static final char EAN13_CHARS[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    
    /**
     * Correspondances EAN13 Caract�re / code pour chacun des 3 'sets' (A, B et C).<br>
     * Un '1' correspond � une barre.<br>
     * Un '0' correspond � un blanc.
     */
    public static final String	EAN13_CODES[][] = {
            /* set A */
            {"0001101", "0011001", "0010011", "0111101", "0100011", "0110001", "0101111", 
                "0111011", "0110111", "0001011"},
            /* set B */
            {"0100111", "0110011", "0011011", "0100001", "0011101", "0111011", "0000101",
                "0010001", "0001001", "0010111"},
            /* set C */
            {"1110010", "1100110", "1101100", "1000010", "1011100", "1001110", "1010000",
                "1000100", "1001000", "1110100"} };
    
    /**
     * Liste des tables de 'sets' d�finissant la fa�on d'encoder la partir gauche du code � barres.
     * Un '0' correspond au set A.<br>
     * Un '1' correspond au set B.<br>
     * Le set C n'est utilis� que pour coder la partie droite du code � barres,<br>
     * laquelle ne d�pend pas de cette table.
     */
    public static final String EAN13_SETS[] = {"000000", "001011", "001101", "001110", "010011",
            "011001", "011100", "010101", "010110", "011010"};
       
    /**
     * Indentifiant d'un code � barre horizontal texte en bas (position normale)
     */                          	             
    public static final int HORIZONTAL_BOTTOM_BARCODE = 0;
    
    /**
     * Indentifiant d'un code � barre horizontal texte en haut (retourn� � 180�)
     */                          	             
    public static final int HORIZONTAL_TOP_BARCODE = 1;
    
    /**
     * Indentifiant d'un code � barre vertical texte � gauche (retourn� � 270�)
     */                          	             
    public static final int VERTICAL_LEFT_BARCODE = 2;

    /**
     * Indentifiant d'un code � barre vertical texte � gauche (retourn� � 90�)
     */                          	             
    public static final int VERTICAL_RIGHT_BARCODE = 3;


	// --------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------

	/* Type de code barre */
	private int		_m_type_code = CODE39;
	
	/* Code barre */
	private String	_m_barcode_str;
	
	/* Orientation */
	private int		_m_orientation = HORIZONTAL_BOTTOM_BARCODE;
	
	/* Taille du texte */
	private float	_m_text_font_size = 12;
	
	/* Hauteur et largeur des barres */
	private int		_m_bar_width = 1;
	private int		_m_bar_height = 60;



	/**
	 * Constructeur
	 */
	public MOBarCode ()
	{
	    this(CODE39, "");	    
	}
	
	
	/**
	 * Constructeur
	 * @param in_type_code	type de code ({@link #CODE39} ou {@link #EAN13})
	 */
	public MOBarCode (int in_type_code)
	{
	    this(in_type_code, "");	    
	}
	
	
    /**
     * Constructeur
	 * @param in_code	libell� du code
     */
    public MOBarCode (String in_code)
    {
	    this(CODE39, in_code);	    
    }

    
    /**
     * Constructeur
	 * @param in_type_code	type de code ({@link #CODE39} ou {@link #EAN13})
	 * @param in_code	libell� du code
     */
    public MOBarCode (int in_type_code, String in_code)
    {
	    super();

	    _m_type_code = in_type_code;
        _m_orientation = HORIZONTAL_BOTTOM_BARCODE;
        
        _m_barcode_str = getFormattedBarcode(in_type_code, in_code);
        
        _createImage();
    }
    
    
    /**
     * Initialisation des dimensions du code � barres et creation de l'image
     * @see	com.mediware.MOTools.MOImage#_createImage()
     */
    protected void _createImage()
    {
	    _initBarCodeDim();
	    
        super._createImage();
        
		/* Initilisation de la taille de police */
		Font tmp_gf = _m_Graphics2D.getFont();

	    if (_m_text_font_size > 0.0)	_m_Graphics2D.setFont(tmp_gf.deriveFont(_m_text_font_size));
    }
    
    
    /**
     * Initialiser les dimensions du code � barres
     */
    private void _initBarCodeDim ()
    {
        switch (_m_type_code){
        
			case CODE39 :
			    if (_m_orientation == HORIZONTAL_BOTTOM_BARCODE || _m_orientation == HORIZONTAL_TOP_BARCODE)
			    {
			        _m_width = _m_barcode_str.length() * 13 * _m_bar_width;
			        _m_height = _m_bar_height + (int)_m_text_font_size + 4;
			    }
			    else
			    {
			        _m_height = _m_barcode_str.length() * 13 * _m_bar_width;
			        _m_width = _m_bar_height + (int)_m_text_font_size + 4;
			    }
			    break;

			case EAN13 :
			    if (_m_orientation == HORIZONTAL_BOTTOM_BARCODE || _m_orientation == HORIZONTAL_TOP_BARCODE)
			    {
			        _m_width = 105 * _m_bar_width;
			        _m_height = _m_bar_height + (int)_m_text_font_size + 4;
			    }
			    else
			    {
			        _m_height = 105 * _m_bar_width;
			        _m_width = _m_bar_height + (int)_m_text_font_size + 4;
			    }
			    break;
        }
    }
    
    
    /**
     * Formatage du code � barres
     * @param in_type_code	type de code ({@link #CODE39} ou {@link #EAN13})
     * @param in_code	libell� du code
     * @return
     */
    public String getFormattedBarcode (int in_type_code, String in_code)
    {
        NumberFormat nbFormat;
        String tmp_out_str = in_code;
        
        switch (in_type_code){
        
    		case CODE39 :
    		    tmp_out_str = "*" + in_code + "*";
    		    break;
    		    
    		case EAN13 :
    		    if (in_code.length() < 13)
    		    {
	    		    nbFormat = NumberFormat.getIntegerInstance(Locale.FRANCE);
	    		    nbFormat.setGroupingUsed(false);
	    		    nbFormat.setMinimumIntegerDigits(13);
	    		    tmp_out_str = nbFormat.format(Long.parseLong(in_code));
    		    }
        }
        
        return tmp_out_str.trim();
    }
    	

	/**
	 * Chercher un caract�re dans un tableau
	 * @param in_search_str	caract�re � chercher
	 * @param in_str_array	tableau de caract�res
	 * @return index du caract�re danbs le tableau ou -1 si le caract�re n'a pas �t� trouv�.
	 */
	public static int indexOfChar(char in_search_str, char[] in_str_array)
	{
	    int ii;
	    
	    for (ii = 0; ii < in_str_array.length; ii++)	if (in_str_array[ii] == in_search_str)	return ii;

	    return -1;
	}


	/**
	 * Dessiner le code � barres pass� en param�tre
	 * @param in_bar_code	libell� du code
	 */
	public void drawBarCode (String in_bar_code)
	{
	    setBarCode(in_bar_code);
	    
	    /* Dessiner le code � barres */
	    draw();
	}
	
	
	/**
	 * D�finir le code � dessiner.
	 * @param in_bar_code : Libell� du code
	 */ 
	public void setBarCode (String in_bar_code)
	{
	    String tmp_new_code_str = getFormattedBarcode(_m_type_code, in_bar_code);
	    String tmp_old_code_str = _m_barcode_str;
	    
	    _m_barcode_str = tmp_new_code_str;
	    
	    if (tmp_old_code_str == null || 
	            tmp_old_code_str != null && tmp_old_code_str.length() != tmp_new_code_str.length())
	        /* Recr�er une image si la longueur du nouveau code est diff�rente */
	        _createImage();
	    else
	        /* Effacer l'image */
	        clear();
	}
	
	
	/**
	 * Dessiner le code � barres
	 */
	public void draw ()
	{
	    AffineTransform tmp_originTr = null, tmp_newTr = null;
	    
	    /* V�rifier le code demand�e */
	    if (_m_barcode_str != null)
	    {
	        
		    /* Couleur de dessin */
		    _m_Graphics2D.setColor(Color.BLACK);
		    
		    /* Changement d'orientation => transformations affines */
		    if (_m_orientation != HORIZONTAL_BOTTOM_BARCODE)
		    {
		        tmp_originTr =_m_Graphics2D.getTransform();
		        
		        tmp_newTr = new AffineTransform();
		        
		        switch (_m_orientation)
		        {
		        	case(HORIZONTAL_TOP_BARCODE):
				        tmp_newTr.rotate(Math.toRadians(-180));
		        		tmp_newTr.translate(-_m_width, -_m_height);
		        	    break;
		        	    
		        	case(VERTICAL_RIGHT_BARCODE):
				        tmp_newTr.rotate(Math.toRadians(-90));
			        	tmp_newTr.translate(-_m_height, 0);
		        	    break;
		        	    
		        	case(VERTICAL_LEFT_BARCODE):
				        tmp_newTr.rotate(Math.toRadians(90));
			        	tmp_newTr.translate(0, -_m_width);
		        	    break;
		        }
		        
		        _m_Graphics2D.transform(tmp_newTr);
		    }
	
		    /* Traitement en fonction du type de code barre */
		    switch (_m_type_code)
		    {
		    	case CODE39 :
		    	    _drawCODE39();
		    	    break;
		    	    
		    	case EAN13 :
		    	    _drawEAN13();
		    	    break;
		    }
		    if (tmp_originTr != null) _m_Graphics2D.setTransform(tmp_originTr);
	    }
	}

	
	/**
	 * Dessiner un code Code39
	 */
	private void _drawCODE39 ()
	{
	    int 		tmp_char_index;
	    int			tmp_char_code_index;
	    int			tmp_barcode_char_index = -1;
	    int			tmp_position = 0;
	    int			tmp_text_position_y = _m_bar_height + (int)_m_text_font_size + 1;
	    TextLayout	tmp_str_layout;
	    
	    /* Parcourrir les caract�res de la chaine � coder */
	    for (tmp_char_index = 0; tmp_char_index < _m_barcode_str.length(); tmp_char_index++)
	    {
	        /* Chercher le caract�re � coder */
	        tmp_barcode_char_index = indexOfChar(_m_barcode_str.charAt(tmp_char_index), CODE39_CHARS);
	        
	        if (tmp_barcode_char_index > -1)
	        {
	            
	            /* Parcourrir les digits (0 et 1) du code correspondant au caract�re */
	            for (tmp_char_code_index = 0; tmp_char_code_index < CODE39_CODES[tmp_barcode_char_index].length(); tmp_char_code_index++)
	            {

	                /* Desssiner une barre si le digit est 1 */
	                if (CODE39_CODES[tmp_barcode_char_index].charAt(tmp_char_code_index) == '1')
	                {
	                    
	                    /* Calculer la position de la barre */
	                    tmp_position = (tmp_char_index * CODE39_CODES[tmp_barcode_char_index].length() + tmp_char_index + tmp_char_code_index) * _m_bar_width;
            	        
            	        /* Tracer une barre */
	            	    _m_Graphics2D.fillRect(tmp_position, 0, _m_bar_width , _m_bar_height);
	                }
	            }
	        }
	        
		    /* Dessiner le libell� */
		    if (_m_text_font_size > 0)
		    {
				tmp_str_layout = new TextLayout(_m_barcode_str.substring(tmp_char_index, tmp_char_index + 1), _m_Graphics2D.getFont(), _m_Graphics2D.getFontRenderContext());

				if (tmp_str_layout != null)	tmp_str_layout.draw(_m_Graphics2D, (tmp_char_index * 13 + 1) * _m_bar_width, tmp_text_position_y);
		    }
	    }
	}
	
	
	/**
	 * Dessiner un code EAN-13
	 */
	private void _drawEAN13 ()
	{
	    int			tmp_char_index;
	    int 		tmp_char_code_index;
	    int			tmp_barcode_char_index = -1;
	    int			tmp_position = 0;
	    int			tmp_set_index = 0;
	    int			tmp_longbar_height = _m_bar_height + (int)_m_text_font_size/2;
	    int			tmp_text_position_y =  _m_bar_height + (int)_m_text_font_size + 1;
	    String		tmp_set_str;
	    TextLayout	tmp_str_layout;
	    
	    /* Dessiner le premier chiffre */
	    if (_m_text_font_size > 0)
	    {
			tmp_str_layout = new TextLayout(_m_barcode_str.substring(0, 1),  _m_Graphics2D.getFont(), _m_Graphics2D.getFontRenderContext());

			if (tmp_str_layout != null)	tmp_str_layout.draw(_m_Graphics2D, (float)(10 * _m_bar_width - tmp_str_layout.getBounds().getWidth() - 1), tmp_text_position_y);	        
	    }

	    /* Dessiner les barres de debut, du centre et de fin */
	    _m_Graphics2D.fillRect(10 * _m_bar_width, 0, _m_bar_width , tmp_longbar_height);
	    _m_Graphics2D.fillRect(12 * _m_bar_width, 0, _m_bar_width , tmp_longbar_height);
	    _m_Graphics2D.fillRect(56 * _m_bar_width, 0, _m_bar_width , tmp_longbar_height);
	    _m_Graphics2D.fillRect(58 * _m_bar_width, 0, _m_bar_width , tmp_longbar_height);
	    _m_Graphics2D.fillRect(102 * _m_bar_width, 0, _m_bar_width , tmp_longbar_height);
	    _m_Graphics2D.fillRect(104 * _m_bar_width, 0, _m_bar_width , tmp_longbar_height);
	    	        
	    /* Calcul de la table des sets � utiliser pour coder les caract�res (voir spec. EAN13) */
	    tmp_set_str = EAN13_SETS[indexOfChar(_m_barcode_str.charAt(0), EAN13_CHARS)];  

	    /* Parcourrir les 6 premiers caract�res de la chaine � coder (partie gauche) */
	    for (tmp_char_index = 1; tmp_char_index <= 6; tmp_char_index++)
	    {
	        
	        /* Chercher le caract�re � coder */
	        tmp_barcode_char_index = indexOfChar(_m_barcode_str.charAt(tmp_char_index), EAN13_CHARS);
	        
	        if (tmp_barcode_char_index > -1)
	        {
	                	    
	            /* Parcourrir les digits (0 et 1) du code correspondant au caract�re */
	            for (tmp_char_code_index = 0; tmp_char_code_index < 7; tmp_char_code_index++)
	            {
	                /* Calcul l'index du set (0 ou 1 => A ou B) � utiliser */
	                tmp_set_index = tmp_set_str.charAt(tmp_char_index - 1) - '0';
	                    
	                /* Desssiner une barre si le digit est 1 */
	                if (EAN13_CODES[tmp_set_index][tmp_barcode_char_index].charAt(tmp_char_code_index) == '1')
	                {
	                    
	                    /* Calculer la position de la barre */
	                    tmp_position = (13 + (tmp_char_index - 1 ) * 7 + tmp_char_code_index) * _m_bar_width;
            	        
            	        /* Tracer une barre */
	            	    _m_Graphics2D.fillRect(tmp_position, 0, _m_bar_width , _m_bar_height);
	                }
	            }
	        }
	        
	        if (_m_text_font_size > 0)
	        {
			    /* Dessiner le chiffre courant */
				tmp_str_layout = new TextLayout(_m_barcode_str.substring(tmp_char_index, tmp_char_index + 1), _m_Graphics2D.getFont(), _m_Graphics2D.getFontRenderContext());

				if (tmp_str_layout != null)	tmp_str_layout.draw(_m_Graphics2D, (float)(14 + (tmp_char_index - 1 ) * 7) * _m_bar_width, tmp_text_position_y);
	        } 
	    }
	    
	    
	    /* Parcourrir les 6 derniers caract�res de la chaine � coder (partie droite) */
	    for (tmp_char_index = 7; tmp_char_index <= 12; tmp_char_index++)
	    {
	        /* Chercher le caract�re � coder */
	        tmp_barcode_char_index = indexOfChar(_m_barcode_str.charAt(tmp_char_index), EAN13_CHARS);
	        
	        if (tmp_barcode_char_index > -1)
	        {
	            
	            /* Parcourrir les digits (0 et 1) du code correspondant au caract�re */
	            for (tmp_char_code_index = 0; tmp_char_code_index < 7; tmp_char_code_index++) {
	                /* Desssiner une barre si le digit est 1 */
	                if (EAN13_CODES[2][tmp_barcode_char_index].charAt(tmp_char_code_index) == '1')
	                {
	                    /* Calculer la position de la barre */
	                    tmp_position = (13 + 5 + (tmp_char_index - 1 ) * 7 + tmp_char_code_index) * _m_bar_width;
            	        
            	        /* Tracer une barre */
	            	    _m_Graphics2D.fillRect(tmp_position, 0, _m_bar_width , _m_bar_height);
	                }
	            }
	        }
	        
	        if (_m_text_font_size > 0)
	        {
		        /* Dessiner le chiffre courant */
				tmp_str_layout = new TextLayout(_m_barcode_str.substring(tmp_char_index, tmp_char_index + 1), _m_Graphics2D.getFont(), _m_Graphics2D.getFontRenderContext());
				if (tmp_str_layout != null)	tmp_str_layout.draw(_m_Graphics2D, (float)(19  + (tmp_char_index - 1 ) * 7) * _m_bar_width, tmp_text_position_y);
	        }
	    }
	}

	
	/**
	 * D�finir les propri�t�s du code � barres
	 * @param in_orientation	Les valeurs qui d�finissent l'orientation sont les suivantes :<br>
	 * 		<ul><li> Une valeur n�gative signifie ne pas modifier l'orientation</li>
	 * 		<li> La valeur {@link #HORIZONTAL_BOTTOM_BARCODE} correspond � un code � barres horizontal avec le texte en bas(c'est la valeur par d�faut)</li>
	 * 		<li> La valeur {@link #HORIZONTAL_TOP_BARCODE} correspond � un code � barres horizontal avec le texte en haut</li>
	 * 		<li> La valeur {@link #VERTICAL_LEFT_BARCODE} correspond � un code � barres vertical avec le texte � gauche</li>
	 * 		<li> La valeur {@link #VERTICAL_RIGHT_BARCODE} correspond � un code � barres vertical avec le texte � droite</li></ul>
	 * @param in_bar_height	hauteur des barres (n�gatif => ne pas modifier)
	 * @param in_bar_width	largeur des barres (n�gatif => ne pas modifier)
	 * @param in_font_size	taille du texte (n�gatif => ne pas modifier, z�ro => pas de texte)
	 */
	public void setBarCodeProperties (int in_orientation, int in_bar_height, int in_bar_width, float in_font_size)
	{
	    if (in_orientation > -1)	_m_orientation = in_orientation;
	    
	    if (in_bar_height > -1)	_m_bar_height = in_bar_height;
	    
	    if (in_bar_width > 0)	_m_bar_width = in_bar_width;
	    
	    if (in_font_size >= 0.0)	_m_text_font_size = in_font_size;
	    
	    /* Recr�er l'image */
	    _createImage();
	}

}
