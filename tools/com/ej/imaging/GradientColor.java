//================================================================================
//CLASSE : 	GradientColor
//FICHIER :	GradientColor.java
//PROJET : 	MOTools
//CREE PAR : EJ
// 
//RESUME : Gestion d'un d�grad� de couleurs
//
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			3 sept. 2004		EJ		Cr�ation du fichier.
//================================================================================
import java.awt.Color;

/**
 * Gestion d'un d�grad� de couleurs
 */
public class GradientColor extends Color
{
	
	protected int		m_total_gradation = 0;
	
	/* Pas(step) de saturation */
	private int			m_step_red = 0;
	private int			m_step_blue = 0;
	private int			m_step_green = 0;
	
	/* Index du d�grad� courant */
	private int			m_current_gradation = 0;
	
	private Color		_m_colors_array[];
	
	/**
	 * Constructeur
	 * @param in_start_color : couleur de d�part
	 */
	public GradientColor(Color in_start_color)
	{
		super(in_start_color.getRGB());
	}
	
	
	/**
	 * Constructeur
	 * @param in_colors_array : tableau de couleurs
	 */
	public GradientColor(Color in_colors_array[])
	{
	    super(in_colors_array[0].getRGB());
	    
		_m_colors_array = in_colors_array;
	}
	
	
	/**
	 * Constructeur
	 * @param in_start_color : couleur de d�part
	 * @param in_gradation : nombre de couleurs
	 */
	public GradientColor(Color in_start_color, int in_gradation)
	{
		this(in_start_color);
		
		setGradation(in_gradation);
	}
	
	
	/**
	 * Constructeur
	 * @param in_colors_array : tableau de couleurs
	 * @param in_gradation : nombre de couleurs
	 */
	public GradientColor(Color in_colors_array[], int in_gradation)
	{
		this(in_colors_array);
		
		setGradation(in_gradation);
	}
	
	
	/**
	 * Couleur d�grad�e suivante
	 * @return
	 */
	public Color nextColor()
	{
		return colorAtIndex(++m_current_gradation);
	}
	
	
	/**
	 * Couleur d�grad�e pr�c�dente
	 * @return
	 */
	public Color previousColor()
	{		
		return colorAtIndex(--m_current_gradation);
	}
	
	
	/**
	 * Couleur d�grad�e � la nim�me position
	 * @return
	 */
	public Color colorAtIndex(int in_color_index)
	{
		/* Composantes de la couleur */
		int tmp_red;
		int tmp_green;
		int tmp_blue;
		int	tmp_index;
		
		if (_m_colors_array != null)
		{
		    if (_m_colors_array.length > in_color_index)
		    {
				return _m_colors_array[in_color_index];
		    }
		    else
		    {
		        m_total_gradation = in_color_index / _m_colors_array.length + 1;
		        tmp_index = in_color_index % _m_colors_array.length;
		        tmp_red = _m_colors_array[tmp_index].getRed();
		        tmp_green = _m_colors_array[tmp_index].getGreen();
		        tmp_blue = _m_colors_array[tmp_index].getBlue();
				
				_stepsForColor(_m_colors_array[tmp_index]);
				
				return new Color(tmp_red + m_step_red * (m_total_gradation-1),
				        tmp_green + m_step_green * (m_total_gradation-1),
				        tmp_blue + m_step_blue * (m_total_gradation-1));		
		    }

		}
		else
		{
		    tmp_red = getRed();
		    tmp_green = getGreen();
		    tmp_blue = getBlue();
			
			return new Color(tmp_red + m_step_red * in_color_index,
			        tmp_green + m_step_green * in_color_index,
			        tmp_blue + m_step_blue * in_color_index);		
		}

	}
	
	
	/**
	 * D�finir le nombre de couleurs d�grad�es
	 * @param in_gradation : nombre de couleurs
	 */
	public void setGradation(int in_gradation)
	{
		/* Composantes de la couleur */
		int tmp_red = getRed();
		int tmp_green = getGreen();
		int tmp_blue = getBlue();
				
		if (in_gradation > 0)
		{
			m_total_gradation = in_gradation;
			/* 
			 * Calcul des pas rouge, vert, bleu pour la saturation.
			 * Cela permet de donner un effet de d�grad� de couleurs.
			 * */
			if (tmp_red >= tmp_green)
			{
				if (tmp_red >= tmp_blue)
				{
					m_step_red = 0;
					m_step_green = (tmp_red - tmp_green) / m_total_gradation;
					m_step_blue = (tmp_red - tmp_blue) / m_total_gradation;
				}
				else
				{
					m_step_blue = 0;
					m_step_green = (tmp_blue - tmp_green) / m_total_gradation;
					m_step_red = (tmp_blue - tmp_red) / m_total_gradation;
				}
			}
			else if (tmp_green >= tmp_blue)
			{
				m_step_green = 0;
				m_step_red = (tmp_green - tmp_red) / m_total_gradation;
				m_step_blue = (tmp_green - tmp_blue) / m_total_gradation;
			}
			else
			{
				m_step_blue = 0;
				m_step_green = (tmp_blue - tmp_green) / m_total_gradation;
				m_step_red = (tmp_blue - tmp_red) / m_total_gradation;
			}
		}
	}
	
	
	private void _stepsForColor(Color in_color)
	{
		/* Composantes de la couleur */
		int tmp_red = in_color.getRed();
		int tmp_green = in_color.getGreen();
		int tmp_blue = in_color.getBlue();
				
		/* 
		 * Calcul des pas rouge, vert, bleu pour la saturation.
		 * Cela permet de donner un effet de d�grad� de couleurs.
		 * */
		if (tmp_red >= tmp_green)
		{
			if (tmp_red >= tmp_blue)
			{
				m_step_red = 0;
				m_step_green = (tmp_red - tmp_green) / m_total_gradation;
				m_step_blue = (tmp_red - tmp_blue) / m_total_gradation;
			}
			else
			{
				m_step_blue = 0;
				m_step_green = (tmp_blue - tmp_green) / m_total_gradation;
				m_step_red = (tmp_blue - tmp_red) / m_total_gradation;
			}
		}
		else if (tmp_green >= tmp_blue)
		{
			m_step_green = 0;
			m_step_red = (tmp_green - tmp_red) / m_total_gradation;
			m_step_blue = (tmp_green - tmp_blue) / m_total_gradation;
		}
		else
		{
			m_step_blue = 0;
			m_step_green = (tmp_blue - tmp_green) / m_total_gradation;
			m_step_red = (tmp_blue - tmp_red) / m_total_gradation;
		}
	}	
}
