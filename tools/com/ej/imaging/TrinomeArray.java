//================================================================================
//CLASSE : 	TrinomeArray
//FICHIER :	TrinomeArray.java
//PROJET : 	MOTools
//CREE PAR : EJ
// 
//RESUME : Gestion d'une liste de Trinome
//
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			19 ao�t 2004		EJ		Cr�ation du fichier.
//================================================================================

import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;

/**
 * Gestion d'un tableau de Trinome
 */
public class TrinomeArray 
{
    /**
     * Liste des objets X
     */
	public Trinome.DataObject[]	m_x_objects = null;
	
	/**
	 * Liste des objets Y
	 */
	public Trinome.DataObject[]	m_y_objects = null;
	
	/**
	 * Liste des objets Z
	 */
	public Trinome.DataObject[]	m_z_objects = null;

	/**
	 * Valeur la plus grande parmi les X 
	 **/
	public Trinome.DataObject	m_max_x = null;
	
	/**
	 * Valeur la plus grande parmi les Y 
	 **/
	public Trinome.DataObject	m_max_y = null;
	
	/**
	 * Valeur la plus grande parmi les Z 
	 **/
	public Trinome.DataObject	m_max_z = null;
	
	/**
	 * Valeur la plus petite parmi les X 
	 **/
	public Trinome.DataObject	m_min_x = null;
	
	/**
	 * Valeur la plus petite parmi les Y 
	 **/
	public Trinome.DataObject	m_min_y = null;
	
	/**
	 * Valeur la plus petite parmi les Z 
	 **/
	public Trinome.DataObject	m_min_z = null;
	
	/**
	 * Somme des X (si les X sont des nombres)
	 */
	public Double		m_total_x = null;

	/**
	 * Somme des Y (si les Y sont des nombres)
	 */
	public Double		m_total_y = null;
	
	/**
	 * Tableau des sommes des valeurs Y par cat�gories X
	 */
	public double		m_total_y_by_x_class[];
	
	/**
	 * Valeur la plus grande des sommes des valeurs Y par cat�gories X
	 */
	public double		m_max_total_y_by_x_class = 0.0;
	
	/**
	 * Somme des Z (si les Z sont des nombres)
	 */
	public Double		m_total_z = null;
	
	/**
	 * Indicateur de pr�sence de nombres d�cimaux parmi les X
	 */
	public boolean		m_has_decimal_x_indic = false;
	
	/**
	 * Indicateur de pr�sence de nombres d�cimaux parmi les Y
	 */
	public boolean		m_has_decimal_y_indic = false;
	
	/**
	 * Nombre de trinomes
	 **/
	public int			length = 0;
	
	/* Tableau des trinomes */
	private NSMutableArray	_m_trinomes_array = null;
	
	/**
	 * Tableau des valeurs distinctes X
	 **/
	public NSMutableArray	m_x_categories = null;
	
	/**
	 * Tableau des valeurs distinctes Y
	 **/
	public NSMutableArray	m_y_categories = null;
	
	/**
	 * Tableau des valeurs distinctes Z
	 **/
	public NSMutableArray	m_z_categories = null;


	/**
	 * Constructeur
	 * @param in_array	tableau de trinomes
	 */
	public TrinomeArray(Trinome[] in_array)
	{
		
		if (in_array != null && in_array.length > 0){
			_m_trinomes_array = new NSMutableArray(in_array);
			
			length = in_array.length;
			
			/* Ordonner le tableau avant traitement */
			orderArray();
		}
	}
	
	
	/**
	 * Constructeur
	 * @param in_array	tableau de trinomes
	 */
	public TrinomeArray(NSMutableArray in_array)
	{
		if (in_array != null && in_array.count() > 0){
			_m_trinomes_array = in_array;
			
			length = in_array.count();
			
			/* Ordonner le tableau avant traitement */
			orderArray();

		}
	}
	
	
	/**
	 * Renvoie le trinome � l'index sp�cifi�
	 * @param in_index
	 * @return
	 */
	public Trinome tupleAtIndex(int in_index)
	{
		return (Trinome)_m_trinomes_array.objectAtIndex(in_index);
	}
	
	
	/**
	 * Ordonner le tableau.<br>
	 * Comparaison trinome par trinome @see Trinome.TupleComparator.
	 */
	public void orderArray()
	{
		try
		{
			_m_trinomes_array.sortUsingComparator(new Trinome.Comparator());

			_createObjectsArrays();
			
		}
		catch(NSComparator.ComparisonException e)
		{
			NSLog.out.appendln("Comparaison impossible.");
		}
		
	}
	
	
	/**
	 * Creation des tableaux d'objets x, y et z.
	 */
	private void _createObjectsArrays()
	{
		int					tmp_index;
		int					tmp_x_class_index;
		Trinome.Comparator 	tmp_comparator = new Trinome.Comparator();

		m_x_objects = new Trinome.DataObject[length];
		m_y_objects = new Trinome.DataObject[length];
		m_z_objects = new Trinome.DataObject[length];

		m_min_x = ((Trinome)_m_trinomes_array.objectAtIndex(0)).m_x_value;
		m_min_y = ((Trinome)_m_trinomes_array.objectAtIndex(0)).m_y_value;
		m_min_z = ((Trinome)_m_trinomes_array.objectAtIndex(0)).m_z_value;
		m_max_x = m_min_x;
		m_max_y = m_min_y;
		m_max_z = m_min_z;
		
		m_total_x = new Double(0);
		m_total_y = new Double(0);
		m_total_z = new Double(0);
		
		m_x_categories = new NSMutableArray();
		m_y_categories = new NSMutableArray();
		m_z_categories = new NSMutableArray();
		
		m_total_y_by_x_class = new double[length];
		
		for (tmp_index = 0; tmp_index < length; tmp_index++)
		{
			m_x_objects[tmp_index] = ((Trinome)_m_trinomes_array.objectAtIndex(tmp_index)).m_x_value;
			m_y_objects[tmp_index] = ((Trinome)_m_trinomes_array.objectAtIndex(tmp_index)).m_y_value;
			m_z_objects[tmp_index] = ((Trinome)_m_trinomes_array.objectAtIndex(tmp_index)).m_z_value;
			
			tmp_x_class_index = m_x_categories.indexOfObject(m_x_objects[tmp_index]);

			/* V�rifier si X est une cat�gorie distincte */
		    if (m_x_objects[tmp_index] != null && tmp_x_class_index < 0)
		    {
		        m_x_categories.addObject(m_x_objects[tmp_index]);
		        tmp_x_class_index = m_x_categories.count() - 1;
		    }
			
			/* V�rifier si Y est une cat�gorie distincte */
		    if (m_y_objects[tmp_index] != null && m_y_categories.indexOfObject(m_y_objects[tmp_index]) < 0)
		    {
		        m_y_categories.addObject(m_y_objects[tmp_index]);
		    }
			
			/* V�rifier si Z est une cat�gorie distincte */
		    if (m_z_objects[tmp_index] != null && m_z_categories.indexOfObject(m_z_objects[tmp_index]) < 0)
		    {
		        m_z_categories.addObject(m_z_objects[tmp_index]);
		    }

			/* Calculer le plus grand X */
			if (tmp_comparator.compare(m_max_x, m_x_objects[tmp_index]) == NSComparator.OrderedAscending)
			    m_max_x = m_x_objects[tmp_index];
			
			/* Calculer le plus grand Y si les valeurs Y sont comparables */
			if (tmp_comparator.compare(m_max_y, m_y_objects[tmp_index]) == NSComparator.OrderedAscending)
				m_max_y = m_y_objects[tmp_index];
			
			/* Calculer le plus grand Z */
			if (tmp_comparator.compare(m_max_z, m_z_objects[tmp_index]) == NSComparator.OrderedAscending)
			    m_max_z = m_z_objects[tmp_index];
			
			/* Calculer le plus petit X */
			if (tmp_comparator.compare(m_x_objects[tmp_index], m_min_x) == NSComparator.OrderedAscending)
			    m_min_x = m_x_objects[tmp_index];
			
			/* Calculer le plus petit Y si les valeurs Y sont comparables */
			if (tmp_comparator.compare(m_y_objects[tmp_index], m_min_y) == NSComparator.OrderedAscending)
				m_min_y = m_y_objects[tmp_index];
			
			/* Calculer le plus petit Z */
			if (tmp_comparator.compare(m_z_objects[tmp_index], m_min_z) == NSComparator.OrderedAscending)
			    m_min_z = m_z_objects[tmp_index];
			
			/* Calculer la somme de la s�rie X si les X sont des nombres */
			if (m_x_objects[tmp_index].isNumber())
			{
				m_total_x = new Double(m_total_x.doubleValue() + ((Number)m_x_objects[tmp_index].m_data).doubleValue());
				if (!m_has_decimal_x_indic && (int)m_total_x.doubleValue() != m_total_x.doubleValue())
				    m_has_decimal_x_indic = true;
			}

			/* Calculer la somme de la s�rie Y si les Y sont des nombres */
			if (m_y_objects[tmp_index].isNumber())
			{
				if (tmp_x_class_index >= 0)
				{
					m_total_y_by_x_class[tmp_x_class_index] += ((Number)m_y_objects[tmp_index].m_data).doubleValue();
					if (m_max_total_y_by_x_class < m_total_y_by_x_class[tmp_x_class_index])
						m_max_total_y_by_x_class = m_total_y_by_x_class[tmp_x_class_index];
				}
				m_total_y = new Double(m_total_y.doubleValue() + ((Number)m_y_objects[tmp_index].m_data).doubleValue());
				if (!m_has_decimal_y_indic && (int)m_total_y.doubleValue() != m_total_y.doubleValue())
				    m_has_decimal_y_indic = true;
			}

			/* Calculer la somme de la s�rie Z si les Z sont des nombres */
			if (m_z_objects[tmp_index] != null && m_z_objects[tmp_index].isNumber())
			{
				m_total_z = new Double(m_total_z.doubleValue() + ((Number)m_z_objects[tmp_index].m_data).doubleValue());
			}
		}
		
		/* Trier les tableaux des categories distinctes X et Z */
		try
		{
			m_x_categories.sortUsingComparator(tmp_comparator);
			m_y_categories.sortUsingComparator(tmp_comparator);
			m_z_categories.sortUsingComparator(tmp_comparator);
		}
		catch(NSComparator.ComparisonException e)
		{
			NSLog.out.appendln("Comparaison impossible.");
		}
		
		/* V�rification d'int�grit� */
		if (m_z_categories.count() == 0 && m_x_categories.count() != length)
		    NSLog.out.appendln("[MOGraphe] WARNING : La s�rie de donn�es X comporte des �l�ments redondants !!");
	}
}
