//================================================================================
//CLASSE : 	Trinome
//FICHIER :	Trinome.java
//PROJET : 	MOTools
//CREE PAR : EJ
//
//RESUME : Gestion d'un trinome : groupe de 3 valeurs
//
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			19 ao�t 2004		EJ		Cr�ation du fichier.
//================================================================================

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.webobjects.foundation.NSComparator;

/**
 * Classe de d�finition d'un trinome<br>
 * Un trinome est un groupe de trois valeurs X, Y et Z.
 */
public class Trinome implements Comparable
{
    /**
     * Valeur X
     */
	public DataObject	m_x_value;
	
	/**
	 * Valeur Y
	 */
	public DataObject	m_y_value;
	
	/**
	 * Valeur Z
	 */
	public DataObject	m_z_value;

	/**
	 * Classe de gestion d'une des trois valeurs dans un trinome
	 */
	public static class	DataObject extends Object implements Comparable
	{
	    
	    protected Object m_data = null;
	    
	    /**
	     * Constructeur
	     * @param in_object
	     */
	    public DataObject(Object in_object)
	    {
	        m_data = in_object;
	    }
	    
	    
	    /**
	     * @return chaine
	     */
	    public static String toString(Object in_object)
	    {
			if (in_object instanceof Number)
			{
			    return NumberFormat.getNumberInstance(Locale.FRANCE).format(in_object);
			}
			else if (in_object instanceof Date)
			{
			    return DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format((Date)in_object);
			}
			else
			{
			    return  in_object.toString();
			}			
	    }

	    
	    /**
	     * 
	     * @return
	     */
	    public Double parseDouble()
	    {
			if (m_data instanceof Number)
			{
			    return new Double(((Double)m_data).doubleValue());
			}
			else if (m_data instanceof Date)
			{
			    return new Double(((Date)m_data).getTime());
			}
			else
			{
			    return  null;
			}			
	    }

	    
	    /**
	     * @return chaine
	     */
	    public String toString()
	    {
			return toString(m_data);
	    }

	    
	    /**
	     * Donner l'egalit� entre une donn�e et un objet
	     */
	    public boolean equals(Object in_object)
	    {
	        if (in_object instanceof DataObject)
	            return m_data.equals(((DataObject)in_object).m_data);
	        else
	            return m_data.equals(in_object);
	    }


		/**
		 * Comparer la donn�e � un objet
		 */
		public int compareTo(Object in_object)
		{
		    if (in_object instanceof DataObject)
		        return compareTo((DataObject)in_object);
		    else
				return ((Comparable)m_data).compareTo(in_object);
		}

	
		/**
		 * Comparer la donn�e � une autre donn�e
		 */
		public int compareTo(DataObject in_object)
		{
			return ((Comparable)m_data).compareTo(in_object.m_data);
		}
		
		
		public boolean isNumber(){return m_data instanceof Number;}
		public boolean isDate(){return m_data instanceof Date;}
		public boolean isString(){return m_data instanceof String;}
}
	
	/**
	 * 
	 * Classe permettant d'ordonner une liste de trinome
	 */
	public static class Comparator extends NSComparator
	{
		/**
		 * Comparer deux objets
		 */
		public int compare(Object in_first_object, Object in_sec_object)
		{
		    if (in_first_object != null)
		    {
		        if (in_sec_object == null)	return NSComparator.OrderedDescending;
		    }
		    else if (in_sec_object == null)
		        return NSComparator.OrderedSame;
		    else
		        return NSComparator.OrderedAscending;
		            
			if (in_first_object instanceof Trinome && in_sec_object instanceof Trinome)
			{
				return compare((Trinome)in_first_object, (Trinome)in_sec_object);
			}
			
			return _compareObjects((Comparable)in_first_object, (Comparable)in_sec_object);	
		}
		
		/**
		 * Comparer deux trinomes
		 * @param in_first_trinome
		 * @param in_sec_trinome
		 * @return
		 */
		public int compare(Trinome in_first_trinome, Trinome in_sec_trinome)
		{
			if (in_first_trinome != null)
				if (in_sec_trinome != null)return in_first_trinome.compareTo(in_sec_trinome);
				else return OrderedDescending;
			else if (in_sec_trinome != null) return OrderedAscending;
				else return OrderedSame;
		}
		
		
		/**
		 * D�finir l'�galit� de deux objets
		 * @param in_first_object
		 * @param in_sec_object
		 * @return
		 */
		static public boolean areEquals(Object in_first_object, Object in_sec_object)
		{
			return (in_first_object == in_sec_object
				|| (in_first_object != null && in_sec_object != null && in_first_object.equals(in_sec_object)));
		}
	}


	/**
	 * Constructeur
	 * @param in_values_array	tableau de valeurs
	 */
	public Trinome(double in_values_array[])
	{
		if (in_values_array != null)
		{
			switch (in_values_array.length)
			{
				case 0 :
					break;
				case 1 :
					m_x_value = new DataObject(new Double(in_values_array[0]));
					break;
				case 2 :
					m_x_value = new DataObject(new Double(in_values_array[0]));
					m_y_value = new DataObject(new Double(in_values_array[1]));
					break;
				default :
					m_x_value = new DataObject(new Double(in_values_array[0]));
					m_y_value = new DataObject(new Double(in_values_array[1]));
					m_z_value = new DataObject(new Double(in_values_array[2]));
					break;
			}
		}
	}

	
	/**
	 * Constructeur
	 * @param in_number_1	valeur X
	 * @param in_number_2	valeur Y
	 */
	public Trinome(double in_number_1, double in_number_2)
	{
		this(new double[]{in_number_1, in_number_2});
	}

	/**
	 * Constructeur
	 * @param in_number	valeur X
	 * @param in_object	valeur Y
	 */
	public Trinome(double in_number, Object in_object)
	{
		this(new Double(in_number), in_object);
	}

	/**
	 * Constructeur
	 * @param in_object	valeur X
	 * @param in_number	valeur Y
	 */
	public Trinome(Comparable in_object, double in_number)
	{
		this(in_object, new Double(in_number));
	}

	/**
	 * Constructeur
	 * @param in_x_value	valeur X
	 * @param in_y_value	valeur Y
	 */
	public Trinome(Comparable in_x_value, Object in_y_value)
	{
		m_x_value = new DataObject(in_x_value);
		m_y_value = new DataObject(in_y_value);
		m_z_value = null;
	}

	/**
	 * Constructeur
	 * @param in_x_value	valeur X
	 * @param in_y_value	valeur Y
	 * @param in_z_value	valeur Z
	 */
	public Trinome(Comparable in_x_value, Object in_y_value, Comparable in_z_value)
	{
		m_x_value = new DataObject(in_x_value);
		m_y_value = new DataObject(in_y_value);
		m_z_value = new DataObject(in_z_value);
	}


	/**
	 * Constructeur
	 * @param in_x_value	valeur X
	 * @param in_y_value	valeur Y
	 * @param in_z_value	valeur Z
	 */
	public Trinome(Comparable in_x_value, double in_y_value, Comparable in_z_value)
	{
		this(in_x_value, new Double(in_y_value), in_z_value);
	}


	/**
	 * Comparer le trinome � un autre objet
	 */
	public int compareTo(Object in_object){
		return compareTo((Trinome)in_object);
	}

	/**
	 * Comparer le trinome � un autre trinome
	 * @param in_tuple	un tuple
	 * @return
	 */
	public int compareTo(Trinome in_tuple)
	{
		int	result = Comparator.OrderedSame;
		
		/* Comparer les valeurs X en premier, puis Z si necessaire */
		if (m_x_value != null)
		{
			if (in_tuple.m_x_value != null)
			{
				if ((result = m_x_value.compareTo(in_tuple.m_x_value)) == 0){
					result = m_z_value.compareTo(in_tuple.m_z_value);
				}
			}
			else
			{
				return Comparator.OrderedDescending;
			}
		}
		else
		{
			if (in_tuple.m_x_value != null)
			{
				return Comparator.OrderedAscending;
			}
			else
			{
				result = m_z_value.compareTo(in_tuple.m_z_value);
			}
		}
		
		return result;
	}
}

