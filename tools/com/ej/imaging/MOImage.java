//================================================================================
//CLASSE : 	MOImage
//FICHIER :	MOImage.java
//
//RESUME : Classe abstraite de g�n�ration d'images
//
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			16 mai 03	EJ		Cr�ation du fichier.
//================================================================================
package com.mediware.MOFoundation.MOControls;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Paint;
import java.util.Iterator;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.TextHitInfo;
import java.awt.font.TextLayout;
import java.awt.geom.Arc2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.spi.ImageWriterSpi;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;

/**
 * Sous-classer cette classe pour r�aliser des traitements d'images.
 * MOImage regroupe des propri�t�s redondantes des images :
 * 		<ul><li>largeur et hauteur</li>
 * 		<li>couleur de fond</li>
 * 		<li>couleur de crayon</li>
 * 		<li>transparence</li>
 * 		<li>format d'image</li><ul>
 * MOImage offre les fonctionnalit�s de bases suivantes :
 * 		<ul><li>sauvegarder l'image sur disque</li>
 * 		<li>mettre en m�moire l'image dans le cache de l'application</li>
 * 		<li>optimiser le rendu soit pour la qualit� soit pour la vitesse de traitement</li>
 */
public abstract class MOImage
{
 	// --------------------------------------------------------------
	// CONSTANTES
	// --------------------------------------------------------------

   /**
     * Liste des polices syst�mes disponibles
     */
    public static final Font[]		AVAILABLES_FONTS = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();
        
    /**
     * Formats possibles de l'image
     */
    public static final String[]	AVAILABLES_IMAGE_FORMATS = ImageIO.getWriterFormatNames();
    

	// --------------------------------------------------------------
	// VARIABLES
	// --------------------------------------------------------------

    /* Compteur pour les images stock�es via le Resource Manager */
    private static long				_m_rm_image_index = 0;
    

	/* Objet image */
	protected BufferedImage			_m_BufferedImage;
	
	/* Contexte graphique */
	protected Graphics2D			_m_Graphics2D;

	/* Largeur et hauteur de l'image */
	protected	double				_m_width;
	protected	double				_m_height;

	/* Format de l'image */
	protected String				_m_image_format_str;

	/* Couleur de fond */
	protected Color					_m_background_Color;
	
	/* Couleur du crayon */
	protected Color					_m_pencil_Color;
	
	/* Indicateur de transparence */
	protected boolean				_m_transparency_indic;	
	
	/* Indicateur de rendu optimis� pour la vitesse */
	protected boolean				_m_rendering_speed_indic = true;
	
	
    /**
     * Constructeur
     */
    protected MOImage()
	{
        super();

        _m_image_format_str = "PNG";
        _m_background_Color = Color.WHITE;
        _m_pencil_Color = Color.BLACK;
        _m_transparency_indic = false;
    }

    
	/**
	* Constructeur secondaire.
	*/
	public MOImage(double in_width, double in_height)
	{
	    this();

        setImageSize(in_width, in_height);
	}
	
	
	/**
	 * Cr�er l'objet image
	 */
	protected void _createImage ()
	{
		/* Cr�er l'image */
	    /* G�rer la transparence alpha uniquement pour les images au format GIF et PNG */
	    if (cantBeTransparent())
	        _m_BufferedImage = new BufferedImage((int)_m_width, (int)_m_height, BufferedImage.TYPE_INT_RGB);
	    else
	        _m_BufferedImage = new BufferedImage((int)_m_width, (int)_m_height, BufferedImage.TYPE_INT_ARGB);
	        
		/* Initialiser le contexte graphique */
		_initGraphicContext();
	}

	
	/**
	 * Initialiser le contexte graphique
	 */
	protected void _initGraphicContext ()
	{
		/* Lib�rer les ressources utilis�es par l'ancien contexte graphique */
		if (_m_Graphics2D != null)	_m_Graphics2D.finalize();
		
		/* Cr�er un nouveau contexte graphique de travail */
		_m_Graphics2D = _m_BufferedImage.createGraphics();
				
		/* Optimisation du rendu */
		if (_m_rendering_speed_indic)
		    setRenderingForSpeed();
		else
		    setRenderingForQuality();
		
		/* Initialiser la couleur de fond */
		_m_background_Color = new Color(_m_background_Color.getRed(),
				_m_background_Color.getGreen(),
				_m_background_Color.getBlue(),
				_m_transparency_indic ? 0x00 : 0xFF);
		
		/* Effacer avec la couleur de fond */
		clear();
	}

	
	/**
	 * Dessiner l'image
	 */
	public abstract void draw();
	

	/**
	 * Effacer le contenu de l'image
	 */
	public void clear ()
	{
		Composite	tmp_composite = _m_Graphics2D.getComposite();

		/* Effacer l'image avec la couleur de fond */
		_m_Graphics2D.setColor(_m_background_Color);
		if (_m_transparency_indic) {
			_m_Graphics2D.setComposite(AlphaComposite.Clear);
		}

		_m_Graphics2D.fill(new Rectangle2D.Double(0, 0, _m_width, _m_height));
		_m_Graphics2D.setComposite(tmp_composite);
	}
	
	
	/**
	 * Liste des polices syst�mes disponibles
	 * @return
	 */
	public NSArray availablesFonts()
	{
	    return new NSArray(AVAILABLES_FONTS);
	}
	

	/**
	 * Renvoie l'objet image
	 * @return
	 */
	public BufferedImage getImage ()
	{
		return _m_BufferedImage;
	}
	
	
	/**
	 * Renvoie les donn�es de l'image
	 * @return
	 */
	public byte[] getImageData()
	{
		ByteArrayOutputStream	tmp_stream = new ByteArrayOutputStream();
		byte[]					tmp_bytes_array = null;

		try
		{
		    /* Utiliser l'encoder Acme pour les gifs */
		    if (_m_image_format_str.toUpperCase().indexOf("GIF") >= 0)
		    {
				/*
		        new GifEncoderNoCM (new FilteredImageSource (_m_BufferedImage.getSource (),
		                									new Web216ColorsFilter ()),
		                									tmp_stream).encode ();
		        tmp_bytes_array = tmp_stream.toByteArray();
				 */
		        
				/* Pour les autres formats utiliser le standard javax */
		    }
		    else if (ImageIO.write(_m_BufferedImage, _m_image_format_str, tmp_stream))
		    {
				tmp_bytes_array = tmp_stream.toByteArray();
			}
		    
	        tmp_stream.close ();

		}
		catch (IOException e)
		{
		    _initGraphicContext();
		}
		
	    return tmp_bytes_array;
	}
	

	/**
	 * Sauvegarder l'image sur le disque
	 * @param in_image_filename : nom du fichier destination
	 * @throws Exception
	 */
	public void saveAs(String in_image_filename)
	{
	    OutputStream	tmp_image_file = null;
	    
		if (in_image_filename != null && in_image_filename.length() > 0) {
			try{
		        if (in_image_filename.toLowerCase().endsWith("." + _m_image_format_str.toLowerCase()))
			        tmp_image_file = new FileOutputStream(in_image_filename);
			    else
			        tmp_image_file = new FileOutputStream(in_image_filename + "." + _m_image_format_str.toLowerCase());

		        /* Utiliser l'encoder Acme pour les gifs */
			    if (_m_image_format_str.toUpperCase().indexOf("GIF") >= 0) {
					/*
			        new GifEncoderNoCM (new FilteredImageSource (_m_BufferedImage.getSource(),
			                								new Web216ColorsFilter()),
			                								tmp_image_file).encode();
					 */
			    }
				else {
				    /* Pour les autres formats utiliser le standard javax */
					ImageIO.write(_m_BufferedImage, _m_image_format_str, tmp_image_file);
			    }
		        tmp_image_file.close ();
			}
			catch(Exception e) {
			    _initGraphicContext();
				NSLog.out.appendln("L'image '" + in_image_filename + "' n'as pu �tre cr��e correctement.");
			}
		}
	}

	
	/**
	 * Mettre l'image dans le cache des ressources de l'application (voir {@link com.webobjects.appserver.WOResourceManager})
	 * @param in_context	contexte
	 * @param in_images_name_array	tableau de noms d'images (utilis� ult�rieurement pour vider le cache).
	 * @return url de l'image cach�e
	 */
	public String urlFromRessourceManager (WOContext in_context, NSMutableArray in_images_name_array)
	{
	    WOApplication		tmp_wo_app = in_context.component().application();
        byte				tmp_img_bytes[] = getImageData();
        String				tmp_img_key = _m_rm_image_index++ + "." + _m_image_format_str;

        if (tmp_img_bytes != null)
        {
            tmp_wo_app.resourceManager().setData(new NSData(tmp_img_bytes), tmp_img_key, _m_image_format_str, in_context.session());

            if (in_images_name_array != null) in_images_name_array.addObject(tmp_img_key);
            
            // return "http://localhost" + in_context.urlWithRequestHandlerKey("wr", null, "wodata=" + tmp_img_key);
			return  "http://" + tmp_wo_app.host() + in_context.urlWithRequestHandlerKey("wr", null, "wodata=" + tmp_img_key);
        }

        return null;
	}
		

	/**
	 * D�finir les dimensions de l'image
	 * @param in_width : largeur
	 * @param in_height : hauteur
	 */
	public void setImageSize(double in_width, double in_height)
	{
	    _m_width = in_width;
	    _m_height = in_height;
	    
	    _createImage();
	}
	
	
	/**
	 * D�finir les couleurs de crayon et du fond
	 * @param in_pencil_color	couleur de dessin du graphique
	 * @param in_bg_color	couleur de fond
	 * @return <code>true</code> si les couleurs d�finies ne rentrent pas en conflit
	 */
	public boolean setColors(Color in_pencil_color, Color in_bg_color)
	{
		Color tmp_pencil_color;
		Color tmp_bg_color;
		
		/* Couleur du graphique */
		if (in_pencil_color != null)
		    tmp_pencil_color = in_pencil_color;
		else
		    tmp_pencil_color = _m_pencil_Color;

		/* Couleur du fond */
		if (in_bg_color != null)
			tmp_bg_color = in_bg_color;
		else
			tmp_bg_color = _m_background_Color;
		
		/* V�rification */
		if (!tmp_pencil_color.equals(tmp_bg_color))
		{
			/* Changer le fond */
			if (!_m_background_Color.equals(tmp_bg_color))
			{
			    _m_background_Color = tmp_bg_color;
				clear();
			}
			
			_m_pencil_Color = tmp_pencil_color;
			
			return true;
		}
		
		return false;
	}

	
	/**
	 * G�n�rer le dessin d'un texte selon une longueur maxi de pixels.<br>
	 * La longueur est calcul�e de gauche � droite horizontalement.
	 * @param in_str : texte � dessiner
	 * @param in_max_length : nb de pixels � ne pas d�passer (si n�gatif pas de limite)
	 * @return
	 */
	public static TextLayout layoutFor(FontRenderContext in_font_render, String in_str, Font in_font, float in_max_length)
	{
	    String			tmp_result_buff = null;
	    TextLayout		tmp_str_layout = null;
	    TextHitInfo		tmp_hit_info = null;
	    char[]			tmp_chars = null;
	    int				ii;
	    
		if (in_str == null || in_str.length() == 0)
		    tmp_chars = new char[]{'(', 'v', 'i', 'd', 'e', ')'};
		else
		    tmp_chars = in_str.toCharArray();
		
		/* V�rifier la validit� de la police d'affichage pour chacun des caract�res */
		for (ii = 0; ii < tmp_chars.length; ii++)
		{
		    if (!in_font.canDisplay(tmp_chars[ii]))
		    {
		        switch (tmp_chars[ii])
		        {
	        		case '�':
	        		case '�':
	        		case '�':
	        		    tmp_chars[ii] = 'a';
	        		    break;
	        		    
		        	case '�':
		        	case '�':
		        	case '�':
		        	case '�':
		        	    tmp_chars[ii] = 'e';
	        		    break;
	        		    
		        	case '�':
		        	case '�':
		        	case '�':
		        	    tmp_chars[ii] = 'i';
	        		    break;
	        		    
		        	case '�':
		        	case '�':
		        	case '�':
		        	    tmp_chars[ii] = 'o';
		        	    break;
		        	    
		        	case '�':
		        	case '�':
		        	case '�':
		        	    tmp_chars[ii] = 'u';
		        	    break;
		        	    
		        	case '�':
		        	    tmp_chars[ii] = 'y';
		        	    break;
		        	    
		        	default:
		        	    tmp_chars[ii] = '\0';
		        }
		        
		        if (!in_font.canDisplay(tmp_chars[ii])) tmp_chars[ii] = '\0';
		    }
		}
	
		/* Cr�er un premier dessin du texte */
		tmp_str_layout = new TextLayout(String.valueOf(tmp_chars), in_font, in_font_render);
		if (in_max_length < 0) return tmp_str_layout;
		
		/* Calculer � quelle position du texte la longueur maxi tombe */
		tmp_hit_info = tmp_str_layout.hitTestChar(in_max_length, 0f);
		if (tmp_hit_info != null && tmp_hit_info.getCharIndex() < tmp_chars.length)
		{
		    if (tmp_hit_info.getCharIndex() > 3 && in_font.canDisplay('.'))
		        tmp_result_buff = new String(tmp_chars, 0, tmp_hit_info.getCharIndex() - 1) + ".";
		    else
		        tmp_result_buff = new String(tmp_chars, 0, tmp_hit_info.getCharIndex());
		}
		else
			tmp_result_buff = new String(tmp_chars);
		
		if (tmp_result_buff.length() > 0)
		    return new TextLayout(tmp_result_buff, in_font, in_font_render);
		else
		    return null;	
	}

	
	/**
	 * Cr�e un objet de classe <code>Color</code> � partir de sa valeur javascript
	 * @param in_js_color : couleur javascript (i.e : #FF0000 => rouge)
	 * @return un objet de classe <code>Color</code>
	 */
	public static Color jsColorToJavaColor(String in_js_color)
	{
		int tmp_red_index = 0;
		int tmp_green_index = 0;
		int tmp_blue_index = 0;
		int	tmp_str_index = 0;
		
		if (in_js_color != null && in_js_color.length() > 5)
		{
			if (in_js_color.charAt(0) == '#') tmp_str_index = 1;
			
			tmp_red_index = Integer.parseInt(in_js_color.substring(tmp_str_index, tmp_str_index += 2), 16);
			tmp_green_index = Integer.parseInt(in_js_color.substring(tmp_str_index, tmp_str_index += 2), 16);
			tmp_blue_index = Integer.parseInt(in_js_color.substring(tmp_str_index, tmp_str_index += 2), 16);
		
			return new Color(tmp_red_index, tmp_green_index, tmp_blue_index);
		}
		
		return null;
	}

	
	/**
	 * Renvoie une couleur javascript � partir d'une couleur java
	 * @param in_color : couleur java (i.e : <code>Color.RED</code>)
	 * @return chaine representant la couleur au format "#xxxxxx"
	 */
	public static String javaColorToJsColor(Color in_color)
	{
		String	tmp_red = Integer.toString(in_color.getRed(), 16);
		String	tmp_green = Integer.toString(in_color.getGreen(), 16);
		String	tmp_blue = Integer.toString(in_color.getBlue(), 16);
		
		return "#" + (tmp_red.length() == 1 ? "0" + tmp_red : tmp_red) 
					+ (tmp_green.length() == 1 ? "0" + tmp_green : tmp_green)
					+ (tmp_blue.length() == 1 ? "0" + tmp_blue : tmp_blue);
	}
	
	
	/**
	 * Calculer les coordonn�es X et Y du centre d'un triangle de sommets A, B, C
	 * @param x0 : abcisse du point A
	 * @param y0 : ordonn�e du point A
	 * @param x1 : abcisse du point B
	 * @param y1 : ordonn�e du point B
	 * @param x2 : abcisse du point C
	 * @param y2 : ordonn�e du point C
	 * @return
	 */
	public static Point2D centerOf(double x0, double y0, double x1, double y1, double x2, double y2)
	{
	    double	X = 0, Y = 0;
	    double	x01 = x0 - (x0 -x1)/2, y01 = y0 - (y0 - y1)/2; // Centre du segment AB
	    double	x12 = x1 - (x1 -x2)/2, y12 = y1 - (y1 - y2)/2; // Centre du segment BC
	    
	    /* Trajectoire de la bissectrice de AB de la forme y = m0 * x + p0 */
	    double	m0 = (y12 - y0)/(x12 - x0);
	    double	p0 = y0 - m0 * x0;
	    
	    /* Trajectoire de la bissectrice de AC de la forme y = m1 * x + p1 */
	    double	m1 = (y01 - y2)/(x01 - x2);
	    double	p1 = y2 - m1 * x2;
	    
	    /* L'intersection des deux bissectrices est le centre du triangle */
	    X = (p1 - p0)/(m0 - m1);
	    Y = m0 * X + p0;
	        
	    return new Point2D.Double(X, Y);
	}
	
	
	/**
	 * Calculer les coordonn�es du centre entre deux points A et B
	 * @param x0 : abcisse du point A
	 * @param y0 : ordonn�e du point A
	 * @param x1 : abcisse du point B
	 * @param y1 : ordonn�e du point B
	 * @return
	 */
	public static Point2D centerOf(double x0, double y0, double x1, double y1)
	{
	    return translateOf(x0, y0, x1, y1, 0.5);
	}
	
	
	/**
	 * Calculer les coordonn�es du point r�sultant de la translation du point A vers le point B,<br>
	 * et d�pendant de <code>in_ratio</code>.
	 * @param x0 : abcisse du point A
	 * @param y0 : ordonn�e du point A
	 * @param x1 : abcisse du point B
	 * @param y1 : ordonn�e du point B
	 * @param in_ratio : le ratio de translation de A->B(ex : 0.5 pour obtenir les coordonn�es du centre de A et B)
	 * @return
	 */
	public static Point2D translateOf(double x0, double y0, double x1, double y1, double in_ratio)
	{
	    return new Point2D.Double(x0 - (x0 - x1) * in_ratio, y0 - (y0 - y1) * in_ratio);
	}
	
	
	/**
	 * Dessiner un rectangle 3D
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 * @param raised
	 */
	protected void draw3DRect(double x, double y, double width, double height, boolean raised)
	{
		Paint p = _m_Graphics2D.getPaint();
		Color c = _m_Graphics2D.getColor();
		Color brighter = c.brighter();
		Color darker = c.darker();
	
		_m_Graphics2D.setColor(raised ? brighter : darker);
		//drawLine(x, y, x, y + height);
		_m_Graphics2D.fill(new Rectangle2D.Double(x, y, 1, height + 1));
		//drawLine(x + 1, y, x + width - 1, y);
		_m_Graphics2D.fill(new Rectangle2D.Double(x + 1, y, width - 1, 1));
		_m_Graphics2D.setColor(raised ? darker : brighter);
		//drawLine(x + 1, y + height, x + width, y + height);
		_m_Graphics2D.fill(new Rectangle2D.Double(x + 1, y + height, width, 1));
		//drawLine(x + width, y, x + width, y + height - 1);
		_m_Graphics2D.fill(new Rectangle2D.Double(x + width, y, 1, height));
		_m_Graphics2D.setPaint(p);
	}    


	/**
	 * 
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	protected void fill3DRect(double x, double y, double width, double height, double in_x_incline, double in_y_incline)
	{
		Paint p = _m_Graphics2D.getPaint();
		Color c = _m_Graphics2D.getColor();
		Color brighter = c.brighter();
		Color darker = c.darker();
		double arcw = (width <= 10 ? 0 : 10), arch = (height <= 10 ? 0 : 10);
		
		/* Desssiner un rectangle arrondi */
		_m_Graphics2D.fill(new RoundRectangle2D.Double(x+1, y+1, width-2, height-2, arcw, arch));
		_m_Graphics2D.fill(new Rectangle2D.Double(x+1, y+height-arch/2, width-2, arch/2));

		/* Dessiner les ombres claires */
		_m_Graphics2D.setColor(brighter);
		/* Ligne claire de gauche */
		_m_Graphics2D.draw(new Line2D.Double(x + 1, y + arch/2, x + 1, y + height - 1));
		/* Arc clair gauche sup�rieur */
		_m_Graphics2D.draw(new Arc2D.Double(x + 1, y + 1, arcw, arch, 90, 90, Arc2D.OPEN));
		/* Ligne claire sup�rieure */
		_m_Graphics2D.draw(new Line2D.Double(x + arcw/2, y, x + width - arcw/2, y));
		/* Arc clair droit sup�rieur*/
		_m_Graphics2D.draw(new Arc2D.Double(x + width - arcw - 1, y + 1, arcw, arch, 0, 90, Arc2D.OPEN));
		/* Dessiner les ombres sombres */
		_m_Graphics2D.setColor(darker);
		/* Ligne sombre inf�rieure */
		_m_Graphics2D.draw(new Line2D.Double(x + 1, y + height - 1, x + width - 1, y + height - 1));
		/* Ligne sombre de droite */
		_m_Graphics2D.draw(new Line2D.Double(x + width - 1, y + arch/2, x + width - 1, y + height - 1));
		_m_Graphics2D.setPaint(p);
	}
	
		
	/**
	 * R�cup�rer la couleur de fond
	 * @return	cha�ne d�finissant la couleur au format javascript (ex:#000000)
	 */
	public String getBackgroundColor()
	{
		return javaColorToJsColor(_m_background_Color);
	}
	
	
	/**
	 * D�finir la couleur de fond
	 * @param in_js_color	cha�ne d�finissant la couleur au format javascript (ex:#000000)
	 */
	public void setBackgroundColor(String in_js_color)
	{
		setColors(null, jsColorToJavaColor(in_js_color));
	}

	
	/**
	 * R�cup�rer la largeur de l'image
	 * @return
	 */
	public double getImageWidth()
	{
		return _m_width;
	}
	
	
	/**
	 * R�cup�rer la hauteur de l'image
	 * @return
	 */
	public double getImageHeight()
	{
		return _m_height;
	}
	
	
	/**
	 * D�finir la largeur de l'image
	 * @return
	 */
	public void setImageWidth(double in_width) {setImageSize(in_width, _m_height);}
	
	
	/**
	 * D�finir la hauteur de l'image
	 * @return
	 */
	public void setImageHeight(double in_height)
	{
		setImageSize(_m_width, in_height);
	}

	
	/**
	 * Indiquer si l'image NE peut PAS �tre transparente
	 * @return <code>true</code> ou <code>false</code>
	 */
	public boolean cantBeTransparent()
	{
		return !(_m_image_format_str.toUpperCase().indexOf("PNG") >= 0 || _m_image_format_str.toUpperCase().indexOf("GIF") >= 0);
	}
	
	
	/**
	 * L'image est-elle transparente ?
	 * @return	<code>true</code> ou <code>false</code>
	 */
	public boolean getTransparency()
	{
		return _m_transparency_indic;
	}


	/**
	 * D�finir la transparence de l'image
	 * @param in_transparency	indicateur <code>true</code> ou <code>false</code>
	 */
	public void setTransparency(boolean in_transparency)
	{
	    boolean tmp_transparency_indic = in_transparency;
		
		if (cantBeTransparent()) {
		    tmp_transparency_indic = false;
		}

		if (tmp_transparency_indic != _m_transparency_indic) {
			_m_transparency_indic = tmp_transparency_indic;
			clear();			
		}
	}


	/**
	 * R�cup�rer le type MIME de l'image
	 * @return	cha�ne de la forme "image/png"
	 */
	public String getMimeType()
	{
	    Iterator		tmp_wr_iter;
	    ImageWriter		tmp_img_wr;
	    ImageWriterSpi	tmp_img_wr_spi;
	    String			tmp_mime_types[];
	    
        tmp_wr_iter = ImageIO.getImageWritersByFormatName(_m_image_format_str);
        while (tmp_wr_iter.hasNext())
        {
            tmp_img_wr = (ImageWriter)tmp_wr_iter.next();
            if (tmp_img_wr != null)
            {
                tmp_img_wr_spi = tmp_img_wr.getOriginatingProvider();
                if (tmp_img_wr_spi != null)
                {
                    tmp_mime_types = tmp_img_wr_spi.getMIMETypes();
                    if (tmp_mime_types != null && tmp_mime_types.length > 0) {
                        return tmp_mime_types[0];
                    }
                }
            }
        }
        
        return null;
	}
	
	
	/**
	 * Am�liorer la qualit� du rendu de l'image (au d�triment de la vitesse)
	 */
	public void setRenderingForQuality()
	{
	    _m_rendering_speed_indic = false;
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);

	}

	
	/**
	 * Am�liorer la vitesse de rendu de l'image (au d�triment de la qualit�)
	 */
	public void setRenderingForSpeed()
	{
	    _m_rendering_speed_indic = true;
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
		_m_Graphics2D.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
	}
	
	
	/**
	 * R�cup�rer le format de l'image
	 * @return	cha�ne de la forme "PNG"
	 */
	public String getFileFormat()
	{
		return _m_image_format_str;
	}
	
	
	/**
	 * D�finir le format de l'image bas� sur les formats disponibles
	 * @param in_file_format	ch�ne de la forme "PNG"
	 */
	public void setFileFormat(String in_file_format)
	{
	    if (_m_image_format_str == null || (in_file_format != null && _m_image_format_str.compareTo(in_file_format) != 0))
	    {
		    for (int tmpIndex = 0; tmpIndex < AVAILABLES_IMAGE_FORMATS.length; tmpIndex++) {
		        if (AVAILABLES_IMAGE_FORMATS[tmpIndex].equals(in_file_format)) {
		            _m_image_format_str = in_file_format;
		            if (cantBeTransparent()) _m_transparency_indic = false;
		                
		            _createImage();
		            break;
		        }
		    }
	    }
	}


	/**
	 * Chercher un caract�re dans un tableau
	 * @param in_search_str	caract�re � chercher
	 * @param in_str_array	tableau de caract�res
	 * @param in_start_index index de d�part de la recherche
	 * @return index du caract�re danbs le tableau ou -1 si le caract�re n'a pas �t� trouv�.
	 */
	public static int indexOfChar(char in_search_str, char[] in_str_array, int in_start_index)
	{
	    int ii;
	    
	    for (ii = in_start_index; ii < in_str_array.length; ii++)	if (in_str_array[ii] == in_search_str)	return ii;

	    return -1;
	}


	/**
	 * Cr�er un sous-tableau
	 * @param in_char_array	tableau d'origine
	 * @param in_start_index	position de d�but de copie
	 * @param in_end_index	position de fin de copie
	 * @return	le sous-tableau
	 */
	public static char[] subCharArray(char[] in_char_array, int in_start_index, int in_end_index)
	{
	    int		ii;
	    char[]	tmp_char_array = new char[in_end_index - in_start_index];
	    
	    for (ii = 0; ii < tmp_char_array.length; ii++)	tmp_char_array[ii] = in_char_array[ii + in_start_index];
	    
	    return tmp_char_array;
	}
}
