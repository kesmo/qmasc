//================================================================================
//CLASSE : 	MOChart
//FICHIER :	MOChart.java
//PROJET : 	MOTools
//CREE PAR : 	EJ
// 
//RESUME : G�n�ration dynamique d'un graphique
//		� partir d'une �quation math�matique ou d'une s�rie de donn�es statistique
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			15 mai 03	EJ		Cr�ation du fichier.
//================================================================================
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Paint;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;

 /**
 * Classe de gestion de graphiques statistiques.<br>
 * Diff�rents types de graphiques sont disponibles :
 * <ul><li>histogrammes</li>
 * <li>lignes polygonales</li>
 * <li>camemberts</li></ul><br>
 * Les fonctions fournies permettent la d�finition (setter) et la r�cup�ration (getter) des param�tres du graphique.<br>
 * Elles doivent �tre utilis�es afin de d�finir les diff�rents aspects du graphique dans un WOComponent.<br>
 * ----------------------------------------------------------------<br>
 * example.java (d�finir un attribut de classe de type MOChart): <br>
 * ----------------------------------------------------------------<br>
 * public class example extends WOComponent<br>
 * {<br>
 * 	[...]<br>
 * 	public MOChart m_graphique = new MOChart();<br>
 * 	[...]<br>
 * }<br>
 * ---------------------------------------------------------------<br>
 * example.wod (associer des bindings aux propri�t�s du graphique): <br>
 * ---------------------------------------------------------------<br>
 * [...]<br>
 * Image1: WOImage {<br>
 *		width = m_graphique.imageWidth;	//Appels implicites de setImageWidth et getImageWidth<br>
 *		height = m_graphique.imageHeight; // Appels implicites de setImageHeight et getImageHeight<br>
 *		mimeType = m_graphique.mimeType; // Appels implicites de setMimeType et getMimeType<br>
 *		data = m_graphique.imageData; // Appels implicites de getImageData<br>
 * }<br>
 * [...]<br>
 * <hr>
 * Pour d�finir les donn�es � dessiner, utiliser la fonction {@link #setData},<br>
 * o� le param�tre TrinomeArray n'est rien d'autres qu'un tableau de donn�es.<br>
 * Chaque �l�ment du tableau (nomm� Trinome) est constitu� de 2 ou 3 valeurs qui vont d�finir les suites de valeurs X, Y et Z du graphique.<br>
 * Exemple d'une s�rie de binome : {{"janvier", 10.5}, {"F�vrier", 12}, {"Mars", 8}, {"Mai", 9.5}}<br>
 * Exemple d'une s�rie de trinome : {{"janvier", 15.5, "David"}, {"F�vrier", 14, "David"}, {"janvier", 11, "Eric"}, {"F�vrier", 12, "Eric"}}<br>
 * Le traitement des valeurs consiste en l'ordonnancement des donn�es par cat�gorie X, Y et Z.<br>
 * Dans les exemples ci-dessus les cat�gories d�finies sont :<br>
 * Cat�gorie X d'une s�rie de binome : {"janvier", "F�vrier", "Mars", "Mai"}<br>
 * Cat�gorie Y d'une s�rie de binome : {10.5, 12, 8, 9.5}<br>
 * Cat�gorie X d'une s�rie de trinome : {"janvier", "F�vrier"}<br>
 * Cat�gorie Y d'une s�rie de trinome : {15.5, 14, 11, 12}<br>
 * Cat�gorie Z d'une s�rie de trinome : {"David", "Eric"}<br>
 * Le tableau pass� en param�tre ne doit pas forc�ment �tre ordonn�. Par contre les valeurs d'une m�me cat�gorie,<br>
 * doivent �tre de m�me type de donn�es (Integer ou String ou Double...etc).<br>
 * Pour chaque cat�gorie on peut d�finir la l�gende et la couleur de dessin :
 * <ul><li>pour la l�gende utiliser les fonctions {@link #setXLegend}, {@link #setYLegend} et {@link #setZLegend}</li>
 * <li>pour les couleurs utiliser les fonctions {@link #setGraphColor}, {@link #setGraphColors}</li></ul><br>
 * D'autres fonctions et propri�t�s permettent de modifier le rendu en fonction du type de graphique :
 * <ul><li>{@link #m_raised_histogram_indic permet de dessiner un histogramme en 3D</li>
 * <li>{@link #setGraphOpacity} pour un aspect transparent des graphiques 3D (histogramme 3D et fromage)</li>
 * <li>{@link #setCheesePortionSpace} pour espacer les diff�rentes portion d'un fromage</li>
 * <li>{@link #setInclineX} et {@link #setInclineY} pour modifier la perspective (histogramme 3D et fromage)</li>
 * <li>{@link #setTitle} pour ajouter un titre au graphique</li>
 * <li>...etc</li></ul>
 * */

public class MOChart extends MOGraphicImage implements NSKeyValueCoding.ErrorHandling
{
/*-----------------------------------------------------------------
  				CONSTANTES ET VARIABLES DE CLASSE
------------------------------------------------------------------*/
	/**
	 * Largeur du graphique par defaut
	 */
	public static final int MAX_WIDTH = 800;
	
	/**
	 * Hauteur du graphique par defaut
	 */
	public static final int MAX_HEIGHT = 600;
	
	/**
	 * Largeur du graphique par defaut
	 */
	public static final int DEFAULT_WIDTH = 480;
	
	/**
	 * Hauteur du graphique par defaut
	 */
	public static final int DEFAULT_HEIGHT = 360;
	
	/**
	 * Largeur de la bordure gauche par defaut
	 */
	public static final int DEFAULT_BORDER_LEFT = 40;
	
	/**
	 * Largeur de la bordure droite par defaut
	 */
	public static final int DEFAULT_BORDER_RIGHT = 40;
	
	/**
	 * Largeur de la bordure haute par defaut
	 */
	public static final int DEFAULT_BORDER_TOP = 40;
	
	/**
	 * Largeur de la bordure basse par defaut
	 */
	public static final int DEFAULT_BORDER_BOTTOM = 40;
	
	/**
	 * Taille de la graduation horizontale par defaut
	 */
	public static final int DEFAULT_X_STEP = 10;
	
	/**
	 * Taille de la graduation verticale par defaut
	 */
	public static final int DEFAULT_Y_STEP = 10;

	/**
	 * Graphique en histogramme (par defaut)
	 */
	public static final int GRAPH_TYPE_HISTOGRAM = 1;

	/**
	 * Graphique en polygone
	 */
	public static final int GRAPH_TYPE_POLYGON = 2;

	/**
	 * Graphique en fromage
	 */
	public static final int GRAPH_TYPE_CHEESE = 3;

	/**
	 * Alignement du texte au centre (hor. et ver.)
	 */
	public static final int TITLE_ALIGN_CENTER = 0;

	/**
	 * Alignement du texte � gauche (hor.)
	 */
	public static final int TITLE_ALIGN_LEFT = 1;

	/**
	 * Alignement du texte � droite (hor.)
	 */
	public static final int TITLE_ALIGN_RIGTH = 2;

	/**
	 * Alignement du texte en haut (ver.)
	 */
	public static final int TITLE_ALIGN_TOP = 3;

	/**
	 * Alignement du texte en bas (ver.)
	 */
	public static final int TITLE_ALIGN_BOTTOM = 4;

	/**
	 * Tableau de couleurs par d�faut
	 */
	public static final Color COLORS[] = new Color[]{Color.RED, Color.PINK, Color.WHITE, Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA};

	/**
	 * Pas de graduations en seconde
	 */
	public static final long SECOND_STEP = 1000;
	
	/**
	 * Pas de graduations en minute
	 */
	public static final long MINUTE_STEP = SECOND_STEP * 60;
	
	/**
	 * Pas de graduations en heure
	 */
	public static final long HOUR_STEP = MINUTE_STEP * 60;
	
	/**
	 * Pas de graduations en jour
	 */
	public static final long DAY_STEP = HOUR_STEP * 24;
	
	/**
	 * Pas de graduations en semaine
	 */
	public static final long WEEK_STEP = DAY_STEP * 7;
	
	/**
	 * Pas de graduations en mois
	 */
	public static final long MONTH_STEP = DAY_STEP * 30;
	
	/**
	 * Pas de graduations en ann�e
	 */
	public static final long YEAR_STEP = DAY_STEP * 365;
	
/*-----------------------------------------------------------------
						PROPRIETES PUBLIQUES
------------------------------------------------------------------*/
	/**
	 * Indicateur d'affichage de l'histogramme en relief
	 **/
	public boolean m_raised_histogram_indic = true;
	
	/**
	 * Indicateur d'affichage d'un histogramme empil�
	 */
	public boolean m_stack_histogram_indic = true;
	
	/**
	 * Indicateur d'affichage de la l�gende
	 **/
	public boolean m_show_legend = true;

	/**
	 * Indicateur de calcul automatique des tailles des bordures.<br>
	 * Si il est �gal � <code>true</code> les tailles des bordures autours du graphique sont calcul�es automatiquement<br>
	 * en fonction de la largeur du dessin et de l'affichage ou non des l�gendes.
	 **/
	public boolean m_auto_borders = true;

/*-----------------------------------------------------------------
						PROPRIETES PRIVEES
------------------------------------------------------------------*/
	
	/* Type de graphique */
	private int	_m_type = GRAPH_TYPE_HISTOGRAM;

	/* Tableau des donn�es des s�ries X, Y et Z */
	private TrinomeArray _m_trinomes_array;

	/* Listes des donn�es distinctes (classes, cat�gories) des s�ries X et Z */
	private NSArray _m_x_categories;
	private NSArray _m_y_categories;
	private NSArray _m_z_categories;
	/* Tableaux des libell�s � dessiner sur les axes pour les s�ries X, Y et Z */
	private NSArray _m_x_labels;
	private NSArray _m_y_labels;
	private NSArray _m_z_labels;
	private NSArray	_m_legend_categories;
	
	/* Valeurs maximales des s�ries X, Y et Z */
	private Trinome.DataObject _m_max_x_categories;
	private Trinome.DataObject _m_max_y_categories;
	private Trinome.DataObject _m_max_z_categories;

	/* Valeurs minimales des s�ries X, Y et Z */
	private Trinome.DataObject _m_min_x_categories;
	private Trinome.DataObject _m_min_y_categories;
	private Trinome.DataObject _m_min_z_categories;

	/* Nombres de s�ries X, Y et Z */
	private int _m_x_categories_count = 1;
	private int _m_y_categories_count = 1;
	private int _m_z_categories_count = 1;

	/* Espace entre chaque graduation verticale et horizontale */
	private	double _m_x_step = DEFAULT_X_STEP;
	private	double _m_y_step = DEFAULT_Y_STEP;

	/* Coordonn�es des points correspondants aux donn�es des s�ries X et Y */
	private double _m_x_array[];
	private double _m_y_array[];
	
	/* Tableau des mesures d'angles des portions dans un fromage */
	private double _m_angles_array[];

	/* D�calage des portions du fromage par rapport au centre */
	private double _m_delta_cheese_portion = 0;

	/* Origine du graphique dans l'image */
	private double _m_graph_origin_x = DEFAULT_BORDER_RIGHT;
	private double _m_graph_origin_y = DEFAULT_HEIGHT - DEFAULT_BORDER_BOTTOM;

	/* Largeur et hauteur du graphique */
	private double _m_graph_width = DEFAULT_WIDTH - DEFAULT_BORDER_LEFT - DEFAULT_BORDER_RIGHT;
	private double _m_graph_height = DEFAULT_HEIGHT - DEFAULT_BORDER_TOP - DEFAULT_BORDER_BOTTOM;

	/* Angle d'inclinaison vertical */
	private double _m_y_incline = 60.0;
	
	/* Angle d'inclinaison horizontal */
	private double _m_x_incline = 30.0;
	
	/* Indicateur de dessin des axes */
	private boolean _m_drawn_axes = false;
	
	/* Police d'affichage du graphique */
	private Font _m_graph_font;
	
	/* Taille de la police d'affichage du graphique */
	private float _m_graph_font_size;
	
	/* Police d'affichage du titre */
	private Font _m_title_font;
	
	/* Titre du graphique */
	private String _m_title;
	
	/* Alignement horizontal du titre (centr� par d�faut) */
	private int _m_graph_title_halign = TITLE_ALIGN_CENTER;

	/* Alignement vertical du titre (en haut par d�faut) */
	private int _m_graph_title_valign = TITLE_ALIGN_TOP;

	/* Couleur du titre (orange par d�faut) */
	private Color _m_graph_title_color = Color.ORANGE;

	/* Tableau des couleurs utilis�es */
	private Color _m_colors[] = COLORS;
	
	/* Niveau de transparence du graphique (� 80% opaque par d�faut) */
	private int _m_graph_alpha_paint_color = 0xCC;
	
	/* Indique si le trac� du graphique d�pend des abcisses
	 * lorsqu'il s'agit de donn�es de type num�rique ou date */
	private boolean _m_abciss_proportionnal_indic = false;
	
/*-----------------------------------------------------------------
							METHODES
------------------------------------------------------------------*/

	/**
	* Constructeur par d�faut.
	*/
	public MOChart()
	{
	    super(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
	

	/**
	* Constructeur secondaire.
	*/
	public MOChart(int in_width, int in_height)
	{
		super(in_width, in_height);
		
		if ((in_width > 0 && in_width <= MAX_WIDTH) || (in_height > 0 && in_height <= MAX_HEIGHT))
		{
			_m_width = in_width;
			_m_height = in_height;		
		}
		_createImage();
	}
	
	
	/**
	 * D�finir la l�gende de la s�rie de donn�es X
	 * @param in_x_objects tableau de libell�s
	 */
	public void setXLegend(String in_x_objects[])
	{
		if (in_x_objects != null && in_x_objects.length > 0)
		{
		    _m_x_labels = new NSArray(in_x_objects);
		    _m_x_categories_count = in_x_objects.length;
		}
	}
	
	
	/**
	 * D�finir la l�gende de la s�rie de donn�es Y
	 * @param in_y_objects tableau de libell�s
	 */
	public void setYLegend(String in_y_objects[])
	{
		if (in_y_objects != null && in_y_objects.length > 0)
		{
		    _m_y_labels = new NSArray(in_y_objects);
		    _m_y_categories_count = in_y_objects.length;
		}
	}
	
	
	/**
	 * D�finir la l�gende de la s�rie de donn�es Z
	 * @param in_z_objects tableau de libell�s
	 */
	public void setZLegend(String in_z_objects[])
	{
		if (in_z_objects != null && in_z_objects.length > 0)
		{
		    _m_z_labels = new NSArray(in_z_objects);
		}
	}
	
	
	/**
	 * D�finir les bornes et le nombre de graduations sur l'axe des abcisses
	 * @param in_min_x	valeur minimale en abcisse
	 * @param in_max_x	valeur maximale en abcisse
	 * @param in_x_count pas de graduations horizontales
	 */
	public void setHorizontalGraduations(Object in_min_x, Object in_max_x, double in_x_step)
	{
		String				tmp_x_labels[];
		Double				tmp_min_x;
		Double				tmp_max_x;
		Date				tmp_date;
		Calendar			tmp_calendar;
		NumberFormat		tmp_number_format = NumberFormat.getIntegerInstance();
		Trinome.DataObject 	tmp_min_x_categories = new Trinome.DataObject(in_min_x);
		Trinome.DataObject 	tmp_max_x_categories = new Trinome.DataObject(in_max_x);
		
		tmp_number_format.setMinimumIntegerDigits(2);

		tmp_min_x = tmp_min_x_categories.parseDouble();
		tmp_max_x = tmp_max_x_categories.parseDouble();
		if (tmp_min_x != null && tmp_max_x != null && tmp_min_x.compareTo(tmp_max_x) == -1)
		{
			tmp_x_labels = new String[(int)((tmp_max_x.doubleValue() - tmp_min_x.doubleValue()) / in_x_step) + 1];
			for (int tmp_index = 0; tmp_index < tmp_x_labels.length; tmp_index++)
			{
				if (tmp_max_x_categories.isNumber())
					tmp_x_labels[tmp_index] = String.valueOf(tmp_min_x.doubleValue() + tmp_index * in_x_step);
				else if (tmp_max_x_categories.isDate())
				{
					tmp_date = new Date((long)(tmp_min_x.doubleValue() + tmp_index * in_x_step));
					tmp_calendar = Calendar.getInstance();
					tmp_calendar.setTime(tmp_date);
					
					if (in_x_step <= SECOND_STEP)
						tmp_x_labels[tmp_index] = tmp_number_format.format(tmp_calendar.get(Calendar.SECOND));
					else if (in_x_step <= MINUTE_STEP)
						tmp_x_labels[tmp_index] = tmp_number_format.format(tmp_calendar.get(Calendar.MINUTE));
					else if (in_x_step <= HOUR_STEP)
						tmp_x_labels[tmp_index] = tmp_number_format.format(tmp_calendar.get(Calendar.HOUR_OF_DAY)) + ":" + tmp_number_format.format(tmp_calendar.get(Calendar.MINUTE));
					else if (in_x_step <= DAY_STEP)
						tmp_x_labels[tmp_index] = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format(tmp_date);
					else if (in_x_step <= WEEK_STEP)
						tmp_x_labels[tmp_index] = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format(tmp_date);
					else if (in_x_step <= MONTH_STEP)
						tmp_x_labels[tmp_index] = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format(tmp_date);
					else if (in_x_step <= YEAR_STEP)
						tmp_x_labels[tmp_index] = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format(tmp_date);
				}
				else
					tmp_x_labels[tmp_index] = "";
			}
			
			setXLegend(tmp_x_labels);

			_m_min_x_categories = tmp_min_x_categories;
			_m_max_x_categories = tmp_max_x_categories;
			
			_m_abciss_proportionnal_indic = true;
		}		
	}
	
	
	/**
	 * D�finir les bornes et le nombre de graduations sur l'axe des ordonn�es
	 * @param in_min_y valeur minimale en ordonn�e
	 * @param in_max_y valeur maximale en ordonn�e
	 * @param in_y_count pas de graduations verticales
	 */
	public void setVerticalGraduations(Object in_min_y, Object in_max_y, int in_y_step)
	{
		String	tmp_y_labels[];
		Double	tmp_min_y;
		Double	tmp_max_y;
		
		Trinome.DataObject tmp_min_y_categories = new Trinome.DataObject(in_min_y);
		Trinome.DataObject tmp_max_y_categories = new Trinome.DataObject(in_max_y);
		
		tmp_min_y = tmp_min_y_categories.parseDouble();
		tmp_max_y = tmp_max_y_categories.parseDouble();
		
		if (tmp_min_y != null && tmp_max_y != null && tmp_min_y.compareTo(tmp_max_y) == -1)
		{
			tmp_y_labels = new String[(int)((tmp_max_y.doubleValue() - tmp_min_y.doubleValue()) / in_y_step)];
			for (int tmp_index = 0; tmp_index < tmp_y_labels.length; tmp_index++)
			{
				if (_m_max_y_categories.isNumber())
					tmp_y_labels[tmp_index] = String.valueOf(tmp_min_y.doubleValue() + (tmp_index + 1) * in_y_step);
				else if (_m_max_y_categories.isDate())
					tmp_y_labels[tmp_index] = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format(new Date((long)(tmp_min_y.doubleValue() + tmp_index * in_y_step)));
				else
					tmp_y_labels[tmp_index] = "";
			}
			
			setYLegend(tmp_y_labels);

			_m_min_y_categories = tmp_min_y_categories;
			_m_max_y_categories = tmp_max_y_categories;
		}		
	}
	
	
	/**
	 * D�finir les bornes et le nombre de graduations sur l'axe des azimuts
	 * @param in_min_z valeur minimale en azimut
	 * @param in_max_z valeur maximale en azimut
	 * @param in_z_count pas de graduations azimutales
	 */
	public void setAzimutalGraduations(Object in_min_z, Object in_max_z, int in_z_step)
	{
		String	tmp_z_labels[];
		Double	tmp_min_z;
		Double	tmp_max_z;
		
		Trinome.DataObject tmp_min_z_categories = new Trinome.DataObject(in_min_z);
		Trinome.DataObject tmp_max_z_categories = new Trinome.DataObject(in_max_z);
		
		tmp_min_z = tmp_min_z_categories.parseDouble();
		tmp_max_z = tmp_max_z_categories.parseDouble();
		
		if (tmp_min_z != null && tmp_max_z != null && tmp_min_z.compareTo(tmp_max_z) == -1)
		{
			tmp_z_labels = new String[(int)((tmp_max_z.doubleValue() - tmp_min_z.doubleValue()) / in_z_step)];
			for (int tmp_index = 0; tmp_index < tmp_z_labels.length; tmp_index++)
			{
				if (_m_max_z_categories.isNumber())
					tmp_z_labels[tmp_index] = String.valueOf(tmp_min_z.doubleValue() + tmp_index * in_z_step);
				else if (_m_max_z_categories.isDate())
					tmp_z_labels[tmp_index] = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE).format(new Date((long)(tmp_min_z.doubleValue() + tmp_index * in_z_step)));
				else
					tmp_z_labels[tmp_index] = "";
			}
			
			setZLegend(tmp_z_labels);

			_m_min_z_categories = tmp_min_z_categories;
			_m_max_z_categories = tmp_max_z_categories;
		}		
	}
	
	
	/**
	 * D�finir le tableau des donn�es
	 * @param in_trinomes_array
	 */
	public void setData(TrinomeArray in_trinomes_array)
	{
		_m_trinomes_array = in_trinomes_array;

		/* Traitement des donn�es */
		if (_m_trinomes_array != null)
		{
			/* Initialiser les tableaux des cat�gories */
			_m_x_categories = _m_trinomes_array.m_x_categories;
			_m_y_categories = _m_trinomes_array.m_y_categories;
		    _m_z_categories = _m_trinomes_array.m_z_categories;

			/* Initialiser les bornes inf�rieures */
	    	_m_min_x_categories = _m_trinomes_array.m_min_x;
	    	_m_min_y_categories = _m_trinomes_array.m_min_y;
	    	_m_min_z_categories = _m_trinomes_array.m_min_z;
	    	
			/* Initialiser les bornes sup�rieures */
	    	_m_max_x_categories = _m_trinomes_array.m_max_x;
	    	_m_max_y_categories = _m_trinomes_array.m_max_y;
	    	_m_max_z_categories = _m_trinomes_array.m_max_z;

	    	_m_abciss_proportionnal_indic = (_m_min_x_categories != null && _m_max_x_categories != null &&
	    			(_m_min_x_categories.isNumber() || _m_min_x_categories.isDate())
					&& (_m_max_x_categories.isNumber() || _m_max_x_categories.isDate()));
		}
	}
	
	
	/**
	 * Initialiser les param�tres du graphique (coordonn�es, bordures, l�gendes...etc) avant le dessin
	 */
	protected void initDrawing()
	{
	    int			tmp_coords_count = 0;
	    
	    if (_m_x_labels != null)	_m_x_categories_count = _m_x_labels.count();
	    else if (_m_x_categories != null)	_m_x_categories_count = _m_x_categories.count();
	    
	    if (_m_y_labels != null)	_m_y_categories_count = _m_y_labels.count();
	    else if (_m_y_categories != null)	_m_y_categories_count = _m_y_categories.count();
	    
	    if (_m_type != GRAPH_TYPE_POLYGON)
	    	_m_abciss_proportionnal_indic = false;
	    
	    if (_m_z_categories.count() > 1)
	    {
	        /* D�finir la s�rie des l�gendes */
		    _m_legend_categories = _m_z_categories;
		    
	        /* D�finir le nombre de valeurs de la cat�gorie Z */
		    _m_z_categories_count = _m_z_categories.count();
		    
	        /* D�finir le nombre de coordonn�es n�cessaires */
		    tmp_coords_count = _m_z_categories_count * _m_x_categories_count;
	    }
	    else
	    {
	        /* D�finir la s�rie des l�gendes */
	        if (_m_type == GRAPH_TYPE_CHEESE)	_m_legend_categories = _m_x_categories;
			else	_m_legend_categories = null;
	        
	        /* D�finir le nombre de valeurs de la cat�gorie Z */
		    _m_z_categories_count = 1;

		    /* D�finir le nombre de coordonn�es n�cessaires */
		    tmp_coords_count = _m_trinomes_array.length;
	    }
	    
	    /* Initialiser les tableaux de coordonn�es */
		if (_m_x_array == null || _m_x_array.length != tmp_coords_count)
			_m_x_array = new double[tmp_coords_count];
		if (_m_y_array == null || _m_y_array.length != tmp_coords_count)
			_m_y_array = new double[tmp_coords_count];
	    
        /* Calculer la taille des graduations sur les axes */
        _calculateGraduations();

		/* Si la taille des bordures est automatique...*/
	    if (m_auto_borders)
	    {
		    /* ...mettre � jour la taille de la bordure du bas 
		     * en fonction de l'affichage ou non de la l�gende */
		    if (_m_legend_categories != null && m_show_legend)
			    _setBorderSize(-1, -1, -1, _m_graph_font_size + 10 + _m_legend_categories.count() * (_m_graph_font_size + 5));			    
			else
			    _setBorderSize(-1, -1, -1, DEFAULT_BORDER_BOTTOM * _m_width / DEFAULT_WIDTH);	
	    }
	}
	
	
	/**
	 * Calculer la taille des graduations horizontales et verticales
	 */
	private void _calculateGraduations()
	{
	    double	tmp_max_y = 0.0;
	    double	tmp_factor = 0.0;
	    double	tmp_step_count = 0.0;
	    int[]	tmp_factors = new int[]{1, 2, 4, 5};

	    int		tmp_index;

	    /* Modifier la taille des graduations horizontales en fonction de la largeur du graphique */
		if (_m_x_categories_count != 0)
		{
			if (_m_abciss_proportionnal_indic)
				_m_x_step = _m_graph_width / (_m_x_categories_count - 1);
			else
				_m_x_step = _m_graph_width / _m_x_categories_count;
		}
		else
		{
			_m_x_step = DEFAULT_X_STEP * _m_graph_width / (DEFAULT_WIDTH - DEFAULT_BORDER_LEFT - DEFAULT_BORDER_RIGHT);
		}

		/* Modifier la taille des graduations verticales en fonction de la hauteur du graphique */
		if (_m_y_labels == null || _m_y_labels.count() == 0)
		{
		    /* V�rifier l'existance de donn�es */
		    if(_m_trinomes_array != null)
		    {
			    /* Graduation maximale */
		    	if (_m_type == GRAPH_TYPE_HISTOGRAM && m_stack_histogram_indic)
		    		tmp_max_y = _m_trinomes_array.m_max_total_y_by_x_class;
		    	else if(_m_trinomes_array.m_max_y != null && _m_trinomes_array.m_max_y.isNumber())
		    		tmp_max_y = ((Number)_m_trinomes_array.m_max_y.m_data).doubleValue();

				if(tmp_max_y > 0.0)
				{
				    /* Valeur par d�faut */
				    _m_y_step = _m_graph_height / 10 ;
				    
				    /* Calcul du facteur de graduation maximal */
				    /* Attention : la fonction Math.log calcule le logarithme n�p�rien bas� sur la constante d'(e)uler
				     * et non pas le logarithme d�cimal bas� sur le nombre dix. Dans notre cas, nous voulons calculer le
				     * logarithme d�cimal. Celui-ci peut �tre calcul� pour un nombre X, � partir de la formule :
				     * log(X) = ln(X)/ln(10) o� 'ln' signifie logarithme n�p�rien 
				     * car si X = 10^y alors ln(X) = ln(10) * y et donc y = ln(X)/ln(10) 
				     */
				    tmp_factor = (int)(Math.log(tmp_max_y) / Math.log(10)) - 1;
				    while (tmp_max_y / Math.pow(10.0, tmp_factor + 1) > 1.0) tmp_factor++;
				    tmp_factor = tmp_max_y / Math.pow(10, tmp_factor);
				    
				    for(tmp_index = 0; tmp_index < tmp_factors.length; tmp_index++)
				    {
				        tmp_step_count = tmp_factor * tmp_factors[tmp_index];
				        /* Il doit y avoir de 1 � 10 graduations pour des valeurs enti�res
				         * et de 4 � 10 pour des valeurs d�cimales */
				        if (tmp_step_count >= (_m_trinomes_array.m_has_decimal_y_indic ? 4.0 : 1.0)
				                && tmp_step_count <= 10.0)
				        {
				            _m_y_step = _m_graph_height / tmp_step_count;
				            break;
				        }
				    }
				}
				else
				{
					_m_y_step = _m_graph_height / (_m_y_categories != null && _m_y_categories.count() != 0 ? _m_y_categories.count() : 1);
				}
			}
		    else
			{
				_m_y_step = _m_graph_height / 10 ;
			}
	    }
		else
		{
			_m_y_step = _m_graph_height / (_m_y_labels != null && _m_y_labels.count() != 0 ? _m_y_labels.count() : 1);
		}   
	}
	
	
	/**
	 * D�finir la taille des bords seulement si <code>m_auto_borders</code> est positionn� � <code>false</code>
	 * @param in_border_left taille du bord gauche
	 * @param in_border_right taille du bord droit
	 * @param in_border_top taille du bord sup�rieur
	 * @param in_border_bottom taille du bord inf�rieur
	 */
	public void setBorderSize(double in_border_left, double in_border_right, double in_border_top, double in_border_bottom)
	{
	    if (!m_auto_borders)	_setBorderSize(in_border_left, in_border_right, in_border_top, in_border_bottom);
	}
	
	
	/**
	 * D�finir la taille des bords
	 * @param in_border_left taille du bord gauche
	 * @param in_border_right taille du bord droit
	 * @param in_border_top taille du bord sup�rieur
	 * @param in_border_bottom taille du bord inf�rieur
	 */
	private void _setBorderSize(double in_border_left, double in_border_right, double in_border_top, double in_border_bottom)
	{
		
		double	tmp_left = (in_border_left >= 0 ? in_border_left : _m_graph_origin_x);
		double	tmp_right = (in_border_right >= 0 ? in_border_right : _m_width - _m_graph_width - _m_graph_origin_x);
		double	tmp_top = (in_border_top >= 0 ? in_border_top : _m_graph_origin_y - _m_graph_height);
		double	tmp_bottom = (in_border_bottom >= 0 ? in_border_bottom : _m_height - _m_graph_origin_y);
		
		/* Ne conserver que des bords suffisemment larges pour afficher le graphe
		 * et ne rien modifier si les nouvelles valeurs sont identiques aux anciennes */
		if (_m_width / 2 <= tmp_left + tmp_right
			|| 2 * _m_height / 3 <= tmp_bottom + tmp_top
			||(tmp_left == _m_graph_origin_x
			&& tmp_bottom == _m_height - _m_graph_origin_y
			&& tmp_top == _m_graph_origin_y - _m_graph_height
			&& tmp_right ==  _m_width - _m_graph_width - _m_graph_origin_x))
		{
			return ;
		}

		/* Mettre � jour la taille et la position du graphique */
		_setGraphRect(tmp_left, _m_height - tmp_bottom, _m_width - tmp_left - tmp_right, _m_height - tmp_top - tmp_bottom);
	}
	
	
	/**
	 * Initialiser le contexte graphique
	 */
	protected void _initGraphicContext()
	{
	    double	tmp_graph_width = (DEFAULT_WIDTH - DEFAULT_BORDER_LEFT - DEFAULT_BORDER_RIGHT) * _m_width / DEFAULT_WIDTH;
	    double	tmp_graph_height = (DEFAULT_HEIGHT - DEFAULT_BORDER_TOP - DEFAULT_BORDER_BOTTOM) * _m_height / DEFAULT_HEIGHT;
	        
		NSLog.out.appendln("[" + MOChart.class.getName() + "] Initialisaton...");
		
		_m_rendering_speed_indic = false;
		
	    super._initGraphicContext();

	    /* Positionner l'indicateur de dessin des axes */
	    _m_drawn_axes = false;
	    
	    /* Initialiser la position et la taille du graphique */
	    _setGraphRect(
	            (_m_width - tmp_graph_width) / 2,
	            _m_height - (_m_height - tmp_graph_height) / 2,
	            tmp_graph_width,
	            tmp_graph_height);
	    
		/* Initialiser les polices par d�faut */
	    _m_graph_font_size = (float)(_m_width / DEFAULT_WIDTH < _m_height / DEFAULT_HEIGHT ? 10 * _m_width / DEFAULT_WIDTH : 10 * _m_height / DEFAULT_HEIGHT);
		if (_m_graph_font == null) _m_graph_font = Font.decode(null).deriveFont(_m_graph_font_size);
		if (_m_title_font == null) _m_title_font = Font.decode(null);
		NSLog.out.appendln("[" + MOChart.class.getName() + "] POLICE = " + _m_graph_font.toString());
	}
	
	
	/**
	 * Dessiner le cadre
	 */
	protected void drawAxis()
	{
		int			tmp_index = 0;
		String		tmp_str = null;
		TextLayout	tmp_str_layout = null;
	    BasicStroke tmp_current_stroke = (BasicStroke)_m_Graphics2D.getStroke();
		double		tmp_x_step = 0.0;
	    AffineTransform tmp_originTr = null, tmp_newTr = null;
	    NSMutableArray	tmp_text_layout_array = new NSMutableArray();
	    boolean		tmp_oblic_text_layout_indic = false;
		
		if (_m_type == GRAPH_TYPE_HISTOGRAM && m_stack_histogram_indic)
			tmp_x_step = _m_x_step / 2;
		else
			tmp_x_step = (_m_x_step / 2) / _m_z_categories_count;

		/* Dessiner le cadre sauf pour le camembert */
		if (_m_type != GRAPH_TYPE_CHEESE && !_m_drawn_axes)
		{
		    _m_drawn_axes = true;
		    
			_m_Graphics2D.setColor(Color.BLACK);
			
			if (_m_type == GRAPH_TYPE_HISTOGRAM && m_raised_histogram_indic)
			{
				_m_Graphics2D.draw(new Line2D.Double(
				        _m_graph_origin_x,
				        _m_graph_origin_y,
				        _m_graph_origin_x + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
				        _m_graph_origin_y - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline))));

				/* Tracer une ligne en pointill�s */
			    _m_Graphics2D.setColor(Color.LIGHT_GRAY);
			    _m_Graphics2D.setStroke(new BasicStroke(tmp_current_stroke.getLineWidth(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{4, 10}, 0));
				_m_Graphics2D.draw(new Line2D.Double(
						_m_graph_origin_x + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
						_m_graph_origin_y - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline)),
				        _m_graph_origin_x + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
				        _m_graph_origin_y - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline)) - _m_graph_height));
		        
			    _m_Graphics2D.setColor(Color.BLACK);
				_m_Graphics2D.setStroke(tmp_current_stroke);
			}
			    
			    
			/* Tracer les graduations horizontales */
			if (_m_x_step != 0)
			{
				for (tmp_index = 0; tmp_index < _m_x_categories_count; tmp_index++)
				{					
					/* Tracer une ligne en pointill�s */
				    _m_Graphics2D.setColor(Color.LIGHT_GRAY);
				    _m_Graphics2D.setStroke(new BasicStroke(tmp_current_stroke.getLineWidth(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{2, 2}, 0));
					if(_m_type == GRAPH_TYPE_HISTOGRAM && m_raised_histogram_indic)
					{
						_m_Graphics2D.draw(new Line2D.Double(
								_m_graph_origin_x + _m_x_step * tmp_index + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
								_m_graph_origin_y - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline)),
						        _m_graph_origin_x + _m_x_step * tmp_index + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
						        _m_graph_origin_y - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline)) - _m_graph_height));
					}
					else
					{
						_m_Graphics2D.draw(new Line2D.Double(
								_m_graph_origin_x + _m_x_step * tmp_index,
								_m_graph_origin_y,
						        _m_graph_origin_x + _m_x_step * tmp_index,
						        _m_graph_origin_y - _m_graph_height));
					}

				    _m_Graphics2D.setColor(Color.BLACK);
					_m_Graphics2D.setStroke(tmp_current_stroke);

					/* Tracer une graduation */
					_m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x + tmp_index * _m_x_step, _m_graph_origin_y - 1, _m_graph_origin_x + tmp_index * _m_x_step, _m_graph_origin_y - 4));

					/* Pr�parer le libell� */
					tmp_str = null;
					if (_m_x_labels != null){
						if (_m_x_labels.count() > tmp_index)
							tmp_str =(String)_m_x_labels.objectAtIndex(tmp_index);
					}else if (tmp_str == null)
						if (_m_x_categories.count() > tmp_index)
							tmp_str = _m_x_categories.objectAtIndex(tmp_index).toString();
					
					if (tmp_str == null)	tmp_str = " ";
						
					tmp_str_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), tmp_str, _m_graph_font, -1);
					if (tmp_str_layout != null)
					{
						if (tmp_str_layout.getBounds().getWidth() > _m_x_step)
						{
							tmp_oblic_text_layout_indic = true;
							tmp_str_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), tmp_str, _m_graph_font, (float)((_m_height - _m_graph_origin_y - 10)/Math.sin(Math.toRadians(45))));
						}
						
						tmp_text_layout_array.addObject(tmp_str_layout);						
					}
				}

				_m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x + _m_graph_width, _m_graph_origin_y - 1, _m_graph_origin_x + _m_graph_width, _m_graph_origin_y - 4));

				/* Dessiner les libell�s */
				for (tmp_index = 0; tmp_index  < tmp_text_layout_array.count(); tmp_index++)
				{
			        tmp_str_layout = (TextLayout)tmp_text_layout_array.objectAtIndex(tmp_index);

			        /* Dessiner le libell� en oblique (45�)*/
					if (tmp_oblic_text_layout_indic)
					{
				        tmp_originTr =_m_Graphics2D.getTransform();
				        
				        tmp_newTr = new AffineTransform();
				        tmp_newTr.translate(_m_graph_origin_x + tmp_index * _m_x_step + (_m_abciss_proportionnal_indic ? 0 : _m_x_step / 2)- tmp_str_layout.getBounds().getWidth() * Math.cos(Math.toRadians(45)),
				        		_m_graph_origin_y + 10 + tmp_str_layout.getBounds().getWidth() * Math.sin(Math.toRadians(45)));
				        tmp_newTr.rotate(Math.toRadians(-45));
				        _m_Graphics2D.transform(tmp_newTr);
				        
						tmp_str_layout.draw(_m_Graphics2D, 0, 0);

						if (tmp_originTr != null) _m_Graphics2D.setTransform(tmp_originTr);
					}
					/* Dessiner le libell� horizontalement */
					else
					{
						tmp_str_layout.draw(_m_Graphics2D,
								(float)(_m_graph_origin_x + tmp_index * _m_x_step + (_m_abciss_proportionnal_indic ? - tmp_str_layout.getBounds().getWidth()/2 : (_m_x_step - tmp_str_layout.getBounds().getWidth())/2)),
								(float)(_m_graph_origin_y + _m_graph_font_size + 5));
					}
				}
			}

			/* Tracer les graduations verticales */
			if (_m_y_step != 0)
			{
				for (tmp_index = 1; tmp_index <= _m_graph_height / _m_y_step; tmp_index++)
				{
				    /* Tracer une ligne en pointill�s */
				    _m_Graphics2D.setColor(Color.LIGHT_GRAY);
				    _m_Graphics2D.setStroke(new BasicStroke(tmp_current_stroke.getLineWidth(), BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{2, 2}, 0));

					if(_m_type == GRAPH_TYPE_HISTOGRAM && m_raised_histogram_indic)
					{
					    _m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x,
        						_m_graph_origin_y - tmp_index * _m_y_step,
        						_m_graph_origin_x + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
        						_m_graph_origin_y - tmp_index * _m_y_step - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline))));
					    _m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)),
					    		_m_graph_origin_y - tmp_index * _m_y_step - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline)),
					    		_m_graph_origin_x + tmp_x_step * Math.sin(Math.toRadians(_m_y_incline)) + _m_graph_width,
					    		_m_graph_origin_y - tmp_index * _m_y_step - tmp_x_step * Math.sin(Math.toRadians(_m_x_incline))));
					}
					else
					{
					    _m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x,
					    		_m_graph_origin_y - tmp_index * _m_y_step,
								_m_graph_origin_x + _m_graph_width,
								_m_graph_origin_y - tmp_index * _m_y_step));
					}
	
			        _m_Graphics2D.setStroke(tmp_current_stroke);

				    /* Tracer une graduation */
				    _m_Graphics2D.setColor(Color.BLACK);
					_m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x, _m_graph_origin_y - tmp_index * _m_y_step, _m_graph_origin_x + 3, _m_graph_origin_y - tmp_index * _m_y_step));

					/* D�finir le libell� de la l�gende */
					tmp_str = null;
					if (_m_y_labels != null && _m_y_labels.count() > tmp_index - 1)
					{
						tmp_str = _m_y_labels.objectAtIndex(tmp_index - 1).toString();
					}
					else if (_m_type == GRAPH_TYPE_HISTOGRAM && m_stack_histogram_indic)
					{
						tmp_str = NumberFormat.getNumberInstance().format(_m_trinomes_array.m_max_total_y_by_x_class * tmp_index * _m_y_step / _m_graph_height);
					}
					else if (_m_trinomes_array.m_max_y != null && _m_trinomes_array.m_max_y.isNumber())
					{
						tmp_str = NumberFormat.getNumberInstance().format(((Double)_m_trinomes_array.m_max_y.m_data).doubleValue() * tmp_index * _m_y_step / _m_graph_height);
					}
					
					if (tmp_str == null && _m_y_categories != null && _m_y_categories.count() > tmp_index - 1)
					{
					    tmp_str = _m_y_categories.objectAtIndex(tmp_index - 1).toString();
					}
					
					/* Dessiner le libell� */
					tmp_str_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), tmp_str, _m_graph_font, -1);
					if (tmp_str_layout != null)
					    tmp_str_layout.draw(_m_Graphics2D, (float)(_m_graph_origin_x - tmp_str_layout.getBounds().getWidth() - 5), (float)(_m_graph_origin_y - tmp_index * _m_y_step + tmp_str_layout.getBounds().getHeight() / 2));
				}
				
				_m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x, _m_graph_origin_y - _m_graph_height, _m_graph_origin_x + 3, _m_graph_origin_y - _m_graph_height));
			}
			
			/* Tracer l'axe des abscisses */
			_m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x, _m_graph_origin_y, _m_graph_origin_x + _m_graph_width, _m_graph_origin_y));
			/* Tracer l'axe des ordonn�es */
			_m_Graphics2D.draw(new Line2D.Double(_m_graph_origin_x, _m_graph_origin_y, _m_graph_origin_x, _m_graph_origin_y - _m_graph_height));

		}
	}
	
	
	/**
	 * Dessiner la l�gende
	 */
	protected void drawLegend()
	{
	    int				tmpIndex = 0;
	    String			tmp_str = null;
		GradientColor	tmp_grad_color = null;
		TextLayout		tmp_str_layout = null;

		/* Dessiner uniquement si il y a une s�rie de valeurs et 
		 * si l'indicateur d'affichage des l�gendes est positionn� � vrai */
	    if (_m_legend_categories != null && m_show_legend)
	    {
		    tmp_grad_color = new GradientColor(_m_colors);
			
			for(tmpIndex = 0; tmpIndex < _m_legend_categories.count(); tmpIndex++)
			{
			    /* Dessiner le carr� de couleur de la cat�gorie courante */
				_m_Graphics2D.setColor(tmp_grad_color.colorAtIndex(tmpIndex));
		        fill3DRect(_m_graph_origin_x, _m_graph_origin_y + _m_graph_font_size + 10 + tmpIndex * (_m_graph_font_size + 5), _m_graph_font_size, _m_graph_font_size, true, _m_x_incline, _m_y_incline);

				/* D�finir le libell� */
		        tmp_str = null;
		        if (_m_legend_categories == _m_x_categories && _m_x_labels != null && _m_x_labels.count() > tmpIndex)
		            tmp_str = (String)_m_x_labels.objectAtIndex(tmpIndex);
		        if (_m_legend_categories == _m_z_categories && _m_z_labels != null && _m_z_labels.count() > tmpIndex)
		            tmp_str = (String)_m_z_labels.objectAtIndex(tmpIndex);
		        if (tmp_str == null)	tmp_str = _m_legend_categories.objectAtIndex(tmpIndex).toString();
		        
				/* Dessiner le libell� */
		        _m_Graphics2D.setColor(Color.BLACK);
				tmp_str_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), tmp_str,
				        					_m_graph_font,
				        					(float)(_m_graph_width - _m_graph_font_size - 5));				
				if (tmp_str_layout != null)
				{
				    tmp_str_layout.draw(_m_Graphics2D,
					        (float)(_m_graph_origin_x + _m_graph_font_size + 5),
					        (float)(_m_graph_origin_y + _m_graph_font_size * 2 + 10 + tmpIndex * (_m_graph_font_size + 5)));
				}
		    }
	    }
	}
	
	
	/**
	 * D�ssiner le titre du graphique.
	 */
	protected void drawTitle()
	{
		float		tmp_font_size = (float)(_m_width / DEFAULT_WIDTH < _m_height / DEFAULT_HEIGHT ? 24 * _m_width / DEFAULT_WIDTH : 24 * _m_height / DEFAULT_HEIGHT);
		TextLayout	tmp_title_layout = null;
		float		tmp_title_pos_x = 0, tmp_title_pos_y = 0;

		if (_m_title != null && _m_title.length() > 0)
		{
			tmp_title_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), _m_title, _m_title_font.deriveFont(tmp_font_size), -1);
	        
			if (tmp_title_layout != null)
			{
				/* Calcul de la position horizontale du titre dans l'image */
				switch (_m_graph_title_halign)
				{
					case TITLE_ALIGN_CENTER :
					    tmp_title_pos_x = (float)( (_m_width - tmp_title_layout.getBounds().getWidth()) / 2);
					    break;
					case TITLE_ALIGN_LEFT :
					    tmp_title_pos_x = 5;
					    break;
					case TITLE_ALIGN_RIGTH :
					    tmp_title_pos_x = (float)(_m_graph_width - tmp_title_layout.getBounds().getWidth() - 5);
					    break;
				}
				
				/* Calcul de la position verticale du titre dans l'image */
				switch (_m_graph_title_valign)
				{
					case TITLE_ALIGN_CENTER :
					    tmp_title_pos_y = (float)((_m_height + tmp_title_layout.getBounds().getHeight()) / 2);
					    break;
					case TITLE_ALIGN_TOP :
					    tmp_title_pos_y = (float)tmp_title_layout.getBounds().getHeight() + 5;
					    break;
					case TITLE_ALIGN_BOTTOM :
					    tmp_title_pos_y = (float)_m_height - 5;
					    break;
				}

				/* Dessiner l'ombre du titre */
			    _m_Graphics2D.setColor(Color.BLACK);
				tmp_title_layout.draw(_m_Graphics2D, tmp_title_pos_x + 1, tmp_title_pos_y - 1);
				tmp_title_layout.draw(_m_Graphics2D, tmp_title_pos_x + 2, tmp_title_pos_y - 2);

				/* Dessiner le titre */
				_m_Graphics2D.setColor(_m_graph_title_color);
				tmp_title_layout.draw(_m_Graphics2D, tmp_title_pos_x, tmp_title_pos_y);
			}
	    }
	}
	
	
	/**
	 * Effacer le contenu de l'image
	 */
	public void clear ()
	{
	    _m_drawn_axes = false;
	    super.clear();
	}
		
	
	/**
	 * D�finir la position et la taille du graphique dans l'image
	 * @param in_graph_x position horizontale
	 * @param in_graph_y position verticale
	 * @param in_graph_width largeur
	 * @param in_graph_height hauteur
	 */
	private void _setGraphRect(double in_graph_x, double in_graph_y, double in_graph_width, double in_graph_height)
	{
	    /* Modifier la position du graphique */
	    _m_graph_origin_x = in_graph_x;
	    _m_graph_origin_y = in_graph_y;

	    /* Modifier les dimensions du graphique */
	    _m_graph_width = in_graph_width;
	    _m_graph_height = in_graph_height;

	    /* Mettre � jour la taille des graduations */
	    _calculateGraduations();
	}
	
	
	/**
	 * Calcul des coordonn�es des points du graphique<br>
	 * Utilis� pour un histogramme et un polygone
	 */
	private void _calculateGraphPoints()
	{
		int		tmpIndex = 0;
		int		tmpIndexX = 0;
		int		tmpIndexZ = 0;
		int		tmpTupleIndex = 0;
		Trinome.DataObject tmp_x_object = null;
		
		/* Parcourrir les cat�gories X */
		for(tmpIndexX = 0; tmpIndexX < _m_x_categories.count(); tmpIndexX++)
		{
			/* Parcourrir les cat�gories Z */
			for(tmpIndexZ = 0; tmpIndexZ < _m_z_categories_count; tmpIndexZ++)
			{
			    tmpIndex = tmpIndexX * _m_z_categories_count + tmpIndexZ;
			    tmp_x_object = (Trinome.DataObject)_m_x_categories.objectAtIndex(tmpIndexX);
			    
				/* V�rifier si la valeur X et Y courantes sont des cat�gories valides */
				if (tmpTupleIndex < _m_trinomes_array.length 
				        &&(_m_z_categories_count == 1 ||
				        (Trinome.Comparator.areEquals(_m_trinomes_array.m_x_objects[tmpTupleIndex], tmp_x_object)
				               && Trinome.Comparator.areEquals(_m_trinomes_array.m_z_objects[tmpTupleIndex], _m_z_categories.objectAtIndex(tmpIndexZ)))))
				{
					/* Calcul des abscisses */
					if (_m_abciss_proportionnal_indic && (_m_trinomes_array.m_x_objects[tmpTupleIndex].isNumber() || _m_trinomes_array.m_x_objects[tmpTupleIndex].isDate()))
					{
						_m_x_array[tmpIndex] = _m_trinomes_array.m_x_objects[tmpTupleIndex].parseDouble().doubleValue();
					}
					else
					{
						_m_x_array[tmpIndex] = (tmpIndexX + 1) * _m_x_step;
					}
			
					/* Calcul des ordonn�es */
					if (_m_trinomes_array.m_y_objects[tmpTupleIndex].isNumber() || _m_trinomes_array.m_y_objects[tmpTupleIndex].isDate())
					{
						_m_y_array[tmpIndex] = _m_trinomes_array.m_y_objects[tmpTupleIndex].parseDouble().doubleValue();
					}
					else
					{
						if (_m_y_labels != null)
							_m_y_array[tmpIndex] = _m_y_labels.indexOfObject(_m_trinomes_array.m_y_objects[tmpTupleIndex]) * _m_y_step;
						else
							_m_y_array[tmpIndex] = (tmpTupleIndex + 1)* _m_y_step;
					}
					
					tmpTupleIndex++;
				}
				else
				{
					/* Calcul des abscisses */
					if (tmp_x_object.isNumber() || tmp_x_object.isDate())
					{
						_m_x_array[tmpIndex] = tmp_x_object.parseDouble().doubleValue();
					}
					else
					{
						_m_x_array[tmpIndex] = (tmpIndexX + 1) * _m_x_step;
					}
					
					_m_y_array[tmpIndex] = 0;
				}
			}
		}		
	}
	

	/**
	 * Dessiner l'histogramme
	 */
	protected void drawHistogram()
	{
		int		tmpIndex = 0;
		int		tmpIndexX = 0;
		int		tmpIndexZ = 0;

		double	tmp_x_step = m_stack_histogram_indic ? _m_x_step - 2 : (_m_x_step - 2) / _m_z_categories_count;
		
		double	tmp_max_x = _m_graph_width;
		double	tmp_max_y = _m_graph_height;

		double	tmp_bar_height = 0;
		double	tmp_prev_y = 0;
		
		GradientColor	tmp_grad_color = new GradientColor(_m_colors);

		Color	tmp_paint_color= null;
		
		if (_m_max_x_categories.isNumber())	tmp_max_x = ((Double)_m_max_x_categories.m_data).doubleValue();
		if (_m_max_y_categories.isNumber())	tmp_max_y = ((Double)_m_max_y_categories.m_data).doubleValue();

		if (_m_max_x_categories.isDate())	tmp_max_x = ((Date)_m_max_x_categories.m_data).getTime();
		if (_m_max_y_categories.isDate())	tmp_max_y = ((Date)_m_max_y_categories.m_data).getTime();

		/* D�finir les coordonn�es des points du graphique */
		_calculateGraphPoints();
		
		/* Dessiner les barres de l'histogramme */
	    if (_m_z_categories_count > 1)
	    {
			/* Parcourrir les cat�gories X */
			for(tmpIndexX = 0; tmpIndexX < _m_x_categories.count(); tmpIndexX++)
			{
				tmp_prev_y = _m_graph_origin_y;
				
				/* Parcourrir les cat�gories Z */
				for(tmpIndexZ = 0; tmpIndexZ < _m_z_categories_count; tmpIndexZ++)
				{
					tmpIndex = tmpIndexX * _m_z_categories_count + tmpIndexZ;
					
				    /* D�finir la couleur selon la s�rie courante */
					tmp_paint_color = tmp_grad_color.colorAtIndex(tmpIndexZ);
					_m_Graphics2D.setPaint(new Color(tmp_paint_color.getRed(),
					        			tmp_paint_color.getGreen(),
				        				tmp_paint_color.getBlue(),
				        				_m_graph_alpha_paint_color));

					/* Dessiner um histogramme empil� */
					if (m_stack_histogram_indic)
					{
						/* Ne pas dessiner si l'ordonn�e est nulle---- (y=0) */
						if (_m_y_array[tmpIndex] != 0)
						{
							/* Placer le point selon la taille et la position du graphe dans l'image */
							_m_x_array[tmpIndex] = _m_graph_origin_x + _m_x_array[tmpIndex];
							tmp_bar_height = _m_y_array[tmpIndex] * _m_graph_height / _m_trinomes_array.m_max_total_y_by_x_class;
							_m_y_array[tmpIndex] = tmp_prev_y - tmp_bar_height;
						    tmp_prev_y = _m_y_array[tmpIndex];
					    
						    fill3DRect(_m_x_array[tmpIndex] - _m_x_step + 1,
						    		_m_y_array[tmpIndex] + 1,
						            tmp_x_step,
						            tmp_bar_height - 2,
						            m_raised_histogram_indic, _m_x_incline, _m_y_incline);							
						}
					}
					else
					{
						/* Placer le point selon la taille et la position du graphe dans l'image */
						_m_x_array[tmpIndex] = _m_graph_origin_x + _m_x_array[tmpIndex];
						_m_y_array[tmpIndex] = _m_graph_origin_y - _m_y_array[tmpIndex] * _m_graph_height / tmp_max_y;

						/* Ne pas dessiner si l'ordonn�e est nulle---- (y=0) */
						if (_m_graph_origin_y > _m_y_array[tmpIndex])
						{
						    fill3DRect(_m_x_array[tmpIndex] - _m_x_step +  tmpIndexZ * tmp_x_step + 1,
						            _m_y_array[tmpIndex] + 1,
						            tmp_x_step,
						            _m_graph_origin_y - _m_y_array[tmpIndex] - 2,
						            m_raised_histogram_indic, _m_x_incline, _m_y_incline);
						}
						else if (m_raised_histogram_indic)
						{
							_m_Graphics2D.fill(new Rectangle3D(_m_x_array[tmpIndex] - _m_x_step + 2,
							        _m_graph_origin_y - 2, 
							        tmp_x_step - 2, 
							        tmp_x_step / 2, 
							        -_m_x_incline, 
							        _m_y_incline, 
							        0));
						}
					}
				}
			}
	    }
	    else
	    {
			/* Parcourrir le tableau de coordonn�es */
			for(tmpIndex = 0; tmpIndex < _m_x_array.length; tmpIndex++)
			{
				/* Placer le point selon la taille et la position du graphe dans l'image */
				_m_x_array[tmpIndex] = _m_graph_origin_x + _m_x_array[tmpIndex];
				_m_y_array[tmpIndex] = _m_graph_origin_y - _m_y_array[tmpIndex] * _m_graph_height / tmp_max_y;
				
				tmp_paint_color = tmp_grad_color.colorAtIndex(tmpIndex);
				_m_Graphics2D.setPaint(new Color(tmp_paint_color.getRed(),
				        			tmp_paint_color.getGreen(),
			        				tmp_paint_color.getBlue(),
			        				_m_graph_alpha_paint_color));
				
				/* Ne pas dessiner si l'ordonn�e est nulle---- (y=0) */
				if (_m_graph_origin_y > _m_y_array[tmpIndex])
				{
				    fill3DRect(_m_x_array[tmpIndex] - _m_x_step + 1,
				            _m_y_array[tmpIndex] + 1,
				            tmp_x_step,
				            _m_graph_origin_y - _m_y_array[tmpIndex] - 2,
				            m_raised_histogram_indic, _m_x_incline, _m_y_incline);
				    
				}				
				else if (m_raised_histogram_indic)
				{
					_m_Graphics2D.fill(new Rectangle3D(_m_x_array[tmpIndex] - _m_x_step + 2,
					        _m_graph_origin_y - 2, 
					        tmp_x_step - 2, 
					        tmp_x_step / 2, 
					        -_m_x_incline, 
					        _m_y_incline, 
					        0));
				}
			}
	    }
	}

	
	/**
	 * Dessiner le polygone
	 */
	protected void drawPolygon()
	{
		double	tmp_min_x = 0.0;
		double	tmp_min_y = 0.0;
		double	tmp_max_x = _m_graph_width;
		double	tmp_max_y = _m_graph_height;

		double	tmp_prev_point_x = 0.0;
		double	tmp_prev_point_y = 0.0;
		
		double	tmp_x_step = 0.0;
		double	tmp_y_step = 0.0;
		
		double	tmp_delta_x = _m_x_step / 2;
		double	tmp_delta_y = 0.0;
		
		GradientColor	tmp_grad_color = new GradientColor(_m_colors);
		Color	tmp_paint_color = null;

		int		tmpIndex = 0;
		int		tmpIndexX = 0;
		int		tmpIndexZ = 0;
		
		if (_m_abciss_proportionnal_indic)
		{
			tmp_min_x = _m_min_x_categories.parseDouble().doubleValue();
			tmp_max_x = _m_max_x_categories.parseDouble().doubleValue();
			tmp_x_step = 0.0;
			tmp_delta_x = 0.0;
		}
		if (_m_max_y_categories.isNumber() || _m_max_y_categories.isDate())
		{
			//if (_m_y_array.length > _m_trinomes_array.length)
				tmp_min_y = 0.0;
			//else
			//	tmp_min_y = _m_min_y_categories.parseDouble().doubleValue();
			tmp_max_y = _m_max_y_categories.parseDouble().doubleValue();
			tmp_y_step = 0.0;
			tmp_delta_y = 0.0;
		}
		
		/* D�finir les coordonn�es des points du graphique */
		_calculateGraphPoints();
				
		/* Dessiner la ligne polygonale */
	    if (_m_z_categories_count > 1)
	    {
			/* Parcourrir les cat�gories Z */
			for(tmpIndexZ = 0; tmpIndexZ < _m_z_categories_count; tmpIndexZ++)
			{
			    /* Tracer le premier point de la cat�gorie X courante */
				_m_x_array[tmpIndexZ] = _m_graph_origin_x + (_m_x_array[tmpIndexZ] - tmp_min_x) * (_m_graph_width - tmp_x_step) / (tmp_max_x - tmp_min_x) - tmp_delta_x;
				_m_y_array[tmpIndexZ] = _m_graph_origin_y - (_m_y_array[tmpIndexZ] - tmp_min_y) * (_m_graph_height - tmp_y_step) / (tmp_max_y - tmp_min_y) - tmp_delta_y;
				tmp_prev_point_x = _m_x_array[tmpIndexZ];
			    tmp_prev_point_y = _m_y_array[tmpIndexZ];
			    
			    tmp_paint_color = tmp_grad_color.colorAtIndex(tmpIndexZ);
				_m_Graphics2D.setPaint(new Color(tmp_paint_color.getRed(),
	        			tmp_paint_color.getGreen(),
        				tmp_paint_color.getBlue(),
        				_m_graph_alpha_paint_color));

				_m_Graphics2D.fill(new Ellipse2D.Double(_m_x_array[tmpIndexZ] - 2, _m_y_array[tmpIndexZ] - 2, 4, 4));	        

				/* Parcourrir les cat�gories Y */
				for(tmpIndexX = 1; tmpIndexX < _m_x_categories.count(); tmpIndexX++)
				{
					tmpIndex = tmpIndexX * _m_z_categories_count + tmpIndexZ;
					
					/* Mettre � l'�chelle de la taille de l'image */
				    _m_x_array[tmpIndex] = _m_graph_origin_x + (_m_x_array[tmpIndex] - tmp_min_x) * (_m_graph_width - tmp_x_step) / (tmp_max_x - tmp_min_x) - tmp_delta_x;
					_m_y_array[tmpIndex] = _m_graph_origin_y - (_m_y_array[tmpIndex] - tmp_min_y) * (_m_graph_height - tmp_y_step) / (tmp_max_y - tmp_min_y) - tmp_delta_y;

					_m_Graphics2D.fill(new Ellipse2D.Double(_m_x_array[tmpIndex] - 2, _m_y_array[tmpIndex] - 2, 4, 4));
					_m_Graphics2D.draw(new Line2D.Double(tmp_prev_point_x, tmp_prev_point_y, _m_x_array[tmpIndex], _m_y_array[tmpIndex]));

					tmp_prev_point_x = _m_x_array[tmpIndex];
				    tmp_prev_point_y = _m_y_array[tmpIndex];
				}		
			}
	    }
	    else
	    {
		    /* Tracer le premier point */
    		_m_x_array[0] = _m_graph_origin_x + (_m_x_array[0] - tmp_min_x) * (_m_graph_width - tmp_x_step) / (tmp_max_x - tmp_min_x) - tmp_delta_x;
	    	_m_y_array[0] = _m_graph_origin_y - (_m_y_array[0] - tmp_min_y) * (_m_graph_height - tmp_y_step) / (tmp_max_y - tmp_min_y) - tmp_delta_y;
			tmp_prev_point_x = _m_x_array[0];
		    tmp_prev_point_y = _m_y_array[0];
			_m_Graphics2D.setColor(_m_pencil_Color);
			_m_Graphics2D.fill(new Ellipse2D.Double(_m_x_array[0] - 2, _m_y_array[0] - 2, 4, 4));	        

			for(tmpIndex = 1; tmpIndex < _m_x_array.length; tmpIndex++)
			{
				/* Mettre � l'�chelle de la taille de l'image */
			    _m_x_array[tmpIndex] = _m_graph_origin_x + (_m_x_array[tmpIndex] - tmp_min_x) * (_m_graph_width  - tmp_x_step) / (tmp_max_x - tmp_min_x) - tmp_delta_x;
				_m_y_array[tmpIndex] = _m_graph_origin_y - (_m_y_array[tmpIndex] - tmp_min_y) * (_m_graph_height - tmp_y_step) / (tmp_max_y - tmp_min_y) - tmp_delta_y;

				_m_Graphics2D.fill(new Ellipse2D.Double(_m_x_array[tmpIndex] - 2, _m_y_array[tmpIndex] - 2, 4, 4));
				_m_Graphics2D.draw(new Line2D.Double(tmp_prev_point_x, tmp_prev_point_y, _m_x_array[tmpIndex], _m_y_array[tmpIndex]));
			    
				tmp_prev_point_x = _m_x_array[tmpIndex];
			    tmp_prev_point_y = _m_y_array[tmpIndex];
			}	    
	    }
	}
	
	
	/**
	 * Dessiner un losange
	 * @param in_x0
	 * @param in_y0
	 * @param in_x1
	 * @param in_y1
	 * @param in_thickness
	 */
	private void _drawPortionSide(double in_x0, double in_y0, double in_x1, double in_y1, double in_thickness)
	{
	    double	m = (in_y1 - in_y0)/(in_x1 - in_x0);
	    double	p = in_y0 - m * in_x0;
	    
	    double	tmp_x;
	    double	tmp_y;
	    double	tmp_start;
	    double	tmp_end;
	    
	    /* Ne pas dessiner si les deux points sont quasiment align�s verticalement */
	    if (Math.abs(in_x1 - in_x0) < 0.001)	return;
	    
	    if (in_x0 > in_x1){
	        tmp_start = in_x1;
	        tmp_end = in_x0;
	    }else{
	        tmp_start = in_x0;
	        tmp_end = in_x1;
	    }
	    
	    for (tmp_x = tmp_start; tmp_x <= tmp_end; tmp_x ++)
	    {
	        tmp_y = m * tmp_x + p;
	        _m_Graphics2D.draw(new Line2D.Double(tmp_x, tmp_y, tmp_x, tmp_y + in_thickness));
	    }
	}
	
	
	/**
	 * Tracer les losanges de c�t� de la portion
	 * @param in_index
	 * @param in_start
	 * @param in_end
	 * @param in_draw_left
	 * @param in_draw_right
	 * @param in_center_x
	 * @param in_center_y
	 * @param in_cheese_thickness
	 * @param in_paint_color
	 */
	private void _drawPortionSides(int in_index, double in_start, double in_end,
	        boolean in_draw_left, boolean in_draw_right,
	        double in_center_x, double in_center_y, double in_cheese_thickness,
	        Paint in_paint_color)
	{
		double 			tmp_cheese_width = _m_graph_width - _m_delta_cheese_portion * 2;		
		double 			tmp_upper_left_x = in_center_x - tmp_cheese_width / 2;

	    /* Dessiner le c�t� droit avant le gauche en fonction des angles */
	    if (in_start < 90 || in_end > 270)
	    {
			if (in_draw_right)
			{
			    _drawPortionSide( _m_x_array[in_index], _m_y_array[in_index], in_center_x, in_center_y, in_cheese_thickness, in_paint_color);
			}
			
			if (in_draw_left)
			{
			    if (in_index > 0)
			        _drawPortionSide( _m_x_array[in_index - 1], _m_y_array[in_index - 1], in_center_x, in_center_y, in_cheese_thickness, in_paint_color);
			    else
			        _drawPortionSide( tmp_upper_left_x + tmp_cheese_width, in_center_y, in_center_x, in_center_y, in_cheese_thickness, in_paint_color);
			}
	    }
	    else
	    {
			if (in_draw_left)
			{
			    if (in_index > 0)
			        _drawPortionSide( _m_x_array[in_index - 1], _m_y_array[in_index - 1], in_center_x, in_center_y, in_cheese_thickness, in_paint_color);
			    else
			        _drawPortionSide( tmp_upper_left_x + tmp_cheese_width, in_center_y, in_center_x, in_center_y, in_cheese_thickness, in_paint_color);
			}
			
			if (in_draw_right)
			{
			    _drawPortionSide( _m_x_array[in_index], _m_y_array[in_index], in_center_x, in_center_y, in_cheese_thickness, in_paint_color);
			}
	    }
	    
	    /* Tracer le segment central */
	    if (in_draw_left || in_draw_right)
	        _m_Graphics2D.draw(new Line2D.Double(in_center_x, in_center_y, in_center_x, in_center_y + in_cheese_thickness));
	    
	}
	
	
	/**
	 * Tracer un losange de c�t� de la portion
	 * @param in_x
	 * @param in_y
	 * @param in_center_x
	 * @param in_center_y
	 * @param in_cheese_thickness
	 * @param in_paint_color
	 */
	private void _drawPortionSide(double in_x, double in_y, double in_center_x, double in_center_y, double in_cheese_thickness, Paint in_paint_color)
	{
		_m_Graphics2D.setPaint(in_paint_color);
		_drawPortionSide(in_center_x, in_center_y, in_x, in_y, in_cheese_thickness);
		_m_Graphics2D.setColor(Color.BLACK);
		/* Tracer le segment du centre � la p�riph�rie */
	    _m_Graphics2D.draw(new Line2D.Double(in_x, in_y + in_cheese_thickness, in_center_x, in_center_y + in_cheese_thickness));
		/* Tracer le segment de fin de portion */
        _m_Graphics2D.draw(new Line2D.Double(in_x, in_y, in_x, in_y + in_cheese_thickness));
	}
	
	
	/**
	 * Dessiner une portion de fromage
	 * @param in_index
	 * @param in_start
	 * @param in_end
	 */
	protected void drawCheesePortion(int in_index, double in_start, double in_end)
	{
		Arc2D.Double	tmp_upper_portion = null;
		Arc2D.Double	tmp_lower_portion = null;

		double 			tmp_cheese_width = _m_graph_width - _m_delta_cheese_portion * 2;
		double 			tmp_cheese_height = (_m_graph_height - _m_delta_cheese_portion * 2) * Math.sin(Math.toRadians(_m_y_incline));
		double 			tmp_cheese_thickness = _m_graph_height - tmp_cheese_height;
		
		double 			tmp_center_x = _m_graph_origin_x + _m_graph_width / 2;
		double 			tmp_center_y = _m_graph_origin_y - _m_graph_height + _m_graph_height * Math.sin(Math.toRadians(_m_y_incline)) / 2;

		double 			tmp_upper_left_x = tmp_center_x - tmp_cheese_width / 2;
		double 			tmp_upper_left_y = tmp_center_y - tmp_cheese_height / 2;

		double			tmp_extend = in_end - in_start;
		
		double			tmp_x;
		double			tmp_y;
		double			tmp_f = in_start >= 180 ? 1 : -1;
		double			tmp_first_x;
		double			tmp_last_x;

		GradientColor	tmp_grad_color = new GradientColor(_m_colors);
		GradientPaint	tmp_grad_paint;
		Color			tmp_paint_color = null;

		boolean			tmp_draw_left = in_start == (in_index > 0 ? _m_angles_array[in_index - 1] : 0);
		boolean			tmp_draw_right = in_end == _m_angles_array[in_index];
		
	    AffineTransform tmp_origin_aft = null;
		AffineTransform	tmp_aft = null;
		
		double			tmp_delta_x;
		double			tmp_delta_y;
		
		NSLog.out.appendln("[MOChart] \t\t- portion n�" + in_index + "( de " + in_start + "� � " + in_end + "�)");
		
		if (tmp_extend > 0)
		{
		    /* Dessiner la portion de fromage d�cal�e de delta pixels par rapport au centre */
		    if (_m_delta_cheese_portion > 0)
		    {
		        tmp_origin_aft = _m_Graphics2D.getTransform();
		        tmp_aft = new AffineTransform();
		        
		        /* D�finir le d�calage horizontal et vertical */
		        if (tmp_draw_left == false || tmp_draw_right == false)
		        {
		            tmp_delta_x = Math.cos(Math.toRadians((_m_angles_array[in_index] + (in_index > 0 ? _m_angles_array[in_index - 1] : 0))/2)) * _m_delta_cheese_portion;
		            tmp_delta_y = - Math.sin(Math.toRadians((_m_angles_array[in_index] + (in_index > 0 ? _m_angles_array[in_index - 1] : 0))/2)) * _m_delta_cheese_portion * Math.sin(Math.toRadians(_m_y_incline));
		        }
		        else
		        {
		    		tmp_delta_x = Math.cos(Math.toRadians(in_end - tmp_extend/2)) * _m_delta_cheese_portion;
		    		tmp_delta_y = - Math.sin(Math.toRadians(in_end - tmp_extend/2)) * _m_delta_cheese_portion * Math.sin(Math.toRadians(_m_y_incline));
		        }
		        
		        tmp_aft.translate(tmp_delta_x, tmp_delta_y);
		        
		        /* Appliquer la transformation affine au contexte graphique */
		        _m_Graphics2D.transform(tmp_aft);
		    }
		    
			/* D�finir la couleur de la portion */
			if (_m_z_categories_count > 1) tmp_paint_color = tmp_grad_color.colorAtIndex(_m_z_categories.indexOfObject(_m_trinomes_array.m_z_objects[in_index]));
		    else tmp_paint_color = tmp_grad_color.colorAtIndex(in_index);
		    tmp_paint_color = new Color(tmp_paint_color.getRed(),
	    			tmp_paint_color.getGreen(),
					tmp_paint_color.getBlue(),
					_m_graph_alpha_paint_color);
		    
		    tmp_grad_paint = new GradientPaint(new Point2D.Double(_m_graph_origin_x, tmp_center_y), 
		    		tmp_paint_color, 
					new Point2D.Double(_m_graph_origin_x + _m_graph_width, tmp_center_y),
					tmp_paint_color.darker());
			
			/* D�finir les parties haute et basse de la portion */
			tmp_lower_portion = new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y + tmp_cheese_thickness, tmp_cheese_width, tmp_cheese_height, in_start, tmp_extend, Arc2D.PIE );
			tmp_upper_portion = new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y, tmp_cheese_width, tmp_cheese_height, in_start, tmp_extend, Arc2D.PIE );
			
			if (tmp_cheese_thickness > 0)
			{				    
				/* Dessiner la partie basse de la portion en premier */
				_m_Graphics2D.setPaint(tmp_grad_paint);
				_m_Graphics2D.fill(tmp_lower_portion);

				//_m_graphic.setColor(Color.BLACK);
				//_m_graphic.draw(new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y + tmp_cheese_thickness, tmp_cheese_width, tmp_cheese_height, in_start, tmp_extend, Arc2D.OPEN ));

		        /* Tracer les losanges de c�t�s */
			    _drawPortionSides(in_index, in_start, in_end,
				        tmp_draw_left && in_start >= 180, tmp_draw_right && in_start >= 180 && in_end > 180,
				        tmp_center_x, tmp_center_y, tmp_cheese_thickness,
				        tmp_grad_paint);
				
				/* Dessiner le bord de portion du fromage */
				tmp_first_x = tmp_center_x + Math.cos(Math.toRadians(in_start)) * tmp_cheese_width / 2;
				tmp_last_x = tmp_center_x + Math.cos(Math.toRadians(in_end)) * tmp_cheese_width / 2;

			    for (tmp_x = tmp_first_x; (tmp_x >= tmp_last_x && tmp_f < 0) || (tmp_x <= tmp_last_x && tmp_f > 0) || (in_end >= 180 && tmp_f < 0); tmp_x += tmp_f)
		        {
		            if (tmp_x <= tmp_upper_left_x)	
		            {
		                tmp_f = 1;
					    
		    			_m_Graphics2D.setPaint(Color.BLACK);
		    			_m_Graphics2D.draw(new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y + tmp_cheese_thickness, tmp_cheese_width, tmp_cheese_height, in_start, 180 - in_start, Arc2D.OPEN));
					    _drawPortionSides(in_index, in_start, in_end,
						        tmp_draw_left, tmp_draw_right,
						        tmp_center_x, tmp_center_y, tmp_cheese_thickness,
						        tmp_grad_paint);
		    			_m_Graphics2D.draw(new Line2D.Double(tmp_x, tmp_center_y, tmp_x, tmp_center_y + tmp_cheese_thickness));		    			
		            }
		            else
		            {
			            tmp_y = Math.sqrt(Math.pow(tmp_cheese_height / 2, 2) * (1 - (Math.pow(tmp_center_x - tmp_x, 2)/Math.pow(tmp_cheese_width / 2, 2)))); 
		    			_m_Graphics2D.setPaint(new GradientPaint(new Point2D.Double(tmp_x, tmp_center_y + tmp_f * tmp_y),
		    					tmp_paint_color,
		    					new Point2D.Double(tmp_x, tmp_center_y + tmp_f * tmp_y + tmp_cheese_thickness),
								tmp_paint_color.darker()));
			            _m_Graphics2D.draw(new Line2D.Double(tmp_x, tmp_center_y + tmp_f * tmp_y, tmp_x, tmp_center_y + tmp_f * tmp_y + tmp_cheese_thickness));
		            }
		        }

				_m_Graphics2D.setPaint(Color.BLACK);
		        if (in_end <= 180 || in_start >= 180)
		        {
					/* Tracer le segment de d�but de portion */
	    			if (tmp_draw_left)
	    			    if (in_index > 0)
	    			        _m_Graphics2D.draw(new Line2D.Double(_m_x_array[in_index - 1], _m_y_array[in_index - 1], _m_x_array[in_index - 1], _m_y_array[in_index - 1] + tmp_cheese_thickness));
	    			    else
	    			        _m_Graphics2D.draw(new Line2D.Double(tmp_upper_left_x + tmp_cheese_width, tmp_center_y, tmp_upper_left_x + tmp_cheese_width, tmp_center_y + tmp_cheese_thickness));
	    			        
					/* Tracer l'arc du bas */
	    			_m_Graphics2D.draw(new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y + tmp_cheese_thickness, tmp_cheese_width, tmp_cheese_height, in_start, tmp_extend, Arc2D.OPEN));
	    		}
		        else
		        {
					/* Tracer l'arc du bas */
	    			_m_Graphics2D.draw(new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y + tmp_cheese_thickness, tmp_cheese_width, tmp_cheese_height, 180, in_end - 180, Arc2D.OPEN));
		        }

		        /* Tracer les losanges de c�t�s */
			    _drawPortionSides(in_index, in_start, in_end,
				        tmp_draw_left && in_start < 180 && in_end <= 180, tmp_draw_right && in_end <= 180,
				        tmp_center_x, tmp_center_y, tmp_cheese_thickness,
				        tmp_grad_paint);

				if (in_end > 180)
				{
					/* Tracer le segment de fin de portion */
					_m_Graphics2D.setPaint(Color.BLACK);
			        _m_Graphics2D.draw(new Line2D.Double(_m_x_array[in_index], _m_y_array[in_index], _m_x_array[in_index], _m_y_array[in_index] + tmp_cheese_thickness));
				}
			}
			
			/* Dessiner la partie haute de la portion en dernier */
			_m_Graphics2D.setPaint(tmp_grad_paint);
			_m_Graphics2D.fill(tmp_upper_portion);

			_m_Graphics2D.setColor(Color.BLACK);
			_m_Graphics2D.draw(new Arc2D.Double(tmp_upper_left_x, tmp_upper_left_y, tmp_cheese_width, tmp_cheese_height, in_start, tmp_extend, Arc2D.OPEN ));
			if (tmp_draw_left)
			    if (in_index > 0)
			        _m_Graphics2D.draw(new Line2D.Double(_m_x_array[in_index - 1], _m_y_array[in_index - 1], tmp_center_x, tmp_center_y));
			    else
			        _m_Graphics2D.draw(new Line2D.Double(tmp_upper_left_x + tmp_cheese_width, tmp_center_y, tmp_center_x, tmp_center_y));

			if (tmp_draw_right)
			    _m_Graphics2D.draw(new Line2D.Double(_m_x_array[in_index], _m_y_array[in_index], tmp_center_x, tmp_center_y));
		    
		    if (tmp_origin_aft != null) _m_Graphics2D.setTransform(tmp_origin_aft);

		}
	}
	
	
	/**
	 * Trier les portions de fromage dans l'ordre o� elles doivent �tre dessin�es, afin d'�viter les chevauchements de dessins.
	 * @return chaine tri�e dont chaque maillon contient l'index de portion
	 */
	protected Chain orderedDrawingCheesePortions()
	{
		int				tmpIndex = 0;
		int				tmp_portion_index = 0;
		int				tmp_last_portion_index = 0;
		double			tmp_total_value = _m_trinomes_array.m_total_y.doubleValue() > 0.0 ? _m_trinomes_array.m_total_y.doubleValue() : _m_trinomes_array.length;

		double 			tmp_current_angle = 0;
		double 			tmp_previous_angle = 0;

		double 			tmp_cheese_width = _m_graph_width - _m_delta_cheese_portion * 2;
		double 			tmp_cheese_height = (_m_graph_height - _m_delta_cheese_portion * 2) * Math.sin(Math.toRadians(_m_y_incline));

		double 			tmp_center_x = _m_graph_origin_x + _m_graph_width / 2;
		double 			tmp_center_y = _m_graph_origin_y - _m_graph_height + _m_graph_height * Math.sin(Math.toRadians(_m_y_incline)) / 2;
		
		Chain			tmp_ordered_portions = new Chain();

		_m_angles_array = new double[_m_trinomes_array.length];
		
		/* Parcourrir la liste des valeurs */
		for(tmpIndex = 0; tmpIndex < _m_trinomes_array.length; tmpIndex++)
		{
			if (_m_trinomes_array.m_y_objects[tmpIndex].isNumber())
			{
				/* Calcul de l'angle de la portion courante pris sur les 360� */
				tmp_current_angle = 360.0 * ((Double)_m_trinomes_array.m_y_objects[tmpIndex].m_data).doubleValue() / tmp_total_value;
				_m_angles_array[tmpIndex] = tmp_previous_angle + tmp_current_angle;
				
				/* Coordonn�es du dernier point de la portion courante sur le cercle */
				_m_x_array[tmpIndex] = tmp_center_x + Math.cos(Math.toRadians(_m_angles_array[tmpIndex])) * tmp_cheese_width / 2;
				_m_y_array[tmpIndex] = tmp_center_y - Math.sin(Math.toRadians(_m_angles_array[tmpIndex])) * tmp_cheese_height /2;

				if (tmp_current_angle > 0)
				{				    
					/* Placer la portion selon l'ordre de son dessin */
					if (tmp_previous_angle < 90 || _m_angles_array[tmpIndex] <= 90)
					{
					    /* Mettre en permier les portions � cheval sur l'angle des 90 degr�s du fromage */
					    tmp_ordered_portions.insertObjectAtIndex(new Integer(tmpIndex), 0);
					    tmp_last_portion_index = tmp_portion_index + 1;
					}
					else
					{
					    /* Mettre les portions plac�es au del� des 270 degr�s du fromage 
					     * avant la derni�re plac�e avant les 270 degr�s */
						if (tmp_previous_angle >= 270 && _m_angles_array[tmpIndex] > 270)
						{
						    tmp_ordered_portions.insertObjectAtIndex(new Integer(tmpIndex), tmp_last_portion_index);
						}
						else
						{
						    /* Mettre la portion courante � la suite des autres */
						    tmp_ordered_portions.add(new Integer(tmpIndex));
						    tmp_last_portion_index = tmp_portion_index;
						}
					}
					    
					tmp_previous_angle = tmp_previous_angle + tmp_current_angle;
				    tmp_portion_index++;

				}
			}
		}
		
		return tmp_ordered_portions;
	}
	
	
	/**
	 * Dessiner le fromage portion par portion.
	 */
	protected void drawCheese()
	{
	    int			tmpIndex = 0;
	    int			tmpNextIndex = 0;

	    double		tmp_previous_angle = 0;

		Object		tmpObject = null;

	    /* Trier les portions par ordre de dessin */
	    Chain	tmp_chain_portions = orderedDrawingCheesePortions();
	    
	    /* Parcourrir la chaine de portions tri�es et les dessiner */
	    while (tmp_chain_portions != null)
	    {
	        tmpObject = tmp_chain_portions.getData();
			
			if (tmpObject != null && tmpObject instanceof Integer == true)
			{
				tmpIndex = ((Integer)tmpObject).intValue();
				
				tmp_previous_angle = tmpIndex > 0 ? _m_angles_array[tmpIndex - 1] : 0;
				
				/* Traiter le chevauchement pour les portions dont le d�but est inf�rieur � 90�
				 * et la fin sup�rieur � 270� */
				if (tmp_chain_portions.next() != null && tmp_previous_angle < 90 && _m_angles_array[tmpIndex] > 270) 
				{
				    tmp_chain_portions = tmp_chain_portions.next();
				    
				    /* Dessiner la premiere tranche de portion inf�rieure � 90� jusqu'� 180� */
					drawCheesePortion(tmpIndex, tmp_previous_angle, 180);
					
				    /* Dessiner les portions suivantes dans la liste */
				    while (tmp_chain_portions != null)
				    {
						tmpObject = tmp_chain_portions.getData();
						if (tmpObject != null && tmpObject instanceof Integer == true)
						{
						    tmpNextIndex = ((Integer)tmpObject).intValue();
						    drawCheesePortion(tmpNextIndex, tmpNextIndex > 0 ?_m_angles_array[tmpNextIndex - 1] : 0, _m_angles_array[tmpNextIndex]);
						}
					    tmp_chain_portions = tmp_chain_portions.next();
				    }
					
				    /* Dessiner la seconde tranche de portion de 180� et sup�rieure � 270� */
					drawCheesePortion(tmpIndex, 180, _m_angles_array[tmpIndex]);
				}
				/* Traitement normal du dessin d'une portion � la suite des autres */
				else
				{
					drawCheesePortion(tmpIndex, tmp_previous_angle, _m_angles_array[tmpIndex]);
			        tmp_chain_portions = tmp_chain_portions.next();
				}
			}else
		        tmp_chain_portions = tmp_chain_portions.next();
			
	    }
	    
	    
		/* Dessiner les libell�s de portions */
		for (tmpIndex = 0; tmpIndex < _m_trinomes_array.length; tmpIndex++)
		{
		    drawCheesePortionLegend(tmpIndex);
		}
	}
	
	
	/**
	 * Dessiner la l�gende d'une portion du fromage
	 * @param in_index	index de la portion
	 */
	protected void drawCheesePortionLegend(int in_index)
	{
		double 			tmp_cheese_height = _m_graph_height * Math.sin(Math.toRadians(_m_y_incline));

		double 			tmp_center_x = _m_graph_origin_x + _m_graph_width / 2;
		double 			tmp_center_y = _m_graph_origin_y - _m_graph_height + tmp_cheese_height / 2;

		Point2D			tmp_portion_center = null;
		
		TextLayout		tmp_str_layout = null;
		
		double			tmp_extend = _m_angles_array[in_index] - (in_index > 0 ? _m_angles_array[in_index - 1] : 0);
		double			tmp_center_angle = _m_angles_array[in_index] - tmp_extend / 2;
		    
		if (tmp_extend > 0)
		{
			/* Calcul de la position o� �crire le pourcentage de la portion */
			tmp_portion_center = translateOf(tmp_center_x + Math.cos(Math.toRadians(tmp_center_angle)) * _m_graph_width / 2, 
			        tmp_center_y - Math.sin(Math.toRadians(tmp_center_angle)) * tmp_cheese_height /2,
			        tmp_center_x,
			        tmp_center_y,
			        0.3);

			/* Dessiner le pourcentage de la valeur Y */
			tmp_str_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), 
			        					NumberFormat.getPercentInstance().format(tmp_extend / 360.0) 
			        					+ " (" + _m_trinomes_array.m_y_objects[in_index].toString() + ")",
			        					_m_graph_font,
			        					-1);
			if (tmp_str_layout != null)
			    tmp_str_layout.draw(_m_Graphics2D,
			            (float)(tmp_portion_center.getX() - tmp_str_layout.getBounds().getWidth() / 2),
			            (float)(tmp_portion_center.getY() + tmp_str_layout.getBounds().getHeight() / 2));
			
			/* Dessiner la valeur de la s�rie X courante */
			if (_m_z_categories_count > 1)
			{
				tmp_str_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), _m_trinomes_array.m_x_objects[in_index].toString(),
						_m_graph_font,
						-1);
				if (tmp_str_layout != null)
				    tmp_str_layout.draw(_m_Graphics2D,
				            (float)(tmp_portion_center.getX() - tmp_str_layout.getBounds().getWidth() / 2),
				            (float)(tmp_portion_center.getY() + tmp_str_layout.getBounds().getHeight() / 2 + _m_graph_font_size + 1));
			}
		}
	}
	
	
	/**
	 * Dessiner le graphe
	 */
	public void draw()
	{
		TextLayout	tmp_title_layout = null;
		
		if (_m_trinomes_array != null && _m_trinomes_array.length >0)
		{
		    NSLog.out.appendln("[MOChart] dessiner :");
		    
		    /* Initialiser les param�tres du graphique avant le dessin */
		    initDrawing();
		    
			/* Dessiner les axes */
		    NSLog.out.appendln("[MOChart] \t- les axes");
			drawAxis();

			/* Dessiner le graphiqe selon le type */
		    NSLog.out.appendln("[MOChart] \t- le graphique");
			switch (_m_type)
			{
				case GRAPH_TYPE_HISTOGRAM :
					drawHistogram();
					break;
				case GRAPH_TYPE_POLYGON :
					drawPolygon();
					break;
				case GRAPH_TYPE_CHEESE :
					drawCheese();
					break;
			}

			/* Dessiner la l�gende */
		    NSLog.out.appendln("[MOChart] \t- la legende");
			drawLegend();

			/* Dessiner le titre */
		    NSLog.out.appendln("[MOChart] \t- le titre");
			drawTitle();

			/* Dessiner le copyright */
		    NSLog.out.appendln("[MOChart] \t- le copyright");
			_m_Graphics2D.setColor(Color.BLACK);
			tmp_title_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), "� Mediware 2005", _m_graph_font.deriveFont(Font.BOLD), -1);
			if (tmp_title_layout != null)
			    tmp_title_layout.draw(_m_Graphics2D,
			            (float)(_m_graph_origin_x + _m_graph_width - tmp_title_layout.getBounds().getWidth() - 5),
			            (float)(_m_graph_origin_y - 5 ));
		}
		else
		{
			_m_Graphics2D.setColor(Color.RED);
			tmp_title_layout = layoutFor(_m_Graphics2D.getFontRenderContext(), "Aucune donn�e � afficher.", _m_graph_font.deriveFont(Font.BOLD), -1);
			if (tmp_title_layout != null)
			    tmp_title_layout.draw(_m_Graphics2D,
			            (float)(_m_width - tmp_title_layout.getBounds().getWidth()) / 2,
			            (float)(_m_height - tmp_title_layout.getBounds().getHeight()) / 2);
		}
	}
	

	/**
	 * Dessiner le graphe d'apr�s le type pass� en param�tre
	 * @param in_graph_type : une des valeurs GRAPH_TYPE_HISTOGRAM, GRAPH_TYPE_POLYGON...etc
	 */
	public void drawGraph (int in_graph_type)
	{
		_m_type = in_graph_type;
		
		draw();
	}
		
	
    /**
     * Renvoyer un objet <code>null</code> en cas de tentative d'acc�s � une propri�t� de classe inconnue
     */
    public Object handleQueryWithUnboundKey(String arg0)
    {
        return null;
    }


    /**
     * Ne rien faire en cas de tentative d'assignement d'une r�f�rence � une propri�t� de classe inconnue
     */
    public void handleTakeValueForUnboundKey(Object arg0, String arg1) 
    {
    }


    /**
     * Ne rien faire en cas de tentative d'assignement d'une valeur nulle � une propri�t� de classe de type primitif (<code>byte, char, int, float, double</code>).
     */
    public void unableToSetNullForKey(String arg0) 
    {
    }

/*-----------------------------------------------------------------
						GETTERS ET SETTERS
------------------------------------------------------------------*/    
    
    /**
     * Renvoie la couleur de dessin du graphique au format html/javascript<br>
     * ex : #FF0000
     */
	public String getGraphColor(){return javaColorToJsColor(_m_pencil_Color);}


	/**
     * Renvoie la couleur du titre au format html/javascript<br>
     * ex : #FF0000
     */
	public String getTitleColor(){return javaColorToJsColor(_m_graph_title_color);}


    /**
     * D�finir la couleur de dessin du graphique
     * @param	in_js_color	couleur au format html/javascript (ex : #FF0000)
     */
	public void setGraphColor(String in_js_color){setColors(jsColorToJavaColor(in_js_color), null);}


    /**
     * D�finir la couleur du titre
     * @param	in_js_color	couleur au format html/javascript (ex : #FF0000)
     */
	public void setTitleColor(String in_js_color){_m_graph_title_color = jsColorToJavaColor(in_js_color);}


    /**
     * D�finir les couleurs de dessin des cat�gories.
     * @param	in_colors	tableau d'objets de type <code>Color</code> pour les couleurs java,<br>
     * o� de type <code>String</code> pour les couleurs au format html/javascript
     */
	public void setGraphColors(Object in_colors[])
	{
	    int	tmp_index = 0;
	    
	    if (in_colors != null && in_colors.length > 0)
	    {
		    _m_colors = new Color[in_colors.length];
		    
		    for (tmp_index = 0; tmp_index < in_colors.length; tmp_index++)
		    {
		        if (in_colors[tmp_index] instanceof Color)
		            _m_colors[tmp_index] = (Color)in_colors[tmp_index];
		        else if (in_colors[tmp_index] instanceof String)
		            _m_colors[tmp_index] = jsColorToJavaColor((String)in_colors[tmp_index]);
		    }
		    
		    _m_pencil_Color = _m_colors[0];
	    }
	    else
	    {
	        _m_colors = COLORS;
	    }
	}


	/**
     * Renvoie le taux d'opacit� ( != transparence) du graphique.
     */
	public int getGraphOpacity(){return 100 * _m_graph_alpha_paint_color / 0xFF;}


	/**
     * D�finir le taux d'opacit� ( != transparence) du graphique.
     * @param	in_alpha	taux d'opacit�
     */
	public void setGraphOpacity(int in_alpha)
	{
	    if (in_alpha >= 0 && in_alpha <= 100)
	    {
	        _m_graph_alpha_paint_color = in_alpha * 0xFF / 100;
	    }
	}
	
	
    /**
     * Renvoie la largeur du graphique en nombre de pixels.
     */	
	public double getGraphWidth() {return _m_graph_width;}

	
    /**
     * Renvoie la hauteur du graphique en nombre de pixels.
     */	
	public double getGraphHeight() {return _m_graph_height;}
	
	
    /**
     * Renvoie l'inclinaison vertical du graphique en degr�s celcius.
     */	
	public double getInclineY() {return _m_y_incline;}
	
	
    /**
     * D�finir l'inclinaison vertical du graphique en degr�s celcius.
     * @param	in_incline	angle d'inclinaison vertical
     */	
	public void setInclineY(int in_incline) 
	{
		if (in_incline >= 0.0 && in_incline <= 90.0)
		{
		    _m_y_incline = in_incline;
		}
	}

	
    /**
     * Renvoie l'inclinaison horizontal du graphique en degr�s celcius.
     */	
	public double getInclineX() {return _m_x_incline;}
	
	
    /**
     * D�finir l'inclinaison horizontal du graphique en degr�s celcius.
     * @param	in_incline	angle d'inclinaison vertical
     */	
	public void setInclineX(int in_incline)
	{
		if (in_incline >= 0.0 && in_incline <= 90.0)
		{
		    _m_x_incline = in_incline;
		}
	}


    /**
     * Renvoie la taille de la bordure � gauche du graphique exprim�e en nombre de pixels.
     */	
	public double getBorderLeft() {return _m_graph_origin_x;}

	
    /**
     * Renvoie la taille de la bordure � droite du graphique exprim�e en nombre de pixels.
     */	
	public double getBorderRight() {return _m_width - _m_graph_width - _m_graph_origin_x;}

	
    /**
     * Renvoie la taille de la bordure en haut du graphique exprim�e en nombre de pixels.
     */	
	public double getBorderTop() {return _m_graph_origin_y - _m_graph_height;}

	
    /**
     * Renvoie la taille de la bordure en bas du graphique exprim�e en nombre de pixels.
     */	
	public double getBorderBottom() {return _m_height - _m_graph_origin_y;}

	
    /**
     * D�finir la taille de la bordure � gauche du graphique.
     * @param	in_size	taille exprim�e en nombre de pixels
     */	
	public void setBorderLeft(double in_size){setBorderSize(in_size, -1, -1, -1);}

	
    /**
     * D�finir la taille de la bordure � droite du graphique.
     * @param	in_size	taille exprim�e en nombre de pixels
     */	
	public void setBorderRight(double in_size){setBorderSize(-1, in_size, -1, -1);}
	
	
    /**
     * D�finir la taille de la bordure en haut du graphique.
     * @param	in_size	taille exprim�e en nombre de pixels
     */	
	public void setBorderTop(double in_size){setBorderSize(-1, -1, in_size, -1);}
	
	
    /**
     * D�finir la taille de la bordure en bas du graphique.
     * @param	in_size	taille exprim�e en nombre de pixels
     */	
	public void setBorderBottom(double in_size){setBorderSize(-1, -1, -1, in_size);}
	
	
	/**
	 * D�finir le titre.
	 * @param in_title	cha�ne de caract�res
	 */
	public void setTitle(String in_title){_m_title = in_title;}
	
	
	/**
	 * Renvoie le titre du graphique.
	 */
	public String getTitle(){return _m_title;}
	
	
	/**
	 * Renvoie la police d'affichage du titre.
	 * @return	un objet de type
	 * @see java.awt.Font
	 */
	public Font getFontTitle(){return _m_title_font;}
	
	
	/**
	 * D�finir la police d'affichage du titre.
	 * @param in_font	un objet de type
	 * @see java.awt.Font
	 */
	public void setFontTitle(Font in_font)
	{
	    if (in_font != null && !in_font.equals(_m_title_font))
	    {
	        _m_title_font = in_font;
	    }
	}
	
	
	/**
	 * D�finir l'espacement entre les portions d'un fromage.
	 * @param in_delta	exprim�e en nombre de pixels
	 */
	public void setCheesePortionSpace(int in_delta){_m_delta_cheese_portion = in_delta;}

	
	/**
	 * Renvoie l'espacement entre les portions d'un fromage.
	 */
	public int getCheesePortionSpace(){return (int)_m_delta_cheese_portion;}
	
	
	/**
	 * Renvoie le tableau des abcisses des points du graphique
	 * @return
	 */
	public double[] getHorizontalCoords()
	{
		return _m_x_array;
	}
	
	
	/**
	 * Renvoie le tableau des ordonn�es des points du graphique
	 * @return
	 */
	public double[] getVerticalCoords()
	{
		return _m_y_array;
	}
}
