//================================================================================
//CLASSE : 	Chain
//FICHIER :	Chain.java
//PROJET : 	MOTools
//CREE PAR : 	EJ
//
//RESUME : Cette classe permet de g�rer une cha�ne d'objets.
//
//================================================================================
//VERSION	DATE			AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0		18 f�vr. 2005	EJ		Cr�ation du fichier.
//================================================================================


/**
 * Cette classe permet de g�rer une cha�ne d'objets.
 */
public class Chain
{
    
    /**
     * Donn�es
     */
    private Object		_m_data;
    
    /**
     * Maillon pr�c�dent
     */
    private	Chain		_m_previous;
    
    /**
     * Maillon suivant
     */
    private Chain		_m_next;
    
    
    /**
     * Constructeur
     */
    public Chain()
    {
        super();
    }
    
    
    /**
     * Constructeur
     */
    public Chain(Object in_data)
    {
        this();
        _m_data = in_data;
    }
    
    
    /**
     * Renvoyer l'objet � la position <code>in_index</code> dans la chaine
     * @param in_index	position de l'objet cherch�
     * @return
     */
    public Object objectAtIndex(int in_index)
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
        
        if (in_index < 0) return null;
        
        while (tmp_chain != null)
        {
            if (in_index == tmp_index)	return tmp_chain._m_data;
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
        
        return null;
    }
    
    
    /**
     * Renvoyer le maillon � la position <code>in_index</code>
     * @param in_index	position du maillon cherch�
     * @return
     */
    public Chain chainAtIndex(int in_index)
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
        
        if (in_index < 0) return null;
        
        while (tmp_chain != null)
        {
            if (in_index == tmp_index)	return tmp_chain;
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
        
        return null;
    }

    
    /**
     * Ins�rer un objet dans la cha�ne
     * @param in_new_object	nouvel objet
     * @param in_index	position d'insertion
     */
    public void insertObjectAtIndex(Object in_new_object, int in_index)
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
        Chain	tmp_new_chain = null;
        
        if (in_index < 0) return;
        
        while (tmp_chain != null)
        {
            if (in_index == tmp_index)
            {
                tmp_new_chain = new Chain(tmp_chain._m_data);
                tmp_new_chain._m_previous = tmp_chain;
                tmp_new_chain._m_next = tmp_chain._m_next;
                tmp_chain._m_next = tmp_new_chain;
                tmp_chain._m_data = in_new_object;
                return;
            }
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
    }
    
    
    /**
     * Supprimer un obet de la cha�ne
     * @param in_index	position de l'objet � supprimer
     */
    public void removeObjectAtIndex(int in_index)
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
        
        if (in_index < 0) return;
        
        while (tmp_chain != null)
        {
            if (in_index == tmp_index)
            {
                tmp_chain._m_data = tmp_chain._m_next._m_data;
                tmp_chain._m_next = tmp_chain._m_next._m_next;
                tmp_chain._m_next._m_previous = tmp_chain;
            }
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
    }
    
    
    /**
     * Chercher un objet dans une cha�ne
     * @param in_object	objet � chercher
     * @return	index de l'objet trouv� ou -1 si l'objet n'a pas �t� trouv�
     */
    public int indexOfObject(Object in_object)
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
        
        while (tmp_chain != null)
        {
            if (tmp_chain._m_data.equals(in_object))	return tmp_index;
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
        
        return -1;
    }
    
    
    /**
     * Longueur de la cha�ne
     * @return
     */
    public int length()
    {
        int		tmp_index = 1;
        Chain	tmp_chain = _m_next;
        
        while (tmp_chain != null)
        {
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
        
        return tmp_index;        
    }
    
    
    /**
     * Ajouter un maillon 
     * @param in_new_object contenu du nouveau maillon
     */
    public void add(Object in_new_object)
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
         
        while (tmp_chain._m_next != null)
        {
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
        
        tmp_chain._m_next = new Chain(in_new_object);
        tmp_chain._m_next._m_previous = tmp_chain;
    }
    
    
    /**
     * Ajouter un maillon
     */
    public void add()
    {
        int		tmp_index = 0;
        Chain	tmp_chain = this;
        
        while (tmp_chain._m_next != null)
        {
            tmp_chain = tmp_chain._m_next;
            tmp_index++;
        }
        
        tmp_chain._m_next = new Chain();
        tmp_chain._m_next._m_previous = tmp_chain;
    }
    
    
    /**
     * D�finir le contenu du maillon courant
     * @param in_new_object
     */
    public void setData(Object in_new_object)
    {
        _m_data = in_new_object;
    }
    
    
    /**
     * D�finir le contenu du maillon � une certaine position
     * @param in_new_object
     */
    public void setDataAtIndex(Object in_new_object, int in_index)
    {
        Chain	tmp_chain = chainAtIndex(in_index);
        
        if (tmp_chain != null)	tmp_chain.setData(in_new_object);
    }
    
    
    /**
     * R�cup�rer le contenu du maillon courant
     * @return
     */
    public Object getData()
    {
        return _m_data;
    }
    
    
    /**
     * R�cup�rer le contenu du maillon � une certaine position
     * @return
     */
    public Object getDataAtIndex(int in_index)
    {
        Chain	tmp_chain = chainAtIndex(in_index);
        
        if (tmp_chain != null)	return tmp_chain.getData();
        
        return null;
    }
    
    
    /**
     * Maillon suivant
     * @return
     */
    public Chain next()
    {
        return _m_next;
    }
    
    
    /**
     * Maillon pr�c�dent
     * @return
     */
    public Chain previous()
    {
        return _m_previous;
    }
    
    
}
