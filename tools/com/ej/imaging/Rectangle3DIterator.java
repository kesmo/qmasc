//================================================================================
//CLASSE : 	Rectangle3DIterator
//FICHIER :	Rectangle3DIterator.java
//PROJET : 	MOTools
//CREE PAR : 	EJ
//
//RESUME : 	Classe de gestion du chemin d'un Rectangle3D
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			15-AVR-03	EJ		Cr�ation du fichier.
//================================================================================

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.util.NoSuchElementException;

/**
 * Classe de gestion du chemin d'un Rectangle3D.
 */
public class Rectangle3DIterator implements PathIterator 
{
    /**
     * Le rectangle3D
     */
    Rectangle3D 	m_losange = null;
    
    /**
     * Transformation affine
     */
    AffineTransform m_affine;
    
    /**
     * Index du segment courant du chemin
     */
    int 			m_index;

    
    /**
     * Constructeur
     * @param in_losange	le Rectangle3D
     * @param in_at		transformation affine	
     */
    Rectangle3DIterator(Rectangle3D in_losange, AffineTransform in_at)
	{
        m_losange = in_losange;
        m_affine = in_at;
    }
    

    /**
     * Retourn� la r�gle de pliage � l'int�rieur du chemin
     * @see #WIND_EVEN_ODD
     * @see #WIND_NON_ZERO
     */
    public int getWindingRule() 
    {
        return WIND_NON_ZERO;
    }

    
    /**
     * Est-ce la fin du chemin ?
     * @return true si il y a d'autres points dans le chemin
     */
    public boolean isDone()
    {
        return m_index > 4;
    }

    /**
     * Segment suivant du chemin
     */
    public void next()
    {
        m_index++;
    }

    /**
     * Renvoie les coordonn�es et type du segment courant
     * @param Un tableaux de 6 coordonn�es doit �tre pass� en param�tres<br>
     * pour enregistrer les coordonn�es des points.<br>
     * Chaque point est enregistr� sous forme de paire de coordonn�es :<br>
     * <ul><li>les types SEG_MOVETO et SEG_LINETO retournent un seul point</li>
     * <li>SEG_QUADTO retourne deux points</li>
     * <li>SEG_CUBICTO retourne 3 points</li>
     * <li>SEG_CLOSE ne retourne aucun point</li></ul>
     * @return le type renvoy� :<br>
     * SEG_MOVETO, SEG_LINETO, SEG_QUADTO, SEG_CUBICTO, ou SEG_CLOSE.
     */
    public int currentSegment(float[] in_coords)
    {
		if (isDone()) 
		{
		    throw new NoSuchElementException("quadrilater iterator out of bounds");
		}
		if (m_index == 4)
		{
		    return SEG_CLOSE;
		}
		
		in_coords[0] = (float)m_losange.m_coords[m_index][0];
		in_coords[1] = (float)m_losange.m_coords[m_index][1];
		
		if (m_affine != null)
		{
		    m_affine.transform(in_coords, 0, in_coords, 0, 1);
		}
		
		return (m_index == 0 ? SEG_MOVETO : SEG_LINETO);
    }

    
    /**
     * Renvoie les coordonn�es et type du segment courant
     * @param Un tableaux de 6 coordonn�es doit �tre pass� en param�tres<br>
     * pour enregistrer les coordonn�es des points.<br>
     * Chaque point est enregistr� sous forme de paire de coordonn�es :<br>
     * <ul><li>les types SEG_MOVETO et SEG_LINETO retournent un seul point</li>
     * <li>SEG_QUADTO retourne deux points</li>
     * <li>SEG_CUBICTO retourne 3 points</li>
     * <li>SEG_CLOSE ne retourne aucun point</li></ul>
     * @return le type renvoy� :<br>
     * SEG_MOVETO, SEG_LINETO, SEG_QUADTO, SEG_CUBICTO, ou SEG_CLOSE.
     */
    public int currentSegment(double[] in_coords) 
    {
		if (isDone()) 
		{
		    throw new NoSuchElementException("Rectangle3D iterator out of bounds");
		}
		
		if (m_index == 4) 
		{
		    return SEG_CLOSE;
		}
		
		in_coords[0] = m_losange.m_coords[m_index][0];
		in_coords[1] = m_losange.m_coords[m_index][1];
		
		if (m_affine != null) {
		    m_affine.transform(in_coords, 0, in_coords, 0, 1);
		}
		return (m_index == 0 ? SEG_MOVETO : SEG_LINETO);
    }

}
