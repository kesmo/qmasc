//================================================================================
//CLASSE : 	Rectangle3D
//FICHIER :	Rectangle3D.java
//PROJET : 	MOTools
//CREE PAR : 	EJ
//
//RESUME : 	Classe de gestion d'un Rectangle3D (losange en 2D)
//================================================================================
//VERSION		DATE		AUTEUR	COMMENTAIRES
//--------------------------------------------------------------------------------
//1.0			15-AVR-03	EJ		Cr�ation du fichier.
//================================================================================

import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * Classe de gestion d'un Rectangle3D.
 * Un rectangle3D est un plan rectangulaire ferm� dans un syst�me � 3 dimensions.<br>
 * Sa repr�sentation graphique en 2D n'est rien d'autre qu'un losange :<br>
 *              /|<br>
 *             / |<br>
 *            /  |<br>
 *           |   |<br>
 *           |  /<br>
 *           | /<br>
 *           |/<br>
 * De plus, elle d�pend de sa position et sa taille (longueur et largeur)<br>
 * dans le syst�me 3D des axes x,y et z ainsi que de l'inclinaison de ces axes.
 */
public class Rectangle3D implements Shape 
{

    /**
     * Coordonn�es des 4 points du losange
     */
    public double[][] m_coords = new double[4][2];
    
    /* Largeur et hauteur */
    private double _m_width = 0;
    private double _m_height = 0;
    

    /**
     * Constructeur
     * @param in_x : abcisse du coin gauche sup�rieur
     * @param in_y : ordonn�e du coin gauche sup�rieur
     * @param in_width : largeur
     * @param in_height : profondeur
     * @param in_x_incline : inclinaison horizontale
     * @param in_y_incline : inclinaison verticale
     * @param in_z_incline : inclinaison azimutale
     */
    public Rectangle3D(double in_x, double in_y, double in_width, double in_height, double in_x_incline, double in_y_incline, double in_z_incline)
    {
        double tmp_cos_z = in_width * Math.cos(Math.toRadians(in_z_incline));
        double tmp_sin_z = in_width * Math.sin(Math.toRadians(in_z_incline));
       
        m_coords[0][0] = in_x;
        m_coords[0][1] = in_y;
        m_coords[1][0] = in_x + in_height * Math.sin(Math.toRadians(in_y_incline));
        m_coords[1][1] = in_y + in_height * Math.sin(Math.toRadians(in_x_incline));
        m_coords[2][0] = m_coords[1][0] + tmp_cos_z;
        m_coords[2][1] = m_coords[1][1] + tmp_sin_z;
        m_coords[3][0] = in_x + tmp_cos_z;
        m_coords[3][1] = in_y + tmp_sin_z;
        
        _m_width = in_width;
        _m_height = in_height;
    }
    
    
    /**
     * Constructeur
     * @param in_rect : rectangle
     * @param in_x_incline : inclinaison horizontale
     * @param in_y_incline : inclinaison verticale
     * @param in_z_incline : inclinaison azimutale
     */
    public Rectangle3D(Rectangle in_rect, double in_x_incline, double in_y_incline, double in_z_incline)
    {
        this(in_rect.getX(), in_rect.getY(), in_rect.getWidth(), in_rect.getHeight(), in_x_incline, in_y_incline, in_z_incline);
    }
    
    
    /**
     * Le losange contient-il le point ?
     * @param in_x	abcisse du point
     * @param in_y	ordonn�e du point
     */
    public boolean contains(double in_x, double in_y)
    {
        double tmp_m = 0;
        double tmp_p = 0;

        if (in_y < m_coords[0][1] || in_y > m_coords[1][1]) return false;
        
        tmp_m = (m_coords[0][1] - m_coords[1][1]) / (m_coords[0][0] - m_coords[1][0]);
        tmp_p = m_coords[0][1] - tmp_m * m_coords[0][0];
        if (in_y < tmp_m * in_x + tmp_p) return false;

        tmp_m = (m_coords[2][1] - m_coords[3][1]) / (m_coords[2][0] - m_coords[3][0]);
        tmp_p = m_coords[2][1] - tmp_m * m_coords[2][0];
        if (in_y > tmp_m * in_x + tmp_p) return false;
        
        return true;
    }


    /**
     * Le losange contient-il le segment ?
     * @param in_x	abcisse du point origine
     * @param in_y	ordonn�e du point origine
     * @param in_w	largeur relative
     * @param in_h	hauteur relative
     */
    public boolean contains(double in_x, double in_y, double in_w, double in_h)
    {
        return contains(in_x, in_y) 
        	&& contains(in_x + in_w, in_y) 
        	&& contains(in_x, in_y + in_h) 
        	&& contains(in_x + in_w, in_y + in_h);
    }

    
    /**
     * Le segment coupe-t'il le losange ?
     * @param in_x	abcisse du point origine
     * @param in_y	ordonn�e du point origine
     * @param in_w	largeur relative
     * @param in_h	hauteur relative
     */
    public boolean intersects(double in_x, double in_y, double in_w, double in_h)
    {
        return contains(in_x, in_y) 
        	|| contains(in_x + in_w, in_y) 
        	|| contains(in_x, in_y + in_h)
        	|| contains(in_x + in_w, in_y + in_h);
    }

    
    /**
     * Renvoie un objet Rectangle � partir des coordonn�es du Rectangle3D
     */
    public Rectangle getBounds()
    {
        return new Rectangle((int)m_coords[0][0], (int)m_coords[0][1], (int)_m_width, (int)_m_height);
    }

    
    
    /**
     * Le losange contient-il le point ?
     * @param in_p	le point
     */
    public boolean contains(Point2D in_p)
    {
        return contains(in_p.getX(), in_p.getY());
    }


    /**
     * Renvoie un objet Rectangle2D � partir des coordonn�es du Rectangle3D
     */
    public Rectangle2D getBounds2D() 
    {
        return new Rectangle2D.Double(m_coords[0][0], m_coords[0][1], _m_width, _m_height);
    }

    
    /**
     * Le losange contient-il le rectangle ?
     * @param in_rect	le rectangle contenu
     */    
    public boolean contains(Rectangle2D in_rect)
    {
        return contains(in_rect.getX(), in_rect.getY(), in_rect.getWidth(), in_rect.getHeight());
    }

    
    /**
     * Le rectangle pass� en param�tre intersecte-t'il le Rectangle3D
     * @param in_rect	rectangle
     */
    public boolean intersects(Rectangle2D in_rect) {
        return intersects(in_rect.getX(), in_rect.getY(), in_rect.getWidth(), in_rect.getHeight());
    }

    
    /**
     * Renvoie le chemin du Rectangle3D d'apr�s la transformation affine pass�e en param�tre
     * @param in_at	transformation affine
     */
    public PathIterator getPathIterator(AffineTransform in_at) 
    {
        return new Rectangle3DIterator(this, in_at);
    }


    /**
     * Renvoie le chemin du Rectangle3D d'apr�s la transformation affine<br>
     * et la platitude pass�es en param�tre
     * @param in_flatness	platitude
     * @param in_at	transformation affine
     */
    public PathIterator getPathIterator(AffineTransform in_at, double in_flatness) {
        return new Rectangle3DIterator(this, in_at);
    }

}
