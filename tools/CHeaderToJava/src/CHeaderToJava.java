import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Permet de convertir un fichier d'entete C en un fichier d'interface Java 
 * @author ejorge
 *
 */
public class CHeaderToJava
{

	public static final int LINE_STANDARD = 0;
	
	public static final int LINE_CONST_CHAR = 1;
	
	public static final int LINE_STRUCT = 2;

	public static class LineType
	{
		private int _m_line_type = LINE_STANDARD;
		
		public void setLineType(int in_new_type){_m_line_type = in_new_type;};
		
		public int getLineType(){return _m_line_type;};
	}
	
	
	/**
	 * Programme principal
	 * @param args liste de fichiers et/ou dossiers
	 */
	public static void main(String[] args)
	{
		if (args != null && args.length > 0)
		{
			for (int i = 0; i < args.length; i++)
			{
				_transform(args[i]);
			}
		}
		else
			System.out.println("Usage : " + CHeaderToJava.class.getName() + " PATTERN...");
	}

	
	/**
	 * Transformer le fichier passé en paramètre
	 * @param in_filename
	 */
	private static void _transform(String in_filename)
	{
		File	tmp_file = new File(in_filename);
			
		if (tmp_file.exists())
		{
			if (tmp_file.isDirectory())
			{
				File[]	tmp_files = tmp_file.listFiles();
				for (int i = 0; i < tmp_files.length; i++)
				{
					_transform(tmp_files[i].getAbsolutePath());
				}
			}
			else if (tmp_file.isFile())
			{
				if (tmp_file.getName().endsWith(".h"))
				{
					System.out.println("Traitement de " + tmp_file.getAbsolutePath());
					_parseFile(tmp_file);
				}
			}
			else
				System.out.println("Type de fichier " + in_filename + " non traité.");
		}
		else
			System.out.println("Fichier " + in_filename + " inexistant.");
	}
	
	
	/**
	 * Parcourrir le contenu du fichier
	 * @param in_file
	 */
	private static void _parseFile(File in_file)
	{
		String						tmp_read_line = null;
		String						tmp_write_line = null;
		BufferedReader	tmp_file_reader;
		BufferedWriter	tmp_file_writer;
		LineType				tmp_line_type = new LineType();
		String[]					tmp_filename_str =in_file.getName().split("\\.");
			
		try
		{
			tmp_file_writer = new BufferedWriter(new FileWriter(tmp_filename_str[0] + ".java"));
			tmp_file_reader = new BufferedReader(new FileReader(in_file));
			
			tmp_file_writer.write("package org.ej;\n");
			tmp_file_writer.write("public interface ");
			tmp_file_writer.write(tmp_filename_str[0]);
			tmp_file_writer.write("\n{\n");
			
			tmp_read_line = tmp_file_reader.readLine();
			while (tmp_read_line != null)
			{
				tmp_write_line =_parseLine(tmp_read_line, tmp_line_type);
				if (tmp_write_line != null)
					tmp_file_writer.write(tmp_write_line);
				
				tmp_read_line = tmp_file_reader.readLine();
			}
			tmp_file_writer.write("\n}\n");
			
			tmp_file_writer.close();
			tmp_file_reader.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Convertir la ligne courante
	 * @param in_source_line
	 * @return
	 */
	private static String _parseLine(String in_source_line, LineType in_line_type)
	{
		String		tmp_write_buffer = in_source_line.trim();
		
		String[]	tmp_words;
		
		switch (in_line_type.getLineType())
		{
			/* Traitement d'une ligne standard */
			case LINE_STANDARD :
				if (tmp_write_buffer.startsWith("#define"))
				{
					tmp_words = tmp_write_buffer.split("\\s");
					if (_notEmptyStringAtIndex(tmp_words, 2) == null)
					{
						tmp_write_buffer = _commentedLine(tmp_write_buffer);
					}
					else
					{
						if (tmp_write_buffer.indexOf("\"") > 0)
							tmp_write_buffer = "public static final String " + _notEmptyStringAtIndex(tmp_words, 1) + " = " + _notEmptyStringAtIndex(tmp_words, 2) + ";";
						else if (tmp_write_buffer.indexOf("'") > 0)
							tmp_write_buffer = "public static final char " + _notEmptyStringAtIndex(tmp_words, 1) + " = " + _notEmptyStringAtIndex(tmp_words, 2) + ";";
						else
							tmp_write_buffer = "public static final int " + _notEmptyStringAtIndex(tmp_words, 1) + " = " + _notEmptyStringAtIndex(tmp_words, 2) + ";";
					}
				}
				else if (tmp_write_buffer.startsWith("#include") || tmp_write_buffer.startsWith("#ifdef") || tmp_write_buffer.startsWith("#ifndef")
						|| tmp_write_buffer.startsWith("#endif") || tmp_write_buffer.startsWith("extern") || tmp_write_buffer.startsWith("{")  || tmp_write_buffer.startsWith("}"))
				{
					tmp_write_buffer = _commentedLine(tmp_write_buffer);
				}
				else if (tmp_write_buffer.startsWith("typedef"))
				{
					tmp_write_buffer = _commentedLine(tmp_write_buffer);
					in_line_type.setLineType(LINE_STRUCT);
				}
				else if (tmp_write_buffer.startsWith("const char *"))
				{
					tmp_write_buffer = tmp_write_buffer.replaceAll("const char \\*", "public static final String ");
					if (tmp_write_buffer.endsWith(";") == false)
						in_line_type.setLineType(LINE_CONST_CHAR);
				}
				else if (tmp_write_buffer.startsWith("static const char *"))
				{
					tmp_write_buffer = tmp_write_buffer.replaceAll("static const char \\*", "public static final String ");
					if (tmp_write_buffer.endsWith(";") == false)
						in_line_type.setLineType(LINE_CONST_CHAR);
				}
				else if (tmp_write_buffer.startsWith("static const "))
				{
					tmp_write_buffer = _commentedLine(tmp_write_buffer);
					in_line_type.setLineType(LINE_STRUCT);
				}
				else if (tmp_write_buffer.endsWith(";"))
					tmp_write_buffer = _commentedLine(tmp_write_buffer);

				break;
				
				/* Traitement d'une ligne définisssant un tableau de chaine const char* */
			case LINE_CONST_CHAR:
				tmp_write_buffer = tmp_write_buffer.replaceAll("NULL", "null");
				 if (tmp_write_buffer.indexOf(";") >= 0)
						in_line_type.setLineType(LINE_STANDARD);
				break;

				/* Traitement d'un ligne définisssant une structure C */
			case LINE_STRUCT:
				 if (tmp_write_buffer.indexOf("}") >= 0)
						in_line_type.setLineType(LINE_STANDARD);
					tmp_write_buffer = _commentedLine(tmp_write_buffer);
				 
				break;
		}
		
		if (tmp_write_buffer.compareTo("") == 0)
			tmp_write_buffer = "\n";
		else
			tmp_write_buffer = "\t" + tmp_write_buffer + "\n";
		
		return tmp_write_buffer;
	}
	
	private static String _notEmptyStringAtIndex(String[] in_string_array, int in_string_index)
	{
		int		tmp_index = 0;
		
		for (int i = 0; i < in_string_array.length; i++) 
		{
			if (in_string_array[i] != null && in_string_array[i].length() > 0)
			{
				if (tmp_index == in_string_index)
					return in_string_array[i];
				
				tmp_index++;
			}
		}
		
		return null;
	}
	
	private static String _commentedLine(String in_line)
	{
		if (in_line.endsWith("*/"))
			return "/*C2J--" + in_line;
		else
			return "/*C2J--" + in_line + "*/";
	}

}
