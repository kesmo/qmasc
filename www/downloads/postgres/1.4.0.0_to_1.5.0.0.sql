\set ON_ERROR_STOP on

SET client_min_messages TO WARNING;
-- Ajout de la notion d'automatisation de test
ALTER TABLE tests_contents_table ADD COLUMN automated CHAR(1) DEFAULT 'N';
ALTER TABLE tests_contents_table ADD COLUMN automation_command VARCHAR(512);
ALTER TABLE tests_contents_table ADD COLUMN automation_command_parameters VARCHAR(512);
ALTER TABLE tests_contents_table ADD COLUMN type CHAR(1) DEFAULT 'N';
ALTER TABLE tests_contents_table ADD COLUMN limit_test_case CHAR(1) DEFAULT 'N';

COMMENT ON COLUMN tests_contents_table.automated IS 'Test automatise (N=No, Y=Yes)';
COMMENT ON COLUMN tests_contents_table.automation_command IS 'Ligne de commande utilisee pour l''automatisation';
COMMENT ON COLUMN tests_contents_table.automation_command_parameters IS 'Parametres de la ligne de commande utilisee pour l''automatisation';
COMMENT ON COLUMN tests_contents_table.type IS 'Type du test (N=Nominal, A=Alternatif, E=Exception)';
COMMENT ON COLUMN tests_contents_table.limit_test_case IS 'Cas aux limites (N=No, Y=Yes)';

CREATE OR REPLACE VIEW tests_contents AS select * from tests_contents_table;
GRANT SELECT ON tests_contents TO admin_role, writer_role, reader_role;

DROP VIEW TESTS_HIERARCHY;
CREATE OR REPLACE VIEW TESTS_HIERARCHY AS 
SELECT
	tests_contents_table.short_name AS short_name,
	tests_contents_table.category_id AS category_id,
	tests_contents_table.priority_level AS priority_level,
	tests_contents_table.version AS content_version,
	tests_table.test_id AS test_id,
	tests_table.test_content_id AS test_content_id,
	tests_table.original_test_id AS original_test_id,
	tests_table.parent_test_id AS parent_test_id,
	tests_table.previous_test_id AS previous_test_id,
	tests_table.project_id AS project_id,
	tests_table.version AS version,
	tests_contents_table.status AS status,
	tests_contents_table.original_test_content_id AS original_test_content_id,
	tests_contents_table.automated AS content_automated,
	tests_contents_table.type AS content_type
FROM 
	tests_contents_table, tests_table 
WHERE
	tests_contents_table.test_content_id = tests_table.test_content_id;
GRANT SELECT ON TESTS_HIERARCHY TO admin_role, writer_role, reader_role;

-- Ajout des actions d'automatisation de test
CREATE SEQUENCE automated_actions_action_id_seq;

CREATE TABLE automated_actions_table (
  automated_action_id bigint NOT NULL DEFAULT nextval('automated_actions_action_id_seq'),
  previous_automated_action_id bigint,
  test_content_id bigint NOT NULL,
  window_id VARCHAR(1024),
  message_type int,
  message_data VARCHAR(1024),
  message_time_delay int,
  PRIMARY KEY (automated_action_id),
  FOREIGN KEY (previous_automated_action_id) REFERENCES automated_actions_table(automated_action_id) ON DELETE SET NULL,
  FOREIGN KEY (test_content_id) REFERENCES tests_contents_table(test_content_id) ON DELETE CASCADE
);

COMMENT ON TABLE automated_actions_table IS 'Actions';
COMMENT ON COLUMN automated_actions_table.automated_action_id IS 'Identifiant unique de l''action';
COMMENT ON COLUMN automated_actions_table.previous_automated_action_id IS 'Identifiant de l''action precedente';
COMMENT ON COLUMN automated_actions_table.test_content_id IS 'Identifiant du contenu de test';
COMMENT ON COLUMN automated_actions_table.window_id IS 'Identifiant de la fenetre';
COMMENT ON COLUMN automated_actions_table.message_type IS 'Type de message envoye a la fenetre';
COMMENT ON COLUMN automated_actions_table.message_data IS 'Contenu du message envoye a la fenetre';
COMMENT ON COLUMN automated_actions_table.message_time_delay IS 'Delai avant traitement du message suivant';

CREATE OR REPLACE VIEW automated_actions AS select * from automated_actions_table;

GRANT SELECT, UPDATE ON automated_actions_action_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE automated_actions_table TO admin_role, writer_role;
GRANT SELECT ON TABLE automated_actions_table TO reader_role;
GRANT SELECT ON automated_actions TO admin_role, writer_role, reader_role;

-- Ajout des validations d'actions d'automatisation de test
CREATE SEQUENCE automated_actions_validations_validation_id_seq;

CREATE TABLE automated_actions_validations_table (
  validation_id bigint NOT NULL DEFAULT nextval('automated_actions_validations_validation_id_seq'),
  previous_validation_id bigint,
  automated_action_id bigint NOT NULL,
  module_name VARCHAR(128),
  module_version VARCHAR(64),
  module_function_name VARCHAR(128),
  module_function_parameters VARCHAR(256),  
  PRIMARY KEY (validation_id),
  FOREIGN KEY (previous_validation_id) REFERENCES automated_actions_validations_table(validation_id) ON DELETE SET NULL,
  FOREIGN KEY (automated_action_id) REFERENCES automated_actions_table(automated_action_id) ON DELETE CASCADE
);

COMMENT ON TABLE automated_actions_validations_table IS 'Validations des actions automatisees';
COMMENT ON COLUMN automated_actions_validations_table.validation_id IS 'Identifiant unique de la validation';
COMMENT ON COLUMN automated_actions_validations_table.previous_validation_id IS 'Identifiant de la validation precedente';
COMMENT ON COLUMN automated_actions_validations_table.automated_action_id IS 'Identifiant de l''action automatisee associee';
COMMENT ON COLUMN automated_actions_validations_table.module_name IS 'Nom du module externe';
COMMENT ON COLUMN automated_actions_validations_table.module_version IS 'Version du module externe';
COMMENT ON COLUMN automated_actions_validations_table.module_function_name IS 'Nom de la fonction du module externe';
COMMENT ON COLUMN automated_actions_validations_table.module_function_parameters IS 'Liste des paramètres de la fonction du module externe';


CREATE OR REPLACE VIEW automated_actions_validations AS select * from automated_actions_validations_table;

GRANT SELECT, UPDATE ON automated_actions_validations_validation_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE automated_actions_validations_table TO admin_role, writer_role;
GRANT SELECT ON TABLE automated_actions_validations_table TO reader_role;
GRANT SELECT ON automated_actions_validations TO admin_role, writer_role, reader_role;

-- automated action execution
CREATE SEQUENCE automated_executions_actions_execution_action_id_seq;

CREATE TABLE automated_executions_actions_table (
	automated_execution_action_id bigint PRIMARY KEY DEFAULT nextval('automated_executions_actions_execution_action_id_seq'),
	execution_test_id bigint NOT NULL,
	automated_action_id bigint NOT NULL,
	previous_automated_execution_action_id bigint,
	result_id CHAR(1) NOT NULL,
	comments VARCHAR(16384),
	FOREIGN KEY (execution_test_id) REFERENCES executions_tests_table(execution_test_id) ON DELETE CASCADE,
	FOREIGN KEY (automated_action_id) REFERENCES automated_actions_table(automated_action_id) ON DELETE CASCADE,
	FOREIGN KEY (previous_automated_execution_action_id) REFERENCES automated_executions_actions_table(automated_execution_action_id) ON DELETE SET NULL
);

COMMENT ON TABLE automated_executions_actions_table IS 'Exécutions d''actions de tests';
COMMENT ON COLUMN automated_executions_actions_table.automated_execution_action_id IS 'Identifiant unique de l''exécution de l''action';
COMMENT ON COLUMN automated_executions_actions_table.execution_test_id IS 'Identifiant de l''exécution de test';
COMMENT ON COLUMN automated_executions_actions_table.automated_action_id IS 'Identifiant de l''action';
COMMENT ON COLUMN automated_executions_actions_table.previous_automated_execution_action_id IS 'Identifiant de l''action précédente';
COMMENT ON COLUMN automated_executions_actions_table.result_id IS 'Identifiant du résultat';
COMMENT ON COLUMN automated_executions_actions_table.comments IS 'Commentaires';


CREATE OR REPLACE VIEW automated_executions_actions AS select * from automated_executions_actions_table;

GRANT SELECT, UPDATE ON automated_executions_actions_execution_action_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE automated_executions_actions_table TO admin_role, writer_role;
GRANT SELECT ON TABLE automated_executions_actions_table TO reader_role;
GRANT SELECT ON automated_executions_actions TO admin_role, writer_role, reader_role;


-----------------------------------------
-- Types de test
-----------------------------------------
CREATE TABLE tests_types_table (
	test_type_id CHAR(1) PRIMARY KEY,
	test_type_label VARCHAR(64) NOT NULL
);


COMMENT ON TABLE tests_types_table IS 'Types de tests';
COMMENT ON COLUMN tests_types_table.test_type_id IS 'Identifiant du type de test';
COMMENT ON COLUMN tests_types_table.test_type_label IS 'Libelle du type de test';

CREATE OR REPLACE VIEW tests_types AS SELECT * FROM tests_types_table;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE tests_types_table TO admin_role;
GRANT SELECT ON tests_types_table TO writer_role, reader_role;
GRANT SELECT ON tests_types TO admin_role, writer_role, reader_role;

INSERT INTO tests_types_table (test_type_id, test_type_label)
VALUES ('N', 'Scénario nominal');

INSERT INTO tests_types_table (test_type_id, test_type_label)
VALUES ('A', 'Scénario alternatif');

INSERT INTO tests_types_table (test_type_id, test_type_label)
VALUES ('E', 'Scénario d''exception');


-- Custom fields
-- entity types are : T=>test or R=>requirement
-- field types are : T=>text, I=>integer, F=>float, B=>boolean, L=>list, R=>Exclusive list
CREATE SEQUENCE custom_fields_desc_id_seq;

CREATE TABLE custom_fields_desc_table (
  custom_field_desc_id bigint NOT NULL DEFAULT nextval('custom_fields_desc_id_seq'),
  entity_type char(1) NOT NULL DEFAULT 'T',
  tab_name VARCHAR(128) NOT NULL,
  field_label VARCHAR(128) NOT NULL,
  field_type char(1) NOT NULL DEFAULT 'T',
  mandatory char(1) NOT NULL DEFAULT 'N',
  default_value varchar(16384),
  comma_separated_values varchar(16384),
  PRIMARY KEY (custom_field_desc_id)
);

CREATE OR REPLACE VIEW custom_fields_desc AS select * from custom_fields_desc_table;

GRANT SELECT, UPDATE ON custom_fields_desc_id_seq TO admin_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE custom_fields_desc_table TO admin_role;
GRANT SELECT ON TABLE custom_fields_desc_table TO writer_role, reader_role;
GRANT SELECT ON custom_fields_desc TO admin_role, writer_role, reader_role;

-- Custom test fields
CREATE SEQUENCE custom_test_fields_id_seq;

CREATE TABLE custom_test_fields_table (
  custom_test_field_id bigint NOT NULL DEFAULT nextval('custom_test_fields_id_seq'),
  custom_field_desc_id bigint NOT NULL,
  test_content_id bigint NOT NULL,
  field_value varchar(16384),
  PRIMARY KEY (custom_test_field_id),
  FOREIGN KEY (custom_field_desc_id) REFERENCES custom_fields_desc_table(custom_field_desc_id) ON DELETE CASCADE,
  FOREIGN KEY (test_content_id) REFERENCES tests_contents_table(test_content_id) ON DELETE CASCADE
);
CREATE OR REPLACE VIEW custom_test_fields AS select * from custom_test_fields_table;

GRANT SELECT, UPDATE ON custom_test_fields_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE custom_test_fields_table TO admin_role, writer_role;
GRANT SELECT ON TABLE custom_test_fields_table TO reader_role;
GRANT SELECT ON custom_test_fields TO admin_role, writer_role, reader_role;

-- Custom requirement fields
CREATE SEQUENCE custom_requirement_fields_id_seq;

CREATE TABLE custom_requirement_fields_table (
  custom_requirement_field_id bigint NOT NULL DEFAULT nextval('custom_requirement_fields_id_seq'),
  custom_field_desc_id bigint NOT NULL,
  requirement_content_id bigint NOT NULL,
  field_value varchar(16384),
  PRIMARY KEY (custom_requirement_field_id),
  FOREIGN KEY (custom_field_desc_id) REFERENCES custom_fields_desc_table(custom_field_desc_id) ON DELETE CASCADE,
  FOREIGN KEY (requirement_content_id) REFERENCES requirements_contents_table(requirement_content_id) ON DELETE CASCADE
);
CREATE OR REPLACE VIEW custom_requirement_fields AS select * from custom_requirement_fields_table;

GRANT SELECT, UPDATE ON custom_requirement_fields_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE custom_requirement_fields_table TO admin_role, writer_role;
GRANT SELECT ON TABLE custom_requirement_fields_table TO reader_role;
GRANT SELECT ON custom_requirement_fields TO admin_role, writer_role, reader_role;

-- Correction fonction html_to_plain_text
DROP FUNCTION html_to_plain_text(in_html_string VARCHAR);

CREATE OR REPLACE FUNCTION html_to_plain_text(in_html_string VARCHAR)
RETURNS varchar AS $$
DECLARE

	tmp_return varchar default '';
	tmp_str varchar default '';
	tmp_start INTEGER DEFAULT 1;
	tmp_end INTEGER DEFAULT 1;

BEGIN

	tmp_start := strpos(in_html_string, '<body');
	IF tmp_start > 0 THEN
		tmp_str := substr(in_html_string, tmp_start);
		WHILE tmp_start > 0 AND tmp_end > 0 LOOP
			tmp_start := strpos(tmp_str, '<');
			IF tmp_start > 0 THEN
				tmp_return := tmp_return || substr(tmp_str, 1, tmp_start - 1);
				tmp_end := strpos(tmp_str, '>');
				IF tmp_end > 0 THEN
					tmp_str := substr(tmp_str, tmp_end + 1);
				END IF;
			END IF;
		END LOOP;
	ELSE
		tmp_return := in_html_string;
	END IF;	
	return tmp_return;

END;
$$ LANGUAGE plpgsql;

-- Modification tests_contents_files_table
CREATE SEQUENCE tests_contents_files_test_content_file_id_seq;
GRANT SELECT, UPDATE ON tests_contents_files_test_content_file_id_seq TO admin_role, writer_role;
ALTER TABLE tests_contents_files_table DROP CONSTRAINT tests_contents_files_table_pkey;
ALTER TABLE tests_contents_files_table RENAME COLUMN test_content_file_id TO test_content_lo_oid;
ALTER TABLE tests_contents_files_table ADD COLUMN test_content_file_id bigint DEFAULT nextval('tests_contents_files_test_content_file_id_seq');
UPDATE tests_contents_files_table set test_content_file_id=test_content_lo_oid;
ALTER TABLE tests_contents_files_table ADD PRIMARY KEY (test_content_file_id);
COMMENT ON COLUMN tests_contents_files_table.test_content_file_id IS 'Identifiant unique de la pièce jointe';
COMMENT ON COLUMN tests_contents_files_table.test_content_lo_oid IS 'Identifiant Large Object';
DROP VIEW tests_contents_files;
CREATE OR REPLACE VIEW tests_contents_files AS select * from tests_contents_files_table;
GRANT SELECT ON tests_contents_files TO admin_role, writer_role, reader_role;


-- Mise à jour de la version logique de la base de données
UPDATE database_version_table set database_version_number='01.05.00.00', upgrade_date=now();
