-----------------------------------------
--	ANOMALIES
-----------------------------------------
CREATE SEQUENCE bugs_bug_id_seq;

CREATE TABLE bugs_table (
	bug_id bigint PRIMARY KEY DEFAULT nextval('bugs_bug_id_seq'),
	execution_test_id bigint NOT NULL,
	execution_action_id bigint,
	creation_date timestamp with time zone NOT NULL DEFAULT now(),
	short_name varchar(64),
	priority varchar(64),
	severity varchar(64),
	platform varchar(64),
	system varchar(64),
	description varchar(16384),
	bugtracker_bug_id varchar(64),
	FOREIGN KEY (execution_test_id) REFERENCES executions_tests_table(execution_test_id) ON DELETE CASCADE,
	FOREIGN KEY (execution_action_id) REFERENCES executions_actions_table(execution_action_id) ON DELETE CASCADE

);

COMMENT ON TABLE bugs_table IS 'Anomalies';
COMMENT ON COLUMN bugs_table.bug_id IS 'Identifiant unique de l''anomalie';
COMMENT ON COLUMN bugs_table.execution_test_id IS 'Identifiant unique de l''exécution de test associée';
COMMENT ON COLUMN bugs_table.execution_action_id IS 'Identifiant unique de l''exécution de l''action de test associée';

COMMENT ON COLUMN bugs_table.creation_date IS 'Date de création de l''anomalie';
COMMENT ON COLUMN bugs_table.short_name IS 'Résumé de l''anomalie';
COMMENT ON COLUMN bugs_table.priority IS 'Priorité de l''anomalie';
COMMENT ON COLUMN bugs_table.severity IS 'Gravité de l''anomalie';
COMMENT ON COLUMN bugs_table.platform IS 'Plateforme';
COMMENT ON COLUMN bugs_table.system IS 'Système d''exploitation';
COMMENT ON COLUMN bugs_table.description IS 'Description de l''anomalie';

COMMENT ON COLUMN bugs_table.bugtracker_bug_id IS 'Identifiant de l''anomalie dans le bugtracker';

CREATE VIEW bugs AS select * from bugs_table;

GRANT SELECT, UPDATE ON bugs_bug_id_seq TO admin_role, writer_role;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE bugs_table TO admin_role, writer_role;
GRANT SELECT ON TABLE bugs_table TO reader_role;
GRANT SELECT ON bugs TO admin_role, writer_role, reader_role;
