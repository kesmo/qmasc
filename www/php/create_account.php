<?php include "functions.php" ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>R.T.M.R - Documentation - Architecture</title>
<meta name="keywords" content="outil, test, logiciel, fonctionnel, exigence, sc&eacute;nario, qualit&eacute;, validation, recette, campagne, plan, r&eacute;gression, open source, linux, mac os">
<meta name="description" content="R.T.M.R, Documentation, Détails des composants et schéma d'architecture">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link href="../stylesheet.css" rel="stylesheet" type="text/css">
</head>
<body>
<table>
	<tr>
		<td>
		<table>
			<tbody>
				<tr>
					<td><a href="index.html"><img style="border: 0px solid;" src="../images/spiral.png" alt="logo"></a></td>
					<td><a href="index.html"><img style="border: 0px solid;" src="../images/rtmr.png" alt="logo"></a></td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table>
			<tr>
				<td valign="top" width="240px"><!-- DEBUT MENU -->
				<a class="menu" href="../index.html">Accueil</a>
				<br>
				<a class="menu" href="../presentation.html">Pr&eacute;sentation</a>
				<br>
				<a class="menu" href="../installation.html">Installation</a>
				<br>
				<a class="menu" href="../screenshots.html">Screenshots</a>
				<br>
				<a class="menu" href="../telechargements.html">T&eacute;l&eacute;chargements</a>
				<br>
				<a class="menu" href="../liens.html">Liens</a>
				<br>
				<a class="menu" href="../contacts.html">Contacts</a>
				<br>
				<a class="menu" href="http://forum.rtmr.net">Forum</a>
				<br>
				<a class="menu" href="http://mantis.rtmr.net">Bugtracker</a>
				<!-- FIN MENU --></td>
				<td valign="top">
				<h1>Création d'un compte client</h1>
				<br>
				<?php
					$create_account = false;

					if (isset($_POST['validate']) && isset($_POST['email']) && !empty($_POST['email']))
					{
						$email = $_POST['email'];
						if (validEmail($email))
						{
							$create_account = true;
							$msg = $mailMsg;
							$msg .= "Auteur: $mailAuteur\n";
							$msg .= "Email : $mailMail\n";
							$headers = "From: 'Contact R.T.M.R'<contact@rtmr.net>\r\n";
							$headers .= "Reply-To: contact@rtmr.net\r\n";
							if(@mail($email, $mailSujet, $msg, $headers))
								echo "statut=".utf8_encode("Un message de confirmation vous a été envoyé à l'adresse indiquée.");
							else
								echo "statut=".utf8_encode("Le message de confirmation n'a pu être envoyé.");
						}
						else
						{
							echo "<p class=\"alerte\">L'adresse de messagerie saisie n'est pas valide.</p>";
						}
					}

					if (!$create_account)
					{
				?>
						<form method="post" action="<?php echo $PHP_SELF;?>">
							<table align="center" class="formulaire">
								<tr>
									<th colspan="2" align="center">Veuillez saisir votre adresse de messagerie, puis cliquez sur le bouton valider</th>
								</tr>
								<tr>
									<td>Email</td>
									<td><input class="texte" type="text" name="email" size="48"/></td>
								</tr>
								<tr>
									<td colspan="2" align="center"><input class="bouton" type="submit" name="validate" value="Valider" /></td>
								</tr>
						</form>
				<?php
					}
				?>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</body>
</html>
