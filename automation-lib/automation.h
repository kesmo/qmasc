/*****************************************************************************
Copyright (C) 2011 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef AUTOMATION_H_
#define AUTOMATION_H_

//#ifdef WINVER
//#undef WINVER
//#endif
//#define WINVER 0x0500

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

#ifdef AUTOMATION_LIBRARY_EXPORTS
#if (defined(_WINDOWS) || defined(WIN32))
#define AUTOMATION_DLLEXPORT   __declspec( dllexport )
#define AUTOMATION_DLLCALL        __stdcall
#else
#define AUTOMATION_DLLEXPORT   __attribute__((visibility("default")))
#define AUTOMATION_DLLCALL
#endif
#else
#ifdef AUTOMATION_LIBRARY_IMPORTS
#if (defined(_WINDOWS) || defined(WIN32))
#define AUTOMATION_DLLEXPORT   __declspec( dllimport )
#define AUTOMATION_DLLCALL        __stdcall
#define _AFXDLL
#else
#define AUTOMATION_DLLEXPORT
#define AUTOMATION_DLLCALL
#endif
#else
#define AUTOMATION_DLLEXPORT
#define AUTOMATION_DLLCALL
#endif
#endif

#ifndef _WINDOWS_
#include <windows.h>
#include <psapi.h>
#endif

#include "../common/utilities.h"
#include "../common/netcommon.h"
#include "../common/entities-def.h"

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct _windows_hierarchy
    {
        ATOM m_window_atom;
        int m_ctrl_id;
        CHAR m_window_class_name[1024];
        CHAR m_window_name[1024];
        HWND m_window_id;
        BOOL m_is_active;
        BOOL m_is_visible;
        RECT m_window_rect;
        struct _windows_hierarchy* m_child_window;
        struct _windows_hierarchy* m_first_child_window;
        struct _windows_hierarchy* m_next_window;
    }
    windows_hierarchy;

    struct _log_message_list;
    typedef int (log_message_callback)(struct _log_message_list*, HWND, char*);
    typedef int (error_callback)(const char* in_error_msg);

    typedef struct _log_message_list
    {
        EventMessageType m_message_type;
        windows_hierarchy* m_window_hierarchy;
        UINT m_message;
        UINT m_mouse_x;
        UINT m_mouse_y;
        UINT m_keyboard_vk;
        UINT m_keyboard_repeat;
        ULONG m_time;
        log_message_callback* m_callback;
        void* m_custom_data_ptr;
        struct _log_message_list* m_next_message;
    }
    log_message_list;

    /*********** Exported functions **************/
    AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL automation_start_record(net_session *in_session, DWORD in_process_id, DWORD in_thread_id, error_callback* in_error_callback, int in_record_mouse_move_event);
    AUTOMATION_DLLEXPORT log_message_list* AUTOMATION_DLLCALL automation_stop_record(net_session *in_session);

    AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL automation_start_playback(net_session *in_session, log_message_list* in_log_message_list, DWORD in_process_id, DWORD in_thread_id, error_callback* in_error_callback);
    AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL automation_stop_playback(net_session *in_session);

    AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL free_messages_list(log_message_list** in_messages_list);
    AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL free_window_hierarchy(windows_hierarchy** in_window_hierarchy_list);

    /********************************************/

    LRESULT CALLBACK JournalRecordProc(int code, WPARAM wParam, LPARAM lParam);
    LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam);

    LRESULT CALLBACK GetMessageProc(int code, WPARAM wparam, LPARAM lparam);
    LRESULT PlaybackMessage();

    windows_hierarchy* GetWindowHierarchy(HWND in_window_id, windows_hierarchy* in_window_hierarchy);
    HWND GetLastChildWindowId(windows_hierarchy* in_window_hierarchy);
    BOOL windowMatches(HWND hwnd, windows_hierarchy* in_window_hierarchy);

    HWND GetChildWindowId(windows_hierarchy* in_window_hierarchy);
    HWND GetThreadWindow();

    BOOL IsWindowFromProcessHook(HWND hwnd);

    void GetChildWindowHierarchy(windows_hierarchy* in_window_hierarchy);
    void FillWindowHierarchyInfos(windows_hierarchy* in_window_hierarchy);

#ifdef __cplusplus
}
#endif


#endif /* AUTOMATION_H_ */
