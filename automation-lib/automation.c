/*****************************************************************************
Copyright (C) 2011 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifdef _AFX
#include <afxwin.h>
#else
#include <WTypes.h>
#include <Winuser.h>
#endif

#include "automation.h"
#include "../common/constants.h"

HINSTANCE m_module_instance = NULL;
HMODULE m_module_handle = NULL;
static HHOOK m_journal_hook_handle = NULL;

static net_session *m_session = NULL;
static DWORD m_process_id = 0;
static DWORD m_thread_id = 0;
static HWND m_thread_window_handle = 0;
static HWND m_last_window = 0;

static BOOL m_move_next_message = TRUE;

static DWORD    m_start_time;
static DWORD    m_previous_message_time;

static log_message_list* m_log_message_list = NULL;
static log_message_list* m_current_log_message = NULL;
static log_message_list* m_previous_log_message = NULL;

static error_callback* m_error_callback = NULL;

static BOOL m_is_dragging = FALSE;

static int m_messages_count = 0;

static int m_record_mouse_move_event = FALSE;

#ifdef _AFX
#ifdef IMPLEMENT_DYNAMIC
//IMPLEMENT_DYNAMIC(CEdit, CWnd)
#endif
#endif

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
    if (dwReason == DLL_PROCESS_ATTACH)
        m_module_handle = hModule;

    return TRUE;
}


AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL automation_start_record(net_session *in_session, DWORD in_process_id, DWORD in_thread_id, error_callback* in_error_callback, int in_record_mouse_move_event)
{
    GUITHREADINFO thread_info;
    int tmp_count = 0;

    memset(&thread_info, 0, sizeof(GUITHREADINFO));
    thread_info.cbSize = sizeof(GUITHREADINFO);

    m_module_instance = GetModuleHandle(NULL);
    m_session = in_session;
    m_process_id = in_process_id;
    m_thread_id = in_thread_id;
    m_start_time = GetTickCount();
    m_error_callback = in_error_callback;
    m_record_mouse_move_event = in_record_mouse_move_event;

    LOG_TRACE(m_session, "automation_start_record module=%p, process=%d, thread=%d\n", m_module_instance, m_process_id, m_thread_id);

    m_thread_window_handle = NULL;
    while (m_thread_window_handle == NULL && tmp_count < 100) {
        m_thread_window_handle = GetThreadWindow();
        Sleep(100);
        ++tmp_count;
    }

    if (tmp_count >= 100) {
        net_session_print_error(m_session, "automation_start_record : unable to retreive main window thread for process %ul (thread %lu)\n", m_process_id, m_thread_id);
        return EMPTY_OBJECT;
    }

    m_log_message_list = (log_message_list*)malloc(sizeof(log_message_list));
    memset(m_log_message_list, 0, sizeof(log_message_list));

    m_current_log_message = m_log_message_list;
    m_current_log_message->m_message_type = StartRecord;
    m_current_log_message->m_window_hierarchy = NULL;
    m_current_log_message->m_message = 0;
    m_current_log_message->m_time = GetTickCount() - m_start_time;

    m_current_log_message->m_next_message = (log_message_list*)malloc(sizeof(log_message_list));
    m_previous_log_message = m_current_log_message;
    m_current_log_message = m_current_log_message->m_next_message;
    memset(m_current_log_message, 0, sizeof(log_message_list));

    m_messages_count = 0;

    m_journal_hook_handle = SetWindowsHookEx(WH_JOURNALRECORD, JournalRecordProc, m_module_handle, 0);
    //m_journal_hook_handle = SetWindowsHookEx(WH_JOURNALPLAYBACK, GetMessageProc, m_module_handle, m_thread_id);
    if (m_journal_hook_handle == NULL)
    {
        FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&m_session->m_column_buffer, sizeof(m_session->m_column_buffer), NULL);

        net_session_print_error(m_session, "automation_start_record : unable to set journal record proc hook (%s) for process %ul (thread %lu)\n", m_session->m_column_buffer, m_process_id, m_thread_id);
        LOG_ERROR(m_session, m_session->m_last_error_msg);
        return EMPTY_OBJECT;
    }

    return NOERR;
}


AUTOMATION_DLLEXPORT log_message_list* AUTOMATION_DLLCALL automation_stop_record(net_session *in_session)
{
    LOG_TRACE(in_session, "automation_stop_record\n");

    if (m_current_log_message) {
        m_current_log_message->m_message_type = StopRecord;
        m_current_log_message->m_time = GetTickCount() - m_start_time;
        if (!m_current_log_message->m_window_hierarchy /* && IsWindowFromProcessHook(GetForegroundWindow())*/) {
            m_current_log_message->m_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
            memset(m_current_log_message->m_window_hierarchy, 0, sizeof(windows_hierarchy));

            m_current_log_message->m_window_hierarchy = GetWindowHierarchy(GetForegroundWindow(), m_current_log_message->m_window_hierarchy);
        }
    }

    if (m_journal_hook_handle)
    {
        LOG_TRACE(in_session, "stopping journal hook\n");
        UnhookWindowsHookEx(m_journal_hook_handle);
    }

    return m_log_message_list;
}


AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL automation_start_playback(net_session *in_session, log_message_list* in_log_message_list, DWORD in_process_id, DWORD in_thread_id, error_callback* in_error_callback)
{
    GUITHREADINFO thread_info;
    int tmp_count = 0;

    memset(&thread_info, 0, sizeof(GUITHREADINFO));
    thread_info.cbSize = sizeof(GUITHREADINFO);

    m_session = in_session;
    m_process_id = in_process_id;
    m_thread_id = in_thread_id;
    m_move_next_message = TRUE;
    m_error_callback = in_error_callback;

    m_thread_window_handle = NULL;

    while (m_thread_window_handle == NULL && tmp_count < 100) {
        m_thread_window_handle = GetThreadWindow();
        Sleep(100);
        ++tmp_count;
    }

    if (tmp_count >= 100) {
        net_session_print_error(m_session, "automation_start_record : unable to retreive main window thread for process %ul (thread %lu)\n", m_process_id, m_thread_id);
        LOG_ERROR(in_session, m_session->m_last_error_msg);
        return EMPTY_OBJECT;
    }

    LOG_TRACE(m_session, "automation_start_playback module=%p, process=%d, thread=%d\n", m_module_instance, m_process_id, m_thread_id);

    m_log_message_list = in_log_message_list;
    m_current_log_message = NULL;
    m_messages_count = 0;

    SetForegroundWindow(m_thread_window_handle);
    SetActiveWindow(m_thread_window_handle);
    BringWindowToTop(m_thread_window_handle);
    ShowWindow(m_thread_window_handle, SW_SHOW);

    PlaybackMessage();

    //m_journal_hook_handle = SetWindowsHookEx(WH_JOURNALPLAYBACK, JournalPlaybackProc, (HINSTANCE)m_module_instance, 0);
    //if (m_journal_hook_handle == NULL)
    //{
    // FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
    //  NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&m_session->m_column_buffer, sizeof(m_session->m_column_buffer), NULL );

    //    net_session_print_error(m_session, "automation_start_playback : unable to set journal playback proc hook (%s) for process %lu (thread %lu)\n", m_session->m_column_buffer, m_process_id, m_thread_id);
    // LOG_ERROR(in_session, m_session->m_last_error_msg);

    // return EMPTY_OBJECT;
    //}

    return NOERR;
}



AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL automation_stop_playback(net_session *in_session)
{
    LOG_TRACE(in_session, "automation_stop_playback\n");

    //UnhookWindowsHookEx(m_journal_hook_handle);

    m_log_message_list = NULL;

    return NOERR;
}


AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL free_messages_list(log_message_list** in_messages_list)
{
    if (in_messages_list && (*in_messages_list))
    {
        free_messages_list(&(*in_messages_list)->m_next_message);
        free_window_hierarchy(&(*in_messages_list)->m_window_hierarchy);
        free(*in_messages_list);
        *in_messages_list = NULL;
    }
    return NOERR;
}



AUTOMATION_DLLEXPORT int AUTOMATION_DLLCALL free_window_hierarchy(windows_hierarchy** in_window_hierarchy_list)
{
    windows_hierarchy* tmp_child_window = NULL;

    if (in_window_hierarchy_list && (*in_window_hierarchy_list))
    {
        free_window_hierarchy(&(*in_window_hierarchy_list)->m_child_window);
        free_window_hierarchy(&(*in_window_hierarchy_list)->m_next_window);
        free_window_hierarchy(&(*in_window_hierarchy_list)->m_first_child_window);
        free(*in_window_hierarchy_list);
        *in_window_hierarchy_list = NULL;
    }

    return NOERR;
}


/****************************************************************
  WH_GETMESSAGE hook procedure
 ****************************************************************/
LRESULT CALLBACK GetMessageProc(int code, WPARAM wparam, LPARAM lparam)
{
    EVENTMSG *tmp_event = NULL;
    SHORT val = GetKeyState(VK_PAUSE);
    WINDOWINFO tmp_active_window_infos;

    GUITHREADINFO thread_info;

    memset(&thread_info, 0, sizeof(GUITHREADINFO));
    thread_info.cbSize = sizeof(GUITHREADINFO);

    GetGUIThreadInfo(m_thread_id, &thread_info);

    if (thread_info.hwndActive == 0)
        return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);

    if (val & 0x80000000)
    {
        return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
    }

    switch (code)
    {
    case HC_ACTION:
        tmp_event = (EVENTMSG *)lparam;

        if (tmp_event->hwnd == NULL) {
            //tmp_event->hwnd = GetForegroundWindow();
            if (!IsWindowFromProcessHook(GetForegroundWindow())) {
                return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
            }
        }
        else {

            if (!IsWindowFromProcessHook(tmp_event->hwnd)) {
                return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
            }

            GetWindowInfo(tmp_event->hwnd, &tmp_active_window_infos);
            /*
            if (!(tmp_active_window_infos.dwStyle & WS_VISIBLE)){
                return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
            }
            */

            LOG_TRACE(m_session, "GetMessageProc active hwnd=%p, hwnd=%p, status=%d, style=%d\n",
                thread_info.hwndActive,
                tmp_event->hwnd,
                tmp_active_window_infos.dwWindowStatus,
                tmp_active_window_infos.dwStyle);
        }

        // Ne pas traiter autre chose que les evenements clavier et souris
        switch (tmp_event->message)
        {
        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_MOUSEWHEEL:
        case WM_MOUSEHWHEEL:
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        case WM_MOUSEMOVE:
            //if (tmp_event->message != WM_MOUSEMOVE || m_is_dragging == TRUE)
            if (tmp_event->message != WM_MOUSEMOVE || m_record_mouse_move_event)
            {
                if (tmp_event->message == WM_LBUTTONDOWN || tmp_event->message == WM_RBUTTONDOWN)
                {
                    m_is_dragging = TRUE;
                }
                else if (tmp_event->message == WM_LBUTTONUP || tmp_event->message == WM_RBUTTONUP)
                {
                    m_is_dragging = FALSE;
                }
                LOG_TRACE(m_session, "GetMessageProc mouse message=%d, X=%d, Y=%d\n",
                    tmp_event->message,
                    tmp_event->paramH,
                    tmp_event->paramL)

                    m_current_log_message->m_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
                memset(m_current_log_message->m_window_hierarchy, 0, sizeof(windows_hierarchy));

                m_current_log_message->m_window_hierarchy = GetWindowHierarchy(tmp_event->hwnd, m_current_log_message->m_window_hierarchy);
                if (tmp_event->time > m_start_time)
                    m_current_log_message->m_time = tmp_event->time - m_start_time;
                m_current_log_message->m_message = tmp_event->message;
                m_current_log_message->m_message_type = Mouse;
                m_current_log_message->m_mouse_x = tmp_event->paramL;
                m_current_log_message->m_mouse_y = tmp_event->paramH;

                m_current_log_message->m_next_message = (log_message_list*)malloc(sizeof(log_message_list));
                m_previous_log_message = m_current_log_message;
                m_current_log_message = m_current_log_message->m_next_message;
                memset(m_current_log_message, 0, sizeof(log_message_list));

                ++m_messages_count;
                m_start_time = GetTickCount();
            }
            break;

        case WM_KEYDOWN:
        case WM_KEYUP:
        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
            LOG_TRACE(m_session, "GetMessageProc keyboard message=%d, paramH=%d, paramL=%d\n",
                tmp_event->message,
                tmp_event->paramH,
                tmp_event->paramL)

                m_current_log_message->m_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
            memset(m_current_log_message->m_window_hierarchy, 0, sizeof(windows_hierarchy));

            m_current_log_message->m_window_hierarchy = GetWindowHierarchy(tmp_event->hwnd, m_current_log_message->m_window_hierarchy);
            m_current_log_message->m_message = tmp_event->message;
            m_current_log_message->m_message_type = Keyboard;
            if (tmp_event->time > m_start_time)
                m_current_log_message->m_time = tmp_event->time - m_start_time;
            m_current_log_message->m_keyboard_vk = tmp_event->paramH;
            m_current_log_message->m_keyboard_repeat = tmp_event->paramL;

            m_current_log_message->m_next_message = (log_message_list*)malloc(sizeof(log_message_list));
            m_previous_log_message = m_current_log_message;
            m_current_log_message = m_current_log_message->m_next_message;
            memset(m_current_log_message, 0, sizeof(log_message_list));
            ++m_messages_count;
            m_start_time = GetTickCount();
            break;

        default:
            return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
        }


        break;
    default:
        return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
    }
    return 0;
}


/****************************************************************
  WH_JOURNALRECORD hook procedure
 ****************************************************************/
LRESULT CALLBACK JournalRecordProc(int code, WPARAM wparam, LPARAM lparam)
{
    EVENTMSG *tmp_event = NULL;
    SHORT val = GetKeyState(VK_PAUSE);
    WINDOWINFO tmp_active_window_infos;
    HWND target_window;

    GUITHREADINFO thread_info;

    memset(&thread_info, 0, sizeof(GUITHREADINFO));
    thread_info.cbSize = sizeof(GUITHREADINFO);

    GetGUIThreadInfo(m_thread_id, &thread_info);

    //if (thread_info.hwndActive == 0)
    // return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);

    if (val & 0x80000000)
    {
        return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
    }

    switch (code)
    {
    case HC_ACTION:
        tmp_event = (EVENTMSG *)lparam;
        target_window = tmp_event->hwnd;
        if (target_window == NULL)
            target_window = GetForegroundWindow();

        if (target_window == NULL) {
            //tmp_event->hwnd = GetForegroundWindow();
            //if (!IsWindowFromProcessHook(GetForegroundWindow())){
            // return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
            //}
        }
        else {

            //if (!IsWindowFromProcessHook(target_window)){
            // return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
            //}

            GetWindowInfo(target_window, &tmp_active_window_infos);
            if (!(tmp_active_window_infos.dwStyle & WS_VISIBLE)) {
                return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
            }

            //LOG_TRACE(m_session, "JournalRecordProc active hwnd=%d, hwnd=%d, status=%d, style=%d\n",
            // thread_info.hwndActive,
   //  target_window,
            // tmp_active_window_infos.dwWindowStatus,
            // tmp_active_window_infos.dwStyle);
        }

        // Ne pas traiter autre chose que les evenements clavier et souris
        switch (tmp_event->message)
        {
        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        case WM_MOUSEWHEEL:
        case WM_MOUSEHWHEEL:
        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        case WM_MOUSEMOVE:
            //if (tmp_event->message != WM_MOUSEMOVE || m_is_dragging == TRUE)
            if (tmp_event->message != WM_MOUSEMOVE || m_record_mouse_move_event)
            {
                if (tmp_event->message == WM_LBUTTONDOWN || tmp_event->message == WM_RBUTTONDOWN)
                {
                    m_is_dragging = TRUE;
                }
                else if (tmp_event->message == WM_LBUTTONUP || tmp_event->message == WM_RBUTTONUP)
                {
                    m_is_dragging = FALSE;
                }
                LOG_TRACE(m_session, "JournalRecordProc mouse message=%d, X=%d, Y=%d\n",
                    tmp_event->message,
                    tmp_event->paramH,
                    tmp_event->paramL)

                    m_current_log_message->m_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
                memset(m_current_log_message->m_window_hierarchy, 0, sizeof(windows_hierarchy));

                m_current_log_message->m_window_hierarchy = GetWindowHierarchy(target_window, m_current_log_message->m_window_hierarchy);
                if (tmp_event->time > m_start_time)
                    m_current_log_message->m_time = tmp_event->time - m_start_time;
                m_current_log_message->m_message = tmp_event->message;
                m_current_log_message->m_message_type = Mouse;
                m_current_log_message->m_mouse_x = tmp_event->paramL;
                m_current_log_message->m_mouse_y = tmp_event->paramH;

                m_current_log_message->m_next_message = (log_message_list*)malloc(sizeof(log_message_list));
                m_previous_log_message = m_current_log_message;
                m_current_log_message = m_current_log_message->m_next_message;
                memset(m_current_log_message, 0, sizeof(log_message_list));

                ++m_messages_count;
                m_start_time = GetTickCount();
            }
            break;

        case WM_KEYDOWN:
        case WM_KEYUP:
        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
            LOG_TRACE(m_session, "JournalRecordProc keyboard message=%d, paramH=%d, paramL=%d\n",
                tmp_event->message,
                tmp_event->paramH,
                tmp_event->paramL)

                m_current_log_message->m_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
            memset(m_current_log_message->m_window_hierarchy, 0, sizeof(windows_hierarchy));

            m_current_log_message->m_window_hierarchy = GetWindowHierarchy(target_window, m_current_log_message->m_window_hierarchy);
            m_current_log_message->m_message = tmp_event->message;
            m_current_log_message->m_message_type = Keyboard;
            if (tmp_event->time > m_start_time)
                m_current_log_message->m_time = tmp_event->time - m_start_time;
            m_current_log_message->m_keyboard_vk = tmp_event->paramH;
            m_current_log_message->m_keyboard_repeat = tmp_event->paramL;

            m_current_log_message->m_next_message = (log_message_list*)malloc(sizeof(log_message_list));
            m_previous_log_message = m_current_log_message;
            m_current_log_message = m_current_log_message->m_next_message;
            memset(m_current_log_message, 0, sizeof(log_message_list));
            ++m_messages_count;
            m_start_time = GetTickCount();
            break;

        default:
            return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
        }


        break;
    default:
        return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
    }
    return 0;
}

/****************************************************************
  Post messages
 ****************************************************************/
LRESULT PlaybackMessage()
{
    MSG tmp_message;

    HWND tmp_active_window = NULL;

    if (m_move_next_message)
    {
        LOG_TRACE(m_session, "PlaybackMessage : Move next message\n");
        if (m_current_log_message != NULL)
        {
            LOG_TRACE(m_session, "PlaybackMessage : check message\n");
            if (m_current_log_message->m_message_type == None)
            {
                LOG_TRACE(m_session, "PlaybackMessage : Unknow message %i\n", m_current_log_message->m_message_type);
                automation_stop_playback(m_session);
                return 0;
            }
            ++m_messages_count;
        }
        else
        {
            LOG_TRACE(m_session, "PlaybackMessage : Start message queue\n");
            m_start_time = GetTickCount();
            m_previous_message_time = 0;
            m_current_log_message = m_log_message_list;
            m_move_next_message = TRUE;

            return PlaybackMessage();
        }

        m_move_next_message = FALSE;
    }

    LOG_TRACE(m_session, "PlaybackMessage : *************** Treat message %i\n", m_messages_count);

    tmp_message.message = m_current_log_message->m_message;
    if (m_current_log_message->m_time > 0)
    {
        LOG_TRACE(m_session, "PlaybackMessage : Wait message %li (%li)\n", m_current_log_message->m_message, m_current_log_message->m_time);
        Sleep(m_current_log_message->m_time);
    }

    LOG_TRACE(m_session, "PlaybackMessage : Play message %li (%li)\n", m_current_log_message->m_message, m_current_log_message->m_time);
    if (m_current_log_message->m_window_hierarchy && m_current_log_message->m_window_hierarchy->m_window_atom) {
        windows_hierarchy *tmp_current_window = m_current_log_message->m_window_hierarchy;
        while (tmp_current_window) {
            LOG_TRACE(m_session, "PlaybackMessage : Retreiving window ATOM=%lu, Class=%s, Text=%s, CtrlId=%lu\n",
                tmp_current_window->m_window_atom,
                tmp_current_window->m_window_class_name,
                tmp_current_window->m_window_name,
                tmp_current_window->m_ctrl_id);

            tmp_current_window = tmp_current_window->m_child_window;
        }

        tmp_message.hwnd = GetLastChildWindowId(m_current_log_message->m_window_hierarchy);
        if (tmp_message.hwnd == NULL) {
            automation_stop_playback(m_session);
            if (m_error_callback)
                m_error_callback(m_session->m_last_error_msg);

            return 0;
        }

        m_last_window = tmp_message.hwnd;
    }
    else if (m_current_log_message->m_message_type == StopRecord) {
        if (IsWindowFromProcessHook(GetForegroundWindow())) {
            automation_stop_playback(m_session);
            net_session_print_error(m_session, "PlaybackMessage : Thread is still alive\n");
            LOG_ERROR(m_session, m_session->m_last_error_msg);
            if (m_error_callback)
                m_error_callback(m_session->m_last_error_msg);
        }

        return 0;
    }
    else
        tmp_message.hwnd = m_last_window;

    if (m_current_log_message->m_callback) {
        if (m_current_log_message->m_callback(m_current_log_message, tmp_message.hwnd, m_session->m_last_error_msg) != NOERR) {
            automation_stop_playback(m_session);
            if (m_error_callback)
                m_error_callback(m_session->m_last_error_msg);
        }
    }

    INPUT input = { 0 };
    switch (m_current_log_message->m_message_type)
    {
    case Keyboard:
        tmp_message.wParam = m_current_log_message->m_keyboard_vk;
        tmp_message.lParam = m_current_log_message->m_keyboard_repeat;
        input.type = INPUT_KEYBOARD;
        input.ki.wVk = MapVirtualKey(m_current_log_message->m_keyboard_vk, MAPVK_VSC_TO_VK_EX);
        input.ki.wScan = m_current_log_message->m_keyboard_vk;
        input.ki.dwFlags = KEYEVENTF_SCANCODE;

        if (m_current_log_message->m_message == WM_SYSKEYUP || m_current_log_message->m_message == WM_KEYUP)
            input.ki.dwFlags |= KEYEVENTF_KEYUP;

        if (m_current_log_message->m_keyboard_vk & 0xE000)
            input.ki.dwFlags |= KEYEVENTF_EXTENDEDKEY;

        SendInput(1, &input, sizeof(INPUT));
        LOG_TRACE(m_session, "PlaybackMessage : Post keyboard message %u,%ul/%ul\n", input.ki.wVk, m_current_log_message->m_keyboard_vk, m_current_log_message->m_keyboard_repeat);
        break;

    case Mouse:
        input.type = INPUT_MOUSE;

        tmp_message.wParam = 0;

        switch (tmp_message.message)
        {
        case WM_LBUTTONDOWN:
            tmp_message.wParam = MK_LBUTTON;
            input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
            break;
        case WM_LBUTTONUP:
            input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
            break;
        case WM_MOUSEWHEEL:
            input.mi.dwFlags = MOUSEEVENTF_WHEEL;
            break;
        case WM_MOUSEHWHEEL:
            input.mi.dwFlags = MOUSEEVENTF_HWHEEL;
            break;
        case WM_RBUTTONDOWN:
            tmp_message.wParam = MK_RBUTTON;
            input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
            break;
        case WM_RBUTTONUP:
            input.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
            break;
        case WM_MOUSEMOVE:
            input.mi.dwFlags = MOUSEEVENTF_MOVE;
            break;
        }

        input.mi.dwFlags |= MOUSEEVENTF_ABSOLUTE;

        POINT pos;
        RECT rect;
        GetWindowRect(tmp_message.hwnd, &rect);
        pos.x = m_current_log_message->m_mouse_x - rect.left;
        pos.y = m_current_log_message->m_mouse_y - rect.top;
        tmp_message.lParam = MAKELPARAM(pos.x, pos.y);
        //tmp_message.lParam = MAKELPARAM(m_current_log_message->m_mouse_x, m_current_log_message->m_mouse_y);

        POINT current_mouse_pos;
        GetCursorPos(&current_mouse_pos);
        SetCursorPos(m_current_log_message->m_mouse_x, m_current_log_message->m_mouse_y);
        SendInput(1, &input, sizeof(input));
        SetCursorPos(current_mouse_pos.x, current_mouse_pos.y);

        LOG_TRACE(m_session, "PlaybackMessage : Post mouse message %i/%i\n", m_current_log_message->m_mouse_x, m_current_log_message->m_mouse_y);
        break;

    default:
        tmp_message.wParam = 0;
        tmp_message.lParam = 0;
        LOG_TRACE(m_session, "PlaybackMessage : Post unknow message\n");
        break;
    }

    LOG_TRACE(m_session, "PlaybackMessage : Next message\n");
    m_move_next_message = TRUE;
    m_previous_log_message = m_current_log_message;
    m_current_log_message = m_current_log_message->m_next_message;
    m_start_time = GetTickCount();

    return PlaybackMessage();
}


/****************************************************************
  WH_JOURNALPLAYBACK hook procedure
 ****************************************************************/
LRESULT CALLBACK JournalPlaybackProc(int code, WPARAM wparam, LPARAM lparam)
{
    EVENTMSG *tmp_event = NULL;

    LRESULT delta;
    HWND tmp_active_window = NULL;

    switch (code)
    {
    case HC_GETNEXT:
        tmp_event = (EVENTMSG*)lparam;
        if (m_move_next_message)
        {
            LOG_TRACE(m_session, "JournalPlaybackProc : Move next message\n");
            if (m_current_log_message != NULL)
            {
                LOG_TRACE(m_session, "JournalPlaybackProc : check message\n");
                if (m_current_log_message->m_message_type == None)
                {
                    LOG_TRACE(m_session, "JournalPlaybackProc : Unknow message %i\n", m_current_log_message->m_message_type);
                    automation_stop_playback(m_session);
                    return 0;
                }
                ++m_messages_count;
            }
            else
            {
                LOG_TRACE(m_session, "JournalPlaybackProc : Start message queue\n");
                m_start_time = GetTickCount();
                m_current_log_message = m_log_message_list;
                m_move_next_message = TRUE;

                return 0;
            }

            m_move_next_message = FALSE;
        }

        LOG_TRACE(m_session, "JournalPlaybackProc : Treat message\n");

        tmp_event->message = m_current_log_message->m_message;
        switch (m_current_log_message->m_message_type)
        {
        case Keyboard:
            tmp_event->paramH = m_current_log_message->m_keyboard_vk;
            tmp_event->paramL = m_current_log_message->m_keyboard_repeat;
            break;

        case Mouse:
            tmp_event->paramL = m_current_log_message->m_mouse_x;
            tmp_event->paramH = m_current_log_message->m_mouse_y;

            break;

        default:
            tmp_event->paramH = 0;
            tmp_event->paramL = 0;
            break;
        }

        tmp_event->time = m_start_time + m_current_log_message->m_time;
        delta = tmp_event->time - GetTickCount();
        if (delta > 0)
        {
            LOG_TRACE(m_session, "JournalPlaybackProc : Wait message %li (%li/%li)\n", m_current_log_message->m_message, delta, m_current_log_message->m_time);
            return delta;
        }
        else
        {
            LOG_TRACE(m_session, "JournalPlaybackProc : Play message %li (%li/%li)\n", m_current_log_message->m_message, delta, m_current_log_message->m_time);
            if (m_current_log_message->m_window_hierarchy && m_current_log_message->m_window_hierarchy->m_window_atom) {
                LOG_TRACE(m_session, "JournalPlaybackProc : Retreiving window ATOM=%lu, Class=%s, Text=%s, CtrlId=%lu\n",
                    m_current_log_message->m_window_hierarchy->m_window_atom,
                    m_current_log_message->m_window_hierarchy->m_window_class_name,
                    m_current_log_message->m_window_hierarchy->m_window_name,
                    m_current_log_message->m_window_hierarchy->m_ctrl_id);

                tmp_event->hwnd = GetLastChildWindowId(m_current_log_message->m_window_hierarchy);
                if (tmp_event->hwnd == NULL) {
                    automation_stop_playback(m_session);
                    if (m_error_callback)
                        m_error_callback(m_session->m_last_error_msg);

                    return 0;
                }

                m_last_window = tmp_event->hwnd;

                if (m_current_log_message->m_callback) {
                    if (m_current_log_message->m_callback(m_current_log_message, tmp_event->hwnd, m_session->m_last_error_msg) != NOERR) {
                        automation_stop_playback(m_session);
                        if (m_error_callback)
                            m_error_callback(m_session->m_last_error_msg);
                    }
                }
            }
            else if (m_current_log_message->m_message_type == StopRecord) {
                if (IsWindowFromProcessHook(GetForegroundWindow())) {
                    automation_stop_playback(m_session);
                    net_session_print_error(m_session, "JournalPlaybackProc : Thread is still alive\n");
                    LOG_ERROR(m_session, m_session->m_last_error_msg);
                    if (m_error_callback)
                        m_error_callback(m_session->m_last_error_msg);
                }
            }

            return 0;
        }
        break;

    case HC_SKIP:
        LOG_TRACE(m_session, "JournalPlaybackProc : Next message\n");
        m_move_next_message = TRUE;
        m_previous_log_message = m_current_log_message;
        m_current_log_message = m_current_log_message->m_next_message;
        m_start_time = GetTickCount();
        break;

    default:
        LOG_TRACE(m_session, "JournalPlaybackProc : %i\n", code);
        return CallNextHookEx(m_journal_hook_handle, code, wparam, lparam);
    }

    return 0;
}


windows_hierarchy* GetWindowHierarchy(HWND in_window_id, windows_hierarchy* in_window_hierarchy)
{
    windows_hierarchy* tmp_root_window_hierarchy = in_window_hierarchy;
    windows_hierarchy* tmp_parent_window_hierarchy = NULL;
    HWND tmp_parent_window_id = NULL;
    windows_hierarchy* tmp_child_window_hierarchy = NULL;

    WINDOWINFO tmp_window_info;

    in_window_hierarchy->m_is_active = (in_window_id == GetForegroundWindow());

    if (GetWindowInfo(in_window_id, &tmp_window_info) == TRUE)
    {
        in_window_hierarchy->m_window_atom = tmp_window_info.atomWindowType;
        GetClassNameA(in_window_id, in_window_hierarchy->m_window_class_name, 1024);
        GetWindowTextA(in_window_id, in_window_hierarchy->m_window_name, 1024);
        in_window_hierarchy->m_window_rect = tmp_window_info.rcWindow;
    }

    in_window_hierarchy->m_ctrl_id = GetDlgCtrlID(in_window_id);

    LOG_TRACE(m_session, "--------------- hwnd=%p, atom=%lu, class=%s, text=%s, ctrlid=%lu\n"
        , in_window_id
        , in_window_hierarchy->m_window_atom
        , in_window_hierarchy->m_window_class_name
        , in_window_hierarchy->m_window_name
        , in_window_hierarchy->m_ctrl_id);

    tmp_parent_window_id = GetAncestor(in_window_id, GA_PARENT);
    if (tmp_parent_window_id != NULL)
    {
        tmp_parent_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
        memset(tmp_parent_window_hierarchy, 0, sizeof(windows_hierarchy));

        tmp_root_window_hierarchy = GetWindowHierarchy(tmp_parent_window_id, tmp_parent_window_hierarchy);
        tmp_parent_window_hierarchy->m_child_window = in_window_hierarchy;
    }
    else
    {
        // Retreive all childs windows
        tmp_root_window_hierarchy->m_window_id = in_window_id;
        GetChildWindowHierarchy(tmp_root_window_hierarchy);
    }

    return tmp_root_window_hierarchy;
}


void GetChildWindowHierarchy(windows_hierarchy* in_window_hierarchy)
{
    HWND tmp_child_window_id = GetWindow(in_window_hierarchy->m_window_id, GW_HWNDNEXT);

    FillWindowHierarchyInfos(in_window_hierarchy);

    if (tmp_child_window_id) {
        in_window_hierarchy->m_next_window = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
        memset(in_window_hierarchy->m_next_window, 0, sizeof(windows_hierarchy));

        in_window_hierarchy->m_next_window->m_window_id = tmp_child_window_id;

        GetChildWindowHierarchy(in_window_hierarchy->m_next_window);
    }

    tmp_child_window_id = GetWindow(in_window_hierarchy->m_window_id, GW_CHILD);
    if (tmp_child_window_id) {
        in_window_hierarchy->m_first_child_window = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
        memset(in_window_hierarchy->m_first_child_window, 0, sizeof(windows_hierarchy));

        in_window_hierarchy->m_first_child_window->m_window_id = tmp_child_window_id;

        GetChildWindowHierarchy(in_window_hierarchy->m_first_child_window);
    }
}

void FillWindowHierarchyInfos(windows_hierarchy* in_window_hierarchy)
{
    WINDOWINFO tmp_window_info;

    in_window_hierarchy->m_is_active = (in_window_hierarchy->m_window_id == GetForegroundWindow());

    if (GetWindowInfo(in_window_hierarchy->m_window_id, &tmp_window_info) == TRUE)
    {
        in_window_hierarchy->m_window_atom = tmp_window_info.atomWindowType;
        GetClassNameA(in_window_hierarchy->m_window_id, in_window_hierarchy->m_window_class_name, 1024);
        GetWindowTextA(in_window_hierarchy->m_window_id, in_window_hierarchy->m_window_name, 1024);
        in_window_hierarchy->m_window_rect = tmp_window_info.rcWindow;
    }

    in_window_hierarchy->m_ctrl_id = GetDlgCtrlID(in_window_hierarchy->m_window_id);
    if (in_window_hierarchy->m_ctrl_id) {
#ifdef _AFX
        CWnd *tmp_wnd = CWnd::FromHandle(in_window_hierarchy->m_window_id);

        if (tmp_wnd) {
            if (tmp_wnd->GetRuntimeClass() == &CEdit::classCEdit) {

            }

            strcpy(in_window_hierarchy->m_window_class_name, tmp_wnd->GetRuntimeClass()->m_lpszClassName);

            in_window_hierarchy->m_is_visible = tmp_wnd->IsWindowVisible();
        }
        LOG_TRACE(m_session, "%s => %s\n", in_window_hierarchy->m_window_name, tmp_wnd->GetRuntimeClass()->m_lpszClassName);
#endif
    }
}


BOOL windowMatches(HWND hwnd, windows_hierarchy* in_window_hierarchy)
{
    WINDOWINFO tmp_window_info;
    CHAR tmp_window_class_name[1024];
    CHAR tmp_window_name[1024];

    if (GetWindowInfo(hwnd, &tmp_window_info) == TRUE) {
        GetClassNameA(hwnd, tmp_window_class_name, 1024);
        GetWindowTextA(hwnd, tmp_window_name, 1024);
        if (strcmp(tmp_window_class_name, in_window_hierarchy->m_window_class_name) == 0
            && strcmp(tmp_window_name, in_window_hierarchy->m_window_name) == 0) {
            return TRUE;
        }
    }

    return FALSE;
}


HWND GetLastChildWindowId(windows_hierarchy* in_root_window_hierarchy)
{
    WINDOWINFO tmp_window_info;
    CHAR tmp_window_class_name[1024];
    CHAR tmp_window_name[1024];

    // root window
    in_root_window_hierarchy->m_window_id = GetDesktopWindow();
    if (in_root_window_hierarchy->m_window_id != NULL && GetWindowInfo(in_root_window_hierarchy->m_window_id, &tmp_window_info) == TRUE)
    {
        in_root_window_hierarchy->m_ctrl_id = GetDlgCtrlID(in_root_window_hierarchy->m_window_id);
        GetClassNameA(in_root_window_hierarchy->m_window_id, tmp_window_class_name, 1024);
        GetWindowTextA(in_root_window_hierarchy->m_window_id, tmp_window_name, 1024);
        if (strcmp(tmp_window_class_name, in_root_window_hierarchy->m_window_class_name) == 0
            && strcmp(tmp_window_name, in_root_window_hierarchy->m_window_name) == 0) {
            LOG_TRACE(m_session, "Found root window  ATOM=%lu, Class=%s, Text=%s, CtrlId=%lu\n", tmp_window_info.atomWindowType, tmp_window_class_name, tmp_window_name, in_root_window_hierarchy->m_ctrl_id);
            return GetChildWindowId(in_root_window_hierarchy);
        }
    }

    net_session_print_error(m_session, "GetLastChildWindowId : unable to retrieve root/desktop window\n");
    LOG_ERROR(m_session, m_session->m_last_error_msg);

    return NULL;
}

HWND GetChildWindowId(windows_hierarchy* in_window_hierarchy)
{
    HWND tmp_parent_wnd = in_window_hierarchy->m_window_id;
    HWND tmp_current_wnd = NULL;
    windows_hierarchy* tmp_window_hierarchy = in_window_hierarchy->m_child_window;

    if (tmp_window_hierarchy) {
        LOG_TRACE(m_session, "GetChildWindowId : Searching window  ATOM=%lu, Class=%s, Text=%s, CtrlId=%lu\n",
            tmp_window_hierarchy->m_window_atom,
            tmp_window_hierarchy->m_window_class_name,
            tmp_window_hierarchy->m_window_name,
            tmp_window_hierarchy->m_ctrl_id);
        tmp_current_wnd = GetWindow(tmp_parent_wnd, GW_CHILD);
        if (tmp_current_wnd) {
            while (tmp_current_wnd != NULL) {
                if (/*(tmp_window_hierarchy->m_is_thread_window && tmp_current_wnd == m_thread_window_handle) || */
                    windowMatches(tmp_current_wnd, tmp_window_hierarchy) == TRUE) {
                    tmp_window_hierarchy->m_window_id = tmp_current_wnd;
                    LOG_TRACE(m_session, "Found window : Id=%p, ATOM=%lu, Class=%s, Text=%s, CtrlId=%lu\n"
                        , tmp_current_wnd
                        , tmp_window_hierarchy->m_window_atom
                        , tmp_window_hierarchy->m_window_class_name
                        , tmp_window_hierarchy->m_window_name
                        , tmp_window_hierarchy->m_ctrl_id);

                    if (SetWindowPos(tmp_current_wnd, NULL,
                        tmp_window_hierarchy->m_window_rect.left,
                        tmp_window_hierarchy->m_window_rect.top,
                        tmp_window_hierarchy->m_window_rect.right - tmp_window_hierarchy->m_window_rect.left,
                        tmp_window_hierarchy->m_window_rect.bottom - tmp_window_hierarchy->m_window_rect.top,
                        SWP_NOZORDER | SWP_NOACTIVATE) == FALSE) {

                        net_session_print_error(m_session, "GetChildWindowId : unable to move/resize window Id=%p, ATOM=%lu, Class=%s, Text=%s, CtrlId=%lu\n"
                            , tmp_current_wnd
                            , tmp_window_hierarchy->m_window_atom
                            , tmp_window_hierarchy->m_window_class_name
                            , tmp_window_hierarchy->m_window_name
                            , tmp_window_hierarchy->m_ctrl_id);

                        LOG_ERROR(m_session, m_session->m_last_error_msg);
                    }
                    return tmp_current_wnd;
                }

                tmp_current_wnd = GetWindow(tmp_current_wnd, GW_HWNDNEXT);
            }
            net_session_print_error(m_session, "GetChildWindowId : unable to retrieve window Class=%s, Text=%s\n"
                , tmp_window_hierarchy->m_window_class_name
                , tmp_window_hierarchy->m_window_name);

            LOG_ERROR(m_session, m_session->m_last_error_msg);

            return NULL;
        }
        else {
            net_session_print_error(m_session, "GetChildWindowId : GetWindow GW_CHILD.\n");
            LOG_ERROR(m_session, m_session->m_last_error_msg);

        }
        return NULL;
    }
    else
        LOG_TRACE(m_session, "GetChildWindowId : No child window.\n");

    return tmp_parent_wnd;
}


HWND GetThreadWindow()
{
    HWND tmp_parent_wnd = GetDesktopWindow();
    HWND tmp_current_wnd = NULL;

    if (tmp_parent_wnd) {
        tmp_current_wnd = GetWindow(tmp_parent_wnd, GW_CHILD);
        if (tmp_current_wnd) {
            while (tmp_current_wnd != NULL) {
                if (IsWindowFromProcessHook(tmp_current_wnd)) {
                    return tmp_current_wnd;
                }

                tmp_current_wnd = GetWindow(tmp_current_wnd, GW_HWNDNEXT);
            }
            LOG_TRACE(m_session, "GetThreadWindow : No window matches.\n");
        }
        else
            LOG_TRACE(m_session, "GetThreadWindow : GetWindow GW_CHILD.\n");

        return NULL;
    }
    else
        LOG_TRACE(m_session, "GetThreadWindow : No child window.\n");

    return NULL;
}

BOOL IsWindowFromProcessHook(HWND hwnd)
{
    DWORD process = m_process_id;
    DWORD thread = GetWindowThreadProcessId(hwnd, &process);

    return TRUE;
    return m_process_id == process /*&& m_thread_id == thread*/;
}