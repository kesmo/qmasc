/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_NEW_PROJECT_H
#define FORM_NEW_PROJECT_H

#include <QtWidgets/QDialog>
#include "entities/project.h"
#include "entities/projectversion.h"
#include "gui/components/RecordTextEditContainer.h"

QT_FORWARD_DECLARE_CLASS(Ui_FormNewProject)

class FormNewProject : public QDialog
{
    Q_OBJECT

public:
    FormNewProject(ProjectVersion* in_project_record = NULL, QWidget *parent = 0);
    ~FormNewProject();

signals:
    void projectSelected(const QString &projectId, const QString &projectVersionId);

public Q_SLOTS:
    void accept();

private:
    Ui_FormNewProject   *m_ui;
    ProjectVersion *m_project_version;

    bool              m_new_version;
};

#endif // FORM_NEW_PROJECT_H
