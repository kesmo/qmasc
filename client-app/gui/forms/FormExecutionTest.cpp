/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormExecutionTest.h"
#include "entities/test.h"
#include "entities/projectgrant.h"
#include "entities/executioncampaignparameter.h"
#include "entities/executiontestparameter.h"
#include "ui_FormExecutionTest.h"
#include "FormExecutionBugs.h"
#include "session.h"
#include "entities/action.h"
#include "entities/campaign.h"
#include "entities/automatedaction.h"
#include "entities/executioncampaign.h"
#include "entities/projectversion.h"

#include <QMessageBox>
#include <QInputDialog>
#include <QDomDocument>

FormExecutionTest::FormExecutionTest(QWidget *parent) : QWidget(parent), m_ui(new Ui::FormExecutionTest)
{
    m_execution_test = NULL;
    m_execution_action = NULL;
    m_test_content = NULL;
    m_ui->setupUi(this);

    m_ui->test_description->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->action_description->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->action_result->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->comments->addTextToolBar(RecordTextEditToolBar::Small);

    QList< QPair<QString, QString> >  tmp_automated_actions_table_headers;
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Type de message"), AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE);
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Données"), AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Délai"), AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY);
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Vérifications"), QString::null);

    m_automated_actions_model = new RecordsTableModel<AutomatedAction>(tmp_automated_actions_table_headers, QList<AutomatedAction*>(), this);
    m_automated_actions_model->setColumnDataFunctionForColumn(0, &FunctionAutomatedActionMessageType);
    m_automated_actions_model->setColumnDataFunctionForColumn(1, &FunctionAutomatedActionMessageData);
    m_automated_actions_model->setColumnDataFunctionForColumn(2, &FunctionAutomatedActionMessageDelay);
    m_automated_actions_model->setColumnDataFunctionForColumn(3, &FunctionAutomatedActionMessageCallback);
    m_ui->automated_actions_list->setModel(m_automated_actions_model);
    m_ui->automated_actions_list->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_ui->automated_actions_list, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showAutomatedActionValidationContextMenu(QPoint)));
    m_ui->automated_actions_list->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_ui->automated_actions_list->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    QList< QPair<QString, QString> >  tmp_parameters_table_headers;
    tmp_parameters_table_headers << qMakePair<QString, QString>(tr("Nom"), EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_NAME);
    tmp_parameters_table_headers << qMakePair<QString, QString>(tr("Valeur"), EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE);
    m_parameters_model = new RecordsTableModel<ExecutionTestParameter>(tmp_parameters_table_headers, QList<ExecutionTestParameter*>(), this);
    m_ui->execution_parameters_list->setModel(m_parameters_model);
    m_ui->execution_parameters_list->setContextMenuPolicy(Qt::CustomContextMenu);
    m_ui->execution_parameters_list->horizontalHeader()->setStretchLastSection(true);
    connect(m_ui->execution_parameters_list, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showExecutionTestParametersContextMenu(QPoint)));

    m_ui->execution_parameters_list->setSelectionMode(QAbstractItemView::SingleSelection);
    m_ui->execution_parameters_list->verticalHeader()->setVisible(false);
    m_ui->execution_parameters_list->verticalHeader()->setDefaultSectionSize(18);
    m_ui->execution_parameters_list->horizontalHeader()->setVisible(true);
    m_ui->execution_parameters_list->horizontalHeader()->setMinimumSectionSize(50);

    connect(m_ui->save_comments, SIGNAL(clicked()), this, SLOT(saveComments()));
    connect(m_ui->save_test_description, SIGNAL(clicked()), this, SLOT(saveTestDescription()));
    connect(m_ui->ok_button, SIGNAL(clicked()), this, SLOT(validateResult()));
    connect(m_ui->ko_button, SIGNAL(clicked()), this, SLOT(inValidateResult()));
    connect(m_ui->bypass_button, SIGNAL(clicked()), this, SLOT(bypass()));
    connect(m_ui->next_button, SIGNAL(clicked()), this, SLOT(selectNext()));
    connect(m_ui->previous_button, SIGNAL(clicked()), this, SLOT(selectPrevious()));
    connect(m_ui->manage_bugs_button, SIGNAL(clicked()), this, SLOT(manageBugs()));
    connect(m_ui->modify_button, SIGNAL(clicked()), this, SLOT(modifyAction()));
    connect(m_ui->modify_test_button, SIGNAL(clicked()), this, SLOT(modifyTest()));
    connect(m_ui->reinit_parameters_button, SIGNAL(clicked()), this, SLOT(reinitExecutionsTestsParameters()));
    connect(m_ui->button_launch_batch_command, SIGNAL(clicked()), this, SLOT(launchBatch()));
    connect(m_ui->button_start_playback, SIGNAL(clicked()), this, SLOT(launchStartPlaybackSystemEvents()));
    connect(m_ui->button_stop_playback, SIGNAL(clicked()), this, SLOT(stopPlaybackProcess()));

    initLayout();
}

FormExecutionTest::~FormExecutionTest()
{
    m_batch_automation_processor.stopProcess(0, false);
    m_gui_automation_processor.stopProcess(0, false);

    delete m_test_content;
    delete m_automated_actions_model;
    delete m_parameters_model;
    delete m_ui;
}

void FormExecutionTest::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


/**
  Charger un test
**/
void FormExecutionTest::loadTest(ExecutionTest *in_execution_test, ExecutionAction *in_execution_action)
{
    Test    *tmp_test = NULL;
    const char        *tmp_test_content_id = NULL;
    int            tmp_result = NOERR;

    m_execution_test = in_execution_test;
    m_execution_action = in_execution_action;

    if (m_execution_test != NULL)
    {
        tmp_test = in_execution_test->projectTest();
        if (tmp_test != NULL)
        {
            if (tmp_test->original() != NULL)
                tmp_test_content_id = tmp_test->original()->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID);
            else
                tmp_test_content_id = tmp_test->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID);

            // Supprimer les données du précédent contenu de test
            if (m_test_content != NULL && compare_values(m_test_content->getIdentifier(), tmp_test_content_id) != 0)
            {
                destroyAttachments();

                delete m_test_content;
                m_test_content = NULL;
            }

            // Charger les données du contenu de test
            if (m_test_content == NULL)
            {
                m_test_content = new TestContent();
                tmp_result = m_test_content->loadRecord(tmp_test_content_id);
                if (tmp_result == NOERR)
                {
                    // Charger les pièces jointes
                    m_files = TestContent::TestContentFileRelation::instance().getChilds(m_test_content);

                    // Charger les actions automatisées associees
                    if (m_test_content->isAutomatedGuiTest())
                    {
                        m_automated_actions_model->setRecordsList(TestContent::AutomatedActionRelation::instance().getChilds(m_test_content));
                        m_ui->automated_actions_list->resizeColumnsToContents();
                    }
                }
            }
        }
    }


    initLayout();
}


/**
  Initialiser l'affichage
**/
void FormExecutionTest::initLayout()
{
    Action        *tmp_action = NULL;
    bool        tmp_modifiable = false;
    bool        tmp_original_action_modifiable = false;

    QPixmap        tmp_test_image_status;
    QString        tmp_image_status_url = QString::fromUtf8(":/images/warning.png");
    QString        tmp_image_status_label = tr("Non passé/incomplet");

    QList<QString>    tmp_parameters_list;

    QList<QString>    tmp_parameters_names_list;

    const char        *tmp_result_id = NULL;

    m_ui->action_group_box->setTitle(tr("Action"));

    m_current_parameters.clear();

    if (m_execution_test != NULL)
    {
        m_ui->groupBox->setVisible(true);
        m_ui->next_button->setVisible(true);
        m_ui->previous_button->setVisible(true);

        if (m_execution_test->executionCampaign() != NULL)
            tmp_parameters_names_list = Parameter::parametersNamesList<ExecutionCampaignParameter>(m_execution_test->executionCampaign()->parametersList());

        m_ui->test_description->textEditor()->setCompletionFromList(tmp_parameters_names_list);
        m_ui->action_description->textEditor()->setCompletionFromList(tmp_parameters_names_list);
        m_ui->action_result->textEditor()->setCompletionFromList(tmp_parameters_names_list);

        tmp_result_id = m_execution_test->getValueForKey(EXECUTIONS_TESTS_TABLE_RESULT_ID);
        if (is_empty_string(tmp_result_id) == FALSE)
        {
            if (compare_values(tmp_result_id, EXECUTION_ACTION_VALIDATED) == 0)
            {
                tmp_image_status_url = QString::fromUtf8(":/images/ok.png");
                tmp_image_status_label = tr("Ok");
            }
            else if (compare_values(tmp_result_id, EXECUTION_ACTION_INVALIDATED) == 0)
            {
                tmp_image_status_url = QString::fromUtf8(":/images/ko.png");
                tmp_image_status_label = tr("Ko");
            }
        }

        tmp_test_image_status = QPixmap(tmp_image_status_url);
        m_ui->test_image_status->setPixmap(tmp_test_image_status);
        m_ui->test_label_status->setText(tmp_image_status_label);

        if (m_execution_test->projectTest())
        {
            tmp_modifiable = m_execution_test->executionCampaign() != NULL
                    && m_execution_test->executionCampaign()->campaign() != NULL
                    && m_execution_test->executionCampaign()->campaign()->projectVersion() != NULL
                    && m_execution_test->executionCampaign()->campaign()->projectVersion()->canWriteExecutionsCampaigns();

            tmp_original_action_modifiable = tmp_modifiable && m_execution_test->executionCampaign()->campaign()->projectVersion()->canWriteTests();

            m_ui->test_name->setText(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME));
            tmp_parameters_list = RecordTextEdit::parametersList(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));
            if (tmp_parameters_list.isEmpty())
            {
                m_ui->test_description->textEditor()->setHtml(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));
            }
            else
            {
                m_current_parameters.append(tmp_parameters_list);

                m_ui->test_description->textEditor()->setHtml(
                            RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(
                                m_execution_test->inheritedParameters(),
                                m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION)));
            }


            m_ui->automated_actions_list->horizontalHeader()->setStretchLastSection(true);
            m_ui->labelCommande->setText(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND));
            m_ui->labelParametres->setText(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS));
            m_ui->batch_return_code_variable->setText(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE));
            m_ui->batch_stdout_variable->setText(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE));
            if (m_execution_test->projectTest()->isAutomatedGuiTest()){
                m_ui->groupBoxAutomatisation->setVisible(true);
                m_ui->action_group_box->setVisible(false);
                m_ui->automated_gui_actions_widget->setVisible(true);
                m_ui->batch_widget->setVisible(false);
            } else if (m_execution_test->projectTest()->isAutomatedBatchTest()){
                m_ui->groupBoxAutomatisation->setVisible(true);
                m_ui->action_group_box->setVisible(false);
                m_ui->automated_gui_actions_widget->setVisible(false);
                m_ui->batch_widget->setVisible(true);
            }
            else
                m_ui->groupBoxAutomatisation->setVisible(false);
        }

        if (m_execution_action != NULL)
        {
            m_ui->action_group_box->setVisible(true);
            tmp_result_id = m_execution_action->getValueForKey(EXECUTIONS_ACTIONS_TABLE_RESULT_ID);
            //tmp_modifiable = (is_empty_string(m_execution_action->getIdentifier()) || is_empty_string(tmp_result_id));
            m_ui->action_group_box->setTitle(tr("Action %1 sur %2").arg(QString::number(m_execution_test->actions().indexOf(m_execution_action) + 1)).arg(QString::number(m_execution_test->actions().count())));

            if (is_empty_string(tmp_result_id) == FALSE)
            {
                tmp_image_status_label = tr("Non passée");
                if (compare_values(tmp_result_id, EXECUTION_ACTION_VALIDATED) == 0)
                {
                    tmp_image_status_url = QString::fromUtf8(":/images/ok.png");
                    tmp_image_status_label = tr("Ok");
                }
                else if (compare_values(tmp_result_id, EXECUTION_ACTION_INVALIDATED) == 0)
                {
                    tmp_image_status_url = QString::fromUtf8(":/images/ko.png");
                    tmp_image_status_label = tr("Ko");
                }
            }

            tmp_test_image_status = QPixmap(tmp_image_status_url);
            m_ui->action_image_status->setPixmap(tmp_test_image_status);
            m_ui->action_label_status->setText(tmp_image_status_label);

            m_ui->comments->textEditor()->setHtml(m_execution_action->getValueForKey(EXECUTIONS_ACTIONS_TABLE_COMMENTS));

            tmp_action = Action::ExecutionActionRelation::instance().getParent(m_execution_action);
            if (tmp_action != NULL)
            {
                // Appliquer les paramètres d'exécutions pour le champ description de l'action
                tmp_parameters_list = RecordTextEdit::parametersList(tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
                if (tmp_parameters_list.isEmpty())
                {
                    m_ui->action_description->textEditor()->setHtml(tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
                }
                else
                {
                    m_current_parameters.append(tmp_parameters_list);

                    m_ui->action_description->textEditor()->setHtml(
                                RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(
                                    m_execution_test->inheritedParameters(),
                                    tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION)));
                }
                loadTextEditAttachments(m_ui->action_description->textEditor());

                // Appliquer les paramètres d'exécutions pour le champ résultat attendu de l'action
                tmp_parameters_list = RecordTextEdit::parametersList(tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
                if (tmp_parameters_list.isEmpty())
                {
                    m_ui->action_result->textEditor()->setHtml(tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
                }
                else
                {
                    m_current_parameters.append(tmp_parameters_list);

                    m_ui->action_result->textEditor()->setHtml(
                                RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(
                                    m_execution_test->inheritedParameters(),
                                    tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT)));
                }
                loadTextEditAttachments(m_ui->action_result->textEditor());

            }
            else
                tmp_original_action_modifiable = false;

        }
        else
        {
            m_ui->action_group_box->setVisible(false);
            tmp_original_action_modifiable = false;
        }

        synchronizeExecutionsTestsParameters(m_current_parameters);


        m_ui->reinit_parameters_button->setEnabled(!m_execution_test->parameters().isEmpty());
    }
    else
    {
        m_ui->groupBox->setVisible(false);
        m_ui->groupBoxAutomatisation->setVisible(false);
        m_ui->action_group_box->setVisible(false);
        m_ui->automated_gui_actions_widget->setVisible(false);
        m_ui->batch_widget->setVisible(false);
        m_ui->next_button->setVisible(false);
        m_ui->previous_button->setVisible(false);

    }

    m_ui->save_comments->setVisible(tmp_modifiable);
    m_ui->save_test_description->setVisible(tmp_modifiable);
    m_ui->reinit_parameters_button->setVisible(tmp_modifiable);
    m_ui->ok_button->setVisible(tmp_modifiable);
    m_ui->ko_button->setVisible(tmp_modifiable);
    m_ui->bypass_button->setVisible(tmp_modifiable);
    m_ui->modify_button->setVisible(tmp_original_action_modifiable);
    m_ui->comments->toolBar()->setVisible(tmp_modifiable);
    m_ui->comments->textEditor()->setReadOnly(!tmp_modifiable);

    m_ui->next_button->setEnabled(true);
    m_ui->previous_button->setEnabled(true);
    m_ui->modify_test_button->setVisible(tmp_modifiable);

    m_ui->test_description->textEditor()->setReadOnly(true);
    m_ui->test_description->toolBar()->hide();

    m_ui->action_description->textEditor()->setReadOnly(true);
    m_ui->action_description->toolBar()->hide();

    m_ui->action_result->textEditor()->setReadOnly(true);
    m_ui->action_result->toolBar()->hide();


    initManageBugsButtonLabel();
}



void FormExecutionTest::setExecutionTestData(const char *in_result_id)
{
    if (m_execution_test != NULL)
    {
        m_execution_test->setValueForKey(NOW, EXECUTIONS_TESTS_TABLE_EXECUTION_DATE);
        m_execution_test->setValueForKey(in_result_id, EXECUTIONS_TESTS_TABLE_RESULT_ID);
    }
}


void FormExecutionTest::setExecutionActionData(const char *in_result_id)
{
    if (m_execution_action != NULL)
    {
        m_execution_action->setValueForKey(in_result_id, EXECUTIONS_ACTIONS_TABLE_RESULT_ID);
        m_execution_action->setValueForKey(m_ui->comments->textEditor()->toOptimizedHtml().toStdString().c_str(), EXECUTIONS_ACTIONS_TABLE_COMMENTS);

        if (m_execution_test != NULL)
        {
            if(m_execution_test->executionBypassedRate() == 1.0)
                m_execution_test->setValueForKey(EXECUTION_TEST_BYPASSED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
            else if(m_execution_test->executionValidatedRate() == 1.0)
                m_execution_test->setValueForKey(EXECUTION_TEST_VALIDATED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
            else if(m_execution_test->executionInValidatedRate() > 0.0)
                m_execution_test->setValueForKey(EXECUTION_TEST_INVALIDATED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
            else if(m_execution_test->executionCoverageRate() < 1.0)
                m_execution_test->setValueForKey(EXECUTION_TEST_INCOMPLETED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
        }

        saveOriginalAction();
    }
}


void FormExecutionTest::validateResult()
{
    if (m_execution_action != NULL)
        setExecutionActionData(EXECUTION_ACTION_VALIDATED);
    else if (m_execution_test != NULL)
        setExecutionTestData(EXECUTION_TEST_VALIDATED);

    emit resultValidated();
}


void FormExecutionTest::saveComments()
{
    if (m_execution_action != NULL)
    {
        m_execution_action->setValueForKey(m_ui->comments->textEditor()->toOptimizedHtml().toStdString().c_str(), EXECUTIONS_ACTIONS_TABLE_COMMENTS);
        saveOriginalAction();
        initLayout();
    }
}



void FormExecutionTest::saveTestDescription()
{
    int tmp_save_result = NOERR;
    if (m_test_content != NULL)
    {
        m_test_content->setValueForKey(m_ui->test_description->textEditor()->toOptimizedHtml().toStdString().c_str(), TESTS_CONTENTS_TABLE_DESCRIPTION);
        tmp_save_result = m_test_content->saveRecord();
        if (tmp_save_result != NOERR)
        {
            QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
        }

        initLayout();
    }
}

void FormExecutionTest::inValidateResult()
{
    if (m_execution_action != NULL)
        setExecutionActionData(EXECUTION_ACTION_INVALIDATED);
    else if (m_execution_test != NULL)
        setExecutionTestData(EXECUTION_TEST_INVALIDATED);

    emit resultInValidated();
}


void FormExecutionTest::bypass()
{
    if (m_execution_action != NULL)
        setExecutionActionData(EXECUTION_ACTION_BYPASSED);
    else if (m_execution_test != NULL)
        setExecutionTestData(EXECUTION_TEST_BYPASSED);

    emit bypassed();
}


void FormExecutionTest::selectNext()
{
    emit nextSelected();
}


void FormExecutionTest::selectPrevious()
{
    emit previousSelected();
}


void FormExecutionTest::manageBugs()
{
    FormExecutionBugs        *tmp_formbug = new FormExecutionBugs(m_execution_test, m_execution_action, this);

    connect(tmp_formbug, SIGNAL(accepted()), this, SLOT(initManageBugsButtonLabel()));

    tmp_formbug->show();
}


void FormExecutionTest::initManageBugsButtonLabel()
{
    char                ***tmp_results = NULL;
    unsigned long            tmp_rows_count = 0, tmp_columns_count = 0;
    net_session                *tmp_session = Session::instance().getClientSession();

    m_ui->manage_bugs_button->setText(tr("Anomalies"));

    if (m_execution_action != NULL)
        net_session_print_query(tmp_session, "SELECT count(*) FROM %s WHERE %s=%s;", BUGS_TABLE_SIG, BUGS_TABLE_EXECUTION_ACTION_ID, m_execution_action->getIdentifier());
    else if (m_execution_test != NULL)
        net_session_print_query(tmp_session, "SELECT count(*) FROM %s WHERE %s=%s;", BUGS_TABLE_SIG, BUGS_TABLE_EXECUTION_TEST_ID, m_execution_test->getIdentifier());
    else
        return;

    tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
    if (tmp_results != NULL)
    {
        if (tmp_rows_count > 0 && tmp_columns_count > 0 && compare_values(tmp_results[0][0], "0") != 0)
        {
            m_ui->manage_bugs_button->setText(tr("Anomalies (%1)").arg(tmp_results[0][0]));
        }

        cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
    }
}


void FormExecutionTest::modifyAction()
{
    Action  *tmp_action = Action::ExecutionActionRelation::instance().getParent(m_execution_action);

    if (tmp_action != NULL)
    {
        m_ui->action_description->textEditor()->setReadOnly(false);
        m_ui->action_description->toolBar()->show();

        m_ui->action_result->textEditor()->setReadOnly(false);
        m_ui->action_result->toolBar()->show();

        m_ui->action_description->textEditor()->setHtml(tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
        m_ui->action_result->textEditor()->setHtml(tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
    }
}


void FormExecutionTest::modifyTest()
{
    if (m_test_content != NULL)
    {
        m_ui->test_description->textEditor()->setReadOnly(false);
        m_ui->test_description->toolBar()->show();

        m_ui->test_description->textEditor()->setHtml(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));
    }
}


void FormExecutionTest::saveOriginalAction()
{
    Action  *tmp_action = NULL;
    int        tmp_save_result = NOERR;

    if (m_execution_action != NULL)
    {
        tmp_action = Action::ExecutionActionRelation::instance().getParent(m_execution_action);
        if (tmp_action != NULL && m_ui->action_description->textEditor()->isReadOnly() == false)
        {
            tmp_action->setValueForKey(m_ui->action_description->textEditor()->toOptimizedHtml().toStdString().c_str(), ACTIONS_TABLE_DESCRIPTION);
            tmp_action->setValueForKey(m_ui->action_result->textEditor()->toOptimizedHtml().toStdString().c_str(), ACTIONS_TABLE_WAIT_RESULT);
            tmp_save_result = tmp_action->saveRecord();
            if (tmp_save_result != NOERR)
            {
                QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
            }
        }
    }
}



void FormExecutionTest::synchronizeExecutionsTestsParameters(QList<QString> in_parameters_list, bool ask_user)
{
    ExecutionTestParameter        *tmp_test_param = NULL;
    const char                *tmp_campaign_param_value = NULL;
    ExecutionTest            *tmp_parent_test = NULL;

    foreach(QString tmp_param_name, in_parameters_list)
    {
        tmp_test_param = Parameter::parameterForParamName<ExecutionTestParameter>(m_execution_test->parameters(), tmp_param_name.toStdString().c_str());
        if (tmp_test_param == NULL || is_empty_string(tmp_test_param->getValueForKey(EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE)))
        {
            tmp_parent_test = m_execution_test;
            while (tmp_test_param == NULL && tmp_parent_test->getParent())
            {
                tmp_parent_test = tmp_parent_test->getParent();
                tmp_test_param = Parameter::parameterForParamName<ExecutionTestParameter>(tmp_parent_test->parameters(), tmp_param_name.toStdString().c_str());
            }

            if (tmp_test_param == NULL || is_empty_string(tmp_test_param->getValueForKey(EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE)))
            {
                bool    tmp_ok = !ask_user;
                QString    tmp_param_value;

                if (ask_user){
                    tmp_campaign_param_value = Parameter::paramValueForParamName<ExecutionCampaignParameter>(tmp_parent_test->executionCampaign()->parametersList(), tmp_param_name.toStdString());
                    tmp_param_value = QInputDialog::getText(this, tr("Paramètre d'exécution..."), tr("Veuillez saisir la valeur du paramètre ${%1} :").arg(tmp_param_name.toStdString().c_str()), QLineEdit::Normal, tmp_campaign_param_value, &tmp_ok);
                }

                if (tmp_ok)
                {
                    // Ajout du paramètre d'execution
                    if (tmp_test_param == NULL)
                    {
                        tmp_test_param = new ExecutionTestParameter();
                        tmp_test_param->setValueForKey(tmp_param_name.toStdString().c_str(), EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_NAME);
                        tmp_test_param->setValueForKey(tmp_param_value.toStdString().c_str(), EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE);
                        tmp_parent_test->addExecutionParameter(tmp_test_param);
                    }
                    // Mise à jour du paramètre d'execution existant
                    else
                    {
                        tmp_test_param->setValueForKey(tmp_param_value.toStdString().c_str(), EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE);
                    }
                }
            }
        }
    }

    m_parameters_model->setRecordsList(m_execution_test->parameters());
}



void FormExecutionTest::reinitExecutionsTestsParameters()
{
    ExecutionTestParameter                 *tmp_test_param = NULL;
    QList<ExecutionTestParameter*>        tmp_inherited_parameters = m_execution_test->inheritedParameters();

    foreach(QString tmp_param_name, m_current_parameters)
    {
        tmp_test_param = Parameter::parameterForParamName<ExecutionTestParameter>(tmp_inherited_parameters, tmp_param_name.toStdString().c_str());
        if (tmp_test_param != NULL)
            tmp_test_param->setValueForKey(NULL, EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE);
    }

    initLayout();
}


void FormExecutionTest::launchBatch()
{
    QString tmp_exec_file = RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(m_execution_test->inheritedParameters(), m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND)).trimmed();
    if (tmp_exec_file.isEmpty())
    {
        QMessageBox::critical(this, tr("Erreur"), tr("Veuillez saisir la commande à lancer dans la section Automatisation."));
    }
    else
    {
        if (!QFile::exists(tmp_exec_file))
        {
            QMessageBox::critical(this, tr("Erreur"), tr("L'executable '%1' n'existe pas.").arg(tmp_exec_file));
        }
        else
        {
            m_batch_automation_processor.stopProcess();

            QString tmp_parameters = RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(m_execution_test->inheritedParameters(),
                                                                                                  m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS));


            QList<QString> tmp_output_parameters;
            QString returnCodeParameterName = m_ui->batch_return_code_variable->text();
            QString returnOutputParameterName = m_ui->batch_stdout_variable->text();
            ExecutionTestParameter* returnCodeParameter = NULL;
            ExecutionTestParameter* returnOutputParameter = NULL;

            if (!returnCodeParameterName.isEmpty())
                tmp_output_parameters << returnCodeParameterName;

            if (!returnOutputParameterName.isEmpty())
                tmp_output_parameters << returnOutputParameterName;

            synchronizeExecutionsTestsParameters(tmp_output_parameters, false);

            returnCodeParameter = Parameter::parameterForParamName(m_execution_test->parameters(), returnCodeParameterName.toStdString().c_str());
            returnOutputParameter = Parameter::parameterForParamName(m_execution_test->parameters(), returnOutputParameterName.toStdString().c_str());

            m_batch_automation_processor = BatchAutomationProcessor(tmp_exec_file, tmp_parameters.split(' ', QString::SkipEmptyParts));
            connect(&m_batch_automation_processor, SIGNAL(processStop(int)), this, SLOT(stopBatchProcess(int)));
            connect(&m_batch_automation_processor, SIGNAL(processError(QString)), this, SLOT(showProcessError(QString)));
            connect(&m_batch_automation_processor, SIGNAL(processStandardOutput(QString)), this, SLOT(fillStandardOutput(QString)));
            connect(&m_batch_automation_processor, SIGNAL(processStandardError(QString)), this, SLOT(fillStandardError(QString)));
            m_batch_automation_processor.launchProcess(returnCodeParameter, returnOutputParameter);

            m_ui->batch_process_stdout->setPlainText("----------------------------------------------------------------------------------------------------------------------");
            m_ui->batch_process_stdout->appendPlainText(tr("Lancement de %1 %2 (PID=%3)")
                                                      .arg(tmp_exec_file)
                                                      .arg(tmp_parameters)
        #if defined(__WINDOWS) ||  defined(WIN32)
                                                      .arg(m_batch_automation_processor.getProcess().pid()->dwProcessId));
        #else
                                                      .arg(m_batch_automation_processor.getProcess().pid()));
        #endif
            m_ui->batch_process_stdout->appendPlainText("----------------------------------------------------------------------------------------------------------------------");

        }
    }
}


void FormExecutionTest::stopBatchProcess(int in_exit_code)
{
    m_ui->batch_process_stdout->appendPlainText("----------------------------------------------------------------------------------------------------------------------");
    m_ui->batch_process_stdout->appendPlainText(tr("Arrêt du processus (code retour=%1)").arg(in_exit_code));
    m_ui->batch_process_stdout->appendPlainText("----------------------------------------------------------------------------------------------------------------------");
    if (in_exit_code == 0)
        validateResult();
    else
        inValidateResult();
}

void FormExecutionTest::stopGuiProcess(int in_exit_code)
{
    if (in_exit_code == 0)
        validateResult();
    else
        inValidateResult();
}


void FormExecutionTest::showProcessError(const QString& errorString)
{
    inValidateResult();
    QMessageBox::critical(this, tr("Erreur"), errorString);
}


void FormExecutionTest::fillStandardOutput(const QString& output)
{
    m_ui->batch_process_stdout->appendPlainText(output);
}


void FormExecutionTest::fillStandardError(const QString& error)
{
    m_ui->batch_process_stdout->appendHtml(QString("<font color=\"red\">%1</font>").arg(error));
}


void FormExecutionTest::launchStartPlaybackSystemEvents()
{
    QString tmp_exec_file = RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(m_execution_test->inheritedParameters(), m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND)).trimmed();
    if (tmp_exec_file.isEmpty())
    {
        QMessageBox::critical(this, tr("Erreur"), tr("Veuillez sélectionner une application à lancer dans la section Automatisation."));
    }
    else
    {
        if (!QFile::exists(tmp_exec_file))
        {
            QMessageBox::critical(this, tr("Erreur"), tr("L'executable '%1' n'existe pas.").arg(tmp_exec_file));
        }
        else
        {
            m_gui_automation_processor.stopProcess();

            QString tmp_parameters = RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(m_execution_test->inheritedParameters(),
                                                                                                  m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS));

            QList<QString> tmp_output_parameters;
            QString returnCodeParameterName = m_ui->batch_return_code_variable->text();
            QString returnOutputParameterName = m_ui->batch_stdout_variable->text();
            ExecutionTestParameter* returnCodeParameter = NULL;
            ExecutionTestParameter* returnOutputParameter = NULL;

            if (!returnCodeParameterName.isEmpty())
                tmp_output_parameters << returnCodeParameterName;

            if (!returnOutputParameterName.isEmpty())
                tmp_output_parameters << returnOutputParameterName;

            synchronizeExecutionsTestsParameters(tmp_output_parameters, false);

            returnCodeParameter = Parameter::parameterForParamName(m_execution_test->parameters(), returnCodeParameterName.toStdString().c_str());
            returnOutputParameter = Parameter::parameterForParamName(m_execution_test->parameters(), returnOutputParameterName.toStdString().c_str());

            m_gui_automation_processor = GuiAutomationPlaybackProcessor(TestContent::AutomatedActionRelation::instance().getChilds(m_test_content), tmp_exec_file, tmp_parameters.split(' ', QString::SkipEmptyParts));
            connect(&m_gui_automation_processor, SIGNAL(processError(QString)), this, SLOT(showProcessError(QString)));
            connect(&m_gui_automation_processor, SIGNAL(processStop(int)), this, SLOT(stopGuiProcess(int)));

            m_gui_automation_processor.launchProcess(returnCodeParameter, returnOutputParameter);
        }
    }
}


void FormExecutionTest::startPlaybackSystemEvents()
{

}

void FormExecutionTest::stopPlaybackSystemEvents()
{

}

