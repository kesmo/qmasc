/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_MANAGE_USERS_H
#define FORM_MANAGE_USERS_H

#include <QList>
#include <QtWidgets/QDialog>

namespace Ui
{
class FormManageUsers;
}


class DBUser
{
private:
    bool    m_new_user;
    char    *m_username;
    char    *m_password;
    bool    m_admin_role;
    bool    m_writer_role;
    bool    m_reader_role;

public:
    DBUser(const char *in_username, bool in_admin_role, bool in_writer_role, bool in_reader_role)
    {
        m_new_user = false;
        m_username = _strdup(in_username);
        m_password = NULL;
        m_admin_role = in_admin_role;
        m_writer_role = in_writer_role;
        m_reader_role = in_reader_role;
    }


    DBUser(const char *in_username, const char *in_password)
    {
        m_new_user = true;
        m_username = _strdup(in_username);
        m_password = _strdup(in_password);
        m_admin_role = false;
        m_writer_role = false;
        m_reader_role = true;
    }

    ~DBUser() { free(m_username); free(m_password); }

    const char* username() { return m_username; }
    const char* password() { return m_password; }
    bool isAdmin() { return m_admin_role; }
    bool isWriter() { return m_writer_role; }
    bool isReader() { return m_reader_role; }
    bool isNewUser() { return m_new_user; }
};


class FormManageUsers : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(FormManageUsers)
public:
    explicit FormManageUsers(QWidget *parent = 0);
    virtual ~FormManageUsers();

    void loadUsers();

    public Q_SLOTS:
    void accept();
    void addUser();
    void changePassword();
    void manageUserGrants();
    void addUser(DBUser *in_user);
    void removeSelectedUsers();
    void userSelectionChanged();

protected:
    virtual void changeEvent(QEvent *e);

private:
    Ui::FormManageUsers    *m_ui;

    QList<DBUser*>            m_users_list;
    QList<DBUser*>            m_removed_users_list;

    void setUserAtIndex(DBUser *in_user, int in_index);
};

#endif // FORM_MANAGE_USERS_H
