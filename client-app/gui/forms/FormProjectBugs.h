/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_PROJECT_BUGS_H
#define FORM_PROJECT_BUGS_H

#include "entities/projectversion.h"
#include "entities/bug.h"
#include "gui/components/AbstractProjectWidget.h"

#include <bugtracker.h>

#include <QtWidgets/QWidget>
#include <QtWidgets/QTableWidgetItem>

namespace Ui {
    class FormProjectBugs;
}

class FormProjectBugs : public AbstractProjectWidget {
    Q_OBJECT
public:
    FormProjectBugs(ProjectVersion *in_project_version, QWidget *parent = 0);
    ~FormProjectBugs();

    QIcon icon() const;
    QString title() const;

public Q_SLOTS:
    void loadBugsList();
    void filterBugsList();

    void bugtrackerLogin();
    void launchUpdateBugsListFromBugtracker();
    void updateBugsListFromBugtracker(QMap< QString, QMap<Bugtracker::BugProperty, QString> >);
    void bugtrackerError(QString errorMsg);

    void deletedSelectedBugs(QList<Record*> in_bugs_list);
    void openBugAtIndex(QTableWidgetItem *in_item);

    void cancel();

Q_SIGNALS:
    void closed();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::FormProjectBugs *m_ui;

    ProjectVersion *m_project_version;
    QList<Bug*> m_bugs_list;
    Bugtracker  *m_bugtracker;
    bool                m_bugtracker_connection_active;

    bool            m_has_bugtracker_error;

    void setBugForRow(Bug *in_bug, int in_row);
    void clearBugsList();
    void initExternalBugtracker();

};

#endif // FORM_PROJECT_BUGS_H
