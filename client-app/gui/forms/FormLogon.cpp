/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "session.h"
#include "FormLogon.h"
#include "ui_FormLogon.h"

#include "client.h"
#include "constants.h"

#include <QMessageBox>
#include <QSettings>
#include <QIntValidator>


/**
  Constructeur
**/
FormLogon::FormLogon(QWidget *parent) : QDialog(parent)
{
    setAttribute(Qt::WA_DeleteOnClose);

    m_ui = new  Ui_FormLogon;

    m_ui->setupUi(this);

    readSettings();

    if (m_ui->username->text().isEmpty())
    {
        m_ui->username->setFocus();
        m_ui->username->selectAll();
    }
    else
        m_ui->password->setFocus();

    m_ui->port_number->setValidator(new QIntValidator(0, 65535, this));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

FormLogon::~FormLogon()
{
    delete m_ui;
}

/**
  Validation par le bouton OK
**/
void FormLogon::accept()
{
    int tmp_login_result = NOERR;
    int tmp_host_port = 0;

    QSettings tmp_settings;

    if (m_ui->port_number->text().isEmpty() == false)
        tmp_host_port = m_ui->port_number->text().toInt();

    tmp_login_result = Session::instance().connect(m_ui->hostname->text(), tmp_host_port,
        m_ui->username->text(), m_ui->password->text(),
        m_ui->debug_mode_check_box->isChecked(), m_ui->logfile_path->text(), m_ui->log_level->value());

    if (tmp_login_result == NOERR) {
        writeSettings();
        QDialog::accept();
    }
    else {
        QMessageBox::critical(this, tr("Erreur de connexion"), Session::instance().getErrorMessage(tmp_login_result));
    }
}


void FormLogon::readSettings()
{
    QSettings settings;
    QString tmp_username = settings.value("username", "").toString();
    QString tmp_host = settings.value("host", "localhost").toString();
    QString tmp_port = settings.value("port", QString::number(SERVER_PORT)).toString();
    QString tmp_logfile = settings.value("logfile", "").toString();
    int tmp_loglevel = settings.value("loglevel", 1).toInt();
    bool tmp_debug = settings.value("debug", false).toBool();


    m_ui->username->setText(tmp_username);
    m_ui->hostname->setText(tmp_host);
    m_ui->port_number->setText(tmp_port);
    m_ui->logfile_path->setText(tmp_logfile);
    m_ui->log_level->setValue(tmp_loglevel);
    m_ui->debug_mode_check_box->setChecked(tmp_debug);
}

void FormLogon::writeSettings()
{
    QSettings settings;

    settings.setValue("username", m_ui->username->text());
    settings.setValue("host", m_ui->hostname->text());
    settings.setValue("port", m_ui->port_number->text());
    settings.setValue("logfile", m_ui->logfile_path->text());
    settings.setValue("loglevel", m_ui->log_level->value());
    settings.setValue("debug", m_ui->debug_mode_check_box->isChecked());
}

