#include "FormFeature.h"
#include "ui_FormFeature.h"

#include "session.h"

#include <QMessageBox>
#include <QPushButton>

FormFeature::FormFeature(Feature *in_feature, QWidget *parent)
    : AbstractProjectWidget(in_feature, parent),
    m_ui(new Ui::FormFeature),
    m_feature_content(NULL)
{
    m_ui->setupUi(this);

    loadStyleSheet(this,
                   "#46b240",
                   "#5c9959",
                   "#3e663c",
                   "#6cab69",
                   "#7bba78",
                   "#4d874a",
                   "#dfe8df",
                   "#82e87d",
                   "#2ac422",
                   "#5c9959");

    m_ui->feature_description->addTextToolBar(RecordTextEditToolBar::Small);

    connect(m_ui->feature_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->feature_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));


    loadFeature(in_feature);
}

FormFeature::~FormFeature()
{
    delete m_ui;
}

QIcon FormFeature::icon() const
{
    return QPixmap(":/images/22x22/feature.png");
}

QString FormFeature::title() const
{
    if (m_feature)
        return m_feature->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME);

    return QString();
}


bool FormFeature::saveRecord()
{
    int                 tmp_compare_versions_result = 0;
    QMessageBox         *tmp_msg_box;
    QPushButton         *tmp_update_button;
    QPushButton         *tmp_conserv_button;
    QPushButton         *tmp_cancel_button;

    FeatureContent*     tmp_old_content = NULL;
    bool                tmp_upgrade = false;

    int                 tmp_save_result = NOERR;

    if (m_ui->feature_name->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Donnée obligatoire non saisi"), tr("Le nom de la fonctionnalité est nécessaire."));
        return false;
    }

    // Mettre a jour le contenu de la fonctionnalité
    tmp_compare_versions_result = compare_values(m_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION), m_feature->getValueForKey(FEATURES_HIERARCHY_VERSION));
    if (tmp_compare_versions_result < 0)
    {
        // La version du contenu de la fonctionnalité est anterieure a la version de la fonctionnalité
        tmp_msg_box = new QMessageBox(QMessageBox::Question, tr("Confirmation..."),
                                      tr("La version du contenu de la fonctionnalité (%1) est antérieure à  la version courante (%2).\nVoulez-vous mettre à niveau la version du contenu vers la version courante ?").arg(ProjectVersion::formatProjectVersionNumber(m_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION))).arg(ProjectVersion::formatProjectVersionNumber(m_feature->getValueForKey(FEATURES_HIERARCHY_VERSION))));
        tmp_update_button = tmp_msg_box->addButton(tr("Mettre à niveau"), QMessageBox::YesRole);
        tmp_conserv_button = tmp_msg_box->addButton(tr("Conserver la version"), QMessageBox::NoRole);
        tmp_cancel_button = tmp_msg_box->addButton(tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->exec();
        if (tmp_msg_box->clickedButton() == tmp_update_button)
        {
            delete tmp_msg_box;
            tmp_upgrade = true;
            tmp_old_content = m_feature_content;
            m_feature_content = tmp_old_content->copy();
            delete tmp_old_content;
        }
        else if(tmp_msg_box->clickedButton() == tmp_cancel_button)
        {
            delete tmp_msg_box;
            return false;
        }
    }

    m_feature_content->setValueForKey(m_ui->feature_name->text().toStdString().c_str(), FEATURES_CONTENTS_TABLE_SHORT_NAME);
    m_feature_content->setValueForKey(m_ui->feature_description->textEditor()->toOptimizedHtml().toStdString().c_str(), FEATURES_CONTENTS_TABLE_DESCRIPTION);
    tmp_save_result = m_feature_content->saveRecord();
    if (tmp_save_result == NOERR)
    {
        m_feature->setDataFromFeatureContent(m_feature_content);
        tmp_save_result = m_feature->saveRecord();
    }

    if (tmp_save_result != NOERR)
    {
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
        return false;
    }

    m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(m_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION)));

    setModified(false);

    return true;
}


void FormFeature::loadFeature(Feature *in_feature)
{
    m_ui->lock_widget->setVisible(false);

    if (in_feature != NULL)
    {
        m_feature = in_feature;

        setWindowTitle(title());

        m_ui->feature_id->setText(QString(m_feature->getIdentifier()) + "(" + QString(m_feature->getValueForKey(FEATURES_HIERARCHY_FEATURE_CONTENT_ID)) + ")");

        if (m_feature_content != NULL)
            delete m_feature_content;

        int tmp_load_content_result = NOERR;
        m_feature_content = FeatureContent::getEntity(m_feature->getValueForKey(FEATURES_HIERARCHY_FEATURE_CONTENT_ID), &tmp_load_content_result);
        if (m_feature_content && tmp_load_content_result == NOERR) {
            loadFeatureContent(m_feature_content);
        }
    }
    else
        cancel();

    m_ui->feature_name->setFocus();

}


void FormFeature::loadFeatureContent(FeatureContent *in_feature_content)
{
    setModifiable(false);

    if (m_feature_content == in_feature_content)
    {
        if (m_feature->isModifiable())
        {
            m_feature_content->lockRecord(true);

            if (m_feature_content->lockRecordStatus() == RECORD_STATUS_LOCKED)
            {
                m_ui->lock_widget->setVisible(true);
                net_get_field(NET_MESSAGE_TYPE_INDEX+1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
                m_ui->label_lock_by->setText(tr("Verrouillée par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            }
            else
                setModifiable(true);
        }
    }

    //! \todo
//    m_ui->previous_version_button->setVisible(false);
//    m_ui->next_version_button->setVisible(false);

    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

    if (in_feature_content != NULL)
    {

        //! \todo
        //        m_previous_feature_content = in_feature_content->previousFeatureContent();
        //        m_next_feature_content = in_feature_content->nextFeatureContent();
//        m_ui->previous_version_button->setVisible(m_previous_feature_content != NULL);
//        m_ui->next_version_button->setVisible(m_next_feature_content != NULL);

        setWindowTitle(title());
        m_ui->feature_name->setText(in_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME));
        m_ui->feature_name->setEnabled(isModifiable());
        m_ui->feature_description->textEditor()->setText(in_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_DESCRIPTION));
        m_ui->feature_description->textEditor()->setReadOnly(!isModifiable());
        if (isModifiable())
            m_ui->feature_description->toolBar()->show();
        else
            m_ui->feature_description->toolBar()->hide();

        m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(in_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION)));

    }

    setModified(false);
}
