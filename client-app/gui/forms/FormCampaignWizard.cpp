/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormCampaignWizard.h"
#include "ui_FormCampaignWizard.h"
#include "entities/project.h"
#include "entities/test.h"
#include "entities/requirementcontent.h"
#include "entities/requirementcategory.h"
#include "session.h"

FormCampaign_Wizard::FormCampaign_Wizard(ProjectVersion *in_project, QWidget *parent) :
    QWizard(parent),
    m_ui(new Ui::FormCampaign_Wizard)
{
    setAttribute(Qt::WA_DeleteOnClose);

    m_project = in_project;

    m_ui->setupUi(this);

    // Initialisation des pages
    m_ui->project_short_name_label->setText(in_project->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
    m_ui->project_version_label->setText(ProjectVersion::formatProjectVersionNumber(in_project->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)));

    foreach(RequirementCategory *tmp_requiremetn_category, Session::instance().requirementsCategories())
    {
        m_ui->test_category->addItem(TR_CUSTOM_MESSAGE(tmp_requiremetn_category->getValueForKey(REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL)), tmp_requiremetn_category->getValueForKey(REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID));
    }

    m_ui->comparison->addItem(tr("Egal à "), Test::EqualTo);
    m_ui->comparison->addItem(tr("Inférieur à "), Test::LowerThan);
    m_ui->comparison->addItem(tr("Supérieur à "), Test::UpperThan);
    m_ui->comparison->addItem(tr("Inférieur ou égal à "), Test::LowerOrEqualTo);
    m_ui->comparison->addItem(tr("Supérieur ou égal à "), Test::UpperOrEqualTo);

    connect(m_ui->campaign_short_name, SIGNAL(textChanged(QString)), this, SLOT(campaignShortNameChanged()));

    connect(m_ui->select_none_test_indic, SIGNAL(clicked()), this, SLOT(updateControls()));
    connect(m_ui->select_specifics_tests_indic, SIGNAL(clicked()), this, SLOT(updateControls()));
    connect(m_ui->select_all_tests_indic, SIGNAL(clicked()), this, SLOT(updateControls()));

    connect(m_ui->category_indic, SIGNAL(clicked()), this, SLOT(updateControls()));
    connect(m_ui->priority_indic, SIGNAL(clicked()), this, SLOT(updateControls()));
}

FormCampaign_Wizard::~FormCampaign_Wizard()
{
    delete m_ui;
}

void FormCampaign_Wizard::changeEvent(QEvent *e)
{
    QWizard::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FormCampaign_Wizard::campaignShortNameChanged()
{
    if (m_ui->campaign_short_name->text().isEmpty()) {
    }
}


void FormCampaign_Wizard::accept()
{
    Campaign                *tmp_new_campaign = NULL;
    int                     tmp_tests_index = 0;
    QList<Record*>          *tmp_tests_category_list = NULL;
    QList<Record*>          *tmp_tests_priority_list = NULL;
    Test            *tmp_test = NULL;
    QVariant                tmp_test_category;
    int                     tmp_save_result = NOERR;
    QList<Test*>    tmp_tests_list;
    QList<Test*>    tmp_final_tests_list;

    if (m_ui->campaign_short_name->text().isEmpty()) {
        return;
    }

    tmp_new_campaign = new Campaign();
    ProjectVersion::CampaignRelation::instance().addChild(m_project, tmp_new_campaign);

    tmp_new_campaign->setValueForKey(m_ui->campaign_short_name->text().toStdString().c_str(), CAMPAIGNS_TABLE_SHORT_NAME);
    tmp_new_campaign->setValueForKey(m_ui->campaign_description->toOptimizedHtml().toStdString().c_str(), CAMPAIGNS_TABLE_DESCRIPTION);
    tmp_save_result = ProjectVersion::CampaignRelation::instance().saveChilds(m_project);
    if (tmp_save_result == NOERR) {
        // Campagne vide
        if (m_ui->select_none_test_indic->isChecked()) {

        }
        // Campagne de tests spécifiques
        else if (m_ui->select_specifics_tests_indic->isChecked()) {
            tmp_test_category = m_ui->test_category->itemData(m_ui->test_category->currentIndex());

            // Recherche par catégories
            if (m_ui->category_indic->isChecked() && tmp_test_category.isValid()) {
                tmp_tests_category_list = new QList<Record*>();
                foreach(tmp_test, m_project->testsHierarchy())
                {
                    tmp_test->searchFieldWithValue(tmp_tests_category_list, TESTS_HIERARCHY_CATEGORY_ID, tmp_test_category.toString().toStdString().c_str(), true);
                }

                // Recherche par catégories et par niveau de priorité
                if (m_ui->priority_indic->isChecked()) {
                    tmp_tests_priority_list = new QList<Record*>();
                    for (tmp_tests_index = 0; tmp_tests_index < tmp_tests_category_list->count(); tmp_tests_index++) {
                        (dynamic_cast<Test*>(tmp_tests_category_list->at(tmp_tests_index)))->searchFieldWithValue(tmp_tests_priority_list, TESTS_HIERARCHY_PRIORITY_LEVEL, QString::number(m_ui->test_priority_level->value()).toStdString().c_str(), false, m_ui->comparison->itemData(m_ui->comparison->currentIndex()).toInt());
                    }

                    for (tmp_tests_index = 0; tmp_tests_index < tmp_tests_priority_list->count(); tmp_tests_index++) {
                        if (is_empty_string(tmp_tests_priority_list->at(tmp_tests_index)->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID)))
                            tmp_tests_list.append(dynamic_cast<Test*>(tmp_tests_priority_list->at(tmp_tests_index)));
                    }
                }
                // Recherche par catégories seulement
                else {
                    for (tmp_tests_index = 0; tmp_tests_index < tmp_tests_category_list->count(); tmp_tests_index++) {
                        if (is_empty_string(tmp_tests_category_list->at(tmp_tests_index)->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID)))
                            tmp_tests_list.append(dynamic_cast<Test*>(tmp_tests_category_list->at(tmp_tests_index)));
                    }
                }
            }
            // Recherche par niveau de priorité
            else if (m_ui->priority_indic->isChecked()) {
                tmp_tests_priority_list = new QList<Record*>();
                foreach(tmp_test, m_project->testsHierarchy())
                {
                    tmp_test->searchFieldWithValue(tmp_tests_priority_list, TESTS_HIERARCHY_PRIORITY_LEVEL, QString::number(m_ui->test_priority_level->value()).toStdString().c_str(), true, m_ui->comparison->itemData(m_ui->comparison->currentIndex()).toInt());
                }

                for (tmp_tests_index = 0; tmp_tests_index < tmp_tests_priority_list->count(); tmp_tests_index++) {
                    if (is_empty_string(tmp_tests_priority_list->at(tmp_tests_index)->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID)))
                        tmp_tests_list.append(dynamic_cast<Test*>(tmp_tests_priority_list->at(tmp_tests_index)));
                }
            }

            tmp_tests_index = 0;
            tmp_final_tests_list = Test::parentRecordsFromRecordsList(tmp_tests_list);
            foreach(Test* tmp_tests_list, tmp_final_tests_list)
            {
                tmp_new_campaign->insertTestAtIndex(tmp_tests_list, tmp_tests_index);
                tmp_tests_index++;
            }

        }
        // Campagne complète (copier tous les tests du projet)
        else if (m_ui->select_all_tests_indic->isChecked()) {
            foreach(Test *tmp_test, m_project->testsHierarchy())
            {
                tmp_new_campaign->insertTestAtIndex(tmp_test, tmp_tests_index);
                tmp_tests_index++;
            }
        }
        else {
            return;
        }
    }

    QWizard::accept();
    emit campaignCreated(tmp_new_campaign);
}


void FormCampaign_Wizard::updateControls()
{
    bool    tmp_specifics_tests_indic = (m_ui->select_specifics_tests_indic->isChecked());

    m_ui->category_indic->setEnabled(tmp_specifics_tests_indic);
    m_ui->test_category->setEnabled(m_ui->category_indic->isChecked());

    m_ui->priority_indic->setEnabled(tmp_specifics_tests_indic);
    m_ui->comparison->setEnabled(m_ui->priority_indic->isChecked());
    m_ui->test_priority_level->setEnabled(m_ui->priority_indic->isChecked());
}
