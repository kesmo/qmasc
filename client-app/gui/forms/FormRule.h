#ifndef FORM_RULE_H
#define FORM_RULE_H

#include "gui/components/AbstractProjectWidget.h"
#include "entities/rule.h"

#include <QtWidgets/QWidget>

namespace Ui {
class FormRule;
}

class FormRule :
        public AbstractProjectWidget
{
    Q_OBJECT
    
public:
    explicit FormRule(Rule* in_rule, QWidget *parent = 0);
    ~FormRule();

    QIcon icon() const;
    QString title() const;

public slots:
    bool saveRecord();

protected:
    void loadRule(Rule* in_rule);
    void loadRuleContent(RuleContent *in_rule_content);

private:
    Ui::FormRule *m_ui;

    Rule            *m_rule;
    RuleContent     *m_rule_content;
};

#endif // FORM_RULE_H
