/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_EXECUTIONS_REPORTS_H
#define FORM_EXECUTIONS_REPORTS_H

#include "imaging/Chart.h"
#include "record.h"
#include "entities/project.h"
#include "entities/projectversion.h"
#include "ui_FormExecutionsReports.h"
#include "gui/components/AbstractProjectWidget.h"

namespace Ui
{
class FormExecutionsReports;
}

class FormExecutionsReports : public AbstractProjectWidget
{
    Q_OBJECT

public:
    FormExecutionsReports(Project *in_project, QWidget *parent = 0);
    ~FormExecutionsReports();

    QIcon icon() const;
    QString title() const;

public slots :
    void draw();
    void updateGraph();
    void save();
    void setTitleColor();
    void cancel();

protected:
    void init();
    TrinomeArray loadDatas();

private:
    Ui::FormExecutionsReports            *m_ui;

    Project                 *m_project;
    Chart                    *m_chart;
    QList<Trinome*>         m_chart_data;

};

#endif // FORM_EXECUTIONS_REPORTS_H
