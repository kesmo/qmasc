/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_NEW_USER_H
#define FORM_NEW_USER_H

#include <QtWidgets/QDialog>
#include "ui_FormNewUser.h"
#include "FormManageUsers.h"

class FormNewUser : public QDialog
{
    Q_OBJECT

public:
    FormNewUser(QWidget *parent = 0);
    ~FormNewUser();

public Q_SLOTS:
    void accept();

signals:
    void userCreated(DBUser *in_user);

private:
    Ui::FormNewUser *m_ui;
};

#endif // FORM_NEW_USER_H
