/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_EXECUTION_CAMPAIGN_H
#define FORM_EXECUTION_CAMPAIGN_H

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "entities/campaign.h"
#include "entities/executiontest.h"
#include "entities/executioncampaign.h"
#include "entities/executioncampaignparameter.h"
#include "entities/executiontestparameter.h"

#include "gui/components/hierarchies/executioncampaignhierarchy.h"
#include "gui/components/TestActionAttachmentsManager.h"

#include "FormExecutionTest.h"

#include "objects/Trinome.h"

#include <QDialog>
#include <QTableWidgetItem>
#include <QFileInfo>

namespace Ui
{
class FormExecutionCampaign;
}

class FormExecutionCampaign :
    public QDialog,
    public TestActionAttachmentsManager
{
    Q_OBJECT
        Q_DISABLE_COPY(FormExecutionCampaign)
public:
    explicit FormExecutionCampaign(ExecutionCampaign *in_execution_campaign, QWidget *parent = 0);
    virtual ~FormExecutionCampaign();

    void showTestInfosForTestWithId(const char *in_execution_test_id);

    public slots:
    void invalidateAction();
    void addBug();
    void showTestInfos();
    void showNextActionInfos();
    void showPreviousActionInfos();
    bool saveExecution();
    void cancelExecution();
    void print();
    void saveAs();
    void parameterItemChanged(QTableWidgetItem *in_widget_item);
    void reloadFromCampaignDatas();
    void insertParameter();
    void removeSelectedParameter();
    void revisionChanged(QString);

protected:
    void showTestInfosAtIndex(QModelIndex in_model_index);
    void showNextActionInfosForTestAtIndex(QModelIndex in_model_index);
    void showPreviousActionInfosForTestAtIndex(QModelIndex in_model_index);
    QTextDocument* generateDocument(bool include_image = true, QFileInfo file_info = QFileInfo());
    virtual void closeEvent(QCloseEvent *in_event);
    bool maybeClose();

private:
    Ui::FormExecutionCampaign *m_ui;

    ExecutionCampaign        *m_execution_campaign;

    RecordsTreeView           *m_tests_tree_view;
    RecordsTreeModel          *m_tests_tree_model;

    FormExecutionTest         *m_execution_test_view;

    QTableWidget            *m_parameters_table_widget;
    QAction                    *insertParameterAction;
    QAction                    *removeParameterAction;

    int                            m_current_action_index;
    bool                        m_index_changed_programmatically;

    QList<Trinome*>             m_trinomes_array;

    QLineEdit        *m_revision_edit;

    void setParameterAtIndex(ExecutionCampaignParameter *in_parameter, int in_index);

    QString executionTestToHtml(QTextDocument *in_doc, ExecutionTest *in_execution_test, QString suffix, int level = 0, QString images_folder = QString());
    ExecutionTest* executionTestForIndex(QModelIndex index) const;
};

#endif // FORM_EXECUTION_CAMPAIGN_H
