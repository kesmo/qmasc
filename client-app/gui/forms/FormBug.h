/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_BUG_H
#define FORM_BUG_H

#include <QMap>
#include "ui_FormBug.h"
#include "gui/components/hierarchies/executiontesthierarchy.h"
#include "entities/executionaction.h"
#include "entities/projectversion.h"
#include "entities/projectgrant.h"
#include "entities/bug.h"

#include "bugtracker.h"
#include "gui/components/AbstractProjectWidget.h"

QT_FORWARD_DECLARE_CLASS(Ui_FormBug)

class FormBug : public AbstractProjectWidget
{
    Q_OBJECT

public:
    FormBug(ProjectVersion *in_project_version, ExecutionTest *in_execution_test, ExecutionAction *in_execution_action, Bug *in_bug = NULL, QWidget *parent = 0, Qt::WindowFlags in_windows_flags = Qt::Dialog);
    ~FormBug();

    QIcon icon() const;
    QString title() const;

    void loadBugtrackerProjectsInformations(QList<QString> in_projects_ids);
    void loadBugtrackerPriorities();
    void loadBugtrackerSeverities();
    void loadBugtrackerReproducibilities();
    void loadBugtrackerProjectComponents();
    void loadBugtrackerProjectVersions();
    void loadBugtrackerPlatforms();
    void loadBugtrackerOsTypes();
    void loadBugtrackerAvailableProjectsIds();
    void addBugtrackerIssue();
    void logoutBugtracker();

    bool generateBugDescriptionUntilAction(ExecutionTest *in_root_test, QString & description, QString in_prefix, ExecutionTest *in_test, ExecutionAction *in_action);

    public Q_SLOTS:
    void save();
    void loginBugtracker();

    void bugtrackerLogin();
    void bugtrackerLogout();

    void bugtrackerPriorities(QMap<QString, QString>);
    void bugtrackerSeverities(QMap<QString, QString>);
    void bugtrackerReproducibilities(QMap<QString, QString>);

    void bugtrackerPlatforms(QList<QString>);
    void bugtrackerOperatingSystems(QList<QString>);
    void bugtrackerAvailableProjectsIds(QList<QString>);

    void bugtrackerProjectsInformations(QList< QPair<QString, QString> >);
    void bugtrackerProjectComponents(QList<QString>);
    void bugtrackerProjectVersions(QList<QString>);

    void bugtrackerBugCreated(QString);

    void bugtrackerError(QString);

    void updateBugFromBugtracker(QMap< QString, QMap<Bugtracker::BugProperty, QString> > in_bugs_list);

    void openExecutionCampaign();

private:
    Ui_FormBug             *m_ui;

    ProjectVersion       *m_project_version;
    ExecutionTest        *m_execution_test;
    ExecutionAction      *m_execution_action;

    Bug             *m_bug;

    bool         m_new_bug;

    Bugtracker         *m_bugtracker;

    bool        m_bugtracker_connection_active;

    QString        m_project_id;
    QString        m_project_name;

    void updateControls();
    void saveBug(QString in_bugtracker_bug_id);
};

#endif // FORM_BUG_H
