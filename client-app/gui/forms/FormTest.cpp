/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormTest.h"
#include "ui_FormTest.h"
#include "entities/action.h"
#include "utilities.h"
#include "entities/project.h"
#include "entities/testrequirement.h"
#include "entities/testcontent.h"
#include "entities/testcontentfile.h"
#include "entities/requirement.h"
#include "entities/automatedactionvalidation.h"
#include "entities/automatedactionwindowhierarchy.h"
#include "entities/projectparameter.h"
#include "entities/requirementcategory.h"
#include "entities/requirementcontent.h"
#include "entities/testtype.h"
#include "session.h"
#include "FormBug.h"
#include "gui/components/widgets/horizontaltabstyle.h"

#include <QMenu>
#include <QDomDocument>
#include <QDomNamedNodeMap>
#include <QDomNode>
#include <QDomNodeList>
#include <QProcess>
#include <QDesktopServices>

#include <QMdiSubWindow>
#include <QMessageBox>
#include <QClipboard>
#include <QFileDialog>
#include <QToolTip>

/**
  Constructeur
**/
FormTest::FormTest(Test *in_test, QWidget *parent) :
    AbstractProjectWidget(in_test, parent),
    CustomFieldsControlsManager(),
    m_ui(new Ui::FormTest)
#ifdef GUI_AUTOMATION_ACTIVATED
    , m_automation_playback_messages(NULL)
#endif
{
    m_ui->setupUi(this);
    m_ui->tabs->setTabPosition(QTabWidget::West);
    m_ui->tabs->tabBar()->setStyle(new Gui::Components::Widgets::HorizontalTabStyle());
    loadStyleSheet(this);

    m_ui->test_description->addTextToolBar(RecordTextEditToolBar::Small);

    foreach(RequirementCategory *tmp_requiremetn_category, Session::instance().requirementsCategories())
    {
        m_ui->test_category->addItem(TR_CUSTOM_MESSAGE(tmp_requiremetn_category->getValueForKey(REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL)), tmp_requiremetn_category->getValueForKey(REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID));
    }

    foreach(TestType *tmp_test_type, Session::instance().testsTypes())
    {
        m_ui->test_type->addItem(TR_CUSTOM_MESSAGE(tmp_test_type->getValueForKey(TESTS_TYPES_TABLE_TEST_TYPE_LABEL)), tmp_test_type->getValueForKey(TESTS_TYPES_TABLE_TEST_TYPE_ID));
    }

    QList< QPair<QString, QString> >  tmp_automated_actions_table_headers;
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Type de message"), AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE);
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Données"), AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Délai"), AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY);
    tmp_automated_actions_table_headers << qMakePair<QString, QString>(tr("Vérifications"), QString::null);

    m_previous_test_content = NULL;
    m_next_test_content = NULL;
    m_test_content = NULL;
    m_bugtracker = NULL;

    m_automated_actions_model = new RecordsTableModel<AutomatedAction>(tmp_automated_actions_table_headers, QList<AutomatedAction*>(), this);
    m_automated_actions_model->setColumnDataFunctionForColumn(0, &FunctionAutomatedActionMessageType);
    m_automated_actions_model->setColumnDataFunctionForColumn(1, &FunctionAutomatedActionMessageData);
    m_automated_actions_model->setColumnDataFunctionForColumn(2, &FunctionAutomatedActionMessageDelay);
    m_automated_actions_model->setColumnDataFunctionForColumn(3, &FunctionAutomatedActionMessageCallback);
    m_ui->automated_actions_list->setModel(m_automated_actions_model);
    m_ui->automated_actions_list->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_ui->automated_actions_list, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showAutomatedActionValidationContextMenu(QPoint)));
    m_ui->automated_actions_list->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_ui->automated_actions_list->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    connect(m_ui->test_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->test_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));
    connect(m_ui->test_category, SIGNAL(currentIndexChanged(int)), this, SLOT(setModified()));
    connect(m_ui->test_type, SIGNAL(currentIndexChanged(int)), this, SLOT(setModified()));
    connect(m_ui->test_limit, SIGNAL(stateChanged(int)), this, SLOT(setModified()));
    connect(m_ui->test_priority_level, SIGNAL(valueChanged(int)), this, SLOT(setModified()));
    connect(m_ui->automation_command_line, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->automation_command_parameters, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));

    connect(m_ui->button_show_original_test, SIGNAL(clicked(bool)), this, SLOT(showOriginalTestInfosClicked()));

    connect(m_ui->button_add, SIGNAL(clicked()), this, SLOT(addAction()));
    connect(m_ui->button_del, SIGNAL(clicked()), this, SLOT(deleteSelectedAction()));
    connect(m_ui->button_up, SIGNAL(clicked()), this, SLOT(moveSelectedActionUp()));
    connect(m_ui->button_down, SIGNAL(clicked()), this, SLOT(moveSelectedActionDown()));

    connect(m_ui->button_copy, SIGNAL(clicked()), this, SLOT(copySelectedActionsToClipboard()));
    connect(m_ui->button_cut, SIGNAL(clicked()), this, SLOT(cutSelectedActionsToClipboard()));
    connect(m_ui->button_paste, SIGNAL(clicked()), this, SLOT(pasteClipboardDataToActionsList()));
    connect(m_ui->button_paste_plain_text, SIGNAL(clicked()), this, SLOT(pasteClipboardPlainTextToActionsList()));

    connect(QApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(clipboardDataChanged()));

    connect(m_ui->button_select_command_line, SIGNAL(clicked()), this, SLOT(selectExternalCommandForAutomation()));

#ifdef GUI_AUTOMATION_ACTIVATED
    m_ui->button_start_record->setVisible(true);
    m_ui->button_stop_record->setVisible(true);
    m_ui->button_start_playback->setVisible(true);
    m_ui->button_stop_playback->setVisible(true);
    m_ui->radioButtonGui->setEnabled(true);
#else
    m_ui->button_start_record->setVisible(false);
    m_ui->button_stop_record->setVisible(false);
    m_ui->button_start_playback->setVisible(false);
    m_ui->button_stop_playback->setVisible(false);
    m_ui->radioButtonGui->setEnabled(false);
#endif

    connect(m_ui->button_start_record, SIGNAL(clicked()), this, SLOT(launchStartRecordSystemEvents()));
    connect(m_ui->button_stop_record, SIGNAL(clicked()), &m_record_processor, SLOT(stopProcess()));
    connect(m_ui->button_start_playback, SIGNAL(clicked()), this, SLOT(launchStartPlaybackSystemEvents()));
    connect(m_ui->button_stop_playback, SIGNAL(clicked()), &m_playback_processor, SLOT(stopProcess()));

    connect(m_ui->button_launch_batch_command, SIGNAL(clicked()), this, SLOT(launchBatch()));
    connect(m_ui->radioButtonGui, SIGNAL(clicked()), this, SLOT(changeAutomatedTestType()));
    connect(m_ui->radioButtonBatch, SIGNAL(clicked()), this, SLOT(changeAutomatedTestType()));

    m_ui->actions_list->setRemoveSelectedRowsOnKeypressEvent(false);
    connect(m_ui->actions_list, SIGNAL(itemSelectionChanged()), this, SLOT(actionSelectionChanged()));
    connect(m_ui->actions_list, SIGNAL(recordslistDrop(QList<Record*>, int)), this, SLOT(actionsDrop(QList<Record*>, int)));
    connect(m_ui->actions_list, SIGNAL(lastRowAndColumnReach()), this, SLOT(addActionAfterLastAction()));
    connect(m_ui->actions_list, SIGNAL(linesCopied()), this, SLOT(copySelectedActionsToClipboard()));
    connect(m_ui->actions_list, SIGNAL(linesCut()), this, SLOT(cutSelectedActionsToClipboard()));
    connect(m_ui->actions_list, SIGNAL(linesPaste()), this, SLOT(pasteClipboardDataToActionsList()));
    connect(m_ui->actions_list, SIGNAL(linesPastePlainText()), this, SLOT(pasteClipboardPlainTextToActionsList()));

    connect(m_ui->requirements_list, SIGNAL(recordslistDrop(QList<Record*>, int)), this, SLOT(requirementsDrop(QList<Record*>, int)));
    connect(m_ui->requirements_list, SIGNAL(rowRemoved(int)), this, SLOT(deletedRequirementAtIndex(int)));
    connect(m_ui->requirements_list, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(showRequirementAtIndex(QModelIndex)));

    //! \todo
//    connect(m_ui->previous_version_button, SIGNAL(clicked()), this, SLOT(loadPreviousTestContent()));
//    connect(m_ui->next_version_button, SIGNAL(clicked()), this, SLOT(loadNextTestContent()));

    m_ui->files_list->setMimeType("text/uri-list");
    connect(m_ui->files_list, SIGNAL(urlsListDrop(QList<QString>, int)), this, SLOT(filesDrop(QList<QString>, int)));
    connect(m_ui->files_list, SIGNAL(rowRemoved(int)), this, SLOT(deletedFileAtIndex(int)));
    connect(m_ui->files_list, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(openFileAtIndex(QTableWidgetItem*)));

    connect(m_ui->bugs_list, SIGNAL(rowRemoved(int)), this, SLOT(deletedBugAtIndex(int)));
    connect(m_ui->bugs_list, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(openBugAtIndex(QTableWidgetItem*)));

    connect(m_ui->tabs, SIGNAL(currentChanged(int)), this, SLOT(changeTab(int)));

    m_ui->test_id->setVisible(Session::instance().getClientSession()->m_debug);

    m_ui->actions_list->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

    loadPluginsViews();
    loadCustomFieldsView(m_ui->tabs, Session::instance().customTestsFieldsDesc());

    loadTest(in_test);
}


/**
  Destructeur
**/
FormTest::~FormTest()
{
    destroyPluginsViews();

    delete m_ui;
    delete m_previous_test_content;
    delete m_next_test_content;
    delete m_test_content;
    delete m_automated_actions_model;


    if (m_bugtracker != NULL) {
        ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_test->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
        if (tmp_bugtracker_module != NULL) {
            static_cast<BugtrackerModule*>(tmp_bugtracker_module)->destroyBugtracker(m_bugtracker);
        }
    }

    qDeleteAll(m_bugs);
    qDeleteAll(m_removed_bugs);

    qDeleteAll(m_custom_tests_fields);
}

QIcon FormTest::icon() const
{
    return QPixmap(":/images/22x22/scenario.png");
}

QString FormTest::title() const
{
    if (m_test)
        return m_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME);

    return QString();
}


void FormTest::loadNextTestContent()
{
    TestContent *tmp_test_content = m_next_test_content;

    if (maybeClose()) {
        if (m_test_content != NULL
            && m_next_test_content != NULL
            && compare_values(m_test_content->getIdentifier(), m_next_test_content->getIdentifier()) == 0)
            tmp_test_content = m_test_content;

        loadTestContent(tmp_test_content);
    }
}


/**
  Charger un test
**/
void FormTest::loadTest(Test *in_test)
{
    QStringList             tmp_actions_headers;
    QStringList             tmp_requirements_headers;
    QStringList             tmp_bugs_headers;
    int                     tmp_load_content_result = NOERR;

    m_ui->test_name->setFocus();

    m_ui->test_description->textEditor()->setCompletionFromList(in_test->projectVersion()->project()->parametersNames());

    tmp_actions_headers << tr("Description") << tr("Résultat attendu");
    m_ui->actions_list->setHorizontalHeaderLabels(tmp_actions_headers);
    m_ui->actions_list->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    m_ui->automated_actions_list->horizontalHeader()->setStretchLastSection(true);

    tmp_requirements_headers << tr("Description") << tr("Catégorie") << tr("Version");
    m_ui->requirements_list->setHorizontalHeaderLabels(tmp_requirements_headers);
    m_ui->requirements_list->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    tmp_bugs_headers << tr("Date") << tr("Résumé") << tr("Priorité") << tr("Sévérité") << tr("Id. externe");
    m_ui->bugs_list->setHorizontalHeaderLabels(tmp_bugs_headers);
    m_ui->bugs_list->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    m_ui->lock_widget->setVisible(false);

    m_test = in_test;
    if (m_test != NULL) {
        setWindowTitle(title());

        if (m_bugtracker == NULL) {
            if (is_empty_string(m_test->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST)) == FALSE) {
                ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_test->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
                if (tmp_bugtracker_module != NULL) {
                    m_bugtracker = static_cast<BugtrackerModule*>(tmp_bugtracker_module)->createBugtracker();
                }
            }
        }

        if (m_bugtracker != NULL)
            m_bugtracker->setBaseUrl(QUrl(m_test->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST)));

        m_ui->test_id->setText(QString(m_test->getIdentifier()) + "(" + QString(m_test->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID)) + ")");

        if (m_test_content != NULL)
            delete m_test_content;

        if (m_test->original() != NULL)
            m_test_content = TestContent::getEntity(m_test->original()->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), &tmp_load_content_result);
        else
            m_test_content = TestContent::getEntity(m_test->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), &tmp_load_content_result);

        if (m_test_content && tmp_load_content_result == NOERR) {
            loadTestContent(m_test_content);
            actionSelectionChanged();
        }
        else {
            QMessageBox::critical(this, tr("Erreur lors de chargement de l'enregistrement"), Session::instance().getErrorMessage(tmp_load_content_result));
            cancel();
        }
    }

}


void FormTest::loadTestContent(TestContent *in_test_content)
{
    Action                *tmp_action = NULL, *tmp_associated_action = NULL;
    int                     tmp_actions_index = 0;
    QList<Action*> tmp_actions_list;

    TestRequirement      *tmp_test_requirement = NULL;
    int                     tmp_requirements_index = 0;

    TestContentFile      *tmp_file = NULL;
    int                     tmp_files_index = 0;

    Bug           *tmp_bug = NULL;
    int                     tmp_bugs_index = 0;

    setModifiable(false);

    if (m_test->original() == NULL && in_test_content == m_test_content) {
        if (m_test->isModifiable()) {
            m_test_content->lockRecord(true);
            if (m_test_content->lockRecordStatus() == RECORD_STATUS_LOCKED) {
                m_ui->lock_widget->setVisible(true);
                net_get_field(NET_MESSAGE_TYPE_INDEX + 1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
                m_ui->label_lock_by->setText(tr("Verrouillé par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            }
            else
                setModifiable(true);
        }
    }

    //! \todo
//    m_ui->previous_version_button->setVisible(false);
//    m_ui->next_version_button->setVisible(false);

    for (tmp_actions_index = 0; tmp_actions_index < m_ui->actions_list->rowCount(); tmp_actions_index++) {
        removeActionWidgetsAtIndex(tmp_actions_index);
    }
    m_ui->actions_list->setRowCount(0);
    m_ui->requirements_list->setRowCount(0);
    m_ui->files_list->setRowCount(0);
    m_ui->bugs_list->setRowCount(0);

    qDeleteAll(m_bugs);
    m_bugs.clear();
    qDeleteAll(m_removed_bugs);
    m_removed_bugs.clear();

    qDeleteAll(m_custom_tests_fields);
    m_custom_tests_fields.clear();

    destroyAttachments();

    if (in_test_content != NULL) {
        m_previous_test_content = in_test_content->previousTestContent();
        m_next_test_content = in_test_content->nextTestContent();

        //! \todo
//        m_ui->previous_version_button->setVisible(m_previous_test_content != NULL);
//        m_ui->next_version_button->setVisible(m_next_test_content != NULL);

        // Charger les pièces jointes (avant les actions)
        m_files = TestContent::TestContentFileRelation::instance().getChilds(in_test_content);
        if (m_files.count() > 0) {
            m_ui->files_list->setRowCount(m_files.count());
            for (tmp_files_index = 0; tmp_files_index < m_files.count(); tmp_files_index++) {
                tmp_file = m_files.at(tmp_files_index);
                setFileForRow(tmp_file, tmp_files_index);
            }
        }

        // Charger les actions associees
        if (m_test_content->isAutomatedGuiTest()) {
            tmp_actions_index = 0;
            m_automated_actions_model->setRecordsList(TestContent::AutomatedActionRelation::instance().getChilds(in_test_content));
            m_ui->automated_actions_list->resizeColumnsToContents();
            m_ui->radioButtonGui->setChecked(true);
            m_ui->radioButtonBatch->setChecked(false);
        }
        else if (m_test_content->isAutomatedBatchTest()) {
            m_ui->radioButtonGui->setChecked(false);
            m_ui->radioButtonBatch->setChecked(true);
        }
        else {
            tmp_actions_list = TestContent::ActionRelation::instance().getChilds(in_test_content);
            if (tmp_actions_list.count() > 0) {
                tmp_actions_index = 0;
                foreach(tmp_action, tmp_actions_list) {
                    m_actions.append(tmp_action);
                    m_ui->actions_list->insertRow(tmp_actions_index);
                    setActionAtIndex(tmp_action, tmp_actions_index);
                    tmp_actions_index++;

                    tmp_action->loadAssociatedActionsForVersion(m_test->getValueForKey(TESTS_HIERARCHY_VERSION));
                    foreach(tmp_associated_action, tmp_action->associatedTestActions())
                    {
                        m_actions.append(tmp_associated_action);
                        m_ui->actions_list->insertRow(tmp_actions_index);
                        setActionAtIndex(tmp_associated_action, tmp_actions_index);
                        tmp_actions_index++;
                    }
                }
            }
        }


        // Charger les exigences associees
        if (in_test_content->getAllTestRequirements().count() > 0) {
            m_ui->requirements_list->setRowCount(in_test_content->getAllTestRequirements().count());
            for (tmp_requirements_index = 0; tmp_requirements_index < in_test_content->getAllTestRequirements().count(); tmp_requirements_index++) {
                tmp_test_requirement = in_test_content->getTestRequirementAtIndex(tmp_requirements_index);
                setRequirementForRow(tmp_test_requirement, tmp_requirements_index);
            }
            m_ui->requirements_list->resizeColumnsToContents();
            m_ui->requirements_list->resizeRowsToContents();
        }

        // Charger les anomalies
        m_bugs = m_test->loadBugs();
        if (m_bugs.count() > 0) {
            m_ui->bugs_list->setRowCount(m_bugs.count());
            for (tmp_bugs_index = 0; tmp_bugs_index < m_bugs.count(); tmp_bugs_index++) {
                tmp_bug = m_bugs.at(tmp_bugs_index);
                setBugForRow(tmp_bug, tmp_bugs_index);
            }
            m_ui->bugs_list->resizeColumnsToContents();
            m_ui->bugs_list->resizeRowsToContents();
        }

        // Charger les champs personnalises
        m_custom_tests_fields = TestContent::CustomTestFieldRelation::instance().getChilds(m_test_content);
        QList<CustomFieldDesc*> customFieldsDesc = Session::instance().customTestsFieldsDesc();

        foreach(CustomFieldDesc* customFieldDesc, customFieldsDesc) {
            bool foundTestField = false;
            foreach(CustomTestField* customTestField, m_custom_tests_fields) {
                if (compare_values(customTestField->getValueForKey(CUSTOM_TEST_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID),
                    customFieldDesc->getIdentifier()) == 0) {
                    customTestField->setFieldDesc(customFieldDesc);
                    popCustomFieldValue(customFieldDesc, customTestField, CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE, isModifiable());
                    foundTestField = true;
                    break;
                }
            }

            if (!foundTestField) {
                CustomTestField* customTestField = new CustomTestField();
                customTestField->setFieldDesc(customFieldDesc);
                TestContent::CustomTestFieldRelation::instance().setParent(m_test_content, customTestField);
                customTestField->setValueForKey(customFieldDesc->getValueForKey(CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_DEFAULT_VALUE), CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE);
                popCustomFieldValue(customFieldDesc, customTestField, CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE, isModifiable());
                m_custom_tests_fields.append(customTestField);
            }
        }

        m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_VERSION)));

        if (is_empty_string(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID)) == FALSE) {
            m_ui->test_category->setCurrentIndex(m_ui->test_category->findData(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID)));
        }
        else
            m_ui->test_category->setCurrentIndex(0);

        if (is_empty_string(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_TYPE)) == FALSE) {
            m_ui->test_type->setCurrentIndex(m_ui->test_type->findData(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_TYPE)));
        }
        else
            m_ui->test_type->setCurrentIndex(0);

        m_ui->test_limit->setChecked(compare_values(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE), YES) == 0);


    }

    setWindowTitle(tr("Scénario : %1").arg(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME)));
    m_ui->test_name->setText(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME));
    m_ui->test_description->textEditor()->setHtml(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));
    m_ui->test_priority_level->setValue(QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL)).toInt());
    m_ui->automation_command_line->setText(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND));
    m_ui->automation_command_parameters->setText(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS));
    m_ui->batch_return_code_variable->setText(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE));
    m_ui->batch_stdout_variable->setText(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE));
    setModified(false);

    updateControls(m_test->original() != NULL);
}


/**
  Modifier la saisie sur les controles si le test est une copie
**/
void FormTest::updateControls(bool in_original_test_indic)
{
    m_ui->button_show_original_test->setVisible(in_original_test_indic);

    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

    m_ui->test_priority_level->setDisabled(!isModifiable());
    m_ui->requirements_list->setDisabled(!isModifiable());
    m_ui->files_list->setDisabled(!isModifiable());
    m_ui->bugs_list->setDisabled(!isModifiable());
    m_ui->button_add->setDisabled(!isModifiable());
    m_ui->button_del->setDisabled(!isModifiable());
    m_ui->button_up->setDisabled(!isModifiable());
    m_ui->button_down->setDisabled(!isModifiable());
    m_ui->test_name->setReadOnly(!isModifiable());
    m_ui->test_description->textEditor()->setReadOnly(!isModifiable());
    m_ui->test_category->setEnabled(isModifiable());
    m_ui->test_type->setEnabled(isModifiable());
    m_ui->test_limit->setEnabled(isModifiable());
    m_ui->automation_command_line->setEnabled(isModifiable());
    m_ui->automation_command_parameters->setEnabled(isModifiable());

    updateTabsWidget();

    // Test automatisé
    if (m_test_content->isAutomatedGuiTest()) {
        setWindowIcon(QPixmap(":/images/48x48/pellicule_auto.png"));

        if (m_automated_actions_model->rowCount() > 0)
            m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->automated_actions), tr("Actions automatisées (%1)").arg(QString::number(m_automated_actions_model->rowCount())));
        else
            m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->automated_actions), tr("Actions automatisées"));

        m_ui->button_start_playback->setEnabled(m_automated_actions_model->rowCount() > 0);
    }
    else if (m_test_content->isAutomatedBatchTest()) {
        setWindowIcon(QPixmap(":/images/48x48/pellicule_auto.png"));
    }
    else {
        setWindowIcon(QPixmap(":/images/48x48/pellicule.png"));

        if (m_actions.count() > 0)
            m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->actions), tr("Actions (%1)").arg(QString::number(m_actions.count())));
        else
            m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->actions), tr("Actions"));
    }

    if (m_test_content->getAllTestRequirements().count() > 0)
        m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->requiremets), tr("Exigences (%1)").arg(QString::number(m_test_content->getAllTestRequirements().count())));
    else
        m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->requiremets), tr("Exigences"));

    if (m_files.count() > 0)
        m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->attachments), tr("Pièces jointes (%1)").arg(QString::number(m_files.count())));
    else
        m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->attachments), tr("Pièces jointes"));

    if (m_bugs.count() > 0)
        m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->bugs), tr("Anomalies (%1)").arg(QString::number(m_bugs.count())));
    else
        m_ui->tabs->setTabText(m_ui->tabs->indexOf(m_ui->bugs), tr("Anomalies"));

    if (isModifiable() == false) {
        m_ui->test_description->toolBar()->hide();
        m_ui->actions_text_toolbar->hide();
    }
    else {
        m_ui->test_description->toolBar()->show();
        m_ui->actions_text_toolbar->show();
    }

    actionSelectionChanged();
}


void FormTest::save()
{
    if (saveTest()) {
        savePluginsDatas();
        setModified(false);
        emit recordSaved(m_test);

        // Tout recharger
//        loadTestContent(m_test_content);
    }
}


/**
  Enregistrer le test en cours de modification
**/
bool FormTest::saveTest()
{
    Action *tmp_action = NULL;
    AutomatedAction *tmp_automated_action = NULL;
    TestRequirement *tmp_test_requirement = NULL;
    TestRequirement *tmp_new_test_requirement = NULL;
    TestContentFile *tmp_file = NULL;
    Bug *tmp_bug = NULL;
    int tmp_compare_versions_result = 0;

    QMessageBox         *tmp_msg_box;
    QPushButton         *tmp_update_button;
    QPushButton         *tmp_conserv_button;
    QPushButton         *tmp_cancel_button;

    TestContent *tmp_old_content = NULL;

    bool                tmp_upgrade_version = false;

    int                 tmp_save_result = NOERR;

    QVariant tmp_test_category;
    QVariant tmp_test_type;

    CustomTestField *tmp_custom_test_field = NULL;

    // Mettre a jour le contenu du test
    tmp_compare_versions_result = compare_values(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_VERSION), m_test->getValueForKey(TESTS_HIERARCHY_VERSION));
    if (tmp_compare_versions_result < 0) {
        // La version du contenu du test est antérieure a la version du test
        tmp_msg_box = new QMessageBox(QMessageBox::Question, tr("Confirmation..."),
                                      tr("La version du contenu du cas de test (%1) est antérieure à la version courante (%2).\nVoulez-vous mettre à niveau la version du contenu vers la version courante ?").arg(ProjectVersion::formatProjectVersionNumber(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_VERSION))).arg(ProjectVersion::formatProjectVersionNumber(m_test->getValueForKey(TESTS_HIERARCHY_VERSION))));
        tmp_update_button = tmp_msg_box->addButton(tr("Mettre à niveau"), QMessageBox::YesRole);
        tmp_conserv_button = tmp_msg_box->addButton(tr("Conserver la version"), QMessageBox::NoRole);
        tmp_cancel_button = tmp_msg_box->addButton(tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->exec();
        if (tmp_msg_box->clickedButton() == tmp_update_button) {
            delete tmp_msg_box;
            tmp_upgrade_version = true;
            tmp_old_content = m_test_content;
            m_test_content = tmp_old_content->copy();
        }
        else if (tmp_msg_box->clickedButton() == tmp_cancel_button) {
            delete tmp_msg_box;
            return false;
        }
    }

    m_test_content->setValueForKey(m_ui->test_name->text().toStdString().c_str(), TESTS_CONTENTS_TABLE_SHORT_NAME);
    m_test_content->setValueForKey(m_ui->test_description->textEditor()->toOptimizedHtml().toStdString().c_str(), TESTS_CONTENTS_TABLE_DESCRIPTION);
    m_test_content->setValueForKey(QString::number(m_ui->test_priority_level->value()).toStdString().c_str(), TESTS_CONTENTS_TABLE_PRIORITY_LEVEL);

    if (m_test_content->isAutomatedGuiTest() || m_test_content->isAutomatedBatchTest()) {
        m_test_content->setValueForKey(m_ui->automation_command_line->text().toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND);
        m_test_content->setValueForKey(m_ui->automation_command_parameters->text().toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS);
        m_test_content->setValueForKey(m_ui->batch_return_code_variable->text().toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE);
        m_test_content->setValueForKey(m_ui->batch_stdout_variable->text().toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE);
    }

    tmp_test_type = m_ui->test_type->itemData(m_ui->test_type->currentIndex());
    if (tmp_test_type.isValid())
        m_test_content->setValueForKey(tmp_test_type.toString().toStdString().c_str(), TESTS_CONTENTS_TABLE_TYPE);

    tmp_test_category = m_ui->test_category->itemData(m_ui->test_category->currentIndex());
    if (tmp_test_category.isValid())
        m_test_content->setValueForKey(tmp_test_category.toString().toStdString().c_str(), TESTS_CONTENTS_TABLE_CATEGORY_ID);

    if (m_ui->test_limit->isChecked())
        m_test_content->setValueForKey(YES, TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE);
    else
        m_test_content->setValueForKey(NO, TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE);


    tmp_save_result = cl_transaction_start(Session::instance().getClientSession());

    if (tmp_save_result == NOERR)
        tmp_save_result = m_test_content->saveRecord();

    if (tmp_save_result == NOERR) {
        m_test->setDataFromTestContent(m_test_content);
        tmp_save_result = m_test->saveRecord();
    }

    if (!tmp_upgrade_version) {
        // Mettre a jour les actions a supprimer
        tmp_save_result = m_test_content->saveActions();

#ifdef GUI_AUTOMATION_ACTIVATED
        m_automated_actions_model->submit();
#endif

        // Mettre a jour les exigences a supprimer
        tmp_save_result = m_test_content->saveTestRequirements();

        // Mettre a jour les pièces jointes a supprimer
        tmp_save_result = m_test_content->saveTestContentFiles();

        // Mettre a jour les anomalies a supprimer
        for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_removed_bugs.count(); tmp_index++) {
            tmp_bug = m_removed_bugs[tmp_index];
            if (tmp_bug != NULL) {
                tmp_save_result = tmp_bug->deleteRecord();
            }
        }

        // Mettre a jour les anomalies existantes
        for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_bugs.count(); tmp_index++) {
            tmp_bug = m_bugs[tmp_index];
            if (tmp_bug != NULL) {
                tmp_save_result = tmp_bug->saveRecord();
            }
        }
    }
    else {
#ifdef GUI_AUTOMATION_ACTIVATED

        // Mettre a jour les actions automatisees
        for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_automated_actions_model->rowCount(); tmp_index++) {
            tmp_automated_action = m_automated_actions_model->recordAtIndex(tmp_index);
            if (tmp_automated_action != NULL) {
                AutomatedAction *tmp_new_action = new AutomatedAction();
                tmp_new_action->setValueForKey(tmp_automated_action->getValueForKey(ACTIONS_TABLE_SHORT_NAME), ACTIONS_TABLE_SHORT_NAME);
                tmp_new_action->setValueForKey(tmp_automated_action->getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID), ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);
                tmp_new_action->setValueForKey(tmp_automated_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION), ACTIONS_TABLE_DESCRIPTION);
                tmp_new_action->setValueForKey(tmp_automated_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT), ACTIONS_TABLE_WAIT_RESULT);
                TestContent::AutomatedActionRelation::instance().appendChild(m_test_content, tmp_new_action);
            }
        }
        tmp_save_result = TestContent::AutomatedActionRelation::instance().saveChilds(m_test_content);
#endif
    }


    // Mettre a jour les exigences
    for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_test_content->getAllTestRequirements().count(); tmp_index++) {
        tmp_test_requirement = m_test_content->getTestRequirementAtIndex(tmp_index);
        if (tmp_test_requirement != NULL) {
            if (tmp_upgrade_version) {
                tmp_new_test_requirement = new TestRequirement;
                tmp_new_test_requirement->setValueForKey(m_test_content->getIdentifier(), TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
                tmp_new_test_requirement->setValueForKey(tmp_test_requirement->getValueForKey(TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID), TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
                tmp_save_result = tmp_new_test_requirement->saveRecord();
            }
            else {
                tmp_save_result = tmp_test_requirement->saveRecord();
            }
        }
    }

    // Enregistrer les pieces jointes apres le commit de la transaction
    // car la creation des blobs côtés serveur est déjà faite dans une transaction
    for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_files.count(); tmp_index++) {
        tmp_file = m_files[tmp_index];
        if (tmp_file != NULL) {
            if (tmp_upgrade_version) {
                tmp_file = tmp_file->copy();
                TestContent::TestContentFileRelation::instance().setParent(m_test_content, tmp_file);
            }

            tmp_save_result = tmp_file->saveRecord();
        }
    }

    // Mettre a jour les actions
    for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_actions.count(); tmp_index++) {
        tmp_action = m_actions[tmp_index];
        if (tmp_action != NULL
            && (compare_values(tmp_action->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID), m_test_content->getIdentifier()) == 0
            || (tmp_upgrade_version && tmp_old_content != NULL && compare_values(tmp_action->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID), tmp_old_content->getIdentifier()) == 0))) {
            
            if (tmp_upgrade_version) {
                Action *tmp_old_action = tmp_action;
                tmp_action = new Action();
                tmp_action->setValueForKey(tmp_old_action->getValueForKey(ACTIONS_TABLE_SHORT_NAME), ACTIONS_TABLE_SHORT_NAME);
                tmp_action->setValueForKey(tmp_old_action->getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID), ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);
                tmp_action->setValueForKey(tmp_old_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION), ACTIONS_TABLE_DESCRIPTION);
                tmp_action->setValueForKey(tmp_old_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT), ACTIONS_TABLE_WAIT_RESULT);
                m_test_content->appendAction(tmp_action);
            }

            // Gestion des resources (images)
            // Colonne description
            tmp_save_result = saveTextEditAttachments(m_test_content, textEditAt(tmp_index, 0));
            if (tmp_save_result == NOERR) {
                // Colonne resultat attendu
                tmp_save_result = saveTextEditAttachments(m_test_content, textEditAt(tmp_index, 1));
                if (tmp_save_result == NOERR) {
                    writeActionDataFromTextEditAtIndex(tmp_action, tmp_index);

                    tmp_save_result = m_test_content->saveActions();
                }
            }
        }
    }


    if (tmp_old_content != NULL)
        delete tmp_old_content;

    // Mettre a jour les champs personnalises
    for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_custom_tests_fields.count(); tmp_index++) {
        tmp_custom_test_field = m_custom_tests_fields[tmp_index];
        if (tmp_custom_test_field != NULL) {
            if (tmp_upgrade_version)
                tmp_custom_test_field = tmp_custom_test_field->copy();

            TestContent::CustomTestFieldRelation::instance().setParent(m_test_content, tmp_custom_test_field);

            tmp_save_result = pushEnteredCustomFieldValue(tmp_custom_test_field->getFieldDesc(), tmp_custom_test_field, CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE);
            if (tmp_save_result == EMPTY_OBJECT) {
                QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), tr("Le champ personnalisé <b>%1</b> de l'onglet <b>%2</b> est obligatoire.")
                                      .arg(tmp_custom_test_field->getFieldDesc()->getValueForKey(CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_LABEL))
                                      .arg(tmp_custom_test_field->getFieldDesc()->getValueForKey(CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TAB_NAME)));
                return false;
            }
            else if (tmp_save_result == NOERR)
                tmp_save_result = tmp_custom_test_field->saveRecord();
        }
    }


    if (tmp_save_result == NOERR) {
        tmp_save_result = cl_transaction_commit(Session::instance().getClientSession());
    }

    if (tmp_save_result != NOERR) {
        cl_transaction_rollback(Session::instance().getClientSession());
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
        return false;
    }

    qDeleteAll(m_removed_bugs);
    m_removed_bugs.clear();

    m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(m_test_content->getValueForKey(TESTS_CONTENTS_TABLE_VERSION)));

    return true;
}


void FormTest::cancel()
{
    QWidget::close();
}


void FormTest::setActionAtIndex(Action *in_action, int in_row)
{
    QModelIndex         tmp_first_column_index = m_ui->actions_list->model()->index(in_row, 0);
    QModelIndex         tmp_second_column_index = m_ui->actions_list->model()->index(in_row, 1);

    RecordTextEdit    *tmp_first_column_content = NULL;
    RecordTextEdit    *tmp_second_column_content = NULL;

    QTableWidgetItem *tmp_widget_item = NULL;

    bool tmp_action_modifiable = true;

    removeActionWidgetsAtIndex(in_row);

    if (is_empty_string(in_action->getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID))) {
        tmp_first_column_content = new RecordTextEdit;
        tmp_second_column_content = new RecordTextEdit;

        tmp_action_modifiable = isModifiable() && (is_empty_string(in_action->getIdentifier()) || (compare_values(m_test_content->getIdentifier(), in_action->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID)) == 0));

        tmp_first_column_content->setCompletionFromList(m_test->projectVersion()->project()->parametersNames());
        tmp_first_column_content->setHtml(in_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
        tmp_first_column_content->setTabChangesFocus(true);
        m_ui->actions_text_toolbar->connectToEditor(tmp_first_column_content);
        tmp_first_column_content->setReadOnly(!tmp_action_modifiable);
        // Charger les resources
        loadTextEditAttachments(tmp_first_column_content);

        tmp_second_column_content->setCompletionFromList(m_test->projectVersion()->project()->parametersNames());
        tmp_second_column_content->setHtml(in_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
        tmp_second_column_content->setTabChangesFocus(true);
        m_ui->actions_text_toolbar->connectToEditor(tmp_second_column_content);
        tmp_second_column_content->setReadOnly(!tmp_action_modifiable);

        m_ui->actions_list->setIndexWidget(tmp_first_column_index, tmp_first_column_content);
        m_ui->actions_list->setIndexWidget(tmp_second_column_index, tmp_second_column_content);
        tmp_first_column_content->setRecord(in_action);
        tmp_second_column_content->setRecord(in_action);
        // Charger les resources
        loadTextEditAttachments(tmp_second_column_content);

        connect(tmp_first_column_content, SIGNAL(focused(Record*)), this, SLOT(selectFirstColumnAction(Record*)));
        connect(tmp_first_column_content, SIGNAL(textChanged()), this, SLOT(actionModified()));
        connect(tmp_second_column_content, SIGNAL(focused(Record*)), this, SLOT(selectSecondColumnAction(Record*)));
        connect(tmp_second_column_content, SIGNAL(textChanged()), this, SLOT(actionModified()));
    }
    else {
        m_ui->actions_list->setSpan(in_row, 0, 1, 2);
        tmp_widget_item = new QTableWidgetItem();
        tmp_widget_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_action));
        tmp_widget_item->setText(in_action->getValueForKey(ACTIONS_TABLE_SHORT_NAME));
        tmp_widget_item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        tmp_widget_item->setTextColor(Qt::white);
        m_ui->actions_list->setItem(in_row, 0, tmp_widget_item);
        m_ui->actions_list->resizeRowToContents(in_row);

        QLinearGradient tmp_linear_grad(0, 0, 0, tmp_widget_item->tableWidget()->rowHeight(in_row));
        tmp_linear_grad.setColorAt(0, "#96c479");
        tmp_linear_grad.setColorAt(0.1, "#a5d488");
        tmp_linear_grad.setColorAt(0.49, "#81b268");
        tmp_linear_grad.setColorAt(0.5, "#73a158");
        tmp_linear_grad.setColorAt(1, "#88b268");

        tmp_widget_item->setBackground(tmp_linear_grad);

    }

    updateRowsHeight();
}



void FormTest::addActionAfterLastAction()
{
    m_ui->actions_list->setCurrentIndex(m_ui->actions_list->model()->index(m_ui->actions_list->rowCount() - 1, 0));

    addAction();
}


void FormTest::pasteClipboardPlainTextToActionsList()
{
    const QClipboard *tmp_app_clipboard = QApplication::clipboard();
    const QMimeData *tmp_mime_data = tmp_app_clipboard->mimeData();

    if (tmp_mime_data != NULL) {
        if (tmp_mime_data->hasText()) {
            QString tmp_clipboard_text = tmp_mime_data->text();
            QTextStream tmp_stream(&tmp_clipboard_text, QIODevice::ReadOnly);

            QList<char> tmp_fields_sep;
            QList<char> tmp_records_sep;
            QList<const char*> tmp_fields;

            tmp_fields_sep << '\t';
            tmp_records_sep << '\n';
            tmp_fields << ACTIONS_TABLE_DESCRIPTION;
            tmp_fields << ACTIONS_TABLE_WAIT_RESULT;

            QList<Action*> tmp_actions = Record::readDataFromDevice<Action>(tmp_stream, tmp_fields_sep, tmp_records_sep, tmp_fields);
            foreach(Action* tmp_action, tmp_actions)
            {
                TestContent::ActionRelation::instance().appendChild(m_test_content, tmp_action);
                addAction(tmp_action);
            }
        }
    }
}


void FormTest::pasteClipboardDataToActionsList()
{
    const QClipboard *tmp_app_clipboard = QApplication::clipboard();
    const QMimeData *tmp_mime_data = tmp_app_clipboard->mimeData();

    if (tmp_mime_data != NULL) {
        if (!tmp_mime_data->data("rtmr/data").isEmpty()) {
            QByteArray tmp_clipboard_data = tmp_mime_data->data("rtmr/data");
            QTextStream tmp_stream(&tmp_clipboard_data, QIODevice::ReadOnly);

            QList<char> tmp_fields_sep;
            QList<char> tmp_records_sep;
            QList<const char*> tmp_fields;

            tmp_fields_sep << '\t';
            tmp_records_sep << '\n';
            tmp_fields << ACTIONS_TABLE_DESCRIPTION;
            tmp_fields << ACTIONS_TABLE_WAIT_RESULT;

            QList<Action*> tmp_actions = Record::readDataFromDevice<Action>(tmp_stream, tmp_fields_sep, tmp_records_sep, tmp_fields);
            foreach(Action* tmp_action, tmp_actions)
            {
                TestContent::ActionRelation::instance().appendChild(m_test_content, tmp_action);
                addAction(tmp_action);
            }
        }
        else {
            pasteClipboardPlainTextToActionsList();
        }
    }
}


void FormTest::copySelectedActionsToClipboard()
{
    QString tmp_plain_text_buffer;
    QString tmp_buffer;
    QClipboard* tmp_app_clipboard = QApplication::clipboard();
    QMimeData *tmp_mime_data = new QMimeData();
    Action *tmp_action = NULL;
    QTextDocument tmp_doc;
    QTextCodec *tmp_codec = QTextCodec::codecForLocale();

    foreach(QModelIndex tmp_index, m_ui->actions_list->selectionModel()->selectedIndexes())
    {
        if (tmp_index.row() < m_actions.count() && tmp_index.column() == 0) {
            tmp_action = m_actions[tmp_index.row()];
            if (tmp_action != NULL) {
                writeActionDataFromTextEditAtIndex(tmp_action, tmp_index.row());

                // Formated
                tmp_buffer += '\"';
                tmp_buffer += QString(tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION)).replace('\"', "\"\"");
                tmp_buffer += "\"\t\"";
                tmp_buffer += QString(tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT)).replace('\"', "\"\"");
                tmp_buffer += '\"';
                tmp_buffer += '\n';

                // Not formated
                tmp_plain_text_buffer += '\"';
                tmp_doc.setHtml(tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
                tmp_plain_text_buffer += tmp_doc.toPlainText().replace('\"', "\"\"");
                tmp_plain_text_buffer += "\"\t\"";
                tmp_doc.setHtml(tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
                tmp_plain_text_buffer += tmp_doc.toPlainText().replace('\"', "\"\"");
                tmp_plain_text_buffer += '\"';
                tmp_plain_text_buffer += '\n';
            }
        }
    }

    tmp_mime_data->setText(tmp_plain_text_buffer);
    tmp_mime_data->setData("rtmr/data", tmp_codec->fromUnicode(tmp_buffer));

    tmp_app_clipboard->setMimeData(tmp_mime_data, QClipboard::Clipboard);
}



void FormTest::cutSelectedActionsToClipboard()
{
    copySelectedActionsToClipboard();
    deleteSelectedAction();
}



void FormTest::selectFirstColumnAction(Record *in_action)
{
    selectAction(in_action, 0);
}


void FormTest::selectSecondColumnAction(Record *in_action)
{
    selectAction(in_action, 1);
}


void FormTest::selectAction(Record *in_action, int in_column)
{
    int tmp_index = 0;
    foreach(Action *tmp_action, m_actions)
    {
        if (tmp_action == in_action) {
            m_ui->actions_list->selectionModel()->setCurrentIndex(m_ui->actions_list->model()->index(tmp_index, in_column), QItemSelectionModel::ClearAndSelect);
            QWidget *tmp_widget = m_ui->actions_list->indexWidget(m_ui->actions_list->model()->index(tmp_index, in_column));
            if (tmp_widget != NULL)
                tmp_widget->setFocus();

            return;
        }
        tmp_index++;
    }
}


void FormTest::actionModified()
{
    setModified();
    updateRowsHeight();
}

void FormTest::updateRowsHeight()
{
    QSizeF tmp_first_size, tmp_second_size;
    QModelIndex      tmp_first_column_index, tmp_second_column_index;
    RecordTextEdit    *tmp_first_column_content = NULL;
    RecordTextEdit    *tmp_second_column_content = NULL;

    for (int tmp_index = 0; tmp_index < m_ui->actions_list->rowCount(); tmp_index++) {
        tmp_first_column_index = m_ui->actions_list->model()->index(tmp_index, 0);
        tmp_first_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_first_column_index));

        tmp_second_column_index = m_ui->actions_list->model()->index(tmp_index, 1);
        tmp_second_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_second_column_index));

        if (tmp_first_column_content != NULL && tmp_second_column_content != NULL) {
            tmp_first_size = tmp_first_column_content->document()->size();
            tmp_second_size = tmp_second_column_content->document()->size();

            if (tmp_first_size.height() > tmp_second_size.height())
                m_ui->actions_list->setRowHeight(tmp_index, tmp_first_size.height() + 10);
            else
                m_ui->actions_list->setRowHeight(tmp_index, tmp_second_size.height() + 10);
        }
        else
            m_ui->actions_list->resizeRowToContents(tmp_index);
    }

    if (m_ui->actions_list->currentRow() == m_ui->actions_list->rowCount() - 1) {
        // Scroll automatique sur la dernière ligne
        m_ui->actions_list->scrollToBottom();
    }
    else {
        // Scroll automatique sur la ligne courante
        if (m_ui->actions_list->rowHeight(m_ui->actions_list->currentRow()) > m_ui->actions_list->height())
            m_ui->actions_list->scrollTo(m_ui->actions_list->currentIndex(), QAbstractItemView::PositionAtBottom);
        else
            m_ui->actions_list->scrollTo(m_ui->actions_list->currentIndex(), QAbstractItemView::EnsureVisible);
    }
}


void FormTest::removeActionWidgetsAtIndex(int in_row)
{
    QModelIndex      tmp_first_column_index, tmp_second_column_index;
    RecordTextEdit    *tmp_first_column_content = NULL;
    RecordTextEdit    *tmp_second_column_content = NULL;

    tmp_first_column_index = m_ui->actions_list->model()->index(in_row, 0);
    if (tmp_first_column_index.isValid()) {
        tmp_first_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_first_column_index));
        if (tmp_first_column_content != NULL) {
            m_ui->actions_text_toolbar->disconnectFromEditor(tmp_first_column_content);
        }
    }

    tmp_second_column_index = m_ui->actions_list->model()->index(in_row, 1);
    if (tmp_second_column_index.isValid()) {
        tmp_second_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_second_column_index));
        if (tmp_second_column_content != NULL) {
            m_ui->actions_text_toolbar->disconnectFromEditor(tmp_second_column_content);
        }
    }
}

void FormTest::addAction()
{
    addAction(new Action());
}


void FormTest::addAction(Action *in_action)
{
    QModelIndex tmp_current_index = m_ui->actions_list->selectionModel()->currentIndex();
    int tmp_row = 0;
    bool tmp_current_action_modifiable = false;

    setModified();

    if (tmp_current_index.isValid()) {
        tmp_row = tmp_current_index.row();
        while (!tmp_current_action_modifiable) {
            tmp_row++;
            if (tmp_row < m_actions.count())
                tmp_current_action_modifiable = compare_values(m_actions[tmp_row]->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID), m_test_content->getIdentifier()) == 0;
            else
                tmp_current_action_modifiable = true;
        }
    }
    else
        tmp_row = m_ui->actions_list->rowCount();

    TestContent::ActionRelation::instance().appendChild(m_test_content, in_action);
    m_actions.insert(tmp_row, in_action);
    m_ui->actions_list->insertRow(tmp_row);

    setActionAtIndex(in_action, tmp_row);

    selectAction(in_action);
}


void FormTest::deleteSelectedAction()
{
    Action *tmp_action = NULL;
    int             tmp_row = 0;
    QModelIndexList tmp_selected_indexes = m_ui->actions_list->selectionModel()->selectedIndexes();

    if (!tmp_selected_indexes.isEmpty()) {
        setModified();

        while (tmp_selected_indexes.count() > 0) {
            if (tmp_selected_indexes[0].isValid()) {
                tmp_row = tmp_selected_indexes[0].row();
                removeActionWidgetsAtIndex(tmp_row);
                m_ui->actions_list->removeRow(tmp_row);
                tmp_action = m_actions.takeAt(tmp_row);
                m_test_content->removeAction(tmp_action);

                for (int tmp_index = 0; tmp_index < tmp_action->associatedTestActions().count(); tmp_index++) {
                    removeActionWidgetsAtIndex(tmp_row);
                    m_ui->actions_list->removeRow(tmp_row);
                    m_actions.removeAt(tmp_row);
                }
            }
            tmp_selected_indexes = m_ui->actions_list->selectionModel()->selectedIndexes();
        }

        if (tmp_row >= m_ui->actions_list->rowCount())
            tmp_row--;

        if (tmp_row < m_ui->actions_list->rowCount())
            m_ui->actions_list->setCurrentIndex(m_ui->actions_list->model()->index(tmp_row, 0));
    }
}


void FormTest::moveSelectedActionUp()
{
    QModelIndex         tmp_current_index = m_ui->actions_list->selectionModel()->currentIndex();
    int                 tmp_current_row = 0, tmp_previous_row = 0, tmp_previous_column = 0, tmp_dest_row = 0;
    int tmp_index = 0;
    Action *tmp_current_action = NULL;
    QList<Action*> tmp_current_action_list;
    bool tmp_link_action = true;

    if (tmp_current_index.isValid()) {
        tmp_current_row = tmp_current_index.row();
        tmp_previous_column = tmp_current_index.column();
        tmp_current_action = m_actions[tmp_current_row];
        tmp_current_action_list = tmp_current_action->associatedTestActions();

        tmp_previous_row = tmp_current_row;
        while (tmp_previous_row >= 0 && tmp_link_action) {
            tmp_previous_row--;
            tmp_link_action = (compare_values(m_test_content->getIdentifier(), m_actions[tmp_previous_row]->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID)) != 0);
        }

        if (tmp_previous_row >= 0) {
            setModified();
            writeActionDataFromTextEditAtIndex(tmp_current_action, tmp_current_row);

            for (tmp_index = 0; tmp_index <= tmp_current_action_list.count(); tmp_index++) {
                removeActionWidgetsAtIndex(tmp_current_row);
                m_ui->actions_list->removeRow(tmp_current_row);
                m_actions.removeAt(tmp_current_row);
            }

            m_actions.insert(tmp_previous_row, tmp_current_action);
            m_ui->actions_list->insertRow(tmp_previous_row);
            setActionAtIndex(tmp_current_action, tmp_previous_row);

            for (tmp_index = 0; tmp_index < tmp_current_action_list.count(); tmp_index++) {
                tmp_dest_row = tmp_previous_row + tmp_index + 1;
                m_actions.insert(tmp_dest_row, tmp_current_action_list[tmp_index]);
                m_ui->actions_list->insertRow(tmp_dest_row);
                setActionAtIndex(tmp_current_action_list[tmp_index], tmp_dest_row);
            }

            m_ui->actions_list->setCurrentIndex(m_ui->actions_list->model()->index(tmp_previous_row, tmp_previous_column));
        }
    }
}


void FormTest::moveSelectedActionDown()
{
    QModelIndex         tmp_current_index = m_ui->actions_list->selectionModel()->currentIndex();
    int                 tmp_current_row = 0, tmp_next_row = 0, tmp_previous_column = 0, tmp_dest_row = 0;
    int tmp_index = 0;
    Action *tmp_current_action = NULL, *tmp_next_action = NULL;
    QList<Action*> tmp_current_action_list;

    if (tmp_current_index.isValid()) {
        tmp_current_row = tmp_current_index.row();
        tmp_previous_column = tmp_current_index.column();
        tmp_current_action = m_actions[tmp_current_row];
        tmp_current_action_list = tmp_current_action->associatedTestActions();

        tmp_next_row = tmp_current_row + tmp_current_action_list.count() + 1;

        if (tmp_next_row < m_ui->actions_list->rowCount()) {
            tmp_next_action = m_actions[tmp_next_row];

            setModified();
            writeActionDataFromTextEditAtIndex(tmp_current_action, tmp_current_row);

            for (tmp_index = 0; tmp_index <= tmp_current_action_list.count(); tmp_index++) {
                removeActionWidgetsAtIndex(tmp_current_row);
                m_ui->actions_list->removeRow(tmp_current_row);
                m_actions.removeAt(tmp_current_row);
            }

            tmp_dest_row = tmp_current_row + tmp_next_action->associatedTestActions().count() + 1;
            m_actions.insert(tmp_dest_row, tmp_current_action);
            m_ui->actions_list->insertRow(tmp_dest_row);
            setActionAtIndex(tmp_current_action, tmp_dest_row);

            for (tmp_index = 0; tmp_index < tmp_current_action_list.count(); tmp_index++) {
                tmp_dest_row++;
                m_actions.insert(tmp_dest_row, tmp_current_action_list[tmp_index]);
                m_ui->actions_list->insertRow(tmp_dest_row);
                setActionAtIndex(tmp_current_action_list[tmp_index], tmp_dest_row);
            }

            m_ui->actions_list->setCurrentIndex(m_ui->actions_list->model()->index(tmp_current_row + tmp_next_action->associatedTestActions().count() + 1, tmp_previous_column));

        }
    }
}



void FormTest::actionSelectionChanged()
{
    QModelIndexList     tmp_selected_indexes = m_ui->actions_list->selectionModel()->selectedIndexes();
    bool     tmp_action_modifiable = false;
    bool     tmp_link_action = false;
    int     tmp_row = 0;

    m_ui->button_add->setEnabled(isModifiable());

    if (!tmp_selected_indexes.isEmpty()) {
        foreach(QModelIndex tmp_index, tmp_selected_indexes)
        {
            if (tmp_index.isValid()) {
                tmp_row = tmp_index.row();
                tmp_link_action |= (compare_values(m_test_content->getIdentifier(), m_actions[tmp_row]->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID)) != 0);
            }
        }

        tmp_action_modifiable = isModifiable() && !tmp_link_action;

        m_ui->button_add->setEnabled(isModifiable() && !tmp_link_action);
        m_ui->button_del->setEnabled(tmp_action_modifiable);

        m_ui->button_up->setEnabled(tmp_action_modifiable && tmp_row > 0);
        m_ui->button_down->setEnabled(tmp_action_modifiable && tmp_row + m_actions[tmp_row]->associatedTestActions().count() < m_ui->actions_list->rowCount() - 1);

        m_ui->button_copy->setEnabled(true);
        m_ui->button_cut->setEnabled(tmp_action_modifiable);
    }
    else {
        m_ui->button_del->setEnabled(false);

        m_ui->button_up->setEnabled(false);
        m_ui->button_down->setEnabled(false);
        m_ui->button_copy->setEnabled(false);
        m_ui->button_cut->setEnabled(false);

    }
}


void FormTest::actionsDrop(QList<Record*> in_list, int in_row)
{
    Test   *tmp_test = NULL;
    int     tmp_action_index = in_row;
    Action     *tmp_action = NULL;

    if (isModifiable() && (in_row >= m_actions.count() ||
        compare_values(m_test_content->getIdentifier(), m_actions[in_row]->getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID)) == 0)) {
        foreach(Record *tmp_record, in_list)
        {
            // L'objet déposé est un test
            if (tmp_record->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID) {
                // Le test déposé n'est pas celui en cours d'édition
                if (compare_values(m_test->getIdentifier(), tmp_record->getIdentifier()) != 0 &&
                    compare_values(m_test->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), tmp_record->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID)) != 0) {
                    tmp_test = ProjectVersion::TestRelation::instance().getUniqueChildWithValueForKey(m_test->projectVersion(), tmp_record->getIdentifier(), TESTS_HIERARCHY_TEST_ID);
                    if (tmp_test != NULL) {
                        tmp_action = new Action();
                        if (tmp_action->setAssociatedTest(tmp_test)) {
                            setModified();

                            TestContent::ActionRelation::instance().insertChild(m_test_content, tmp_action_index, tmp_action);
                            m_actions.insert(tmp_action_index, tmp_action);
                            m_ui->actions_list->insertRow(tmp_action_index);

                            setActionAtIndex(tmp_action, tmp_action_index);
                            tmp_action_index++;

                            foreach(Action *tmp_associated_action, tmp_action->associatedTestActions()) {
                                m_actions.insert(tmp_action_index, tmp_associated_action);
                                m_ui->actions_list->insertRow(tmp_action_index);

                                setActionAtIndex(tmp_associated_action, tmp_action_index);
                                tmp_action_index++;
                            }
                        }
                        else {
                            delete tmp_action;
                            QApplication::restoreOverrideCursor();
                            QMessageBox::warning(this, tr("Redondance cyclique..."), tr("Les actions du scénario que vous voulez insérer font directement ou indirectement référence aux actions du scénario en cours d'édition."));
                        }
                    }
                }
            }
        }
    }
    qDeleteAll(in_list);
}


void FormTest::requirementsDrop(QList<Record*> in_list, int /* in_row */)
{
    TestRequirement   *tmp_test_requirement = NULL;
    TestRequirement   *tmp_current_test_requirement = NULL;
    QTableWidgetItem     *tmp_item = NULL;
    bool                 tmp_item_exists = false;

    if (isModifiable()) {
        foreach(Record *tmp_record, in_list)
        {
            if (tmp_record->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID) {
                if (tmp_record != NULL) {
                    // Verifier le projet
                    if (compare_values(tmp_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), m_test->getValueForKey(TESTS_HIERARCHY_PROJECT_ID)) == 0) {
                        tmp_item_exists = false;
                        for (int tmp_index = 0; tmp_item_exists == false && tmp_index < m_ui->requirements_list->rowCount(); tmp_index++) {
                            tmp_item = m_ui->requirements_list->item(tmp_index, 0);
                            tmp_current_test_requirement = (TestRequirement*)tmp_item->data(Qt::UserRole).value<void*>();
                            tmp_item_exists = (tmp_current_test_requirement != NULL
                                               && compare_values(
                                               tmp_record->getValueForKey(REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID),
                                               tmp_current_test_requirement->getValueForKey(TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID)) == 0);
                        }

                        if (tmp_item_exists == false) {
                            setModified();
                            tmp_test_requirement = new TestRequirement();
                            tmp_test_requirement->setValueForKey(m_test_content->getIdentifier(), TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
                            tmp_test_requirement->setValueForKey(tmp_record->getValueForKey(REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID), TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
                            m_test_content->appendTestRequirement(tmp_test_requirement);
                            m_ui->requirements_list->insertRow(m_ui->requirements_list->rowCount());
                            setRequirementForRow(tmp_test_requirement, m_ui->requirements_list->rowCount() - 1);
                        }
                    }
                }
            }
        }
    }
    qDeleteAll(in_list);
}


void FormTest::deletedRequirementAtIndex(int in_row)
{
    setModified();
    m_test_content->removeTestRequirementAtIndex(in_row);
}

void FormTest::setRequirementForRow(TestRequirement *in_requirement, int in_row)
{
    QTableWidgetItem    *tmp_first_column_item = NULL;
    QTableWidgetItem    *tmp_second_column_item = NULL;
    QTableWidgetItem    *tmp_third_column_item = NULL;

    if (in_requirement != NULL) {
        // Premiere colonne
        tmp_first_column_item = new QTableWidgetItem;
        tmp_first_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_requirement));
        RequirementContent *requirement = RequirementContent::TestRequirementRelation::instance().getParent(in_requirement);
        if (requirement) {
            tmp_first_column_item->setText(requirement->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION));
        }
        tmp_first_column_item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
        m_ui->requirements_list->setItem(in_row, 0, tmp_first_column_item);

        // Deuxieme colonne
        tmp_second_column_item = new QTableWidgetItem;
        tmp_second_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_requirement));
        if (requirement) {
            tmp_second_column_item->setText(m_ui->test_category->itemText(m_ui->test_category->findData(requirement->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID))));
        }
        tmp_second_column_item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
        m_ui->requirements_list->setItem(in_row, 1, tmp_second_column_item);

        // Troisième colonne
        tmp_third_column_item = new QTableWidgetItem;
        tmp_third_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_requirement));
        if (requirement) {
            tmp_third_column_item->setText(ProjectVersion::formatProjectVersionNumber(requirement->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION)));
        }
        tmp_third_column_item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
        m_ui->requirements_list->setItem(in_row, 2, tmp_third_column_item);

    }
}

RecordTextEdit* FormTest::textEditAt(int in_row, int in_column)
{
    QModelIndex      tmp_column_index;
    RecordTextEdit    *tmp_column_content = NULL;

    tmp_column_index = m_ui->actions_list->model()->index(in_row, in_column);
    if (tmp_column_index.isValid()) {
        tmp_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_column_index));
    }

    return tmp_column_content;
}

void FormTest::writeActionDataFromTextEditAtIndex(Action *in_action, int in_index)
{
    QModelIndex      tmp_first_column_index, tmp_second_column_index;
    RecordTextEdit    *tmp_first_column_content = NULL;
    RecordTextEdit    *tmp_second_column_content = NULL;

    tmp_first_column_index = m_ui->actions_list->model()->index(in_index, 0);
    if (tmp_first_column_index.isValid()) {
        tmp_first_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_first_column_index));
        if (tmp_first_column_content != NULL)
            in_action->setValueForKey(tmp_first_column_content->toOptimizedHtml().toStdString().c_str(), ACTIONS_TABLE_DESCRIPTION);
    }

    tmp_second_column_index = m_ui->actions_list->model()->index(in_index, 1);
    if (tmp_second_column_index.isValid()) {
        tmp_second_column_content = ::qobject_cast<RecordTextEdit *>(m_ui->actions_list->indexWidget(tmp_second_column_index));
        if (tmp_second_column_content != NULL)
            in_action->setValueForKey(tmp_second_column_content->toOptimizedHtml().toStdString().c_str(), ACTIONS_TABLE_WAIT_RESULT);
    }
}


void FormTest::filesDrop(QList<QString> in_list, int /*in_row*/)
{
    TestContentFile *tmp_file = NULL;

    if (isModifiable()) {
        setModified();
        foreach(QString tmp_filename, in_list)
        {
            tmp_file = new TestContentFile();
            TestContent::TestContentFileRelation::instance().setParent(m_test_content, tmp_file);
            tmp_file->setSourceFile(tmp_filename.toStdString().c_str());
            m_files.insert(m_files.count(), tmp_file);
            m_ui->files_list->insertRow(m_ui->files_list->rowCount());
            setFileForRow(tmp_file, m_ui->files_list->rowCount() - 1);
        }
    }
}


void FormTest::setFileForRow(TestContentFile *in_file, int in_row)
{
    QTableWidgetItem    *tmp_column_item = NULL;

    if (in_file != NULL) {
        tmp_column_item = new QTableWidgetItem;
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_file));
        tmp_column_item->setText(in_file->getValueForKey(TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILENAME));
        m_ui->files_list->setItem(in_row, 0, tmp_column_item);
    }
}


void FormTest::setBugForRow(Bug *in_bug, int in_row)
{
    QTableWidgetItem    *tmp_first_column_item = NULL;
    QTableWidgetItem    *tmp_second_column_item = NULL;
    QTableWidgetItem    *tmp_third_column_item = NULL;
    QTableWidgetItem    *tmp_fourth_column_item = NULL;
    QTableWidgetItem    *tmp_fifth_column_item = NULL;

    QString tmp_tool_tip;
    QString tmp_url;
    QString tmp_external_link;
    QDateTime tmp_date_time;

    QLabel              *tmp_external_link_label = NULL;

    if (in_bug != NULL) {
        tmp_date_time = QDateTime::fromString(QString(in_bug->getValueForKey(BUGS_TABLE_CREATION_DATE)).left(16), "yyyy-MM-dd hh:mm");
        tmp_tool_tip += "<p><b>" + tr("Date de création") + "</b> : " + tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm") + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Résumé") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SHORT_NAME)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Priorité") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_PRIORITY)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Gravité") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SEVERITY)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Plateforme") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_PLATFORM)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Système d'exploitation") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SYSTEM)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Description") + "</b> :<br>" + QString(in_bug->getValueForKey(BUGS_TABLE_DESCRIPTION)).replace('\n', "<BR>") + "</p>";
        if (is_empty_string(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE) {
            tmp_tool_tip += "<p><b>" + tr("Identifiant externe") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) + "</p>";
            if (m_bugtracker != NULL) {
                tmp_url = m_bugtracker->urlForBugWithId(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
                if (!tmp_url.isEmpty())
                    tmp_external_link = "<a href=\"" + tmp_url + "\">" + tmp_url + "</a>";
            }

            if (tmp_external_link.isEmpty())
                tmp_external_link = QString(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
        }

        // Premirère colonne (date de création du bug)
        tmp_first_column_item = new QTableWidgetItem;
        tmp_first_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_first_column_item->setText(tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm"));
        tmp_first_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 0, tmp_first_column_item);

        // Seconde colonne (résumé)
        tmp_second_column_item = new QTableWidgetItem;
        tmp_second_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_second_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_SHORT_NAME));
        tmp_second_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 1, tmp_second_column_item);

        // Troisième colonne (priorité)
        tmp_third_column_item = new QTableWidgetItem;
        tmp_third_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_third_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_PRIORITY));
        tmp_third_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 2, tmp_third_column_item);

        // Quatrième colonne (sévérité)
        tmp_fourth_column_item = new QTableWidgetItem;
        tmp_fourth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_fourth_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_SEVERITY));
        tmp_fourth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 3, tmp_fourth_column_item);

        // Cinquième colonne (id externe)
        tmp_external_link_label = new QLabel(tmp_external_link);
        tmp_external_link_label->setOpenExternalLinks(true);
        tmp_fifth_column_item = new QTableWidgetItem;
        tmp_fifth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_fifth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 4, tmp_fifth_column_item);
        m_ui->bugs_list->setCellWidget(in_row, 4, tmp_external_link_label);
    }
}


void FormTest::deletedFileAtIndex(int in_index)
{
    setModified();
    m_test_content->removeTestContentFile(m_files.takeAt(in_index));
}


void FormTest::deletedBugAtIndex(int in_index)
{
    setModified();
    m_removed_bugs.append(m_bugs.takeAt(in_index));
}


void FormTest::openBugAtIndex(QTableWidgetItem *in_item)
{
    Bug     *tmp_bug = NULL;
    FormBug     *tmp_formbug = NULL;

    if (in_item != NULL) {
        tmp_bug = (Bug*)in_item->data(Qt::UserRole).value<void*>();
        if (tmp_bug != NULL) {
            tmp_formbug = new FormBug(m_test->projectVersion(), NULL, NULL, tmp_bug);
            tmp_formbug->show();
        }
    }
}

void FormTest::loadPreviousTestContent()
{
    TestContent *tmp_test_content = m_previous_test_content;

    if (maybeClose()) {
        if (m_test_content != NULL
            && m_previous_test_content != NULL
            && compare_values(m_test_content->getIdentifier(), m_previous_test_content->getIdentifier()) == 0)
            tmp_test_content = m_test_content;

        loadTestContent(tmp_test_content);
    }
}



void FormTest::openFileAtIndex(QTableWidgetItem *in_item)
{
    TestContentFile *tmp_file = NULL;
    QString tmp_filename;
    const char *tmp_filename_str = NULL;
    int tmp_return = NOERR;
    QString tmp_temp_dir = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    net_session *tmp_session = Session::instance().getClientSession();

    if (in_item != NULL) {
        tmp_file = (TestContentFile*)in_item->data(Qt::UserRole).value<void*>();
        if (tmp_file != NULL) {
            if (is_empty_string(tmp_file->getIdentifier())) {

            }
            else {
                tmp_filename_str = tmp_file->getValueForKey(TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILENAME);
                if (is_empty_string(tmp_filename_str) == FALSE) {
#if QT_VERSION >= 0x040800
                    if (QApplication::queryKeyboardModifiers() & Qt::ControlModifier)
#else
                    if (QApplication::keyboardModifiers() & Qt::ControlModifier)
#endif
                    {
                        tmp_filename = QFileDialog::getSaveFileName(this, tr("Enregistrer le fichier sous..."), tmp_filename_str);
                        if (tmp_filename.isEmpty() == false) {
                            tmp_return = cl_get_blob(tmp_session, tmp_file->getValueForKey(TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_LO_ID), tmp_filename.toStdString().c_str());
                            if (tmp_return != NOERR) {
                                QMessageBox::critical(this, tr("Erreur lors de la récupération du fichier"), Session::instance().getErrorMessage(tmp_return));
                            }
                        }
                    }
                    else {
                        tmp_filename = tmp_temp_dir + "/" + tmp_filename_str;
                        tmp_return = cl_get_blob(tmp_session, tmp_file->getValueForKey(TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_LO_ID), tmp_filename.toStdString().c_str());
                        if (tmp_return != NOERR) {
                            QMessageBox::critical(this, tr("Erreur lors de la récupération du fichier"), Session::instance().getErrorMessage(tmp_return));
                        }
                        else {
                            QDesktopServices::openUrl(QUrl("file:///" + tmp_filename));
                        }
                    }
                }
            }
        }
    }
}


void FormTest::showEvent(QShowEvent * event)
{
    QWidget::showEvent(event);

    updateRowsHeight();
}


void FormTest::closeEvent(QCloseEvent *in_event)
{
    if (maybeClose())
        in_event->accept();
    else
        in_event->ignore();
}


void FormTest::showOriginalTestInfosClicked()
{
    emit showOriginalTestInfos(m_test);
}



bool FormTest::maybeClose()
{
    int tmp_confirm_choice = 0;
    bool    tmp_return = true;

    if (m_test_content != NULL) {
        if (isModified()) {
            tmp_confirm_choice = QMessageBox::question(
                this,
                tr("Confirmation..."),
                tr("Le test a été modifié. Voulez-vous enregistrer les modifications ?"),
                QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                QMessageBox::Cancel);

            if (tmp_confirm_choice == QMessageBox::Yes)
                tmp_return = saveTest();
            else if (tmp_confirm_choice == QMessageBox::Cancel)
                tmp_return = false;
        }

        if (tmp_return)
            m_test_content->unlockRecord();
    }

    return tmp_return;
}


void FormTest::showRequirementAtIndex(QModelIndex in_index)
{
    TestRequirement             *tmp_test_requirement = NULL;
    QTableWidgetItem            *tmp_column_item = NULL;

    if (in_index.isValid()) {
        tmp_column_item = m_ui->requirements_list->item(in_index.row(), 0);
        if (tmp_column_item != NULL) {
            tmp_test_requirement = (TestRequirement*)tmp_column_item->data(Qt::UserRole).value<void*>();
            if (tmp_test_requirement != NULL) {
                emit showRequirementWithOriginalContentId(m_test->projectVersion(), tmp_test_requirement->getValueForKey(TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID));
            }
        }
    }
}



void FormTest::changeTab(int in_tab_index)
{
    if (in_tab_index == 0) {
        updateRowsHeight();
    }
}


void FormTest::selectExternalCommandForAutomation()
{
    QString tmp_filename = QFileDialog::getOpenFileName(
        this,
        tr("Sélectionner un fichier"), m_ui->automation_command_line->text(), "Executables (*.exe)");

    if (tmp_filename.isEmpty() == false) {
        m_ui->automation_command_line->setText(tmp_filename);
    }
}


void FormTest::showProcessError(const QString & in_process_error)
{
    QMessageBox::critical(this, tr("Erreur"), tr("Le progamme %1 n'a pû être démarré correctement : %2.").arg(in_process_error));
}


void FormTest::launchStartRecordSystemEvents()
{
    QString tmp_exec_file = RecordTextEdit::toHtmlWithParametersValues<ProjectParameter>(Project::ProjectParameterRelation::instance().getChilds(m_test->projectVersion()->project()), m_ui->automation_command_line->text()).trimmed();
    if (tmp_exec_file.isEmpty()) {
        QMessageBox::critical(this, tr("Erreur"), tr("Veuillez sélectionner une application à lancer dans la section Automatisation."));
    }
    else {
        if (!QFile::exists(tmp_exec_file)) {
            QMessageBox::critical(this, tr("Erreur"), tr("L'executable' %1 n'existe pas.").arg(tmp_exec_file));
        }
        else {
            m_record_processor.stopProcess();

            m_record_processor = GuiAutomationRecordProcessor(tmp_exec_file,
                                                              RecordTextEdit::toHtmlWithParametersValues<ProjectParameter>(
                                                              Project::ProjectParameterRelation::instance().getChilds(
                                                              m_test->projectVersion()->project()),
                                                              m_ui->automation_command_parameters->text()).split(' ', QString::SkipEmptyParts));

            connect(&m_record_processor, SIGNAL(processError(QString)), this, SLOT(showProcessError(QString)));
            connect(&m_record_processor, SIGNAL(actionsRecorded(QList<AutomatedAction*>)), this, SLOT(automatedActionsRecorded(QList<AutomatedAction*>)));

            m_record_processor.launchProcess();

            m_ui->button_start_record->setEnabled(false);
            m_ui->button_stop_record->setEnabled(true);
            m_ui->button_start_playback->setEnabled(false);

        }
    }
}


void FormTest::launchBatch()
{
    QString tmp_exec_file = RecordTextEdit::toHtmlWithParametersValues<ProjectParameter>(Project::ProjectParameterRelation::instance().getChilds(m_test->projectVersion()->project()), m_ui->automation_command_line->text()).trimmed();
    if (tmp_exec_file.isEmpty()) {
        QMessageBox::critical(this, tr("Erreur"), tr("Veuillez saisir la commande à lancer dans la section Automatisation."));
    }
    else {
        if (!QFile::exists(tmp_exec_file)) {
            QMessageBox::critical(this, tr("Erreur"), tr("L'executable' %1 n'existe pas.").arg(tmp_exec_file));
        }
        else {
            m_batch_processor.stopProcess();

            QString tmp_parameters = RecordTextEdit::toHtmlWithParametersValues<ProjectParameter>(Project::ProjectParameterRelation::instance().getChilds(m_test->projectVersion()->project()),
                                                                                                  m_ui->automation_command_parameters->text());


            QList<QString> tmp_output_parameters;
            QString returnCodeParameterName = m_ui->batch_return_code_variable->text();
            QString returnOutputParameterName = m_ui->batch_stdout_variable->text();
            Parameter* returnCodeParameter = NULL;
            Parameter* returnOutputParameter = NULL;

            if (!returnCodeParameterName.isEmpty())
                tmp_output_parameters << returnCodeParameterName;

            if (!returnOutputParameterName.isEmpty())
                tmp_output_parameters << returnOutputParameterName;

            m_batch_processor = BatchAutomationProcessor(tmp_exec_file, tmp_parameters.split(' ', QString::SkipEmptyParts));
            connect(&m_batch_processor, SIGNAL(processStop(int)), this, SLOT(stopBatchProcess(int)));
            connect(&m_batch_processor, SIGNAL(processError(QString)), this, SLOT(showProcessError(QString)));
            connect(&m_batch_processor, SIGNAL(processStandardOutput(QString)), this, SLOT(readBatchProcessStandardOutput(QString)));
            connect(&m_batch_processor, SIGNAL(processStandardError(QString)), this, SLOT(readBatchProcessStandardError(QString)));
            m_batch_processor.launchProcess(returnCodeParameter, returnOutputParameter);

            m_ui->batch_process_stdout->setPlainText("----------------------------------------------------------------------------------------------------------------------");
            m_ui->batch_process_stdout->appendPlainText(tr("Lancement de %1 %2 (PID=%3)")
                                                        .arg(tmp_exec_file)
                                                        .arg(tmp_parameters)
#if defined(__WINDOWS) ||  defined(WIN32)
                                                        .arg(m_batch_processor.getProcess().pid()->dwProcessId));
#else
                .arg(m_batch_processor.getProcess().pid()));
#endif
            m_ui->batch_process_stdout->appendPlainText("----------------------------------------------------------------------------------------------------------------------");
        }
    }
}


void FormTest::launchStartPlaybackSystemEvents()
{
    QString tmp_exec_file = RecordTextEdit::toHtmlWithParametersValues<ProjectParameter>(Project::ProjectParameterRelation::instance().getChilds(m_test->projectVersion()->project()), m_ui->automation_command_line->text()).trimmed();
    if (tmp_exec_file.isEmpty()) {
        QMessageBox::critical(this, tr("Erreur"), tr("Veuillez sélectionner une application à lancer dans la section Automatisation."));
    }
    else {
        if (!QFile::exists(tmp_exec_file)) {
            QMessageBox::critical(this, tr("Erreur"), tr("L'executable' %1 n'existe pas.").arg(tmp_exec_file));
        }
        else {
            m_playback_processor.stopProcess();

            QStringList tmp_parameters = RecordTextEdit::toHtmlWithParametersValues<ProjectParameter>(Project::ProjectParameterRelation::instance().getChilds(m_test->projectVersion()->project()),
                                                                                                      m_ui->automation_command_parameters->text()).split(' ', QString::SkipEmptyParts);

            QList<QString> tmp_output_parameters;
            QString returnCodeParameterName = m_ui->batch_return_code_variable->text();
            QString returnOutputParameterName = m_ui->batch_stdout_variable->text();
            Parameter* returnCodeParameter = NULL;
            Parameter* returnOutputParameter = NULL;

            if (!returnCodeParameterName.isEmpty())
                tmp_output_parameters << returnCodeParameterName;

            if (!returnOutputParameterName.isEmpty())
                tmp_output_parameters << returnOutputParameterName;

            m_playback_processor = GuiAutomationPlaybackProcessor(TestContent::AutomatedActionRelation::instance().getChilds(m_test_content), tmp_exec_file, tmp_parameters);
            connect(&m_playback_processor, SIGNAL(processError(QString)), this, SLOT(showProcessError(QString)));

            m_playback_processor.launchProcess(returnCodeParameter, returnOutputParameter);
        }
    }
}


void FormTest::loadPluginsViews()
{
    QMap < QString, ClientModule*> tmp_modules_map = Session::instance().externalsModules().value(ClientModule::TestPlugin);

    TestModule   *tmp_test_module = NULL;

    foreach(ClientModule *tmp_module, tmp_modules_map)
    {
        tmp_test_module = static_cast<TestModule*>(tmp_module);

        tmp_test_module->loadTestModuleDatas(m_test);

        QWidget     *tmp_module_view = tmp_test_module->createView(this);
        if (tmp_module_view != NULL) {
            m_views_modules_map[tmp_test_module] = tmp_module_view;
            m_ui->tabs->addTab(tmp_module_view, tmp_module->getModuleName());
        }
    }
}


void FormTest::savePluginsDatas()
{
    QMap<TestModule*, QWidget*>::iterator tmp_module_iterator;

    for (tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++) {
        tmp_module_iterator.key()->saveTestModuleDatas();
    }
}


void FormTest::destroyPluginsViews()
{
    QMap<TestModule*, QWidget*>::iterator tmp_module_iterator;

    for (tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++) {
        tmp_module_iterator.key()->destroyView(tmp_module_iterator.value());
    }
}


void FormTest::clipboardDataChanged()
{
    m_ui->button_paste->setEnabled(!QApplication::clipboard()->text().isEmpty());
    m_ui->button_paste_plain_text->setEnabled(!QApplication::clipboard()->text().isEmpty());
}

void FormTest::addAutomatedActionWindowHierarchyMenu(QMenu* in_menu, QList<AutomatedActionWindowHierarchy*> in_windows_list)
{
    foreach(AutomatedActionWindowHierarchy* tmp_window, in_windows_list)
    {
        //        if (compare_values(tmp_window->getValueForKey(AUTOMATED_ACTION_WINDOW_HIERARCHY_IS_VISIBLE), NO) == 0){
        const QList<AutomatedActionWindowHierarchy*>& tmp_child_windows_list = AutomatedActionWindowHierarchy::ParentAutomatedActionWindowHierarchyRelation::instance().getChilds(tmp_window);

        if (tmp_child_windows_list.isEmpty()) {
            in_menu->addAction(QString("%1/%2")
                               .arg(tmp_window->getValueForKey(AUTOMATED_ACTION_WINDOW_HIERARCHY_TEXT))
                               .arg(tmp_window->getValueForKey(AUTOMATED_ACTION_WINDOW_HIERARCHY_CLASS)));
        }
        else {
            QMenu* submenu = in_menu->addMenu(QString("%1/%2")
                                              .arg(tmp_window->getValueForKey(AUTOMATED_ACTION_WINDOW_HIERARCHY_TEXT))
                                              .arg(tmp_window->getValueForKey(AUTOMATED_ACTION_WINDOW_HIERARCHY_CLASS)));


            addAutomatedActionWindowHierarchyMenu(submenu, tmp_child_windows_list);
            //            }
        }
    }
}


void FormTest::showAutomatedActionValidationContextMenu(const QPoint & position)
{
#ifdef GUI_AUTOMATION_ACTIVATED
    QMenu *tmp_menu = NULL;
    QMenu *tmp_function_menu = NULL;
    QMenu*  tmp_module_menu = NULL;
    QMenu*  tmp_automated_action_menu = NULL;
    QAction *tmp_action = NULL;
    QModelIndex tmp_model_index = m_ui->automated_actions_list->indexAt(position);
    AutomatedAction*  tmp_automated_action = static_cast<AutomatedAction*>(tmp_model_index.internalPointer());
    AutomatedActionValidation* tmp_automated_action_validation = NULL;
    AutomationModule* tmp_automation_module = NULL;
    AutomationCallbackFunction* tmp_callback = NULL;

    QMap < QString, ClientModule*> tmp_modules_map = Session::instance().externalsModules().value(ClientModule::AutomationPlugin);

    tmp_menu = new QMenu();
    tmp_automated_action_menu = tmp_menu->addMenu(tr("Toutes les actions"));
    tmp_function_menu = tmp_automated_action_menu->addMenu(tr("Ajouter une fonction de validation"));
    foreach(ClientModule* tmp_module, tmp_modules_map.values())
    {
        tmp_automation_module = static_cast<AutomationModule*>(tmp_module);
        if (tmp_automation_module) {
            tmp_module_menu = tmp_function_menu->addMenu(tr("Module %1").arg(tmp_automation_module->getModuleName()));
            QMap<QString, AutomationCallbackFunction*> tmp_module_functions_list = tmp_automation_module->getFunctionsMap();
            foreach(tmp_callback, tmp_module_functions_list.values())
            {
                tmp_action = tmp_module_menu->addAction(tr("Fonction %1").arg(tmp_callback->getName()));
                tmp_action->setToolTip(QString("<p>%1</p>").arg(tmp_callback->getDescription()));
                connect(tmp_action, SIGNAL(hovered()), this, SLOT(showAutomatedActionValidationToolTip()));
                tmp_action->setData(QVariant::fromValue<void*>(tmp_callback));
                tmp_action->setProperty("type", ALL_ACTIONS_ADD_FUNCTION);
            }
            if (tmp_module_menu->isEmpty()) {
                tmp_action = tmp_module_menu->addAction(tr("Aucune fonction disponible"));
                tmp_action->setFont(QFont(tmp_action->font().family(), -1, -1, true));
                tmp_action->setProperty("type", ACTION_NONE);
            }
        }
    }
    if (tmp_function_menu->isEmpty()) {
        tmp_action = tmp_function_menu->addAction(tr("Aucun module disponible"));
        tmp_action->setFont(QFont(tmp_action->font().family(), -1, -1, true));
        tmp_action->setProperty("type", ACTION_NONE);
    }

    tmp_action = tmp_automated_action_menu->addAction(tr("Enlever toutes les fonctions de validation"));
    tmp_action->setProperty("type", ALL_ACTIONS_REMOVE_FUNCTION);



    if (tmp_automated_action) {
        QList<AutomatedActionValidation*> tmp_childs = AutomatedAction::AutomatedActionValidationRelation::instance().getChilds(tmp_automated_action);
        QList<AutomatedActionWindowHierarchy*> tmp_windows = AutomatedAction::AutomatedActionWindowHierarchyRelation::instance().getChilds(tmp_automated_action);
        int tmp_index = 0;

        tmp_automated_action_menu = tmp_menu->addMenu(tr("Action sélectionnée"));

        for (tmp_index = 0; tmp_index < tmp_childs.count(); ++tmp_index) {

            tmp_automated_action_validation = tmp_childs[tmp_index];
            tmp_function_menu = tmp_automated_action_menu->addMenu(tmp_automated_action_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME));

            if (tmp_index > 0) {
                tmp_action = tmp_function_menu->addAction(tr("Monter"));
                tmp_action->setProperty("record", QVariant::fromValue<void*>(tmp_automated_action_validation));
                tmp_action->setProperty("type", CURRENT_ACTION_MOVE_FUNCTION_UP);
            }

            if (tmp_index < tmp_childs.count() - 1) {
                tmp_action = tmp_function_menu->addAction(tr("Déscendre"));
                tmp_action->setProperty("record", QVariant::fromValue<void*>(tmp_automated_action_validation));
                tmp_action->setProperty("type", CURRENT_ACTION_MOVE_FUNCTION_DOWN);
            }

            tmp_function_menu->addSeparator();
            tmp_action = tmp_function_menu->addAction(tr("Enlever"));
            tmp_action->setProperty("record", QVariant::fromValue<void*>(tmp_automated_action_validation));
            tmp_action->setProperty("type", CURRENT_ACTION_REMOVE_FUNCTION);
        }

        if (!tmp_automated_action_menu->isEmpty())
            tmp_automated_action_menu->addSeparator();

        tmp_function_menu = tmp_automated_action_menu->addMenu(tr("Ajouter une fonction de validation"));
        foreach(ClientModule* tmp_module, tmp_modules_map.values())
        {
            tmp_automation_module = static_cast<AutomationModule*>(tmp_module);
            if (tmp_automation_module) {
                tmp_module_menu = tmp_function_menu->addMenu(tr("Module %1").arg(tmp_automation_module->getModuleName()));
                QMap<QString, AutomationCallbackFunction*> tmp_module_functions_list = tmp_automation_module->getFunctionsMap();
                foreach(tmp_callback, tmp_module_functions_list.values())
                {
                    tmp_action = tmp_module_menu->addAction(tr("Fonction %1").arg(tmp_callback->getName()));
                    tmp_action->setToolTip(QString("<p>%1</p>").arg(tmp_callback->getDescription()));
                    connect(tmp_action, SIGNAL(hovered()), this, SLOT(showAutomatedActionValidationToolTip()));
                    tmp_action->setData(QVariant::fromValue<void*>(tmp_callback));
                    tmp_action->setProperty("type", CURRENT_ACTION_ADD_FUNCTION);

                }
                if (tmp_module_menu->isEmpty()) {
                    tmp_action = tmp_module_menu->addAction(tr("Aucune fonction disponible"));
                    tmp_action->setFont(QFont(tmp_action->font().family(), -1, -1, true));
                    tmp_action->setProperty("type", ACTION_NONE);
                }
            }
        }
        if (tmp_function_menu->isEmpty()) {
            tmp_action = tmp_function_menu->addAction(tr("Aucun module disponible"));
            tmp_action->setFont(QFont(tmp_action->font().family(), -1, -1, true));
            tmp_action->setProperty("type", ACTION_NONE);
        }

        addAutomatedActionWindowHierarchyMenu(tmp_automated_action_menu, tmp_windows);

    }

    tmp_action = tmp_menu->exec(QCursor::pos());
    if (tmp_action) {
        tmp_automated_action_validation = static_cast<AutomatedActionValidation*>(tmp_action->property("record").value<void*>());
        if (tmp_automated_action_validation && tmp_automated_action) {
            int tmp_child_index = AutomatedAction::AutomatedActionValidationRelation::instance().getChilds(tmp_automated_action).indexOf(tmp_automated_action_validation);
            switch (tmp_action->property("type").toInt()) {
            case CURRENT_ACTION_REMOVE_FUNCTION:
                AutomatedAction::AutomatedActionValidationRelation::instance().removeChild(tmp_automated_action, tmp_automated_action_validation);
                break;
            case CURRENT_ACTION_MOVE_FUNCTION_UP:
                AutomatedAction::AutomatedActionValidationRelation::instance().moveChild(tmp_automated_action, tmp_child_index, tmp_child_index - 1);
                break;
            case CURRENT_ACTION_MOVE_FUNCTION_DOWN:
                AutomatedAction::AutomatedActionValidationRelation::instance().moveChild(tmp_automated_action, tmp_child_index, tmp_child_index + 1);
                break;
            default:
                break;
            }
        }
        else {
            switch (tmp_action->property("type").toInt()) {
            case CURRENT_ACTION_ADD_FUNCTION:
                tmp_callback = (AutomationCallbackFunction*)tmp_action->data().value<void*>();
                if (tmp_callback && tmp_automated_action) {
                    tmp_automated_action_validation = new AutomatedActionValidation();
                    tmp_automated_action_validation->setValueForKey(tmp_callback->getModule()->getModuleName().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME);
                    tmp_automated_action_validation->setValueForKey(tmp_callback->getModule()->getModuleVersion().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION);
                    tmp_automated_action_validation->setValueForKey(tmp_callback->getName().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME);
                    AutomatedAction::AutomatedActionValidationRelation::instance().appendChild(tmp_automated_action, tmp_automated_action_validation);
                }
                break;

            case ALL_ACTIONS_ADD_FUNCTION:

                foreach(tmp_automated_action, m_automated_actions_model->getRecordsList())
                {
                    tmp_automated_action_validation = new AutomatedActionValidation();
                    tmp_automated_action_validation->setValueForKey(tmp_callback->getModule()->getModuleName().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME);
                    tmp_automated_action_validation->setValueForKey(tmp_callback->getModule()->getModuleVersion().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION);
                    tmp_automated_action_validation->setValueForKey(tmp_callback->getName().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME);
                    AutomatedAction::AutomatedActionValidationRelation::instance().appendChild(tmp_automated_action, tmp_automated_action_validation);
                }
                break;

            case ALL_ACTIONS_REMOVE_FUNCTION:
                foreach(tmp_automated_action, m_automated_actions_model->getRecordsList())
                {
                    AutomatedAction::AutomatedActionValidationRelation::instance().removeAllChilds(tmp_automated_action);
                }

                break;
            }

        }
    }

    delete tmp_menu;

    m_ui->automated_actions_list->resizeRowsToContents();

#endif
}


void FormTest::showAutomatedActionValidationToolTip()
{
    QToolTip::showText(QCursor::pos(), dynamic_cast<QAction*>(sender())->toolTip());
}


void FormTest::automatedActionsRecorded(QList<AutomatedAction*> actions)
{
    m_ui->button_start_record->setEnabled(true);
    m_ui->button_stop_record->setEnabled(false);

    foreach(AutomatedAction* action, actions) {
        TestContent::AutomatedActionRelation::instance().appendChild(m_test_content, action);
    }

    m_automated_actions_model->setRecordsList(actions, true);
    m_ui->automated_actions_list->resizeColumnsToContents();
    m_ui->button_start_playback->setEnabled(m_automated_actions_model->rowCount() > 0);
}


void FormTest::stopBatchProcess(int in_exit_code)
{
    m_ui->batch_process_stdout->appendPlainText("----------------------------------------------------------------------------------------------------------------------");
    m_ui->batch_process_stdout->appendPlainText(tr("Arrêt du processus (code retour=%1)").arg(in_exit_code));
    m_ui->batch_process_stdout->appendPlainText("----------------------------------------------------------------------------------------------------------------------");
}


void FormTest::readBatchProcessStandardOutput(const QString& output)
{
    m_ui->batch_process_stdout->appendPlainText(output);
}



void FormTest::readBatchProcessStandardError(const QString& output)
{
    m_ui->batch_process_stdout->appendHtml(QString("<font color=\"red\">%1</font>").arg(output));
}


void FormTest::changeAutomatedTestType()
{
    setModified();
    if (m_ui->radioButtonGui->isChecked()) {
        m_test_content->setValueForKey(TEST_CONTENT_TABLE_AUTOMATED_GUI, TESTS_CONTENTS_TABLE_AUTOMATED);
    }
    else if (m_ui->radioButtonBatch->isChecked()) {
        m_test_content->setValueForKey(TEST_CONTENT_TABLE_AUTOMATED_BATCH, TESTS_CONTENTS_TABLE_AUTOMATED);
    }
    updateTabsWidget();
}


void FormTest::updateTabsWidget()
{
    int startIndex = m_ui->tabs->indexOf(m_ui->requiremets) + 1;

    if (m_test_content->isAutomatedGuiTest()) {
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->actions));
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->batch));

        if (m_ui->tabs->indexOf(m_ui->automation) < 0)
            m_ui->tabs->insertTab(startIndex, m_ui->automation, tr("Automatisation"));

        if (m_ui->tabs->indexOf(m_ui->automated_actions) < 0)
            m_ui->tabs->insertTab(startIndex + 1, m_ui->automated_actions, tr("Actions automatisées"));
    }
    else if (m_test_content->isAutomatedBatchTest()) {
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->actions));
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->automated_actions));

        if (m_ui->tabs->indexOf(m_ui->automation) < 0)
            m_ui->tabs->insertTab(startIndex, m_ui->automation, tr("Automatisation"));

        if (m_ui->tabs->indexOf(m_ui->batch) < 0)
            m_ui->tabs->insertTab(startIndex + 1, m_ui->batch, tr("Batch"));
    }
    else {
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->automation));
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->batch));
        m_ui->tabs->removeTab(m_ui->tabs->indexOf(m_ui->automated_actions));

        if (m_ui->tabs->indexOf(m_ui->actions) < 0)
            m_ui->tabs->insertTab(startIndex, m_ui->actions, tr("Actions"));
    }

    if (m_ui->test_name->text().isEmpty())
        m_ui->tabs->setCurrentWidget(m_ui->details);
    else if (m_test_content->isAutomatedGuiTest())
        if (m_ui->automation_command_line->text().isEmpty())
            m_ui->tabs->setCurrentWidget(m_ui->automation);
        else
            m_ui->tabs->setCurrentWidget(m_ui->automated_actions);
    else if (m_test_content->isAutomatedBatchTest())
        if (m_ui->automation_command_line->text().isEmpty())
            m_ui->tabs->setCurrentWidget(m_ui->automation);
        else
            m_ui->tabs->setCurrentWidget(m_ui->batch);
    else
        m_ui->tabs->setCurrentWidget(m_ui->actions);

}