/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_PROJECT_VERSION_H
#define FORM_PROJECT_VERSION_H

#include "entities/project.h"
#include "entities/projectversion.h"
#include "entities/projectgrant.h"

#include "gui/components/RecordTextEditContainer.h"
#include "gui/components/AbstractProjectWidget.h"

#include <bugtracker.h>
#include <clientmodule.h>

#include <QtWidgets/QWidget>

QT_FORWARD_DECLARE_CLASS(Ui_FormProjectVersion)

class FormProjectVersion : public AbstractProjectWidget
{
    Q_OBJECT

public:
    FormProjectVersion(ProjectVersion* in_project_record = NULL, QWidget *parent = 0);
    ~FormProjectVersion();

    QIcon icon() const;
    QString title() const;

protected:
    bool maybeClose();
    bool saveProject();
    void initLayout();

    public Q_SLOTS:
    void save();

    void checkBugtrackerConnexion();

    void getBugtrackerVersion(QString version);
    void getBugtrackerError(QString errorMsg);

private:
    Ui_FormProjectVersion      *m_ui;
    ProjectVersion               *m_project_record;

    Bugtracker                    *m_bugtracker;

    void updateControls();

    QMap<ProjectModule*, QWidget*>   m_views_modules_map;

    void loadPluginsViews();
    void destroyPluginsViews();
    void savePluginsDatas();
};

#endif // FORM_PROJECT_VERSION_H
