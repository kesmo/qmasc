/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_PROJECT_SELECTION_H
#define FORM_PROJECT_SELECTION_H

#include <QtWidgets/QDialog>
#include "entities/project.h"
#include "entities/projectversion.h"
#include "ui_FormProjectSelection.h"

QT_FORWARD_DECLARE_CLASS(Ui_FormProjectSelection)

class FormProjectSelection : public QDialog
{
    Q_OBJECT

public:
    FormProjectSelection(QWidget *parent = 0);
    ~FormProjectSelection();

signals:
    void projectSelected(const QString &projectId, const QString &projectVersionId);

public slots:
    void accept();
    void newVersion();
    void selectedProjectChanged(int);
    void selectedVersionChanged(int);
    void selectVersion(ProjectVersion *in_project_version);

    void deleteProject();
    void deleteProjectVersion();

private:
    Ui_FormProjectSelection   *m_ui;
    QList<Project*>             m_projects_list;
    QList<ProjectVersion*>      m_versions_list;
};

#endif // FORM_PROJECT_SELECTION_H
