#include "FormOptions.h"
#include "session.h"
#include "entities/executiontest.h"

#include "../client-launcher/utils/ProcessUtils.h"

#include <QColorDialog>
#include <QProcess>
#include <QNetworkProxy>
#include <QMessageBox>

FormOptions::FormOptions(QWidget *parent) :
    QDialog(parent)
{
    m_ui = new Ui_FormOptions;

    m_ui->setupUi(this);
    m_ui->modules_widget->setAttribute(Qt::WA_TranslucentBackground);
    m_ui->no_modules_widget->setAttribute(Qt::WA_TranslucentBackground);

    connect(m_ui->clear_prefs_button, SIGNAL(clicked()), this, SLOT(clearPreferences()));

    connect(m_ui->bugtracker_type, SIGNAL(currentIndexChanged(int)), this, SLOT(updateBugtrackerView()));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    connect(m_ui->proxy_none, SIGNAL(clicked()), this, SLOT(selectedProxyTypeChanged()));
    connect(m_ui->proxy_socks5, SIGNAL(clicked()), this, SLOT(selectedProxyTypeChanged()));
    connect(m_ui->proxy_http, SIGNAL(clicked()), this, SLOT(selectedProxyTypeChanged()));
    connect(m_ui->proxy_http_only, SIGNAL(clicked()), this, SLOT(selectedProxyTypeChanged()));
    connect(m_ui->proxy_ftp_only, SIGNAL(clicked()), this, SLOT(selectedProxyTypeChanged()));

    connect(m_ui->check_new_versions_button, SIGNAL(clicked()), this, SLOT(checkNewVersions()));

    /* Paramètres de couleur */
    connect(m_ui->test_execution_ok_button, SIGNAL(clicked()), this, SLOT(selectColorForExecutionTestOk()));
    connect(m_ui->test_execution_ko_button, SIGNAL(clicked()), this, SLOT(selectColorForExecutionTestKo()));
    connect(m_ui->test_execution_incompleted_button, SIGNAL(clicked()), this, SLOT(selectColorForExecutionTestIncompleted()));
    connect(m_ui->test_execution_by_passed_button, SIGNAL(clicked()), this, SLOT(selectColorForExecutionTestBypassed()));

    initPrefsTab();
    readSettings();
}

FormOptions::~FormOptions()
{
    delete m_ui;
}

void FormOptions::readSettings()
{
    QSettings tmp_settings;
    int tmp_proxy_type = QNetworkProxy::NoProxy;
    int tmp_proxy_capabilities = 0;
    bool tmp_proxy_use_system_config = tmp_settings.value("proxy_use_system_config", QVariant(false)).toBool();

    m_ui->check_new_versions->setChecked(tmp_settings.value("versions_check_new", QVariant(false)).toBool());
    m_ui->versions_url->setText(tmp_settings.value("versions_url", "http://rtmr.net").toString());
    m_ui->proxy->setText(tmp_settings.value("proxy_address", "").toString());
    m_ui->port->setText(tmp_settings.value("proxy_port", "").toString());
    m_ui->login->setText(tmp_settings.value("proxy_login", "").toString());
    m_ui->password->setText(tmp_settings.value("proxy_password", "").toString());

    if (!tmp_proxy_use_system_config) {
        tmp_proxy_type = tmp_settings.value("proxy_type", QVariant(QNetworkProxy::NoProxy)).toInt();
        switch (tmp_proxy_type) {
        case QNetworkProxy::Socks5Proxy:
            m_ui->proxy_socks5->setChecked(true);
            break;

        case QNetworkProxy::HttpProxy:
            m_ui->proxy_http->setChecked(true);
            break;

        case QNetworkProxy::HttpCachingProxy:
            m_ui->proxy_http_only->setChecked(true);
            break;

        case QNetworkProxy::FtpCachingProxy:
            m_ui->proxy_ftp_only->setChecked(true);
            break;

        default:
            m_ui->proxy_none->setChecked(true);
            break;
        }
    }

    tmp_proxy_capabilities = tmp_settings.value("proxy_capabilities", QVariant(0)).toInt();
    m_ui->proxy_tunneling->setChecked((tmp_proxy_capabilities & QNetworkProxy::TunnelingCapability)
                                       == QNetworkProxy::TunnelingCapability);
    m_ui->proxy_listening->setChecked((tmp_proxy_capabilities & QNetworkProxy::ListeningCapability)
                                       == QNetworkProxy::ListeningCapability);
    m_ui->proxy_udptunneling->setChecked((tmp_proxy_capabilities & QNetworkProxy::UdpTunnelingCapability)
                                          == QNetworkProxy::UdpTunnelingCapability);
    m_ui->proxy_caching->setChecked((tmp_proxy_capabilities & QNetworkProxy::CachingCapability)
                                     == QNetworkProxy::CachingCapability);
    m_ui->proxy_lookup->setChecked((tmp_proxy_capabilities & QNetworkProxy::HostNameLookupCapability)
                                    == QNetworkProxy::HostNameLookupCapability);

    /* Couleurs */
    m_test_execution_ok_color = ExecutionTest::okColor();
    m_test_execution_ko_color = ExecutionTest::koColor();
    m_test_execution_incompleted_color = ExecutionTest::incompleteColor();
    m_test_execution_by_passed_color = ExecutionTest::byPassedColor();

    setButtonColor(m_test_execution_ok_color, m_ui->test_execution_ok_button);
    setButtonColor(m_test_execution_ko_color, m_ui->test_execution_ko_button);
    setButtonColor(m_test_execution_incompleted_color, m_ui->test_execution_incompleted_button);
    setButtonColor(m_test_execution_by_passed_color, m_ui->test_execution_by_passed_button);

    /* Bugtrackers */
    QList<ClientModule*> tmp_externals_modules = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).values();

    m_ui->modules_widget->setVisible(!tmp_externals_modules.isEmpty());
    m_ui->no_modules_widget->setVisible(tmp_externals_modules.isEmpty());

    foreach(ClientModule *tmp_module, tmp_externals_modules)
    {
        QString tmp_module_name = tmp_module->getModuleName();

        m_ui->bugtracker_type->addItem(tmp_module_name);
        m_bugtrackers_credentials[tmp_module_name] = Session::instance().getBugtrackersCredentials()[tmp_module_name];
    }
    m_ui->bugtracker_type->setCurrentIndex(0);
    m_current_bugtracker_name = "";

    updateBugtrackerView();

    /* Onglet serveur */
    m_ui->server_ping_interval->setValue(tmp_settings.value("server_keepalive_interval", 10).toInt());
    m_ui->server_recv_timeout->setValue(tmp_settings.value("server_recv_timeout", 30).toInt());
    m_ui->server_send_timeout->setValue(tmp_settings.value("server_send_timeout", 30).toInt());


}

void FormOptions::accept()
{
    QSettings tmp_settings;
    QNetworkProxy tmp_proxy;
    QNetworkProxy::ProxyType tmp_proxy_type = QNetworkProxy::NoProxy;
    QFlags<QNetworkProxy::Capability> tmp_proxy_capabilities = 0;

    tmp_settings.setValue("versions_check_new", m_ui->check_new_versions->isChecked());

    tmp_settings.setValue("versions_url", m_ui->versions_url->text());

    tmp_settings.setValue("proxy_use_system_config", m_ui->proxy_system_config->isChecked());

    tmp_settings.setValue("proxy_address", m_ui->proxy->text());
    tmp_settings.setValue("proxy_port", m_ui->port->text());
    tmp_settings.setValue("proxy_login", m_ui->login->text());
    tmp_settings.setValue("proxy_password", m_ui->password->text());

    if (m_ui->proxy_system_config->isChecked()) {
        QNetworkProxyFactory::setUseSystemConfiguration(true);
    }
    else {
        QNetworkProxyFactory::setUseSystemConfiguration(false);
        if (m_ui->proxy->text().isEmpty()) {
            tmp_proxy_type = QNetworkProxy::NoProxy;
        }
        else {
            if (m_ui->proxy_none->isChecked())
                tmp_proxy_type = QNetworkProxy::NoProxy;
            else if (m_ui->proxy_socks5->isChecked())
                tmp_proxy_type = QNetworkProxy::Socks5Proxy;
            else if (m_ui->proxy_http->isChecked())
                tmp_proxy_type = QNetworkProxy::HttpProxy;
            else if (m_ui->proxy_http_only->isChecked())
                tmp_proxy_type = QNetworkProxy::HttpCachingProxy;
            else if (m_ui->proxy_ftp_only->isChecked())
                tmp_proxy_type = QNetworkProxy::FtpCachingProxy;

            if (m_ui->proxy_tunneling->isChecked())
                tmp_proxy_capabilities = QNetworkProxy::TunnelingCapability;

            if (m_ui->proxy_listening->isChecked())
                tmp_proxy_capabilities = tmp_proxy_capabilities | QNetworkProxy::ListeningCapability;

            if (m_ui->proxy_udptunneling->isChecked())
                tmp_proxy_capabilities = tmp_proxy_capabilities | QNetworkProxy::UdpTunnelingCapability;

            if (m_ui->proxy_caching->isChecked())
                tmp_proxy_capabilities = tmp_proxy_capabilities | QNetworkProxy::CachingCapability;

            if (m_ui->proxy_lookup->isChecked())
                tmp_proxy_capabilities = tmp_proxy_capabilities | QNetworkProxy::HostNameLookupCapability;

            tmp_proxy.setCapabilities(tmp_proxy_capabilities);
            tmp_proxy.setHostName(m_ui->proxy->text());
            tmp_proxy.setPort(m_ui->port->text().toInt());
            if (!m_ui->login->text().isEmpty()) {
                tmp_proxy.setUser(m_ui->login->text());
                tmp_proxy.setPassword(m_ui->password->text());
            }
        }

        tmp_settings.setValue("proxy_type", QVariant(tmp_proxy_type));
        tmp_settings.setValue("proxy_capabilities", QVariant(tmp_proxy_capabilities));

        tmp_proxy.setType(tmp_proxy_type);
        QNetworkProxy::setApplicationProxy(tmp_proxy);
    }

    /* Couleurs */
    tmp_settings.setValue("test_execution_ok_color", m_test_execution_ok_color);
    tmp_settings.setValue("test_execution_ko_color", m_test_execution_ko_color);
    tmp_settings.setValue("test_execution_incompleted_color", m_test_execution_incompleted_color);
    tmp_settings.setValue("test_execution_by_passed_color", m_test_execution_by_passed_color);

    ExecutionTest::initDefaultColors(
        m_test_execution_ok_color,
        m_test_execution_ko_color,
        m_test_execution_incompleted_color,
        m_test_execution_by_passed_color);

    /* Bugtrackers */
    updateBugtrackerView();

    foreach(ClientModule *tmp_module, Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).values())
    {
        QString tmp_module_name = tmp_module->getModuleName();
        QString tmp_setting_prefix = tmp_module_name.toLower().trimmed();

        Session::instance().setBugtrackerCredential(tmp_module_name, m_bugtrackers_credentials[tmp_module_name]);

        tmp_settings.setValue(tmp_setting_prefix + "_login", m_bugtrackers_credentials[tmp_module_name].first);
        tmp_settings.setValue(tmp_setting_prefix + "_password", encrypt_str(m_bugtrackers_credentials[tmp_module_name].second.toStdString().c_str()));
    }

    /* Onglet serveur */
    tmp_settings.setValue("server_keepalive_interval", m_ui->server_ping_interval->value());
    tmp_settings.setValue("server_recv_timeout", m_ui->server_recv_timeout->value());
    tmp_settings.setValue("server_send_timeout", m_ui->server_send_timeout->value());

    QDialog::accept();
}

void FormOptions::selectedProxyTypeChanged()
{
    QNetworkProxy tmp_proxy;
    QNetworkProxy::ProxyType tmp_proxy_type = QNetworkProxy::NoProxy;
    QFlags<QNetworkProxy::Capability> tmp_proxy_capabilities = 0;

    if (m_ui->proxy_none->isChecked())
        tmp_proxy_type = QNetworkProxy::NoProxy;
    else if (m_ui->proxy_socks5->isChecked())
        tmp_proxy_type = QNetworkProxy::Socks5Proxy;
    else if (m_ui->proxy_http->isChecked())
        tmp_proxy_type = QNetworkProxy::HttpProxy;
    else if (m_ui->proxy_http_only->isChecked())
        tmp_proxy_type = QNetworkProxy::HttpCachingProxy;
    else if (m_ui->proxy_ftp_only->isChecked())
        tmp_proxy_type = QNetworkProxy::FtpCachingProxy;

    tmp_proxy.setType(tmp_proxy_type);

    tmp_proxy_capabilities = tmp_proxy.capabilities();
    m_ui->proxy_tunneling->setChecked((tmp_proxy_capabilities & QNetworkProxy::TunnelingCapability)
                                       == QNetworkProxy::TunnelingCapability);
    m_ui->proxy_listening->setChecked((tmp_proxy_capabilities & QNetworkProxy::ListeningCapability)
                                       == QNetworkProxy::ListeningCapability);
    m_ui->proxy_udptunneling->setChecked((tmp_proxy_capabilities & QNetworkProxy::UdpTunnelingCapability)
                                          == QNetworkProxy::UdpTunnelingCapability);
    m_ui->proxy_caching->setChecked((tmp_proxy_capabilities & QNetworkProxy::CachingCapability)
                                     == QNetworkProxy::CachingCapability);
    m_ui->proxy_lookup->setChecked((tmp_proxy_capabilities & QNetworkProxy::HostNameLookupCapability)
                                    == QNetworkProxy::HostNameLookupCapability);
}

void FormOptions::checkNewVersions()
{
    QProcess tmp_client_launcher;
    QString tmp_app_path = QApplication::applicationDirPath() + "/";

#if defined(__WINDOWS) ||  defined(WIN32)
    tmp_client_launcher.startDetached("\"" + tmp_app_path + "rtmr.exe\" -check -url " + m_ui->versions_url->text());
#else
#ifdef __APPLE__
    tmp_client_launcher.startDetached(tmp_app_path + "rtmr -check -url " + m_ui->versions_url->text());
    QTimer::singleShot(1000, this, SLOT(bringClientLauncherToFront()));
#else
#if defined __linux__
    tmp_client_launcher.startDetached(tmp_app_path + "rtmr -check -url " + m_ui->versions_url->text());
#endif // __linux__
#endif // __APPLE__
#endif //__WINDOWS

}


void FormOptions::bringClientLauncherToFront()
{
    ProcessUtils::isRunning("rtmr", true);
}

void FormOptions::selectColorForExecutionTestOk()
{
    pickColor(m_test_execution_ok_color, m_ui->test_execution_ok_button);
}


void FormOptions::selectColorForExecutionTestKo()
{
    pickColor(m_test_execution_ko_color, m_ui->test_execution_ko_button);
}


void FormOptions::selectColorForExecutionTestIncompleted()
{
    pickColor(m_test_execution_incompleted_color, m_ui->test_execution_incompleted_button);
}


void FormOptions::selectColorForExecutionTestBypassed()
{
    pickColor(m_test_execution_by_passed_color, m_ui->test_execution_by_passed_button);
}


void FormOptions::pickColor(QColor & inColor, QPushButton *inButton)
{
    QColor tmpColor = QColorDialog::getColor(inColor, this);

    if (tmpColor.isValid()) {
        inColor = tmpColor;
        setButtonColor(inColor, inButton);
    }
}


void FormOptions::setButtonColor(const QColor & inColor, QPushButton *inButton)
{
    QPixmap tmpPix = QPixmap(24, 24);

    tmpPix.fill(inColor);
    inButton->setIcon(QIcon(tmpPix));
}



void FormOptions::updateBugtrackerView()
{
    QString     tmp_bugtracker_name = m_ui->bugtracker_type->currentText();

    if (!m_current_bugtracker_name.isEmpty()) {
        m_bugtrackers_credentials[m_current_bugtracker_name] = QPair<QString, QString>(m_ui->bugtracker_login->text(), m_ui->bugtracker_password->text());
    }

    if (!tmp_bugtracker_name.isEmpty()) {
        m_ui->bugtracker_groupbox->setTitle(tmp_bugtracker_name);

        m_ui->bugtracker_login->setText(m_bugtrackers_credentials[tmp_bugtracker_name].first);
        m_ui->bugtracker_password->setText(m_bugtrackers_credentials[tmp_bugtracker_name].second);

        m_current_bugtracker_name = tmp_bugtracker_name;
    }
}


void FormOptions::initPrefsTab()
{
    QSettings tmp_settings;
    QStringList tmp_prefs_list = tmp_settings.allKeys();

    QSettings tmp_options_settings;
    QStringList tmp_options_prefs_list = tmp_options_settings.allKeys();
    QStringList tmp_headers;

    QTableWidgetItem        *tmp_widget_item = NULL;

    int     tmp_index = 0;

    tmp_headers << tr("Nom") << tr("Valeur") << tr("Groupe");

    m_ui->user_prefs_list->clear();
    m_ui->user_prefs_list->setRowCount(tmp_prefs_list.count() + tmp_options_prefs_list.count());
    m_ui->user_prefs_list->setColumnCount(3);

    m_ui->user_prefs_list->setHorizontalHeaderLabels(tmp_headers);

    m_ui->user_prefs_list->setEditTriggers(QAbstractItemView::NoEditTriggers);

    foreach(QString tmp_pref, tmp_prefs_list)
    {
        tmp_widget_item = new QTableWidgetItem;
        tmp_widget_item->setText(tmp_pref);
        m_ui->user_prefs_list->setItem(tmp_index, 0, tmp_widget_item);

        tmp_widget_item = new QTableWidgetItem;
        tmp_widget_item->setText(tmp_settings.value(tmp_pref, "").toString());
        m_ui->user_prefs_list->setItem(tmp_index, 1, tmp_widget_item);

        tmp_widget_item = new QTableWidgetItem;
        tmp_widget_item->setText("");
        m_ui->user_prefs_list->setItem(tmp_index, 2, tmp_widget_item);

        tmp_index++;
    }


    foreach(QString tmp_pref, tmp_options_prefs_list)
    {
        tmp_widget_item = new QTableWidgetItem;
        tmp_widget_item->setText(tmp_pref);
        m_ui->user_prefs_list->setItem(tmp_index, 0, tmp_widget_item);

        tmp_widget_item = new QTableWidgetItem;
        tmp_widget_item->setText(tmp_options_settings.value(tmp_pref, "").toString());
        m_ui->user_prefs_list->setItem(tmp_index, 1, tmp_widget_item);

        tmp_widget_item = new QTableWidgetItem;
        tmp_widget_item->setText(tr("options"));
        m_ui->user_prefs_list->setItem(tmp_index, 2, tmp_widget_item);

        tmp_index++;
    }

    m_ui->user_prefs_list->resizeColumnsToContents();
}



void FormOptions::clearPreferences()
{
    QSettings tmp_settings;

    if (QMessageBox::question(this, tr("Confirmation..."), tr("Etes-vous sûr(e) de vouloir supprimer les préférences utilisateur ?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
        tmp_settings.clear();
    }

    initPrefsTab();
}
