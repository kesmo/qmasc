/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_CAMPAIGN_WIZARD_H
#define FORM_CAMPAIGN_WIZARD_H

#include <QtWidgets/QWizard>
#include "entities/projectversion.h"
#include "entities/campaign.h"
#include "gui/components/RecordTextEditContainer.h"

namespace Ui {
    class FormCampaign_Wizard;
}

class FormCampaign_Wizard : public QWizard {
    Q_OBJECT
public:
    FormCampaign_Wizard(ProjectVersion *in_project, QWidget *parent = 0);
    ~FormCampaign_Wizard();

signals:
    void campaignCreated(Campaign *in_new_campaign);

public slots:
    void accept();
    void campaignShortNameChanged();
    void updateControls();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::FormCampaign_Wizard    *m_ui;

    ProjectVersion           *m_project;
};

#endif // FORM_CAMPAIGN_WIZARD_H
