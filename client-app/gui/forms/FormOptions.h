#ifndef FORM_OPTIONS_H
#define FORM_OPTIONS_H

#include <QtWidgets/QDialog>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QAuthenticator>

#include "ui_FormOptions.h"

QT_FORWARD_DECLARE_CLASS(FormOptions);

class FormOptions : public QDialog
{
    Q_OBJECT

public:
    FormOptions(QWidget *parent = 0);
    ~FormOptions();

public Q_SLOTS:
    void accept();
    void selectedProxyTypeChanged();

    void checkNewVersions();

    void selectColorForExecutionTestOk();
    void selectColorForExecutionTestKo();
    void selectColorForExecutionTestIncompleted();
    void selectColorForExecutionTestBypassed();
    void pickColor(QColor & inColor, QPushButton *inButton);

    void updateBugtrackerView();

    void clearPreferences();

private:
    Ui_FormOptions *m_ui;

    QColor                  m_test_execution_ok_color;
    QColor                  m_test_execution_ko_color;
    QColor                  m_test_execution_incompleted_color;
    QColor                  m_test_execution_by_passed_color;

    QMap<QString, QPair<QString, QString> > m_bugtrackers_credentials;

    QString     m_current_bugtracker_name;

    void setButtonColor(const QColor & inColor, QPushButton *inButton);

    void readSettings();
    void initPrefsTab();

private slots:
    void bringClientLauncherToFront();

};

#endif // FORM_OPTIONS_H
