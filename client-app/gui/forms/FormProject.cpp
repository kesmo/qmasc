#include "FormProject.h"
#include "ui_FormProject.h"

#include "session.h"

#include <QMessageBox>
#include <QPushButton>
#include <QDialogButtonBox>

#define QLINEEDIT_WRITABLE "QLineEdit {background: #FFFFFF;border: 1px solid #4080b2;}"
#define QLINEEDIT_UNWRITABLE "QLineEdit {background: #FFFFFF;dfe0e8: color: gray;}"


FormProject::FormProject(Project *in_project, QWidget *parent) :
    AbstractProjectWidget(in_project, parent),
    m_ui(new Ui::FormProject),
    m_project(in_project)
{
    m_ui->setupUi(this);

    setWindowTitle(title());
    m_ui->project_description->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->project_name->setFocus();

    m_ui->project_name->setText(m_project->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
    m_ui->project_description->textEditor()->setHtml(m_project->getValueForKey(PROJECTS_TABLE_DESCRIPTION));

    if (!m_project->isModifiable())
    {
        setModifiable(false);
    }
    else
    {
        m_project->lockRecord(true);
        if (m_project->lockRecordStatus() == RECORD_STATUS_LOCKED)
        {
            m_ui->lock_widget->setVisible(true);
            net_get_field(NET_MESSAGE_TYPE_INDEX+1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
            m_ui->label_lock_by->setText(tr("Verrouillé par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            setModifiable(false);
        }
        else
            setModifiable(true);
    }

    connect(m_ui->project_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->project_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    updateControls();
}

FormProject::~FormProject()
{
    delete m_ui;
}

QIcon FormProject::icon() const
{
    return QPixmap(":/images/22x22/project.png");
}

QString FormProject::title() const
{
    if (m_project)
        return m_project->getValueForKey(PROJECTS_TABLE_SHORT_NAME);

    return QString();
}


bool FormProject::saveRecord()
{
    int     tmp_save_result = NOERR;

    m_project->setValueForKey(m_ui->project_name->text().toStdString().c_str(), PROJECTS_TABLE_SHORT_NAME);
    m_project->setValueForKey(m_ui->project_description->textEditor()->toOptimizedHtml().toStdString().c_str(), PROJECTS_TABLE_DESCRIPTION);
    tmp_save_result = m_project->saveRecord();
    if (tmp_save_result == NOERR)
    {
        setModified(false);
        updateControls();
        emit recordSaved(m_project);
        return true;
    }

    QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
    return false;
}

void FormProject::updateControls()
{
    m_ui->lock_widget->setVisible(m_project->lockRecordStatus() == RECORD_STATUS_LOCKED);
    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

    m_ui->project_name->setReadOnly(!isModifiable());
    m_ui->project_description->textEditor()->setReadOnly(!isModifiable());

    if (isModifiable() == false)
    {
        m_ui->project_description->toolBar()->hide();
    }
    else
    {
        m_ui->project_description->toolBar()->show();
    }
}



bool FormProject::maybeClose()
{
    int tmp_confirm_choice = 0;
    bool    tmp_return = true;

    if (m_project != NULL)
    {
        if (isModified())
        {
            tmp_confirm_choice = QMessageBox::question(
                        this,
                        tr("Confirmation..."),
                        tr("Le projet a été modifié. Voulez-vous enregistrer les modifications ?"),
                        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                        QMessageBox::Cancel);

            if (tmp_confirm_choice == QMessageBox::Yes)
                tmp_return = saveRecord();
            else if (tmp_confirm_choice == QMessageBox::Cancel)
                tmp_return = false;
        }

        if (tmp_return)
            m_project->unlockRecord();
    }

    return tmp_return;
}

