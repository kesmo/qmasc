/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_REQUIREMENT_H
#define FORM_REQUIREMENT_H

#include <QtWidgets/QDialog>
#include "entities/requirement.h"
#include "entities/requirementcontent.h"
#include "entities/customrequirementfield.h"

#include "gui/components/RecordTextEditContainer.h"
#include "gui/components/AbstractProjectWidget.h"

#include "entities/testrequirement.h"
#include "imaging/Chart.h"

#include "gui/components/CustomFieldsControlsManager.h"

#include <clientmodule.h>

class Ui_FormRequirement;

class FormRequirement :
    public AbstractProjectWidget,
    public CustomFieldsControlsManager
{
    Q_OBJECT
    Q_DISABLE_COPY(FormRequirement)
public:
    explicit FormRequirement(Requirement *in_requirement, QWidget *parent = 0);
    virtual ~FormRequirement();

    QIcon icon() const;
    QString title() const;

signals:
    void showTestWithContentId(ProjectVersion *in_project_version, const char *in_test_content_id);

public slots:
    void loadPreviousRequirementContent();
    void loadNextRequirementContent();
    void save();
    void cancel();

    void testsDrop(QList<Record*> in_list, int in_row);
    void deletedTestAtIndex(int in_row);
    void showTestAtIndex(QModelIndex in_index);

protected:
    bool saveRequirement();
    bool maybeClose();
    void setTestForRow(TestRequirement *in_test, int in_row);

    void loadRequirement(Requirement *in_requirement);
    void loadRequirementContent(RequirementContent *in_requirement_content);

private:
    Requirement          *m_requirement;
    RequirementContent   *m_requirement_content;
    RequirementContent   *m_previous_requirement_content;
    RequirementContent   *m_next_requirement_content;

    Ui_FormRequirement *m_ui;

    Chart                    *m_chart;
    QList<Trinome*>         m_chart_data;

    void initGraph(RequirementContent *in_requirement_content);

    QMap<RequirementModule*, QWidget*>   m_views_modules_map;

    QList<CustomRequirementField*>        m_custom_requirements_fields;

    void loadPluginsViews();
    void destroyPluginsViews();
    void savePluginsDatas();

};

#endif // FORM_REQUIREMENT_H
