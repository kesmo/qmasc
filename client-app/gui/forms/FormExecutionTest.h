/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_EXECUTION_TEST_H
#define FORM_EXECUTION_TEST_H

#include <QtWidgets/QWidget>

#include "gui/components/hierarchies/executiontesthierarchy.h"
#include "gui/components/RecordTextEditContainer.h"
#include "gui/components/TestActionAttachmentsManager.h"

#include "gui/components/RecordsTableModel.h"
#include "automation/GuiAutomationPlaybackProcessor.h"
#include "automation/BatchAutomationProcessor.h"

#ifdef GUI_AUTOMATION_ACTIVATED
    #include "automation.h"
#endif

namespace Ui {
    class FormExecutionTest;
}

class FormExecutionTest : public QWidget, public TestActionAttachmentsManager
{
    Q_OBJECT
    Q_DISABLE_COPY(FormExecutionTest)
public:
    explicit FormExecutionTest(QWidget *parent = 0);
    virtual ~FormExecutionTest();

    void loadTest(ExecutionTest *in_execution_test, ExecutionAction *in_execution_action);

public Q_SLOTS:
    void modifyTest();
    void saveComments();
    void saveTestDescription();
    void validateResult();
    void inValidateResult();
    void bypass();
    void selectNext();
    void selectPrevious();
    void manageBugs();
    void initManageBugsButtonLabel();
    void modifyAction();
    void reinitExecutionsTestsParameters();

    void launchBatch();
    void stopBatchProcess(int in_exit_code = 0);
    void stopGuiProcess(int in_exit_code = 0);

    void launchStartPlaybackSystemEvents();
    void startPlaybackSystemEvents();
    void stopPlaybackSystemEvents();

    void showProcessError(const QString& errorString);
    void fillStandardOutput(const QString& output);
    void fillStandardError(const QString& error);

Q_SIGNALS:
    void resultValidated();
    void resultInValidated();
    void bypassed();
        void nextSelected();
        void previousSelected();

protected:
    virtual void changeEvent(QEvent *e);
    void initLayout();

    void setExecutionTestData(const char *in_result_id);
    void setExecutionActionData(const char *in_result_id);

private:
    Ui::FormExecutionTest *m_ui;

    ExecutionTest                               *m_execution_test;
    ExecutionAction                             *m_execution_action;

    TestContent                                 *m_test_content;
    RecordsTableModel<AutomatedAction>          *m_automated_actions_model;

    RecordsTableModel<ExecutionTestParameter>   *m_parameters_model;

    QList<QString>                              m_current_parameters;

    bool                                        m_is_automated_program_playbacking;

    void saveOriginalAction();
    void synchronizeExecutionsTestsParameters(QList<QString> in_parameters_list, bool ask_user = true);

    GuiAutomationPlaybackProcessor              m_gui_automation_processor;
    BatchAutomationProcessor                    m_batch_automation_processor;

};

#endif // FORM_EXECUTION_TEST_H
