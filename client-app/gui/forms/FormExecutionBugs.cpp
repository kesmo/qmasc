/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormExecutionBugs.h"
#include "ui_FormExecutionBugs.h"
#include "FormBug.h"

#include "entities/test.h"
#include "session.h"

#include <QMessageBox>

#include <clientmodule.h>

FormExecutionBugs::FormExecutionBugs(ExecutionTest *in_execution_test, ExecutionAction *in_execution_action, QWidget *parent) : QDialog(parent), m_ui(new Ui::FormExecutionBugs)
{
    bool tmp_write_access = in_execution_test != NULL && in_execution_test->projectTest() != NULL && in_execution_test->projectTest()->projectVersion() != NULL
        && in_execution_test->projectTest()->projectVersion()->canWriteExecutionsCampaigns();

    setAttribute(Qt::WA_DeleteOnClose);

    m_ui->setupUi(this);
    m_ui->bugs_list->setRemoveSelectedRowsOnKeypressEvent(false);

    m_execution_test = in_execution_test;
    m_execution_action = in_execution_action;

    m_bugtracker = NULL;

    if (m_execution_test != NULL && m_execution_test->projectTest() != NULL && m_execution_test->projectTest()->projectVersion() != NULL) {
        setWindowTitle(tr("Anomalies du test : %1").arg(m_execution_test->projectTest()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));

        if (is_empty_string(m_execution_test->projectTest()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST)) == FALSE) {
            ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_execution_test->projectTest()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
            if (tmp_bugtracker_module != NULL) {
                m_bugtracker = static_cast<BugtrackerModule*>(tmp_bugtracker_module)->createBugtracker();
                if (m_bugtracker != NULL)
                    m_bugtracker->setBaseUrl(QUrl(m_execution_test->projectTest()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST)));
            }
        }

    }

    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(accept()));
    connect(m_ui->bugs_list, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(openBugAtIndex(QTableWidgetItem*)));

    if (!tmp_write_access) {
        m_ui->add_bug_button->setEnabled(false);
        m_ui->bugs_list->setRemoveSelectedRowsOnKeypressEvent(false);
    }
    else {
        connect(m_ui->add_bug_button, SIGNAL(clicked()), this, SLOT(addBug()));
        connect(m_ui->bugs_list, SIGNAL(delKeyPressed(QList<Record*>)), this, SLOT(deletedSelectedBugs(QList<Record*>)));
    }

    updateBugsList();
}

FormExecutionBugs::~FormExecutionBugs()
{
    qDeleteAll(m_bugs_list);

    if (m_bugtracker != NULL && m_execution_test != NULL && m_execution_test->projectTest() != NULL && m_execution_test->projectTest()->projectVersion() != NULL) {
        ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_execution_test->projectTest()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
        if (tmp_bugtracker_module != NULL) {
            static_cast<BugtrackerModule*>(tmp_bugtracker_module)->destroyBugtracker(m_bugtracker);
        }
    }

    delete m_ui;
}

void FormExecutionBugs::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


void FormExecutionBugs::addBug()
{
    FormBug *tmp_formbug = new FormBug(NULL, m_execution_test, m_execution_action, NULL);

    connect(tmp_formbug, SIGNAL(recordSaved(Record*)), this, SLOT(updateBugsList()));

    tmp_formbug->show();
}



void FormExecutionBugs::updateBugsList()
{
    int     tmp_bugs_index = 0;
    Bug     *tmp_bug = NULL;
    QStringList             tmp_bugs_headers;

    m_ui->bugs_list->clear();
    m_bugs_list.clear();

    if (m_execution_action != NULL) {
        m_bugs_list = ExecutionAction::BugRelation::instance().getChilds(m_execution_action);
    }
    else if (m_execution_test != NULL) {
        m_bugs_list = m_execution_test->loadBugs();
    }

    m_ui->bugs_list->setRowCount(m_bugs_list.count());

    tmp_bugs_headers << tr("Date") << tr("Résumé") << tr("Priorité") << tr("Sévérité") << tr("Id. externe");
    m_ui->bugs_list->setHorizontalHeaderLabels(tmp_bugs_headers);

    for (tmp_bugs_index = 0; tmp_bugs_index < m_bugs_list.count(); tmp_bugs_index++) {
        tmp_bug = m_bugs_list.at(tmp_bugs_index);
        setBugForRow(tmp_bug, tmp_bugs_index);
    }

    m_ui->bugs_list->resizeColumnsToContents();
}



void FormExecutionBugs::setBugForRow(Bug *in_bug, int in_row)
{
    QTableWidgetItem    *tmp_first_column_item = NULL;
    QTableWidgetItem    *tmp_second_column_item = NULL;
    QTableWidgetItem    *tmp_third_column_item = NULL;
    QTableWidgetItem    *tmp_fourth_column_item = NULL;
    QTableWidgetItem     *tmp_fifth_column_item = NULL;

    QDateTime tmp_date_time;
    QString tmp_tool_tip;
    QString tmp_url;
    QString tmp_external_link;

    QLabel              *tmp_external_link_label = NULL;


    if (in_bug != NULL) {
        tmp_date_time = QDateTime::fromString(QString(in_bug->getValueForKey(BUGS_TABLE_CREATION_DATE)).left(16), "yyyy-MM-dd hh:mm");
        tmp_tool_tip += "<p><b>" + tr("Date de création") + "</b> : " + tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm") + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Résumé") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SHORT_NAME)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Priorité") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_PRIORITY)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Gravité") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SEVERITY)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Plateforme") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_PLATFORM)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Système d'exploitation") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SYSTEM)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Description") + "</b> :<br>" + QString(in_bug->getValueForKey(BUGS_TABLE_DESCRIPTION)).replace('\n', "<BR>") + "</p>";
        if (is_empty_string(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE) {
            if (m_bugtracker != NULL) {
                tmp_url = m_bugtracker->urlForBugWithId(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
                if (!tmp_url.isEmpty())
                    tmp_external_link = "<a href=\"" + tmp_url + "\">" + tmp_url + "</a>";
            }

            if (tmp_external_link.isEmpty())
                tmp_external_link = QString(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
        }

        // Première colonne (date de création du bug)
        tmp_first_column_item = new QTableWidgetItem;
        tmp_first_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_first_column_item->setText(tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm"));
        tmp_first_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 0, tmp_first_column_item);

        // Seconde colonne (résumé)
        tmp_second_column_item = new QTableWidgetItem;
        tmp_second_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_second_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_SHORT_NAME));
        tmp_second_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 1, tmp_second_column_item);

        // Troisième colonne (priorité)
        tmp_third_column_item = new QTableWidgetItem;
        tmp_third_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_third_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_PRIORITY));
        tmp_third_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 2, tmp_third_column_item);

        // Quatrième colonne (sévérité)
        tmp_fourth_column_item = new QTableWidgetItem;
        tmp_fourth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_fourth_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_SEVERITY));
        tmp_fourth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 3, tmp_fourth_column_item);

        // Cinquième colonne (id externe)
        tmp_external_link_label = new QLabel(tmp_external_link);
        tmp_external_link_label->setOpenExternalLinks(true);
        tmp_fifth_column_item = new QTableWidgetItem;
        tmp_fifth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_fifth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 4, tmp_fifth_column_item);
        m_ui->bugs_list->setCellWidget(in_row, 4, tmp_external_link_label);
    }
}


void FormExecutionBugs::openBugAtIndex(QTableWidgetItem *in_item)
{
    Bug     *tmp_bug = NULL;
    FormBug     *tmp_formbug = NULL;

    if (in_item != NULL) {
        tmp_bug = (Bug*)in_item->data(Qt::UserRole).value<void*>();
        if (tmp_bug != NULL) {
            tmp_formbug = new FormBug(NULL, m_execution_test, m_execution_action, tmp_bug);
            connect(tmp_formbug, SIGNAL(recordSaved(Record*)), this, SLOT(updateBugsList()));
            tmp_formbug->show();
        }
    }
}


void FormExecutionBugs::deletedSelectedBugs(QList<Record*> in_bugs_list)
{
    if (in_bugs_list.count() > 0) {
        if (QMessageBox::question(this, tr("Confirmation"),
            tr("Etes-vous sûr(e) de vouloir supprimer %1 anomalie(s) sélectionnée(s) ?").arg(QString::number(in_bugs_list.count())),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
            foreach(Record *tmp_bug, in_bugs_list)
            {
                tmp_bug->deleteRecord();
            }

            updateBugsList();
        }
    }
}


