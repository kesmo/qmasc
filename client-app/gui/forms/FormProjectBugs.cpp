/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormProjectBugs.h"
#include "ui_FormProjectBugs.h"

#include "gui/forms/FormBug.h"

#include "session.h"

#include <QMessageBox>
#include <QLabel>

FormProjectBugs::FormProjectBugs(ProjectVersion *in_project_version, QWidget *parent) :
    AbstractProjectWidget(in_project_version, parent),
    m_ui(new Ui::FormProjectBugs)
{
    setWindowTitle(title());

    m_project_version = in_project_version;
    m_bugtracker = NULL;
    m_bugtracker_connection_active = false;
    m_has_bugtracker_error = false;

    m_ui->setupUi(this);

    m_ui->bugs_list->setRemoveSelectedRowsOnKeypressEvent(false);

    m_ui->bug_status_filter->addItem(TR_CUSTOM_MESSAGE("Tous"), "");
    m_ui->bug_status_filter->addItem(TR_CUSTOM_MESSAGE("Ouvert"), BUG_STATUS_OPENED);
    m_ui->bug_status_filter->addItem(TR_CUSTOM_MESSAGE("Fermé"), BUG_STATUS_CLOSED);

    connect(m_ui->bug_filter, SIGNAL(textChanged(QString)), this, SLOT(filterBugsList()));
    connect(m_ui->bug_status_filter, SIGNAL(currentIndexChanged(int)), this, SLOT(filterBugsList()));

    connect(m_ui->buttonBox->button(QDialogButtonBox::Close), SIGNAL(clicked()), this, SLOT(cancel()));
    connect(m_ui->reload_bugs_button, SIGNAL(clicked()), this, SLOT(launchUpdateBugsListFromBugtracker()));
    connect(m_ui->bugs_list, SIGNAL(delKeyPressed(QList<Record*>)), this, SLOT(deletedSelectedBugs(QList<Record*>)));
    connect(m_ui->bugs_list, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(openBugAtIndex(QTableWidgetItem*)));

    loadBugsList();
    initExternalBugtracker();
}

FormProjectBugs::~FormProjectBugs()
{
    qDeleteAll(m_bugs_list);

    if (m_bugtracker != NULL) {
        ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
        if (tmp_bugtracker_module != NULL) {
            static_cast<BugtrackerModule*>(tmp_bugtracker_module)->destroyBugtracker(m_bugtracker);
        }
    }

    delete m_ui;
}

QIcon FormProjectBugs::icon() const
{
    return QPixmap(":/images/22x22/bug.png");
}

QString FormProjectBugs::title() const
{
    return tr("Liste des anomalies");
}


void FormProjectBugs::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


void FormProjectBugs::initExternalBugtracker()
{
    QString     tmp_base_url = m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST);
    QString     tmp_url = m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL);

    m_bugtracker_connection_active = false;

    if (!tmp_base_url.isEmpty() || !tmp_url.isEmpty()) {
        if (!tmp_base_url.isEmpty())
            m_ui->bugtracker_url->setText("<a href=\"" + tmp_base_url + "\">" + tmp_base_url + "</a>");
        else if (!tmp_url.isEmpty())
            m_ui->bugtracker_url->setText("<a href=\"" + tmp_url + "\">" + tmp_url + "</a>");

        ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
        if (tmp_bugtracker_module != NULL) {
            if (m_bugtracker == NULL)
                m_bugtracker = static_cast<BugtrackerModule*>(tmp_bugtracker_module)->createBugtracker();

            if (m_bugtracker != NULL) {
                if (!tmp_base_url.isEmpty())
                    m_bugtracker->setBaseUrl(tmp_base_url);

                if (!tmp_url.isEmpty())
                    m_bugtracker->setWebserviceUrl(tmp_url);

                connect(m_bugtracker, SIGNAL(loginSignal()), this, SLOT(bugtrackerLogin()));
                connect(m_bugtracker, SIGNAL(error(QString)), this, SLOT(bugtrackerError(QString)));

                m_bugtracker->setCredential(Session::instance().getBugtrackersCredentials()[tmp_bugtracker_module->getModuleName()].first, Session::instance().getBugtrackersCredentials()[tmp_bugtracker_module->getModuleName()].second);
                m_bugtracker->login();
            }
        }
    }

    m_ui->url_externe_widget->setVisible(m_bugtracker != NULL);
}


void FormProjectBugs::clearBugsList()
{
    QStringList             tmp_bugs_headers;

    m_ui->bugs_list->clear();
    m_ui->bugs_list->setRowCount(0);

    tmp_bugs_headers << tr("Date") << tr("Résumé") << tr("Priorité") << tr("Sévérité") << tr("Id. externe") << tr("Etat");
    m_ui->bugs_list->setHorizontalHeaderLabels(tmp_bugs_headers);
}


void FormProjectBugs::loadBugsList()
{
    m_bugs_list.clear();
    m_bugs_list = m_project_version->loadBugs();

    filterBugsList();
}


void FormProjectBugs::filterBugsList()
{
    int tmp_index = 0;
    QString tmp_filter = m_ui->bug_filter->text();

    QString tmp_status_str = m_ui->bug_status_filter->itemData(m_ui->bug_status_filter->currentIndex()).toString();

    clearBugsList();

    foreach(Bug *tmp_bug, m_bugs_list)
    {
        if (QString(tmp_bug->getValueForKey(BUGS_TABLE_SHORT_NAME)).indexOf(tmp_filter) >= 0 || QString(tmp_bug->getValueForKey(BUGS_TABLE_DESCRIPTION)).indexOf(tmp_filter) >= 0) {
            if (tmp_status_str.isEmpty() || compare_values(tmp_status_str.toStdString().c_str(), tmp_bug->getValueForKey(BUGS_TABLE_STATUS)) == 0) {
                m_ui->bugs_list->insertRow(tmp_index);
                setBugForRow(tmp_bug, tmp_index++);
            }
        }
    }

    m_ui->bugs_list->resizeColumnsToContents();
    m_ui->bugs_list->resizeRowsToContents();
}


void FormProjectBugs::setBugForRow(Bug *in_bug, int in_row)
{
    QTableWidgetItem    *tmp_first_column_item = NULL;
    QTableWidgetItem    *tmp_second_column_item = NULL;
    QTableWidgetItem    *tmp_third_column_item = NULL;
    QTableWidgetItem    *tmp_fourth_column_item = NULL;
    QTableWidgetItem     *tmp_fifth_column_item = NULL;
    QTableWidgetItem     *tmp_sixth_column_item = NULL;

    QDateTime tmp_date_time;
    QString tmp_tool_tip;
    QString tmp_url;
    QString tmp_external_link;

    QLabel              *tmp_external_link_label = NULL;

    if (in_bug != NULL) {
        tmp_date_time = QDateTime::fromString(QString(in_bug->getValueForKey(BUGS_TABLE_CREATION_DATE)).left(16), "yyyy-MM-dd hh:mm");
        tmp_tool_tip += "<p><b>" + tr("Date de création") + "</b> : " + tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm") + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Résumé") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SHORT_NAME)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Priorité") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_PRIORITY)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Gravité") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SEVERITY)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Plateforme") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_PLATFORM)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Système d'exploitation") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_SYSTEM)) + "</p>";
        tmp_tool_tip += "<p><b>" + tr("Description") + "</b> :<br>" + QString(in_bug->getValueForKey(BUGS_TABLE_DESCRIPTION)).replace('\n', "<BR>") + "</p>";
        if (is_empty_string(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE) {
            tmp_tool_tip += "<p><b>" + tr("Identifiant externe") + "</b> : " + QString(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) + "</p>";
            if (m_bugtracker != NULL) {
                tmp_url = m_bugtracker->urlForBugWithId(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
                if (!tmp_url.isEmpty())
                    tmp_external_link = "<a href=\"" + tmp_url + "\">" + tmp_url + "</a>";
            }

            if (tmp_external_link.isEmpty())
                tmp_external_link = QString(in_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
        }

        // Premirère colonne (date de création du bug)
        tmp_first_column_item = new QTableWidgetItem;
        tmp_first_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_first_column_item->setText(tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm"));
        tmp_first_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 0, tmp_first_column_item);

        // Seconde colonne (résumé)
        tmp_second_column_item = new QTableWidgetItem;
        tmp_second_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_second_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_SHORT_NAME));
        tmp_second_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 1, tmp_second_column_item);

        // Troisième colonne (priorité)
        tmp_third_column_item = new QTableWidgetItem;
        tmp_third_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_third_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_PRIORITY));
        tmp_third_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 2, tmp_third_column_item);

        // Quatrième colonne (sévérité)
        tmp_fourth_column_item = new QTableWidgetItem;
        tmp_fourth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_fourth_column_item->setText(in_bug->getValueForKey(BUGS_TABLE_SEVERITY));
        tmp_fourth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 3, tmp_fourth_column_item);

        // Cinquième colonne (id externe)
        tmp_external_link_label = new QLabel(tmp_external_link);
        tmp_external_link_label->setOpenExternalLinks(true);
        tmp_fifth_column_item = new QTableWidgetItem;
        tmp_fifth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        tmp_fifth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 4, tmp_fifth_column_item);
        m_ui->bugs_list->setCellWidget(in_row, 4, tmp_external_link_label);

        // Sixième colonne (status)
        tmp_sixth_column_item = new QTableWidgetItem;
        tmp_sixth_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_bug));
        if (compare_values(in_bug->getValueForKey(BUGS_TABLE_STATUS), BUG_STATUS_OPENED) == 0)
            tmp_sixth_column_item->setText(tr("Ouvert"));
        else if (compare_values(in_bug->getValueForKey(BUGS_TABLE_STATUS), BUG_STATUS_CLOSED) == 0)
            tmp_sixth_column_item->setText(tr("Fermé"));
        else
            tmp_sixth_column_item->setText(tr("Non disponible"));

        tmp_sixth_column_item->setToolTip(tmp_tool_tip);
        m_ui->bugs_list->setItem(in_row, 5, tmp_sixth_column_item);


    }
}


void FormProjectBugs::openBugAtIndex(QTableWidgetItem *in_item)
{
    Bug     *tmp_bug = NULL;
    FormBug     *tmp_formbug = NULL;

    if (in_item != NULL) {
        tmp_bug = (Bug*)in_item->data(Qt::UserRole).value<void*>();
        if (tmp_bug != NULL) {
            tmp_formbug = new FormBug(m_project_version, NULL, NULL, tmp_bug);
            connect(tmp_formbug, SIGNAL(recordSaved(Record*)), this, SLOT(loadBugsList()));
            tmp_formbug->show();
        }
    }
}


void FormProjectBugs::deletedSelectedBugs(QList<Record*> in_bugs_list)
{
    if (in_bugs_list.count() > 0) {
        if (QMessageBox::question(this, tr("Confirmation"),
            tr("Etes-vous sûr(e) de vouloir supprimer %1 anomalie(s) sélectionnée(s) ?").arg(QString::number(in_bugs_list.count())),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
            foreach(Record *tmp_bug, in_bugs_list)
            {
                tmp_bug->deleteRecord();
            }

            loadBugsList();
        }
    }
}

void FormProjectBugs::cancel()
{
    QWidget::close();
}



void FormProjectBugs::updateBugsListFromBugtracker(QMap< QString, QMap<Bugtracker::BugProperty, QString> > in_bugs_list)
{
    QMap< QString, QMap<Bugtracker::BugProperty, QString> >::iterator tmp_bug_iterator;

    for (tmp_bug_iterator = in_bugs_list.begin(); tmp_bug_iterator != in_bugs_list.end(); tmp_bug_iterator++) {
        int tmp_bug_index = Record::indexOfMatchingValueInRecordsList<Bug>(m_bugs_list, BUGS_TABLE_BUGTRACKER_BUG_ID, tmp_bug_iterator.key().toStdString().c_str());
        if (tmp_bug_index >= 0 && m_bugs_list[tmp_bug_index] != NULL) {
            // Mettre à jour le bug
            QMap<Bugtracker::BugProperty, QString> tmp_bug_infos = tmp_bug_iterator.value();

            if (!tmp_bug_infos[Bugtracker::Priority].isEmpty())
                m_bugs_list[tmp_bug_index]->setValueForKey(tmp_bug_infos[Bugtracker::Priority].toStdString().c_str(), BUGS_TABLE_PRIORITY);

            if (!tmp_bug_infos[Bugtracker::Severity].isEmpty())
                m_bugs_list[tmp_bug_index]->setValueForKey(tmp_bug_infos[Bugtracker::Severity].toStdString().c_str(), BUGS_TABLE_SEVERITY);

            if (!tmp_bug_infos[Bugtracker::Summary].isEmpty())
                m_bugs_list[tmp_bug_index]->setValueForKey(tmp_bug_infos[Bugtracker::Summary].toStdString().c_str(), BUGS_TABLE_SHORT_NAME);

            if (!tmp_bug_infos[Bugtracker::Status].isEmpty())
                m_bugs_list[tmp_bug_index]->setValueForKey(tmp_bug_infos[Bugtracker::Status].toStdString().c_str(), BUGS_TABLE_STATUS);

            m_bugs_list[tmp_bug_index]->saveRecord();
        }
    }

    filterBugsList();

}

void FormProjectBugs::launchUpdateBugsListFromBugtracker()
{
    m_has_bugtracker_error = false;

    if (m_bugtracker == NULL || !m_bugtracker_connection_active) {
        initExternalBugtracker();
    }
    else {
        QStringList tmp_bugs_ids_list;

        foreach(Bug *tmp_bug, m_bugs_list)
        {
            if (is_empty_string(tmp_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE)
                tmp_bugs_ids_list << tmp_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID);
        }

        if (!tmp_bugs_ids_list.isEmpty())
            m_bugtracker->getBugs(tmp_bugs_ids_list);
    }
}


void FormProjectBugs::bugtrackerLogin()
{
    m_bugtracker_connection_active = true;

    connect(m_bugtracker, SIGNAL(bugsInformations(QMap<QString, QMap<Bugtracker::BugProperty, QString> >)), this, SLOT(updateBugsListFromBugtracker(QMap< QString, QMap<Bugtracker::BugProperty, QString> >)));

    launchUpdateBugsListFromBugtracker();
}


void FormProjectBugs::bugtrackerError(QString errorMsg)
{
    if (!m_has_bugtracker_error) {
        m_has_bugtracker_error = true;
        QMessageBox::critical(this, tr("Erreur"), tr("La synchronisation avec le gestionnaire d'anomalies (%1) n'a pu être réalisée.").arg(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL)) + "<br>" + errorMsg + "<p><span style=\"color: red\"><b>" + tr("La requête a échouée.") + "</b></span></p>");
    }
}


