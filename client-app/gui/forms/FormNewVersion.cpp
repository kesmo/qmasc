/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormNewVersion.h"
#include "ui_FormNewVersion.h"
#include "utilities.h"
#include "session.h"

#include <QMessageBox>
#include <QComboBox>

/**
  Constructeur
**/
FormNewVersion::FormNewVersion(Project *in_project, QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::FormNewVersion)
{
    setAttribute(Qt::WA_DeleteOnClose);

    m_ui->setupUi(this);

    m_ui->project_version_info->addTextToolBar(RecordTextEditToolBar::Small);
    m_project = in_project;

    // Init UI
    m_ui->project_name->setText(m_project->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
    m_ui->project_description->setHtml(m_project->getValueForKey(PROJECTS_TABLE_DESCRIPTION));

    foreach(ProjectVersion* tmp_project_version, Project::ProjectVersionRelation::instance().getChilds(m_project))
    {
        m_ui->projects_versions_list->addItem(ProjectVersion::formatProjectVersionNumber(tmp_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)), QVariant::fromValue<void*>(tmp_project_version));
    }
    m_ui->projects_versions_list->setCurrentIndex(m_ui->projects_versions_list->count() - 1);
    selectedVersionChanged(m_ui->projects_versions_list->count() - 1);

    connect(m_ui->projects_versions_list, SIGNAL(currentIndexChanged(int)), this, SLOT(selectedVersionChanged(int)));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}


/**
  Destructeur
**/
FormNewVersion::~FormNewVersion()
{
    delete m_ui;
}


/**
  Changement de selection de version
**/
void FormNewVersion::selectedVersionChanged(int in_version_index)
{
    QStringList tmp_version_numbers;

    ProjectVersion   *tmp_version = (ProjectVersion*)m_ui->projects_versions_list->itemData(in_version_index, Qt::UserRole).value<void*>();

    if (tmp_version) {
        tmp_version_numbers = QString(tmp_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)).split(".");
        if (tmp_version_numbers.count() > 0)
            m_ui->major_version_number->setValue(tmp_version_numbers[0].toInt());
        if (tmp_version_numbers.count() > 1)
            m_ui->medium_version_number->setValue(tmp_version_numbers[1].toInt());
        if (tmp_version_numbers.count() > 2)
            m_ui->minor_version_number->setValue(tmp_version_numbers[2].toInt());
        if (tmp_version_numbers.count() > 3)
            m_ui->maintenance_version_number->setValue(tmp_version_numbers[3].toInt());

        m_ui->project_src_version_info->setHtml(tmp_version->getValueForKey(PROJECTS_VERSIONS_TABLE_DESCRIPTION));
    }
}

/**
  Validation par le bouton OK
**/
void FormNewVersion::accept()
{
    QString         tmp_selected_version, tmp_version, tmp_major_version, tmp_medium_version, tmp_minor_version, tmp_maintenance_version;

    QList<ProjectVersion*> tmp_versions;
    ProjectVersion  *tmp_selected_project_version = NULL;
    ProjectVersion  *tmp_new_project_version = NULL;

    net_session     *tmp_session = Session::instance().getClientSession();
    char            ***tmp_copy_return_results = NULL;
    unsigned long   tmp_rows_count = 0;
    unsigned long   tmp_columns_count = 0;

    int     tmp_save_return = NOERR;

    tmp_major_version.setNum(m_ui->major_version_number->value());
    tmp_medium_version.setNum(m_ui->medium_version_number->value());
    tmp_minor_version.setNum(m_ui->minor_version_number->value());
    tmp_maintenance_version.setNum(m_ui->maintenance_version_number->value());

    tmp_version = QString("%1.%2.%3.%4")
        .arg(tmp_major_version, 2, '0')
        .arg(tmp_medium_version, 2, '0')
        .arg(tmp_minor_version, 2, '0')
        .arg(tmp_maintenance_version, 2, '0');

    if (m_ui->projects_versions_list->currentIndex() >= 0) {
        tmp_selected_project_version = (ProjectVersion*)m_ui->projects_versions_list->itemData(m_ui->projects_versions_list->currentIndex(), Qt::UserRole).value<void*>();
        tmp_selected_version = tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION);
        if (tmp_selected_version == tmp_version) {
            QMessageBox::critical(this, tr("Donnée non cohérante"), tr("La version existe déjà."));
            return;
        }
        else if (tmp_selected_version > tmp_version) {
            QMessageBox::critical(this, tr("Donnée non cohérante"), tr("La nouvelle version est antérieure à la version sélectionnée."));
            return;
        }

        net_session_print_query(tmp_session, "select copy_project_version(%s,'%s','%s');",
                                tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID),
                                tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                                tmp_version.toStdString().c_str());

        tmp_copy_return_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_copy_return_results != NULL) {
            if (tmp_rows_count == 1 && tmp_columns_count == 1) {
                if (is_empty_string(tmp_copy_return_results[0][0]) == FALSE) {
                    tmp_new_project_version = ProjectVersion::getEntity(tmp_copy_return_results[0][0]);
                    if (tmp_new_project_version) {
                        tmp_new_project_version->setValueForKey(tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE);
                        tmp_new_project_version->setValueForKey(tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST);
                        tmp_new_project_version->setValueForKey(tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL);
                        tmp_new_project_version->setValueForKey(tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID);
                        tmp_new_project_version->setValueForKey(tmp_selected_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION);
                    }
                }
            }

            cl_free_rows_columns_array(&tmp_copy_return_results, tmp_rows_count, tmp_columns_count);
        }
    }
    else {
        tmp_new_project_version = new ProjectVersion();
        tmp_new_project_version->setValueForKey(tmp_version.toStdString().c_str(), PROJECTS_VERSIONS_TABLE_VERSION);

    }

    if (tmp_new_project_version != NULL) {
        tmp_new_project_version->setValueForKey(m_ui->project_version_info->textEditor()->toOptimizedHtml().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_DESCRIPTION);

        Project::ProjectVersionRelation::instance().addChild(m_project, tmp_new_project_version);
        tmp_save_return = Project::ProjectVersionRelation::instance().saveChilds(m_project);
        if (tmp_save_return == NOERR) {
            QDialog::accept();
            emit versionCreated(tmp_new_project_version);
            return;
        }
    }
    Project::ProjectVersionRelation::instance().reset(m_project, true);

    QMessageBox::critical(this, tr("Erreur d'enregistrement"), Session::instance().getErrorMessage(tmp_save_return));
}
