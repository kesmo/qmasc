#ifndef FORM_FEATURE_H
#define FORM_FEATURE_H

#include "gui/components/AbstractProjectWidget.h"
#include "entities/feature.h"
#include "entities/featurecontent.h"

#include <QtWidgets/QWidget>

namespace Ui {
class FormFeature;
}

class FormFeature :
        public AbstractProjectWidget
{
    Q_OBJECT
    
public:
    explicit FormFeature(Feature *in_feature, QWidget *parent = 0);
    ~FormFeature();

    QIcon icon() const;
    QString title() const;

public slots:
    bool saveRecord();

protected:
    void loadFeature(Feature *in_feature);
    void loadFeatureContent(FeatureContent *in_feature_content);

private:
    Ui::FormFeature *m_ui;

    Feature*            m_feature;
    FeatureContent*     m_feature_content;


};

#endif // FORM_FEATURE_H
