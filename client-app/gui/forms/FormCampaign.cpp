/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "session.h"
#include "FormCampaign.h"
#include "ui_FormCampaign.h"
#include "FormExecutionCampaign.h"
#include "entities/testcontent.h"
#include "entities/projectversion.h"
#include "entities/projectgrant.h"
#include "entities/testsplan/testsplan.h"

#include <QMessageBox>
#include <QProgressBar>

/**
  Constructeur
**/
FormCampaign::FormCampaign(Campaign *in_campaign, QWidget *parent) :
    AbstractProjectWidget(in_campaign, parent),
    m_ui(new Ui::FormCampaign),
    m_tests_tree_model(NULL),
    m_campaign(NULL),
    m_tests_plan_scene(NULL),
    m_page_tests_plan_first_time_show(false)

{
    QStringList           tmp_headers;

    m_ui->setupUi(this);
    m_tests_tree_view = new RecordsTreeView;
    m_tests_tree_view->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tests_tree_view->setSelectionMode(QAbstractItemView::ExtendedSelection);
    m_tests_tree_view->setDragEnabled(true);
    m_tests_tree_view->setAcceptDrops(true);
    m_tests_tree_view->setDropIndicatorShown(true);
    m_tests_tree_view->setHeaderHidden(true);
    m_tests_tree_view->setExpandsOnDoubleClick(false);

    m_ui->tests_campaign_frame->layout()->addWidget(m_tests_tree_view);
    m_ui->campaign_description->addTextToolBar(RecordTextEditToolBar::Small);

    // Liste des executions
    tmp_headers << tr("Date") << tr("Révision") << tr("Couverture") << tr("Réussite") << tr("Echec") << tr("Non passé");
    m_ui->executions_campaigns_list->setColumnCount(tmp_headers.count());
    m_ui->executions_campaigns_list->setHorizontalHeaderLabels(tmp_headers);
    m_ui->executions_campaigns_list->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_ui->executions_campaigns_list->setRowCount(0);
    m_ui->executions_campaigns_list->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    connect(m_tests_tree_view, SIGNAL(delKeyPressed(QList<Hierarchy*>)), this, SLOT(deleteTestsCampaignList(QList<Hierarchy*>)));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    connect(m_ui->execute_button, SIGNAL(clicked()), this, SLOT(execute()));
    connect(m_ui->execute_tests_plan_button, SIGNAL(clicked()), this, SLOT(executeTestsPlan()));
    connect(m_ui->button_continue_execution, SIGNAL(clicked()), this, SLOT(executeSelectedExecutionCampaign()));
    connect(m_ui->executions_campaigns_list, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(open(QModelIndex)));
    connect(m_ui->executions_campaigns_list->selectionModel(), SIGNAL(selectionChanged(QItemSelection, QItemSelection)), this, SLOT(manageExecutionsSelection()));

    connect(m_ui->button_delete_execution, SIGNAL(clicked()), this, SLOT(deleteSelectedExecutionCampaign()));

    connect(m_ui->campaign_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->campaign_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));

    connect(m_ui->campaignTabWidget, SIGNAL(currentChanged(int)), this, SLOT(toolBoxPageChanged(int)));

    manageExecutionsSelection();

    loadCampaign(in_campaign);

}


/**
  Destructeur
**/
FormCampaign::~FormCampaign()
{
    delete m_tests_plan_scene;

    destroyPluginsViews();

    delete m_ui;

    delete m_tests_tree_view;
    delete m_tests_tree_model;
}

QIcon FormCampaign::icon() const
{
    return QPixmap(":/images/22x22/campaigns.png");
}

QString FormCampaign::title() const
{
    if (m_campaign)
        return m_campaign->getValueForKey(CAMPAIGNS_TABLE_SHORT_NAME);

    return QString();
}

void FormCampaign::reloadCampaign()
{
    loadCampaign(m_campaign);
}


void FormCampaign::loadCampaign(Campaign *in_campaign)
{
    int                   tmp_execution_index = 0;
    bool           tmp_modifiable = false;
    bool           tmp_executable = false;

    m_ui->lock_widget->setVisible(false);

    if (in_campaign != NULL) {
        m_campaign = in_campaign;

        tmp_modifiable = m_campaign->isModifiable();
        if (tmp_modifiable)
            m_campaign->lockRecord(true);

        tmp_modifiable = tmp_modifiable && m_campaign->lockRecordStatus() == RECORD_STATUS_OWN_LOCK;

        if (m_campaign->lockRecordStatus() == RECORD_STATUS_LOCKED) {
            m_ui->lock_widget->setVisible(true);
            net_get_field(NET_MESSAGE_TYPE_INDEX + 1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
            m_ui->label_lock_by->setText(tr("Verrouillé par %1").arg(Session::instance().getClientSession()->m_column_buffer));
        }

        tmp_executable = m_campaign->projectVersion() != NULL && m_campaign->projectVersion()->canWriteExecutionsCampaigns();

        m_ui->execute_button->setEnabled(tmp_executable);
        m_ui->button_delete_execution->setVisible(Session::instance().currentUserRoles().contains("admin_role"));

        setWindowTitle(title());

        delete m_tests_tree_model;

        m_tests_tree_model = new RecordsTreeModel(new Hierarchies::CampaignHierarchy(m_campaign, true, this));
        m_tests_tree_model->setSupportedDropActions(Qt::CopyAction | Qt::MoveAction);
        m_tests_tree_view->setRecordsTreeModel(m_tests_tree_model);

        m_ui->campaign_name->setText(m_campaign->getValueForKey(CAMPAIGNS_TABLE_SHORT_NAME));
        m_ui->campaign_description->textEditor()->setHtml(m_campaign->getValueForKey(CAMPAIGNS_TABLE_DESCRIPTION));

        m_ui->campaign_name->setReadOnly(!tmp_modifiable);
        m_ui->campaign_description->textEditor()->setReadOnly(!tmp_modifiable);
        if (tmp_modifiable)
            m_ui->campaign_description->toolBar()->show();
        else
            m_ui->campaign_description->toolBar()->hide();

        m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(tmp_modifiable);

        const QList<ExecutionCampaign*> &executionCampaigns(Campaign::ExecutionCampaignRelation::instance().getChilds(m_campaign));
        m_ui->executions_campaigns_list->setRowCount(executionCampaigns.count());
        for (tmp_execution_index = 0; tmp_execution_index < executionCampaigns.count(); tmp_execution_index++) {
            setExecutionCampaignForRow(executionCampaigns.at(tmp_execution_index), tmp_execution_index);
        }
        m_ui->executions_campaigns_list->resizeColumnsToContents();

        destroyPluginsViews();
        loadPluginsViews();

        delete m_tests_plan_scene;
        const QList<TestsPlan*>& testsPlansList = Campaign::TestsPlanRelation::instance().getChilds(m_campaign);
        TestsPlan* testsplan = NULL;
        if (testsPlansList.isEmpty()) {
            testsplan = new TestsPlan();
            Campaign::TestsPlanRelation::instance().addChild(m_campaign, testsplan);
            Campaign::TestsPlanRelation::instance().saveChilds(m_campaign);
        }
        else
            testsplan = testsPlansList.first();

        m_tests_plan_scene = new TestsPlanScene(testsplan);

        m_ui->tests_plan_view->setScene(m_tests_plan_scene);
        m_ui->tests_plan_view->setAcceptDrops(true);
        m_ui->tests_plan_view->setDragMode(QGraphicsView::ScrollHandDrag);
        m_ui->tests_plan_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        m_ui->tests_plan_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }

    setModified(false);
}


void FormCampaign::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FormCampaign::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

}

/**
  Enregistrer les modifications
**/
bool FormCampaign::saveCampaign()
{
    int result = NOERR;

    m_campaign->setValueForKey(m_ui->campaign_name->text().toStdString().c_str(), CAMPAIGNS_TABLE_SHORT_NAME);
    m_campaign->setValueForKey(m_ui->campaign_description->textEditor()->toOptimizedHtml().toStdString().c_str(), CAMPAIGNS_TABLE_DESCRIPTION);
    result = m_campaign->saveRecord();
    if (result == NOERR) {
        result = m_tests_plan_scene->save();
    }

    if (result != NOERR)
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(result));

    return result == NOERR;
}


/**
  Valider la saisie
**/
void FormCampaign::save()
{
    if (saveCampaign()) {
        savePluginsDatas();
        setModified(false);
        emit recordSaved(m_campaign);
    }
}


void FormCampaign::cancel()
{
    QWidget::close();
}


void FormCampaign::deleteTestsCampaignList(QList<Hierarchy*> in_records_list)
{
    QModelIndex         tmp_model_index;
    QMessageBox         *tmp_msg_box = NULL;
    QPushButton         *tmp_del_button = NULL;
    QPushButton         *tmp_keep_button = NULL;
    QPushButton         *tmp_del_all_button = NULL;
    QPushButton         *tmp_cancel_button = NULL;
    bool                tmp_delete_all = false;
    bool                tmp_delete_current = false;

    QList<Hierarchy*>    tmp_records_list;

    if (m_campaign->isModifiable()) {
        foreach(Hierarchy *tmp_item, in_records_list)
        {
            if (tmp_item->parent()->getRecord() == m_campaign)
                tmp_records_list.append(tmp_item);
        }

        if (tmp_records_list.count() > 0) {
            tmp_msg_box = new QMessageBox(this);
            tmp_msg_box->setIcon(QMessageBox::Question);
            tmp_msg_box->setWindowTitle(tr("Confirmation..."));

            tmp_del_button = tmp_msg_box->addButton(tr("Supprimer le test"), QMessageBox::YesRole);
            tmp_keep_button = tmp_msg_box->addButton(tr("Conserver le test"), QMessageBox::YesRole);
            tmp_cancel_button = tmp_msg_box->addButton(tr("Annuler"), QMessageBox::RejectRole);

            if (tmp_records_list.count() > 1)
                tmp_del_all_button = tmp_msg_box->addButton(tr("Supprimer tous les tests"), QMessageBox::NoRole);

            foreach(Hierarchy *tmp_item, tmp_records_list)
            {
                tmp_delete_current = tmp_delete_all;

                if (tmp_delete_all == false) {
                    tmp_msg_box->setText(tr("Etes-vous sûr(e) de vouloir supprimer le test \"%1\" ?").arg(tmp_item->getRecord()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
                    tmp_msg_box->exec();
                    if (tmp_msg_box->clickedButton() == tmp_del_button) {
                        tmp_delete_current = true;
                    }
                    else if (tmp_msg_box->clickedButton() == tmp_del_all_button) {
                        tmp_delete_current = true;
                        tmp_delete_all = true;
                    }
                    else if (tmp_msg_box->clickedButton() == tmp_cancel_button)
                        break;
                }

                tmp_model_index = m_tests_tree_model->modelIndexForRecord(tmp_item->getRecord());
                if (tmp_delete_current && tmp_model_index.isValid())
                    m_tests_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
            }
        }
    }
}



void FormCampaign::setExecutionCampaignForRow(ExecutionCampaign *in_execution_campaign, int in_row)
{
    QTableWidgetItem    *tmp_column_item = NULL;
    QString                tmp_float_string;
    QDateTime            tmp_date_time;
    QProgressBar        *tmp_progress_bar = NULL;

    float        tmp_tests_coverage_rate = 0;
    float        tmp_tests_validated_rate = 0;
    float        tmp_tests_invalidated_rate = 0;
    float        tmp_tests_bypassed_rate = 0;

    if (in_execution_campaign != NULL) {
        tmp_tests_coverage_rate = in_execution_campaign->executionCoverageRate() * 100;
        tmp_tests_validated_rate = in_execution_campaign->executionValidatedRate() * 100;
        tmp_tests_invalidated_rate = in_execution_campaign->executionInValidatedRate() * 100;
        tmp_tests_bypassed_rate = in_execution_campaign->executionBypassedRate() * 100;

        // Premiere colonne (date)
        qDebug() << Q_FUNC_INFO << in_execution_campaign->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE);

        tmp_date_time = QDateTime::fromString(QString(in_execution_campaign->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE)).left(16), "yyyy-MM-dd hh:mm");
        tmp_column_item = new QTableWidgetItem;
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_execution_campaign));
        tmp_column_item->setText(tmp_date_time.toString("dddd dd MMMM yyyy à hh:mm"));
        m_ui->executions_campaigns_list->setItem(in_row, 0, tmp_column_item);

        // Deuxieme colonne (Révision)
        tmp_column_item = new QTableWidgetItem;
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_execution_campaign));
        tmp_column_item->setText(in_execution_campaign->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_REVISION));
        m_ui->executions_campaigns_list->setItem(in_row, 1, tmp_column_item);

        // Troisième colonne (Couverture)
        tmp_column_item = new QTableWidgetItem;
        tmp_progress_bar = new QProgressBar(this);
        tmp_progress_bar->setMinimum(0);
        tmp_progress_bar->setMaximum(100);
        tmp_progress_bar->setValue(tmp_tests_coverage_rate);
        tmp_progress_bar->setStyleSheet("QProgressBar::chunk {background: #c4c2c3};");
        m_ui->executions_campaigns_list->setCellWidget(in_row, 2, tmp_progress_bar);
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_execution_campaign));
        tmp_float_string.setNum(tmp_tests_coverage_rate, 'f', 2);
        //tmp_column_item->setText(tmp_float_string + " %");
        m_ui->executions_campaigns_list->setItem(in_row, 2, tmp_column_item);


        // Quatrième colonne (réussite)
        tmp_column_item = new QTableWidgetItem;
        tmp_progress_bar = new QProgressBar(this);
        tmp_progress_bar->setMinimum(0);
        tmp_progress_bar->setMaximum(100);
        tmp_progress_bar->setValue(tmp_tests_validated_rate);
        tmp_progress_bar->setStyleSheet("QProgressBar::chunk {background: #77d57a};");
        m_ui->executions_campaigns_list->setCellWidget(in_row, 3, tmp_progress_bar);
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_execution_campaign));
        tmp_float_string.setNum(tmp_tests_validated_rate, 'f', 2);
        //tmp_column_item->setText(tmp_float_string + " %");
        m_ui->executions_campaigns_list->setItem(in_row, 3, tmp_column_item);

        // Cinquième colonne (echec)
        tmp_column_item = new QTableWidgetItem;
        tmp_progress_bar = new QProgressBar(this);
        tmp_progress_bar->setMinimum(0);
        tmp_progress_bar->setMaximum(100);
        tmp_progress_bar->setValue(tmp_tests_invalidated_rate);
        tmp_progress_bar->setStyleSheet("QProgressBar::chunk {background: #d56060};");
        m_ui->executions_campaigns_list->setCellWidget(in_row, 4, tmp_progress_bar);
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_execution_campaign));
        tmp_float_string.setNum(tmp_tests_invalidated_rate, 'f', 2);
        //tmp_column_item->setText(tmp_float_string + " %");
        m_ui->executions_campaigns_list->setItem(in_row, 4, tmp_column_item);

        // Sixième colonne (non passé)
        tmp_column_item = new QTableWidgetItem;
        tmp_progress_bar = new QProgressBar(this);
        tmp_progress_bar->setMinimum(0);
        tmp_progress_bar->setMaximum(100);
        tmp_progress_bar->setValue(tmp_tests_bypassed_rate);
        tmp_progress_bar->setStyleSheet("QProgressBar::chunk {background: #c4c2c3};");
        m_ui->executions_campaigns_list->setCellWidget(in_row, 5, tmp_progress_bar);
        tmp_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_execution_campaign));
        tmp_float_string.setNum(tmp_tests_bypassed_rate, 'f', 2);
        //tmp_column_item->setText(tmp_float_string + " %");
        m_ui->executions_campaigns_list->setItem(in_row, 5, tmp_column_item);

    }
}


void FormCampaign::execute(ExecutionCampaign *in_execution_campaign)
{
    FormExecutionCampaign* tmp_dialog = new FormExecutionCampaign(in_execution_campaign, this);
    tmp_dialog->showMaximized();
    connect(tmp_dialog, SIGNAL(destroyed()), this, SLOT(reloadCampaign()));
}


/**
  Lancer une execution de la campagne
**/
void FormCampaign::execute()
{
    execute(ExecutionCampaign::create(m_campaign));
}


void FormCampaign::executeTestsPlan()
{
    execute(ExecutionCampaign::createFromTestsPlan(m_campaign));
}

/**
  Ouvrir une execution de la campagne
**/
void FormCampaign::open(QModelIndex in_index)
{
    ExecutionCampaign           *tmp_execution_campaign = NULL;
    QTableWidgetItem            *tmp_column_item = NULL;

    if (in_index.isValid()) {
        tmp_column_item = m_ui->executions_campaigns_list->item(in_index.row(), 0);
        if (tmp_column_item != NULL) {
            tmp_execution_campaign = (ExecutionCampaign*)tmp_column_item->data(Qt::UserRole).value<void*>();
            if (tmp_execution_campaign != NULL) {
                execute(tmp_execution_campaign);
            }
        }
    }
}


void FormCampaign::closeEvent(QCloseEvent *in_event)
{
    int        tmp_confirm_choice = 0;

    if (m_campaign != NULL) {
        if (isModified()) {
            tmp_confirm_choice = QMessageBox::question(
                this,
                tr("Confirmation..."),
                tr("La campagne a été modifiée voules-vous enregistrer les modifications ?"),
                QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                QMessageBox::Cancel);

            if (tmp_confirm_choice == QMessageBox::Yes) {
                if (saveCampaign() == false) {
                    in_event->ignore();
                    return;
                }
            }
            else if (tmp_confirm_choice == QMessageBox::Cancel) {
                in_event->ignore();
                return;
            }
        }

        m_campaign->unlockRecord();
    }

    in_event->accept();
}


void FormCampaign::manageExecutionsSelection()
{
    if (!m_ui->executions_campaigns_list->selectedItems().isEmpty()) {
        m_ui->button_continue_execution->setEnabled(true);
        m_ui->button_delete_execution->setEnabled(true);
    }
    else {
        m_ui->button_continue_execution->setDisabled(true);
        m_ui->button_delete_execution->setDisabled(true);
    }
}


void FormCampaign::executeSelectedExecutionCampaign()
{
    QModelIndex             tmp_select_row = m_ui->executions_campaigns_list->selectionModel()->currentIndex();

    if (tmp_select_row.isValid()) {
        open(tmp_select_row);
    }
}

void FormCampaign::deleteSelectedExecutionCampaign()
{
    QModelIndex             tmp_select_row = m_ui->executions_campaigns_list->selectionModel()->currentIndex();
    ExecutionCampaign        *tmp_execution_campaign = NULL;
    QTableWidgetItem            *tmp_column_item = NULL;
    int                     tmp_row_index = 0;

    if (tmp_select_row.isValid()) {
        tmp_row_index = tmp_select_row.row();
        tmp_column_item = m_ui->executions_campaigns_list->item(tmp_row_index, 0);
        if (tmp_column_item != NULL) {
            tmp_execution_campaign = (ExecutionCampaign*)tmp_column_item->data(Qt::UserRole).value<void*>();
            if (tmp_execution_campaign != NULL) {
                if (QMessageBox::question(
                    this,
                    tr("Confirmation..."),
                    tr("Etes-vous sûr(e) de vouloir supprimer l'exécution de la campagne du %1 ?")
                    .arg(QDateTime::fromString(QString(tmp_execution_campaign->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE)).left(16), "yyyy-MM-dd hh:mm").toString("dddd dd MMMM yyyy à hh:mm")),
                    QMessageBox::Yes | QMessageBox::No,
                    QMessageBox::No) == QMessageBox::Yes) {
                    m_ui->executions_campaigns_list->removeRow(tmp_row_index);

                    Campaign::ExecutionCampaignRelation::instance().removeChild(m_campaign, tmp_execution_campaign);
                    Campaign::ExecutionCampaignRelation::instance().saveChilds(m_campaign);
                }
            }
        }
    }
}


void FormCampaign::loadPluginsViews()
{
    QMap < QString, ClientModule*>    tmp_modules_map = Session::instance().externalsModules().value(ClientModule::CampaignPlugin);

    CampaignModule   *tmp_campaign_module = NULL;

    foreach(ClientModule *tmp_module, tmp_modules_map)
    {
        tmp_campaign_module = static_cast<CampaignModule*>(tmp_module);

        tmp_campaign_module->loadCampaignModuleDatas(m_campaign);

        QWidget        *tmp_module_view = tmp_campaign_module->createView(this);
        if (tmp_module_view != NULL) {
            m_views_modules_map[tmp_campaign_module] = tmp_module_view;
            m_ui->campaignTabWidget->addTab(tmp_module_view, tmp_module->getModuleName());
        }
    }
}


void FormCampaign::savePluginsDatas()
{
    QMap<CampaignModule*, QWidget*>::iterator tmp_module_iterator;

    for (tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++) {
        tmp_module_iterator.key()->saveCampaignModuleDatas();
    }
}


void FormCampaign::destroyPluginsViews()
{
    QMap<CampaignModule*, QWidget*>::iterator tmp_module_iterator;

    for (tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++) {
        tmp_module_iterator.key()->destroyView(tmp_module_iterator.value());
    }
}

void FormCampaign::toolBoxPageChanged(int /*index*/)
{
    if (m_ui->campaignTabWidget->currentWidget() == m_ui->tests_plan && !m_page_tests_plan_first_time_show) {
        m_ui->tests_plan_view->fitInView(m_tests_plan_scene->sceneRect(), Qt::KeepAspectRatio);
        m_ui->tests_plan_view->scale(0.9, 0.9);
        m_page_tests_plan_first_time_show = true;
    }
}
