/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormUserProjectsGrants.h"
#include "ui_FormUserProjectsGrants.h"

#include "session.h"

#include "client.h"
#include "utilities.h"
#include "constants.h"
#include "entities/projectgrant.h"

#include "gui/components/hierarchies/userprojectsgrantshierarchy.h"

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QMessageBox>
#include <QStandardItemModel>

FormUserProjectsGrants::FormUserProjectsGrants(const char *in_username, QWidget *parent) : QDialog(parent), m_ui(new Ui::FormUserProjectsGrants)
{
    QStringList             tmp_headers;

    setAttribute(Qt::WA_DeleteOnClose);

    m_ui->setupUi(this);

    setWindowTitle(tr("Gestion des droits ") + QString(in_username));

    m_projects_grants_tree_model = new RecordsTreeModel(new Hierarchies::UserProjectsGrantsHierarchy(in_username, this));
    m_ui->projects_grants_tree_widget->setRecordsTreeModel(m_projects_grants_tree_model);
    m_projects_grants_tree_model->setColumnsCount(2);

    tmp_headers << tr("Project") << tr("Droits");
    QStandardItemModel *tmp_header_model = new QStandardItemModel();
    tmp_header_model->setHorizontalHeaderLabels(tmp_headers);
    m_ui->projects_grants_tree_widget->header()->setModel(tmp_header_model);
    m_ui->projects_grants_tree_widget->setColumnWidth(0, 400);

    m_username = in_username;
    m_ui->projects_grants_tree_widget->expandAll();

}

FormUserProjectsGrants::~FormUserProjectsGrants()
{
    delete m_ui;
}

void FormUserProjectsGrants::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


void FormUserProjectsGrants::accept()
{
    if (m_projects_grants_tree_model->submit())
    {
        QDialog::accept();
    }
    else
    {
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getLastErrorMessage());
    }
}

void FormUserProjectsGrants::reject()
{
    m_projects_grants_tree_model->revert();
    QDialog::reject();
}
