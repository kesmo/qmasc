/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormBug.h"
#include "session.h"
#include "gui/components/RecordTextEdit.h"
#include "FormExecutionCampaign.h"

#include "entities/project.h"
#include "entities/action.h"
#include "entities/bug.h"
#include "entities/executiontestparameter.h"

#include <QMessageBox>


FormBug::FormBug(ProjectVersion *in_project_version, ExecutionTest *in_execution_test, ExecutionAction *in_execution_action, Bug *in_bug, QWidget *parent, Qt::WindowFlags in_windows_flags)
    : AbstractProjectWidget(in_bug, parent, in_windows_flags)
{
    QString tmp_base_url;
    QString tmp_url;

    m_bugtracker = NULL;
    m_bugtracker_connection_active = false;

    m_ui = new  Ui_FormBug;

    m_ui->setupUi(this);

    m_project_version = in_project_version;
    m_execution_test = in_execution_test;
    m_execution_action = in_execution_action;
    m_bug = in_bug;

    m_new_bug = (m_bug == NULL);
    if (m_new_bug)
        m_bug = new Bug();

    if (m_project_version == NULL) {
        if (m_execution_test != NULL && m_execution_test->projectTest() != NULL) {
            m_project_version = m_execution_test->projectTest()->projectVersion();
        }
    }


    if (m_project_version != NULL) {
        tmp_base_url = m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST);
        tmp_url = m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL);

        if (tmp_base_url.isEmpty() && tmp_url.isEmpty()) {
            m_ui->priorite->addItem("P1");
            m_ui->priorite->addItem("P2");
            m_ui->priorite->addItem("P3");
            m_ui->priorite->addItem("P4");
            m_ui->priorite->addItem("P5");

            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Bloquant"));
            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Critique"));
            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Majeur"));
            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Normal"));
            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Mineur"));
            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Banal"));
            m_ui->gravite->addItem(TR_CUSTOM_MESSAGE("Evolution"));

            m_ui->plateforme->addItem(TR_CUSTOM_MESSAGE("Toutes"));
            m_ui->plateforme->addItem(TR_CUSTOM_MESSAGE("PC"));
            m_ui->plateforme->addItem(TR_CUSTOM_MESSAGE("Macintosh"));
            m_ui->plateforme->addItem(TR_CUSTOM_MESSAGE("Autre"));

            m_ui->systeme->addItem(TR_CUSTOM_MESSAGE("Tous"));
            m_ui->systeme->addItem(TR_CUSTOM_MESSAGE("Windows"));
            m_ui->systeme->addItem(TR_CUSTOM_MESSAGE("Mac OS"));
            m_ui->systeme->addItem(TR_CUSTOM_MESSAGE("Linux"));
            m_ui->systeme->addItem(TR_CUSTOM_MESSAGE("UNIX"));
            m_ui->systeme->addItem(TR_CUSTOM_MESSAGE("Autre"));
        }
        else {
            if (!tmp_base_url.isEmpty())
                m_ui->bugtracker_url->setText("<a href=\"" + tmp_base_url + "\">" + tmp_base_url + "</a>");
            else if (!tmp_url.isEmpty())
                m_ui->bugtracker_url->setText("<a href=\"" + tmp_url + "\">" + tmp_url + "</a>");

            ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
            if (tmp_bugtracker_module != NULL) {
                m_bugtracker = static_cast<BugtrackerModule*>(tmp_bugtracker_module)->createBugtracker();

                if (m_bugtracker != NULL) {
                    if (!tmp_base_url.isEmpty())
                        m_bugtracker->setBaseUrl(tmp_base_url);

                    if (!tmp_url.isEmpty())
                        m_bugtracker->setWebserviceUrl(tmp_url);

                    m_ui->username->setText(Session::instance().getBugtrackersCredentials()[tmp_bugtracker_module->getModuleName()].first);
                    m_ui->password->setText(Session::instance().getBugtrackersCredentials()[tmp_bugtracker_module->getModuleName()].second);

                    connect(m_ui->connect_button, SIGNAL(clicked()), this, SLOT(loginBugtracker()));
                    connect(m_bugtracker, SIGNAL(error(QString)), this, SLOT(bugtrackerError(QString)));
                }
            }
        }

        m_ui->bug_status->addItem(TR_CUSTOM_MESSAGE("Ouvert"), BUG_STATUS_OPENED);
        m_ui->bug_status->addItem(TR_CUSTOM_MESSAGE("Fermé"), BUG_STATUS_CLOSED);

        connect(m_ui->buttonBox->button(QDialogButtonBox::Save), SIGNAL(clicked()), this, SLOT(save()));
        connect(m_ui->buttonBox->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), this, SLOT(cancel()));

        connect(m_ui->execution_button, SIGNAL(clicked()), this, SLOT(openExecutionCampaign()));

        updateControls();
    }
}

FormBug::~FormBug()
{
    if (m_bugtracker != NULL) {
        ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
        if (tmp_bugtracker_module != NULL) {
            static_cast<BugtrackerModule*>(tmp_bugtracker_module)->destroyBugtracker(m_bugtracker);
        }
    }

    if (m_new_bug)
        delete m_bug;

    delete m_ui;
}

QIcon FormBug::icon() const
{
    return QPixmap(":/images/22x22/bug.png");
}

QString FormBug::title() const
{
    if (m_bug)
        return m_bug->getValueForKey(BUGS_TABLE_DESCRIPTION);

    return QString();
}

void FormBug::save()
{
    bool    tmp_has_bugtracker_id = is_empty_string(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE;

    if (m_ui->resume->text().isEmpty() || m_ui->description->toPlainText().isEmpty()) {
        QMessageBox::critical(this, tr("Données non saisies"), tr("Le résumé de l'anomalie ainsi que sa description sont nécessaires."));
    }
    else {
        if (m_bugtracker == NULL) {
            saveBug(QString());
        }
        else {
            if (tmp_has_bugtracker_id) {
                int tmp_save_result = m_bug->saveRecord();
                if (tmp_save_result != NOERR) {
                    QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
                }
                else {
                    emit recordSaved(m_bug);
                    close();
                }
            }
            else
                addBugtrackerIssue();
        }
    }
}


void FormBug::loginBugtracker()
{
    m_bugtracker->setCredential(m_ui->username->text(), m_ui->password->text());
    connect(m_bugtracker, SIGNAL(loginSignal()), this, SLOT(bugtrackerLogin()));

    m_bugtracker->login();

    if (m_bugtracker_connection_active) {
    }
    else {
    }
}

void FormBug::logoutBugtracker()
{
    connect(m_bugtracker, SIGNAL(logoutSignal()), this, SLOT(bugtrackerLogout()));

    m_bugtracker->logout();
}


void FormBug::loadBugtrackerProjectsInformations(QList<QString> in_projects_ids)
{
    connect(m_bugtracker, SIGNAL(projectsInformations(QList<QPair<QString, QString> >)), this, SLOT(bugtrackerProjectsInformations(QList<QPair<QString, QString> >)));

    m_bugtracker->getProjectsInformations(in_projects_ids);
}


void FormBug::loadBugtrackerPriorities()
{
    connect(m_bugtracker, SIGNAL(priorities(QMap<QString, QString>)), this, SLOT(bugtrackerPriorities(QMap<QString, QString>)));

    m_bugtracker->getPriorities();
}


void FormBug::loadBugtrackerSeverities()
{
    connect(m_bugtracker, SIGNAL(severities(QMap<QString, QString>)), this, SLOT(bugtrackerSeverities(QMap<QString, QString>)));

    m_bugtracker->getSeverities();
}


void FormBug::loadBugtrackerReproducibilities()
{
    connect(m_bugtracker, SIGNAL(reproducibilities(QMap<QString, QString>)), this, SLOT(bugtrackerReproducibilities(QMap<QString, QString>)));

    m_bugtracker->getReproducibilities();
}


void FormBug::loadBugtrackerProjectComponents()
{
    connect(m_bugtracker, SIGNAL(projectComponents(QList<QString>)), this, SLOT(bugtrackerProjectComponents(QList<QString>)));

    m_bugtracker->getProjectComponents(m_project_id);
}


void FormBug::loadBugtrackerProjectVersions()
{
    connect(m_bugtracker, SIGNAL(projectVersions(QList<QString>)), this, SLOT(bugtrackerProjectVersions(QList<QString>)));

    m_bugtracker->getProjectVersions(m_project_id);
}


void FormBug::loadBugtrackerPlatforms()
{
    connect(m_bugtracker, SIGNAL(platforms(QList<QString>)), this, SLOT(bugtrackerPlatforms(QList<QString>)));

    m_bugtracker->getPlatforms();
}


void FormBug::loadBugtrackerOsTypes()
{
    connect(m_bugtracker, SIGNAL(operatingSystems(QList<QString>)), this, SLOT(bugtrackerOperatingSystems(QList<QString>)));

    m_bugtracker->getOperatingSystems();
}


void FormBug::loadBugtrackerAvailableProjectsIds()
{
    connect(m_bugtracker, SIGNAL(availableProjectsIds(QList<QString>)), this, SLOT(bugtrackerAvailableProjectsIds(QList<QString>)));

    m_bugtracker->getAvailableProjectsIds();
}


void FormBug::addBugtrackerIssue()
{
    connect(m_bugtracker, SIGNAL(bugCreated(QString)), this, SLOT(bugtrackerBugCreated(QString)));

    m_bugtracker->addBug(m_project_id,
                         m_project_name,
                         m_ui->composant->itemText(m_ui->composant->currentIndex()),
                         m_ui->version->itemText(m_ui->version->currentIndex()),
                         m_execution_test->executionCampaign()->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_REVISION),
                         m_ui->plateforme->itemText(m_ui->plateforme->currentIndex()),
                         m_ui->systeme->itemText(m_ui->systeme->currentIndex()),
                         m_ui->resume->text(),
                         m_ui->description->toPlainText(),
                         m_ui->priorite->itemData(m_ui->priorite->currentIndex()),
                         m_ui->gravite->itemData(m_ui->gravite->currentIndex()),
                         m_ui->reproductibilite->itemData(m_ui->reproductibilite->currentIndex())
    );
}


/* Traitement des réponses du bugtracker */
void FormBug::bugtrackerLogin()
{
    bool    tmp_has_bugtracker_id = is_empty_string(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE;

    disconnect(m_bugtracker, SIGNAL(loginSignal()), this, SLOT(bugtrackerLogin()));

    m_bugtracker_connection_active = true;
    if (tmp_has_bugtracker_id) {
        connect(m_bugtracker, SIGNAL(bugsInformations(QMap< QString, QMap<Bugtracker::BugProperty, QString> >)), this, SLOT(updateBugFromBugtracker(QMap< QString, QMap<Bugtracker::BugProperty, QString> >)));
        m_bugtracker->getBug(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
    }
    else
        loadBugtrackerPriorities();
}


void FormBug::bugtrackerLogout()
{
    disconnect(m_bugtracker, SIGNAL(logoutSignal()), this, SLOT(bugtrackerLogout()));

    m_bugtracker_connection_active = false;
}



void FormBug::bugtrackerPriorities(QMap<QString, QString> priorities)
{
    disconnect(m_bugtracker, SIGNAL(priorities(QMap<QString, QString>)), this, SLOT(bugtrackerPriorities(QMap<QString, QString>)));

    m_ui->priorite->clear();
    foreach(QString priority_id, priorities.keys())
    {
        m_ui->priorite->addItem(priorities.value(priority_id), priority_id);
    }

    loadBugtrackerSeverities();
}


void FormBug::bugtrackerSeverities(QMap<QString, QString> severities)
{
    disconnect(m_bugtracker, SIGNAL(severities(QMap<QString, QString>)), this, SLOT(bugtrackerSeverities(QMap<QString, QString>)));

    m_ui->gravite->clear();
    foreach(QString severity_id, severities.keys())
    {
        m_ui->gravite->addItem(severities.value(severity_id), severity_id);
    }

    loadBugtrackerReproducibilities();
}


void FormBug::bugtrackerReproducibilities(QMap<QString, QString> reproducibilities)
{
    disconnect(m_bugtracker, SIGNAL(reproducibilities(QMap<QString, QString>)), this, SLOT(bugtrackerReproducibilities(QMap<QString, QString>)));

    m_ui->reproductibilite->clear();
    foreach(QString reproducibility_id, reproducibilities.keys())
    {
        m_ui->reproductibilite->addItem(reproducibilities.value(reproducibility_id), reproducibility_id);
    }

    loadBugtrackerPlatforms();
}



void FormBug::bugtrackerPlatforms(QList<QString> platforms)
{
    disconnect(m_bugtracker, SIGNAL(platforms(QList<QString>)), this, SLOT(bugtrackerPlatforms(QList<QString>)));

    m_ui->plateforme->clear();
    foreach(QString platform, platforms)
    {
        m_ui->plateforme->addItem(platform);
    }

    loadBugtrackerOsTypes();
}


void FormBug::bugtrackerOperatingSystems(QList<QString> operatingSystems)
{
    disconnect(m_bugtracker, SIGNAL(operatingSystems(QList<QString>)), this, SLOT(bugtrackerOperatingSystems(QList<QString>)));

    m_ui->systeme->clear();
    foreach(QString operatingSystem, operatingSystems)
    {
        m_ui->systeme->addItem(operatingSystem);
    }

    loadBugtrackerAvailableProjectsIds();
}


void FormBug::bugtrackerAvailableProjectsIds(QList<QString> in_projects_ids)
{
    disconnect(m_bugtracker, SIGNAL(availableProjectsIds(QList<QString>)), this, SLOT(bugtrackerAvailableProjectsIds(QList<QString>)));

    loadBugtrackerProjectsInformations(in_projects_ids);
}



void FormBug::bugtrackerProjectsInformations(QList< QPair<QString, QString> > in_projects_infos)
{
    bool                    tmp_found_project = false;
    QPair<QString, QString>  tmp_project_infos;

    disconnect(m_bugtracker, SIGNAL(projectsInformations(QList<QPair<QString, QString> >)), this, SLOT(bugtrackerProjectsInformations(QList<QPair<QString, QString> >)));

    // Vérifier l'existance du projet courant
    // Recherche le projet courant par son identifiant paramétré au niveau des propriétés de la version de projet
    if (is_empty_string(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID)) == FALSE) {
        foreach(tmp_project_infos, in_projects_infos)
        {
            if (compare_values(tmp_project_infos.first.toStdString().c_str(), m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID)) == 0) {
                m_project_id = m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID);
                m_project_name = tmp_project_infos.second;
                tmp_found_project = true;
                break;
            }
        }
    }
    else {
        // Recherche le produit courant par son nom de projet
        foreach(tmp_project_infos, in_projects_infos)
        {
            if (compare_values(tmp_project_infos.second.toStdString().c_str(), m_project_version->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME)) == 0) {
                m_project_id = tmp_project_infos.first;
                m_project_name = m_project_version->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME);
                tmp_found_project = true;
                break;
            }
        }
    }

    if (!tmp_found_project) {
        QMessageBox::critical(this, tr("Projet introuvable"), tr("Le projet %1 n'a pas été trouvé dans le bugtracker.").arg(m_project_version->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME)));
        m_bugtracker->disconnect();
    }
    else
        loadBugtrackerProjectComponents();
}


void FormBug::bugtrackerProjectComponents(QList<QString> componants)
{
    disconnect(m_bugtracker, SIGNAL(projectComponents(QList<QString>)), this, SLOT(bugtrackerProjectComponents(QList<QString>)));

    m_ui->composant->clear();
    foreach(QString componant, componants)
    {
        m_ui->composant->addItem(componant);
    }

    loadBugtrackerProjectVersions();
}


void FormBug::bugtrackerProjectVersions(QList<QString> versions)
{
    bool    tmp_current_version_found = false;
    QString tmp_bugtracker_project_version = m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION);
    QString tmp_project_version = ProjectVersion::formatProjectVersionNumber(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

    disconnect(m_bugtracker, SIGNAL(projectVersions(QList<QString>)), this, SLOT(bugtrackerProjectVersions(QList<QString>)));

    m_ui->version->clear();
    foreach(QString version, versions)
    {
        m_ui->version->addItem(version);
    }


    // Recherche de la version correspondante paramétrée au niveau des propriétés de la version de projet
    if (!tmp_bugtracker_project_version.isEmpty()) {
        int     tmp_index = 0;
        foreach(QString tmp_version, versions)
        {
            if (compare_values(tmp_bugtracker_project_version.toStdString().c_str(), tmp_version.toStdString().c_str()) == 0) {
                tmp_current_version_found = true;
                m_ui->version->setCurrentIndex(tmp_index);
            }
            tmp_index++;
        }
    }

    // Recherche de la version correspondante à la version du projet
    if (!tmp_current_version_found) {
        int     tmp_index = 0;
        foreach(QString tmp_version, versions)
        {
            if (compare_values(tmp_project_version.toStdString().c_str(), tmp_version.toStdString().c_str()) == 0) {
                tmp_current_version_found = true;
                m_ui->version->setCurrentIndex(tmp_index);
            }
            tmp_index++;
        }
    }

    if (!tmp_current_version_found) {
        if (tmp_bugtracker_project_version.isEmpty())
            QMessageBox::warning(this, tr("Version non trouvée"), tr("La version courante du projet (%1) n'existe pas dans le bugtracker cible.").arg(tmp_project_version));
        else
            QMessageBox::warning(this, tr("Version non trouvée"), tr("La version du projet prédéfinie (%1) n'existe pas dans le bugtracker cible.").arg(tmp_bugtracker_project_version));
    }

    updateControls();
}



void FormBug::bugtrackerBugCreated(QString in_bug_id)
{
    disconnect(m_bugtracker, SIGNAL(bugCreated(QString)), this, SLOT(bugtrackerBugCreated(QString)));

    saveBug(in_bug_id);
    //logoutBugtracker();
}



void FormBug::bugtrackerError(QString errorMsg)
{
    QMessageBox::critical(this, tr("Erreur"), errorMsg + "<p><span style=\"color: red\"><b>" + tr("La requête a échouée.") + "</b></span></p>");
}


void FormBug::saveBug(QString in_bugtracker_bug_id)
{
    int                tmp_save_result = NOERR;

    m_bug->setValueForKey(CLIENT_MACRO_NOW, BUGS_TABLE_CREATION_DATE);
    m_bug->setValueForKey(m_ui->resume->text().toStdString().c_str(), BUGS_TABLE_SHORT_NAME);
    m_bug->setValueForKey(m_ui->description->toPlainText().toStdString().c_str(), BUGS_TABLE_DESCRIPTION);
    m_bug->setValueForKey(m_ui->priorite->itemText(m_ui->priorite->currentIndex()).toStdString().c_str(), BUGS_TABLE_PRIORITY);
    m_bug->setValueForKey(m_ui->gravite->itemText(m_ui->gravite->currentIndex()).toStdString().c_str(), BUGS_TABLE_SEVERITY);
    m_bug->setValueForKey(m_ui->reproductibilite->itemText(m_ui->reproductibilite->currentIndex()).toStdString().c_str(), BUGS_TABLE_REPRODUCIBILITY);
    m_bug->setValueForKey(m_ui->plateforme->itemText(m_ui->plateforme->currentIndex()).toStdString().c_str(), BUGS_TABLE_PLATFORM);
    m_bug->setValueForKey(m_ui->systeme->itemText(m_ui->systeme->currentIndex()).toStdString().c_str(), BUGS_TABLE_SYSTEM);
    m_bug->setValueForKey(m_ui->bug_status->itemData(m_ui->bug_status->currentIndex()).toString().toStdString().c_str(), BUGS_TABLE_STATUS);

    if (m_execution_test != NULL)
        m_bug->setValueForKey(m_execution_test->getIdentifier(), BUGS_TABLE_EXECUTION_TEST_ID);

    if (m_execution_action != NULL)
        m_bug->setValueForKey(m_execution_action->getIdentifier(), BUGS_TABLE_EXECUTION_ACTION_ID);

    if (!in_bugtracker_bug_id.isEmpty())
        m_bug->setValueForKey(in_bugtracker_bug_id.toStdString().c_str(), BUGS_TABLE_BUGTRACKER_BUG_ID);

    tmp_save_result = m_bug->saveRecord();
    if (tmp_save_result != NOERR) {
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
    }

    emit recordSaved(m_bug);
    close();
}


void FormBug::updateControls()
{
    bool    tmp_has_bugtracker = (m_bugtracker != NULL);
    bool    tmp_has_bugtracker_id = is_empty_string(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID)) == FALSE;

    int        tmp_index = 0;

    QString    tmp_url;
    QString    tmp_external_link;

    ExecutionTest        *tmp_parent_execution_test = NULL;

    QString                tmp_bug_description = tr("Scénario : ");

    bool    tmp_write_access = m_project_version != NULL && m_project_version->canWriteExecutionsCampaigns();

    bool    tmp_read_write = tmp_write_access && !tmp_has_bugtracker_id && (!tmp_has_bugtracker || m_bugtracker_connection_active);

    m_ui->resume->setText(m_bug->getValueForKey(BUGS_TABLE_SHORT_NAME));

    if (is_empty_string(m_bug->getIdentifier()) && m_execution_test != NULL) {
        tmp_parent_execution_test = m_execution_test;
        while (ExecutionTest::ChildRelation::instance().getParent(tmp_parent_execution_test) != NULL) {
            tmp_parent_execution_test = ExecutionTest::ChildRelation::instance().getParent(tmp_parent_execution_test);
        }

        generateBugDescriptionUntilAction(tmp_parent_execution_test, tmp_bug_description, "", m_execution_test, m_execution_action);
        m_ui->description->setPlainText(tmp_bug_description);
    }
    else
        m_ui->description->setPlainText(m_bug->getValueForKey(BUGS_TABLE_DESCRIPTION));

    if (tmp_has_bugtracker && is_empty_string(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST)) == FALSE) {
        tmp_url = m_bugtracker->urlForBugWithId(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
        if (tmp_url.isEmpty())
            tmp_external_link = QString(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));
        else
            tmp_external_link = "<a href=\"" + tmp_url + "\">" + tmp_url + "</a>";
    }
    else
        tmp_external_link = QString(m_bug->getValueForKey(BUGS_TABLE_BUGTRACKER_BUG_ID));

    m_ui->url_externe->setText(tmp_external_link);

    // Priorité
    if (is_empty_string(m_bug->getValueForKey(BUGS_TABLE_PRIORITY)) == FALSE) {
        tmp_index = m_ui->priorite->findText(m_bug->getValueForKey(BUGS_TABLE_PRIORITY));
        if (tmp_index < 0) {
            m_ui->priorite->addItem(m_bug->getValueForKey(BUGS_TABLE_PRIORITY));
            m_ui->priorite->setCurrentIndex(m_ui->priorite->count() - 1);
        }
        else
            m_ui->priorite->setCurrentIndex(tmp_index);
    }

    // Gravité
    if (is_empty_string(m_bug->getValueForKey(BUGS_TABLE_SEVERITY)) == FALSE) {
        tmp_index = m_ui->gravite->findText(m_bug->getValueForKey(BUGS_TABLE_SEVERITY));
        if (tmp_index < 0) {
            m_ui->gravite->addItem(m_bug->getValueForKey(BUGS_TABLE_SEVERITY));
            m_ui->gravite->setCurrentIndex(m_ui->gravite->count() - 1);
        }
        else
            m_ui->gravite->setCurrentIndex(tmp_index);
    }

    // Reproductibilité
    if (is_empty_string(m_bug->getValueForKey(BUGS_TABLE_REPRODUCIBILITY)) == FALSE) {
        tmp_index = m_ui->reproductibilite->findText(m_bug->getValueForKey(BUGS_TABLE_REPRODUCIBILITY));
        if (tmp_index < 0) {
            m_ui->reproductibilite->addItem(m_bug->getValueForKey(BUGS_TABLE_REPRODUCIBILITY));
            m_ui->reproductibilite->setCurrentIndex(m_ui->reproductibilite->count() - 1);
        }
        else
            m_ui->reproductibilite->setCurrentIndex(tmp_index);
    }

    // Plateforme
    if (is_empty_string(m_bug->getValueForKey(BUGS_TABLE_PLATFORM)) == FALSE) {
        tmp_index = m_ui->plateforme->findText(m_bug->getValueForKey(BUGS_TABLE_PLATFORM));
        if (tmp_index < 0) {
            m_ui->plateforme->addItem(m_bug->getValueForKey(BUGS_TABLE_PLATFORM));
            m_ui->plateforme->setCurrentIndex(m_ui->plateforme->count() - 1);
        }
        else
            m_ui->plateforme->setCurrentIndex(tmp_index);
    }

    // OS
    if (is_empty_string(m_bug->getValueForKey(BUGS_TABLE_SYSTEM)) == FALSE) {
        tmp_index = m_ui->systeme->findText(m_bug->getValueForKey(BUGS_TABLE_SYSTEM));
        if (tmp_index < 0) {
            m_ui->systeme->addItem(m_bug->getValueForKey(BUGS_TABLE_SYSTEM));
            m_ui->systeme->setCurrentIndex(m_ui->systeme->count() - 1);
        }
        else
            m_ui->systeme->setCurrentIndex(tmp_index);
    }

    // Status
    if (is_empty_string(m_bug->getValueForKey(BUGS_TABLE_STATUS)) == FALSE) {
        tmp_index = m_ui->bug_status->findData(m_bug->getValueForKey(BUGS_TABLE_STATUS));
        if (tmp_index < 0) {
            m_ui->bug_status->addItem(m_bug->getValueForKey(BUGS_TABLE_STATUS));
            m_ui->bug_status->setCurrentIndex(m_ui->bug_status->count() - 1);
        }
        else
            m_ui->bug_status->setCurrentIndex(tmp_index);
    }
    else
        m_ui->status_widget->setVisible(false);

    m_ui->url_externe->setVisible(tmp_has_bugtracker_id);
    m_ui->bugtracker_group_box->setVisible(!m_bugtracker_connection_active && tmp_has_bugtracker);
    m_ui->bugtracker_component_frame->setVisible(tmp_has_bugtracker && !tmp_has_bugtracker_id);

    m_ui->connect_button->setEnabled(!m_bugtracker_connection_active || tmp_has_bugtracker_id);
    m_ui->username->setEnabled(!m_bugtracker_connection_active);
    m_ui->password->setEnabled(!m_bugtracker_connection_active);

    m_ui->composant->setEnabled(m_bugtracker_connection_active);
    m_ui->version->setEnabled(m_bugtracker_connection_active);

    m_ui->gravite->setEnabled(tmp_read_write);
    m_ui->priorite->setEnabled(tmp_read_write);
    m_ui->reproductibilite->setEnabled(tmp_read_write);
    m_ui->plateforme->setEnabled(tmp_read_write);
    m_ui->systeme->setEnabled(tmp_read_write);
    m_ui->resume->setEnabled(tmp_read_write);

    m_ui->description->setReadOnly(!tmp_read_write);

    m_ui->bug_status->setEnabled(tmp_read_write);

    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(tmp_write_access && (!tmp_has_bugtracker || m_bugtracker_connection_active));

    m_ui->execution_button->setVisible(m_execution_test == NULL);

    m_ui->read_warning_widget->setVisible(m_bugtracker_connection_active && tmp_has_bugtracker_id);
}


void FormBug::updateBugFromBugtracker(QMap< QString, QMap<Bugtracker::BugProperty, QString> > in_bugs_list)
{
    QMap< QString, QMap<Bugtracker::BugProperty, QString> >::iterator tmp_bug_iterator;

    for (tmp_bug_iterator = in_bugs_list.begin(); tmp_bug_iterator != in_bugs_list.end(); tmp_bug_iterator++) {
        // Mettre à jour le bug
        QMap<Bugtracker::BugProperty, QString> tmp_bug_infos = tmp_bug_iterator.value();

        if (!tmp_bug_infos[Bugtracker::Priority].isEmpty())
            m_bug->setValueForKey(tmp_bug_infos[Bugtracker::Priority].toStdString().c_str(), BUGS_TABLE_PRIORITY);

        if (!tmp_bug_infos[Bugtracker::Severity].isEmpty())
            m_bug->setValueForKey(tmp_bug_infos[Bugtracker::Severity].toStdString().c_str(), BUGS_TABLE_SEVERITY);

        if (!tmp_bug_infos[Bugtracker::Summary].isEmpty())
            m_bug->setValueForKey(tmp_bug_infos[Bugtracker::Summary].toStdString().c_str(), BUGS_TABLE_SHORT_NAME);

        if (!tmp_bug_infos[Bugtracker::Status].isEmpty())
            m_bug->setValueForKey(tmp_bug_infos[Bugtracker::Status].toStdString().c_str(), BUGS_TABLE_STATUS);
    }

    updateControls();
}


void FormBug::openExecutionCampaign()
{
    FormExecutionCampaign     *tmp_dialog = NULL;
    ExecutionCampaign         *tmp_execution_campaign = NULL;
    Campaign                  *tmp_campaign = NULL;
    QList<ExecutionCampaign*> tmp_executions;

    net_session_print_where_clause(Session::instance().getClientSession(), "%s IN (SELECT %s FROM %s WHERE %s=%s)",
                                   EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID,
                                   EXECUTIONS_TESTS_TABLE_EXECUTION_CAMPAIGN_ID,
                                   EXECUTIONS_TESTS_TABLE_SIG,
                                   EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID,
                                   m_bug->getValueForKey(BUGS_TABLE_EXECUTION_TEST_ID));

    tmp_executions = ExecutionCampaign::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer);
    if (tmp_executions.size() == 1) {
        tmp_execution_campaign = tmp_executions[0];
    }
    else
        qDeleteAll(tmp_executions);

    if (tmp_execution_campaign != NULL) {
        foreach(Campaign* tmp_cmp, ProjectVersion::CampaignRelation::instance().getChilds(m_project_version))
        {
            if (compare_values(tmp_cmp->getIdentifier(), tmp_execution_campaign->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_CAMPAIGN_ID)) == 0) {
                tmp_campaign = tmp_cmp;
            }
        }
        if (tmp_campaign != NULL) {
            tmp_dialog = new FormExecutionCampaign(tmp_execution_campaign, parentWidget());
            tmp_dialog->showMaximized();
            tmp_dialog->showTestInfosForTestWithId(m_bug->getValueForKey(BUGS_TABLE_EXECUTION_TEST_ID));
        }
        else
            delete tmp_execution_campaign;
    }
}


bool FormBug::generateBugDescriptionUntilAction(ExecutionTest *in_root_test, QString & description, QString in_prefix, ExecutionTest *in_test, ExecutionAction *in_action)
{
    if (in_root_test != NULL) {
        description.append(in_prefix);
        if (in_root_test->projectTest() != NULL) {
            description.append(in_root_test->projectTest()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME));
        }
        description.append("\n");

        if (in_test == in_root_test) {
            int    tmp_action_index = 1;

            foreach(ExecutionAction *tmp_execution_action, in_root_test->actions())
            {
                QTextDocument    tmp_doc;
                Action *tmp_action = Action::ExecutionActionRelation::instance().getParent(tmp_execution_action);

                tmp_doc.setHtml(RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(
                    in_test->inheritedParameters(), tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION)));
                description.append(tr("Action %1 => ").arg(tmp_action_index));
                description.append(tmp_doc.toPlainText());
                description.append("\n");

                tmp_doc.setHtml(RecordTextEdit::toHtmlWithParametersValues<ExecutionTestParameter>(
                    in_test->inheritedParameters(), tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT)));
                description.append(tr("Résultat attendu => "));
                description.append(tmp_doc.toPlainText());
                description.append("\n");

                if (in_action != NULL && tmp_execution_action == in_action) {
                    tmp_doc.setHtml(tmp_execution_action->getValueForKey(EXECUTIONS_ACTIONS_TABLE_COMMENTS));
                    description.append(tr("Problème rencontré => "));
                    description.append(tmp_doc.toPlainText());
                    description.append("\n");

                    return true;
                }

                tmp_action_index++;
            }

            return true;
        }

        int    tmp_test_index = 1;

        foreach(ExecutionTest *tmp_child_test, in_root_test->getAllChilds())
        {
            if (generateBugDescriptionUntilAction(tmp_child_test, description, in_prefix + QString::number(tmp_test_index) + ".", in_test, in_action))
                return true;

            tmp_test_index++;
        }
    }

    return false;
}
