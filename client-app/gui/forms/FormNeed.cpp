#include "FormNeed.h"
#include "ui_FormNeed.h"

#include "session.h"

#include <QPushButton>
#include <QMessageBox>

FormNeed::FormNeed(Need* in_need, QWidget *parent)
    : AbstractProjectWidget(in_need, parent),
    m_ui(new Ui::FormNeed),
    m_need(in_need)
{
    m_ui->setupUi(this);
    loadStyleSheet(this,
                   "#aeb240",
                   "#979959",
                   "#65663c",
                   "#a9ab69",
                   "#b8ba78",
                   "#85874a",
                   "#e8e8df",
                   "#e4e87d",
                   "#bfc422",
                   "#979959");

    m_ui->need_description->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->need_id->setVisible(Session::instance().getClientSession()->m_debug);


    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    connect(m_ui->need_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->need_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));

    loadNeed(in_need);

}

FormNeed::~FormNeed()
{
    delete m_ui;
}

QIcon FormNeed::icon() const
{
    return QPixmap(":/images/22x22/need.png");
}

QString FormNeed::title() const
{
    if (m_need)
        return m_need->getValueForKey(NEEDS_TABLE_SHORT_NAME);

    return QString();
}


void FormNeed::loadNeed(Need* in_need)
{
    m_ui->need_name->setFocus();

    m_ui->lock_widget->setVisible(false);

    if (in_need != NULL)
    {
        m_need = in_need;

        setWindowTitle(tr("Besoin : %1").arg(m_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)));

        m_ui->need_id->setText(m_need->getIdentifier());

        if (m_need->isModifiable())
        {
            m_need->lockRecord(true);

            if (m_need->lockRecordStatus() == RECORD_STATUS_LOCKED)
            {
                m_ui->lock_widget->setVisible(true);
                net_get_field(NET_MESSAGE_TYPE_INDEX+1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
                m_ui->label_lock_by->setText(tr("Verrouillé par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            }
            else
                setModifiable(true);
        }

        m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

        m_ui->need_name->setText(m_need->getValueForKey(NEEDS_TABLE_SHORT_NAME));
        m_ui->need_name->setEnabled(isModifiable());
        m_ui->need_description->textEditor()->setText(m_need->getValueForKey(NEEDS_TABLE_DESCRIPTION));
        m_ui->need_description->textEditor()->setReadOnly(!isModifiable());
        if (isModifiable())
            m_ui->need_description->toolBar()->show();
        else
            m_ui->need_description->toolBar()->hide();

        setModified(false);
    }
    else
        cancel();
}


bool FormNeed::saveRecord()
{
    int                 tmp_save_result = NOERR;

    m_need->setValueForKey(m_ui->need_name->text().toStdString().c_str(), NEEDS_TABLE_SHORT_NAME);
    m_need->setValueForKey(m_ui->need_description->textEditor()->toOptimizedHtml().toStdString().c_str(), NEEDS_TABLE_DESCRIPTION);
    tmp_save_result = m_need->saveRecord();
    if (tmp_save_result != NOERR)
    {
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
        return false;
    }

    setModified(false);

    return true;
}
