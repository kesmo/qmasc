/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormProjectSelection.h"

#include "FormNewVersion.h"
#include "client.h"
#include "session.h"

#include <QMessageBox>
#include <QComboBox>
#include <QPushButton>
#include <QSettings>

/**
  Constructeur
**/
FormProjectSelection::FormProjectSelection(QWidget *parent) :
    QDialog(parent)
{
    unsigned long   tmp_index = 0;

    QSettings        tmp_settings;


    QString        tmp_last_opened_project_id = tmp_settings.value("last_opened_project_id", "").toString();

    int            tmp_select_project_index = 0;

    setAttribute(Qt::WA_DeleteOnClose);

    m_ui = new  Ui_FormProjectSelection();

    m_ui->setupUi(this);
    m_ui->projects_list->setFocus();

    connect(m_ui->button_open, SIGNAL(clicked()), this, SLOT(accept()));
    connect(m_ui->button_new_version, SIGNAL(clicked()), this, SLOT(newVersion()));
    connect(m_ui->button_cancel, SIGNAL(clicked()), this, SLOT(reject()));

    if (Session::instance().currentUserRoles().contains("admin_role")) {
        connect(m_ui->button_delete_project, SIGNAL(clicked()), this, SLOT(deleteProject()));
        connect(m_ui->button_delete_project_version, SIGNAL(clicked()), this, SLOT(deleteProjectVersion()));
    }
    else {
        m_ui->button_delete_project->setVisible(false);
        m_ui->button_delete_project_version->setVisible(false);
    }

    net_session_print_where_clause(Session::instance().getClientSession(), "%s IN (SELECT %s FROM %s WHERE %s.%s=%s.%s AND %s='%s')",
            PROJECTS_TABLE_PROJECT_ID,
            PROJECTS_GRANTS_TABLE_PROJECT_ID,
            PROJECTS_GRANTS_TABLE_SIG,
            PROJECTS_GRANTS_TABLE_SIG,
            PROJECTS_GRANTS_TABLE_PROJECT_ID,
            PROJECTS_TABLE_SIG,
            PROJECTS_TABLE_PROJECT_ID,
            PROJECTS_GRANTS_TABLE_USERNAME,
            cl_current_user(Session::instance().getClientSession())
    );

    m_projects_list = Project::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer);
    if (!m_projects_list.isEmpty()) {
        foreach(Project* tmp_project, m_projects_list)
        {
            if (tmp_project != NULL) {
                m_ui->projects_list->addItem(tmp_project->getValueForKey(PROJECTS_TABLE_SHORT_NAME));

                if (!tmp_last_opened_project_id.isEmpty() && tmp_last_opened_project_id.compare(tmp_project->getIdentifier()) == 0) {
                    tmp_select_project_index = tmp_index;
                    m_ui->projects_list->setCurrentIndex(tmp_select_project_index);
                }
                ++tmp_index;
            }

        }
        selectedProjectChanged(tmp_select_project_index);
    }



    connect(m_ui->projects_list, SIGNAL(currentIndexChanged(int)), this, SLOT(selectedProjectChanged(int)));
    connect(m_ui->projects_versions_list, SIGNAL(currentIndexChanged(int)), this, SLOT(selectedVersionChanged(int)));
}


/**
  Destructeur
**/
FormProjectSelection::~FormProjectSelection()
{
    qDeleteAll(m_projects_list);
    delete m_ui;
}


/**
  Changement de selection de projet
**/
void FormProjectSelection::selectedProjectChanged(int in_project_index)
{
    QSettings        tmp_settings;
    QString        tmp_last_opened_project_id = tmp_settings.value("last_opened_project_id", "").toString();
    QString        tmp_last_opened_project_version = tmp_settings.value("last_opened_project_version", "").toString();

    int            tmp_select_project_version_index = 0;

    if (in_project_index >= 0 && in_project_index < m_projects_list.count()) {
        m_ui->project_description->setText(m_projects_list[in_project_index]->getValueForKey(PROJECTS_TABLE_DESCRIPTION));
        m_ui->projects_versions_list->clear();

        m_versions_list = Project::ProjectVersionRelation::instance().getChilds(m_projects_list[in_project_index]);
        tmp_select_project_version_index = m_versions_list.count() - 1;
        for (int tmp_version_index = 0; tmp_version_index < m_versions_list.count(); tmp_version_index++) {
            m_ui->projects_versions_list->addItem(ProjectVersion::formatProjectVersionNumber(m_versions_list[tmp_version_index]->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)));
            if (!tmp_last_opened_project_id.isEmpty() && tmp_last_opened_project_id.compare(m_projects_list[in_project_index]->getIdentifier()) == 0
                && !tmp_last_opened_project_version.isEmpty() && tmp_last_opened_project_version.compare(m_versions_list[tmp_version_index]->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)) == 0) {
                tmp_select_project_version_index = tmp_version_index;
            }
        }
        // Sélection de la dernière version
        m_ui->projects_versions_list->setCurrentIndex(tmp_select_project_version_index);
        selectedVersionChanged(0);
    }
}

/**
  Changement de selection de version
**/
void FormProjectSelection::selectedVersionChanged(int in_version_index)
{
    if (in_version_index >= 0 && in_version_index < m_versions_list.count()) {
        m_ui->project_version_description->setText(m_versions_list[in_version_index]->getValueForKey(PROJECTS_VERSIONS_TABLE_DESCRIPTION));
    }
}


/**
  Validation par le bouton OK
**/
void FormProjectSelection::accept()
{
    if (m_ui->projects_versions_list->currentIndex() >= 0 && m_ui->projects_versions_list->currentIndex() < m_versions_list.count())
        selectVersion(m_versions_list[m_ui->projects_versions_list->currentIndex()]);
}


/**
  Saisie d'une nouvelle version du projet
**/
void FormProjectSelection::newVersion()
{
    FormNewVersion    *tmp_form = NULL;

    if (m_ui->projects_list->currentIndex() >= 0) {
        tmp_form = new FormNewVersion(m_projects_list[m_ui->projects_list->currentIndex()], this);
        connect(tmp_form, &FormNewVersion::versionCreated, this, &FormProjectSelection::selectVersion);

        tmp_form->show();
    }
}


void FormProjectSelection::selectVersion(ProjectVersion *in_project_version)
{
    QSettings        tmp_settings;

    // Conserver le dernier projet ouvert dans les preferences utilisateur
    tmp_settings.setValue("last_opened_project_id", in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID));
    tmp_settings.setValue("last_opened_project_version", in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

    emit projectSelected(in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_project_version->getIdentifier());
    QDialog::accept();
}


void FormProjectSelection::deleteProject()
{
    Project*        tmp_projet = NULL;
    int                tmp_save_result = NOERR;

    if (m_ui->projects_list->currentIndex() >= 0) {
        tmp_projet = m_projects_list[m_ui->projects_list->currentIndex()];
        if (QMessageBox::question(this,
            tr("Confirmation..."),
            tr("Etes-vous sûr(e) de vouloir supprimer le projet %1 ?").arg(tmp_projet->getValueForKey(PROJECTS_TABLE_SHORT_NAME)),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
            tmp_save_result = tmp_projet->deleteRecord();
            if (tmp_save_result != NOERR) {
                QMessageBox::critical(this, tr("Erreur lors de la suppression"), Session::instance().getErrorMessage(tmp_save_result));
            }
            else
                QDialog::accept();
        }
    }
}


void FormProjectSelection::deleteProjectVersion()
{
    Project*            tmp_projet = NULL;
    ProjectVersion*        tmp_projet_version = NULL;
    int                    tmp_save_result = NOERR;

    if (m_ui->projects_list->currentIndex() >= 0 && m_ui->projects_versions_list->currentIndex() >= 0) {
        tmp_projet = m_projects_list[m_ui->projects_list->currentIndex()];
        tmp_projet_version = m_versions_list[m_ui->projects_versions_list->currentIndex()];
        if (QMessageBox::question(
            this,
            tr("Confirmation..."),
            tr("Etes-vous sûr(e) de vouloir supprimer la version %1 du projet %2 ?").arg(ProjectVersion::formatProjectVersionNumber(tmp_projet_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION))).arg(QString(tmp_projet->getValueForKey(PROJECTS_TABLE_SHORT_NAME))),
            QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
            tmp_save_result = tmp_projet_version->deleteRecord();
            if (tmp_save_result != NOERR) {
                QMessageBox::critical(this, tr("Erreur lors de la suppression"), Session::instance().getErrorMessage(tmp_save_result));
            }
            else
                QDialog::accept();
        }
    }
}
