/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormNewProject.h"
#include "ui_FormNewProject.h"

#include "client.h"
#include "constants.h"
#include "session.h"
#include "entities/projectgrant.h"

#include <QMessageBox>

/**
  Constructeur
**/
FormNewProject::FormNewProject(ProjectVersion* in_project_record, QWidget *parent) : QDialog(parent)
{
    QStringList tmp_version_numbers;

    setAttribute(Qt::WA_DeleteOnClose);
    m_project_version = in_project_record;

    m_ui = new  Ui_FormNewProject();

    m_ui->setupUi(this);
    m_ui->project_description->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->project_version_info->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->project_name->setFocus();

    if (m_project_version != NULL)
    {
        m_ui->project_name->setText(m_project_version->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
        m_ui->project_description->textEditor()->setHtml(m_project_version->project()->getValueForKey(PROJECTS_TABLE_DESCRIPTION));
        tmp_version_numbers = QString(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)).split(".");
        if (tmp_version_numbers.count() > 0)
            m_ui->major_version_number->setValue(tmp_version_numbers[0].toInt());
        if (tmp_version_numbers.count() > 1)
            m_ui->medium_version_number->setValue(tmp_version_numbers[1].toInt());
        if (tmp_version_numbers.count() > 2)
            m_ui->minor_version_number->setValue(tmp_version_numbers[2].toInt());
        if (tmp_version_numbers.count() > 3)
            m_ui->maintenance_version_number->setValue(tmp_version_numbers[3].toInt());

        setWindowTitle(m_project_version->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
        m_new_version = false;
    }
    else
    {
        m_ui->major_version_number->setValue(1);
        setWindowTitle(tr("Nouveau projet"));
        m_new_version = true;
    }

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}



/**
  Destruction
**/
FormNewProject::~FormNewProject()
{
    delete m_ui;
}


/**
  Validation par le bouton OK
**/
void FormNewProject::accept()
{
    QString         tmp_version, tmp_major_version, tmp_medium_version, tmp_minor_version, tmp_maintenance_version;
    int             tmp_return = NOERR;

    /* Affecter les nouvelles valeurs */
    if (m_ui->project_name->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Donnée obligatoire non saisi"), tr("Le nom du projet est nécessaire."));
        return;
    }
    else
    {
        if (m_new_version){
            m_project_version = new ProjectVersion();
            Project::ProjectVersionRelation::instance().addChild(new Project(), m_project_version);
        }


        m_project_version->project()->setValueForKey((char*)m_ui->project_name->text().toStdString().c_str(), PROJECTS_TABLE_SHORT_NAME);
        m_project_version->project()->setValueForKey((char*)m_ui->project_description->textEditor()->toOptimizedHtml().toStdString().c_str(), PROJECTS_TABLE_DESCRIPTION);

        tmp_major_version.setNum(m_ui->major_version_number->value());
        tmp_medium_version.setNum(m_ui->medium_version_number->value());
        tmp_minor_version.setNum(m_ui->minor_version_number->value());
        tmp_maintenance_version.setNum(m_ui->maintenance_version_number->value());

        tmp_version = QString("%1.%2.%3.%4")
                .arg(tmp_major_version, 2, '0')
                .arg(tmp_medium_version, 2, '0')
                .arg(tmp_minor_version, 2, '0')
                .arg(tmp_maintenance_version, 2, '0');

        m_project_version->setValueForKey((char*)tmp_version.toStdString().c_str(), PROJECTS_VERSIONS_TABLE_VERSION);
        m_project_version->setValueForKey((char*)m_ui->project_version_info->textEditor()->toOptimizedHtml().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_DESCRIPTION);

        if (m_new_version)
        {
            m_project_version->project()->createProjectGrants(PROJECT_GRANT_WRITE);
            tmp_return = m_project_version->project()->insertRecord();
            if (tmp_return == NOERR)
            {
                tmp_return = m_project_version->saveRecord();
            }
        }
        else
            tmp_return = m_project_version->saveRecord();

        if (tmp_return == NOERR) {
            tmp_return  = Project::ProjectVersionRelation::instance().saveChilds(m_project_version->project());
            if (tmp_return == NOERR) {
                projectSelected(m_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), m_project_version->getIdentifier());
                QDialog::accept();
                return;
            }
        }

        Project::ProjectVersionRelation::instance().reset(m_project_version->project(), true);

        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_return));
    }
}
