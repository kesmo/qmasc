/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_DATA_IMPORT_H
#define FORM_DATA_IMPORT_H

#include <QtWidgets/QDialog>
#include <QFile>
#include <QCloseEvent>
#include <QPair>
#include "gui/components/importexportcontext.h"

#define CHAR_SEPARATOR        '+'
#define PARENT_HIERARCHY_SEPARATOR          '\\'

namespace Ui
{
class FormDataImport;
}

class FormDataImport : public QDialog
{

    Q_OBJECT

public:
    FormDataImport(Gui::Components::ImportContext *in_import_context, QWidget *parent = 0);
    ~FormDataImport();

    static QList<char> stringToSeparatorsList(const QString & in_str, char in_separator);

    public slots:
    virtual void accept();

    protected slots:
    void updateSeparators();
    void updateFileOverview();
    void loadFile();
    void updateMappingTable();

protected:
    void changeEvent(QEvent *e);
    bool setFieldRecordAt(Record *in_record, QByteArray in_field_value, int in_column);
    void setFieldAt(QByteArray in_field_value, int in_row, int in_column);
    bool readFile(bool in_generate_record);

private:
    Ui::FormDataImport *m_ui;

    QFile        m_file;

    Gui::Components::ImportContext* m_import_context;
};

#endif // FORM_DATA_IMPORT_H
