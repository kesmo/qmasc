#ifndef FORM_PROJECT_H
#define FORM_PROJECT_H

#include <QtWidgets/QWidget>
#include "entities/project.h"

#include "gui/components/RecordTextEditContainer.h"
#include "gui/components/AbstractProjectWidget.h"

#include <bugtracker.h>
#include <clientmodule.h>

namespace Ui {
class FormProject;
}

class FormProject : public AbstractProjectWidget
{
    Q_OBJECT

public:
    explicit FormProject(Project* in_project = NULL, QWidget *parent = 0);
    ~FormProject();

    QIcon icon() const;
    QString title() const;

protected:
    bool maybeClose();
    bool saveRecord();
    void initLayout();

private:
    Ui::FormProject *m_ui;

    Project            *m_project;

    void updateControls();

};

#endif // FORM_PROJECT_H
