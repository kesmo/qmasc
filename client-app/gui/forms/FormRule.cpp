#include "FormRule.h"
#include "ui_FormRule.h"

#include "session.h"
#include "entities/projectversion.h"
#include "entities/rulecontent.h"

#include <QMessageBox>
#include <QPushButton>


FormRule::FormRule(Rule* rule, QWidget *parent) :
    AbstractProjectWidget(rule, parent),
    m_ui(new Ui::FormRule),
    m_rule(NULL),
    m_rule_content(NULL)
{
    m_ui->setupUi(this);

    loadStyleSheet(this,
                   "#b29b40",
                   "#998c59",
                   "#665e3c",
                   "#ab9e69",
                   "#baad78",
                   "#877b4a",
                   "#e8e6df",
                   "#e8d37d",
                   "#c4a422",
                   "#998c59");

    m_ui->rule_description->addTextToolBar(RecordTextEditToolBar::Small);

    connect(m_ui->rule_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->rule_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    loadRule(rule);
}

FormRule::~FormRule()
{
    delete m_ui;
}

QIcon FormRule::icon() const
{
    return QPixmap(":/images/22x22/rule.png");
}

QString FormRule::title() const
{
    if (m_rule)
        return m_rule->getValueForKey(RULES_HIERARCHY_SHORT_NAME);

    return QString();
}


void FormRule::loadRule(Rule* in_rule)
{
    m_ui->rule_name->setFocus();

    m_ui->lock_widget->setVisible(false);

    if (in_rule != NULL)
    {
        m_rule = in_rule;

        setWindowTitle(title());

        m_ui->rule_id->setText(QString(m_rule->getIdentifier()) + "(" + QString(m_rule->getValueForKey(RULES_HIERARCHY_RULE_CONTENT_ID)) + ")");

        if (m_rule_content != NULL)
            delete m_rule_content;

        m_rule_content = RuleContent::RuleRelation::instance().getParent(m_rule);
        if (m_rule_content) {
            loadRuleContent(m_rule_content);
        }
    }
    else
        cancel();
}

void FormRule::loadRuleContent(RuleContent *in_rule_content)
{
    setModifiable(false);

    if (m_rule_content == in_rule_content)
    {
        if (m_rule->isModifiable())
        {
            m_rule_content->lockRecord(true);

            if (m_rule_content->lockRecordStatus() == RECORD_STATUS_LOCKED)
            {
                m_ui->lock_widget->setVisible(true);
                net_get_field(NET_MESSAGE_TYPE_INDEX+1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
                m_ui->label_lock_by->setText(tr("Verrouillée par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            }
            else
                setModifiable(true);
        }
    }

    //! \todo
//    m_ui->previous_version_button->setVisible(false);
//    m_ui->next_version_button->setVisible(false);

    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

    if (in_rule_content != NULL)
    {

        //! \todo
        //        m_previous_rule_content = in_rule_content->previousRuleContent();
        //        m_next_rule_content = in_rule_content->nextRuleContent();
//        m_ui->previous_version_button->setVisible(m_previous_rule_content != NULL);
//        m_ui->next_version_button->setVisible(m_next_rule_content != NULL);

        setWindowTitle(tr("Règle de gestion : %1").arg(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)));
        m_ui->rule_name->setText(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME));
        m_ui->rule_name->setEnabled(isModifiable());
        m_ui->rule_description->textEditor()->setText(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_DESCRIPTION));
        m_ui->rule_description->textEditor()->setReadOnly(!isModifiable());
        if (isModifiable())
            m_ui->rule_description->toolBar()->show();
        else
            m_ui->rule_description->toolBar()->hide();

        m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_VERSION)));

    }

    setModified(false);
}


bool FormRule::saveRecord()
{
    int                 tmp_compare_versions_result = 0;
    QMessageBox         *tmp_msg_box;
    QPushButton         *tmp_update_button;
    QPushButton         *tmp_conserv_button;
    QPushButton         *tmp_cancel_button;

    RuleContent*        tmp_old_content = NULL;
    bool                tmp_upgrade = false;

    int                 tmp_save_result = NOERR;

    if (m_ui->rule_name->text().isEmpty())
    {
        QMessageBox::critical(this, tr("Donnée obligatoire non saisi"), tr("Le nom de la règle de gestion est nécessaire."));
        return false;
    }

    // Mettre a jour le contenu de la règle de gestion
    tmp_compare_versions_result = compare_values(m_rule_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION), m_rule->getValueForKey(FEATURES_HIERARCHY_VERSION));
    if (tmp_compare_versions_result < 0)
    {
        // La version du contenu de la règle de gestion est anterieure a la version de la règle de gestion
        tmp_msg_box = new QMessageBox(QMessageBox::Question, tr("Confirmation..."),
                                      tr("La version du contenu de la règle de gestion (%1) est antérieure à  la version courante (%2).\nVoulez-vous mettre à niveau la version du contenu vers la version courante ?").arg(ProjectVersion::formatProjectVersionNumber(m_rule_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION))).arg(ProjectVersion::formatProjectVersionNumber(m_rule->getValueForKey(FEATURES_HIERARCHY_VERSION))));
        tmp_update_button = tmp_msg_box->addButton(tr("Mettre à niveau"), QMessageBox::YesRole);
        tmp_conserv_button = tmp_msg_box->addButton(tr("Conserver la version"), QMessageBox::NoRole);
        tmp_cancel_button = tmp_msg_box->addButton(tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->exec();
        if (tmp_msg_box->clickedButton() == tmp_update_button)
        {
            delete tmp_msg_box;
            tmp_upgrade = true;
            tmp_old_content = m_rule_content;
            m_rule_content = tmp_old_content->copy();
            delete tmp_old_content;
        }
        else if(tmp_msg_box->clickedButton() == tmp_cancel_button)
        {
            delete tmp_msg_box;
            return false;
        }
    }

    m_rule_content->setValueForKey(m_ui->rule_name->text().toStdString().c_str(), FEATURES_CONTENTS_TABLE_SHORT_NAME);
    m_rule_content->setValueForKey(m_ui->rule_description->textEditor()->toOptimizedHtml().toStdString().c_str(), FEATURES_CONTENTS_TABLE_DESCRIPTION);
    tmp_save_result = m_rule_content->saveRecord();
    if (tmp_save_result == NOERR)
    {
        m_rule->setDataFromRuleContent(m_rule_content);
        tmp_save_result = m_rule->saveRecord();
    }

    if (tmp_save_result != NOERR)
    {
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
        return false;
    }

    m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(m_rule_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION)));

    setModified(false);

    return true;
}

