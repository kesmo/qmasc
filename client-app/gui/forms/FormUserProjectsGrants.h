/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_USER_PROJECTS_GRANTS_H
#define FORM_USER_PROJECTS_GRANTS_H

#include "entities/project.h"

#include "gui/components/RecordsTreeModel.h"

#include <QtWidgets/QDialog>

namespace Ui {
    class FormUserProjectsGrants;
}

class FormUserProjectsGrants : public QDialog {
    Q_OBJECT
    Q_DISABLE_COPY(FormUserProjectsGrants)
public:
    explicit FormUserProjectsGrants(const char *in_username, QWidget *parent = 0);
    virtual ~FormUserProjectsGrants();

public Q_SLOTS:
    void accept();
    void reject();

protected:
    virtual void changeEvent(QEvent *e);

private:
    Ui::FormUserProjectsGrants   *m_ui;
    const char                      *m_username;

    RecordsTreeModel                *m_projects_grants_tree_model;
};

#endif // FORM_USER_PROJECTS_GRANTS_H
