/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "session.h"
#include "utilities.h"
#include "FormRequirement.h"
#include "ui_FormRequirement.h"
#include "entities/testrequirement.h"
#include "entities/executiontest.h"
#include "entities/testcontent.h"
#include "entities/requirementcategory.h"
#include "entities/testresult.h"
#include "gui/components/widgets/horizontaltabstyle.h"

#include <QMessageBox>
#include <QPushButton>

/**
  Constructeur
**/
FormRequirement::FormRequirement(Requirement *in_requirement, QWidget *parent)
    : AbstractProjectWidget(in_requirement, parent),
    CustomFieldsControlsManager(),
    m_ui(new Ui::FormRequirement),
    m_chart(NULL)
{
    vector<QColor>              tmp_colors;

    m_requirement = NULL;
    m_requirement_content = NULL;
    m_previous_requirement_content = NULL;
    m_next_requirement_content = NULL;

    m_ui->setupUi(this);
    m_ui->requirementTabWidget->setTabPosition(QTabWidget::East);
    m_ui->requirementTabWidget->tabBar()->setStyle(new Gui::Components::Widgets::HorizontalTabStyle());

    loadStyleSheet(this,
                   "#b24040",
                   "#995959",
                   "#663c3c",
                   "#ab6969",
                   "#ba7878",
                   "#874a4a",
                   "#e8dfdf",
                   "#e87d7d",
                   "#c42222",
                   "#995959");

    m_ui->requirement_description->addTextToolBar(RecordTextEditToolBar::Small);

    foreach(RequirementCategory *tmp_requiremetn_category, Session::instance().requirementsCategories())
    {
        m_ui->requirement_category->addItem(TR_CUSTOM_MESSAGE(tmp_requiremetn_category->getValueForKey(REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL)), tmp_requiremetn_category->getValueForKey(REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID));
    }

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    connect(m_ui->requirement_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->requirement_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));
    connect(m_ui->requirement_category, SIGNAL(currentIndexChanged(int)), this, SLOT(setModified()));
    connect(m_ui->requirement_priority_level, SIGNAL(valueChanged(int)), this, SLOT(setModified()));

    connect(m_ui->tests_list, SIGNAL(recordslistDrop(QList<Record*>, int)), this, SLOT(testsDrop(QList<Record*>, int)));
    connect(m_ui->tests_list, SIGNAL(rowRemoved(int)), this, SLOT(deletedTestAtIndex(int)));
    connect(m_ui->tests_list, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(showTestAtIndex(QModelIndex)));


    //! \todo
//    connect(m_ui->previous_version_button, SIGNAL(clicked()), this, SLOT(loadPreviousRequirementContent()));
//    connect(m_ui->next_version_button, SIGNAL(clicked()), this, SLOT(loadNextRequirementContent()));

    m_chart = new Chart(800, 600);
    m_chart->setRenderingForQuality();
    m_chart->m_stack_histogram_indic = true;
    m_chart->m_show_legend = true;

    tmp_colors.push_back(ExecutionTest::incompleteColor());
    tmp_colors.push_back(ExecutionTest::koColor());
    tmp_colors.push_back(ExecutionTest::okColor());
    tmp_colors.push_back(ExecutionTest::byPassedColor());

    m_chart->setGraphColors(tmp_colors);
    m_chart->setLegendPosition(Chart::LEGEND_LEFT);

    m_ui->requirement_id->setVisible(Session::instance().getClientSession()->m_debug);

    loadPluginsViews();
    loadCustomFieldsView(m_ui->requirementTabWidget, Session::instance().customRequirementsFieldsDesc());

    loadRequirement(in_requirement);
}


/**
  Destructeur
**/
FormRequirement::~FormRequirement()
{
    destroyPluginsViews();

    delete m_chart;
    delete m_requirement_content;
    delete m_previous_requirement_content;
    delete m_next_requirement_content;
    delete m_ui;

    qDeleteAll(m_chart_data);

    qDeleteAll(m_custom_requirements_fields);

}

QIcon FormRequirement::icon() const
{
    return QPixmap(":/images/22x22/requirement.png");
}

QString FormRequirement::title() const
{
    if (m_requirement)
        return m_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME);

    return QString();
}


void FormRequirement::initGraph(RequirementContent *in_requirement_content)
{
    char                ***tmp_results = NULL;
    unsigned long       tmp_rows_count = 0;
    unsigned long       tmp_columns_count = 0;

    QDateTime           tmp_date_time;

    const char          *tmp_tests_results[] = { EXECUTION_TEST_VALIDATED, EXECUTION_TEST_INVALIDATED, EXECUTION_TEST_BYPASSED, EXECUTION_TEST_INCOMPLETED };

    char                *tmp_ptr = Session::instance().getClientSession()->m_last_query;

    qDeleteAll(m_chart_data);
    m_chart_data.clear();

    if (Session::instance().testsResults().count() == 4) {
        for (int tmp_index = 0; tmp_index < 4; tmp_index++) {
            tmp_ptr += net_session_print_query(Session::instance().getClientSession(),
                                               "select count(resultat), '%s', executions_campaigns_table.execution_date, campaigns_table.short_name "\
                                               "from campaigns_table, executions_campaigns_table LEFT OUTER JOIN "\
                                               "("\
                                               "select executions_campaigns_table.execution_campaign_id "\
                                               "from requirements_contents_table, tests_requirements_table, tests_table, "\
                                               "executions_campaigns_table, campaigns_table, executions_tests_table "\
                                               "where  "\
                                               "requirements_contents_table.requirement_content_id=%s and "\
                                               "requirements_contents_table.original_requirement_content_id=tests_requirements_table.original_requirement_content_id and "\
                                               "tests_requirements_table.test_content_id=tests_table.test_content_id and "\
                                               "tests_table.version>='%s' and "\
                                               "executions_campaigns_table.campaign_id=campaigns_table.campaign_id and "\
                                               "campaigns_table.version>='%s' and " \
                                               "executions_tests_table.execution_campaign_id=executions_campaigns_table.execution_campaign_id and "\
                                               "tests_table.test_id=executions_tests_table.test_id and "\
                                               "executions_tests_table.result_id='%s' "\
                                               ") as resultat "\
                                               "ON (executions_campaigns_table.execution_campaign_id=resultat.execution_campaign_id) "\
                                               "where "\
                                               "executions_campaigns_table.campaign_id=campaigns_table.campaign_id "\
                                               "group by executions_campaigns_table.execution_date, campaigns_table.short_name",
                                               TR_CUSTOM_MESSAGE(Session::instance().testsResults()[tmp_index]->getValueForKey(TESTS_RESULTS_TABLE_DESCRIPTION)).toStdString().c_str(),
                                               in_requirement_content->getIdentifier(),
                                               in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION),
                                               in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION),
                                               tmp_tests_results[tmp_index]);

            if (tmp_index < 3)
                tmp_ptr += sprintf_s(tmp_ptr, LARGE_TEXT_SIZE, "\nUNION ALL\n");
            else
                tmp_ptr += sprintf_s(tmp_ptr, LARGE_TEXT_SIZE, ";");
        }

        tmp_results = cl_run_sql(Session::instance().getClientSession(), Session::instance().getClientSession()->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL) {
            if (tmp_columns_count == 4) {
                for (unsigned long tmp_index = 0; tmp_index < tmp_rows_count; tmp_index++) {
                    tmp_date_time = QDateTime::fromString(QString(tmp_results[tmp_index][2]).left(16), "yyyy-MM-dd hh:mm");
                    m_chart_data.append(new Trinome(new String(QString(tmp_results[tmp_index][3]) + " " + QString(tmp_date_time.toString("yyyy-MM-dd hh:mm"))),
                                        new Double(atol(tmp_results[tmp_index][0])),
                                        new String(tmp_results[tmp_index][1])));
                }
            }

            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    m_chart->m_auto_borders = true;
    m_chart->setData(TrinomeArray(m_chart_data));
    m_chart->setBorderSize(-1, -1, -1, -1);
    m_chart->m_auto_borders = false;
    m_chart->setBorderSize(-1, 10, 10, 10);
    m_chart->clear();
    m_chart->drawGraph(Chart::Camembert);

    m_ui->pixmap->setPixmap(*m_chart->getImage());
    m_ui->pixmap->update();
}


void FormRequirement::loadRequirement(Requirement *in_requirement)
{
    QStringList             tmp_tests_headers;

    m_ui->requirement_name->setFocus();

    m_ui->lock_widget->setVisible(false);

    tmp_tests_headers << tr("Description") << tr("Catégorie") << tr("Version");
    m_ui->tests_list->setHorizontalHeaderLabels(tmp_tests_headers);
    m_ui->tests_list->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);

    if (in_requirement != NULL) {
        m_requirement = in_requirement;

        setWindowTitle(title());

        m_ui->requirement_id->setText(QString(m_requirement->getIdentifier()) + "(" + QString(m_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID)) + ")");

        if (m_requirement_content != NULL)
            delete m_requirement_content;

        m_requirement_content = RequirementContent::RequirementRelation::instance().getParent(m_requirement);
        if (m_requirement_content) {
            loadRequirementContent(m_requirement_content);
        }
    }
    else
        cancel();
}


void FormRequirement::loadPreviousRequirementContent()
{
    RequirementContent    *tmp_requirement_content = m_previous_requirement_content;

    if (maybeClose()) {
        if (m_requirement_content != NULL
            && m_previous_requirement_content != NULL
            && compare_values(m_requirement_content->getIdentifier(), m_previous_requirement_content->getIdentifier()) == 0)
            tmp_requirement_content = m_requirement_content;

        loadRequirementContent(tmp_requirement_content);
    }
}


void FormRequirement::loadNextRequirementContent()
{
    RequirementContent    *tmp_requirement_content = m_next_requirement_content;

    if (maybeClose()) {
        if (m_requirement_content != NULL
            && m_next_requirement_content != NULL
            && compare_values(m_requirement_content->getIdentifier(), m_next_requirement_content->getIdentifier()) == 0)
            tmp_requirement_content = m_requirement_content;

        loadRequirementContent(tmp_requirement_content);
    }
}


void FormRequirement::loadRequirementContent(RequirementContent *in_requirement_content)
{

    TestRequirement     *tmp_test_requirement = NULL;
    int                 tmp_tests_index = 0;

    setModifiable(false);

    m_ui->tests_list->setRowCount(0);

    if (m_requirement_content == in_requirement_content) {
        if (m_requirement->isModifiable()) {
            m_requirement_content->lockRecord(true);

            if (m_requirement_content->lockRecordStatus() == RECORD_STATUS_LOCKED) {
                m_ui->lock_widget->setVisible(true);
                net_get_field(NET_MESSAGE_TYPE_INDEX + 1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
                m_ui->label_lock_by->setText(tr("Verrouillée par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            }
            else
                setModifiable(true);
        }
    }

    initGraph(in_requirement_content);

    //! \todo
//    m_ui->previous_version_button->setVisible(false);
//    m_ui->next_version_button->setVisible(false);

    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

    m_ui->tests_list->setRowCount(0);

    qDeleteAll(m_custom_requirements_fields);
    m_custom_requirements_fields.clear();

    if (in_requirement_content != NULL) {
        m_previous_requirement_content = in_requirement_content->previousRequirementContent();
        m_next_requirement_content = in_requirement_content->nextRequirementContent();

        //! \todo
//        m_ui->previous_version_button->setVisible(m_previous_requirement_content != NULL);
//        m_ui->next_version_button->setVisible(m_next_requirement_content != NULL);

        setWindowTitle(tr("Exigence : %1").arg(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME)));
        m_ui->requirement_name->setText(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME));
        m_ui->requirement_name->setEnabled(isModifiable());
        m_ui->requirement_description->textEditor()->setText(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION));
        m_ui->requirement_description->textEditor()->setReadOnly(!isModifiable());
        if (isModifiable())
            m_ui->requirement_description->toolBar()->show();
        else
            m_ui->requirement_description->toolBar()->hide();

        m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION)));
        m_ui->requirement_priority_level->setEnabled(isModifiable());
        m_ui->requirement_priority_level->setValue(QString(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL)).toInt());

        m_ui->requirement_category->setEnabled(isModifiable());
        if (is_empty_string(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID)) == FALSE) {
            m_ui->requirement_category->setCurrentIndex(m_ui->requirement_category->findData(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID)));
        }
        else
            m_ui->requirement_category->setCurrentIndex(0);

        // Charger les tests associes
        if (in_requirement_content->getAllTestRequirements().count() > 0) {
            m_ui->tests_list->setRowCount(in_requirement_content->getAllTestRequirements().count());
            for (tmp_tests_index = 0; tmp_tests_index < in_requirement_content->getAllTestRequirements().count(); tmp_tests_index++) {
                tmp_test_requirement = in_requirement_content->getTestRequirementAtIndex(tmp_tests_index);
                setTestForRow(tmp_test_requirement, tmp_tests_index);
            }
            m_ui->tests_list->resizeColumnsToContents();
            m_ui->tests_list->resizeRowsToContents();
        }
        m_ui->tests_list->setDisabled(!isModifiable());

        // Charger les champs personnalises
        m_custom_requirements_fields = RequirementContent::CustomRequirementFieldRelation::instance().getChilds(m_requirement_content);
        QList<CustomFieldDesc*> customFieldsDesc = Session::instance().customRequirementsFieldsDesc();

        foreach(CustomFieldDesc* customFieldDesc, customFieldsDesc)
        {
            bool foundRequirementField = false;
            foreach(CustomRequirementField* customRequirementField, m_custom_requirements_fields)
            {
                if (compare_values(customRequirementField->getValueForKey(CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID),
                    customFieldDesc->getIdentifier()) == 0) {
                    customRequirementField->setFieldDesc(customFieldDesc);
                    popCustomFieldValue(customFieldDesc, customRequirementField, CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE, isModifiable());
                    foundRequirementField = true;
                    break;
                }
            }

            if (!foundRequirementField) {
                CustomRequirementField* customRequirementField = new CustomRequirementField();
                customRequirementField->setFieldDesc(customFieldDesc);
                RequirementContent::CustomRequirementFieldRelation::instance().setParent(m_requirement_content, customRequirementField);
                customRequirementField->setValueForKey(customFieldDesc->getValueForKey(CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_DEFAULT_VALUE), CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE);
                popCustomFieldValue(customFieldDesc, customRequirementField, CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE, isModifiable());
                m_custom_requirements_fields.append(customRequirementField);
            }
        }


    }

    setModified(false);
}


void FormRequirement::save()
{
    if (saveRequirement()) {
        savePluginsDatas();
        setModified(false);
        emit recordSaved(m_requirement);
    }
}


/**
  Validation par le bouton OK
**/
bool FormRequirement::saveRequirement()
{
    int                 tmp_compare_versions_result = 0;
    QMessageBox         *tmp_msg_box;
    QPushButton         *tmp_update_button;
    QPushButton         *tmp_conserv_button;
    QPushButton         *tmp_cancel_button;

    RequirementContent* tmp_old_content = NULL;
    bool                tmp_upgrade = false;

    int                 tmp_save_result = NOERR;

    TestRequirement    *tmp_test_requirement = NULL;

    QVariant        tmp_requirement_category;

    CustomRequirementField        *tmp_custom_requirement_field = NULL;

    if (m_ui->requirement_name->text().isEmpty()) {
        QMessageBox::critical(this, tr("Donnée obligatoire non saisi"), tr("Le nom de l'exigence est nécessaire."));
        return false;
    }

    // Mettre a jour le contenu de l'exigence
    tmp_compare_versions_result = compare_values(m_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION), m_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION));
    if (tmp_compare_versions_result < 0) {
        // La version du contenu de l'exigence est anterieure a la version de l'exigence
        tmp_msg_box = new QMessageBox(QMessageBox::Question, tr("Confirmation..."),
                                      tr("La version du contenu de l'exigence (%1) est antérieure à  la version courante (%2).\nVoulez-vous mettre à niveau la version du contenu vers la version courante ?").arg(ProjectVersion::formatProjectVersionNumber(m_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION))).arg(ProjectVersion::formatProjectVersionNumber(m_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION))));
        tmp_update_button = tmp_msg_box->addButton(tr("Mettre à niveau"), QMessageBox::YesRole);
        tmp_conserv_button = tmp_msg_box->addButton(tr("Conserver la version"), QMessageBox::NoRole);
        tmp_cancel_button = tmp_msg_box->addButton(tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->exec();
        if (tmp_msg_box->clickedButton() == tmp_update_button) {
            delete tmp_msg_box;
            tmp_upgrade = true;
            tmp_old_content = m_requirement_content;
            m_requirement_content = tmp_old_content->copy();
            delete tmp_old_content;
        }
        else if (tmp_msg_box->clickedButton() == tmp_cancel_button) {
            delete tmp_msg_box;
            return false;
        }
    }

    m_requirement_content->setValueForKey(m_ui->requirement_name->text().toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME);
    m_requirement_content->setValueForKey(m_ui->requirement_description->textEditor()->toOptimizedHtml().toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION);
    m_requirement_content->setValueForKey(QString::number(m_ui->requirement_priority_level->value()).toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL);

    tmp_requirement_category = m_ui->requirement_category->itemData(m_ui->requirement_category->currentIndex());
    if (tmp_requirement_category.isValid())
        m_requirement_content->setValueForKey(tmp_requirement_category.toString().toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID);

    tmp_save_result = m_requirement_content->saveRecord();
    if (tmp_save_result == NOERR) {
        m_requirement->setDataFromRequirementContent(m_requirement_content);
        tmp_save_result = m_requirement->saveRecord();
    }

    // Mettre a jour les tests
    m_requirement_content->saveTestRequirements();

    // Mettre a jour les champs personnalises
    for (int tmp_index = 0; tmp_save_result == NOERR && tmp_index < m_custom_requirements_fields.count(); tmp_index++) {
        tmp_custom_requirement_field = m_custom_requirements_fields[tmp_index];
        if (tmp_custom_requirement_field != NULL) {
            if (tmp_upgrade)
                tmp_custom_requirement_field = tmp_custom_requirement_field->copy();

            RequirementContent::CustomRequirementFieldRelation::instance().setParent(m_requirement_content, tmp_custom_requirement_field);

            tmp_save_result = pushEnteredCustomFieldValue(tmp_custom_requirement_field->getFieldDesc(), tmp_custom_requirement_field, CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE);
            if (tmp_save_result == EMPTY_OBJECT) {
                QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), tr("Le champ personnalisé <b>%1</b> de l'onglet <b>%2</b> est obligatoire.")
                                      .arg(tmp_custom_requirement_field->getFieldDesc()->getValueForKey(CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_LABEL))
                                      .arg(tmp_custom_requirement_field->getFieldDesc()->getValueForKey(CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TAB_NAME)));
                return false;
            }
            else if (tmp_save_result == NOERR)
                tmp_save_result = tmp_custom_requirement_field->saveRecord();

        }
    }

    if (tmp_save_result != NOERR) {
        QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
        return false;
    }

    m_ui->version_label->setText(ProjectVersion::formatProjectVersionNumber(m_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION)));

    return true;
}


void FormRequirement::cancel()
{
    QWidget::close();
}


bool FormRequirement::maybeClose()
{
    int        tmp_confirm_choice = 0;
    bool    tmp_return = true;

    if (m_requirement_content != NULL) {
        if (isModified()) {
            tmp_confirm_choice = QMessageBox::question(
                this,
                tr("Confirmation..."),
                tr("L'exigence a été modifiée. Voulez-vous enregistrer les modifications ?"),
                QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                QMessageBox::Cancel);

            if (tmp_confirm_choice == QMessageBox::Yes)
                tmp_return = saveRequirement();
            else if (tmp_confirm_choice == QMessageBox::Cancel)
                tmp_return = false;
        }

        if (tmp_return)
            m_requirement_content->unlockRecord();
    }

    return tmp_return;
}



void FormRequirement::testsDrop(QList<Record*> in_list, int /* in_row */)
{
    TestRequirement          *tmp_test_requirement = NULL;
    TestRequirement          *tmp_current_test_requirement = NULL;
    QTableWidgetItem        *tmp_item = NULL;
    bool                    tmp_item_exists = false;

    if (isModifiable()) {
        foreach(Record *tmp_record, in_list)
        {
            if (tmp_record->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID) {
                if (tmp_record != NULL) {
                    // Verifier le projet
                    if (compare_values(tmp_record->getValueForKey(TESTS_HIERARCHY_PROJECT_ID), m_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0) {
                        tmp_item_exists = false;
                        for (int tmp_index = 0; tmp_item_exists == false && tmp_index < m_ui->tests_list->rowCount(); tmp_index++) {
                            tmp_item = m_ui->tests_list->item(tmp_index, 0);
                            tmp_current_test_requirement = (TestRequirement*)tmp_item->data(Qt::UserRole).value<void*>();
                            tmp_item_exists = (tmp_current_test_requirement != NULL
                                               && compare_values(
                                               tmp_record->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID),
                                               tmp_current_test_requirement->getValueForKey(TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID)) == 0);
                        }

                        if (tmp_item_exists == false) {
                            setModified();
                            tmp_test_requirement = new TestRequirement();
                            tmp_test_requirement->setValueForKey(tmp_record->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
                            tmp_test_requirement->setValueForKey(m_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_ORIGINAL_REQUIREMENT_CONTENT_ID), TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
                            m_requirement_content->appendTestRequirement(tmp_test_requirement);
                            m_ui->tests_list->insertRow(m_ui->tests_list->rowCount());
                            setTestForRow(tmp_test_requirement, m_ui->tests_list->rowCount() - 1);
                        }
                    }
                }
            }
        }
    }
    qDeleteAll(in_list);
}


void FormRequirement::deletedTestAtIndex(int in_row)
{
    setModified();
    m_requirement_content->removeTestRequirementAtIndex(in_row);
}

void FormRequirement::setTestForRow(TestRequirement *in_test, int in_row)
{
    QTableWidgetItem    *tmp_first_column_item = NULL;
    QTableWidgetItem    *tmp_second_column_item = NULL;
    QTableWidgetItem    *tmp_third_column_item = NULL;

    if (in_test != NULL) {
        // Premiere colonne
        tmp_first_column_item = new QTableWidgetItem;
        tmp_first_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_test));
        TestContent *test = TestContent::TestRequirementRelation::instance().getParent(in_test);
        if (test) {
            tmp_first_column_item->setText(test->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));
        }
        tmp_first_column_item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
        m_ui->tests_list->setItem(in_row, 0, tmp_first_column_item);

        // Deuxieme colonne
        tmp_second_column_item = new QTableWidgetItem;
        tmp_second_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_test));
        if (test) {
            tmp_second_column_item->setText(m_ui->requirement_category->itemText(m_ui->requirement_category->findData(test->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID))));
        }

        tmp_second_column_item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
        m_ui->tests_list->setItem(in_row, 1, tmp_second_column_item);

        // TroisiÃ¨me colonne
        tmp_third_column_item = new QTableWidgetItem;
        tmp_third_column_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_test));
        if (test) {
            tmp_third_column_item->setText(ProjectVersion::formatProjectVersionNumber(test->getValueForKey(TESTS_CONTENTS_TABLE_VERSION)));
        }
        tmp_third_column_item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
        m_ui->tests_list->setItem(in_row, 2, tmp_third_column_item);

    }
}


void FormRequirement::showTestAtIndex(QModelIndex in_index)
{
    TestRequirement              *tmp_test_requirement = NULL;
    QTableWidgetItem            *tmp_column_item = NULL;

    if (in_index.isValid()) {
        tmp_column_item = m_ui->tests_list->item(in_index.row(), 0);
        if (tmp_column_item != NULL) {
            tmp_test_requirement = (TestRequirement*)tmp_column_item->data(Qt::UserRole).value<void*>();
            if (tmp_test_requirement != NULL) {
                emit showTestWithContentId(m_requirement->projectVersion(), tmp_test_requirement->getValueForKey(TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID));
            }
        }
    }
}


void FormRequirement::loadPluginsViews()
{
    QMap < QString, ClientModule*>    tmp_modules_map = Session::instance().externalsModules().value(ClientModule::RequirementPlugin);

    RequirementModule   *tmp_requirement_module = NULL;

    foreach(ClientModule *tmp_module, tmp_modules_map)
    {
        tmp_requirement_module = static_cast<RequirementModule*>(tmp_module);

        tmp_requirement_module->loadRequirementModuleDatas(m_requirement);

        QWidget        *tmp_module_view = tmp_requirement_module->createView(this);
        if (tmp_module_view != NULL) {
            m_views_modules_map[tmp_requirement_module] = tmp_module_view;
            m_ui->requirementTabWidget->addTab(tmp_module_view, tmp_module->getModuleName());
        }
    }
}


void FormRequirement::savePluginsDatas()
{
    QMap<RequirementModule*, QWidget*>::iterator tmp_module_iterator;

    for (tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++) {
        tmp_module_iterator.key()->saveRequirementModuleDatas();
    }
}


void FormRequirement::destroyPluginsViews()
{
    QMap<RequirementModule*, QWidget*>::iterator tmp_module_iterator;

    for (tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++) {
        tmp_module_iterator.key()->destroyView(tmp_module_iterator.value());
    }
}
