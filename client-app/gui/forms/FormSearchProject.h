/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_SEARCH_PROJECT_H
#define FORM_SEARCH_PROJECT_H

#include <QtWidgets/QDialog>
#include <QList>
#include "entities/projectversion.h"
#include "entities/requirement.h"
#include "entities/test.h"
#include "record.h"

namespace Ui {
    class FormSearchProject;
}

class FormSearchProject : public QDialog {
    Q_OBJECT
public:
    FormSearchProject(ProjectVersion *in_project_version, QWidget *parent = 0);
    ~FormSearchProject();

public slots:
    void accept();
    void updateControls();

    void launchTextualSearch();
    void launchTestsFromCurrentProjectVersionSearch();
    void launchRequirementsFromCurrentProjectVersionSearch();
    void launchNotCoveredRequirementsSearch();

signals:
    void foundRecords(const QList<Record*> & out_records_list);

protected:
    void changeEvent(QEvent *e);

private:
    Ui::FormSearchProject *m_ui;

    ProjectVersion *m_project_version;
};

#endif // FORM_SEARCH_PROJECT_H
