/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_TEST_H
#define FORM_TEST_H

#ifdef GUI_AUTOMATION_ACTIVATED
#include "automation.h"
#endif

#include <QtWidgets/QTableWidgetItem>
#include "entities/testcontent.h"
#include "entities/test.h"
#include "entities/action.h"
#include "entities/automatedaction.h"
#include "entities/testrequirement.h"
#include "entities/testcontentfile.h"
#include "entities/bug.h"
#include "gui/components/RecordTextEditContainer.h"
#include "gui/components/TestActionAttachmentsManager.h"
#include "gui/components/AbstractProjectWidget.h"
#include "gui/components/RecordsTableModel.h"
#include "entities/customfielddesc.h"
#include "entities/customtestfield.h"
#include "entities/automatedactionwindowhierarchy.h"

#include "gui/components/CustomFieldsControlsManager.h"

#include "automation/BatchAutomationProcessor.h"
#include "automation/GuiAutomationPlaybackProcessor.h"
#include "automation/GuiAutomationRecordProcessor.h"

#include <bugtracker.h>
#include <clientmodule.h>


#define     ALL_ACTIONS_ADD_FUNCTION            0
#define     ALL_ACTIONS_REMOVE_FUNCTION         1
#define     CURRENT_ACTION_ADD_FUNCTION         2
#define     CURRENT_ACTION_MOVE_FUNCTION_UP     3
#define     CURRENT_ACTION_MOVE_FUNCTION_DOWN   4
#define     CURRENT_ACTION_REMOVE_FUNCTION      5
#define     ACTION_NONE                         6


namespace Ui
{
class FormTest;
}

class FormTest :
    public AbstractProjectWidget,
    public TestActionAttachmentsManager,
    public CustomFieldsControlsManager
{
    Q_OBJECT

public:
    FormTest(Test *in_test, QWidget *parent = 0);
    ~FormTest();

    QIcon icon() const;
    QString title() const;

protected:
    void setActionAtIndex(Action *in_action, int in_row);
    void setRequirementForRow(TestRequirement *in_requirement, int in_row);
    void setBugForRow(Bug *in_bug, int in_row);
    void setFileForRow(TestContentFile *in_file, int in_row);
    bool saveTest();

    virtual void showEvent(QShowEvent * event);
    virtual void closeEvent(QCloseEvent *in_event);

    bool maybeClose();

    void loadTest(Test *in_test);
    void loadTestContent(TestContent *in_test_content);

    public slots:
    void save();
    void cancel();
    void updateControls(bool in_original_test_indic);
    void addAction();
    void deleteSelectedAction();
    void moveSelectedActionUp();
    void moveSelectedActionDown();
    void actionSelectionChanged();

    void actionsDrop(QList<Record*> in_list, int in_row);

    void requirementsDrop(QList<Record*> in_list, int in_row);
    void deletedRequirementAtIndex(int in_row);

    void loadPreviousTestContent();
    void loadNextTestContent();

    void filesDrop(QList<QString> in_list, int);
    void openFileAtIndex(QTableWidgetItem *in_item);
    void deletedFileAtIndex(int in_index);

    void openBugAtIndex(QTableWidgetItem *in_item);
    void deletedBugAtIndex(int in_index);

    void actionModified();

    void showOriginalTestInfosClicked();

    void showRequirementAtIndex(QModelIndex in_index);

    void addActionAfterLastAction();

    void copySelectedActionsToClipboard();
    void cutSelectedActionsToClipboard();
    void pasteClipboardDataToActionsList();
    void pasteClipboardPlainTextToActionsList();

    void selectFirstColumnAction(Record *in_action);
    void selectSecondColumnAction(Record *in_action);

    void selectAction(Record *in_action, int in_column = 0);

    void changeTab(int in_tab_index);

    void selectExternalCommandForAutomation();
    void launchStartRecordSystemEvents();
    void launchStartPlaybackSystemEvents();
    void showProcessError(const QString& in_process_error);
    void automatedActionsRecorded(QList<AutomatedAction *> actions);

    void clipboardDataChanged();

    void addAutomatedActionWindowHierarchyMenu(QMenu* in_menu, QList<AutomatedActionWindowHierarchy*> in_windows_list);

    void showAutomatedActionValidationContextMenu(const QPoint &position);
    void showAutomatedActionValidationToolTip();

    void changeAutomatedTestType();
    void launchBatch();
    void stopBatchProcess(int in_exit_code = 0);
    void readBatchProcessStandardOutput(const QString &output);
    void readBatchProcessStandardError(const QString& output);

    private slots:
    void updateRowsHeight();

signals:
    void showOriginalTestInfos(Test *in_test);
    void showRequirementWithOriginalContentId(ProjectVersion *in_project_version, const char *in_original_requirement_content_id);

private:
    Ui::FormTest         *m_ui;
    Test        *m_test;
    TestContent           *m_test_content;
    TestContent           *m_previous_test_content;
    TestContent           *m_next_test_content;

    QList<CustomTestField*>        m_custom_tests_fields;

    RecordsTableModel<AutomatedAction>         *m_automated_actions_model;

    QList<Action*>        m_actions;

    QList<Bug*>            m_bugs;
    QList<Bug*>            m_removed_bugs;

    Bugtracker            *m_bugtracker;

    GuiAutomationRecordProcessor    m_record_processor;
    GuiAutomationPlaybackProcessor  m_playback_processor;
    BatchAutomationProcessor        m_batch_processor;

    //        QProcess            m_automated_program_record_process;
    //        QProcess            m_automated_program_playback_process;
    //        QProcess            m_automated_batch_process;

    void writeActionDataFromTextEditAtIndex(Action *in_action, int in_index);
    void removeActionWidgetsAtIndex(int in_row);
    RecordTextEdit* textEditAt(int in_row, int in_column);

    QMap<TestModule*, QWidget*>   m_views_modules_map;

#ifdef GUI_AUTOMATION_ACTIVATED

    log_message_list* m_automation_playback_messages;

#endif

    void loadPluginsViews();
    void destroyPluginsViews();
    void savePluginsDatas();

    void addAction(Action *in_action);

    void updateTabsWidget();
};

#endif // FORM_TEST_H
