#ifndef FORM_NEED_H
#define FORM_NEED_H

#include "gui/components/AbstractProjectWidget.h"
#include "entities/need.h"

#include <QtWidgets/QWidget>

namespace Ui {
class FormNeed;
}

class FormNeed :
        public AbstractProjectWidget
{
    Q_OBJECT
public:
    explicit FormNeed(Need* in_need, QWidget *parent = 0);
    ~FormNeed();

    QIcon icon() const;
    QString title() const;

public slots:
    bool saveRecord();

protected:
    void loadNeed(Need* in_need);

private:
    Ui::FormNeed *m_ui;

    Need    *m_need;
};

#endif // FORM_NEED_H
