/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormProjectVersion.h"
#include "ui_FormProjectVersion.h"

#include "client.h"
#include "constants.h"
#include "session.h"

#include <QMessageBox>

#define QLINEEDIT_WRITABLE "QLineEdit {background: #FFFFFF;border: 1px solid #4080b2;}"
#define QLINEEDIT_UNWRITABLE "QLineEdit {background: #FFFFFF;dfe0e8: color: gray;}"


/**
  Constructeur
**/
FormProjectVersion::FormProjectVersion(ProjectVersion* in_project_record, QWidget *parent) :
    AbstractProjectWidget(in_project_record, parent),
    m_project_record(in_project_record)
{
    m_bugtracker = NULL;

    m_ui = new  Ui_FormProjectVersion();

    m_ui->setupUi(this);
    setWindowTitle(title());
    m_ui->project_description->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->project_version_info->addTextToolBar(RecordTextEditToolBar::Small);
    m_ui->project_name->setFocus();

    foreach(ClientModule *tmp_bugtracker_module, Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin))
        m_ui->bug_tracker_type->addItem(tmp_bugtracker_module->getModuleName());

    m_ui->project_name->setText(m_project_record->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
    m_ui->project_version->setText(ProjectVersion::formatProjectVersionNumber(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)));

    initLayout();

    connect(m_ui->project_name, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->project_description->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));
    connect(m_ui->project_version_info->textEditor(), SIGNAL(textChanged()), this, SLOT(setModified()));
    connect(m_ui->bug_tracker_type, SIGNAL(currentIndexChanged(int)), this, SLOT(setModified()));
    connect(m_ui->bug_tracker_host, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->bug_tracker_url, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->bug_tracker_project_id, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));
    connect(m_ui->bug_tracker_project_version, SIGNAL(textChanged(const QString &)), this, SLOT(setModified()));

    connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(save()));
    connect(m_ui->buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));

    connect(m_ui->bugtracker_test_button, SIGNAL(clicked()), this, SLOT(checkBugtrackerConnexion()));

    updateControls();

    loadPluginsViews();
}



/**
  Destruction
**/
FormProjectVersion::~FormProjectVersion()
{
    destroyPluginsViews();

    if (m_bugtracker != NULL)
    {
        ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
        if (tmp_bugtracker_module != NULL)
        {
            static_cast<BugtrackerModule*>(tmp_bugtracker_module)->destroyBugtracker(m_bugtracker);
        }
    }

    delete m_ui;
}

QIcon FormProjectVersion::icon() const
{
    return QPixmap(":/images/22x22/version.png");
}

QString FormProjectVersion::title() const
{
    if (m_project_record)
        return QString(m_project_record->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME))
        + QString(" - VERSION ")
        + ProjectVersion::formatProjectVersionNumber(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

    return QString();
}


/**
  Validation par le bouton OK
**/
bool FormProjectVersion::saveProject()
{
    int     tmp_save_result = NOERR;

    m_project_record->project()->setValueForKey(m_ui->project_name->text().toStdString().c_str(), PROJECTS_TABLE_SHORT_NAME);
    m_project_record->project()->setValueForKey(m_ui->project_description->textEditor()->toOptimizedHtml().toStdString().c_str(), PROJECTS_TABLE_DESCRIPTION);
    tmp_save_result = m_project_record->project()->saveRecord();
    if (tmp_save_result == NOERR)
    {
        m_project_record->setValueForKey(m_ui->project_version_info->textEditor()->toOptimizedHtml().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_DESCRIPTION);

        if (m_ui->bug_tracker_type->currentIndex() >= 0)
            m_project_record->setValueForKey(m_ui->bug_tracker_type->currentText().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE);

        m_project_record->setValueForKey(m_ui->bug_tracker_host->text().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST);
        m_project_record->setValueForKey(m_ui->bug_tracker_url->text().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL);
        m_project_record->setValueForKey(m_ui->bug_tracker_project_id->text().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID);
        m_project_record->setValueForKey(m_ui->bug_tracker_project_version->text().toStdString().c_str(), PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION);

        tmp_save_result = m_project_record->saveRecord();
        if (tmp_save_result == NOERR)
        {
            setModified(false);
            updateControls();
            return true;
        }
    }

    QMessageBox::critical(this, tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));
    return false;
}


void FormProjectVersion::initLayout()
{
    m_ui->project_name->setText(m_project_record->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
    m_ui->project_description->textEditor()->setHtml(m_project_record->project()->getValueForKey(PROJECTS_TABLE_DESCRIPTION));
    m_ui->project_version_info->textEditor()->setHtml(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_DESCRIPTION));

    int tmp_bt_index = m_ui->bug_tracker_type->findText(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE));
    if (tmp_bt_index >= 0)
    {
        m_ui->bug_tracker_type->setCurrentIndex(tmp_bt_index);
    }

    m_ui->bug_tracker_host->setText(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST));
    m_ui->bug_tracker_url->setText(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL));
    m_ui->bug_tracker_project_id->setText(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID));
    m_ui->bug_tracker_project_version->setText(m_project_record->getValueForKey(PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION));

    if (!m_project_record->project()->isModifiable())
    {
        setModifiable(false);
    }
    else
    {
        m_project_record->lockRecord(true);
        if (m_project_record->lockRecordStatus() == RECORD_STATUS_LOCKED)
        {
            m_ui->lock_widget->setVisible(true);
            net_get_field(NET_MESSAGE_TYPE_INDEX+1, Session::instance().getClientSession()->m_response, Session::instance().getClientSession()->m_column_buffer, SEPARATOR_CHAR);
            m_ui->label_lock_by->setText(tr("Verrouillé par ") + QString(Session::instance().getClientSession()->m_column_buffer));
            setModifiable(false);
        }
        else
            setModifiable(true);
    }

}


void FormProjectVersion::updateControls()
{
    m_ui->lock_widget->setVisible(m_project_record->lockRecordStatus() == RECORD_STATUS_LOCKED);
    m_ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(isModifiable());

    m_ui->project_name->setReadOnly(!isModifiable());
    m_ui->project_description->textEditor()->setReadOnly(!isModifiable());
    m_ui->project_version_info->textEditor()->setReadOnly(!isModifiable());

    m_ui->bug_tracker_type->setEnabled(isModifiable());
    m_ui->bug_tracker_host->setReadOnly(!isModifiable());
    m_ui->bug_tracker_url->setReadOnly(!isModifiable());
    m_ui->bug_tracker_project_id->setReadOnly(!isModifiable());
    m_ui->bug_tracker_project_version->setReadOnly(!isModifiable());

    m_ui->bugtracker_test_button->setEnabled(isModifiable());

    if (isModifiable() == false)
    {
        m_ui->project_description->toolBar()->hide();
        m_ui->project_version_info->toolBar()->hide();
    }
    else
    {
        m_ui->project_description->toolBar()->show();
        m_ui->project_version_info->toolBar()->show();
    }
}



bool FormProjectVersion::maybeClose()
{
    int tmp_confirm_choice = 0;
    bool    tmp_return = true;

    if (m_project_record != NULL)
    {
        if (isModified())
        {
            tmp_confirm_choice = QMessageBox::question(
                        this,
                        tr("Confirmation..."),
                        tr("Le projet a été modifié. Voulez-vous enregistrer les modifications ?"),
                        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                        QMessageBox::Cancel);

            if (tmp_confirm_choice == QMessageBox::Yes)
                tmp_return = saveProject();
            else if (tmp_confirm_choice == QMessageBox::Cancel)
                tmp_return = false;
        }

        if (tmp_return)
            m_project_record->unlockRecord();
    }

    return tmp_return;
}


void FormProjectVersion::save()
{
    if (saveProject())
    {
        savePluginsDatas();

        emit recordSaved(m_project_record);
    }
}



void FormProjectVersion::checkBugtrackerConnexion()
{
    QString tmp_base_url = m_ui->bug_tracker_host->text();
    QString tmp_url = m_ui->bug_tracker_url->text();

    if (tmp_base_url.isEmpty() && tmp_url.isEmpty())
    {
        QMessageBox::critical(this, tr("Paramètre manquant"), tr("L'URL de base ou l'URL du Webservice est indispensable pour tester la connexion."));
    }
    else
    {
        if (m_ui->bug_tracker_type->currentIndex() >= 0)
        {
            ClientModule *tmp_bugtracker_module = Session::instance().externalsModules().value(ClientModule::BugtrackerPlugin).value(m_ui->bug_tracker_type->currentText());
            if (tmp_bugtracker_module != NULL)
            {
                if (m_bugtracker != NULL)
                    static_cast<BugtrackerModule*>(tmp_bugtracker_module)->destroyBugtracker(m_bugtracker);

                m_bugtracker = static_cast<BugtrackerModule*>(tmp_bugtracker_module)->createBugtracker();

                if (m_bugtracker != NULL)
                {
                    if (!tmp_base_url.isEmpty())
                        m_bugtracker->setBaseUrl(tmp_base_url);

                    if (!tmp_url.isEmpty())
                        m_bugtracker->setWebserviceUrl(tmp_url);

                    connect(m_bugtracker, SIGNAL(version(QString)), this, SLOT(getBugtrackerVersion(QString)));
                    connect(m_bugtracker, SIGNAL(error(QString)), this, SLOT(getBugtrackerError(QString)));

                    m_bugtracker->getVersion();
                }
            }
        }
    }
}


void FormProjectVersion::getBugtrackerVersion(QString version)
{
    QMessageBox::information(this, m_ui->bug_tracker_url->text(), tr("La version du bugtracker est : %1").arg("<b>" + version + "</b>") + "<p><span style=\"color: green\"><b>" + tr("La connexion est correcte.") + "</b></span></p>");
}


void FormProjectVersion::getBugtrackerError(QString errorMsg)
{
    QMessageBox::critical(this, m_ui->bug_tracker_url->text(),  errorMsg+"<p><span style=\"color: red\"><b>"+tr("La requête a échouée.")+"</b></span><p>");
}



void FormProjectVersion::loadPluginsViews()
{
    QMap < QString, ClientModule*> tmp_modules_map = Session::instance().externalsModules().value(ClientModule::ProjectPlugin);

    ProjectModule   *tmp_project_module = NULL;

    foreach(ClientModule *tmp_module, tmp_modules_map)
    {
        tmp_project_module = static_cast<ProjectModule*>(tmp_module);

        tmp_project_module->loadProjectModuleDatas(m_project_record);

        QWidget     *tmp_module_view = tmp_project_module->createView(this);
        if (tmp_module_view != NULL)
        {
            m_views_modules_map[tmp_project_module] = tmp_module_view;
            m_ui->projectTabWidget->addTab(tmp_module_view, tmp_module->getModuleName());
        }
    }
}


void FormProjectVersion::savePluginsDatas()
{
    QMap<ProjectModule*, QWidget*>::iterator tmp_module_iterator;

    for(tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++)
    {
        tmp_module_iterator.key()->saveProjectModuleDatas();
    }
}


void FormProjectVersion::destroyPluginsViews()
{
    QMap<ProjectModule*, QWidget*>::iterator tmp_module_iterator;

    for(tmp_module_iterator = m_views_modules_map.begin(); tmp_module_iterator != m_views_modules_map.end(); tmp_module_iterator++)
    {
        tmp_module_iterator.key()->destroyView(tmp_module_iterator.value());
    }
}
