/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef FORM_CAMPAIGN_H
#define FORM_CAMPAIGN_H

#include <QtWidgets/QTreeView>
#include "gui/components/hierarchies/campaignhierarchy.h"
#include "entities/testcampaign.h"
#include "entities/executioncampaign.h"
#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/AbstractProjectWidget.h"

#include "gui/testsplaneditor/testsplanscene.h"

#include <clientmodule.h>

namespace Ui {
    class FormCampaign;
}

class FormCampaign : public AbstractProjectWidget {
    Q_OBJECT
    Q_DISABLE_COPY(FormCampaign)
public:
    explicit FormCampaign(Campaign *in_campaign, QWidget *parent = 0);
    virtual ~FormCampaign();

    QIcon icon() const;
    QString title() const;

signals:
    void canceled();

public slots:
    void save();
    void cancel();
    void deleteTestsCampaignList(QList<Hierarchy*> in_records_list);
    void execute();
    void executeTestsPlan();
    void open(QModelIndex in_index);
    void reloadCampaign();
    void deleteSelectedExecutionCampaign();
    void executeSelectedExecutionCampaign();
    void manageExecutionsSelection();
    void toolBoxPageChanged(int index);

protected:
    virtual void changeEvent(QEvent *e);
    void showEvent(QShowEvent* event);
    void setExecutionCampaignForRow(ExecutionCampaign *in_execution_campaign, int in_row);

    bool saveCampaign();

    virtual void closeEvent(QCloseEvent *in_event);

    void loadCampaign(Campaign *in_campaign);

    void execute(ExecutionCampaign *in_execution_campaign);

private:
    Ui::FormCampaign       *m_ui;

    RecordsTreeView       *m_tests_tree_view;
    RecordsTreeModel      *m_tests_tree_model;

    Campaign             *m_campaign;

    TestsPlanScene*     m_tests_plan_scene;

    QMap<CampaignModule*, QWidget*>   m_views_modules_map;

    bool                m_page_tests_plan_first_time_show;

    void loadPluginsViews();
    void destroyPluginsViews();
    void savePluginsDatas();

};

#endif // FORM_CAMPAIGN_H
