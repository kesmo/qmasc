/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gui/components/RecordTextEditContainer.h"
#include "entities/projectversion.h"
#include "entities/campaign.h"
#include "gui/forms/FormRequirement.h"
#include "gui/forms/FormCampaign.h"
#include "gui/forms/FormProjectVersion.h"
#include "gui/forms/FormExecutionsReports.h"
#include "gui/forms/FormProjectBugs.h"
#include "gui/forms/FormTest.h"

#include "gui/components/hierarchies/projectshierarchy.h"
#include "gui/components/hierarchies/projectrequirementshierarchy.h"
#include "gui/components/hierarchies/projecttestshierarchy.h"

#if defined(NETSCAPE_PLUGIN)
#include <qtbrowserplugin.h>

#ifdef QAXSERVER
#include <ActiveQt/QAxBindable>
#include <ActiveQt/QAxFactory>
#include <qt_windows.h>
#endif

#endif

#include <QMainWindow>
#include <QtGlobal>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QFileInfo>
#include <QtPrintSupport/QPrinter>
#include <QSignalMapper>
#include <QMdiSubWindow>
#include <QMdiArea>

class QAction;
class QToolBar;
class QMenu;
class QMenuBar;
class QStatusBar;
class QFileInfo;

void updateProgressBar(unsigned long long int in_index, unsigned long long int in_count, unsigned long long int in_step, const char* in_message);

#define     DOCK_WIDGET_TESTS               "0"
#define     DOCK_WIDGET_REQUIREMENTS        "1"
#define     DOCK_WIDGET_CAMPAIGNS           "2"
#define     DOCK_WIDGET_PARAMETERS          "3"
#define     DOCK_WIDGET_SEARCH              "4"

class MainWindow :
    public QMainWindow,
    public TestActionAttachmentsManager
{
    Q_OBJECT

        Q_CLASSINFO("MIME", "application/x-rtmr:xxx:RTMR application")

public:

    static MainWindow* instance()
    {
        return m_unique_instance;
    }

    MainWindow();
    ~MainWindow();

    QMdiSubWindow* createMdiChildForRecord(QWidget* child);

    template <class T>
    QMdiSubWindow* findSubWindowForRecordColumnValue(const QString &in_record_column_name, const QString &in_record_column_value)
    {
        QList< QPair<QString, QString> > tmp_list;
        tmp_list << QPair<QString, QString>(in_record_column_name, in_record_column_value);
        return findSubWindowForRecordColumnsValues<T>(tmp_list);
    }

protected slots:

    void closeEvent(QCloseEvent *event);
    void changeEvent(QEvent*);
    void slotLanguageChanged(QAction* action);
    void timerEvent(QTimerEvent * event);

public slots:

    void logon();
    void logoff();

    void saveCurrentChildWindow();

    void newProject();
    void projectSelected(const QString &projectId, const QString &projectVersionId);

    void openProject();

    void setConnected();
    void setDisconnected();

    void updateActiveSubWindowMenu();

    void manageUsers();

    void projectsReports();
    void manageCustomTestsFields();
    void manageCustomRequirementsFields();

    void searchProject();

    void menuChildsWindowsAboutToShow();
    void setActiveSubWindow(QWidget *in_window);

    void about();

    void changePassword();
    void showOption();

    void toogleSearchPanel();

    void loadLanguage(const QString& in_language);
    void retranslateUi();


    void checkServerConnection();

private:

    static MainWindow* m_unique_instance;

    int m_keepalive_timer_id;
    int  m_previous_ping_status;

    QMdiArea    *mdiArea;
    QSignalMapper   *windowMapper;

    /* Docks widgets */
    QDockWidget *m_dock_search_widget;

    /* Menu ? */
    QMenu       *menuHelp;

    /* Menu des projets ouverts */
    QMenu       *menuChildsWindows;
    QAction     *closeWindowAction;
    QAction     *closeAllWindowsAction;
    QAction     *tileWindowAction;
    QAction     *cascadeWindowsAction;
    QAction     *nextWindowAction;
    QAction     *previousWindowAction;
    QAction     *separatorWindowAction;
    QAction     *showSearchPanelAction;

    QAction     *aboutAction;

    /* Menu fichier */
    QMenu       *menuFichier;
    QAction     *actionConnection;
    QAction     *actionDeconnection;

    QAction     *actionNewProject;
    QAction     *actionOpenProject;

    QAction     *actionEnregistrerFenetreCourante;
    QAction     *actionFermerFenetreCourante;
    QAction     *actionFermerFenetres;

    QAction     *actionQuitter;

    /* Menu outils */
    QMenu       *menuOutils;
    QAction     *actionChangePassword;
    QAction     *actionShowOptions;
    QMenu *menuSelectLanguage;

    /* Menu administration */
    QMenu *menuAdministration;
    // Gestion des utilisateurs
    QAction *manageUsersAction;
    // Gestion des rapports
    QAction *projectsReportsAction;
    QAction *toolbarSeparatorAboutAction;
    // Champs personnalisés
    QMenu *manageCustomFieldsMenu;
    QAction *manageCustomTestsFieldsAction;
    QAction *manageCustomRequirementsFieldsAction;

    /* Barre de menus */
    QMenuBar    *menuBar;

    /* Barre d'outils */
    QToolBar    *mainToolBar;

    /* Barre de status */
    QStatusBar  *statusBar;

    QTableWidget          *m_search_table_widget;

    // Actions du menu navigation
    void createSearchWidget();
    void createToolBar();
    void createStatusBar();

    void createMenus();

    void readSettings();
    void writeSettings();

    void createLanguageMenu();

    bool isSearchPanelVisible();

    void loadStyleSheet(bool loadFile);

    template <class T>
    QMdiSubWindow* findSubWindowForRecordColumnsValues(QList< QPair<QString, QString> > in_record_columns_values)
    {
        foreach(QMdiSubWindow *window, mdiArea->subWindowList())
        {
            T *mdiChild = qobject_cast<T*>(window->widget());
            if (mdiChild && mdiChild->record()) {
                bool tmp_found = true;
                QPair<QString, QString> tmp_column_value_pair;
                foreach(tmp_column_value_pair, in_record_columns_values)
                {
                    tmp_found = (strcmp(mdiChild->record()->getValueForKey(tmp_column_value_pair.first.toStdString().c_str()),
                                        tmp_column_value_pair.second.toStdString().c_str()) == 0);
                }

                if (tmp_found)
                    return window;
            }
        }

        return NULL;
    }


    QPrinter *print();
    QTextDocument* generateTestsDocument(QList<Test*> in_tests_list, bool include_image = true, QFileInfo file_info = QFileInfo());
    QTextDocument* generateRequirementsDocument(QList<Requirement*> in_requirements_list, bool include_image = true, QFileInfo file_info = QFileInfo());

    QString testToHtml(QTextDocument *in_doc, Test *in_test, QString suffix, int level, QString images_folder);
    QString requirementToHtml(QTextDocument *in_doc, Requirement *in_requirement, QString suffix, int level, QString images_folder);


signals:
    void testUpdated(QModelIndex);
    void requirementUpdated(QModelIndex);

    public slots:

    // Exigences
    void showSearchItem(QModelIndex in_model_index);
    void searchItemsSelectionChanged();

    void showSearchResults(const QList<Record*> & out_records_list);
    void setSearchRecordAtIndex(Record *in_record, int in_index);

    void showCylicRedundancyAlert();
};

#endif // MAINWINDOW_H
