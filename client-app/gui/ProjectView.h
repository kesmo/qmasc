#ifndef PROJECTVIEW_H
#define PROJECTVIEW_H

#include "entities/projectversion.h"
#include "gui/components/hierarchies/projecthierarchy.h"
#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "components/AbstractProjectWidget.h"

#include <QWidget>
#include <QTabWidget>

class ProjectView :
    public AbstractProjectWidget
{
    Q_OBJECT

public:
    ProjectView(const QString &projectId, const QString &projectVersionId = QString(), QWidget *parent = NULL);
    ~ProjectView();

    template <typename T, typename U>
    T *addProjectWidget(U *record) {
        bool found = false;
        T *projectWidget = nullptr;
        for (int index = 0; index < m_tabWidget->count(); index++) {
            T *widget = qobject_cast<T*>(m_tabWidget->widget(index));
            if (widget && widget->record() == record) {
                projectWidget = widget;
                break;
            }
        }

        if (!projectWidget) {
            projectWidget = new T(record, this);
            connect(projectWidget, &AbstractProjectWidget::recordSaved, this, &ProjectView::recordSaved);
        }
        addTab(projectWidget);

        return projectWidget;
    }

    void addTab(AbstractProjectWidget *widget);

    void selectProjectVersion(const QString &id);

public slots:
    void showTestWithContentId(ProjectVersion *in_project_version, const char *in_test_content_id);
    void showOriginalTestInfos(Test *in_test);
    void showRequirementWithOriginalContentId(ProjectVersion *in_project_version, const char *in_original_requirement_content_id);

protected slots:
    void userEnterIndex(const QModelIndex& modelIndex);
    void closeTabAtIndex(int index);
    void recordSaved(Record *in_record);

private:
    ProjectHierarchy *m_projectHierarchy;
    RecordsTreeModel *m_projectTreeModel;
    RecordsTreeView *m_projectTreeView;

    QTabWidget *m_tabWidget;

};

#endif // PROJECTVIEW_H
