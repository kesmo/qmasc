#include "ProjectView.h"
#include "gui/services/testsservices.h"
#include "gui/services/requirementsservices.h"

#include <QGridLayout>
#include <QSplitter>
#include <QPainter>
#include <QPalette>
#include <QProxyStyle>
#include <QSettings>



ProjectView::ProjectView(const QString &projectId, const QString &projectVersionId, QWidget *parent) :
    AbstractProjectWidget(Project::loadEntity(projectId), parent)
{
    m_projectTreeView = new RecordsTreeView();
    m_projectTreeView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_projectTreeView->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
    m_projectTreeView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    m_projectTreeView->setDragEnabled(true);
    m_projectTreeView->setAcceptDrops(true);
    m_projectTreeView->setDropIndicatorShown(true);
    m_projectTreeView->setHeaderHidden(true);
    m_projectTreeView->setExpandsOnDoubleClick(false);

    connect(m_projectTreeView, &RecordsTreeView::userEnterIndex, this, &ProjectView::userEnterIndex);

    m_projectHierarchy = new ProjectHierarchy(dynamic_cast<Project*>(record()), this);

    m_projectTreeModel = new RecordsTreeModel(m_projectHierarchy);
    m_projectTreeView->setRecordsTreeModel(m_projectTreeModel);

    QGridLayout *gridLayout = new QGridLayout(this);
    gridLayout->setSpacing(0);
    gridLayout->setContentsMargins(0, 0, 0, 0);

    QSplitter *splitter = new QSplitter(Qt::Horizontal, this);
    splitter->addWidget(m_projectTreeView);

    m_tabWidget = new QTabWidget(this);
    m_tabWidget->tabBar()->setTabsClosable(true);
    connect(m_tabWidget->tabBar(), &QTabBar::tabCloseRequested, this, &ProjectView::closeTabAtIndex);
    splitter->addWidget(m_tabWidget);

    splitter->setSizes(QList<int>() << 200 << 1000);

    gridLayout->addWidget(splitter);

    // Projet par defaut
    QSettings tmp_settings;

    QString tmp_default_project_version_id;
    if (projectVersionId.isEmpty())
        tmp_default_project_version_id = tmp_settings.value("last_opened_project_version_id").toString();
    else
        tmp_default_project_version_id = projectVersionId;

    selectProjectVersion(tmp_default_project_version_id);

}


ProjectView::~ProjectView()
{
    delete record();
}

void ProjectView::selectProjectVersion(const QString &projectVersionId)
{
    if (projectVersionId.isEmpty())
        return;

    ProjectVersion tmpProjectVersion;
    tmpProjectVersion.setValueForKey(record()->getIdentifier(), PROJECTS_VERSIONS_TABLE_PROJECT_ID);
    tmpProjectVersion.setValueForKey(projectVersionId.toStdString().c_str(), PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID);

    QModelIndex  tmp_index = m_projectTreeModel->modelIndexForRecord(&tmpProjectVersion, false);
    if (tmp_index.isValid()) {
        ProjectVersion *projectVersion(dynamic_cast<ProjectVersion*>(m_projectTreeModel->getItem(tmp_index)->getRecord()));
        if (projectVersion) {
            Session::instance().setDefaultProjectVersionId(projectVersion->getIdentifier());
            m_projectTreeView->expandIndex(tmp_index);
        }
    }
}


void ProjectView::showTestWithContentId(ProjectVersion *in_project_version, const char *in_test_content_id)
{
    Gui::Services::Tests::showTestWithContentId(m_projectTreeView, in_project_version, in_test_content_id);
}

void ProjectView::showOriginalTestInfos(Test *in_test)
{
    Gui::Services::Tests::showOriginalTestInfos(m_projectTreeView, in_test);
}

void ProjectView::showRequirementWithOriginalContentId(ProjectVersion *in_project_version, const char *in_original_requirement_content_id)
{
    Gui::Services::Requirements::showRequirementWithOriginalContentId(in_project_version, m_projectTreeView, m_projectTreeModel, in_original_requirement_content_id);
}

void ProjectView::userEnterIndex(const QModelIndex& modelIndex)
{
    Hierarchy     *tmp_selected_item = NULL;

    if (modelIndex.isValid()) {
        m_projectTreeView->setCurrentIndex(modelIndex);
        tmp_selected_item = m_projectTreeView->getRecordsTreeModel()->getItem(modelIndex);
        if (tmp_selected_item) {
            tmp_selected_item->userSelectItem();
        }
    }
}

void ProjectView::recordSaved(Record *in_record)
{
    QModelIndex tmp_model_index = m_projectTreeModel->modelIndexForRecord(in_record);
    if (tmp_model_index.isValid())
        m_projectTreeView->update(tmp_model_index);
}

void ProjectView::addTab(AbstractProjectWidget *widget)
{
    if (widget) {
        if (m_tabWidget->indexOf(widget) < 0) {
            m_tabWidget->addTab(widget, widget->icon(), widget->title());
        }
        m_tabWidget->setCurrentWidget(widget);
    }
}

void ProjectView::closeTabAtIndex(int index)
{
    if (index >= 0 && index < m_tabWidget->count()) {
        AbstractProjectWidget* tmpWidget = qobject_cast<AbstractProjectWidget*>(m_tabWidget->widget(index));
        bool close = false;
        if (tmpWidget)
            close = tmpWidget->close();

        if (close)
            m_tabWidget->removeTab(index);
    }
}
