#include "requirementsservices.h"

#include "services.h"

#include "session.h"

#include "entities/requirementcategory.h"

#include "gui/forms/FormDataExport.h"
#include "gui/forms/FormDataImport.h"

#include "gui/mainwindow.h"

#include "projectsservices.h"

#include <QFileDialog>
#include <QApplication>
#include <QMessageBox>
#include <QtPrintSupport/QPrinter>
#include <QMdiSubWindow>

namespace Gui
{
namespace Services
{
namespace Requirements
{
static const char *REQUIREMENT_COLUMNS_IMPORT[] = {REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME, REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION, REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID, REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL};
static const entity_def requirement_entity_import = {0, 0, 0, 0, REQUIREMENT_COLUMNS_IMPORT, 0, 0, sizeof(REQUIREMENT_COLUMNS_IMPORT)/sizeof(char*)};


void saveRequirementsAsHtml(QList<Requirement*> requirements_list)
{
    if (!requirements_list.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(QApplication::activeWindow(), "Export html", QString(), "*.html");
        if (!fileName.isEmpty())
        {
            QFile tmp_file(fileName);
            if (tmp_file.open(QIODevice::WriteOnly))
            {
                TestActionAttachmentsManager manager;
                tmp_file.write(generateRequirementsDocument(&manager, requirements_list, false, QFileInfo(tmp_file))->toHtml("utf-8").toLatin1());
                tmp_file.close();
            }
            else
            {
                QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
            }
        }
    }
}

QTextDocument* generateRequirementsDocument(TestActionAttachmentsManager* /*in_attachments_manager*/, QList<Requirement*> in_requirements_list, bool include_image, QFileInfo file_info)
{
    QString            tmp_doc_content = QString("<html><head></head><body>");
    QTextDocument               *tmp_doc = new QTextDocument();
    int                tmp_index = 0;

    QString         tmp_images_folder_absolute_path;
    QString         tmp_images_folder_name;

    if (!include_image)
    {
        if (file_info.isFile())
        {
            tmp_images_folder_absolute_path = file_info.canonicalFilePath()+"_images";
            QDir tmp_images_dir(tmp_images_folder_absolute_path);
            tmp_images_folder_name = tmp_images_dir.dirName();
            if (!tmp_images_dir.exists())
                file_info.absoluteDir().mkdir(tmp_images_folder_name);
        }
    }

    tmp_index = 0;
    foreach(Requirement *tmp_requirement, in_requirements_list)
    {
        tmp_index++;
        tmp_doc_content += requirementToHtml(tmp_doc, tmp_requirement, QString::number(tmp_index) + ".", 0, tmp_images_folder_absolute_path);
    }

    tmp_doc_content += "</body></html>";

    tmp_doc->setHtml(tmp_doc_content);


    return tmp_doc;
}

void printRequirements(QList<Requirement*> requirements_list)
{
    if (!requirements_list.isEmpty())
    {
        QPrinter *printer = Gui::Services::Global::print();
        if (printer)
        {
            if (!printer->outputFileName().isEmpty())
            {
                TestActionAttachmentsManager manager;
                generateRequirementsDocument(&manager, requirements_list)->print(printer);
            }
            delete printer;
        }
    }
}


QString requirementToHtml(QTextDocument *in_doc, Requirement *in_requirement, QString suffix, int level, QString images_folder)
{
    QString        tmp_html_content = QString();
    int            tmp_index = 0;
    RequirementContent        *tmp_requirement_content = new RequirementContent();

    if (tmp_requirement_content->loadRecord(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID)) == NOERR)
    {
        switch (level)
        {
        case 0:
            tmp_html_content += "<h2 style=\"page-break-before: auto;\">" + suffix + " " + QString(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)) + "</h2>";
            break;
        case 1:
            tmp_html_content += "<h3>" + suffix + " " + QString(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)) + "</h3>";
            break;
        case 2:
            tmp_html_content += "<h4>" + suffix + " " + QString(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)) + "</h4>";
            break;
        case 3:
            tmp_html_content += "<h5>" + suffix + " " + QString(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)) + "</h5>";
            break;
        default:
            tmp_html_content += "<h6>" + suffix + " " + QString(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)) + "</h6>";
            break;
        }

        if (!is_empty_string(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION)))
            tmp_html_content += "<u>"+qApp->tr("Description")+"</u> : " + QString(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION)) + "<br/>";

        const char *category_label = Record::matchingValueInRecordsList(Session::instance().requirementsCategories(),
                                                                        REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID,
                                                                        tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID),
                                                                        REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL);
        if (!is_empty_string(category_label))
            tmp_html_content += "<u>"+qApp->tr("Catégorie")+"</u> : " + QString(category_label) + "<br/>";

        tmp_html_content += "<u>"+qApp->tr("Priorité")+"</u> : " + tmp_requirement_content->getPriorityLabel() + "<br/>";

        tmp_html_content += "<br/>";

        foreach(Requirement *tmp_requirement, in_requirement->getAllChilds())
        {
            tmp_index++;
            tmp_html_content += requirementToHtml(in_doc, tmp_requirement, suffix + QString::number(tmp_index) + ".", level + 1, images_folder);
        }
    }
    delete tmp_requirement_content;

    return tmp_html_content;
}

void writeRequirementsListToExportFile(QFile & in_file, QList<Requirement*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QString                 tmp_str;

    QTextDocument           tmp_doc;

    Requirement    *tmp_parent = NULL;
    RequirementContent      *tmp_requirement_content = NULL;

    qint64                  tmp_bytes_write = 0;

    foreach(Requirement *tmp_record, in_records_list)
    {
        tmp_requirement_content = new RequirementContent();
        if (tmp_requirement_content != NULL)
        {
            if (tmp_requirement_content->loadRecord(tmp_record->getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID)) == NOERR)
            {
                // Nom de l'exigence
                tmp_str = "";
                tmp_parent = tmp_record;
                while (tmp_parent->parentRequirement() != NULL)
                {
                    tmp_parent = tmp_parent->parentRequirement();
                    tmp_str = QString(tmp_parent->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)) + PARENT_HIERARCHY_SEPARATOR + tmp_str;
                }
                tmp_str += QString(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME));

                tmp_str = in_field_enclosing_char + tmp_str.replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
                tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
                if (tmp_bytes_write < 0)    break;

                // Separateur de champs
                tmp_bytes_write = in_file.write(in_field_separator);
                if (tmp_bytes_write < 0)    break;

                // Description de l'exigence
                tmp_doc.setHtml(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION));
                tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
                tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
                if (tmp_bytes_write < 0)    break;

                // Separateur de champs
                tmp_bytes_write = in_file.write(in_field_separator);
                if (tmp_bytes_write < 0)    break;

                // Catégorie de l'exigence
                tmp_str = in_field_enclosing_char + QString(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID)).replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
                tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
                if (tmp_bytes_write < 0)    break;

                // Separateur de champs
                tmp_bytes_write = in_file.write(in_field_separator);
                if (tmp_bytes_write < 0)    break;

                // Priorité de l'exigence
                tmp_str = in_field_enclosing_char + QString(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL)).replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
                tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
                if (tmp_bytes_write < 0)    break;

                // Separateur d'enregistrements
                tmp_bytes_write = in_file.write(in_record_separator);

                // Exigences filles
                writeRequirementsListToExportFile(in_file, tmp_record->getAllChilds(), in_field_separator, in_record_separator, in_field_enclosing_char);
            }

            delete tmp_requirement_content;
        }
    }

}

/**
  Exporter des exigences
**/
void exportRequirements(RecordsTreeView* in_tree_view)
{
    FormDataExport    *tmp_export_form = NULL;

    tmp_export_form = new FormDataExport(new ExportContext(in_tree_view->selectedHierachies()), QApplication::activeWindow());
//    connect(tmp_export_form, SIGNAL(startExport(QString,QByteArray,QByteArray, QByteArray)), QApplication::activeWindow(), SLOT(startExportRequirements(QString,QByteArray,QByteArray, QByteArray)));
    tmp_export_form->show();
}


/**
  Afficher les informations d'une exigence
**/
void showRequirementInfos(RecordsTreeView *in_tree_view, QModelIndex in_model_index, bool /*in_update_history*/)
{
    Hierarchy         *tmp_selected_requirement = NULL;

    if (in_model_index.isValid())
    {
        in_tree_view->setCurrentIndex(in_model_index);
        tmp_selected_requirement = in_tree_view->getRecordsTreeModel()->getItem(in_model_index);
        showRequirementInfos(dynamic_cast<Requirement*>(tmp_selected_requirement->getRecord()));
    }
}


/**
  Inserer une exigence
**/
void insertRequirementAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(in_tree_view->getRecordsTreeModel()->rowCount(index), 1, index))
            showRequirementInfos(in_tree_view, in_tree_view->getRecordsTreeModel()->index(in_tree_view->getRecordsTreeModel()->rowCount(index) - 1, 0, index));
    }
}


void addRequirementAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(index.row()+1, 1, index.parent()))
            showRequirementInfos(in_tree_view, in_tree_view->getRecordsTreeModel()->index(index.row()+1, 0, index.parent()));
    }
}


/**
  Afficher les informations de l'exigence selectionnee
**/
void showRequirementInfos(Requirement *in_requirement)
{
    Qt::KeyboardModifiers tmp_modifiers = QApplication::keyboardModifiers();

    if (in_requirement != NULL && !(tmp_modifiers == Qt::ShiftModifier || tmp_modifiers == Qt::ControlModifier || tmp_modifiers == (Qt::ControlModifier|Qt::ShiftModifier)))
    {
        ProjectView *projectView = Projects::showProjectView(in_requirement->projectVersion()->project()->getIdentifier());
        FormRequirement *formRequirement = projectView->addProjectWidget<FormRequirement, Requirement>(in_requirement);
        QObject::connect(formRequirement, &FormRequirement::showTestWithContentId, projectView, &ProjectView::showTestWithContentId, Qt::UniqueConnection);

    }
}


void showRequirementWithOriginalContentId(ProjectVersion *in_project_version, RecordsTreeView* in_tree_view, RecordsTreeModel* in_tree_model, const char *in_original_requirement_content_id)
{
    QModelIndex                tmp_model_index;
    Requirement    *tmp_requirement = NULL;
    QList<Requirement*> tmp_requirements_list;

    net_session             *tmp_session = Session::instance().getClientSession();

    if (is_empty_string(in_original_requirement_content_id) == FALSE)
    {
        in_tree_view->selectionModel()->select(in_tree_view->selectionModel()->selection(), QItemSelectionModel::Deselect);

        /* Chercher l'exigence dans l'arborescence */
        tmp_requirement = Requirement::findRequirementWithValueForKey(in_project_version->requirementsHierarchy(), in_original_requirement_content_id, REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID);
        if (tmp_requirement != NULL)
        {
            tmp_model_index = in_tree_model->modelIndexForRecord(tmp_requirement);
            if (tmp_model_index.isValid())
            {
                in_tree_view->expandIndex(tmp_model_index);
                showRequirementInfos(in_tree_view, tmp_model_index);
            }
        }
        /* Chercher l'exigence en base (ex: si l'exigence est dans une autre version du projet) */
        else
        {
            net_session_print_where_clause(tmp_session, "%s=%s", REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID, in_original_requirement_content_id);

            tmp_requirements_list = Requirement::loadRecordsList(tmp_session->m_where_clause_buffer);
            if (tmp_requirements_list.size() == 1) {
                showRequirementInfos(tmp_requirements_list[0]);
            }
            else {
                qDeleteAll(tmp_requirements_list);
            }

        }
    }
}


/**
  Supprimer la liste des exigences passée en paramètre
**/
void removeRequirementsList(RecordsTreeModel* in_tree_model, const QList<Requirement*>& in_records_list)
{
    QModelIndex         tmp_model_index;
    QMessageBox         *tmp_msg_box = NULL;
    QPushButton         *tmp_del_button = NULL;
    QPushButton         *tmp_del_all_button = NULL;
    QPushButton         *tmp_cancel_button = NULL;
    bool                tmp_delete_all = false;
    bool                tmp_delete_current = false;

    if (in_records_list.count() > 0)
    {
        tmp_msg_box = new QMessageBox(QApplication::activeWindow());
        tmp_msg_box->setIcon(QMessageBox::Question);
        tmp_msg_box->setWindowTitle(qApp->tr("Confirmation..."));

        if (in_records_list.count() > 1)
            tmp_del_all_button = tmp_msg_box->addButton(qApp->tr("Supprimer tous les exigences"), QMessageBox::NoRole);

        tmp_del_button = tmp_msg_box->addButton(qApp->tr("Supprimer l'exigence"), QMessageBox::YesRole);
        tmp_cancel_button = tmp_msg_box->addButton(qApp->tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->setDefaultButton(tmp_cancel_button);

        foreach(Requirement *tmp_requirement, in_records_list)
        {
            tmp_delete_current = tmp_delete_all;

            if (tmp_delete_all == false)
            {
                tmp_msg_box->setText(qApp->tr("Etes-vous sûr(e) de vouloir supprimer l'exigence \"%1\" ?").arg(tmp_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)));
                tmp_msg_box->exec();
                if (tmp_msg_box->clickedButton() == tmp_del_button)
                {
                    tmp_delete_current = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_del_all_button)
                {
                    tmp_delete_current = true;
                    tmp_delete_all = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_cancel_button)
                    break;
            }

            tmp_model_index = in_tree_model->modelIndexForRecord(tmp_requirement);
            if (tmp_delete_current && tmp_model_index.isValid())
                in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
        }

        delete tmp_msg_box;
    }
}

ExportContext::ExportContext(QList<Hierarchy*> in_records_list) :
    Gui::Components::ExportContext(in_records_list)
{

}

void ExportContext::startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QFile                   tmp_file;

    tmp_file.setFileName(in_filepath);
    if (tmp_file.open(QIODevice::WriteOnly))
    {
        // Entête
        QString tmp_header;
        QStringList        tmp_columns_names = getImportExportRequirementColumnsNames();
        for (int tmp_column_index = 0; tmp_column_index < tmp_columns_names.count() - 1; ++tmp_column_index)
        {
            tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_column_index] + in_field_enclosing_char + in_field_separator;
            tmp_file.write(tmp_header.toStdString().c_str());
        }
        tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_columns_names.count() - 1] + in_field_enclosing_char + in_record_separator;
        tmp_file.write(tmp_header.toStdString().c_str());

        QList<Requirement*>    exportList;
        foreach(Hierarchy* hierarchy, m_records_list){
            if (hierarchy->getRecord()){
                if (hierarchy->getRecord()->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID){
                    exportList.append(dynamic_cast<Requirement*>(hierarchy->getRecord()));
                }
                else if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_VERSIONS_TABLE_SIG_ID){
                    exportList.append(ProjectVersion::RequirementRelation::instance().getChilds(dynamic_cast<ProjectVersion*>(hierarchy->getRecord())));
                }
            }
        }

        writeRequirementsListToExportFile(tmp_file, exportList, in_field_separator, in_record_separator, in_field_enclosing_char);

        tmp_file.close();
    }
    else
    {
        QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
    }
}

QStringList getImportExportRequirementColumnsNames()
{
    QStringList        tmp_columns_names;

    tmp_columns_names << qApp->tr("Nom de l'exigence") << qApp->tr("Description de l'exigence") << qApp->tr("Catégorie de l'exigence") << qApp->tr("Priorité de l'exigence");

    return tmp_columns_names;
}


ImportContext::ImportContext(RecordsTreeView* in_tree_view) :
    Gui::Components::ImportContext(),
    m_tree_view(in_tree_view)
{
    m_columns_names = getImportExportRequirementColumnsNames();
    m_entity = &requirement_entity_import;

    QList<Hierarchy*> tmp_records_list = in_tree_view->selectedHierachies();
    Requirement *tmp_requirement = NULL;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(in_tree_view->getRecordsTreeModel(), in_tree_view->selectionModel()->currentIndex());

    if (tmp_project_version)
    {
        if (tmp_records_list.isEmpty())
        {
            if (tmp_project_version->requirementsHierarchy().isEmpty() == false)
            {
                tmp_requirement = tmp_project_version->requirementsHierarchy().last();
            }
        }
        else
        {
            tmp_requirement = dynamic_cast<Requirement*>(tmp_records_list.last()->getRecord());
        }

        if (tmp_requirement != NULL)
        {
            QMessageBox::information(MainWindow::instance(), qApp->tr("Information"), qApp->tr("Les exigences seront importées après l'exigence' \"%1\".").arg(tmp_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)));

            if (tmp_requirement->parentRequirement() != NULL)
                m_parent = new Requirement(tmp_requirement->parentRequirement());
            else
                m_parent = new Requirement(tmp_project_version);
        }
        else
            m_parent = new Requirement(tmp_project_version);
    }
}


void ImportContext::importRecord(Record *in_record, bool in_last_record)
{
    RequirementContent        *tmp_requirement_content = NULL;
    int            tmp_save_result = NOERR;

    Requirement    *tmp_requirement_parent = NULL;
    Requirement    *tmp_requirement = NULL;

    Requirement    *tmp_existing_requirement = NULL;

    QStringList                 tmp_str_list;

    if (m_parent != NULL && in_record != NULL)
    {
        tmp_requirement_parent = dynamic_cast<Requirement*>(m_parent);

        if (is_empty_string(in_record->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME)) == FALSE)
        {
            tmp_str_list = QString(in_record->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME)).split(PARENT_HIERARCHY_SEPARATOR, QString::SkipEmptyParts);

            foreach(QString tmp_str_name, tmp_str_list)
            {
                if (!tmp_str_name.isEmpty())
                {
                    tmp_existing_requirement = tmp_requirement_parent->getUniqueChildWithValueForKey(tmp_str_name.toStdString().c_str(), REQUIREMENTS_HIERARCHY_SHORT_NAME);
                    if (tmp_existing_requirement == NULL)
                    {
                        // Creation de l'exigence importée
                        tmp_requirement_content = new RequirementContent();
                        if (tmp_requirement_content != NULL)
                        {
                            tmp_requirement_content->setValueForKey(tmp_str_name.toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME);
                            tmp_requirement_content->setValueForKey(in_record->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION), REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION);
                            tmp_requirement_content->setValueForKey(in_record->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID), REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID);
                            tmp_requirement_content->setValueForKey(in_record->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL), REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL);
                            ProjectVersion::RequirementContentRelation::instance().setParent(tmp_requirement_parent->projectVersion(), tmp_requirement_content);
                            tmp_save_result = tmp_requirement_content->saveRecord();
                            if (tmp_save_result == NOERR)
                            {
                                // Ajout de l'exigence à l'arborescence
                                tmp_requirement = new Requirement(tmp_requirement_parent->projectVersion());
                                if (tmp_requirement != NULL)
                                {
                                    tmp_requirement->setDataFromRequirementContent(tmp_requirement_content);

                                    tmp_requirement_parent->appendChild(tmp_requirement);

                                    tmp_requirement_parent = tmp_requirement;
                                }
                            }

                            delete tmp_requirement_content;
                        }
                    }
                    else
                        tmp_requirement_parent = tmp_existing_requirement;
                }
            }
        }
    }

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());

        QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));

    }
    else if (in_last_record)
    {
        endImportRequirements();
    }

}


/**
  Importer des exigences
**/
void importRequirements(RecordsTreeView* in_tree_view)
{
    FormDataImport* tmp_import_form = new FormDataImport(new ImportContext(in_tree_view), MainWindow::instance());

    tmp_import_form->show();
}


void ImportContext::startImport()
{
    RequirementContent        *tmp_requirement_content = NULL;
    int                tmp_save_result = NOERR;

    Requirement    *tmp_requirement_parent = NULL;
    QString detail = qApp->tr("_ A classer (import des exigences de %1 le %2)")
            .arg(Session::instance().getClientSession()->m_username)
            .arg(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm"));

    tmp_requirement_parent = dynamic_cast<Requirement*>(m_parent);

    cl_transaction_start(Session::instance().getClientSession());

    tmp_requirement_content = new RequirementContent();
    ProjectVersion::RequirementContentRelation::instance().setParent(tmp_requirement_parent->projectVersion(), tmp_requirement_content);

    tmp_requirement_content->setValueForKey(detail.toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME);
    tmp_requirement_content->setValueForKey(detail.toStdString().c_str(), REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION);
    tmp_save_result = tmp_requirement_content->saveRecord();
    if (tmp_save_result == NOERR)
    {
        tmp_requirement_parent->setDataFromRequirementContent(tmp_requirement_content);
        tmp_save_result = tmp_requirement_parent->saveRecord();
    }
    delete tmp_requirement_content;

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


void ImportContext::endImportRequirements()
{
    QList<Hierarchy*>    tmp_records_list = m_tree_view->selectedHierachies();
    Requirement        *tmp_requirement = NULL;
    QModelIndex        tmp_model_index;
    int            tmp_index = 0;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(m_tree_view->getRecordsTreeModel(), m_tree_view->selectionModel()->currentIndex());

    int            tmp_tr_result = NOERR;

    Requirement    *tmp_requirement_parent = NULL;

    tmp_requirement_parent = dynamic_cast<Requirement*>(m_parent);

    if (tmp_records_list.isEmpty())
    {
        if (tmp_project_version->requirementsHierarchy().isEmpty() == false)
        {
            tmp_requirement = tmp_project_version->requirementsHierarchy().last();
        }
    }
    else
    {
        tmp_requirement = dynamic_cast<Requirement*>(tmp_records_list.last()->getRecord());
    }

    if (tmp_requirement != NULL)
    {
        tmp_model_index = m_tree_view->getRecordsTreeModel()->modelIndexForRecord(tmp_requirement);
        if (tmp_model_index.isValid())
        {
            tmp_index = tmp_model_index.row() + 1;
            tmp_model_index = tmp_model_index.parent();
        }
    }
    else
    {
        tmp_index = tmp_project_version->requirementsHierarchy().count();
        tmp_model_index  = m_tree_view->currentIndex();
    }

    if (m_tree_view->getRecordsTreeModel()->insertItem(tmp_index, tmp_model_index, tmp_requirement_parent))
    {
        tmp_tr_result = cl_transaction_commit(Session::instance().getClientSession());
        if (tmp_tr_result != NOERR)
            QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_tr_result));
    }
    else
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}



}
}
}
