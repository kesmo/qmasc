#ifndef REQUIREMENTSSERVICES_H
#define REQUIREMENTSSERVICES_H

#include "entities/feature.h"
#include "entities/featurecontent.h"
#include "gui/components/hierarchies/projectfeatureshierarchy.h"

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/TestActionAttachmentsManager.h"
#include "gui/components/importexportcontext.h"

#include <QTextDocument>
#include <QFileInfo>
#include <QModelIndex>
#include <QTreeView>

namespace Gui
{
namespace Services
{
namespace Features
{
    void saveFeaturesAsHtml(QList<Feature *> features_list);
    QTextDocument* generateFeaturesDocument(TestActionAttachmentsManager* in_attachments_manager, QList<Feature*> in_features_list, bool include_image = true, QFileInfo file_info = QFileInfo());
    void printFeatures(QList<Feature *> features_list);
    QString featureToHtml(QTextDocument *in_doc, Feature *in_feature, QString suffix, int level, QString images_folder);
    void writeFeaturesListToExportFile(QFile & in_file, QList<Feature*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    void exportFeatures(RecordsTreeView *in_tree_view);
    void addFeatureAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void insertFeatureAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void showFeatureInfos(Feature *in_feature);
    void removeFeaturesList(RecordsTreeModel *in_tree_model, const QList<Feature *> &in_features_list);
    void startExportFeatures(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    QStringList getImportExportFeatureColumnsNames();
    void importFeatures(RecordsTreeView *in_tree_view);

    class ImportContext :
            public Gui::Components::ImportContext
    {
    public:
        ImportContext(RecordsTreeView* in_tree_view);

        void startImport();
        void importRecord(Record *out_record, bool in_last_record);

        void endImportFeatures();

    private:

        RecordsTreeView* m_tree_view;
    };


    class ExportContext :
            public Gui::Components::ExportContext
    {
    public:
        ExportContext(QList<Hierarchy*> in_records_list);

        virtual void startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);


    };

}
}
}
#endif // REQUIREMENTSSERVICES_H
