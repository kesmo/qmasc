#ifndef REQUIREMENTSSERVICES_H
#define REQUIREMENTSSERVICES_H

#include "entities/rule.h"
#include "entities/rulecontent.h"
#include "gui/components/hierarchies/projectruleshierarchy.h"

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/TestActionAttachmentsManager.h"
#include "gui/components/importexportcontext.h"

#include <QTextDocument>
#include <QFileInfo>
#include <QModelIndex>
#include <QTreeView>

namespace Gui
{
namespace Services
{
namespace Rules
{
    void saveRulesAsHtml(const QList<Rule *> &rules_list);
    QTextDocument* generateRulesDocument(TestActionAttachmentsManager* in_attachments_manager, QList<Rule*> in_rules_list, bool include_image = true, QFileInfo file_info = QFileInfo());
    void printRules(const QList<Rule *> &rules_list);
    QString ruleToHtml(QTextDocument *in_doc, Rule *in_rule, QString suffix, int level, QString images_folder);
    void writeRulesListToExportFile(QFile & in_file, QList<Rule*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    void exportRules(RecordsTreeView *in_tree_view);
    void addRuleAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void insertRuleAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void showRuleInfos(Rule *in_rule);
    void removeRulesList(RecordsTreeModel *in_tree_model, const QList<Rule *> &in_records_list);
    void startExportRules(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    QStringList getImportExportRuleColumnsNames();
    void importRules(RecordsTreeView *in_tree_view);

    class ImportContext :
            public Gui::Components::ImportContext
    {
    public:
        ImportContext(RecordsTreeView* in_tree_view);

        void startImport();
        void importRecord(Record *out_record, bool in_last_record);

        void endImportRules();

    private:

        RecordsTreeView* m_tree_view;
    };


    class ExportContext :
            public Gui::Components::ExportContext
    {
    public:
        ExportContext(QList<Hierarchy*> in_records_list);

        virtual void startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);


    };

}
}
}
#endif // REQUIREMENTSSERVICES_H
