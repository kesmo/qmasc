#include "featuresservices.h"

#include "services.h"

#include "session.h"

#include "gui/forms/FormFeature.h"
#include "gui/forms/FormDataExport.h"
#include "gui/forms/FormDataImport.h"

#include "gui/mainwindow.h"

#include "projectsservices.h"

#include <QFileDialog>
#include <QApplication>
#include <QMdiSubWindow>
#include <QMessageBox>
#include <QtPrintSupport/QPrinter>

namespace Gui
{
namespace Services
{
namespace Features
{
static const char *FEATURES_COLUMNS_IMPORT[] = {FEATURES_CONTENTS_TABLE_SHORT_NAME, FEATURES_CONTENTS_TABLE_DESCRIPTION};
static const entity_def feature_entity_import = {0, 0, 0, 0, FEATURES_COLUMNS_IMPORT, 0, 0, sizeof(FEATURES_COLUMNS_IMPORT)/sizeof(char*)};


void saveFeaturesAsHtml(QList<Feature*> features_list)
{
    if (!features_list.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(QApplication::activeWindow(), "Export html", QString(), "*.html");
        if (!fileName.isEmpty())
        {
            QFile tmp_file(fileName);
            if (tmp_file.open(QIODevice::WriteOnly))
            {
                TestActionAttachmentsManager manager;
                tmp_file.write(generateFeaturesDocument(&manager, features_list, false, QFileInfo(tmp_file))->toHtml("utf-8").toLatin1());
                tmp_file.close();
            }
            else
            {
                QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
            }
        }
    }
}

QTextDocument* generateFeaturesDocument(TestActionAttachmentsManager* /*in_attachments_manager*/, QList<Feature*> in_features_list, bool include_image, QFileInfo file_info)
{
    QString tmp_doc_content = QString("<html><head></head><body>");
    QTextDocument               *tmp_doc = new QTextDocument();
    int tmp_index = 0;

    QString         tmp_images_folder_absolute_path;
    QString         tmp_images_folder_name;

    if (!include_image)
    {
        if (file_info.isFile())
        {
            tmp_images_folder_absolute_path = file_info.canonicalFilePath()+"_images";
            QDir tmp_images_dir(tmp_images_folder_absolute_path);
            tmp_images_folder_name = tmp_images_dir.dirName();
            if (!tmp_images_dir.exists())
                file_info.absoluteDir().mkdir(tmp_images_folder_name);
        }
    }

    tmp_index = 0;
    foreach(Feature *tmp_feature, in_features_list)
    {
        tmp_index++;
        tmp_doc_content += featureToHtml(tmp_doc, tmp_feature, QString::number(tmp_index) + ".", 0, tmp_images_folder_absolute_path);
    }

    tmp_doc_content += "</body></html>";

    tmp_doc->setHtml(tmp_doc_content);


    return tmp_doc;
}

void printFeatures(QList<Feature*> features_list)
{
    if (!features_list.isEmpty())
    {
        QPrinter *printer = Gui::Services::Global::print();
        if (printer)
        {
            if (!printer->outputFileName().isEmpty())
            {
                TestActionAttachmentsManager manager;
                generateFeaturesDocument(&manager, features_list)->print(printer);
            }
            delete printer;
        }
    }
}


QString featureToHtml(QTextDocument *in_doc, Feature *in_feature, QString suffix, int level, QString images_folder)
{
    QString tmp_html_content = QString();
    int tmp_index = 0;
    FeatureContent *tmp_feature_content = new FeatureContent();

    if (tmp_feature_content->loadRecord(in_feature->getValueForKey(FEATURES_TABLE_FEATURE_CONTENT_ID)) == NOERR)
    {
        switch (level)
        {
        case 0:
            tmp_html_content += "<h2 style=\"page-break-before: auto;\">" + suffix + " " + QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)) + "</h2>";
            break;
        case 1:
            tmp_html_content += "<h3>" + suffix + " " + QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)) + "</h3>";
            break;
        case 2:
            tmp_html_content += "<h4>" + suffix + " " + QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)) + "</h4>";
            break;
        case 3:
            tmp_html_content += "<h5>" + suffix + " " + QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)) + "</h5>";
            break;
        default:
            tmp_html_content += "<h6>" + suffix + " " + QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)) + "</h6>";
            break;
        }

        if (!is_empty_string(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_DESCRIPTION)))
            tmp_html_content += "<u>"+qApp->tr("Description")+"</u> : " + QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_DESCRIPTION)) + "<br/>";

        tmp_html_content += "<br/>";

        foreach(Feature *tmp_feature, in_feature->getAllChilds())
        {
            tmp_index++;
            tmp_html_content += featureToHtml(in_doc, tmp_feature, suffix + QString::number(tmp_index) + ".", level + 1, images_folder);
        }
    }
    delete tmp_feature_content;

    return tmp_html_content;
}

void writeFeaturesListToExportFile(QFile & in_file, QList<Feature*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QString                 tmp_str;

    QTextDocument           tmp_doc;

    Feature    *tmp_parent = NULL;
    FeatureContent      *tmp_feature_content = NULL;

    qint64                  tmp_bytes_write = 0;

    foreach(Feature *tmp_record, in_records_list)
    {
        tmp_feature_content = new FeatureContent(tmp_record->projectVersion());
        if (tmp_feature_content != NULL)
        {
            if (tmp_feature_content->loadRecord(tmp_record->getValueForKey(FEATURES_HIERARCHY_FEATURE_CONTENT_ID)) == NOERR)
            {
                // Nom de la fonctionnalité
                tmp_str = "";
                tmp_parent = tmp_record;
                while (tmp_parent->parentFeature() != NULL)
                {
                    tmp_parent = tmp_parent->parentFeature();
                    tmp_str = QString(tmp_parent->getValueForKey(FEATURES_HIERARCHY_SHORT_NAME)) + PARENT_HIERARCHY_SEPARATOR + tmp_str;
                }
                tmp_str += QString(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME));

                tmp_str = in_field_enclosing_char + tmp_str.replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
                tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
                if (tmp_bytes_write < 0)    break;

                // Separateur de champs
                tmp_bytes_write = in_file.write(in_field_separator);
                if (tmp_bytes_write < 0)    break;

                // Description de la fonctionnalité
                tmp_doc.setHtml(tmp_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_DESCRIPTION));
                tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
                tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
                if (tmp_bytes_write < 0)    break;

                // Separateur de champs
                tmp_bytes_write = in_file.write(in_field_separator);
                if (tmp_bytes_write < 0)    break;

                // Separateur d'enregistrement
                tmp_bytes_write = in_file.write(in_record_separator);

                // fonctionnalités filles
                writeFeaturesListToExportFile(in_file, tmp_record->getAllChilds(), in_field_separator, in_record_separator, in_field_enclosing_char);
            }

            delete tmp_feature_content;
        }
    }

}

/**
  Exporter des fonctionnalités
**/
void exportFeatures(RecordsTreeView* in_tree_view)
{
    FormDataExport *tmp_export_form = NULL;

    tmp_export_form = new FormDataExport(new ExportContext(in_tree_view->selectedHierachies()), QApplication::activeWindow());
//    connect(tmp_export_form, SIGNAL(startExport(QString,QByteArray,QByteArray, QByteArray)), QApplication::activeWindow(), SLOT(startExportFeatures(QString,QByteArray,QByteArray, QByteArray)));
    tmp_export_form->show();
}


/**
  Inserer une fonctionnalité
**/
void insertFeatureAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(in_tree_view->getRecordsTreeModel()->rowCount(index), 1, index))
            Gui::Services::Global::selectIndex(in_tree_view, in_tree_view->getRecordsTreeModel()->index(in_tree_view->getRecordsTreeModel()->rowCount(index) - 1, 0, index));
    }
}


void addFeatureAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(index.row()+1, 1, index.parent()))
            Gui::Services::Global::selectIndex(in_tree_view, in_tree_view->getRecordsTreeModel()->index(index.row()+1, 0, index.parent()));
    }
}



/**
  Afficher les informations de la fonctionnalité selectionnee
**/
void showFeatureInfos(Feature *in_feature)
{
    Qt::KeyboardModifiers tmp_modifiers = QApplication::keyboardModifiers();

    if (in_feature != NULL && !(tmp_modifiers == Qt::ShiftModifier || tmp_modifiers == Qt::ControlModifier || tmp_modifiers == (Qt::ControlModifier|Qt::ShiftModifier)))
    {
        ProjectView *projectView = Projects::showProjectView(in_feature->projectVersion()->project()->getIdentifier());
        projectView->addProjectWidget<FormFeature, Feature>(in_feature);
    }
}


/**
  Supprimer la liste des fonctionnalités passée en paramètre
**/
void removeFeaturesList(RecordsTreeModel* in_tree_model, const QList<Feature*>& in_features_list)
{
    QModelIndex         tmp_model_index;
    QMessageBox         *tmp_msg_box = NULL;
    QPushButton         *tmp_del_button = NULL;
    QPushButton         *tmp_del_all_button = NULL;
    QPushButton         *tmp_cancel_button = NULL;
    bool                tmp_delete_all = false;
    bool                tmp_delete_current = false;

    if (in_features_list.count() > 0)
    {
        tmp_msg_box = new QMessageBox(QApplication::activeWindow());
        tmp_msg_box->setIcon(QMessageBox::Question);
        tmp_msg_box->setWindowTitle(qApp->tr("Confirmation..."));

        if (in_features_list.count() > 1)
            tmp_del_all_button = tmp_msg_box->addButton(qApp->tr("Supprimer toutes les fonctionnalités"), QMessageBox::NoRole);

        tmp_del_button = tmp_msg_box->addButton(qApp->tr("Supprimer la fonctionnalité"), QMessageBox::YesRole);
        tmp_cancel_button = tmp_msg_box->addButton(qApp->tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->setDefaultButton(tmp_cancel_button);

        foreach(Feature *tmp_feature, in_features_list)
        {
            tmp_delete_current = tmp_delete_all;

            if (tmp_delete_all == false)
            {
                tmp_msg_box->setText(qApp->tr("Etes-vous sûr(e) de vouloir supprimer la fonctionnalité \"%1\" ?").arg(tmp_feature->getValueForKey(FEATURES_HIERARCHY_SHORT_NAME)));
                tmp_msg_box->exec();
                if (tmp_msg_box->clickedButton() == tmp_del_button)
                {
                    tmp_delete_current = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_del_all_button)
                {
                    tmp_delete_current = true;
                    tmp_delete_all = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_cancel_button)
                    break;
            }

            tmp_model_index = in_tree_model->modelIndexForRecord(tmp_feature);
            if (tmp_delete_current && tmp_model_index.isValid())
                in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
        }

        delete tmp_msg_box;
    }
}

ExportContext::ExportContext(QList<Hierarchy*> in_records_list) :
    Gui::Components::ExportContext(in_records_list)
{

}

void ExportContext::startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QFile                   tmp_file;

    tmp_file.setFileName(in_filepath);
    if (tmp_file.open(QIODevice::WriteOnly))
    {
        // Entête
        QString tmp_header;
        QStringList tmp_columns_names = getImportExportFeatureColumnsNames();
        for (int tmp_column_index = 0; tmp_column_index < tmp_columns_names.count() - 1; ++tmp_column_index)
        {
            tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_column_index] + in_field_enclosing_char + in_field_separator;
            tmp_file.write(tmp_header.toStdString().c_str());
        }
        tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_columns_names.count() - 1] + in_field_enclosing_char + in_record_separator;
        tmp_file.write(tmp_header.toStdString().c_str());

        QList<Feature*>    exportList;
        foreach(Hierarchy* hierarchy, m_records_list){
            if (hierarchy->getRecord()){
                if (hierarchy->getRecord()->getEntityDefSignatureId() == FEATURES_HIERARCHY_SIG_ID){
                    exportList.append(dynamic_cast<Feature*>(hierarchy->getRecord()));
                }
                else if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_VERSIONS_TABLE_SIG_ID){
                    exportList.append(ProjectVersion::FeatureRelation::instance().getChilds(dynamic_cast<ProjectVersion*>(hierarchy->getRecord())));
                }
            }
        }

        writeFeaturesListToExportFile(tmp_file, exportList, in_field_separator, in_record_separator, in_field_enclosing_char);

        tmp_file.close();
    }
    else
    {
        QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
    }
}

QStringList getImportExportFeatureColumnsNames()
{
    QStringList tmp_columns_names;

    tmp_columns_names << qApp->tr("Nom de la fonctionnalité") << qApp->tr("Description de la fonctionnalité");

    return tmp_columns_names;
}


ImportContext::ImportContext(RecordsTreeView* in_tree_view) :
    Gui::Components::ImportContext(),
    m_tree_view(in_tree_view)
{
    m_columns_names = getImportExportFeatureColumnsNames();
    m_entity = &feature_entity_import;

    QList<Hierarchy*> tmp_records_list = in_tree_view->selectedHierachies();
    Feature *tmp_feature = NULL;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(in_tree_view->getRecordsTreeModel(), in_tree_view->selectionModel()->currentIndex());

    if (tmp_project_version)
    {
        if (tmp_records_list.isEmpty())
        {
            if (tmp_project_version->featuresHierarchy().isEmpty() == false)
            {
                tmp_feature = tmp_project_version->featuresHierarchy().last();
            }
        }
        else
        {
            tmp_feature = dynamic_cast<Feature*>(tmp_records_list.last()->getRecord());
        }

        if (tmp_feature != NULL)
        {
            QMessageBox::information(MainWindow::instance(), qApp->tr("Information"), qApp->tr("Les fonctionnalités seront importées après la fonctionnalité' \"%1\".").arg(tmp_feature->getValueForKey(FEATURES_HIERARCHY_SHORT_NAME)));

            if (tmp_feature->parentFeature() != NULL)
                m_parent = new Feature(tmp_feature->parentFeature());
            else
                m_parent = new Feature(tmp_project_version);
        }
        else
            m_parent = new Feature(tmp_project_version);
    }
}


void ImportContext::importRecord(Record *in_record, bool in_last_record)
{
    FeatureContent *tmp_feature_content = NULL;
    int tmp_save_result = NOERR;

    Feature *tmp_feature_parent = NULL;
    Feature *tmp_feature = NULL;

    Feature *tmp_existing_feature = NULL;

    QStringList                 tmp_str_list;

    if (m_parent != NULL && in_record != NULL)
    {
        tmp_feature_parent = dynamic_cast<Feature*>(m_parent);

        if (is_empty_string(in_record->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)) == FALSE)
        {
            tmp_str_list = QString(in_record->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME)).split(PARENT_HIERARCHY_SEPARATOR, QString::SkipEmptyParts);

            foreach(QString tmp_str_name, tmp_str_list)
            {
                if (!tmp_str_name.isEmpty())
                {
                    tmp_existing_feature = tmp_feature_parent->getUniqueChildWithValueForKey(tmp_str_name.toStdString().c_str(), FEATURES_CONTENTS_TABLE_SHORT_NAME);
                    if (tmp_existing_feature == NULL)
                    {
                        // Creation de la fonctionnalité importée
                        tmp_feature_content = new FeatureContent(tmp_feature_parent->projectVersion());
                        if (tmp_feature_content != NULL)
                        {
                            tmp_feature_content->setValueForKey(tmp_str_name.toStdString().c_str(), FEATURES_CONTENTS_TABLE_SHORT_NAME);
                            tmp_feature_content->setValueForKey(in_record->getValueForKey(FEATURES_CONTENTS_TABLE_DESCRIPTION), FEATURES_CONTENTS_TABLE_DESCRIPTION);
                            tmp_save_result = tmp_feature_content->saveRecord();
                            if (tmp_save_result == NOERR)
                            {
                                // Ajout de la fonctionnalité à l'arborescence
                                tmp_feature = new Feature(tmp_feature_parent->projectVersion());
                                if (tmp_feature != NULL)
                                {
                                    tmp_feature->setDataFromFeatureContent(tmp_feature_content);

                                    tmp_feature_parent->appendChild(tmp_feature);

                                    tmp_feature_parent = tmp_feature;
                                }
                            }

                            delete tmp_feature_content;
                        }
                    }
                    else
                        tmp_feature_parent = tmp_existing_feature;
                }
            }
        }
    }

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());

        QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));

    }
    else if (in_last_record)
    {
        endImportFeatures();
    }

}


/**
  Importer des fonctionnalités
**/
void importFeatures(RecordsTreeView* in_tree_view)
{
    FormDataImport* tmp_import_form = new FormDataImport(new ImportContext(in_tree_view), MainWindow::instance());

    tmp_import_form->show();
}


void ImportContext::startImport()
{
    FeatureContent *tmp_feature_content = NULL;
    int tmp_save_result = NOERR;

    Feature *tmp_feature_parent = NULL;
    QString detail = qApp->tr("_ A classer (import des fonctionnalités de %1 le %2)")
            .arg(Session::instance().getClientSession()->m_username)
            .arg(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm"));

    tmp_feature_parent = dynamic_cast<Feature*>(m_parent);

    cl_transaction_start(Session::instance().getClientSession());

    tmp_feature_content = new FeatureContent(tmp_feature_parent->projectVersion());


    tmp_feature_content->setValueForKey(detail.toStdString().c_str(), FEATURES_CONTENTS_TABLE_SHORT_NAME);
    tmp_feature_content->setValueForKey(detail.toStdString().c_str(), FEATURES_CONTENTS_TABLE_DESCRIPTION);
    tmp_save_result = tmp_feature_content->saveRecord();
    if (tmp_save_result == NOERR)
    {
        tmp_feature_parent->setDataFromFeatureContent(tmp_feature_content);
        tmp_save_result = tmp_feature_parent->saveRecord();
    }
    delete tmp_feature_content;

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


void ImportContext::endImportFeatures()
{
    QList<Hierarchy*> tmp_records_list = m_tree_view->selectedHierachies();
    Feature *tmp_feature = NULL;
    QModelIndex tmp_model_index;
    int tmp_index = 0;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(m_tree_view->getRecordsTreeModel(), m_tree_view->selectionModel()->currentIndex());

    int tmp_tr_result = NOERR;

    Feature *tmp_feature_parent = NULL;

    tmp_feature_parent = dynamic_cast<Feature*>(m_parent);

    if (tmp_records_list.isEmpty())
    {
        if (tmp_project_version->featuresHierarchy().isEmpty() == false)
        {
            tmp_feature = tmp_project_version->featuresHierarchy().last();
        }
    }
    else
    {
        tmp_feature = dynamic_cast<Feature*>(tmp_records_list.last()->getRecord());
    }

    if (tmp_feature != NULL)
    {
        tmp_model_index = m_tree_view->getRecordsTreeModel()->modelIndexForRecord(tmp_feature);
        if (tmp_model_index.isValid())
        {
            tmp_index = tmp_model_index.row() + 1;
            tmp_model_index = tmp_model_index.parent();
        }
    }
    else
    {
        tmp_index = tmp_project_version->featuresHierarchy().count();
        tmp_model_index  = m_tree_view->currentIndex();
    }

    if (m_tree_view->getRecordsTreeModel()->insertItem(tmp_index, tmp_model_index, tmp_feature_parent))
    {
        tmp_tr_result = cl_transaction_commit(Session::instance().getClientSession());
        if (tmp_tr_result != NOERR)
            QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_tr_result));
    }
    else
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}



}
}
}
