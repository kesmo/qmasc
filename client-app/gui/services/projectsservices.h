#ifndef PROJECTSSERVICES_H
#define PROJECTSSERVICES_H

#include "entities/projectversion.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/RecordsTreeModel.h"
#include "gui/ProjectView.h"

namespace Gui
{
namespace Services
{
namespace Projects
{
    ProjectView *showProjectView(const QString &projectId, const QString &projectVersionId = QString());
    void showProjectInfos(Project* in_project);
    void showProjectVersionInfos(ProjectVersion* in_project_version);
    void showProjectsStats();
    void showExecutionsStats(Project *in_project);
    void showProjectVersionBugs(ProjectVersion *in_project_version);
    void importProject();
    void exportProject(Project *in_project);
    void removeProject(RecordsTreeModel *in_tree_model, Project *in_project);
    void removeProjectVersion(RecordsTreeModel *in_tree_model, ProjectVersion *in_project_version);
    ProjectVersion* projectVersionForIndex(RecordsTreeModel* in_tree_model, QModelIndex modelIndex);
    Project* projectForIndex(RecordsTreeModel* in_tree_model, QModelIndex modelIndex);
    void setDefaultProjectVersion(ProjectVersion *in_project_version);
    void askUserForSetDefaultProjectVersion(ProjectVersion* projectVersion);
}
}
}

#endif // PROJECTSSERVICES_H
