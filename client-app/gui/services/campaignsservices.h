#ifndef CAMPAIGNSSERVICES_H
#define CAMPAIGNSSERVICES_H

#include "entities/campaign.h"

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/TestActionAttachmentsManager.h"

#include <QTextDocument>
#include <QFileInfo>
#include <QModelIndex>
#include <QTreeView>

namespace Gui
{
namespace Services
{
namespace Campaigns
{
    void showCampaignInfos(Campaign *in_campaign);
    void insertCampaign(RecordsTreeView *in_tree_view, const QModelIndex &index, Campaign *in_campaign);
    void removeCampaign(RecordsTreeView* in_tree_view, const QModelIndex& index);
}
}
}

#endif // CAMPAIGNSSERVICES_H
