#ifndef SERVICES_H
#define SERVICES_H

#include <QtPrintSupport/QPrinter>
#include <QTreeView>

#include "gui/components/RecordsTreeView.h"

namespace Gui
{
namespace Services
{
namespace Global
{

    QString & formatFilename(QString & filename);

    void showRecordInfos(RecordsTreeView *in_tree_view, Record *in_record);
    void selectIndex(RecordsTreeView* in_tree_view, const QModelIndex& modelIndex);

    QPrinter* print();
}
}
}

#endif // SERVICES_H
