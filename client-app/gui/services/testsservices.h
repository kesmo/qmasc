#ifndef TESTSSERVICES_H
#define TESTSSERVICES_H

#include "entities/test.h"
#include "entities/action.h"
#include "entities/automatedaction.h"

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/TestActionAttachmentsManager.h"

#include "gui/components/hierarchies/projecttestshierarchy.h"

#include "gui/components/importexportcontext.h"

#include <QTextDocument>
#include <QFileInfo>
#include <QModelIndex>
#include <QTreeView>
#include <QFileDialog>
#include <QApplication>
#include <QMessageBox>
#include <QtPrintSupport/QPrinter>

namespace Gui
{
namespace Services
{
namespace Tests
{
void saveTestsAsHtml(const QList<Test *> &tests_list);
QTextDocument* generateTestsDocument(TestActionAttachmentsManager *in_attachments_manager, QList<Test*> in_tests_list, bool include_image = true, QFileInfo file_info = QFileInfo());
QString testToHtml(TestActionAttachmentsManager *in_attachments_manager, QTextDocument *in_doc, Test *in_test, QString suffix, int level, QString images_folder);
void printTests(const QList<Test *> &tests_list);
void showTestInfos(RecordsTreeView* in_tree_view, QModelIndex in_model_index = QModelIndex(), bool in_update_history = true);
void showTestInfos(Test *in_test, bool in_update_history = true);
void insertTestAtIndex(RecordsTreeView *in_tree_view, QModelIndex parentIndex, int row, bool in_automated_test = false);
void insertChildTestAtIndex(RecordsTreeView *in_tree_view, QModelIndex index, bool in_automated_test = false);

void showOriginalTestInfos(RecordsTreeView* in_tree_view, Test *in_test_hierarchy);
void showTestWithContentId(RecordsTreeView* in_tree_view, ProjectVersion *in_project_version, const char *in_test_content_id);
void testSaved(QTreeView* in_tree_view, RecordsTreeModel* in_tree_model, Test *in_test);
void removeTestsList(RecordsTreeModel *in_tree_model, const QList<Test *> &in_records_list);

void importTests(RecordsTreeView* in_tree_view);

void selectLinkTests(RecordsTreeView* in_tree_view, const QList<Test *> &in_records_list);
QModelIndexList dependantsTestsIndexesFromRequirementsList(RecordsTreeView *in_tree_view, const QList<Requirement *> &in_records_list);
QStringList getImportExportTestColumnsNames();

void exportTests(RecordsTreeView* in_tree_view);
void writeTestsListToExportFile(QFile & in_file, QList<Test*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
void writeTestToExportFile(QFile & in_file, Test *in_test, TestContent *in_test_content, Action *in_action, AutomatedAction *in_automated_action, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
void startExportTests(QList<Hierarchy*> in_records_list, QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);


class ImportContext :
        public Gui::Components::ImportContext
{
public:
    ImportContext(RecordsTreeView* in_tree_view);

    void startImport();
    void importRecord(Record *out_record, bool in_last_record);

    void endImportTests();

private:
    Test* m_last_imported_test;

    RecordsTreeView* m_tree_view;
};


class ExportContext :
        public Gui::Components::ExportContext
{
public:
    ExportContext(QList<Hierarchy*> in_records_list);

    virtual void startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);


};

}
}
}

#endif // TESTSSERVICES_H
