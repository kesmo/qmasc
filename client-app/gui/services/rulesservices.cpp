#include "rulesservices.h"

#include "services.h"

#include "session.h"

#include "gui/forms/FormRule.h"
#include "gui/forms/FormDataExport.h"
#include "gui/forms/FormDataImport.h"

#include "gui/mainwindow.h"

#include "projectsservices.h"

#include <QFileDialog>
#include <QApplication>
#include <QMessageBox>
#include <QMdiSubWindow>
#include <QtPrintSupport/QPrinter>

namespace Gui
{
namespace Services
{
namespace Rules
{
static const char *RULES_COLUMNS_IMPORT[] = {RULES_CONTENTS_TABLE_SHORT_NAME, RULES_CONTENTS_TABLE_DESCRIPTION};
static const entity_def rule_entity_import = {0, 0, 0, 0, RULES_COLUMNS_IMPORT, 0, 0, sizeof(RULES_COLUMNS_IMPORT)/sizeof(char*)};


void saveRulesAsHtml(const QList<Rule*>& rules_list)
{
    if (!rules_list.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(QApplication::activeWindow(), "Export html", QString(), "*.html");
        if (!fileName.isEmpty())
        {
            QFile tmp_file(fileName);
            if (tmp_file.open(QIODevice::WriteOnly))
            {
                TestActionAttachmentsManager manager;
                tmp_file.write(generateRulesDocument(&manager, rules_list, false, QFileInfo(tmp_file))->toHtml("utf-8").toLatin1());
                tmp_file.close();
            }
            else
            {
                QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
            }
        }
    }
}

QTextDocument* generateRulesDocument(TestActionAttachmentsManager* /*in_attachments_manager*/, QList<Rule*> in_rules_list, bool include_image, QFileInfo file_info)
{
    QString tmp_doc_content = QString("<html><head></head><body>");
    QTextDocument               *tmp_doc = new QTextDocument();
    int tmp_index = 0;

    QString         tmp_images_folder_absolute_path;
    QString         tmp_images_folder_name;

    if (!include_image)
    {
        if (file_info.isFile())
        {
            tmp_images_folder_absolute_path = file_info.canonicalFilePath()+"_images";
            QDir tmp_images_dir(tmp_images_folder_absolute_path);
            tmp_images_folder_name = tmp_images_dir.dirName();
            if (!tmp_images_dir.exists())
                file_info.absoluteDir().mkdir(tmp_images_folder_name);
        }
    }

    tmp_index = 0;
    foreach(Rule *tmp_rule, in_rules_list)
    {
        tmp_index++;
        tmp_doc_content += ruleToHtml(tmp_doc, tmp_rule, QString::number(tmp_index) + ".", 0, tmp_images_folder_absolute_path);
    }

    tmp_doc_content += "</body></html>";

    tmp_doc->setHtml(tmp_doc_content);


    return tmp_doc;
}

void printRules(const QList<Rule*>& rules_list)
{
    if (!rules_list.isEmpty())
    {
        QPrinter *printer = Gui::Services::Global::print();
        if (printer)
        {
            if (!printer->outputFileName().isEmpty())
            {
                TestActionAttachmentsManager manager;
                generateRulesDocument(&manager, rules_list)->print(printer);
            }
            delete printer;
        }
    }
}


QString ruleToHtml(QTextDocument *in_doc, Rule *in_rule, QString suffix, int level, QString images_folder)
{
    QString tmp_html_content = QString();
    int tmp_index = 0;
    RuleContent *tmp_rule_content = RuleContent::getEntity(in_rule->getValueForKey(RULES_TABLE_RULE_CONTENT_ID));

    if (tmp_rule_content)
    {
        switch (level)
        {
        case 0:
            tmp_html_content += "<h2 style=\"page-break-before: auto;\">" + suffix + " " + QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)) + "</h2>";
            break;
        case 1:
            tmp_html_content += "<h3>" + suffix + " " + QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)) + "</h3>";
            break;
        case 2:
            tmp_html_content += "<h4>" + suffix + " " + QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)) + "</h4>";
            break;
        case 3:
            tmp_html_content += "<h5>" + suffix + " " + QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)) + "</h5>";
            break;
        default:
            tmp_html_content += "<h6>" + suffix + " " + QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)) + "</h6>";
            break;
        }

        if (!is_empty_string(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_DESCRIPTION)))
            tmp_html_content += "<u>"+qApp->tr("Description")+"</u> : " + QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_DESCRIPTION)) + "<br/>";

        tmp_html_content += "<br/>";

        foreach(Rule *tmp_rule, in_rule->getAllChilds())
        {
            tmp_index++;
            tmp_html_content += ruleToHtml(in_doc, tmp_rule, suffix + QString::number(tmp_index) + ".", level + 1, images_folder);
        }
    }
    delete tmp_rule_content;

    return tmp_html_content;
}

void writeRulesListToExportFile(QFile & in_file, QList<Rule*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QString                 tmp_str;

    QTextDocument           tmp_doc;

    Rule    *tmp_parent = NULL;
    RuleContent      *tmp_rule_content = NULL;

    qint64                  tmp_bytes_write = 0;

    foreach(Rule *tmp_record, in_records_list)
    {
        tmp_rule_content = RuleContent::getEntity(tmp_record->getValueForKey(RULES_TABLE_RULE_CONTENT_ID));
        if (tmp_rule_content != NULL)
        {
            // Nom de la règle de gestion
            tmp_str = "";
            tmp_parent = tmp_record;
            while (tmp_parent->parentRule() != NULL)
            {
                tmp_parent = tmp_parent->parentRule();
                tmp_str = QString(tmp_parent->getValueForKey(RULES_HIERARCHY_SHORT_NAME)) + PARENT_HIERARCHY_SEPARATOR + tmp_str;
            }
            tmp_str += QString(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME));

            tmp_str = in_field_enclosing_char + tmp_str.replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
            tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
            if (tmp_bytes_write < 0)    break;

            // Separateur de champs
            tmp_bytes_write = in_file.write(in_field_separator);
            if (tmp_bytes_write < 0)    break;

            // Description de la règle de gestion
            tmp_doc.setHtml(tmp_rule_content->getValueForKey(RULES_CONTENTS_TABLE_DESCRIPTION));
            tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
            tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
            if (tmp_bytes_write < 0)    break;

            // Separateur de champs
            tmp_bytes_write = in_file.write(in_field_separator);
            if (tmp_bytes_write < 0)    break;

            // Separateur d'enregistrement
            tmp_bytes_write = in_file.write(in_record_separator);

            // règles de gestion filles
            writeRulesListToExportFile(in_file, tmp_record->getAllChilds(), in_field_separator, in_record_separator, in_field_enclosing_char);

            delete tmp_rule_content;
        }
    }

}

/**
  Exporter des règles de gestion
**/
void exportRules(RecordsTreeView* in_tree_view)
{
    FormDataExport *tmp_export_form = NULL;

    tmp_export_form = new FormDataExport(new ExportContext(in_tree_view->selectedHierachies()), QApplication::activeWindow());
//    connect(tmp_export_form, SIGNAL(startExport(QString,QByteArray,QByteArray, QByteArray)), QApplication::activeWindow(), SLOT(startExportRules(QString,QByteArray,QByteArray, QByteArray)));
    tmp_export_form->show();
}


/**
  Inserer une règle de gestion
**/
void insertRuleAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(in_tree_view->getRecordsTreeModel()->rowCount(index), 1, index))
            Gui::Services::Global::selectIndex(in_tree_view, in_tree_view->getRecordsTreeModel()->index(in_tree_view->getRecordsTreeModel()->rowCount(index) - 1, 0, index));
    }
}


void addRuleAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(index.row()+1, 1, index.parent()))
            Gui::Services::Global::selectIndex(in_tree_view, in_tree_view->getRecordsTreeModel()->index(index.row()+1, 0, index.parent()));
    }
}



/**
  Afficher les informations de la règle de gestion selectionnee
**/
void showRuleInfos(Rule *in_rule)
{
    Qt::KeyboardModifiers tmp_modifiers = QApplication::keyboardModifiers();

    if (in_rule != NULL && !(tmp_modifiers == Qt::ShiftModifier || tmp_modifiers == Qt::ControlModifier || tmp_modifiers == (Qt::ControlModifier|Qt::ShiftModifier)))
    {
        ProjectView *projectView = Projects::showProjectView(in_rule->projectVersion()->project()->getIdentifier());
        projectView->addProjectWidget<FormRule, Rule>(in_rule);
    }
}


/**
  Supprimer la liste des règles de gestion passée en paramètre
**/
void removeRulesList(RecordsTreeModel* in_tree_model, const QList<Rule*>& in_records_list)
{
    QModelIndex         tmp_model_index;
    QMessageBox         *tmp_msg_box = NULL;
    QPushButton         *tmp_del_button = NULL;
    QPushButton         *tmp_del_all_button = NULL;
    QPushButton         *tmp_cancel_button = NULL;
    bool                tmp_delete_all = false;
    bool                tmp_delete_current = false;

    if (in_records_list.count() > 0)
    {
        tmp_msg_box = new QMessageBox(QApplication::activeWindow());
        tmp_msg_box->setIcon(QMessageBox::Question);
        tmp_msg_box->setWindowTitle(qApp->tr("Confirmation..."));

        if (in_records_list.count() > 1)
            tmp_del_all_button = tmp_msg_box->addButton(qApp->tr("Supprimer tous les règles de gestion"), QMessageBox::NoRole);

        tmp_del_button = tmp_msg_box->addButton(qApp->tr("Supprimer la règle de gestion"), QMessageBox::YesRole);
        tmp_cancel_button = tmp_msg_box->addButton(qApp->tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->setDefaultButton(tmp_cancel_button);

        foreach(Rule *tmp_rule, in_records_list)
        {
            tmp_delete_current = tmp_delete_all;

            if (tmp_delete_all == false)
            {
                tmp_msg_box->setText(qApp->tr("Etes-vous sûr(e) de vouloir supprimer la règle de gestion \"%1\" ?").arg(tmp_rule->getValueForKey(RULES_HIERARCHY_SHORT_NAME)));
                tmp_msg_box->exec();
                if (tmp_msg_box->clickedButton() == tmp_del_button)
                {
                    tmp_delete_current = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_del_all_button)
                {
                    tmp_delete_current = true;
                    tmp_delete_all = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_cancel_button)
                    break;
            }

            tmp_model_index = in_tree_model->modelIndexForRecord(tmp_rule);
            if (tmp_delete_current && tmp_model_index.isValid())
                in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
        }

        delete tmp_msg_box;
    }
}

ExportContext::ExportContext(QList<Hierarchy*> in_records_list) :
    Gui::Components::ExportContext(in_records_list)
{

}

void ExportContext::startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QFile                   tmp_file;

    tmp_file.setFileName(in_filepath);
    if (tmp_file.open(QIODevice::WriteOnly))
    {
        // Entête
        QString tmp_header;
        QStringList tmp_columns_names = getImportExportRuleColumnsNames();
        for (int tmp_column_index = 0; tmp_column_index < tmp_columns_names.count() - 1; ++tmp_column_index)
        {
            tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_column_index] + in_field_enclosing_char + in_field_separator;
            tmp_file.write(tmp_header.toStdString().c_str());
        }
        tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_columns_names.count() - 1] + in_field_enclosing_char + in_record_separator;
        tmp_file.write(tmp_header.toStdString().c_str());

        QList<Rule*>    exportList;
        foreach(Hierarchy* hierarchy, m_records_list){
            if (hierarchy->getRecord()){
                if (hierarchy->getRecord()->getEntityDefSignatureId() == RULES_HIERARCHY_SIG_ID){
                    exportList.append(dynamic_cast<Rule*>(hierarchy->getRecord()));
                }
                else if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_VERSIONS_TABLE_SIG_ID){
                    exportList.append(ProjectVersion::RuleRelation::instance().getChilds(dynamic_cast<ProjectVersion*>(hierarchy->getRecord())));
                }
            }
        }

        writeRulesListToExportFile(tmp_file, exportList, in_field_separator, in_record_separator, in_field_enclosing_char);

        tmp_file.close();
    }
    else
    {
        QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
    }
}

QStringList getImportExportRuleColumnsNames()
{
    QStringList tmp_columns_names;

    tmp_columns_names << qApp->tr("Nom de la règle de gestion") << qApp->tr("Description de la règle de gestion");

    return tmp_columns_names;
}


ImportContext::ImportContext(RecordsTreeView* in_tree_view) :
    Gui::Components::ImportContext(),
    m_tree_view(in_tree_view)
{
    m_columns_names = getImportExportRuleColumnsNames();
    m_entity = &rule_entity_import;

    QList<Hierarchy*> tmp_records_list = in_tree_view->selectedHierachies();
    Rule *tmp_rule = NULL;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(in_tree_view->getRecordsTreeModel(), in_tree_view->selectionModel()->currentIndex());

    if (tmp_project_version)
    {
        if (tmp_records_list.isEmpty())
        {
            if (tmp_project_version->rulesHierarchy().isEmpty() == false)
            {
                tmp_rule = tmp_project_version->rulesHierarchy().last();
            }
        }
        else
        {
            tmp_rule = dynamic_cast<Rule*>(tmp_records_list.last()->getRecord());
        }

        if (tmp_rule != NULL)
        {
            QMessageBox::information(MainWindow::instance(), qApp->tr("Information"), qApp->tr("Les règles de gestion seront importées après la règle de gestion' \"%1\".").arg(tmp_rule->getValueForKey(RULES_HIERARCHY_SHORT_NAME)));

            if (tmp_rule->parentRule() != NULL)
                m_parent = new Rule(tmp_rule->parentRule());
            else
                m_parent = new Rule(tmp_project_version);
        }
        else
            m_parent = new Rule(tmp_project_version);
    }
}


void ImportContext::importRecord(Record *in_record, bool in_last_record)
{
    RuleContent *tmp_rule_content = NULL;
    int tmp_save_result = NOERR;

    Rule *tmp_rule_parent = NULL;
    Rule *tmp_rule = NULL;

    Rule *tmp_existing_rule = NULL;

    QStringList                 tmp_str_list;

    if (m_parent != NULL && in_record != NULL)
    {
        tmp_rule_parent = dynamic_cast<Rule*>(m_parent);

        if (is_empty_string(in_record->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)) == FALSE)
        {
            tmp_str_list = QString(in_record->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME)).split(PARENT_HIERARCHY_SEPARATOR, QString::SkipEmptyParts);

            foreach(QString tmp_str_name, tmp_str_list)
            {
                if (!tmp_str_name.isEmpty())
                {
                    tmp_existing_rule = tmp_rule_parent->getUniqueChildWithValueForKey(tmp_str_name.toStdString().c_str(), RULES_HIERARCHY_SHORT_NAME);
                    if (tmp_existing_rule == NULL)
                    {
                        // Creation de la règle de gestion importée
                        tmp_rule_content = new RuleContent();
                        if (tmp_rule_content != NULL)
                        {
                            ProjectVersion::RuleContentRelation::instance().setParent(tmp_rule_parent->projectVersion(), tmp_rule_content);
                            tmp_rule_content->setValueForKey(tmp_str_name.toStdString().c_str(), RULES_CONTENTS_TABLE_SHORT_NAME);
                            tmp_rule_content->setValueForKey(in_record->getValueForKey(RULES_CONTENTS_TABLE_DESCRIPTION), RULES_CONTENTS_TABLE_DESCRIPTION);
                            tmp_save_result = tmp_rule_content->saveRecord();
                            if (tmp_save_result == NOERR)
                            {
                                // Ajout de la règle de gestion à l'arborescence
                                tmp_rule = new Rule(tmp_rule_parent->projectVersion());
                                if (tmp_rule != NULL)
                                {
                                    tmp_rule->setDataFromRuleContent(tmp_rule_content);

                                    tmp_rule_parent->appendChild(tmp_rule);

                                    tmp_rule_parent = tmp_rule;
                                }
                            }

                            delete tmp_rule_content;
                        }
                    }
                    else
                        tmp_rule_parent = tmp_existing_rule;
                }
            }
        }
    }

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());

        QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));

    }
    else if (in_last_record)
    {
        endImportRules();
    }

}


/**
  Importer des règles de gestion
**/
void importRules(RecordsTreeView* in_tree_view)
{
    FormDataImport* tmp_import_form = new FormDataImport(new ImportContext(in_tree_view), MainWindow::instance());

    tmp_import_form->show();
}


void ImportContext::startImport()
{
    RuleContent *tmp_rule_content = NULL;
    int tmp_save_result = NOERR;
    Rule *tmp_rule_parent = NULL;

    QString detail = qApp->tr("_ A classer (import des règles de gestion de %1 le %2)")
            .arg(Session::instance().getClientSession()->m_username)
            .arg(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm"));

    tmp_rule_parent = dynamic_cast<Rule*>(m_parent);

    cl_transaction_start(Session::instance().getClientSession());

    tmp_rule_content = new RuleContent();
    ProjectVersion::RuleContentRelation::instance().setParent(tmp_rule_parent->projectVersion(), tmp_rule_content);

    tmp_rule_content->setValueForKey(detail.toStdString().c_str(), RULES_CONTENTS_TABLE_SHORT_NAME);
    tmp_rule_content->setValueForKey(detail.toStdString().c_str(), RULES_CONTENTS_TABLE_DESCRIPTION);
    tmp_save_result = tmp_rule_content->saveRecord();
    if (tmp_save_result == NOERR)
    {
        tmp_rule_parent->setDataFromRuleContent(tmp_rule_content);
        tmp_save_result = tmp_rule_parent->saveRecord();
    }
    delete tmp_rule_content;

    if (tmp_save_result != NOERR) {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


void ImportContext::endImportRules()
{
    QList<Hierarchy*> tmp_records_list = m_tree_view->selectedHierachies();
    Rule *tmp_rule = NULL;
    QModelIndex tmp_model_index;
    int tmp_index = 0;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(m_tree_view->getRecordsTreeModel(), m_tree_view->selectionModel()->currentIndex());

    int tmp_tr_result = NOERR;

    Rule *tmp_rule_parent = NULL;

    tmp_rule_parent = dynamic_cast<Rule*>(m_parent);

    if (tmp_records_list.isEmpty())
    {
        if (tmp_project_version->rulesHierarchy().isEmpty() == false)
        {
            tmp_rule = tmp_project_version->rulesHierarchy().last();
        }
    }
    else
    {
        tmp_rule = dynamic_cast<Rule*>(tmp_records_list.last()->getRecord());
    }

    if (tmp_rule != NULL)
    {
        tmp_model_index = m_tree_view->getRecordsTreeModel()->modelIndexForRecord(tmp_rule);
        if (tmp_model_index.isValid())
        {
            tmp_index = tmp_model_index.row() + 1;
            tmp_model_index = tmp_model_index.parent();
        }
    }
    else
    {
        tmp_index = tmp_project_version->rulesHierarchy().count();
        tmp_model_index  = m_tree_view->currentIndex();
    }

    if (m_tree_view->getRecordsTreeModel()->insertItem(tmp_index, tmp_model_index, tmp_rule_parent))
    {
        tmp_tr_result = cl_transaction_commit(Session::instance().getClientSession());
        if (tmp_tr_result != NOERR)
            QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_tr_result));
    }
    else
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}



}
}
}
