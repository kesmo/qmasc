#ifndef HIERARCHYCONTEXTMENUSSERVICES_H
#define HIERARCHYCONTEXTMENUSSERVICES_H

#include "gui/components/RecordsTreeView.h"

#include <QMenu>
#include <QModelIndexList>

namespace Gui
{
namespace Services
{
namespace HierarchyContextMenus
{

    bool contextMenuForRecordsTreeView(RecordsTreeView* in_recordstreeview, const QPoint &position);

}
}
}
#endif // HIERARCHYCONTEXTMENUSSERVICES_H
