#ifndef REQUIREMENTSSERVICES_H
#define REQUIREMENTSSERVICES_H

#include "entities/requirement.h"
#include "entities/requirementcontent.h"
#include "gui/components/hierarchies/projectrequirementshierarchy.h"

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/TestActionAttachmentsManager.h"
#include "gui/components/importexportcontext.h"

#include <QTextDocument>
#include <QFileInfo>
#include <QModelIndex>
#include <QTreeView>

namespace Gui
{
namespace Services
{
namespace Requirements
{
    void saveRequirementsAsHtml(QList<Requirement *> requirements_list);
    QTextDocument* generateRequirementsDocument(TestActionAttachmentsManager* in_attachments_manager, QList<Requirement*> in_requirements_list, bool include_image = true, QFileInfo file_info = QFileInfo());
    void printRequirements(QList<Requirement *> requirements_list);
    QString requirementToHtml(QTextDocument *in_doc, Requirement *in_requirement, QString suffix, int level, QString images_folder);
    void writeRequirementsListToExportFile(QFile & in_file, QList<Requirement*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    void exportRequirements(RecordsTreeView *in_tree_view);
    void showRequirementInfos(RecordsTreeView* in_tree_view, QModelIndex in_model_index = QModelIndex(), bool in_update_history = true);
    void addRequirementAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void insertRequirementAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void showRequirementInfos(Requirement *in_requirement);
    void showRequirementWithOriginalContentId(ProjectVersion *in_project_version, RecordsTreeView* in_tree_view, RecordsTreeModel* in_tree_model, const char *in_original_requirement_content_id);
    void removeRequirementsList(RecordsTreeModel *in_tree_model, const QList<Requirement *> &in_records_list);
    void startExportRequirements(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    QStringList getImportExportRequirementColumnsNames();
    void importRequirements(RecordsTreeView *in_tree_view);

    class ImportContext :
            public Gui::Components::ImportContext
    {
    public:
        ImportContext(RecordsTreeView* in_tree_view);

        void startImport();
        void importRecord(Record *out_record, bool in_last_record);

        void endImportRequirements();

    private:

        RecordsTreeView* m_tree_view;
    };


    class ExportContext :
            public Gui::Components::ExportContext
    {
    public:
        ExportContext(QList<Hierarchy*> in_records_list);

        virtual void startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);


    };

}
}
}
#endif // REQUIREMENTSSERVICES_H
