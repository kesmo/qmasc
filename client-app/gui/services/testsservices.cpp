#include "testsservices.h"

#include "entities/testcontent.h"
#include "entities/testtype.h"
#include "entities/requirementcategory.h"
#include "session.h"
#include "services.h"
#include "projectsservices.h"

#include "gui/mainwindow.h"
#include "gui/forms/FormTest.h"

#include "gui/forms/FormDataImport.h"
#include "gui/forms/FormDataExport.h"

#include <QMdiSubWindow>

namespace Gui
{
namespace Services
{
namespace Tests
{
#define ACTION_IMPORT_DESCRIPTION    "action_description"
#define ACTION_IMPORT_WAIT_RESULT    "action_result"

static const char *TEST_COLUMNS_IMPORT[] = { TESTS_CONTENTS_TABLE_SHORT_NAME,
                                            TESTS_CONTENTS_TABLE_DESCRIPTION,
                                            TESTS_CONTENTS_TABLE_CATEGORY_ID,
                                            TESTS_CONTENTS_TABLE_PRIORITY_LEVEL,
                                            ACTION_IMPORT_DESCRIPTION,
                                            ACTION_IMPORT_WAIT_RESULT,
                                            TESTS_CONTENTS_TABLE_STATUS,
                                            TESTS_CONTENTS_TABLE_AUTOMATED,
                                            TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND,
                                            TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS,
                                            TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE,
                                            TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE,
                                            TESTS_CONTENTS_TABLE_TYPE,
                                            TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE,
                                            AUTOMATED_ACTIONS_TABLE_WINDOW_ID,
                                            AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE,
                                            AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA,
                                            AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY };
static const entity_def test_entity_import = { 0, 0, 0, 0, TEST_COLUMNS_IMPORT, 0, 0, sizeof(TEST_COLUMNS_IMPORT) / sizeof(char*) };


void saveTestsAsHtml(const QList<Test*>& tests_list)
{
    if (!tests_list.isEmpty()) {
        QString fileName = QFileDialog::getSaveFileName(QApplication::activeWindow(), "Export html", QString(), "*.html");
        if (!fileName.isEmpty()) {
            QFile tmp_file(fileName);
            if (tmp_file.open(QIODevice::WriteOnly)) {
                TestActionAttachmentsManager manager;
                tmp_file.write(generateTestsDocument(&manager, tests_list, false, QFileInfo(tmp_file))->toHtml("utf-8").toLatin1());
                tmp_file.close();
            }
            else {
                QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
            }
        }
    }
}

QTextDocument* generateTestsDocument(TestActionAttachmentsManager* in_attachments_manager, QList<Test*> in_tests_list, bool include_image, QFileInfo file_info)
{
    QString            tmp_doc_content = QString("<html><head></head><body>");
    QTextDocument               *tmp_doc = new QTextDocument();
    int                tmp_index = 0;

    QString         tmp_images_folder_absolute_path;
    QString         tmp_images_folder_name;

    if (!include_image) {
        if (file_info.isFile()) {
            tmp_images_folder_absolute_path = file_info.canonicalFilePath() + "_images";
            QDir tmp_images_dir(tmp_images_folder_absolute_path);
            tmp_images_folder_name = tmp_images_dir.dirName();
            if (!tmp_images_dir.exists())
                file_info.absoluteDir().mkdir(tmp_images_folder_name);
        }
    }

    tmp_index = 0;
    foreach(Test *tmp_test, in_tests_list)
    {
        tmp_index++;
        tmp_doc_content += testToHtml(in_attachments_manager, tmp_doc, tmp_test, QString::number(tmp_index) + ".", 0, tmp_images_folder_absolute_path);
    }

    tmp_doc_content += "</body></html>";

    tmp_doc->setHtml(tmp_doc_content);


    return tmp_doc;
}

void printTests(const QList<Test*>& tests_list)
{
    if (!tests_list.isEmpty()) {
        QPrinter *printer = Gui::Services::Global::print();
        if (printer) {
            if (!printer->outputFileName().isEmpty()) {
                TestActionAttachmentsManager manager;
                generateTestsDocument(&manager, tests_list)->print(printer);
            }
            delete printer;
        }
    }
}

QString testToHtml(TestActionAttachmentsManager* in_attachments_manager, QTextDocument *in_doc, Test *in_test, QString suffix, int level, QString images_folder)
{
    QString        tmp_html_content = QString();
    QString        tmp_html_action_content;
    int            tmp_index = 0;
    TestContent        *tmp_test_content = TestContent::getEntity(in_test->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID));
    QList<Action*> tmp_actions;
    QList<Action*> tmp_all_actions;

    if (tmp_test_content) {
        const QList<TestContentFile*> &tmp_files = TestContent::TestContentFileRelation::instance().getChilds(tmp_test_content);

        tmp_actions = TestContent::ActionRelation::instance().getChilds(tmp_test_content);
        foreach(Action *tmp_action, tmp_actions)
        {
            tmp_action->loadAssociatedActionsForVersion(in_test->getValueForKey(TESTS_HIERARCHY_VERSION), in_test->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID));
            if (tmp_action->associatedTestActions().count() > 0) {
                foreach(Action *tmp_associated_action, tmp_action->associatedTestActions())
                {
                    tmp_all_actions.append(tmp_associated_action);
                }
            }
            else {
                tmp_all_actions.append(tmp_action);
            }
        }

        switch (level) {
        case 0:
            tmp_html_content += "<h2 style=\"page-break-before: auto;\">" + suffix + " " + QString(in_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) + "</h2>";
            break;
        case 1:
            tmp_html_content += "<h3>" + suffix + " " + QString(in_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) + "</h3>";
            break;
        case 2:
            tmp_html_content += "<h4>" + suffix + " " + QString(in_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) + "</h4>";
            break;
        case 3:
            tmp_html_content += "<h5>" + suffix + " " + QString(in_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) + "</h5>";
            break;
        default:
            tmp_html_content += "<h6>" + suffix + " " + QString(in_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) + "</h6>";
            break;
        }

        if (!is_empty_string(tmp_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION)))
            tmp_html_content += "<u>" + qApp->tr("Description") + "</u> : " + QString(tmp_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION)) + "<br/>";

        const char *test_type_label = Record::matchingValueInRecordsList(Session::instance().testsTypes(),
                                                                         TESTS_TYPES_TABLE_TEST_TYPE_ID,
                                                                         tmp_test_content->getValueForKey(TESTS_CONTENTS_TABLE_TYPE),
                                                                         TESTS_TYPES_TABLE_TEST_TYPE_LABEL);

        const char *category_label = Record::matchingValueInRecordsList(Session::instance().requirementsCategories(),
                                                                        REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID,
                                                                        tmp_test_content->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID),
                                                                        REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL);

        if (!is_empty_string(test_type_label))
            tmp_html_content += "<u>" + qApp->tr("Type") + "</u> : " + QString(test_type_label) + "<br/>";

        if (!is_empty_string(category_label))
            tmp_html_content += "<u>" + qApp->tr("Categorie") + "</u> : " + QString(category_label) + "<br/>";

        if (compare_values(tmp_test_content->getValueForKey(TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE), YES) == 0)
            tmp_html_content += "<u>" + qApp->tr("Test aux limites") + "</u> : Oui<br/>";
        else
            tmp_html_content += "<u>" + qApp->tr("Test aux limites") + "</u> : Non<br/>";


        tmp_html_content += "<u>" + qApp->tr("Priorité") + "</u> : " + tmp_test_content->getPriorityLabel() + "<br/>";

        if (tmp_all_actions.count() > 0) {
            tmp_html_content += "<u>" + qApp->tr("Liste des actions") + "</u><table border=\"1\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">";
            tmp_html_content += "<tr><th>" + qApp->tr("Description") + "</th>";
            tmp_html_content += "<th>" + qApp->tr("Résultat attendu") + "</th></tr>";

            foreach(Action *tmp_action, tmp_all_actions)
            {
                tmp_html_action_content = tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION);
                in_attachments_manager->loadTextEditAttachments(tmp_html_action_content, in_doc, images_folder);
                tmp_html_content += "<tr><td>" + tmp_html_action_content + "</td>";

                tmp_html_action_content = tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT);
                in_attachments_manager->loadTextEditAttachments(tmp_html_action_content, in_doc, images_folder);
                tmp_html_content += "<td>" + tmp_html_action_content + "</td>";
            }
            tmp_html_content += "</table>";
        }

        tmp_html_content += "<br/>";

        foreach(Test *tmp_test, in_test->getAllChilds())
        {
            tmp_index++;
            tmp_html_content += testToHtml(in_attachments_manager, in_doc, tmp_test, suffix + QString::number(tmp_index) + ".", level + 1, images_folder);
        }
    }

    delete tmp_test_content;
    qDeleteAll(tmp_all_actions);

    return tmp_html_content;
}

/**
  Afficher les informations du test selectionne
**/
void showTestInfos(RecordsTreeView *in_tree_view, QModelIndex in_model_index, bool in_update_history)
{
    Test     *tmp_selected_test = NULL;

    if (in_model_index.isValid()) {
        in_tree_view->setCurrentIndex(in_model_index);
        tmp_selected_test = dynamic_cast<Test*>(in_tree_view->getRecordsTreeModel()->getItem(in_model_index)->getRecord());
        showTestInfos(tmp_selected_test, in_update_history);
    }
}


void insertChildTestAtIndex(RecordsTreeView* in_tree_view, QModelIndex index, bool in_automated_test)
{
    Hierarchy        *tmp_item = NULL;
    int                tmp_row = 0;

    if (index.isValid()) {
        tmp_item = in_tree_view->getRecordsTreeModel()->getItem(index);

        if (tmp_item != NULL)
            tmp_row = tmp_item->childCount();

        insertTestAtIndex(in_tree_view, index, tmp_row, in_automated_test);
    }
}

/**
  Inserer un test
**/
void insertTestAtIndex(RecordsTreeView* in_tree_view, QModelIndex parentIndex, int row, bool in_automated_test)
{
    QModelIndex tmp_new_test_index;

    if (in_automated_test) {
        Test* tmp_test = NULL;

        ProjectVersion* projectVersion = Gui::Services::Projects::projectVersionForIndex(in_tree_view->getRecordsTreeModel(), parentIndex);


        if (projectVersion) {
            TestContent* tmp_test_content = new TestContent();
            ProjectVersion::TestContentRelation::instance().setParent(projectVersion, tmp_test_content);
            tmp_test_content->setValueForKey("", TESTS_CONTENTS_TABLE_SHORT_NAME);
#ifdef GUI_AUTOMATION_ACTIVATED
            tmp_test_content->setValueForKey(TEST_CONTENT_TABLE_AUTOMATED_GUI, TESTS_CONTENTS_TABLE_AUTOMATED);
#else
            tmp_test_content->setValueForKey(TEST_CONTENT_TABLE_AUTOMATED_BATCH, TESTS_CONTENTS_TABLE_AUTOMATED);
#endif

            if (tmp_test_content->saveRecord() == NOERR) {
                tmp_test = new Test(projectVersion);
                tmp_test->setDataFromTestContent(tmp_test_content);
            }
            delete tmp_test_content;

            if (tmp_test)
                in_tree_view->getRecordsTreeModel()->insertItem(row, parentIndex, tmp_test);
        }

    }
    else
        in_tree_view->getRecordsTreeModel()->insertRows(row, 1, parentIndex);

    tmp_new_test_index = in_tree_view->getRecordsTreeModel()->index(row, 0, parentIndex);
    if (tmp_new_test_index.isValid()) {
        showTestInfos(in_tree_view, tmp_new_test_index);
    }
}


/**
  Afficher les informations du test selectionne
**/
void showTestInfos(Test *in_test, bool in_update_history)
{
    Qt::KeyboardModifiers tmp_modifiers = QApplication::keyboardModifiers();

    if (in_test != NULL && !(tmp_modifiers == Qt::ShiftModifier || tmp_modifiers == Qt::ControlModifier || tmp_modifiers == (Qt::ControlModifier | Qt::ShiftModifier))) {
        ProjectView *projectView = Projects::showProjectView(in_test->projectVersion()->project()->getIdentifier());
        FormTest *formTest = projectView->addProjectWidget<FormTest, Test>(in_test);
        QObject::connect(formTest, &FormTest::showOriginalTestInfos, projectView, &ProjectView::showOriginalTestInfos, Qt::UniqueConnection);
        QObject::connect(formTest, &FormTest::showRequirementWithOriginalContentId, projectView, &ProjectView::showRequirementWithOriginalContentId, Qt::UniqueConnection);
    }
}

void showOriginalTestInfos(RecordsTreeView* in_tree_view, Test *in_test_hierarchy)
{
    QModelIndex    tmp_model_index;

    if (in_test_hierarchy != NULL && in_test_hierarchy->original() != NULL) {
        in_tree_view->selectionModel()->select(in_tree_view->selectionModel()->selection(), QItemSelectionModel::Deselect);
        tmp_model_index = in_tree_view->getRecordsTreeModel()->modelIndexForRecord(in_test_hierarchy->original());
        if (tmp_model_index.isValid()) {
            in_tree_view->selectionModel()->select(tmp_model_index, QItemSelectionModel::Select);
            in_tree_view->scrollTo(tmp_model_index);
            in_tree_view->expandIndex(tmp_model_index);
            showTestInfos(in_test_hierarchy->original());
        }
    }
}

void showTestWithContentId(RecordsTreeView* in_tree_view, ProjectVersion *in_project_version, const char *in_test_content_id)
{
    QModelIndex                tmp_model_index;
    Test    *tmp_test = NULL;
    QList<Test*> tmp_tests_array;

    net_session             *tmp_session = Session::instance().getClientSession();

    if (is_empty_string(in_test_content_id) == FALSE) {
        in_tree_view->selectionModel()->select(in_tree_view->selectionModel()->selection(), QItemSelectionModel::Deselect);

        /* Chercher le test dans l'arborescence */
        tmp_test = ProjectVersion::TestRelation::instance().getUniqueChildWithValueForKey(in_project_version, in_test_content_id, TESTS_HIERARCHY_TEST_CONTENT_ID);
        if (tmp_test != NULL) {
            tmp_model_index = in_tree_view->getRecordsTreeModel()->modelIndexForRecord(tmp_test);
            if (tmp_model_index.isValid()) {
                in_tree_view->expandIndex(tmp_model_index);
                showTestInfos(in_tree_view, tmp_model_index);
            }
        }
        /* Chercher le test en base (ex: si le test est dans une autre version du projet) */
        else {
            net_session_print_where_clause(tmp_session, "%s=%s", TESTS_HIERARCHY_TEST_CONTENT_ID, in_test_content_id);

            tmp_tests_array = Test::loadRecordsList(tmp_session->m_where_clause_buffer);
            if (tmp_tests_array.count() == 1) {
                showTestInfos(tmp_tests_array[0]);
            }
            else {
                qDeleteAll(tmp_tests_array);
            }

        }
    }

}

void testSaved(QTreeView* in_tree_view, RecordsTreeModel* in_tree_model, Test *in_test)
{
    QModelIndex    tmp_model_index = in_tree_model->modelIndexForRecord(in_test);
    if (tmp_model_index.isValid())
        in_tree_view->update(tmp_model_index);
}


/**
  Supprimer la liste des tests
**/
void removeTestsList(RecordsTreeModel *in_tree_model, const QList<Test*>& in_records_list)
{
    QModelIndex         tmp_model_index;
    QMessageBox         *tmp_msg_box = NULL;
    QPushButton         *tmp_del_button = NULL;
    QPushButton         *tmp_del_all_button = NULL;
    QPushButton         *tmp_cancel_button = NULL;
    bool                tmp_delete_all = false;
    bool                tmp_delete_current = false;

    if (in_records_list.count() > 0) {
        tmp_msg_box = new QMessageBox(MainWindow::instance());
        tmp_msg_box->setIcon(QMessageBox::Question);
        tmp_msg_box->setWindowTitle("Confirmation...");

        if (in_records_list.count() > 1)
            tmp_del_all_button = tmp_msg_box->addButton(qApp->tr("Supprimer tous les tests"), QMessageBox::NoRole);

        tmp_del_button = tmp_msg_box->addButton(qApp->tr("Supprimer le test"), QMessageBox::YesRole);
        tmp_cancel_button = tmp_msg_box->addButton(qApp->tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->setDefaultButton(tmp_cancel_button);

        foreach(Test *tmp_test, in_records_list)
        {
            tmp_delete_current = tmp_delete_all;

            if (tmp_delete_all == false) {
                tmp_msg_box->setText(qApp->tr("Etes-vous sûr(e) de vouloir supprimer le test \"%1\" ?").arg(tmp_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
                tmp_msg_box->exec();
                if (tmp_msg_box->clickedButton() == tmp_del_button) {
                    tmp_delete_current = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_del_all_button) {
                    tmp_delete_current = true;
                    tmp_delete_all = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_cancel_button)
                    break;
            }

            tmp_model_index = in_tree_model->modelIndexForRecord(tmp_test);
            if (tmp_delete_current && tmp_model_index.isValid()) {
                in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
            }
        }

        delete tmp_msg_box;
    }
}


void selectLinkTests(RecordsTreeView* in_tree_view, const QList<Test*>& in_records_list)
{
    QModelIndex         tmp_model_index;
    QList<Test*>        tmp_link_tests_list;

    if (in_records_list.count() > 0) {
        in_tree_view->selectionModel()->select(in_tree_view->selectionModel()->selection(), QItemSelectionModel::Deselect);

        foreach(Test *tmp_test, in_records_list)
        {
            tmp_link_tests_list = Test::OriginalTestRelation::instance().getChilds(tmp_test);

            foreach(Test *tmp_link_test, tmp_link_tests_list)
            {
                tmp_model_index = in_tree_view->getRecordsTreeModel()->modelIndexForRecord(tmp_link_test);
                if (tmp_model_index.isValid()) {
                    in_tree_view->selectionModel()->select(tmp_model_index, QItemSelectionModel::Select);
                    in_tree_view->scrollTo(tmp_model_index);
                    in_tree_view->expandIndex(tmp_model_index);
                }
            }
        }
    }
}


QModelIndexList dependantsTestsIndexesFromRequirementsList(RecordsTreeView* in_tree_view, const QList<Requirement*>& in_records_list)
{
    net_session         *tmp_session = Session::instance().getClientSession();
    char                *tmp_query = tmp_session->m_last_query;
    char                ***tmp_query_results = NULL;
    unsigned long       tmp_rows_count = 0;
    unsigned long       tmp_columns_count = 0;

    Test                tmp_test;
    QModelIndexList     tmp_final_indexes_list;

    if (in_records_list.count() > 0) {
        in_tree_view->selectionModel()->select(in_tree_view->selectionModel()->selection(), QItemSelectionModel::Deselect);

        tmp_query += net_session_print_query(tmp_session, "SELECT %s.%s,%s.%s,%s.%s,%s.%s,%s.%s FROM %s,%s WHERE %s.%s=%s.%s AND %s.%s IN("
                             , TESTS_HIERARCHY_SIG
                             , TESTS_HIERARCHY_TEST_ID
                             , TESTS_HIERARCHY_SIG
                             , TESTS_HIERARCHY_PROJECT_VERSION_ID
                             , TESTS_HIERARCHY_SIG
                             , TESTS_HIERARCHY_PROJECT_ID
                             , TESTS_HIERARCHY_SIG
                             , TESTS_HIERARCHY_VERSION
                             , TESTS_HIERARCHY_SIG
                             , TESTS_HIERARCHY_TEST_CONTENT_ID
                             , TESTS_HIERARCHY_SIG
                             , TESTS_REQUIREMENTS_TABLE_SIG
                             , TESTS_HIERARCHY_SIG
                             , TESTS_HIERARCHY_TEST_CONTENT_ID
                             , TESTS_REQUIREMENTS_TABLE_SIG
                             , TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID
                             , TESTS_REQUIREMENTS_TABLE_SIG
                             , TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID
        );
        for (int tmp_index = 0; tmp_index < in_records_list.count(); tmp_index++) {
            tmp_query += sprintf_s(tmp_query, MAX_SQL_STATEMENT_LENGTH - (tmp_query - tmp_session->m_last_query), "%s", in_records_list[tmp_index]->getValueForKey(REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID));
            if (tmp_index + 1 < in_records_list.count())
                tmp_query += sprintf_s(tmp_query, MAX_SQL_STATEMENT_LENGTH - (tmp_query - tmp_session->m_last_query), ",");
        }
        tmp_query += sprintf_s(tmp_query, MAX_SQL_STATEMENT_LENGTH - (tmp_query - tmp_session->m_last_query), ");");

        tmp_query_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_query_results != NULL) {
            for (unsigned long tmp_index = 0; tmp_index < tmp_rows_count; tmp_index++) {
                tmp_test.setValueForKey(tmp_query_results[tmp_index][0], TESTS_HIERARCHY_TEST_ID);
                tmp_test.setValueForKey(tmp_query_results[tmp_index][1], TESTS_HIERARCHY_PROJECT_VERSION_ID);
                tmp_test.setValueForKey(tmp_query_results[tmp_index][2], TESTS_HIERARCHY_PROJECT_ID);
                tmp_test.setValueForKey(tmp_query_results[tmp_index][3], TESTS_HIERARCHY_VERSION);
                tmp_test.setValueForKey(tmp_query_results[tmp_index][4], TESTS_HIERARCHY_TEST_CONTENT_ID);

                tmp_final_indexes_list.append(in_tree_view->getRecordsTreeModel()->modelIndexesForRecord(&tmp_test));
            }
            cl_free_rows_columns_array(&tmp_query_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_final_indexes_list;
}


/**
  Importer des tests
**/
void importTests(RecordsTreeView* in_tree_view)
{
    FormDataImport    *tmp_import_form = NULL;

    tmp_import_form = new FormDataImport(new ImportContext(in_tree_view), MainWindow::instance());
    tmp_import_form->show();

}

QStringList getImportExportTestColumnsNames()
{
    QStringList        tmp_columns_names;

    tmp_columns_names << qApp->tr("Nom du test") << qApp->tr("Description du test") << qApp->tr("Catégorie du test") << qApp->tr("Priorité du test") << qApp->tr("Action standard : Description") << qApp->tr("Action standard : Résultat attendu");
    tmp_columns_names << qApp->tr("Status") << qApp->tr("Automatisé") << qApp->tr("Automatisation : Commande") << qApp->tr("Automatisation : Paramètres de commande");
    tmp_columns_names << qApp->tr("Automatisation : Variable de code retour") << qApp->tr("Automatisation : Variable de sortie standard") << qApp->tr("Type") << qApp->tr("Cas aux limites");
    tmp_columns_names << qApp->tr("Action automatisée : Id fenêtre") << qApp->tr("Action automatisée : Type de message") << qApp->tr("Action automatisée : Données") << qApp->tr("Action automatisée : Délai");

    return tmp_columns_names;
}


ImportContext::ImportContext(RecordsTreeView* in_tree_view) :
    Gui::Components::ImportContext(),
    m_last_imported_test(NULL),
    m_tree_view(in_tree_view)
{
    m_columns_names = getImportExportTestColumnsNames(),
        m_entity = &test_entity_import;

    QList<Hierarchy*>    tmp_hierarchies_list = in_tree_view->selectedHierachies();
    Test    *tmp_selected_test = NULL;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(in_tree_view->getRecordsTreeModel(), in_tree_view->selectionModel()->currentIndex());

    if (tmp_project_version) {
        if (tmp_hierarchies_list.isEmpty()) {
            if (tmp_project_version->testsHierarchy().isEmpty() == false) {
                tmp_selected_test = tmp_project_version->testsHierarchy().last();
            }
        }
        else {
            tmp_selected_test = dynamic_cast<Test*>(tmp_hierarchies_list.last()->getRecord());
        }

        if (tmp_selected_test != NULL)
            m_parent = new Test(tmp_selected_test);
        else
            m_parent = new Test(tmp_project_version);
    }
}

void ImportContext::startImport()
{
    TestContent        *tmp_test_content = NULL;
    int            tmp_save_result = NOERR;

    Test    *tmp_test_parent = NULL;
    QString detail = qApp->tr("_ A classer (import des tests de %1 le %2)")
        .arg(Session::instance().getClientSession()->m_username)
        .arg(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm"));

    tmp_test_parent = dynamic_cast<Test*>(m_parent);

    cl_transaction_start(Session::instance().getClientSession());

    tmp_test_content = new TestContent();
    ProjectVersion::TestContentRelation::instance().setParent(tmp_test_parent->projectVersion(), tmp_test_content);
    tmp_test_content->setValueForKey(detail.toStdString().c_str(), TESTS_CONTENTS_TABLE_SHORT_NAME);
    tmp_test_content->setValueForKey(detail.toStdString().c_str(), TESTS_CONTENTS_TABLE_DESCRIPTION);
    tmp_save_result = tmp_test_content->saveRecord();
    if (tmp_save_result == NOERR) {
        tmp_test_parent->setDataFromTestContent(tmp_test_content);
        tmp_save_result = tmp_test_parent->saveRecord();
    }
    delete tmp_test_content;

    if (tmp_save_result != NOERR) {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


void ImportContext::endImportTests()
{
    QList<Hierarchy*>    tmp_records_list = m_tree_view->selectedHierachies();
    Test        *tmp_test = NULL;
    int            tmp_index = 0;
    ProjectVersion* tmp_project_version = Gui::Services::Projects::projectVersionForIndex(m_tree_view->getRecordsTreeModel(), m_tree_view->selectionModel()->currentIndex());

    Test    *tmp_test_parent = NULL;

    int            tmp_tr_result = NOERR;

    bool    tmp_insert_result = false;

    tmp_test_parent = dynamic_cast<Test*>(m_parent);

    if (tmp_records_list.isEmpty()) {
        if (tmp_project_version->testsHierarchy().isEmpty() == false) {
            tmp_test = tmp_project_version->testsHierarchy().last();
        }
    }
    else {
        tmp_test = dynamic_cast<Test*>(tmp_records_list.last()->getRecord());
    }

    if (tmp_test != NULL) {
        tmp_test->insertChild(tmp_index, tmp_test_parent);
        tmp_insert_result = tmp_test->saveChilds() == NOERR;
    }
    else {
        ProjectVersion::TestRelation::instance().insertChild(tmp_project_version, tmp_index, tmp_test_parent);
        tmp_insert_result = ProjectVersion::TestRelation::instance().saveChilds(tmp_project_version) == NOERR;
    }

    if (tmp_insert_result) {
        tmp_tr_result = cl_transaction_commit(Session::instance().getClientSession());
        if (tmp_tr_result != NOERR)
            QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_tr_result));

    }
    else {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


void ImportContext::importRecord(Record *in_record, bool in_last_record)
{
    TestContent        *tmp_test_content = NULL;
    Action        *tmp_action = NULL;
    AutomatedAction    *tmp_automated_action = NULL;
    int            tmp_save_result = NOERR;

    Test    *tmp_test = NULL;

    Test    *tmp_existing_test = NULL;

    bool        tmp_create_action = false;

    Test    *tmp_test_parent = NULL;

    QStringList         tmp_str_list;

    if (m_parent != NULL && in_record != NULL) {
        tmp_test_parent = dynamic_cast<Test*>(m_parent);

        tmp_create_action = is_empty_string(in_record->getValueForKey(ACTION_IMPORT_DESCRIPTION)) == FALSE || is_empty_string(in_record->getValueForKey(ACTION_IMPORT_WAIT_RESULT)) == FALSE;

        tmp_str_list = QString(in_record->getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME)).split(PARENT_HIERARCHY_SEPARATOR, QString::SkipEmptyParts);
        if (tmp_str_list.isEmpty()) {
            tmp_test_parent = m_last_imported_test;
        }
        else {
            foreach(QString tmp_str_name, tmp_str_list)
            {
                tmp_existing_test = tmp_test_parent->getUniqueChildWithValueForKey(tmp_str_name.toStdString().c_str(), TESTS_HIERARCHY_SHORT_NAME);
                if (tmp_existing_test == NULL) {
                    tmp_test_content = new TestContent();
                    if (tmp_test_content != NULL) {
                        tmp_test_content->setValueForKey(tmp_str_name.toStdString().c_str(), TESTS_CONTENTS_TABLE_SHORT_NAME);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION), TESTS_CONTENTS_TABLE_DESCRIPTION);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID), TESTS_CONTENTS_TABLE_CATEGORY_ID);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL), TESTS_CONTENTS_TABLE_PRIORITY_LEVEL);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_STATUS), TESTS_CONTENTS_TABLE_STATUS);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATED), TESTS_CONTENTS_TABLE_AUTOMATED);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND), TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS), TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE), TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE), TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_TYPE), TESTS_CONTENTS_TABLE_TYPE);
                        tmp_test_content->setValueForKey(in_record->getValueForKey(TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE), TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE);
                        ProjectVersion::TestContentRelation::instance().setParent(tmp_test_parent->projectVersion(), tmp_test_content);
                        tmp_save_result = tmp_test_content->saveRecord();
                        if (tmp_save_result == NOERR) {
                            tmp_test = new Test(tmp_test_parent->projectVersion());
                            if (tmp_test != NULL) {
                                tmp_test->setDataFromTestContent(tmp_test_content);
                                tmp_test_parent->appendChild(tmp_test);
                                tmp_save_result = tmp_test_parent->saveChilds();
                                tmp_test_parent = tmp_test;
                            }
                        }

                        delete tmp_test_content;
                    }
                }
                else
                    tmp_test_parent = tmp_existing_test;
            }
        }

        if (tmp_test_parent != NULL) {
            if (tmp_create_action) {
                tmp_action = new Action();
                tmp_action->setValueForKey(tmp_test_parent->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), ACTIONS_TABLE_TEST_CONTENT_ID);
                tmp_action->setValueForKey(in_record->getValueForKey(ACTION_IMPORT_DESCRIPTION), ACTIONS_TABLE_DESCRIPTION);
                tmp_action->setValueForKey(in_record->getValueForKey(ACTION_IMPORT_WAIT_RESULT), ACTIONS_TABLE_WAIT_RESULT);
                tmp_save_result = tmp_action->saveRecord();
                delete tmp_action;
            }
            else if (tmp_test_parent->isAutomatedGuiTest() && is_empty_string(in_record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE)) == FALSE) {
                tmp_automated_action = new AutomatedAction();
                tmp_automated_action->setValueForKey(tmp_test_parent->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID);
                tmp_automated_action->setValueForKey(in_record->getValueForKey(AUTOMATED_ACTIONS_TABLE_WINDOW_ID), AUTOMATED_ACTIONS_TABLE_WINDOW_ID);
                tmp_automated_action->setValueForKey(in_record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE), AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE);
                tmp_automated_action->setValueForKey(in_record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA), AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
                tmp_automated_action->setValueForKey(in_record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY), AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY);
                tmp_save_result = tmp_automated_action->saveRecord();
                delete tmp_automated_action;
            }
        }
    }

    if (tmp_save_result != NOERR) {
        cl_transaction_rollback(Session::instance().getClientSession());

        QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));

    }
    else if (in_last_record) {
        endImportTests();
    }
}


/**
  Exporter des tests
**/
void exportTests(RecordsTreeView* in_tree_view)
{
    FormDataExport    *tmp_export_form = NULL;

    tmp_export_form = new FormDataExport(new ExportContext(in_tree_view->selectedHierachies()), MainWindow::instance());
    tmp_export_form->show();
}


ExportContext::ExportContext(QList<Hierarchy*> in_records_list) :
    Gui::Components::ExportContext(in_records_list)
{

}

void ExportContext::startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QFile                   tmp_file;

    tmp_file.setFileName(in_filepath);
    if (tmp_file.open(QIODevice::WriteOnly)) {
        // Entête
        QString tmp_header;
        QStringList        tmp_columns_names = getImportExportTestColumnsNames();
        for (int tmp_column_index = 0; tmp_column_index < tmp_columns_names.count() - 1; ++tmp_column_index) {
            tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_column_index] + in_field_enclosing_char + in_field_separator;
            tmp_file.write(tmp_header.toStdString().c_str());
        }
        tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_columns_names.count() - 1] + in_field_enclosing_char + in_record_separator;
        tmp_file.write(tmp_header.toStdString().c_str());

        QList<Test*>    exportList;
        foreach(Hierarchy* hierarchy, m_records_list)
        {
            if (hierarchy->getRecord()) {
                if (hierarchy->getRecord()->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID) {
                    exportList.append(dynamic_cast<Test*>(hierarchy->getRecord()));
                }
                else if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_VERSIONS_TABLE_SIG_ID) {
                    exportList.append(ProjectVersion::TestRelation::instance().getChilds(dynamic_cast<ProjectVersion*>(hierarchy->getRecord())));
                }
            }
        }

        writeTestsListToExportFile(tmp_file, exportList, in_field_separator, in_record_separator, in_field_enclosing_char);

        tmp_file.close();
    }
    else {
        QMessageBox::critical(MainWindow::instance(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
    }
}


void writeTestsListToExportFile(QFile & in_file, QList<Test*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    TestContent             *tmp_test_content = NULL;
    QList<Action*>          tmp_test_actions;
    QList<AutomatedAction*>  tmp_test_automated_actions;

    foreach(Test *tmp_record, in_records_list)
    {
        tmp_test_content = TestContent::getEntity(tmp_record->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID));
        if (tmp_test_content != NULL) {
            if (tmp_test_content->isAutomatedGuiTest()) {
                // Actions automatisées
                tmp_test_automated_actions = TestContent::AutomatedActionRelation::instance().getChilds(tmp_test_content);
                if (tmp_test_automated_actions.isEmpty()) {
                    writeTestToExportFile(in_file, tmp_record, tmp_test_content, NULL, NULL, in_field_separator, in_record_separator, in_field_enclosing_char);
                }
                else {
                    foreach(AutomatedAction *tmp_automated_action, tmp_test_automated_actions)
                    {
                        writeTestToExportFile(in_file, tmp_record, tmp_test_content, NULL, tmp_automated_action, in_field_separator, in_record_separator, in_field_enclosing_char);
                    }
                }
            }
            else {
                // Actions
                tmp_test_actions = TestContent::ActionRelation::instance().getChilds(tmp_test_content);
                if (tmp_test_actions.isEmpty()) {
                    writeTestToExportFile(in_file, tmp_record, tmp_test_content, NULL, NULL, in_field_separator, in_record_separator, in_field_enclosing_char);
                }
                else {
                    foreach(Action *tmp_action, tmp_test_actions)
                    {
                        writeTestToExportFile(in_file, tmp_record, tmp_test_content, tmp_action, NULL, in_field_separator, in_record_separator, in_field_enclosing_char);
                    }
                }
            }

            // Tests enfants
            writeTestsListToExportFile(in_file, (tmp_record)->getAllChilds(), in_field_separator, in_record_separator, in_field_enclosing_char);

            delete tmp_test_content;
        }
    }

}


void writeTestToExportFile(QFile & in_file, Test *in_test, TestContent *in_test_content, Action *in_action, AutomatedAction *in_automated_action, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QString                 tmp_str;

    QTextDocument           tmp_doc;

    Test           *tmp_parent = NULL;

    qint64                  tmp_bytes_write = 0;

    // Action
    if (in_action != NULL) {
        in_action->loadAssociatedActionsForVersion(in_test->getValueForKey(TESTS_HIERARCHY_VERSION));
        if (!in_action->associatedTestActions().isEmpty()) {
            foreach(Action *tmp_associated_action, in_action->associatedTestActions())
            {
                writeTestToExportFile(in_file, in_test, in_test_content, tmp_associated_action, NULL, in_field_separator, in_record_separator, in_field_enclosing_char);
            }
            return;
        }
    }

    // Nom du test
    tmp_str = "";
    tmp_parent = in_test;
    while (tmp_parent->parentTest() != NULL) {
        tmp_parent = tmp_parent->parentTest();
        tmp_str = QString(tmp_parent->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) + PARENT_HIERARCHY_SEPARATOR + tmp_str;
    }
    tmp_str += QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME));

    tmp_str = in_field_enclosing_char + tmp_str.replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Description du test
    tmp_doc.setHtml(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));
    tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Catégorie du test
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Priorité du test
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Action
    if (in_action != NULL) {
        // Description de l'action
        tmp_doc.setHtml(in_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
        tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    return;
    }

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Action
    if (in_action != NULL) {
        // Résultat attendu
        tmp_doc.setHtml(in_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
        tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    return;
    }

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Status
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_STATUS)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Automatisé
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATED)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Commande d'automatisation
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Paramètres de la commande d'automatisation
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Variable code retour du batch
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Variable stdout du batch
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Type
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_TYPE)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    // Cas aux limites
    tmp_str = in_field_enclosing_char + QString(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
    tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
    if (tmp_bytes_write < 0)    return;

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    if (in_automated_action) {
        // Id fenetre
        tmp_str = in_field_enclosing_char + QString(in_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_WINDOW_ID)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    return;
    }

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    if (in_automated_action) {
        // Type de message
        tmp_str = in_field_enclosing_char + QString(in_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    return;
    }

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    if (in_automated_action) {
        // Données du message
        tmp_str = in_field_enclosing_char + QString(in_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    return;
    }

    // Separateur de champs
    tmp_bytes_write = in_file.write(in_field_separator);
    if (tmp_bytes_write < 0)    return;

    if (in_automated_action) {
        // Delai avant message suivant
        tmp_str = in_field_enclosing_char + QString(in_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY)).replace(in_field_enclosing_char, QString(in_field_enclosing_char + in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    return;
    }

    // Separateur d'enregistrements
    tmp_bytes_write = in_file.write(in_record_separator);
}

}
}
}
