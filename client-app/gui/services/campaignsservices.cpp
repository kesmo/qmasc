#include "campaignsservices.h"
#include "projectsservices.h"

#include "gui/mainwindow.h"
#include "gui/ProjectView.h"

#include <QMdiSubWindow>
#include <QMessageBox>

namespace Gui
{
namespace Services
{
namespace Campaigns
{


/**
  Afficher les informations de la campagne selectionnee
**/
void showCampaignInfos(Campaign *in_campaign)
{
    ProjectView *projectView = Projects::showProjectView(in_campaign->projectVersion()->project()->getIdentifier());
    projectView->addProjectWidget<FormCampaign, Campaign>(in_campaign);
}


/**
  Ajout d'une campagne
**/
void insertCampaign(RecordsTreeView* in_tree_view, const QModelIndex& index, Campaign *in_campaign)
{
    Hierarchy* hierarchy = in_tree_view->getRecordsTreeModel()->getItem(index);
    in_tree_view->getRecordsTreeModel()->insertItem(hierarchy->childCount(), index, in_campaign);
    showCampaignInfos(in_campaign);
}


/**
  Suppression de la campagne sélectionnée
**/
void removeCampaign(RecordsTreeView* in_tree_view, const QModelIndex& index)
{
    Hierarchy               *tmp_hierarchy = NULL;
    Campaign                *tmp_campaign = NULL;

    if (index.isValid())
    {
        tmp_hierarchy = in_tree_view->getRecordsTreeModel()->getItem(index);
        if (tmp_hierarchy != NULL)
        {
            tmp_campaign = dynamic_cast<Campaign*>(tmp_hierarchy->getRecord());
            if (tmp_campaign != NULL)
            {
                if (QMessageBox::question(
                            MainWindow::instance(),
                            qApp->tr("Confirmation..."),
                            qApp->tr("Etes-vous sûr(e) de vouloir supprimer la campagne \"%1\" ?\nToutes les exécutions associées seront supprimées.").arg(tmp_campaign->getValueForKey(CAMPAIGNS_TABLE_SHORT_NAME)),
                            QMessageBox::Yes | QMessageBox::No,
                            QMessageBox::No) == QMessageBox::Yes)
                {
                    in_tree_view->getRecordsTreeModel()->removeItem(index.row(), index.parent());
                }
            }
        }
    }
}



}
}
}
