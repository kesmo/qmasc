
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPageSetupDialog>
#include <QFileInfo>
#include <QFileDialog>
#include <QApplication>

#include "services.h"

#include "record.h"

#include "requirementsservices.h"
#include "campaignsservices.h"
#include "testsservices.h"
#include "projectsservices.h"

#include "gui/mainwindow.h"

namespace Gui
{
namespace Services
{
namespace Global
{


QString & formatFilename(QString & filename)
{
    return filename.replace('\\', '_')
            .replace('/', '_')
            .replace(':', '_')
            .replace('*', '_')
            .replace('?', '_')
            .replace('"', '_')
            .replace('<', '_')
            .replace('>', '_')
            .replace('|', '_');
}


void showRecordInfos(RecordsTreeView* in_tree_view, Record *in_record)
{
    QModelIndex                 tmp_model_index = in_tree_view->getRecordsTreeModel()->modelIndexForRecord(in_record);

    if (in_record != NULL)
    {
        selectIndex(in_tree_view, tmp_model_index);
    }
}

void selectIndex(RecordsTreeView* in_tree_view, const QModelIndex& modelIndex)
{
    if (modelIndex.isValid())
    {
        Hierarchy* tmp_item = in_tree_view->getRecordsTreeModel()->getItem(modelIndex);
        if (tmp_item){

            in_tree_view->setCurrentIndex(modelIndex);
            in_tree_view->expandIndex(modelIndex);
            tmp_item->userSelectItem();
        }
    }
}

QPrinter* print()
{
    QPrinter *printer = new QPrinter(QPrinter::HighResolution);
    QPageSetupDialog printer_setup(printer);

    if (printer_setup.exec() == QDialog::Accepted)
    {
        QString fileName = QFileDialog::getSaveFileName(QApplication::activeWindow(), "Export PDF", QString(), "*.pdf");
        if (!fileName.isEmpty())
        {
            if (QFileInfo(fileName).suffix().isEmpty())
                fileName.append(".pdf");

            printer->setOutputFormat(QPrinter::PdfFormat);
            printer->setOutputFileName(fileName);
        }
    }

    return printer;
}

}
}
}
