#include "needsservices.h"

#include "services.h"

#include "session.h"

#include "gui/forms/FormNeed.h"
#include "gui/forms/FormDataExport.h"
#include "gui/forms/FormDataImport.h"

#include "gui/mainwindow.h"

#include "projectsservices.h"

#include <QFileDialog>
#include <QApplication>
#include <QMessageBox>
#include <QtPrintSupport/QPrinter>

namespace Gui
{
namespace Services
{
namespace Needs
{
static const char *NEED_COLUMNS_IMPORT[] = {NEEDS_TABLE_SHORT_NAME, NEEDS_TABLE_DESCRIPTION};
static const entity_def need_entity_import = {0, 0, 0, 0, NEED_COLUMNS_IMPORT, 0, 0, sizeof(NEED_COLUMNS_IMPORT)/sizeof(char*)};


void saveNeedsAsHtml(QList<Need*> needs_list)
{
    if (!needs_list.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(QApplication::activeWindow(), "Export html", QString(), "*.html");
        if (!fileName.isEmpty())
        {
            QFile tmp_file(fileName);
            if (tmp_file.open(QIODevice::WriteOnly))
            {
                TestActionAttachmentsManager manager;
                tmp_file.write(generateNeedsDocument(&manager, needs_list, false, QFileInfo(tmp_file))->toHtml("utf-8").toLatin1());
                tmp_file.close();
            }
            else
            {
                QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
            }
        }
    }
}

QTextDocument* generateNeedsDocument(TestActionAttachmentsManager* /*in_attachments_manager*/, QList<Need*> in_needs_list, bool include_image, QFileInfo file_info)
{
    QString tmp_doc_content = QString("<html><head></head><body>");
    QTextDocument               *tmp_doc = new QTextDocument();
    int tmp_index = 0;

    QString         tmp_images_folder_absolute_path;
    QString         tmp_images_folder_name;

    if (!include_image)
    {
        if (file_info.isFile())
        {
            tmp_images_folder_absolute_path = file_info.canonicalFilePath()+"_images";
            QDir tmp_images_dir(tmp_images_folder_absolute_path);
            tmp_images_folder_name = tmp_images_dir.dirName();
            if (!tmp_images_dir.exists())
                file_info.absoluteDir().mkdir(tmp_images_folder_name);
        }
    }

    tmp_index = 0;
    foreach(Need *tmp_need, in_needs_list)
    {
        tmp_index++;
        tmp_doc_content += needToHtml(tmp_doc, tmp_need, QString::number(tmp_index) + ".", 0, tmp_images_folder_absolute_path);
    }

    tmp_doc_content += "</body></html>";

    tmp_doc->setHtml(tmp_doc_content);


    return tmp_doc;
}

void printNeeds(QList<Need*> needs_list)
{
    if (!needs_list.isEmpty())
    {
        QPrinter *printer = Gui::Services::Global::print();
        if (printer)
        {
            if (!printer->outputFileName().isEmpty())
            {
                TestActionAttachmentsManager manager;
                generateNeedsDocument(&manager, needs_list)->print(printer);
            }
            delete printer;
        }
    }
}


QString needToHtml(QTextDocument *in_doc, Need *in_need, QString suffix, int level, QString images_folder)
{
    QString tmp_html_content = QString();
    int tmp_index = 0;

    switch (level)
    {
    case 0:
        tmp_html_content += "<h2 style=\"page-break-before: auto;\">" + suffix + " " + QString(in_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)) + "</h2>";
        break;
    case 1:
        tmp_html_content += "<h3>" + suffix + " " + QString(in_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)) + "</h3>";
        break;
    case 2:
        tmp_html_content += "<h4>" + suffix + " " + QString(in_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)) + "</h4>";
        break;
    case 3:
        tmp_html_content += "<h5>" + suffix + " " + QString(in_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)) + "</h5>";
        break;
    default:
        tmp_html_content += "<h6>" + suffix + " " + QString(in_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)) + "</h6>";
        break;
    }

    if (!is_empty_string(in_need->getValueForKey(NEEDS_TABLE_DESCRIPTION)))
        tmp_html_content += "<u>"+qApp->tr("Description")+"</u> : " + QString(in_need->getValueForKey(NEEDS_TABLE_DESCRIPTION)) + "<br/>";

    tmp_html_content += "<br/>";

    foreach(Need *tmp_need, in_need->getAllChilds())
    {
        tmp_index++;
        tmp_html_content += needToHtml(in_doc, tmp_need, suffix + QString::number(tmp_index) + ".", level + 1, images_folder);
    }

    return tmp_html_content;
}

void writeNeedsListToExportFile(QFile & in_file, QList<Need*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QString                 tmp_str;

    QTextDocument           tmp_doc;

    Need    *tmp_parent = NULL;

    qint64                  tmp_bytes_write = 0;

    foreach(Need *tmp_record, in_records_list)
    {
        // Nom du besoin
        tmp_str = "";
        tmp_parent = tmp_record;
        while (tmp_parent->parentNeed() != NULL)
        {
            tmp_parent = tmp_parent->parentNeed();
            tmp_str = QString(tmp_parent->getValueForKey(NEEDS_TABLE_SHORT_NAME)) + PARENT_HIERARCHY_SEPARATOR + tmp_str;
        }
        tmp_str += QString(tmp_record->getValueForKey(NEEDS_TABLE_SHORT_NAME));

        tmp_str = in_field_enclosing_char + tmp_str.replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    break;

        // Separateur de champs
        tmp_bytes_write = in_file.write(in_field_separator);
        if (tmp_bytes_write < 0)    break;

        // Description du besoin
        tmp_doc.setHtml(tmp_record->getValueForKey(NEEDS_TABLE_DESCRIPTION));
        tmp_str = in_field_enclosing_char + tmp_doc.toPlainText().replace(in_field_enclosing_char, QString(in_field_enclosing_char+in_field_enclosing_char)) + in_field_enclosing_char;
        tmp_bytes_write = in_file.write(tmp_str.toStdString().c_str());
        if (tmp_bytes_write < 0)    break;

        // Separateur de champs
        tmp_bytes_write = in_file.write(in_field_separator);
        if (tmp_bytes_write < 0)    break;

        // Separateur d'enregistrement
        tmp_bytes_write = in_file.write(in_record_separator);

        // Besoins fils
        writeNeedsListToExportFile(in_file, tmp_record->getAllChilds(), in_field_separator, in_record_separator, in_field_enclosing_char);
    }

}

/**
  Exporter des besoins
**/
void exportNeeds(RecordsTreeView* in_tree_view)
{
    FormDataExport *tmp_export_form = NULL;

    tmp_export_form = new FormDataExport(new ExportContext(in_tree_view->selectedHierachies()), QApplication::activeWindow());
//    connect(tmp_export_form, SIGNAL(startExport(QString,QByteArray,QByteArray, QByteArray)), QApplication::activeWindow(), SLOT(startExportNeeds(QString,QByteArray,QByteArray, QByteArray)));
    tmp_export_form->show();
}


/**
  Inserer un besoin
**/
void insertNeedAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(in_tree_view->getRecordsTreeModel()->rowCount(index), 1, index))
            Gui::Services::Global::selectIndex(in_tree_view, in_tree_view->getRecordsTreeModel()->index(in_tree_view->getRecordsTreeModel()->rowCount(index) - 1, 0, index));
    }
}


void addNeedAtIndex(RecordsTreeView* in_tree_view, QModelIndex index)
{
    if (index.isValid()){
        if (in_tree_view->getRecordsTreeModel()->insertRows(index.row()+1, 1, index.parent()))
            Gui::Services::Global::selectIndex(in_tree_view, in_tree_view->getRecordsTreeModel()->index(index.row()+1, 0, index.parent()));
    }
}



/**
  Afficher les informations du besoin selectionne
**/
void showNeedInfos(Need *in_need)
{
    Qt::KeyboardModifiers tmp_modifiers = QApplication::keyboardModifiers();

    if (in_need != NULL && !(tmp_modifiers == Qt::ShiftModifier || tmp_modifiers == Qt::ControlModifier || tmp_modifiers == (Qt::ControlModifier | Qt::ShiftModifier))) {
        ProjectView *projectView = Projects::showProjectView(in_need->project()->getIdentifier());
        projectView->addProjectWidget<FormNeed, Need>(in_need);
    }
}


/**
  Supprimer la liste des besoins passée en paramètre
**/
void removeNeedsList(RecordsTreeModel* in_tree_model, const QList<Need*>& in_records_list)
{
    QModelIndex         tmp_model_index;
    QMessageBox         *tmp_msg_box = NULL;
    QPushButton         *tmp_del_button = NULL;
    QPushButton         *tmp_del_all_button = NULL;
    QPushButton         *tmp_cancel_button = NULL;
    bool                tmp_delete_all = false;
    bool                tmp_delete_current = false;

    if (in_records_list.count() > 0)
    {
        tmp_msg_box = new QMessageBox(QApplication::activeWindow());
        tmp_msg_box->setIcon(QMessageBox::Question);
        tmp_msg_box->setWindowTitle(qApp->tr("Confirmation..."));

        if (in_records_list.count() > 1)
            tmp_del_all_button = tmp_msg_box->addButton(qApp->tr("Supprimer tous les besoins"), QMessageBox::NoRole);

        tmp_del_button = tmp_msg_box->addButton(qApp->tr("Supprimer le besoin"), QMessageBox::YesRole);
        tmp_cancel_button = tmp_msg_box->addButton(qApp->tr("Annuler"), QMessageBox::RejectRole);

        tmp_msg_box->setDefaultButton(tmp_cancel_button);

        foreach(Need *tmp_need, in_records_list)
        {
            tmp_delete_current = tmp_delete_all;

            if (tmp_delete_all == false)
            {
                tmp_msg_box->setText(qApp->tr("Etes-vous sûr(e) de vouloir supprimer le besoin \"%1\" ?").arg(tmp_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)));
                tmp_msg_box->exec();
                if (tmp_msg_box->clickedButton() == tmp_del_button)
                {
                    tmp_delete_current = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_del_all_button)
                {
                    tmp_delete_current = true;
                    tmp_delete_all = true;
                }
                else if (tmp_msg_box->clickedButton() == tmp_cancel_button)
                    break;
            }

            tmp_model_index = in_tree_model->modelIndexForRecord(tmp_need);
            if (tmp_delete_current && tmp_model_index.isValid())
                in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
        }

        delete tmp_msg_box;
    }
}

ExportContext::ExportContext(QList<Hierarchy*> in_records_list) :
    Gui::Components::ExportContext(in_records_list)
{

}

void ExportContext::startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char)
{
    QFile                   tmp_file;

    tmp_file.setFileName(in_filepath);
    if (tmp_file.open(QIODevice::WriteOnly))
    {
        // Entête
        QString tmp_header;
        QStringList tmp_columns_names = getImportExportNeedColumnsNames();
        for (int tmp_column_index = 0; tmp_column_index < tmp_columns_names.count() - 1; ++tmp_column_index)
        {
            tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_column_index] + in_field_enclosing_char + in_field_separator;
            tmp_file.write(tmp_header.toStdString().c_str());
        }
        tmp_header = in_field_enclosing_char + tmp_columns_names[tmp_columns_names.count() - 1] + in_field_enclosing_char + in_record_separator;
        tmp_file.write(tmp_header.toStdString().c_str());

        QList<Need*>    exportList;
        foreach(Hierarchy* hierarchy, m_records_list){
            if (hierarchy->getRecord()){
                if (hierarchy->getRecord()->getEntityDefSignatureId() == NEEDS_TABLE_SIG_ID){
                    exportList.append(dynamic_cast<Need*>(hierarchy->getRecord()));
                }
                else if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_TABLE_SIG_ID){
                    exportList.append(Project::NeedRelation::instance().getChilds(dynamic_cast<Project*>(hierarchy->getRecord())));
                }
            }
        }

        writeNeedsListToExportFile(tmp_file, exportList, in_field_separator, in_record_separator, in_field_enclosing_char);

        tmp_file.close();
    }
    else
    {
        QMessageBox::critical(QApplication::activeWindow(), qApp->tr("Fichier non créé"), qApp->tr("L'ouverture du fichier en écriture est impossible (%1).").arg(tmp_file.errorString()));
    }
}

QStringList getImportExportNeedColumnsNames()
{
    QStringList tmp_columns_names;

    tmp_columns_names << qApp->tr("Nom du besoin") << qApp->tr("Description du besoin");

    return tmp_columns_names;
}


ImportContext::ImportContext(RecordsTreeView* in_tree_view) :
    Gui::Components::ImportContext(),
    m_tree_view(in_tree_view)
{
    m_columns_names = getImportExportNeedColumnsNames();
    m_entity = &need_entity_import;

    QList<Hierarchy*> tmp_records_list = in_tree_view->selectedHierachies();
    Need *tmp_need = NULL;
    Project* tmp_project = Gui::Services::Projects::projectForIndex(in_tree_view->getRecordsTreeModel(), in_tree_view->selectionModel()->currentIndex());

    if (tmp_project)
    {
        if (tmp_records_list.isEmpty())
        {
            if (tmp_project->needsHierarchy().isEmpty() == false)
            {
                tmp_need = tmp_project->needsHierarchy().last();
            }
        }
        else
        {
            tmp_need = dynamic_cast<Need*>(tmp_records_list.last()->getRecord());
        }

        if (tmp_need != NULL)
        {
            QMessageBox::information(MainWindow::instance(), qApp->tr("Information"), qApp->tr("Les besoins seront importés après le besoin \"%1\".").arg(tmp_need->getValueForKey(NEEDS_TABLE_SHORT_NAME)));

            if (tmp_need->parentNeed() != NULL)
                m_parent = new Need(tmp_need->parentNeed());
            else
                m_parent = new Need(tmp_project);
        }
        else
            m_parent = new Need(tmp_project);
    }
}


void ImportContext::importRecord(Record *in_record, bool in_last_record)
{
    int tmp_save_result = NOERR;

    Need *tmp_need_parent = NULL;
    Need *tmp_need = NULL;

    Need *tmp_existing_need = NULL;

    QStringList                 tmp_str_list;

    if (m_parent != NULL && in_record != NULL)
    {
        tmp_need_parent = dynamic_cast<Need*>(m_parent);

        if (is_empty_string(in_record->getValueForKey(NEEDS_TABLE_SHORT_NAME)) == FALSE)
        {
            tmp_str_list = QString(in_record->getValueForKey(NEEDS_TABLE_SHORT_NAME)).split(PARENT_HIERARCHY_SEPARATOR, QString::SkipEmptyParts);

            foreach(QString tmp_str_name, tmp_str_list)
            {
                if (!tmp_str_name.isEmpty())
                {
                    tmp_existing_need = tmp_need_parent->getUniqueChildWithValueForKey(tmp_str_name.toStdString().c_str(), NEEDS_TABLE_SHORT_NAME);
                    if (tmp_existing_need == NULL)
                    {
                        // Creation du besoin importé
                        tmp_need = new Need(tmp_need_parent->project());
                        if (tmp_need != NULL)
                        {
                            tmp_need->setValueForKey(tmp_str_name.toStdString().c_str(), NEEDS_TABLE_SHORT_NAME);
                            tmp_need->setValueForKey(in_record->getValueForKey(NEEDS_TABLE_DESCRIPTION), NEEDS_TABLE_DESCRIPTION);
                            tmp_save_result = tmp_need->saveRecord();
                            if (tmp_save_result == NOERR)
                            {
                                tmp_need_parent->appendChild(tmp_need);

                                tmp_need_parent = tmp_need;
                            }
                        }
                    }
                    else
                        tmp_need_parent = tmp_existing_need;
                }
            }
        }
    }

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());

        QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_save_result));

    }
    else if (in_last_record)
    {
        endImportNeeds();
    }

}


/**
  Importer des besoins
**/
void importNeeds(RecordsTreeView* in_tree_view)
{
    FormDataImport* tmp_import_form = new FormDataImport(new ImportContext(in_tree_view), MainWindow::instance());

    tmp_import_form->show();
}


void ImportContext::startImport()
{
    int tmp_save_result = NOERR;

    Need *tmp_need_parent = NULL;
    QString detail = qApp->tr("_ A classer (import des besoins de %1 le %2)")
            .arg(Session::instance().getClientSession()->m_username)
            .arg(QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm"));


    tmp_need_parent = dynamic_cast<Need*>(m_parent);

    cl_transaction_start(Session::instance().getClientSession());

    tmp_need_parent->setValueForKey(detail.toStdString().c_str(), NEEDS_TABLE_SHORT_NAME);
    tmp_need_parent->setValueForKey(detail.toStdString().c_str(), NEEDS_TABLE_DESCRIPTION);
    tmp_save_result = tmp_need_parent->saveRecord();

    if (tmp_save_result != NOERR)
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


void ImportContext::endImportNeeds()
{
    QList<Hierarchy*> tmp_records_list = m_tree_view->selectedHierachies();
    Need *tmp_need = NULL;
    QModelIndex tmp_model_index;
    int tmp_index = 0;
    Project* tmp_project = Gui::Services::Projects::projectForIndex(m_tree_view->getRecordsTreeModel(), m_tree_view->selectionModel()->currentIndex());

    int tmp_tr_result = NOERR;

    Need *tmp_need_parent = NULL;

    tmp_need_parent = dynamic_cast<Need*>(m_parent);

    if (tmp_records_list.isEmpty())
    {
        if (tmp_project->needsHierarchy().isEmpty() == false)
        {
            tmp_need = tmp_project->needsHierarchy().last();
        }
    }
    else
    {
        tmp_need = dynamic_cast<Need*>(tmp_records_list.last()->getRecord());
    }

    if (tmp_need != NULL)
    {
        tmp_model_index = m_tree_view->getRecordsTreeModel()->modelIndexForRecord(tmp_need);
        if (tmp_model_index.isValid())
        {
            tmp_index = tmp_model_index.row() + 1;
            tmp_model_index = tmp_model_index.parent();
        }
    }
    else
    {
        tmp_index = tmp_project->needsHierarchy().count();
        tmp_model_index  = m_tree_view->currentIndex();
    }

    if (m_tree_view->getRecordsTreeModel()->insertItem(tmp_index, tmp_model_index, tmp_need_parent))
    {
        tmp_tr_result = cl_transaction_commit(Session::instance().getClientSession());
        if (tmp_tr_result != NOERR)
            QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'enregistrement"), Session::instance().getErrorMessage(tmp_tr_result));
    }
    else
    {
        cl_transaction_rollback(Session::instance().getClientSession());
    }
}


}
}
}
