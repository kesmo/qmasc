#include "projectsservices.h"
#include "testsservices.h"
#include "requirementsservices.h"
#include "campaignsservices.h"
#include "services.h"
#include "entities/feature.h"
#include "entities/rule.h"
#include "entities/need.h"

#include "session.h"

#include "gui/mainwindow.h"

#include "gui/forms/FormExecutionsReports.h"
#include "gui/forms/FormProjectBugs.h"
#include "gui/forms/FormProject.h"
#include "gui/forms/FormProjectsReports.h"
#include "gui/ProjectView.h"

#include <QtWidgets/QWidget>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QTableWidgetItem>
#include <QtWidgets/QMdiSubWindow>
#include <QSettings>

namespace Gui
{
namespace Services
{
namespace Projects
{
ProjectView *showProjectView(const QString &projectId, const QString &projectVersionId)
{
    QMdiSubWindow      *tmp_child_window = MainWindow::instance()->findSubWindowForRecordColumnValue<ProjectView>(PROJECTS_TABLE_PROJECT_ID, projectId);
    ProjectView        *projectView = NULL;

    if (tmp_child_window == NULL) {
        projectView = new ProjectView(projectId, projectVersionId, MainWindow::instance());
        tmp_child_window = MainWindow::instance()->createMdiChildForRecord(projectView);
        tmp_child_window->setWindowIcon(QIcon(QPixmap(":/images/22x22/project.png")));
        tmp_child_window->showMaximized();
    }
    else {
        projectView = qobject_cast<ProjectView*>(tmp_child_window->widget());
        projectView->selectProjectVersion(projectVersionId);
        tmp_child_window->activateWindow();
    }

    MainWindow::instance()->updateActiveSubWindowMenu();

    return projectView;
}

/**
  Afficher les informations du projet
**/
void showProjectInfos(Project* in_project)
{
    ProjectView *projectView = Projects::showProjectView(in_project->getIdentifier());
    projectView->addProjectWidget<FormProject, Project>(in_project);
}


/**
  Afficher les informations de la version du projet
**/
void showProjectVersionInfos(ProjectVersion* in_project_version)
{
    ProjectView *projectView = Projects::showProjectView(in_project_version->project()->getIdentifier());
    projectView->addProjectWidget<FormProjectVersion, ProjectVersion>(in_project_version);
}

/**
  Afficher les statistiques d'executions du projet
**/
void showExecutionsStats(Project* in_project)
{
    ProjectView *projectView = Projects::showProjectView(in_project->getIdentifier());
    projectView->addProjectWidget<FormExecutionsReports, Project>(in_project);
}


void showProjectsStats()
{
    FormProjectsReports   *tmp_reports_form = new FormProjectsReports();

    tmp_reports_form->show();
}



/**
  Afficher les anomalies du projet
**/
void showProjectVersionBugs(ProjectVersion* in_project_version)
{
    ProjectView *projectView = Projects::showProjectView(in_project_version->project()->getIdentifier());
    projectView->addProjectWidget<FormProjectBugs, ProjectVersion>(in_project_version);
}

/**
  Importer un projet
**/
void importProject()
{
    QString tmp_filename;
    QFile tmp_file;
    QXmlStreamReader tmp_xml_reader;

    tmp_filename = QFileDialog::getOpenFileName(
                MainWindow::instance(),
                qApp->tr("Sélectionner un fichier d'export..."),
                QString(),
                qApp->tr("Fichier XML (*.xml)"));

    if (tmp_filename.isEmpty() == false)
    {
        tmp_file.setFileName(tmp_filename);
        if (tmp_file.open(QIODevice::ReadOnly))
        {
            QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
            tmp_xml_reader.setDevice(&tmp_file);
            Project *tmp_project = new Project();
            XmlProjectDatas xmlData = tmp_project->readXml(tmp_xml_reader);
            tmp_file.close();
            QApplication::restoreOverrideCursor();
            bool result = xmlData.m_is_valid;
            if (result)
                result = (tmp_project->saveFromXmlProjectDatas(xmlData) == NOERR);

            if (result == false){
                delete tmp_project;
                QMessageBox::critical(MainWindow::instance(), qApp->tr("Erreur lors de l'import du projet"), Session::instance().getLastErrorMessage());
            }
            else{
                Gui::Services::Projects::showProjectView(tmp_project->getIdentifier());
            }
        }
    }
}



/**
  Exporter le projet courant
**/
void exportProject(Project* in_project)
{
    QFile tmp_file;
    QXmlStreamWriter tmp_xml_writer;

    if (in_project != NULL)
    {
        QString tmp_filename = QString("%1.xml").arg(in_project->getValueForKey(PROJECTS_TABLE_SHORT_NAME));
        tmp_filename = QFileDialog::getSaveFileName(MainWindow::instance(), qApp->tr("Exporter sous..."),
                                                    Gui::Services::Global::formatFilename(tmp_filename),
                                                    "Fichier XML (*.xml)");
        if (!tmp_filename.isEmpty())
        {
            tmp_file.setFileName(tmp_filename);
            if (tmp_file.open(QIODevice::ReadWrite))
            {
                QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
                tmp_xml_writer.setAutoFormatting(true);
                tmp_xml_writer.setDevice(&tmp_file);
                in_project->writeXml(tmp_xml_writer, &updateProgressBar);
                tmp_file.close();

                QApplication::restoreOverrideCursor();
            }
        }
    }
}



void removeProject(RecordsTreeModel *in_tree_model, Project *in_project)
{
    if (QMessageBox::question(MainWindow::instance(),
                              qApp->tr("Confirmation..."),
                              qApp->tr("Etes-vous sûr(e) de vouloir supprimer le projet %1 ?").arg(in_project->getValueForKey(PROJECTS_TABLE_SHORT_NAME)),
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
    {
        QModelIndex tmp_model_index = in_tree_model->modelIndexForRecord(in_project);
        if (tmp_model_index.isValid())
            in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
    }
}

void removeProjectVersion(RecordsTreeModel *in_tree_model, ProjectVersion *in_project_version)
{
    if (QMessageBox::question(
                MainWindow::instance(),
                qApp->tr("Confirmation..."),
                qApp->tr("Etes-vous sûr(e) de vouloir supprimer la version %1 du projet %2 ?").arg(ProjectVersion::formatProjectVersionNumber(in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION))).arg(QString(in_project_version->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME))),
                QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
    {
        QModelIndex tmp_model_index = in_tree_model->modelIndexForRecord(in_project_version);
        if (tmp_model_index.isValid())
            in_tree_model->removeItem(tmp_model_index.row(), tmp_model_index.parent());
    }
}


ProjectVersion* projectVersionForIndex(RecordsTreeModel* in_tree_model, QModelIndex modelIndex)
{
    if (in_tree_model)
    {
        Hierarchy* hierarchy = in_tree_model->getItem(modelIndex);
        if (hierarchy->getRecord()){
            if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_VERSIONS_TABLE_SIG_ID)
                return dynamic_cast<ProjectVersion*>(hierarchy->getRecord());
            else if (hierarchy->getRecord()->getEntityDefSignatureId() == FEATURES_CONTENTS_TABLE_SIG_ID)
                return dynamic_cast<Feature*>(hierarchy->getRecord())->projectVersion();
            else if (hierarchy->getRecord()->getEntityDefSignatureId() == RULES_CONTENTS_TABLE_SIG_ID)
                return dynamic_cast<Rule*>(hierarchy->getRecord())->projectVersion();
            else if (hierarchy->getRecord()->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID)
                return dynamic_cast<Requirement*>(hierarchy->getRecord())->projectVersion();
            else if (hierarchy->getRecord()->getEntityDefSignatureId() == CAMPAIGNS_TABLE_SIG_ID)
                return dynamic_cast<Campaign*>(hierarchy->getRecord())->projectVersion();
            else if (hierarchy->getRecord()->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
                return dynamic_cast<Test*>(hierarchy->getRecord())->projectVersion();
        }
    }

    return NULL;
}



Project* projectForIndex(RecordsTreeModel* in_tree_model, QModelIndex modelIndex)
{
    if (in_tree_model)
    {
        Hierarchy* hierarchy = in_tree_model->getItem(modelIndex);
        if (hierarchy->getRecord()){
            if (hierarchy->getRecord()->getEntityDefSignatureId() == PROJECTS_TABLE_SIG_ID)
                return dynamic_cast<Project*>(hierarchy->getRecord());
            else if (hierarchy->getRecord()->getEntityDefSignatureId() == NEEDS_TABLE_SIG_ID)
                return dynamic_cast<Need*>(hierarchy->getRecord())->project();
        }
    }

    return NULL;
}


void setDefaultProjectVersion(ProjectVersion *in_project_version)
{
    if (Session::instance().getDefaultProjectVersionId() != in_project_version->getIdentifier()){
        QSettings tmp_settings;

        tmp_settings.setValue("last_opened_project_id", in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID));
        tmp_settings.setValue("last_opened_project_version_id", in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID));
        tmp_settings.setValue("last_opened_project_version", in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

        Session::instance().setDefaultProjectVersionId(in_project_version->getIdentifier());
    }
}

void askUserForSetDefaultProjectVersion(ProjectVersion* projectVersion)
{
    int tmp_confirm_choice = QMessageBox::question(
        MainWindow::instance(),
        qApp->tr("Confirmation..."),
        qApp->tr("Voules-vous définir la version '%1'' du projet '%2'' comme version par défaut ?")
        .arg(ProjectVersion::formatProjectVersionNumber(projectVersion->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)))
        .arg(projectVersion->project()->getValueForKey(PROJECTS_TABLE_SHORT_NAME)),
        QMessageBox::Yes | QMessageBox::No,
        QMessageBox::Yes);

    if (tmp_confirm_choice == QMessageBox::Yes) {
        Gui::Services::Projects::setDefaultProjectVersion(projectVersion);
    }
}

}
}
}
