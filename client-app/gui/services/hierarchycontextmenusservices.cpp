#include "hierarchycontextmenusservices.h"
#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Gui
{
namespace Services
{
namespace HierarchyContextMenus
{

bool contextMenuForRecordsTreeView(RecordsTreeView *in_recordstreeview, const QPoint& /*position*/)
{
    bool tmp_return = false;
    QMenu* menu = NULL;
    Hierarchy* hierarchy = NULL;

    if (in_recordstreeview->getRecordsTreeModel()) {
        QList<Hierarchy*> hierarchyList = in_recordstreeview->selectedHierachies();

        if (hierarchyList.count() == 0) {
            hierarchy = in_recordstreeview->getRecordsTreeModel()->getItem();
            if (hierarchy)
                menu = Hierarchies::ContextMenuActions::ActionsFactory::instance()->createMenuForActionsTypes(hierarchy->availableContextMenuActionTypes());
        }
        if (hierarchyList.count() == 1) {
            menu = Hierarchies::ContextMenuActions::ActionsFactory::instance()->createMenuForActionsTypes(hierarchyList.first()->availableContextMenuActionTypes());
        }
        else if (hierarchyList.count() > 1) {

            int index = 0;
            QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;
            QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> finalActionsTypesList;

            foreach(Hierarchy* tmp_hierarchy, hierarchyList)
            {
                if (tmp_hierarchy && tmp_hierarchy->getRecord()) {
                    if (index == 0) {
                        finalActionsTypesList = tmp_hierarchy->availableContextMenuActionTypes();
                        foreach(Hierarchies::ContextMenuActions::ActionsFactory::ActionType actionType, finalActionsTypesList)
                        {
                            if (!(actionType & Hierarchies::ContextMenuActions::ActionsFactory::HomogeneousMultipleTargetActionMask))
                                finalActionsTypesList.removeOne(actionType);
                        }

                    }
                    else {
                        actionsTypesList = tmp_hierarchy->availableContextMenuActionTypes();
                        foreach(Hierarchies::ContextMenuActions::ActionsFactory::ActionType actionType, actionsTypesList)
                        {
                            if ((actionType & Hierarchies::ContextMenuActions::ActionsFactory::HomogeneousMultipleTargetActionMask)
                                && !finalActionsTypesList.contains(actionType))
                                finalActionsTypesList.append(actionType);
                        }
                    }
                }
                ++index;
            }

            menu = Hierarchies::ContextMenuActions::ActionsFactory::instance()->createMenuForActionsTypes(finalActionsTypesList);

        }

        if (menu != NULL) {

            QAction* selectedAction = menu->exec(QCursor::pos());
            if (selectedAction) {
                Hierarchies::ContextMenuActions::AbstractAction* action = dynamic_cast<Hierarchies::ContextMenuActions::AbstractAction*>(selectedAction);
                tmp_return = action->exec(in_recordstreeview);
            }

            delete menu;
        }
    }

    return tmp_return;
}

}
}
}
