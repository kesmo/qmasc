#ifndef REQUIREMENTSSERVICES_H
#define REQUIREMENTSSERVICES_H

#include "entities/need.h"
#include "gui/components/hierarchies/projectneedshierarchy.h"

#include "gui/components/RecordsTreeModel.h"
#include "gui/components/RecordsTreeView.h"
#include "gui/components/TestActionAttachmentsManager.h"
#include "gui/components/importexportcontext.h"

#include <QTextDocument>
#include <QFileInfo>
#include <QModelIndex>
#include <QTreeView>

namespace Gui
{
namespace Services
{
namespace Needs
{
    void saveNeedsAsHtml(QList<Need *> needs_list);
    QTextDocument* generateNeedsDocument(TestActionAttachmentsManager* in_attachments_manager, QList<Need*> in_needs_list, bool include_image = true, QFileInfo file_info = QFileInfo());
    void printNeeds(QList<Need *> needs_list);
    QString needToHtml(QTextDocument *in_doc, Need *in_need, QString suffix, int level, QString images_folder);
    void writeNeedsListToExportFile(QFile & in_file, QList<Need*> in_records_list, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    void exportNeeds(RecordsTreeView *in_tree_view);
    void addNeedAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void insertNeedAtIndex(RecordsTreeView* in_tree_view, QModelIndex index);
    void showNeedInfos(Need *in_need);
    void removeNeedsList(RecordsTreeModel *in_tree_model, const QList<Need*>& in_records_list);
    void startExportNeeds(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);
    QStringList getImportExportNeedColumnsNames();
    void importNeeds(RecordsTreeView *in_tree_view);

    class ImportContext :
            public Gui::Components::ImportContext
    {
    public:
        ImportContext(RecordsTreeView* in_tree_view);

        void startImport();
        void importRecord(Record *out_record, bool in_last_record);

        void endImportNeeds();

    private:

        RecordsTreeView* m_tree_view;
    };


    class ExportContext :
            public Gui::Components::ExportContext
    {
    public:
        ExportContext(QList<Hierarchy*> in_records_list);

        virtual void startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char);


    };

}
}
}
#endif // REQUIREMENTSSERVICES_H
