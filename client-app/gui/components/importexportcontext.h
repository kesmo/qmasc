#ifndef IMPORTEXPORTCONTEXT_H
#define IMPORTEXPORTCONTEXT_H

#include "record.h"
#include "hierarchies/hierarchy.h"

#include <QStringList>

namespace Gui
{
namespace Components
{
class ImportContext
{
public:
    ImportContext();

    virtual void startImport() = 0;
    virtual void importRecord(Record *out_record, bool in_last_record) = 0;

    const entity_def *getEntityDef() const { return m_entity; }
    const QStringList& getColumnsNames() const { return m_columns_names; }

protected:
    Record        *m_parent;
    QStringList        m_columns_names;

    const entity_def        *m_entity;

};


class ExportContext
{
public:
    ExportContext(QList<Hierarchy*> in_records_list);

    virtual void startExport(QString in_filepath, QByteArray in_field_separator, QByteArray in_record_separator, QByteArray in_field_enclosing_char) = 0;

protected:
    QList<Hierarchy*> m_records_list;

};

}
}

#endif // IMPORTEXPORTCONTEXT_H
