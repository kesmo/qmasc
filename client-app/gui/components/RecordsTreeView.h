/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef RECORDS_TREE_VIEW_H
#define RECORDS_TREE_VIEW_H

#include <QtWidgets/QTreeView>
#include "hierarchies/hierarchy.h"
#include "RecordsTreeModel.h"

class RecordsTreeView : public QTreeView
{
    Q_OBJECT

public:
    RecordsTreeView(QWidget * parent = 0);

    void moveDown();
    QList<Hierarchy*> selectedHierachies();

    template <class T>
    QList<T*> selectedItems()
    {
        QList<T*>     tmp_list;
        T             *tmp_item = NULL;

        foreach(QModelIndex tmp_index, selectedIndexes())
        {
            tmp_item = static_cast<T*>(tmp_index.internalPointer());
            if (tmp_item != NULL)
                tmp_list.append(tmp_item);
        }

        return tmp_list;
    }

    template <class T>
    QList<T*> selectedRecords()
    {
        QList<T*>     tmp_records_list;
        Hierarchy     *tmp_item = NULL;

        foreach(QModelIndex tmp_index, selectedIndexes())
        {
            tmp_item = static_cast<Hierarchy*>(tmp_index.internalPointer());
            if (tmp_item != NULL){
                T* tmp_record = dynamic_cast<T*>(tmp_item->getRecord());
                if (tmp_record)
                    tmp_records_list.append(tmp_record);
            }
        }

        return tmp_records_list;
    }


    void setRecordsTreeModel(RecordsTreeModel* in_recordstreemodel);
    RecordsTreeModel* getRecordsTreeModel() const;

    void expandIndex(const QModelIndex &in_model_index, bool in_expand_parent = true, bool in_expand_childs = false);
    void setExpandedIndex(const QModelIndex &in_parent_index, bool expanded, bool recursively = true);

Q_SIGNALS:
    void delKeyPressed(QList<Hierarchy*>);
    void userEnterIndex(QModelIndex);

protected Q_SLOTS:
    void modelIndexCreated(const QModelIndex & modelIndex );

protected:
    void keyPressEvent ( QKeyEvent * event );
    void dropEvent ( QDropEvent * event );
    void mousePressEvent ( QMouseEvent * event );
    void mouseReleaseEvent ( QMouseEvent * event );
    void contextMenuEvent ( QContextMenuEvent * event );


    QList<Record *> recordsFromMimeData(const QMimeData *data);

private:
    RecordsTreeModel* mRecordsTreeModel;
    bool mUserPushLeftMouseButton;
};

#endif // RECORDS_TREE_VIEW_H
