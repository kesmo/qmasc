#include "horizontaltabstyle.h"

#include <QStyleOptionTab>

namespace Gui
{
namespace Components
{
namespace Widgets
{

QSize HorizontalTabStyle::sizeFromContents(ContentsType type, const QStyleOption* option,
                        const QSize& size, const QWidget* widget) const
{
    QSize s = QSize(64, QProxyStyle::sizeFromContents(type, option, size, widget).height());
    if (type == QStyle::CT_TabBarTab) {
        s.transpose();
    }
    return s;
}

void HorizontalTabStyle::drawControl(ControlElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget) const
{
    if (element == CE_TabBarTabLabel) {
        if (const QStyleOptionTab* tab = qstyleoption_cast<const QStyleOptionTab*>(option)) {
            QStyleOptionTab opt(*tab);
            opt.shape = QTabBar::RoundedSouth;
            QProxyStyle::drawControl(element, &opt, painter, widget);
            return;
        }
    }
    QProxyStyle::drawControl(element, option, painter, widget);
}

}
}
}
