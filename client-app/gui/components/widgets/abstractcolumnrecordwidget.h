#ifndef ABSTRACTCOLUMNRECORDWIDGET_H
#define ABSTRACTCOLUMNRECORDWIDGET_H

#include "record.h"

#include <QtWidgets/QWidget>

namespace Gui
{
namespace Components
{
namespace Widgets
{

class AbstractColumnRecordWidget : public QWidget
{
    Q_OBJECT

public:
    AbstractColumnRecordWidget(Record* in_record, int columnIndex, QWidget* parent = NULL, Qt::WindowFlags flags = 0);

    virtual void applyWidgetChangesToRecord();
    virtual void applyRecordChangesToWidget();

    void setApplyRecordChangesOnValueChanged(bool autoCommit);
    void setWidgetInternalValue(const QVariant& value);

    bool applyRecordChangesOnValueChanged() const {return m_auto_commit;}

    Record* getRecord() const{return m_record;}
    int getColumnIndex() const{return m_column_index;}

public Q_SLOTS:
    virtual void widgetDataChanged();

private:
    Record*         m_record;
    QVariant        m_value;
    int             m_column_index;

    bool            m_auto_commit;
};

}
}
}

#endif // ABSTRACTCOLUMNRECORDWIDGET_H
