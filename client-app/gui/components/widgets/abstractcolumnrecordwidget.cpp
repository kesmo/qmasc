#include "abstractcolumnrecordwidget.h"

namespace Gui
{
namespace Components
{
namespace Widgets
{

AbstractColumnRecordWidget::AbstractColumnRecordWidget(Record *in_record, int columnIndex, QWidget *parent, Qt::WindowFlags flags) :
    QWidget(parent, flags),
    m_record(in_record),
    m_value(NULL),
    m_column_index(columnIndex),
    m_auto_commit(true)
{
    if (m_record){
        m_value = m_record->getValueForKeyAtIndex(m_column_index);
    }
}

void AbstractColumnRecordWidget::setApplyRecordChangesOnValueChanged(bool autoCommit)
{
    m_auto_commit = autoCommit;
}


void AbstractColumnRecordWidget::setWidgetInternalValue(const QVariant &value)
{
    m_value = value;
    if (applyRecordChangesOnValueChanged()){
        applyWidgetChangesToRecord();
    }

}


void AbstractColumnRecordWidget::applyWidgetChangesToRecord()
{
    if (m_record){
        m_record->setValueForKeyAtIndex(m_value.toString().toStdString().c_str(), m_column_index);
    }
}

void AbstractColumnRecordWidget::widgetDataChanged()
{
    // do nothing
}

void AbstractColumnRecordWidget::applyRecordChangesToWidget()
{
    // do nothing
}

}
}
}
