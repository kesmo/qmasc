#include "radiobuttons.h"

#include <QHBoxLayout>
#include <QRadioButton>

namespace Gui
{
namespace Components
{
namespace Widgets
{

RadioButtons::RadioButtons(const QList< QPair<QString, QString> >& values, Record* in_record, int columnIndex, QWidget* parent) :
    AbstractColumnRecordWidget(in_record, columnIndex, parent)
{
    QHBoxLayout* tmp_grid_layout = new QHBoxLayout(this);
    tmp_grid_layout->setContentsMargins(0, 0, 0, 0);
    QPair<QString, QString> value;

    foreach(value, values){
        QRadioButton* tmp_radio_button = new QRadioButton(value.second, this);
        tmp_radio_button->setChecked(compare_values(value.first.toStdString().c_str(), in_record->getValueForKeyAtIndex(columnIndex)) == 0);
        tmp_radio_button->setProperty("value", value.first);
        connect(tmp_radio_button, SIGNAL(clicked()), this, SLOT(widgetDataChanged()));
        tmp_grid_layout->addWidget(tmp_radio_button);
        m_radio_buttons.append(tmp_radio_button);
    }
}


void RadioButtons::widgetDataChanged()
{
    QRadioButton* radioButton = ::qobject_cast<QRadioButton*>(sender());
    if (radioButton){
        setWidgetInternalValue(radioButton->property("value"));
    }
}

void RadioButtons::applyRecordChangesToWidget()
{
    const char* value = getRecord()->getValueForKeyAtIndex(getColumnIndex());
    foreach(QRadioButton* tmp_radio_button, m_radio_buttons){
        tmp_radio_button->setChecked(compare_values(tmp_radio_button->property("value").toString().toStdString().c_str(), value) == 0);
    }
}

}
}
}
