#ifndef RADIOBUTTONS_H
#define RADIOBUTTONS_H

#include "abstractcolumnrecordwidget.h"

#include <QRadioButton>


namespace Gui
{
namespace Components
{
namespace Widgets
{

class RadioButtons : public AbstractColumnRecordWidget
{
    Q_OBJECT

public:
    RadioButtons(const QList< QPair<QString, QString> >& values, Record* in_record, int columnIndex, QWidget* parent = NULL);

    void applyRecordChangesToWidget();

protected Q_SLOTS:
    void widgetDataChanged();

private:
    QList<QRadioButton*>  m_radio_buttons;

};

}
}
}

#endif // RADIOBUTTONS_H
