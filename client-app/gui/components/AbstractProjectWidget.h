/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef ABSTRACT_PROJECT_WIDGET_H
#define ABSTRACT_PROJECT_WIDGET_H

#include "record.h"
#include <QtWidgets/QWidget>
#include <QCloseEvent>
#include <QIcon>

#define BORDER_COLOR                "#a5b2e8" // bleu <BORDER_COLOR>

#define BORDER_COLOR2               "#597099" // bleu <BORDER_COLOR2>
#define BORDER_COLOR_BUTTON         "#3c4c66" // bleu <BORDER_COLOR_BUTTON>

#define GRADIENT_0                  "#6980ab" // bleu <GRADIENT_0>
#define GRADIENT_01                 "#788fba" // bleu <GRADIENT_0.1>
#define GRADIENT_05                 "#4a5f87" // bleu <GRADIENT_0.5>

#define DSIABLED_CONTROL            "#dfe0e8" // bleu <DSIABLED_CONTROL>

#define COLUMN_HEADER               "#7d92e8" // bleu <COLUMN_HEADER>
#define COLUMN_HEADER_CHECKED       "#2242c4" // bleu <COLUMN_HEADER_CHECKED>

#define GRADIENT_1                  "#a5b2e8" // bleu <GRADIENT_1>


class AbstractProjectWidget :
        public QWidget
{
    Q_OBJECT
public:
    AbstractProjectWidget(Record *in_record, QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~AbstractProjectWidget();

    Record *record() const;

    virtual QIcon icon() const;
    virtual QString title() const;

    static void loadStyleSheet(
            QWidget* widget,
            const QString& borderColor = BORDER_COLOR,
            const QString& borderColor2 = BORDER_COLOR2,
            const QString& borderColorButton = BORDER_COLOR_BUTTON,
            const QString& gradientColor0 = GRADIENT_0,
            const QString& gradientColor01 = GRADIENT_01,
            const QString& gradientColor05 = GRADIENT_05,
            const QString& disabledControlColor = DSIABLED_CONTROL,
            const QString& columnHeaderColor = COLUMN_HEADER,
            const QString& columnHeaderCheckedColor = COLUMN_HEADER_CHECKED,
            const QString& gradientColor1 = GRADIENT_1);

public slots:
    virtual bool saveRecord();
    virtual void save();
    virtual void cancel();

    void setModified(bool modified = true);
    void setModifiable(bool modifiable = true);

    bool isModified() const {return m_modified;}
    bool isModifiable() const {return m_modifiable;}

signals:
    void recordSaved(Record* in_record);
    void recordCanceled(Record* in_record);

protected:
    virtual void closeEvent(QCloseEvent *in_event);
    virtual bool maybeClose();

private:
    Record* m_record;
    bool m_modified;
    bool m_modifiable;

    static const QString STYLESHEET_TEMPLATE;

};

#endif // ABSTRACT_PROJECT_WIDGET_H
