/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "RecordsTreeModel.h"
#include "utilities.h"
#include "session.h"
#include <stdlib.h>

#include <QMimeData>

/**
  Constructeur
**/
RecordsTreeModel::RecordsTreeModel()
{
    m_root_item = NULL;
    m_drop_actions = Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
    m_mime_type = DEFAULT_RECORD_MIME_TYPE;
    m_columns_count = 1;
}

/**
  Constructeur
**/
RecordsTreeModel::RecordsTreeModel(Hierarchy* in_root_item)
{
    m_drop_actions = Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
    m_mime_type = DEFAULT_RECORD_MIME_TYPE;
    m_columns_count = 1;
    setRootItem(in_root_item);
}


/**
  Destructeur
**/
RecordsTreeModel::~RecordsTreeModel()
{
}


void RecordsTreeModel::setRootItem(Hierarchy* in_root_item)
{
    m_root_item = in_root_item;
    connect(in_root_item, &Hierarchy::childrenChanged, this, &RecordsTreeModel::updateHierarchy, Qt::UniqueConnection);
}


void RecordsTreeModel::setColumnsCount(int count)
{
    m_columns_count = count;
}


/**
  Renvoie le nombre de colonnes
**/
int RecordsTreeModel::columnCount(const QModelIndex & /* parent */) const
{
    return m_columns_count;
}


/**
  Renvoie les donnees en fonction de l'index passe en parametre
**/
QVariant RecordsTreeModel::data(const QModelIndex &index, int role) const
{
    Hierarchy *tmp_item = NULL;

    if (!index.isValid())
        return QVariant();

    tmp_item = getItem(index);
    if (tmp_item != NULL)
        return tmp_item->data(index.column(), role);

    return QVariant();
}


/**
  Renvoie les proprietes du item se trouvant a l'index passe en parametre
**/
Qt::ItemFlags RecordsTreeModel::flags(const QModelIndex &index) const
{
    Hierarchy *tmp_item = NULL;
    Qt::ItemFlags defaultFlags;

    defaultFlags = QAbstractItemModel::flags(index);

    tmp_item = getItem(index);
    if (tmp_item != NULL) {
        if (tmp_item->isWritable())
            defaultFlags |= Qt::ItemIsDropEnabled;

        if (tmp_item->isDataEditable())
            defaultFlags |= Qt::ItemIsEditable;

        if (tmp_item->canMove())
            defaultFlags |= Qt::ItemIsDragEnabled;
    }

    return defaultFlags;
}


/**
    Renvoie le item se trouvant a l'index passe en parametre
**/
Hierarchy* RecordsTreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        Hierarchy *item = static_cast<Hierarchy*>(index.internalPointer());
        if (item != NULL)
            return item;
    }

    return m_root_item;
}


QModelIndexList RecordsTreeModel::modelIndexesForItemsWithValueForKey(int in_signature, const char *in_value, const char *in_key, const QModelIndex &in_parent) const
{
    QModelIndexList tmp_final_indexes_list, tmp_indexes_list;
    QModelIndex     tmp_current_model_index;
    Hierarchy       *tmp_item = NULL;
    Hierarchy       *tmp_parent_item = NULL;

    if (in_parent.isValid() == false)
        tmp_parent_item = m_root_item;
    else
        tmp_parent_item = getItem(in_parent);

    for (int tmp_index = 0; tmp_index < tmp_parent_item->childCount(); tmp_index++) {
        tmp_item = tmp_parent_item->child(tmp_index);
        if (tmp_item != NULL) {
            tmp_current_model_index = index(tmp_index, 0, in_parent);
            if (tmp_current_model_index.isValid()) {
                if (tmp_item->getRecord() && tmp_item->getRecord()->getEntityDefSignatureId() == in_signature && compare_values(tmp_item->getRecord()->getValueForKey(in_key), in_value) == 0) {
                    tmp_final_indexes_list.append(tmp_current_model_index);
                }
                else {
                    tmp_indexes_list = modelIndexesForItemsWithValueForKey(in_signature, in_value, in_key, tmp_current_model_index);
                    tmp_final_indexes_list.append(tmp_indexes_list);
                }
            }
        }
    }

    return tmp_final_indexes_list;
}


QModelIndexList RecordsTreeModel::modelIndexesForItemsWithValuesForKeys(int in_signature, QList<const char *> in_values, QList<const char *> in_keys, const QModelIndex &in_parent) const
{
    QModelIndexList tmp_final_indexes_list, tmp_indexes_list;
    QModelIndex     tmp_current_model_index;
    Hierarchy       *tmp_item = NULL;
    Hierarchy       *tmp_parent_item = NULL;

    if (in_keys.count() == in_values.count()) {
        if (in_parent.isValid() == false)
            tmp_parent_item = m_root_item;
        else
            tmp_parent_item = getItem(in_parent);

        for (int tmp_index = 0; tmp_index < tmp_parent_item->childCount(); tmp_index++) {
            tmp_item = tmp_parent_item->child(tmp_index);
            if (tmp_item != NULL) {
                tmp_current_model_index = index(tmp_index, 0, in_parent);
                if (tmp_current_model_index.isValid()) {
                    bool found = false;

                    if (tmp_item->getRecord() && tmp_item->getRecord()->getEntityDefSignatureId() == in_signature) {
                        for (int tmp_value_index = 0; tmp_value_index < in_values.count(); ++tmp_value_index) {
                            found = (compare_values(tmp_item->getRecord()->getValueForKey(in_keys[tmp_value_index]), in_values[tmp_value_index]) == 0);

                            if (!found) {
                                break;
                            }
                        }
                    }

                    if (found) {
                        tmp_final_indexes_list.append(tmp_current_model_index);
                    }
                    else {
                        tmp_indexes_list = modelIndexesForItemsWithValuesForKeys(in_signature, in_values, in_keys, tmp_current_model_index);
                        tmp_final_indexes_list.append(tmp_indexes_list);
                    }
                }
            }
        }
    }

    return tmp_final_indexes_list;
}


QModelIndex RecordsTreeModel::modelIndexForRecord(Record *in_record, bool in_by_ref, const QModelIndex &in_parent) const
{
    QModelIndex     tmp_model_index, tmp_current_model_index;
    Hierarchy       *tmp_item = NULL;
    Hierarchy       *tmp_parent_item = NULL;

    if (in_record) {
        if (in_parent.isValid() == false)
            tmp_parent_item = m_root_item;
        else
            tmp_parent_item = getItem(in_parent);

        for (int tmp_index = 0; tmp_index < tmp_parent_item->childCount(); tmp_index++) {

            tmp_item = tmp_parent_item->child(tmp_index);
            if (tmp_item != NULL && tmp_item->getRecord()) {
                tmp_current_model_index = index(tmp_index, 0, in_parent);
                if (tmp_current_model_index.isValid()) {
                    if (
                        (
                        (in_by_ref && in_record == tmp_item->getRecord())
                        || (!in_by_ref && tmp_item->getRecord()->getEntityDef()->m_entity_signature_id == in_record->getEntityDef()->m_entity_signature_id &&
                        tmp_item->getRecord()->compareTo(in_record) == 0)
                        )
                        ) {
                        return tmp_current_model_index;
                    }
                    else if (tmp_item->mayPerformChildLookupForRecord(in_record)) {
                        tmp_model_index = modelIndexForRecord(in_record, in_by_ref, tmp_current_model_index);
                        if (tmp_model_index.isValid())
                            return tmp_model_index;
                    }
                }
            }
        }
    }

    return QModelIndex();
}


QModelIndexList RecordsTreeModel::modelIndexesForRecord(Record *in_record, const QModelIndex &in_parent) const
{
    QModelIndex     tmp_model_index;
    Hierarchy       *tmp_item = NULL;
    Hierarchy       *tmp_parent_item = NULL;
    QModelIndexList tmp_final_indexes_list, tmp_indexes_list;

    if (in_record) {
        if (in_parent.isValid() == false)
            tmp_parent_item = m_root_item;
        else
            tmp_parent_item = getItem(in_parent);

        for (int tmp_index = 0; tmp_index < tmp_parent_item->childCount(); tmp_index++) {

            tmp_item = tmp_parent_item->child(tmp_index);
            if (tmp_item != NULL && tmp_item->getRecord()) {
                tmp_model_index = index(tmp_index, 0, in_parent);
                if (tmp_model_index.isValid()) {
                    if (tmp_item->getRecord()->getEntityDef()->m_entity_signature_id == in_record->getEntityDef()->m_entity_signature_id
                        && tmp_item->getRecord()->compareTo(in_record) == 0) {
                        tmp_final_indexes_list.append(tmp_model_index);
                    }
                    else if (tmp_item->mayPerformChildLookupForRecord(in_record)) {
                        tmp_indexes_list = modelIndexesForRecord(in_record, tmp_model_index);
                        tmp_final_indexes_list.append(tmp_indexes_list);
                    }
                }
            }
        }
    }

    return tmp_final_indexes_list;
}


QModelIndex RecordsTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    Hierarchy *parentItem = NULL;
    Hierarchy *childItem = NULL;

    if (!hasIndex(row, column, parent))
        return QModelIndex();

    if (!parent.isValid())
        parentItem = m_root_item;
    else
        parentItem = static_cast<Hierarchy*>(parent.internalPointer());

    childItem = parentItem->child(row);

    if (childItem != NULL) {
        connect(childItem, &Hierarchy::childrenChanged, this, &RecordsTreeModel::updateHierarchy, Qt::UniqueConnection);

        QModelIndex index = createIndex(row, column, childItem);
        emit indexCreated(index);
        return index;
    }
    else
        return QModelIndex();
}



/**
  Inserer un nouveau item
**/
bool RecordsTreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    Hierarchy *parentItem = getItem(parent);
    bool success = false;

    if (parentItem->isWritable() && parentItem->canInsert(position)) {
        beginInsertRows(parent, position, position + rows - 1);
        success = parentItem->insertChildren(position, rows, NULL);
        endInsertRows();
    }

    return success;
}


bool RecordsTreeModel::insertRow(int row, const QModelIndex &parent)
{
    return insertRows(row, 1, parent);
}


/**
  Inserer un nouveau item
**/
bool RecordsTreeModel::insertItem(int position, const QModelIndex &parent, Record *in_item)
{
    Hierarchy *parentItem = getItem(parent);
    bool success = false;

    if (parentItem->isWritable() && parentItem->canInsert(position)) {
        beginInsertRows(parent, position, position);
        success = parentItem->insertChildren(position, 1, in_item);
        endInsertRows();
    }

    return success;
}


bool RecordsTreeModel::insertLinkForItem(int position, const QModelIndex &parent, Record *in_item)
{
    Hierarchy *parentItem = getItem(parent);
    bool success = false;

    if (parentItem->isWritable() && parentItem->canInsert(position)) {
        qDebug() << Q_FUNC_INFO << position;
        beginInsertRows(parent, position, position);
        success = parentItem->insertLinkOfChildren(position, in_item);
        endInsertRows();
    }

    return success;
}



/**
  Inserer une copie de l'item
**/
bool RecordsTreeModel::insertCopyOfItem(int position, const QModelIndex &parent, Record *in_item)
{
    Hierarchy            *parentItem = getItem(parent);
    bool                success = false;
    QList<Record*>        *tmp_list = NULL;
    int                    tmp_item_index = 0;

    if (parentItem->isWritable() && parentItem->canInsert(position)) {
        tmp_list = parentItem->orginalsRecordsForRelationsEntity(in_item);
        if (tmp_list != NULL) {
            if (tmp_list->isEmpty()) {
                success = true;
            }
            else {
                beginInsertRows(parent, position, position + tmp_list->count() - 1);
                for (tmp_item_index = 0; tmp_item_index < tmp_list->count(); tmp_item_index++) {
                    success = parentItem->insertChildren(position + tmp_item_index, 1, tmp_list->at(tmp_item_index));
                    if (!success)
                        break;
                }
                endInsertRows();
            }

            delete tmp_list;
        }
        else {
            beginInsertRows(parent, position, position);
            success = parentItem->insertCopyOfChildren(position, 1, in_item);
            endInsertRows();
        }
    }

    return success;
}


/**
  Return parent item
**/
QModelIndex RecordsTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    Hierarchy *childItem = static_cast<Hierarchy*>(index.internalPointer());
    Hierarchy *parentItem = childItem->parent();

    if (parentItem == NULL || parentItem == m_root_item)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}


/**
  Remove item
**/
bool RecordsTreeModel::removeRows(int /*position*/, int /*rows*/, const QModelIndex &/*parent*/)
{
    // This function is call directly by Qt framework when moving an item into the hierarchy (insert, then remove).
    // As the move is done before in dropMimeData function we only have to return true to inform Qt framework that it can remove from its cache the index for that item.

    return true;
}


/**
  Remove item
**/
bool RecordsTreeModel::removeItem(int position, const QModelIndex &parent, bool in_move_indic)
{
    Hierarchy *parentItem = getItem(parent);
    bool success = false;

    if (parentItem->isWritable() && parentItem->canRemove(position)) {
        qDebug() << Q_FUNC_INFO << position;
        beginRemoveRows(parent, position, position);
        success = parentItem->removeChildren(position, 1, in_move_indic);
        endRemoveRows();
    }

    return success;
}
/**
  Renvoie le nombre de items enfants du item passe en parametre
**/
int RecordsTreeModel::rowCount(const QModelIndex &parent) const
{
    Hierarchy *parentItem = NULL;

    if (!parent.isValid())
        parentItem = m_root_item;
    else
        parentItem = static_cast<Hierarchy*>(parent.internalPointer());

    if (parentItem)
        return parentItem->childCount();

    return 0;
}

void RecordsTreeModel::setSupportedDropActions(Qt::DropActions in_drop_actions)
{
    m_drop_actions = in_drop_actions;
}


Qt::DropActions RecordsTreeModel::supportedDropActions() const
{
    return m_drop_actions;
}


bool RecordsTreeModel::dropRecords(QList<Record*> in_records_list, Qt::DropAction action, int row, const QModelIndex &parent)
{
    int                 tmp_index = 0;

    Hierarchy           *tmp_hierarchy = NULL;
    int                 tmp_lock_record_status = RECORD_STATUS_MODIFIABLE;
    Hierarchy           *tmp_drop_parent_item = NULL;
    Hierarchy           *tmp_drag_parent_item = NULL;

    bool                tmp_return = false;
    int                 beginRow = 0;

    QModelIndex         tmp_model_index;

    if (row != -1)
        beginRow = row;
    else if (parent.isValid())
        beginRow = 0;
    else
        beginRow = rowCount(QModelIndex());

    // Verifier les donnees deposees
    foreach(Record* tmp_generic_record, in_records_list)
    {

        switch (action) {
        case Qt::IgnoreAction:
            return true;

        case Qt::CopyAction:
            tmp_return = insertCopyOfItem(beginRow + tmp_index, parent, tmp_generic_record);
            break;

        case Qt::MoveAction:
            tmp_model_index = modelIndexForRecord(tmp_generic_record, false);
            if (tmp_model_index.isValid()) {
                tmp_hierarchy = getItem(tmp_model_index);

                if (tmp_hierarchy != NULL && tmp_hierarchy->getRecord()) {
                    tmp_drag_parent_item = tmp_hierarchy->parent();
                    if (tmp_drag_parent_item != NULL) {
                        tmp_drop_parent_item = getItem(parent);
                        if (tmp_hierarchy->mayHaveCyclicRedundancy(tmp_drop_parent_item, false)) {
                            emit cyclicRedundancy();
                        }
                        else {
                            tmp_lock_record_status = tmp_hierarchy->getRecord()->lockRecordStatus();
                            if (tmp_drag_parent_item->canRemove(tmp_hierarchy->row())) {
                                if (tmp_drop_parent_item->canInsert(beginRow + tmp_index)) {
                                    if (tmp_drag_parent_item == tmp_drop_parent_item && tmp_hierarchy->row() < beginRow) {
                                        --beginRow;
                                    }
                                    tmp_return = removeItem(tmp_hierarchy->row(), tmp_model_index.parent(), true);
                                    if (tmp_return) {
                                        tmp_return = insertItem(beginRow + tmp_index, parent, tmp_hierarchy->getRecord());
                                    }
                                }
                                else {
                                    // Annuler le verrou
                                    if (tmp_lock_record_status != RECORD_STATUS_OWN_LOCK)
                                        tmp_hierarchy->getRecord()->unlockRecord();
                                }
                            }
                        }
                    }
                }
            }
            break;

        case Qt::LinkAction:
            tmp_model_index = modelIndexForRecord(tmp_generic_record, false);
            if (tmp_model_index.isValid()) {
                tmp_hierarchy = getItem(tmp_model_index);
                if (tmp_hierarchy != NULL && tmp_hierarchy->getRecord()) {
                    tmp_drop_parent_item = getItem(parent);

                    if (tmp_hierarchy->mayHaveCyclicRedundancy(tmp_drop_parent_item, true)) {
                        emit cyclicRedundancy();
                    }
                    else {
                        if (tmp_drop_parent_item->canInsert(beginRow + tmp_index)) {
                            tmp_return = insertLinkForItem(beginRow + tmp_index, parent, tmp_hierarchy->getRecord());
                        }
                    }
                }
            }
            break;

        default:
            break;
        }

        ++tmp_index;
    }

    return tmp_return;
}


bool RecordsTreeModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    return QAbstractItemModel::dropMimeData(data, action, row, column, parent);
}



QStringList RecordsTreeModel::mimeTypes() const
{
    QStringList types;
    types << m_mime_type;
    return types;
}


QMimeData* RecordsTreeModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData           *tmp_mime_data = new QMimeData();
    QByteArray          tmp_byte_array;
    Hierarchy           *tmp_record = NULL;
    Hierarchy        *tmp_current_record = NULL;
    QList<Hierarchy*>    tmp_records_list;
    QList<Hierarchy*>    tmp_remove_records_list;
    bool        tmp_indic = false;

    foreach(QModelIndex tmp_current_index, indexes)
    {
        if (tmp_current_index.isValid()) {
            tmp_record = getItem(tmp_current_index);
            if (tmp_record != NULL) {
                tmp_indic = false;

                foreach(tmp_current_record, tmp_records_list)
                {
                    if (tmp_record->isChildOf(tmp_current_record)) {
                        tmp_indic = true;
                        break;
                    }

                    if (tmp_record->isParentOf(tmp_current_record)) {
                        tmp_remove_records_list.append(tmp_current_record);
                    }
                }

                if (!tmp_indic) {
                    tmp_records_list.append(tmp_record);
                }
            }
        }
    }

    foreach(tmp_record, tmp_records_list)
    {
        if (tmp_remove_records_list.indexOf(tmp_record) < 0) {
            tmp_byte_array.append(tmp_record->getRecord()->serialize());
            tmp_byte_array.append(RECORD_SEPARATOR_CHAR);
        }
    }

    tmp_mime_data->setData(m_mime_type, tmp_byte_array);

    return tmp_mime_data;
}


void RecordsTreeModel::setMimeType(QString in_mime_type)
{
    m_mime_type = in_mime_type;
}


void RecordsTreeModel::updateHierarchy(Hierarchy* /*hierarchy*/, int /*in_old_rows_count*/, int /*in_new_rows_count*/)
{
    emit layoutChanged();
}


bool RecordsTreeModel::submit()
{
    if (m_root_item)
        return m_root_item->applyChanges();

    return false;
}


void RecordsTreeModel::revert()
{
    if (m_root_item)
        m_root_item->revertChanges();
}
