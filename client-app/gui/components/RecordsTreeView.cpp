/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "RecordsTreeView.h"
#include "gui/services/hierarchycontextmenusservices.h"
#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"
#include "entitiesmodel.h"

#include <QVBoxLayout>
#include <QRadioButton>
#include <QMimeData>

RecordsTreeView::RecordsTreeView(QWidget * parent) : QTreeView(parent),
    mRecordsTreeModel(NULL),
    mUserPushLeftMouseButton(false)
{
    setDefaultDropAction(Qt::MoveAction);
    setUniformRowHeights(true);
}

void RecordsTreeView::setRecordsTreeModel(RecordsTreeModel* in_recordstreemodel)
{
    mRecordsTreeModel = in_recordstreemodel;
    setModel(mRecordsTreeModel);

    connect(mRecordsTreeModel, SIGNAL(indexCreated(QModelIndex)), this, SLOT(modelIndexCreated(QModelIndex)));
}


RecordsTreeModel* RecordsTreeView::getRecordsTreeModel() const
{
    return mRecordsTreeModel;
}


void RecordsTreeView::keyPressEvent ( QKeyEvent * event )
{

    if (selectionModel() != NULL)
    {
        if (event != NULL)
        {
            switch (event->key())
            {
            case Qt::Key_Delete:
                emit delKeyPressed(selectedHierachies());
                break;
            case Qt::Key_Enter:
            case Qt::Key_Return:
                emit userEnterIndex(currentIndex());
                break;
            }
        }
    }

    QTreeView::keyPressEvent(event);
}


void RecordsTreeView::dropEvent ( QDropEvent * event )
{
    if (model()->supportedDropActions() & event->dropAction()) {

        const QMimeData *tmp_mime_data = event->mimeData();
        if (tmp_mime_data != NULL && tmp_mime_data->hasFormat(DEFAULT_RECORD_MIME_TYPE)){

            QList<Record*> tmp_records_list = recordsFromMimeData(tmp_mime_data);
            QModelIndex tmp_index = indexAt(event->pos());
            int row = -1;
            Qt::DropAction drop_action;
            Qt::DropActions drop_actions(Qt::ActionMask);


            if (tmp_index != QModelIndex()) {

                switch (dropIndicatorPosition()) {
                case QAbstractItemView::AboveItem:
                    row = tmp_index.row();
                    tmp_index = tmp_index.parent();
                    break;

                case QAbstractItemView::BelowItem:
                    row = tmp_index.row() + 1;
                    tmp_index = tmp_index.parent();
                    break;

                case QAbstractItemView::OnItem:
                case QAbstractItemView::OnViewport:
                    break;
                }

            }

            if (event->source() != this)
            {
                drop_action = Qt::CopyAction;
            }
            // Gérer les éléments différents
            else
            {
                Hierarchy* tmp_hierarchy = getRecordsTreeModel()->getItem(tmp_index);
                foreach (Record* tmp_generic_record, tmp_records_list)
                {
                    drop_actions = drop_actions & tmp_hierarchy->possibleDropActionsForRecord(tmp_generic_record);
                }

                if (drop_actions.testFlag(event->dropAction()))
                    drop_action = event->dropAction();
                else if (drop_actions & Qt::CopyAction)
                    drop_action = Qt::CopyAction;
                else if (drop_actions & Qt::MoveAction)
                    drop_action = Qt::MoveAction;
                else if (drop_actions & Qt::LinkAction)
                    drop_action = Qt::LinkAction;
                else
                    drop_action = Qt::IgnoreAction;
            }

            event->setDropAction(drop_action);

            event->setAccepted(getRecordsTreeModel()->dropRecords(tmp_records_list, event->dropAction(), row, tmp_index));
            qDeleteAll(tmp_records_list);
            return;
        }

    }

    QTreeView::dropEvent(event);
}


void RecordsTreeView::moveDown()
{
    moveCursor(MoveNext, Qt::NoModifier);
}


QList<Hierarchy*> RecordsTreeView::selectedHierachies()
{
    return selectedItems<Hierarchy>();
}


void RecordsTreeView::expandIndex(const QModelIndex &in_model_index, bool in_expand_parent, bool in_expand_childs)
{
    QModelIndex tmp_parent_index;

    if (in_model_index.isValid())
    {
        expand(in_model_index);

        if (in_expand_parent)
        {
            tmp_parent_index = in_model_index.parent();
            while (tmp_parent_index.isValid())
            {
                expand(tmp_parent_index);
                tmp_parent_index = tmp_parent_index.parent();
            }
        }

        if (in_expand_childs)
        {
            int childsCount = model()->rowCount(in_model_index);
            for(int tmp_row_index = 0; tmp_row_index < childsCount; ++tmp_row_index){
                expandIndex(model()->index(tmp_row_index, 0, in_model_index), false, in_expand_childs);
            }
        }
    }
}


void RecordsTreeView::mousePressEvent(QMouseEvent *event)
{
    mUserPushLeftMouseButton = false;

    QModelIndex tmp_index = indexAt(event->pos());
    if (tmp_index.isValid())
    {
        if (event->buttons() & Qt::LeftButton && event->modifiers() ^ Qt::ControlModifier && event->modifiers() ^ Qt::ShiftModifier)
        {
            if (visualRect(tmp_index).contains(event->pos().x(), event->pos().y()))
                mUserPushLeftMouseButton = true;
        }
    }
    QTreeView::mousePressEvent(event);
}


void RecordsTreeView::mouseReleaseEvent(QMouseEvent *event)
{
    if(mUserPushLeftMouseButton){
        QModelIndex tmp_index = indexAt(event->pos());
        if (tmp_index.isValid())
        {
            emit userEnterIndex(tmp_index);
        }

    }

    QTreeView::mouseReleaseEvent(event);
}


void RecordsTreeView::contextMenuEvent(QContextMenuEvent *event)
{
    if (!Gui::Services::HierarchyContextMenus::contextMenuForRecordsTreeView(this, event->pos()))
        QTreeView::contextMenuEvent(event);
}


void RecordsTreeView::setExpandedIndex(const QModelIndex &in_parent_index, bool expanded, bool recursively)
{
    if (in_parent_index.isValid())
    {
        setExpanded(in_parent_index, expanded);
        if (recursively)
        {
            QModelIndex tmp_child = in_parent_index.child(0, 0);
            while (tmp_child.isValid())
            {
                setExpandedIndex(tmp_child, expanded, recursively);
                tmp_child = in_parent_index.child(tmp_child.row() + 1, 0);
            }
        }
    }
}


void RecordsTreeView::modelIndexCreated( const QModelIndex & modelIndex )
{
    Hierarchy* hierarchy = mRecordsTreeModel->getItem(modelIndex);
    if (hierarchy && hierarchy->isDataEditable()){
        QWidget* widgetItem = QTreeView::indexWidget(modelIndex);
        if (widgetItem == NULL){
            Gui::Components::Widgets::AbstractColumnRecordWidget* columnRecordWidgetItem = hierarchy->createCustomWidgetEditorForColumn(modelIndex.column());
            if (columnRecordWidgetItem){
                columnRecordWidgetItem->setParent(this);
                QTreeView::setIndexWidget(modelIndex, columnRecordWidgetItem);
            }
        }
    }
}



QList<Record*> RecordsTreeView::recordsFromMimeData(const QMimeData *data)
{
    const QString&      tmp_mime_type = getRecordsTreeModel()->getMimeType();
    QByteArray          tmp_bytes_array = data->data(tmp_mime_type);
    QList<QString>      tmp_string_records_list;
    int                 tmp_index = 0;

    entity_def          *tmp_entity_def = NULL;
    int                 tmp_start_index = 0;

    Record       *tmp_generic_record = NULL;

    QList<Record*>      tmp_records_list;

    // Verifier les donnees deposees
    if (tmp_bytes_array.isEmpty() == false)
    {
        tmp_string_records_list = QString(tmp_bytes_array).split(RECORD_SEPARATOR_CHAR);
        for (tmp_index = 0; tmp_index < tmp_string_records_list.count(); tmp_index++)
        {
            tmp_start_index = tmp_string_records_list[tmp_index].indexOf(SEPARATOR_CHAR);
            if (tmp_start_index >= 0)
            {
                std::string tmp_std_string  = tmp_string_records_list[tmp_index].section(SEPARATOR_CHAR, 0, 0).toStdString();
                if (get_table_def(atoi(tmp_std_string.c_str()), &tmp_entity_def) == NOERR)
                {
                    tmp_generic_record = Relations::EntitiesModel::instance().createEntity(tmp_entity_def->m_entity_signature_id);
                    if (tmp_generic_record != NULL)
                    {
                        tmp_generic_record->deserialize(tmp_string_records_list[tmp_index]);
                        tmp_records_list.append(tmp_generic_record);
                    }
                }
            }
        }
    }

    return tmp_records_list;
}
