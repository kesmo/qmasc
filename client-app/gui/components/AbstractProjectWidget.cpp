/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#include "AbstractProjectWidget.h"
#include "entitiesmodel.h"

#include <QMessageBox>

const QString AbstractProjectWidget::STYLESHEET_TEMPLATE = "";

AbstractProjectWidget::AbstractProjectWidget(Record* in_record, QWidget *parent, Qt::WindowFlags flags)
    : QWidget(parent, flags),
      m_record(in_record),
      m_modified(false),
      m_modifiable(false)
{
    setAttribute(Qt::WA_DeleteOnClose);
    Relations::EntitiesModel::instance().createEntitiesLoggerContext(QString("%1").arg((quintptr)this));
}


AbstractProjectWidget::~AbstractProjectWidget()
{
    Relations::EntitiesModel::instance().removeEntitiesLoggerContext(QString("%1").arg((quintptr)this));
}

Record *AbstractProjectWidget::record() const
{
    return m_record;
}

QIcon AbstractProjectWidget::icon() const
{
    return QIcon();
}

QString AbstractProjectWidget::title() const
{
    return QString();
}

void AbstractProjectWidget::loadStyleSheet(
        QWidget* widget,
        const QString& borderColor,
        const QString& borderColor2,
        const QString& borderColorButton,
        const QString& gradientColor0,
        const QString& gradientColor01,
        const QString& gradientColor05,
        const QString& disabledControlColor,
        const QString& columnHeaderColor,
        const QString& columnHeaderCheckedColor,
        const QString& gradientColor1
        )
{
    QFile     tmp_css_file( ":/abstractprojectwidget_stylesheet.css" );


    // Gestion de la feuille de style
    if ( tmp_css_file.open( QIODevice::ReadOnly | QIODevice::Text ))
    {
        QString template_sheet = tmp_css_file.readAll();
        tmp_css_file.close();

        template_sheet.replace("<BORDER_COLOR>", borderColor);
        template_sheet.replace("<BORDER_COLOR2>", borderColor2);
        template_sheet.replace("<BORDER_COLOR_BUTTON>", borderColorButton);
        template_sheet.replace("<GRADIENT_0>", gradientColor0);
        template_sheet.replace("<GRADIENT_0.1>", gradientColor01);
        template_sheet.replace("<GRADIENT_0.5>", gradientColor05);
        template_sheet.replace("<DSIABLED_CONTROL>", disabledControlColor);
        template_sheet.replace("<COLUMN_HEADER>", columnHeaderColor);
        template_sheet.replace("<COLUMN_HEADER_CHECKED>", columnHeaderCheckedColor);
        template_sheet.replace("<GRADIENT_1>", gradientColor1);

        //widget->setStyleSheet(template_sheet);
    }

}

void AbstractProjectWidget::save()
{
    if (m_record){
        if (saveRecord()){
            emit recordSaved(m_record);
        }
    }
}


bool AbstractProjectWidget::saveRecord()
{
    Q_ASSERT(m_record != NULL);

    return (m_record->saveRecord() == NOERR);
}

void AbstractProjectWidget::cancel()
{
    emit recordCanceled(m_record);
    close();
}


void AbstractProjectWidget::setModified(bool modified)
{
    if (m_modified != modified)
    {
        m_modified = modified;
        if (m_modified)
            setWindowTitle(QString("%1 (*)").arg(windowTitle()));
        else
            setWindowTitle(windowTitle().replace(" (*)", ""));
    }
}


void AbstractProjectWidget::setModifiable(bool modifiable)
{
    m_modifiable = modifiable;
}

void AbstractProjectWidget::closeEvent(QCloseEvent *in_event)
{
    if (maybeClose())
        in_event->accept();
    else
        in_event->ignore();
}


bool AbstractProjectWidget::maybeClose()
{
    int tmp_confirm_choice = 0;
    bool    tmp_return = true;

    if (m_modified && m_record)
    {
        tmp_confirm_choice = QMessageBox::question(
                    this,
                    tr("Confirmation..."),
                    tr("L'enregistrement a été modifié. Voulez-vous enregistrer les modifications ?"),
                    QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
                    QMessageBox::Cancel);

        if (tmp_confirm_choice == QMessageBox::Yes)
            tmp_return = saveRecord();
        else if (tmp_confirm_choice == QMessageBox::Cancel)
            tmp_return = false;
    }

    return tmp_return;
}
