#ifndef PROJECTREQUIREMENTSHIERARCHY_H
#define PROJECTREQUIREMENTSHIERARCHY_H

#include "hierarchy.h"

#include "entities/projectversion.h"

namespace Hierarchies
{


class ProjectRequirementsHierarchy :
    public EntityNode<ProjectVersion::RequirementRelation>
{
public:
    ProjectRequirementsHierarchy(ProjectVersion* projectVersion);
    Hierarchy* createChildHierarchy(Requirement* child);

    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool insertChildren(int position, int count, Record *in_child = NULL);
    bool removeChildren(int position, int count, bool in_move_indic = true);

    bool isWritable();

    QVariant data(int column, int role = 0);

    int dbChildCount();

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    Qt::DropActions possibleDropActionsForRecord(Record* in_record);

};

}
#endif // PROJECTREQUIREMENTSHIERARCHY_H
