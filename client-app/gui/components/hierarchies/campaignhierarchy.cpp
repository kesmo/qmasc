#include "campaignhierarchy.h"

#include "requirementhierarchy.h"

#include "entities/testcampaign.h"
#include "testcampaignhierarchy.h"

#include "entities/requirement.h"
#include "entities/test.h"

#include "entities/projectversion.h"
#include "gui/services/campaignsservices.h"

#include <QIcon>

namespace Hierarchies
{

CampaignHierarchy::CampaignHierarchy(Campaign *campaign, bool in_show_shilds, QObject *parent /*= NULL*/) :
    EntityNode(campaign, parent),
    m_show_childs(in_show_shilds)
{
}

Hierarchy* CampaignHierarchy::createChildHierarchy(TestCampaign* child)
{
    return new TestCampaignHierarchy(child);
}


QList<Record*>* CampaignHierarchy::orginalsRecordsForRelationsEntity(Record *in_item)
{
    QList<Record*>*         tmp_list = new QList<Record*>();
    Test           *tmp_test_hierarchy = NULL;
    Requirement    *tmp_requirement = NULL;

    if (in_item != NULL && getEntity() != NULL && getEntity()->projectVersion() != NULL)
    {
        if (in_item->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID)
        {
            tmp_requirement = Requirement::findRequirementWithId(getEntity()->projectVersion()->requirementsHierarchy(), in_item->getIdentifier(), true);
            if (tmp_requirement != NULL)
            {
                foreach(Test *tmp_test, tmp_requirement->dependantsTests())
                {
                    tmp_list->append(tmp_test);
                }
            }
        }
        else if (in_item->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
        {
            tmp_test_hierarchy = Test::findTestWithId(getEntity()->projectVersion()->testsHierarchy(), in_item->getIdentifier(), true);
            if (tmp_test_hierarchy != NULL)
            {
                tmp_list->append(tmp_test_hierarchy);
            }
        }
    }

    return tmp_list;
}

bool CampaignHierarchy::insertChildren(int position, int /* count */, Record *in_item)
{
    if (in_item != NULL)
    {
        if (in_item->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
            getEntity()->insertTestAtIndex(dynamic_cast<Test*>(in_item), position);
        else if (in_item->getEntityDefSignatureId() == TESTS_CAMPAIGNS_TABLE_SIG_ID)
            getEntity()->insertTestCampaign(position, dynamic_cast<TestCampaign*>(in_item));
    }

    return getEntity()->saveTestCampaigns() == NOERR;
}


bool CampaignHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    ::TestCampaign *tmp_src_item = NULL;

    if (in_item != NULL && in_item->getEntityDefSignatureId() == TESTS_CAMPAIGNS_TABLE_SIG_ID)
    {
        tmp_src_item = getEntity()->getUniqueTestCampaignWithValueForKey(in_item->getIdentifier(), TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID);
        if (tmp_src_item != NULL)
            return insertChildren(position, count, tmp_src_item);
    }

    return false;
}


bool CampaignHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    if (in_move_indic)
        getEntity()->detachTestCampaignAtIndex(position);
    else
        getEntity()->removeTestCampaignAtIndex(position);
    return getEntity()->saveTestCampaigns() == NOERR;
}


QVariant CampaignHierarchy::data(int /* column */, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        switch (getRecord()->lockRecordStatus())
        {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/campaigns.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/campaigns.png")));
            break;

        case RECORD_STATUS_BROKEN:
        default:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/campaigns.png")));
        }

        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(getRecord()->getValueForKey(CAMPAIGNS_TABLE_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }

    return QVariant();
}

int CampaignHierarchy::childCount()
{
    if (m_show_childs)
        return getEntity()->testsList().count();

    return 0;
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> CampaignHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertCampaign;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveCampaign;

    return actionsTypesList;
}


void CampaignHierarchy::userSelectItem()
{
    Gui::Services::Campaigns::showCampaignInfos(getEntity());
}


}
