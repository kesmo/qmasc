/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "requirementhierarchy.h"
#include "entities/test.h"
#include "testhierarchy.h"
#include "entities/requirementcontent.h"
#include "entities/projectgrant.h"
#include "entities/project.h"

#include "utilities.h"
#include "entities.h"
#include "session.h"

#include "gui/services/requirementsservices.h"

#include <QTextDocument>
#include <QIcon>
#include <QPixmap>

namespace Hierarchies
{


/**
  Constructeur
**/
RequirementHierarchy::RequirementHierarchy(Requirement *in_parent) : EntityNode(in_parent)
{
}


/**
  Destructeur
**/
RequirementHierarchy::~RequirementHierarchy()
{
}

Hierarchy* RequirementHierarchy::createChildHierarchy(Requirement* child)
{
    return new RequirementHierarchy(child);
}

/**
  Renvoie la valeur de la colonne <i>column</i>
**/
QVariant RequirementHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        switch (getEntity()->lockRecordStatus())
        {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/requirement.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/requirement.png")));
            break;

        case RECORD_STATUS_BROKEN:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/requirement.png")));
            break;

        default:
            if(childCount() > 0)
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/requirements_1.png")));
            else
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/requirement.png")));
        }

        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(getEntity()->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }

    return QVariant();
}


bool RequirementHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Requirement         *tmp_requirement = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char                ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool                        tmp_return = false;

    if (in_item != NULL)
    {
        if (in_item->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_requirement_from_requirement(%s, '%s', %s, %s, %s, NULL);",
                    getEntity()->projectVersion()->project()->getIdentifier(),
                    getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                    getEntity()->getIdentifier(),
                    (position > 0 ? getEntity()->getAllChilds()[position -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_requirement = new Requirement(getEntity());
                        if (tmp_requirement->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(position, count, tmp_requirement);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
    }

    return tmp_return;
}


/**
  Insertion d'un nouveau requirement enfant
**/
bool RequirementHierarchy::insertChildren(int in_index, int /* count */, Record *in_requirement)
{
    Requirement *tmp_requirement = NULL;
    RequirementContent* tmp_requirement_content = NULL;

    int tmp_save_result = NOERR;

    if (in_requirement == NULL)
    {
        tmp_requirement_content = new RequirementContent();
        tmp_requirement_content->setValueForKey("", REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME);
        ProjectVersion::RequirementContentRelation::instance().setParent(getEntity()->projectVersion(), tmp_requirement_content);
        tmp_save_result = tmp_requirement_content->saveRecord();
        if (tmp_save_result == NOERR)
        {
            tmp_requirement = new Requirement(getEntity()->projectVersion());
            tmp_requirement->setDataFromRequirementContent(tmp_requirement_content);
        }
        delete tmp_requirement_content;
    }
    else
        tmp_requirement = dynamic_cast<Requirement*>(in_requirement);

    getEntity()->insertChild(in_index, tmp_requirement);
    return getEntity()->saveChilds() == NOERR;
}


/**
  Supprimer un requirement enfant
**/
bool RequirementHierarchy::removeChildren(int in_index, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        getEntity()->removeChildAtIndex(in_index);
    else
        getEntity()->detachChildAtIndex(in_index);

    return getEntity()->saveChilds() == NOERR;
}


bool RequirementHierarchy::isWritable()
{
    return getEntity()->projectVersion()->canWriteRequirements();
}


bool RequirementHierarchy::canMove()
{
    return getEntity()->projectVersion()->canWriteRequirements();
}



int RequirementHierarchy::dbChildCount()
{
    char ***tmp_results = NULL;
    unsigned long tmp_rows_count = 0;
    unsigned long tmp_columns_count = 0;
    int tmp_records_count = -1;

    if (is_empty_string(getEntity()->getIdentifier()) == false)
    {
        /* Verifier qu'il n'existe pas un nouvel element a la suite */
        net_session_print_query(Session::instance().getClientSession(), "SELECT COUNT(%s) FROM %s WHERE %s=%s;"
                , REQUIREMENTS_TABLE_REQUIREMENT_ID
                , REQUIREMENTS_TABLE_SIG
                , REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID
                , getRecord()->getIdentifier());
        tmp_results = cl_run_sql(Session::instance().getClientSession(), Session::instance().getClientSession()->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL)
        {
            tmp_records_count = atoi(tmp_results[0][0]);
            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_records_count;
}

QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> RequirementHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SelectDepedantTests;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::PrintRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SaveRequirementAsHtml;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveRequirement;
//    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::AssociateTestsAndRequirements;

    return actionsTypesList;
}


Qt::DropActions RequirementHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case REQUIREMENTS_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0){
            if (compare_values(getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION)) == 0){
                return Qt::CopyAction | Qt::MoveAction;
            }else {
                return Qt::CopyAction;
            }
        }else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}

void RequirementHierarchy::userSelectItem()
{
    Gui::Services::Requirements::showRequirementInfos(getEntity());
}


}


