#include "projectgranthierarchy.h"

#include "gui/components/widgets/radiobuttons.h"

namespace Hierarchies
{


ProjectGrantHierarchy::ProjectGrantHierarchy(ProjectGrant *projectGrant) : Hierarchy(projectGrant)
{
}


QVariant ProjectGrantHierarchy::data(int column, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        if (column == 0){
            if (compare_values(getRecord()->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0)
                return QIcon(QPixmap(QString::fromUtf8(":/images/hierarchies/access.png")));
            else if (compare_values(getRecord()->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_READ) == 0)
                return QIcon(QPixmap(QString::fromUtf8(":/images/hierarchies/partial-access.png")));
            else
                return QIcon(QPixmap(QString::fromUtf8(":/images/hierarchies/no-access.png")));
        }
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        switch (column){
        case 0:
            switch(atoi(getRecord()->getValueForKey(PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID))){
            case PROJECTS_TABLE_SIG_ID:
                return tr("Droits de modification des données du projet");
                break;
            case TESTS_HIERARCHY_SIG_ID:
                return tr("Droits d'accès aux scénarios et cas de tests du projet");
                break;
            case REQUIREMENTS_HIERARCHY_SIG_ID:
                return tr("Droits d'accès aux exigences du projet");
                break;
            case CAMPAIGNS_TABLE_SIG_ID:
                return tr("Droits d'accès aux campagnes du projet");
                break;
            case EXECUTIONS_CAMPAIGNS_TABLE_SIG_ID:
                return tr("Droits d'accès aux exécutions de campagnes du projet");
                break;
            case NEEDS_TABLE_SIG_ID:
                return tr("Droits d'accès aux besoins du projet");
                break;
            case FEATURES_HIERARCHY_SIG_ID:
                return tr("Droits d'accès aux fonctionnalités du projet");
                break;
            case RULES_HIERARCHY_SIG_ID:
                return tr("Droits d'accès aux règles de gestion du projet");
                break;
            default:
                return tr("Droits d'accès aux entités de signature %1 du projet").arg(getRecord()->getValueForKey(PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID));
            }

            break;
        }
        break;

    default:
        return QVariant();

    }

    return QVariant();
}


int ProjectGrantHierarchy::childCount()
{
    return 0;
}


Hierarchy *ProjectGrantHierarchy::child(int /*number*/)
{
    return NULL;
}


Gui::Components::Widgets::AbstractColumnRecordWidget* ProjectGrantHierarchy::createCustomWidgetEditorForColumn(int column)
{
    if (column == 1){
        QList< QPair<QString, QString> > values;
        values << QPair<QString, QString>(PROJECT_GRANT_NONE, tr("Aucun"));
        values << QPair<QString, QString>(PROJECT_GRANT_READ, tr("Lecture"));
        values << QPair<QString, QString>(PROJECT_GRANT_WRITE, tr("Ecriture"));

        return new Gui::Components::Widgets::RadioButtons(values, getRecord(), ProjectGrant::indexForKey(PROJECTS_GRANTS_TABLE_RIGHTS));
    }

    return NULL;
}

}
