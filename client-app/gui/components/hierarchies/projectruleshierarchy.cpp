#include "projectruleshierarchy.h"
#include "rulehierarchy.h"
#include "entities/rulecontent.h"
#include "entities/project.h"

#include "session.h"

#include <QIcon>

namespace Hierarchies
{


ProjectRulesHierarchy::ProjectRulesHierarchy(ProjectVersion *projectVersion) : EntityNode(projectVersion)
{
}

Hierarchy* ProjectRulesHierarchy::createChildHierarchy(Rule* child)
{
    return new RuleHierarchy(child);
}

bool ProjectRulesHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Rule            *tmp_rule = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool                tmp_return = false;

    if (in_item != NULL)
    {
        net_session_print_query(tmp_session, "select create_rule_from_rule(%s, '%s', %s, %s, %s, NULL);",
                getEntity()->project()->getIdentifier(),
                getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                "NULL",
                (position > 0 ? getEntity()->rulesHierarchy()[position -1]->getIdentifier() : "NULL"),
                in_item->getIdentifier());
        tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL)
        {
            if (tmp_rows_count == 1 && tmp_columns_count == 1)
            {
                if (is_empty_string(tmp_results[0][0]) == FALSE)
                {
                    tmp_rule = new Rule(getEntity());
                    if (tmp_rule->loadRecord(tmp_results[0][0]) == NOERR)
                    {
                        tmp_return = insertChildren(position, count, tmp_rule);
                    }
                }
            }

            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_return;
}



bool ProjectRulesHierarchy::insertChildren(int position, int /*count*/, Record *in_child)
{
    Rule*           tmp_child_rule = NULL;
    RuleContent* tmp_rule_content = NULL;

    int tmp_save_result = NOERR;

    if (in_child == NULL)
    {
        tmp_rule_content = new RuleContent();
        tmp_rule_content->setValueForKey("", RULES_CONTENTS_TABLE_SHORT_NAME);
        ProjectVersion::RuleContentRelation::instance().setParent(getEntity(), tmp_rule_content);
        tmp_save_result = tmp_rule_content->saveRecord();
        if (tmp_save_result == NOERR)
        {
            tmp_child_rule = new Rule(getEntity());
            tmp_child_rule->setDataFromRuleContent(tmp_rule_content);
            ProjectVersion::RuleRelation::instance().insertChild(getEntity(), position, tmp_child_rule);
            return ProjectVersion::RuleRelation::instance().saveChilds(getEntity()) == NOERR;
        }
        delete tmp_rule_content;
    }
    else if (in_child->getEntityDefSignatureId() == RULES_HIERARCHY_SIG_ID)
    {
        ProjectVersion::RuleRelation::instance().insertChild(getEntity(), position, dynamic_cast<Rule*>(in_child));
        return ProjectVersion::RuleRelation::instance().saveChilds(getEntity()) == NOERR;
    }

    return false;
}


bool ProjectRulesHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    if (in_move_indic)
        ProjectVersion::RuleRelation::instance().detachChildAtIndex(getEntity(), position);
    else
        ProjectVersion::RuleRelation::instance().removeChildAtIndex(getEntity(), position, count, in_move_indic);
    return ProjectVersion::RuleRelation::instance().saveChilds(getEntity()) == NOERR;
}


bool ProjectRulesHierarchy::isWritable()
{
    return getEntity()->canWriteRules();
}


QVariant ProjectRulesHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/rules.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Règles de gestion");
        break;

    default:
        return QVariant();

    }

    return QVariant();

}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectRulesHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportRule;

    return actionsTypesList;
}


Qt::DropActions ProjectRulesHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case RULES_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(RULES_HIERARCHY_PROJECT_ID)) == 0){
            if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(RULES_HIERARCHY_VERSION)) == 0){
                return Qt::CopyAction | Qt::MoveAction;
            }else {
                return Qt::CopyAction;
            }
        }else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}



}
