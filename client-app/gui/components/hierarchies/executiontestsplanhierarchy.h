#ifndef EXECUTIONTESTSPLANHIERARCHY_H
#define EXECUTIONTESTSPLANHIERARCHY_H

#include "hierarchy.h"

#include "entities/executioncampaign.h"

namespace Hierarchies
{

class ExecutionTestsPlanHierarchy :
    public EntityNode<ExecutionCampaign::ExecutionTestRelation>
{
public:
    ExecutionTestsPlanHierarchy(ExecutionCampaign* executionCampaign);

    Hierarchy* createChildHierarchy(ExecutionTest* child);

};

}

#endif // EXECUTIONTESTSPLANHIERARCHY_H
