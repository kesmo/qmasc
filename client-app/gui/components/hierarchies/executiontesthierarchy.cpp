#include "executiontesthierarchy.h"
#include "entities/action.h"
#include "entities/executionaction.h"
#include "entities/test.h"

namespace Hierarchies
{

ExecutionTestHierarchy::ExecutionTestHierarchy(ExecutionTest* executionTest) : EntityNode(executionTest)
{
}


QVariant ExecutionTestHierarchy::data(int column, int role)
{
    ExecutionTest* executionTest = dynamic_cast<ExecutionTest*>(getRecord());

    float tmp_test_execution_coverage = executionTest->executionCoverageRate() * 100;
    float tmp_test_execution_validated = executionTest->executionValidatedRate() * 100;
    float tmp_test_execution_invalidated = executionTest->executionInValidatedRate() * 100;
    float tmp_test_execution_bypassed = executionTest->executionBypassedRate() * 100;

    QString     tmp_float_string;

    if (role == Qt::ToolTipRole)
    {
        return QVariant();
    }
    else if (role == Qt::TextColorRole)
    {
        // Vert
        if (tmp_test_execution_validated == 100.0)
        {
            return QVariant(ExecutionTest::OK_COLOR);
        }
        else
        {
            if (tmp_test_execution_invalidated > 0.0)
            {
                return QVariant(ExecutionTest::KO_COLOR);
            }
            else if (tmp_test_execution_coverage == 0.0)
            {
                return QVariant(ExecutionTest::BY_PASSED_COLOR);
            }
            else
            {
                return QVariant(ExecutionTest::INCOMPLETED_COLOR);
            }
        }
    }
    else if (role == Qt::DisplayRole)
    {
        if (executionTest->projectTest() != NULL)
        {
            switch (column)
            {
            case 0:
                if (role == Qt::DisplayRole && executionTest->actions().count() >= 1)
                    return QVariant(QString("%1 (%2)").arg(executionTest->projectTest()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)).arg(QString::number(executionTest->actions().count())));
                else
                    return executionTest->projectTest()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME);
                break;

            case 1:
                if (tmp_test_execution_coverage > 0){
                    tmp_float_string.setNum(tmp_test_execution_coverage, 'f', 0);
                    return QVariant(tmp_float_string + " %");
                }
                break;

            case 2:
                if (tmp_test_execution_validated > 0)
                {
                    tmp_float_string.setNum(tmp_test_execution_validated, 'f', 0);
                    return QVariant(tmp_float_string + " %");
                }
                break;

            case 3:
                if (tmp_test_execution_invalidated > 0)
                {
                    tmp_float_string.setNum(tmp_test_execution_invalidated, 'f', 0);
                    return QVariant(tmp_float_string + " %");
                }
                break;

            case 4:
                if (tmp_test_execution_bypassed > 0)
                {
                    tmp_float_string.setNum(tmp_test_execution_bypassed, 'f', 0);
                    return QVariant(tmp_float_string + " %");
                }
                break;
            }
        }
    }

    return QVariant();
}

Hierarchy* ExecutionTestHierarchy::createChildHierarchy(ExecutionTest* child)
{
    return new ExecutionTestHierarchy(child);
}

}
