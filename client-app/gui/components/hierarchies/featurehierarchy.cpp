#include "featurehierarchy.h"
#include "entities/project.h"
#include "session.h"

#include "gui/services/featuresservices.h"

namespace Hierarchies
{

FeatureHierarchy::FeatureHierarchy(Feature *in_feature) : EntityNode(in_feature)
{
}

bool FeatureHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Feature         *tmp_feature = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char                ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool                        tmp_return = false;

    if (in_item != NULL) {
        if (in_item->getEntityDefSignatureId() == FEATURES_HIERARCHY_SIG_ID) {
            net_session_print_query(tmp_session, "select create_feature_from_feature(%s, '%s', %s, %s, %s, NULL);",
                                    getEntity()->projectVersion()->project()->getIdentifier(),
                                    getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                                    getEntity()->getIdentifier(),
                                    (position > 0 ? getEntity()->getAllChilds()[position - 1]->getIdentifier() : "NULL"),
                                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL) {
                if (tmp_rows_count == 1 && tmp_columns_count == 1) {
                    if (is_empty_string(tmp_results[0][0]) == FALSE) {
                        tmp_feature = new Feature(getEntity());
                        if (tmp_feature->loadRecord(tmp_results[0][0]) == NOERR) {
                            tmp_return = insertChildren(position, count, tmp_feature);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
    }

    return tmp_return;
}



bool FeatureHierarchy::insertChildren(int in_index, int /*count*/, Record *in_feature)
{
    Feature *tmp_feature = NULL;
    FeatureContent* tmp_feature_content = NULL;

    int tmp_save_result = NOERR;

    if (in_feature == NULL) {
        tmp_feature_content = new FeatureContent(getEntity()->projectVersion());
        tmp_feature_content->setValueForKey("", FEATURES_CONTENTS_TABLE_SHORT_NAME);
        tmp_save_result = tmp_feature_content->saveRecord();
        if (tmp_save_result == NOERR) {
            tmp_feature = new Feature(getEntity()->projectVersion());
            tmp_feature->setDataFromFeatureContent(tmp_feature_content);
        }
        delete tmp_feature_content;
    }
    else
        tmp_feature = dynamic_cast<Feature*>(in_feature);

    if (tmp_save_result == NOERR) {
        tmp_feature->setProjectVersion(getEntity()->projectVersion());
        getEntity()->insertChild(in_index, tmp_feature);
        return getEntity()->saveChilds() == NOERR;
    }

    return false;
}


bool FeatureHierarchy::removeChildren(int in_index, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        getEntity()->removeChildAtIndex(in_index);
    else
        getEntity()->detachChildAtIndex(in_index);

    return getEntity()->saveChilds() == NOERR;
}


bool FeatureHierarchy::isWritable()
{
    return getEntity()->projectVersion()->canWriteFeatures();
}

QVariant FeatureHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role) {
        /* Icone */
    case Qt::DecorationRole:
        switch (getEntity()->lockRecordStatus()) {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/feature.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/feature.png")));
            break;

        case RECORD_STATUS_BROKEN:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/feature.png")));
            break;

        default:
            if (childCount() > 0)
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/features.png")));
            else
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/feature.png")));
        }

        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(getEntity()->getValueForKey(FEATURES_HIERARCHY_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }

    return QVariant();

}


Hierarchy* FeatureHierarchy::createChildHierarchy(Feature* child)
{
    return new FeatureHierarchy(child);
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> FeatureHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::PrintFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SaveFeatureAsHtml;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveFeature;

    return actionsTypesList;
}


Qt::DropActions FeatureHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch (in_record->getEntityDef()->m_entity_signature_id) {
    case FEATURES_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID)) == 0) {
            if (compare_values(getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(FEATURES_HIERARCHY_VERSION)) == 0) {
                return Qt::CopyAction | Qt::MoveAction;
            }
            else {
                return Qt::CopyAction;
            }
        }
        else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}


void FeatureHierarchy::userSelectItem()
{
    Gui::Services::Features::showFeatureInfos(getEntity());
}


bool FeatureHierarchy::canMove()
{
    return getEntity()->projectVersion()->canWriteFeatures();
}

}
