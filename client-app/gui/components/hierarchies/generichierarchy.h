#ifndef GENERICHIERARCHY_H
#define GENERICHIERARCHY_H

#include "filterhierarchy.h"

#include <QPixmap>
#include <QIcon>


namespace Hierarchies
{

template < class T>
class GenericHierarchy :
        public FilterHierarchy
{
public:
    GenericHierarchy(Hierarchy* parent, const QString& label, const QString& icon, const QList<T*> &list) :
        FilterHierarchy(parent, label, icon),
        mList(list)
    {
    }


    int childCount()
    {
        return mList.count();
    }


    Hierarchy* child(int number)
    {
        return mList[number];
    }



private:
    const QList<T*>& mList;
};

}
#endif // GENERICHIERARCHY_H
