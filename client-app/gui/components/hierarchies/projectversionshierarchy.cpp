#include "projectversionshierarchy.h"
#include "projectversionhierarchy.h"

#include "session.h"
#include "gui/services/projectsservices.h"

#include <QIcon>

namespace Hierarchies
{


ProjectVersionsHierarchy::ProjectVersionsHierarchy(Project *project) : EntityNode(project)
{
    Project::ProjectVersionRelation::instance().addParentalityListener(project, this);
}


ProjectVersionsHierarchy::~ProjectVersionsHierarchy()
{
    Project::ProjectVersionRelation::instance().removeParentalityListener(getEntity(), this);
}

Hierarchy* ProjectVersionsHierarchy::createChildHierarchy(ProjectVersion* child)
{
    return new ProjectVersionHierarchy(child);
}


QVariant ProjectVersionsHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/version.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Versions");
        break;

    default:
        return QVariant();

    }

    return QVariant();

}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectVersionsHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    if(isWritable())
    {
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertProjectVersion;
    }

    return actionsTypesList;
}


bool ProjectVersionsHierarchy::isWritable()
{
    return Session::instance().currentUserRoles().contains("admin_role");
}


bool ProjectVersionsHierarchy::removeChildren(int position, int /*count*/, bool in_move_indic)
{
    // user cannot move project versions
    if (in_move_indic)
        return false;

    int tmp_save_result = NOERR;

    Project* project = getEntity();
    if (project){
        ProjectVersion* projectVersion = project->getProjectVersionAtIndex(position);
        project->removeProjectVersion(projectVersion);

        return project->saveProjectVersions() == NOERR;
    }

    return tmp_save_result == NOERR;
}


}


