#ifndef FEATUREHIERARCHY_H
#define FEATUREHIERARCHY_H

#include "entities/feature.h"
#include "hierarchy.h"

namespace Hierarchies
{

class FeatureHierarchy :
    public EntityNode<Feature::ChildRelation>
{
public:
    FeatureHierarchy(Feature* in_feature);
    Hierarchy* createChildHierarchy(Feature* child);

    bool insertChildren(int in_index, int, Record *in_need = NULL);
    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool removeChildren(int in_index, int count, bool in_move_indic = true);

    bool isWritable();
    bool canMove();

    QVariant data(int column, int role = 0);

    virtual QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();
    virtual Qt::DropActions possibleDropActionsForRecord(Record* in_record);

    virtual void userSelectItem();

};

}

#endif // FEATUREHIERARCHY_H
