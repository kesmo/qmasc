#include "testcampaignhierarchy.h"
#include "testhierarchy.h"

#include <QFont>
#include <QIcon>

namespace Hierarchies
{


TestCampaignHierarchy::TestCampaignHierarchy(TestCampaign *testCampaign) : Hierarchy(testCampaign)
{
    Test::TestCampaignRelation::instance().addParentalityListener(Test::TestCampaignRelation::instance().getParent(testCampaign), this);
}

TestCampaignHierarchy::~TestCampaignHierarchy()
{
    Test::TestCampaignRelation::instance().removeParentalityListener(Test::TestCampaignRelation::instance().getParent(testCampaign()), this);
}

QVariant TestCampaignHierarchy::data(int /*column*/, int role)
{
    Test *tmp_test = Test::TestCampaignRelation::instance().getParent(testCampaign());
    if (tmp_test != NULL)
    {
        QFont   tmp_font;

        switch (role)
        {
        /* Icone */
        case Qt::DecorationRole:
            switch (tmp_test->lockRecordStatus())
            {
            case RECORD_STATUS_OUT_OF_SYNC:
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                break;

            case RECORD_STATUS_LOCKED:
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                break;

            case RECORD_STATUS_BROKEN:
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                break;

            default:
                if (tmp_test->isAutomatedGuiTest() || tmp_test->isAutomatedBatchTest())
                {
                    return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                }
                else
                {
                    if (childCount() > 0)
                        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenarios.png")));
                    else
                        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                }
            }

            break;

            // Renvoie le style de police de caracteres du item courant
        case Qt::FontRole:
            if (isALink())
                tmp_font.setItalic(true);

            return QVariant(tmp_font);
            break;

            // Renvoie le titre du item courant
        case Qt::ToolTipRole:
        case Qt::DisplayRole:
        case Qt::EditRole:
            if (tmp_test->original() != NULL)
                return QVariant(QString(tmp_test->original()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
            else
                return QVariant(QString(tmp_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
            break;

        default:
            return QVariant();

        }
    }


    return QVariant();
}



Hierarchy* TestCampaignHierarchy::child(int number)
{
    Test *tmp_test = Test::TestCampaignRelation::instance().getParent(testCampaign());
    Test* tmpTest = tmp_test->getAllChilds()[number];

    Hierarchy* testCampaignHierarchy = getChildHierarchyForRecord(tmpTest);
    if (testCampaignHierarchy == NULL){
        testCampaignHierarchy = new SubTestCampaignHierarchy(tmpTest);
        addChildHierarchyForRecord(testCampaignHierarchy, tmpTest);
    }

    return testCampaignHierarchy;
}


int TestCampaignHierarchy::childCount()
{
    Test *tmp_test = Test::TestCampaignRelation::instance().getParent(testCampaign());
    if (tmp_test)
        return tmp_test->getAllChilds().count();

    return 0;
}


bool TestCampaignHierarchy::canMove(){
    return true;
}


bool TestCampaignHierarchy::isWritable(){
    return true;
}

bool TestCampaignHierarchy::isALink()
{
    Test *tmp_test = Test::TestCampaignRelation::instance().getParent(testCampaign());
    if (tmp_test)
        return tmp_test->original() != NULL;

    return false;
}


SubTestCampaignHierarchy::SubTestCampaignHierarchy(Test *test) : EntityNode(test)
{
    test->addChildParentalityListener(this);
}


SubTestCampaignHierarchy::~SubTestCampaignHierarchy()
{
    getEntity()->removeChildParentalityListener(this);
}

Hierarchy* SubTestCampaignHierarchy::createChildHierarchy(Test* child)
{
    return new SubTestCampaignHierarchy(child);
}


QVariant SubTestCampaignHierarchy::data(int /*column*/, int role)
{
    if (getEntity() != NULL)
    {
        QFont   tmp_font;

        switch (role)
        {
        /* Icone */
        case Qt::DecorationRole:
            switch (getEntity()->lockRecordStatus())
            {
            case RECORD_STATUS_OUT_OF_SYNC:
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                break;

            case RECORD_STATUS_LOCKED:
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                break;

            case RECORD_STATUS_BROKEN:
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                break;

            default:
                if (getEntity()->isAutomatedGuiTest() || getEntity()->isAutomatedBatchTest())
                {
                    return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                }
                else
                {
                    if (childCount() > 0)
                        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenarios.png")));
                    else
                        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
                }
            }

            break;

            // Renvoie le style de police de caracteres du item courant
        case Qt::FontRole:
            if (isALink())
                tmp_font.setItalic(true);

            return QVariant(tmp_font);
            break;

            // Renvoie le titre du item courant
        case Qt::ToolTipRole:
        case Qt::DisplayRole:
        case Qt::EditRole:
            if (getEntity()->original() != NULL)
                return QVariant(QString(getEntity()->original()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
            else
                return QVariant(QString(getEntity()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
            break;

        default:
            return QVariant();

        }
    }


    return QVariant();
}


bool SubTestCampaignHierarchy::isChildOfLinkTestHierarchy()
{
    Hierarchy* parentHierachy = parent();
    if (parentHierachy && parentHierachy->getRecord()
            && parentHierachy->getRecord()->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID){
        return (parentHierachy->isALink());
    }

    return false;
}

bool SubTestCampaignHierarchy::isALink()
{
    return getEntity()->original() != NULL || isChildOfLinkTestHierarchy();
}

}
