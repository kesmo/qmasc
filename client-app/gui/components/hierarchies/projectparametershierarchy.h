#ifndef PROJECTPARAMETERSHIERARCHY_H
#define PROJECTPARAMETERSHIERARCHY_H

#include "nochildrenhierarchy.h"

#include "entities/projectversion.h"

namespace Hierarchies
{


class ProjectParametersHierarchy :
        public NoChildrenHierarchy
{
public:
    ProjectParametersHierarchy(ProjectVersion* projectVersion, QObject *parent = NULL);

    ProjectVersion* projectVersion() const { return dynamic_cast<ProjectVersion*>(getRecord());}

    virtual void userSelectItem();

};

}
#endif // PROJECTPARAMETERSHIERARCHY_H
