#include "userprojectgrantshierarchy.h"
#include "projectgranthierarchy.h"

namespace Hierarchies
{

UserProjectGrantsHierarchy::UserProjectGrantsHierarchy(Project* project) : Hierarchy(project)
{
}


QVariant UserProjectGrantsHierarchy::data(int column, int in_role)
{
    if (column == 0){
        switch (in_role)
        {
        /* Icone */
        case Qt::DecorationRole:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/project.png")));
            break;

            // Renvoie le titre du item courant
        case Qt::ToolTipRole:
        case Qt::DisplayRole:
        case Qt::EditRole:
            return QVariant(QString(getRecord()->getValueForKey(PROJECTS_TABLE_SHORT_NAME)));
            break;

        default:
            return QVariant();

        }
    }

    return QVariant();
}

QList<ProjectGrant*> UserProjectGrantsHierarchy::userGrants() const
{
    QList<ProjectGrant*> tmp_projects_grants;
    Project *project(dynamic_cast<Project*>(getRecord()));
    if (project) {
        tmp_projects_grants  = Project::ProjectGrantRelation::instance().getChildrenWithValueForKey(
            project,
            project->usernameFilter().toStdString().c_str(),
            PROJECTS_GRANTS_TABLE_USERNAME);
    }
    return tmp_projects_grants;
}


int UserProjectGrantsHierarchy::childCount()
{
    return userGrants().count();
}


Hierarchy *UserProjectGrantsHierarchy::child(int number)
{
    ProjectGrant* projectGrant = userGrants().at(number);

    Hierarchy* tmpProjectGrantHierarchy = getChildHierarchyForRecord(projectGrant);
    if (tmpProjectGrantHierarchy == NULL) {
        tmpProjectGrantHierarchy = new ProjectGrantHierarchy(projectGrant);
        addChildHierarchyForRecord(tmpProjectGrantHierarchy, projectGrant);
    }

    return tmpProjectGrantHierarchy;
}

}
