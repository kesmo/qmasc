#include "executioncampaignhierarchy.h"
#include "entities/projectparameter.h"
#include "entities/projectversion.h"
#include "entities/executiontest.h"
#include "entities/executioncampaignparameter.h"
#include "executiontesthierarchy.h"

namespace Hierarchies
{

ExecutionCampaignHierarchy::ExecutionCampaignHierarchy(ExecutionCampaign *executionCampaign, QObject *parent /*= NULL*/) : EntityNode(executionCampaign, parent)
{
}



Hierarchy* ExecutionCampaignHierarchy::createChildHierarchy(ExecutionTest* child){
    return new ExecutionTestHierarchy(child);
}

}
