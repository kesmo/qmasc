#include "projectcampaignshierarchy.h"

#include "campaignhierarchy.h"

#include <QIcon>

namespace Hierarchies
{

ProjectCampaignsHierarchy::ProjectCampaignsHierarchy(ProjectVersion *projectVersion) : EntityNode(projectVersion)
{
    ProjectVersion::CampaignRelation::instance().addParentalityListener(projectVersion, this);
}

Hierarchy* ProjectCampaignsHierarchy::createChildHierarchy(Campaign* child)
{
    return new CampaignHierarchy(child);
}

QVariant ProjectCampaignsHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/campaigns.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Campagnes");
        break;

    default:
        return QVariant();

    }

    return QVariant();

}


bool ProjectCampaignsHierarchy::insertChildren(int /*position*/, int /*count*/, Record *in_child)
{
    ProjectVersion::CampaignRelation::instance().addChild(getEntity(), dynamic_cast<Campaign*>(in_child));
    return ProjectVersion::CampaignRelation::instance().saveChilds(getEntity()) == NOERR;
}

bool ProjectCampaignsHierarchy::removeChildren(int position, int /*count*/, bool /*in_move_indic*/)
{
    int tmp_result = NOERR;
    Campaign* tmpCampaign = getEntity()->campaignsList()[position];
    if (tmpCampaign){
        ProjectVersion::CampaignRelation::instance().removeChild(getEntity(), tmpCampaign);
        return ProjectVersion::CampaignRelation::instance().saveChilds(getEntity());
    }

    return (tmp_result == NOERR);
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectCampaignsHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertCampaign;

    return actionsTypesList;
}

}
