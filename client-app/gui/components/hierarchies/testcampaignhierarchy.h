#ifndef TESTCAMPAIGNHIERARCHY_H
#define TESTCAMPAIGNHIERARCHY_H

#include "hierarchy.h"

#include "entities/test.h"

namespace Hierarchies
{


class TestCampaignHierarchy :
    public Hierarchy
{
public:
    TestCampaignHierarchy(TestCampaign* testCampaign);
    ~TestCampaignHierarchy();

    TestCampaign* testCampaign() const {return dynamic_cast<TestCampaign*>(getRecord());}

    QVariant data(int column, int role = 0);

    Hierarchy* child(int number);
    int childCount();

    bool canMove();
    bool isWritable();

    bool isALink();

};

class SubTestCampaignHierarchy :
    public EntityNode<Test::ChildRelation>
{
public:
    SubTestCampaignHierarchy(Test* test);
    ~SubTestCampaignHierarchy();

    Hierarchy* createChildHierarchy(Test* child);

    QVariant data(int column, int role = 0);

    bool isChildOfLinkTestHierarchy();
    bool isALink();

};

}

#endif // TESTCAMPAIGNHIERARCHY_H
