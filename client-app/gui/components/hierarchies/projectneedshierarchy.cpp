#include "projectneedshierarchy.h"
#include "needhierarchy.h"
#include "session.h"

#include <QIcon>

namespace Hierarchies
{


ProjectNeedsHierarchy::ProjectNeedsHierarchy(Project *project) : EntityNode(project)
{
}

Hierarchy* ProjectNeedsHierarchy::createChildHierarchy(Need* child)
{
    return new NeedHierarchy(child);
}

bool ProjectNeedsHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Need *tmp_need = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool                tmp_return = false;

    if (in_item != NULL)
    {
        net_session_print_query(tmp_session, "select create_need_from_need(%s, %s, %s, %s, NULL);",
                getEntity()->getIdentifier(),
                "NULL",
                (position > 0 ? getEntity()->needsHierarchy()[position -1]->getIdentifier() : "NULL"),
                in_item->getIdentifier());
        tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL)
        {
            if (tmp_rows_count == 1 && tmp_columns_count == 1)
            {
                if (is_empty_string(tmp_results[0][0]) == FALSE)
                {
                    tmp_need = new Need(getEntity());
                    if (tmp_need->loadRecord(tmp_results[0][0]) == NOERR)
                    {
                        tmp_return = insertChildren(position, count, tmp_need);
                    }
                }
            }

            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_return;
}



bool ProjectNeedsHierarchy::insertChildren(int position, int /*count*/, Record *in_child)
{
    Need* tmp_child_need = NULL;

    if (in_child == NULL)
    {
        tmp_child_need = new Need(getEntity());
        Project::NeedRelation::instance().insertChild(getEntity(), position, tmp_child_need);
        return Project::NeedRelation::instance().saveChilds(getEntity()) == NOERR;
    }
    else if (in_child->getEntityDefSignatureId() == NEEDS_TABLE_SIG_ID)
    {
        Project::NeedRelation::instance().insertChild(getEntity(), position, dynamic_cast<Need*>(in_child));
        return Project::NeedRelation::instance().saveChilds(getEntity()) == NOERR;
    }

    return false;
}


bool ProjectNeedsHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        Project::NeedRelation::instance().removeChildAtIndex(getEntity(), position, count, in_move_indic);
    else
        Project::NeedRelation::instance().detachChildAtIndex(getEntity(), position);

    return Project::NeedRelation::instance().saveChilds(getEntity()) == NOERR;
}

bool ProjectNeedsHierarchy::isWritable()
{
    return getEntity()->canWriteNeeds();
}


QVariant ProjectNeedsHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/needs.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Recueil des besoins");
        break;

    default:
        return QVariant();

    }

    return QVariant();

}

QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectNeedsHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportNeed;

    return actionsTypesList;
}


Qt::DropActions ProjectNeedsHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case NEEDS_TABLE_SIG_ID:
        if (compare_values(getRecord()->getValueForKey(PROJECTS_TABLE_PROJECT_ID), in_record->getValueForKey(NEEDS_TABLE_PROJECT_ID)) == 0){
            return Qt::CopyAction | Qt::MoveAction;
        }else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}


}
