#include "nochildrenhierarchy.h"

namespace Hierarchies
{


NoChildrenHierarchy::NoChildrenHierarchy(const QString& label, const QIcon& icon, QObject *parent /*= NULL*/)
    : Hierarchy(parent),
      m_label(label),
      m_icon(icon)
{

}

NoChildrenHierarchy::NoChildrenHierarchy(const QString& label, const QIcon& icon, Record* record, QObject *parent /*= NULL*/)
    : Hierarchy(record, parent),
      m_label(label),
      m_icon(icon)
{

}

QVariant NoChildrenHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return m_icon;
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return m_label;
        break;

    default:
        return QVariant();

    }

    return QVariant();

}

}
