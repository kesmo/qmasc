#include "projectshierarchy.h"
#include "projecthierarchy.h"

#include "session.h"


ProjectsHierarchy::ProjectsHierarchy() :
    m_projects_loaded(false)
{
}


ProjectsHierarchy::~ProjectsHierarchy()
{
}


const QList<Project*>& ProjectsHierarchy::projects()
{
    if (!m_projects_loaded)
    {
        net_session_print_query(Session::instance().getClientSession(), "%s IN (SELECT %s FROM %s WHERE %s.%s=%s.%s AND %s='%s')",
                PROJECTS_TABLE_PROJECT_ID,
                PROJECTS_GRANTS_TABLE_PROJECT_ID,
                PROJECTS_GRANTS_TABLE_SIG,
                PROJECTS_GRANTS_TABLE_SIG,
                PROJECTS_GRANTS_TABLE_PROJECT_ID,
                PROJECTS_TABLE_SIG,
                PROJECTS_TABLE_PROJECT_ID,
                PROJECTS_GRANTS_TABLE_USERNAME,
                cl_current_user(Session::instance().getClientSession())
                );

        m_projects_list = Project::loadRecordsList(Session::instance().getClientSession()->m_last_query, PROJECTS_TABLE_SHORT_NAME);
        m_projects_loaded = true;
    }

    return m_projects_list;
}


Hierarchy* ProjectsHierarchy::child(int number)
{
    Project* project = projects().value(number);

    Hierarchy* tmpProjectHierarchy = getChildHierarchyForRecord(project);
    if (tmpProjectHierarchy == NULL){
        tmpProjectHierarchy = new ProjectHierarchy(project);
        addChildHierarchyForRecord(tmpProjectHierarchy, project);
    }

    return tmpProjectHierarchy;
}



int ProjectsHierarchy::childCount()
{
    int count = projects().count();

    return count;
}



/**
  Renvoie la valeur de la colonne <i>column</i>
**/
QVariant ProjectsHierarchy::data(int /*column*/, int /*in_role*/)
{

    return QVariant();
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectsHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    if(isWritable())
    {
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertProject;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportProject;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ShowProjectsReportsAction;
    }

    return actionsTypesList;
}


void ProjectsHierarchy::addProject(Project* project)
{
    if (project && !m_projects_list.contains(project)){
        m_projects_list.append(project);
        emit childrenChanged(this, 0, 0);
    }
}


void ProjectsHierarchy::removeProject(Project* project)
{
    m_projects_list.removeAll(project);
    emit childrenChanged(this, 0, 0);
}


bool ProjectsHierarchy::isWritable()
{
    return Session::instance().currentUserRoles().contains("admin_role");
}


bool ProjectsHierarchy::removeChildren(int position, int /*count*/, bool /*in_move_indic*/)
{

    int     tmp_save_result = NOERR;

    if (position >= 0 && position < m_projects_list.count())
    {
        Project* project = m_projects_list.takeAt(position);
        if (project){
            tmp_save_result = project->deleteRecord();
            if (tmp_save_result == NOERR)
                delete project;
        }
    }

    return tmp_save_result == NOERR;

}

