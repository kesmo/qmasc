#ifndef NEEDHIERARCHY_H
#define NEEDHIERARCHY_H

#include "entities/need.h"
#include "hierarchy.h"

namespace Hierarchies
{

class NeedHierarchy :
    public EntityNode<Need::ChildRelation>
{
public:
    NeedHierarchy(Need* need);

    Hierarchy* createChildHierarchy(Need* child);

    bool insertChildren(int in_index, int count, Record *in_need = NULL);
    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool removeChildren(int in_index, int count, bool in_move_indic = true);

    bool isWritable();

    QVariant data(int column, int in_role = 0);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    Qt::DropActions possibleDropActionsForRecord(Record* in_record);

    virtual void userSelectItem();
    bool canMove();

};

}
#endif // NEEDHIERARCHY_H
