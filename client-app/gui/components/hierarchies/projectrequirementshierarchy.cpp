#include "projectrequirementshierarchy.h"

#include "session.h"

#include "requirementhierarchy.h"
#include "entities/requirementcontent.h"
#include "entities/project.h"

#include <QIcon>

namespace Hierarchies
{


ProjectRequirementsHierarchy::ProjectRequirementsHierarchy(ProjectVersion *projectVersion) : EntityNode(projectVersion)
{
}

Hierarchy* ProjectRequirementsHierarchy::createChildHierarchy(Requirement* child)
{
    return new RequirementHierarchy(child);
}

bool ProjectRequirementsHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Requirement *tmp_requirement = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool tmp_return = false;

    if (in_item != NULL) {
        net_session_print_query(tmp_session, "select create_requirement_from_requirement(%s, '%s', %s, %s, %s, NULL);",
                                getEntity()->project()->getIdentifier(),
                                getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                                "NULL",
                                (position > 0 ? getEntity()->requirementsHierarchy()[position - 1]->getIdentifier() : "NULL"),
                                in_item->getIdentifier());
        tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL) {
            if (tmp_rows_count == 1 && tmp_columns_count == 1) {
                if (is_empty_string(tmp_results[0][0]) == FALSE) {
                    tmp_requirement = new Requirement(getEntity());
                    if (tmp_requirement->loadRecord(tmp_results[0][0]) == NOERR) {
                        tmp_return = insertChildren(position, count, tmp_requirement);
                    }
                }
            }

            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_return;
}


bool ProjectRequirementsHierarchy::insertChildren(int position, int /*count*/, Record *in_child)
{
    Requirement* tmp_child_requirement = NULL;
    RequirementContent* tmp_requirement_content = NULL;

    int tmp_save_result = NOERR;

    if (in_child == NULL) {
        tmp_requirement_content = new RequirementContent();
        tmp_requirement_content->setValueForKey("", REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME);
        ProjectVersion::RequirementContentRelation::instance().setParent(getEntity(), tmp_requirement_content);
        tmp_save_result = tmp_requirement_content->saveRecord();
        if (tmp_save_result == NOERR) {
            tmp_child_requirement = new Requirement(getEntity());
            tmp_child_requirement->setDataFromRequirementContent(tmp_requirement_content);
            ProjectVersion::RequirementRelation::instance().insertChild(getEntity(), position, tmp_child_requirement);
            return ProjectVersion::RequirementRelation::instance().saveChilds(getEntity()) == NOERR;
        }
        delete tmp_requirement_content;
    }
    else if (in_child->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID) {
        ProjectVersion::RequirementRelation::instance().insertChild(getEntity(), position, dynamic_cast<Requirement*>(in_child));
        return ProjectVersion::RequirementRelation::instance().saveChilds(getEntity()) == NOERR;
    }

    return false;
}


bool ProjectRequirementsHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        ProjectVersion::RequirementRelation::instance().removeChildAtIndex(getEntity(), position, count, in_move_indic);
    else
        ProjectVersion::RequirementRelation::instance().detachChildAtIndex(getEntity(), position);

    return ProjectVersion::RequirementRelation::instance().saveChilds(getEntity()) == NOERR;
}


bool ProjectRequirementsHierarchy::isWritable()
{
    return getEntity()->canWriteRequirements();
}


QVariant ProjectRequirementsHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role) {
        /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/requirements_1.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Exigences");
        break;

    default:
        return QVariant();

    }

    return QVariant();
}


int ProjectRequirementsHierarchy::dbChildCount()
{
    char ***tmp_results = NULL;
    unsigned long tmp_rows_count = 0;
    unsigned long tmp_columns_count = 0;
    int tmp_records_count = -1;

    if (is_empty_string(getEntity()->getIdentifier()) == false) {
        net_session_print_query(Session::instance().getClientSession(), "SELECT COUNT(%s) FROM %s WHERE %s=%s AND %s='%s';"
                , REQUIREMENTS_TABLE_REQUIREMENT_ID
                , REQUIREMENTS_TABLE_SIG
                , REQUIREMENTS_TABLE_PROJECT_ID
                , getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID)
                , REQUIREMENTS_TABLE_VERSION
                , getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

        tmp_results = cl_run_sql(Session::instance().getClientSession(), Session::instance().getClientSession()->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL) {
            tmp_records_count = atoi(tmp_results[0][0]);
            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_records_count;
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectRequirementsHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportRequirement;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportRequirement;

    return actionsTypesList;
}


Qt::DropActions ProjectRequirementsHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch (in_record->getEntityDef()->m_entity_signature_id) {
    case REQUIREMENTS_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0) {
            if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION)) == 0) {
                return Qt::CopyAction | Qt::MoveAction;
            }
            else {
                return Qt::CopyAction;
            }
        }
        else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}

}
