#ifndef USERPROJECTGRANTSHIERARCHY_H
#define USERPROJECTGRANTSHIERARCHY_H

#include "hierarchy.h"
#include "entities/project.h"

namespace Hierarchies
{

class UserProjectGrantsHierarchy :
        public Hierarchy
{
private:
    QList<ProjectGrant*> userGrants() const;

public:
    UserProjectGrantsHierarchy(Project* project);

    Project* project() const {return dynamic_cast<Project*>(getRecord());}

    QVariant data(int column, int role = 0);

    Hierarchy* child(int number);
    int childCount();

};

}

#endif // USERPROJECTGRANTSHIERARCHY_H
