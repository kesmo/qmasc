#include "userprojectsgrantshierarchy.h"
#include "userprojectgrantshierarchy.h"

#include "entities/projectgrant.h"

namespace Hierarchies
{

UserProjectsGrantsHierarchy::UserProjectsGrantsHierarchy(const QString& username, QObject *parent /*= NULL*/) :
    Hierarchy(parent)
{
    ProjectGrant* tmp_project_grant = NULL;

    const entity_def*   tmp_entities_array[] = {
        &projects_table_def,
        &tests_hierarchy_def,
        &requirements_hierarchy_def,
        &campaigns_table_def,
        &executions_campaigns_table_def,
        &needs_table_def,
        &features_hierarchy_def,
        &rules_hierarchy_def,
        NULL
    };

    m_projects_list = Project::loadRecordsList();

    // Add non existing project grants
    foreach(Project* tmp_project, m_projects_list)
    {
        tmp_project->setProjectGrantsUsernameFilter(username);
        QList<ProjectGrant*> tmp_projects_grants = Project::ProjectGrantRelation::instance().getChildrenWithValueForKey(tmp_project,
            username.toStdString().c_str(),
            PROJECTS_GRANTS_TABLE_USERNAME);

        for (int tmp_row_index = 0; tmp_entities_array[tmp_row_index] != NULL; ++tmp_row_index)
        {
            tmp_project_grant = ProjectGrant::findEntityWithValueForKey(tmp_projects_grants,
                                                                        QString::number(tmp_entities_array[tmp_row_index]->m_entity_signature_id).toStdString().c_str(),
                                                                        PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

            if (tmp_project_grant == NULL){
                tmp_project_grant = new ProjectGrant();
                tmp_project_grant->setValueForKey(tmp_project->getIdentifier(), PROJECTS_GRANTS_TABLE_PROJECT_ID);
                tmp_project_grant->setValueForKey(username.toStdString().c_str(), PROJECTS_GRANTS_TABLE_USERNAME);
                tmp_project_grant->setValueForKey(QString::number(tmp_entities_array[tmp_row_index]->m_entity_signature_id).toStdString().c_str(), PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);
                tmp_project_grant->setValueForKey(PROJECT_GRANT_NONE, PROJECTS_GRANTS_TABLE_RIGHTS);
                Project::ProjectGrantRelation::instance().addChild(tmp_project, tmp_project_grant);
            }
        }

        Project::ProjectGrantRelation::instance().saveChilds(tmp_project);
    }

}

UserProjectsGrantsHierarchy::~UserProjectsGrantsHierarchy()
{
    qDeleteAll(m_projects_list);
}


QVariant UserProjectsGrantsHierarchy::data(int /*column*/, int /*in_role*/)
{
    return QVariant();
}


int UserProjectsGrantsHierarchy::childCount()
{
    return m_projects_list.count();
}


Hierarchy *UserProjectsGrantsHierarchy::child(int number)
{
    Project* project = m_projects_list.value(number);

    Hierarchy* tmpProjectHierarchy = getChildHierarchyForRecord(project);
    if (tmpProjectHierarchy == NULL){
        tmpProjectHierarchy = new UserProjectGrantsHierarchy(project);
        addChildHierarchyForRecord(tmpProjectHierarchy, project);
    }

    return tmpProjectHierarchy;
}

}
