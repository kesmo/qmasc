/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef REQUIREMENT_HIERARCHY_H
#define REQUIREMENT_HIERARCHY_H

#include "hierarchy.h"
#include "entities/requirement.h"


namespace Hierarchies
{
class RequirementHierarchy :
    public EntityNode<Requirement::ChildRelation>
{

public:
    RequirementHierarchy(Requirement *in_parent);
    ~RequirementHierarchy();

    Hierarchy* createChildHierarchy(Requirement* child);

    // Hierarchy methods
    bool insertChildren(int in_index, int count, Record *in_test = NULL);
    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool removeChildren(int in_index, int count, bool in_move_indic = true);

    bool isWritable();
    bool canMove();

    QVariant data(int column, int role = 0);

    int dbChildCount();

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();
    Qt::DropActions possibleDropActionsForRecord(Record* in_record);

    virtual void userSelectItem();
};
}

#endif // REQUIREMENT_HIERARCHY_H
