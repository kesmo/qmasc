#ifndef PROJECTVERSIONSHIERARCHY_H
#define PROJECTVERSIONSHIERARCHY_H

#include "hierarchy.h"

#include "entities/project.h"

namespace Hierarchies
{


class ProjectVersionsHierarchy :
    public EntityNode<Project::ProjectVersionRelation>
{
public:
    ProjectVersionsHierarchy(Project *project);
    ~ProjectVersionsHierarchy();

    Hierarchy* createChildHierarchy(ProjectVersion* child);

    virtual QVariant data(int column, int role = 0);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    bool isWritable();
    bool removeChildren(int position, int count, bool in_move_indic);

};

}

#endif // PROJECTVERSIONSHIERARCHY_H
