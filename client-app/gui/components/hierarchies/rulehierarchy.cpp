#include "rulehierarchy.h"
#include "entities/project.h"
#include "session.h"

#include "gui/services/rulesservices.h"

namespace Hierarchies
{

RuleHierarchy::RuleHierarchy(Rule* in_rule) : EntityNode(in_rule)
{
}

Hierarchy* RuleHierarchy::createChildHierarchy(Rule* child)
{
    return new RuleHierarchy(child);
}

bool RuleHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Rule         *tmp_rule = NULL;
    net_session            *tmp_session = Session::instance().getClientSession();
    char                ***tmp_results = NULL;

    unsigned long        tmp_rows_count, tmp_columns_count;

    bool                        tmp_return = false;

    if (in_item != NULL)
    {
        if (in_item->getEntityDefSignatureId() == RULES_HIERARCHY_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_rule_from_rule(%s, '%s', %s, %s, %s, NULL);",
                    getEntity()->projectVersion()->project()->getIdentifier(),
                    getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                    getEntity()->getIdentifier(),
                    (position > 0 ? getEntity()->getAllChilds()[position -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_rule = new Rule(getEntity());
                        if (tmp_rule->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(position, count, tmp_rule);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
    }

    return tmp_return;
}



bool RuleHierarchy::insertChildren(int in_index, int /*count*/, Record *in_rule)
{
    Rule        *tmp_rule = NULL;
    Rule        *tmp_next_rule = NULL;
    RuleContent*    tmp_rule_content = NULL;

    int                tmp_save_result = NOERR;

    if (in_rule == NULL)
    {
        tmp_rule_content = new RuleContent();
        tmp_rule_content->setValueForKey("", RULES_CONTENTS_TABLE_SHORT_NAME);
        ProjectVersion::RuleContentRelation::instance().setParent(getEntity()->projectVersion(), tmp_rule_content);
        tmp_save_result = tmp_rule_content->saveRecord();
        if (tmp_save_result == NOERR)
        {
            tmp_rule = new Rule(getEntity()->projectVersion());
            tmp_rule->setDataFromRuleContent(tmp_rule_content);
        }
        delete tmp_rule_content;
    }
    else
        tmp_rule = dynamic_cast<Rule*>(in_rule);

    getEntity()->insertChild(in_index, tmp_rule);
    return getEntity()->saveChilds() == NOERR;

}


bool RuleHierarchy::removeChildren(int in_index, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        getEntity()->removeChildAtIndex(in_index);
    else
        getEntity()->detachChildAtIndex(in_index);

    return getEntity()->saveChilds() == NOERR;
}


bool RuleHierarchy::isWritable()
{
    return getEntity()->projectVersion()->canWriteRules();
}


QVariant RuleHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        switch (getEntity()->lockRecordStatus())
        {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/rule.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/rule.png")));
            break;

        case RECORD_STATUS_BROKEN:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/rule.png")));
            break;

        default:
            if(childCount() > 0)
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/rules.png")));
            else
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/rule.png")));
        }

        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(getEntity()->getValueForKey(RULES_HIERARCHY_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }

    return QVariant();

}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> RuleHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::PrintRule;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SaveRuleAsHtml;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveRule;

    return actionsTypesList;
}


Qt::DropActions RuleHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case RULES_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(RULES_HIERARCHY_PROJECT_ID)) == 0){
            if (compare_values(getEntity()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(RULES_HIERARCHY_VERSION)) == 0){
                return Qt::CopyAction | Qt::MoveAction;
            }else {
                return Qt::CopyAction;
            }
        }else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}

void RuleHierarchy::userSelectItem()
{
    Gui::Services::Rules::showRuleInfos(getEntity());
}

bool RuleHierarchy::canMove()
{
    return getEntity()->projectVersion()->canWriteRules();
}

}
