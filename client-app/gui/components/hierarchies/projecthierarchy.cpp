#include "projecthierarchy.h"
#include "projectshierarchy.h"

#include "needhierarchy.h"
#include "entities/feature.h"
#include "projectversionhierarchy.h"

#include "projectversionshierarchy.h"
#include "projectneedshierarchy.h"

#include "session.h"

#include "gui/services/projectsservices.h"

#include <QIcon>

ProjectHierarchy::ProjectHierarchy(Project* project, QObject *parent /*= NULL*/) : Hierarchy(project, parent)
{
    Hierarchies::ProjectNeedsHierarchy* needs = new Hierarchies::ProjectNeedsHierarchy(project);
    Hierarchies::ProjectVersionsHierarchy* versions = new Hierarchies::ProjectVersionsHierarchy(project);

    needs->setParentHierarchy(this);
    versions->setParentHierarchy(this);

    m_generic_hierarchy_list.append(needs);
    m_generic_hierarchy_list.append(versions);
}


ProjectHierarchy::~ProjectHierarchy()
{
}


QVariant ProjectHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        switch (getRecord()->lockRecordStatus())
        {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/project.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/project.png")));
            break;

        case RECORD_STATUS_BROKEN:
        default:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/project.png")));
        }

        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(getRecord()->getValueForKey(PROJECTS_TABLE_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }

    return QVariant();
}


int ProjectHierarchy::childCount()
{
    return m_generic_hierarchy_list.count();
}


Hierarchy *ProjectHierarchy::child(int number)
{
    return m_generic_hierarchy_list[number];
}

QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ShowProjectPropertiesAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ShowExecutionReportsProjectAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertProjectVersion;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportProject;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveProject;

    return actionsTypesList;
}

void ProjectHierarchy::userSelectItem()
{
    Gui::Services::Projects::showProjectView(project()->getIdentifier());
}
