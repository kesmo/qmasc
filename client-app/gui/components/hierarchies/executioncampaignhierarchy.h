#ifndef EXECUTIONCAMPAIGNHIERARCHY_H
#define EXECUTIONCAMPAIGNHIERARCHY_H

#include "hierarchy.h"

#include "entities/executioncampaign.h"

namespace Hierarchies
{

class ExecutionCampaignHierarchy :
    public EntityNode<ExecutionCampaign::ExecutionTestRelation>
{
public:
    ExecutionCampaignHierarchy(ExecutionCampaign* executionCampaign, QObject *parent = NULL);

    Hierarchy* createChildHierarchy(ExecutionTest* child);
};

}

#endif // EXECUTIONCAMPAIGNHIERARCHY_H
