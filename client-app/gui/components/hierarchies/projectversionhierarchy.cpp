#include "projectversionhierarchy.h"

#include "filterhierarchy.h"

#include "testhierarchy.h"
#include "requirementhierarchy.h"
#include "campaignhierarchy.h"
#include "entities/feature.h"
#include "entities/rule.h"
#include "projectrequirementshierarchy.h"
#include "projecttestshierarchy.h"
#include "projectcampaignshierarchy.h"
#include "projectfeatureshierarchy.h"
#include "projectruleshierarchy.h"
#include "projectparametershierarchy.h"

#include "session.h"

#include <QIcon>

namespace Hierarchies
{


ProjectVersionHierarchy::ProjectVersionHierarchy(ProjectVersion *projectVersion) : Hierarchy(projectVersion)
{
    ProjectFeaturesHierarchy* projectFeaturesHierarchy = new ProjectFeaturesHierarchy(projectVersion);
    ProjectRulesHierarchy* projectRulesHierarchy = new ProjectRulesHierarchy(projectVersion);
    ProjectRequirementsHierarchy* projectRequirementsHierarchy = new ProjectRequirementsHierarchy(projectVersion);
    ProjectTestsHierarchy* projectTestsHierarchy = new ProjectTestsHierarchy(projectVersion);
    ProjectCampaignsHierarchy* projectCampaignsHierarchy = new ProjectCampaignsHierarchy(projectVersion);
    ProjectParametersHierarchy* parameters = new ProjectParametersHierarchy(projectVersion);

    projectFeaturesHierarchy->setParentHierarchy(this);
    projectRulesHierarchy->setParentHierarchy(this);
    projectRequirementsHierarchy->setParentHierarchy(this);
    projectTestsHierarchy->setParentHierarchy(this);
    projectCampaignsHierarchy->setParentHierarchy(this);
    parameters->setParentHierarchy(this);

    m_generic_hierarchy_list.append(projectFeaturesHierarchy);
    m_generic_hierarchy_list.append(projectRulesHierarchy);
    m_generic_hierarchy_list.append(projectRequirementsHierarchy);
    m_generic_hierarchy_list.append(projectTestsHierarchy);
    m_generic_hierarchy_list.append(projectCampaignsHierarchy);
    m_generic_hierarchy_list.append(parameters);
}


ProjectVersionHierarchy::~ProjectVersionHierarchy()
{
}


QVariant ProjectVersionHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/version.png")));

    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(ProjectVersion::formatProjectVersionNumber(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION)));

    case Qt::FontRole:
    {
        if (Session::instance().getDefaultProjectVersionId()== getRecord()->getIdentifier()){
            QFont font;
            font.setBold(true);
            return font;
        }
    }
        break;

    default:
        return QVariant();

    }

    return QVariant();
}



Hierarchy* ProjectVersionHierarchy::child(int number)
{
    return m_generic_hierarchy_list[number];
}


int ProjectVersionHierarchy::childCount()
{
    return m_generic_hierarchy_list.count();
}

QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectVersionHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SetAsDefaultProjectVersionAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ShowProjectVersionPropertiesAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SearchProjectVersionAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ShowProjectVersionBugsAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveProjectVersion;

    return actionsTypesList;
}

}
