#ifndef RULEHIERARCHY_H
#define RULEHIERARCHY_H

#include "hierarchy.h"
#include "entities/rule.h"

namespace Hierarchies
{

class RuleHierarchy :
    public EntityNode<Rule::ChildRelation>
{
public:
    RuleHierarchy(Rule* in_rule);
    Hierarchy* createChildHierarchy(Rule* child);

    bool insertChildren(int in_index, int count, Record *in_rule = NULL);
    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool removeChildren(int in_index, int count, bool in_move_indic = true);

    bool isWritable();
    bool canMove();

    QVariant data(int column, int role = 0);

    virtual QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();
    virtual Qt::DropActions possibleDropActionsForRecord(Record* in_record);

    virtual void userSelectItem();
};

}

#endif // RULEHIERARCHY_H
