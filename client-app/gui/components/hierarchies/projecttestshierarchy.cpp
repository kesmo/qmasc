#include "projecttestshierarchy.h"

#include "session.h"
#include "testhierarchy.h"
#include "entities/testcontent.h"
#include "entities/requirement.h"
#include "entities/project.h"

#include <QIcon>

namespace Hierarchies
{



ProjectTestsHierarchy::ProjectTestsHierarchy(ProjectVersion *projectVersion) : EntityNode(projectVersion)
{
    ProjectVersion::TestRelation::instance().addParentalityListener(projectVersion, this);
}


ProjectTestsHierarchy::~ProjectTestsHierarchy()
{
    ProjectVersion::TestRelation::instance().removeParentalityListener(getEntity(), this);
}

Hierarchy* ProjectTestsHierarchy::createChildHierarchy(Test* child)
{
    return new TestHierarchy(child);
}


bool ProjectTestsHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Test    *tmp_test = NULL;
    net_session        *tmp_session = Session::instance().getClientSession();
    char        ***tmp_results = NULL;

    unsigned long    tmp_rows_count, tmp_columns_count;

    bool                tmp_return = false;

    if (in_item != NULL)
    {
        if (in_item->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_test_from_test(%s, '%s', %s, %s, %s, NULL);",
                    getEntity()->project()->getIdentifier(),
                    getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                    "NULL",
                    (position > 0 ? getEntity()->testsHierarchy()[position -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_test = new Test(getEntity());
                        if (tmp_test->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(position, count, tmp_test);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }

        }
        else if (in_item->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_test_from_requirement(%s, '%s', %s, %s, %s);",
                    getEntity()->project()->getIdentifier(),
                    getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                    "NULL",
                    (position > 0 ? getEntity()->testsHierarchy()[position -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_test = new Test(getEntity());
                        if (tmp_test->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(position, count, tmp_test);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
    }

    return tmp_return;
}


bool ProjectTestsHierarchy::insertChildren(int position, int /*count*/, Record *in_child)
{
    Test*            tmp_child_test = NULL;

    TestContent*        tmp_test_content = NULL;

    int            tmp_save_result = NOERR;

    if (in_child == NULL)
    {
        tmp_test_content = new TestContent();
        tmp_test_content->setValueForKey("", TESTS_CONTENTS_TABLE_SHORT_NAME);
        ProjectVersion::TestContentRelation::instance().setParent(getEntity(), tmp_test_content);
        tmp_save_result = tmp_test_content->saveRecord();
        if (tmp_save_result == NOERR)
        {
            tmp_child_test = new Test(getEntity());
            tmp_child_test->setDataFromTestContent(tmp_test_content);

            ProjectVersion::TestRelation::instance().insertChild(getEntity(), position, tmp_child_test);
            return ProjectVersion::TestRelation::instance().saveChilds(getEntity()) == NOERR;
        }
        delete tmp_test_content;
    }
    else if (in_child->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
    {
        ProjectVersion::TestRelation::instance().insertChild(getEntity(), position, dynamic_cast<Test*>(in_child));
        return ProjectVersion::TestRelation::instance().saveChilds(getEntity()) == NOERR;
    }
    else if (in_child->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID)
    {
        tmp_child_test = new Test();
        if (tmp_child_test->setDataFromRequirement(dynamic_cast<Requirement*>(in_child)) == NOERR)
        {
            ProjectVersion::TestRelation::instance().insertChild(getEntity(), position, tmp_child_test);
            if (ProjectVersion::TestRelation::instance().saveChilds(getEntity()) == NOERR) {
                return tmp_child_test->saveChilds() == NOERR;
            }
        }
    }
    else
        return false;

    return false;

}



bool ProjectTestsHierarchy::insertLinkOfChildren(int in_index, Record * in_item)
{
    int tmp_return = EMPTY_OBJECT;
    Test* tmp_original_test = dynamic_cast<Test*>(in_item);

    if (in_item && tmp_original_test){
        Test* tmp_test = new Test(getEntity());
        tmp_test->setIsALinkOf(tmp_original_test);
        tmp_return = insertChildren(in_index, 1, tmp_test);
    }

    return (tmp_return == NOERR);
}


bool ProjectTestsHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    if (in_move_indic)
        ProjectVersion::TestRelation::instance().detachChildAtIndex(getEntity(), position);
    else
        ProjectVersion::TestRelation::instance().removeChildAtIndex(getEntity(), position, count, in_move_indic);
    return ProjectVersion::TestRelation::instance().saveChilds(getEntity()) == NOERR;
}


bool ProjectTestsHierarchy::isWritable()
{
    return getEntity()->canWriteTests();
}


QVariant ProjectTestsHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenarios.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Scénarios");
        break;

    default:
        return QVariant();

    }

    return QVariant();
}


int ProjectTestsHierarchy::dbChildCount()
{
    char            ***tmp_results = NULL;
    unsigned long    tmp_rows_count = 0;
    unsigned long    tmp_columns_count = 0;
    int                tmp_records_count = -1;

    if (is_empty_string(getEntity()->getIdentifier()) == false)
    {
        net_session_print_query(Session::instance().getClientSession(), "SELECT COUNT(%s) FROM %s WHERE %s=%s AND %s='%s';"
                , TESTS_TABLE_TEST_ID
                , TESTS_TABLE_SIG
                , TESTS_TABLE_PROJECT_ID
                , getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID)
                , TESTS_TABLE_VERSION
                , getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

        tmp_results = cl_run_sql(Session::instance().getClientSession(), Session::instance().getClientSession()->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL)
        {
            tmp_records_count = atoi(tmp_results[0][0]);
            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_records_count;
}

QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectTestsHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildTest;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildAutomatedTest;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandTest;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseTest;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportTest;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportTest;

    return actionsTypesList;
}


Qt::DropActions ProjectTestsHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case TESTS_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(TESTS_HIERARCHY_PROJECT_ID)) == 0){
            if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(TESTS_HIERARCHY_VERSION)) == 0){
                return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
            }else {
                return Qt::CopyAction;
            }
        }else {
            return Qt::CopyAction;
        }


        break;
    case REQUIREMENTS_HIERARCHY_SIG_ID:
        return Qt::CopyAction;
        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}


}
