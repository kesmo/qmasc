#include "filterhierarchy.h"

#include <QPixmap>
#include <QIcon>

namespace Hierarchies
{


FilterHierarchy::FilterHierarchy(Hierarchy *parent, const QString &label, const QString &icon) :
    Hierarchy(),
    mLabel(label),
    mIcon(icon)
{
    setParentHierarchy(parent);
}


bool FilterHierarchy::insertChildren(int position, int count, Record* in_item)
{
    return parent()->insertChildren(position, count, in_item);
}


bool FilterHierarchy::insertCopyOfChildren(int position, int count, Record* in_item)
{
    return parent()->insertCopyOfChildren(position, count, in_item);
}


bool FilterHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    return parent()->removeChildren(position, count, in_move_indic);
}


bool FilterHierarchy::isWritable()
{
    return parent()->isWritable();
}


bool FilterHierarchy::canInsert(int position)
{
    return parent()->canInsert(position);
}


bool FilterHierarchy::canRemove(int position)
{
    return parent()->canRemove(position);
}


bool FilterHierarchy::canMove()
{
    return parent()->canMove();
}


QVariant FilterHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(mIcon));

    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(mLabel));

    default:
        return QVariant();

    }

    return QVariant();
}



int FilterHierarchy::childCount()
{
    int tmp_count = parent()->childCount();

    return tmp_count;
}


Hierarchy *FilterHierarchy::child(int number)
{
    Hierarchy* tmp_child = parent()->child(number);

    return tmp_child;
}

}
