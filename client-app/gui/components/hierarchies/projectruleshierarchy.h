#ifndef PROJECTRULESHIERARCHY_H
#define PROJECTRULESHIERARCHY_H

#include "hierarchy.h"

#include "entities/projectversion.h"

namespace Hierarchies
{


class ProjectRulesHierarchy :
    public EntityNode<ProjectVersion::RuleRelation>
{
public:
    ProjectRulesHierarchy(ProjectVersion* projectVersion);
    Hierarchy* createChildHierarchy(Rule* child);

    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool insertChildren(int position, int count, Record *in_child = NULL);
    bool removeChildren(int position, int count, bool in_move_indic = true);

    bool isWritable();

    QVariant data(int column, int role = 0);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    Qt::DropActions possibleDropActionsForRecord(Record* in_record);

};

}
#endif // PROJECTRULESHIERARCHY_H
