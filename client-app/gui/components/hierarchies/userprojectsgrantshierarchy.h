#ifndef USERPROJECTSGRANTSHIERARCHY_H
#define USERPROJECTSGRANTSHIERARCHY_H

#include "hierarchy.h"
#include "entities/project.h"

namespace Hierarchies
{

class UserProjectsGrantsHierarchy :
        public Hierarchy
{
public:
    UserProjectsGrantsHierarchy(const QString &username, QObject *parent = NULL);
    ~UserProjectsGrantsHierarchy();

    QVariant data(int column, int role = 0);

    Hierarchy* child(int number);
    int childCount();

private:
    QList<Project*> m_projects_list;
};

}

#endif // USERPROJECTSGRANTSHIERARCHY_H
