/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "hierarchy.h"
#include "session.h"
#include <QList>

Hierarchy::Hierarchy(QObject *parent /*= NULL*/) :
    QObject(parent),
    m_record(NULL),
    m_parent(NULL)
{
}


Hierarchy::Hierarchy(Record *record, QObject *parent /*= NULL*/) :
    QObject(parent),
    m_record(record),
    m_parent(NULL)
{
    if (m_record)
        m_record->addRecordListener(this);
}


Hierarchy::~Hierarchy()
{
    if (m_record)
        m_record->removeRecordListener(this);

    foreach(Hierarchy *child, m_childs) {
        child->setParentHierarchy(nullptr);
    }

    m_childs.clear();
}


void Hierarchy::addChildHierarchyForRecord(Hierarchy* child, Record* record)
{
    child->setParentHierarchy(this);
    m_childs[record] = child;
}

void Hierarchy::removeChildHierarchyForRecord(Record* record)
{
    Hierarchy *child = m_childs.take(record);
    if (child)
        child->setParentHierarchy(nullptr);
}

Hierarchy* Hierarchy::getChildHierarchyForRecord(Record* record) const
{
    return m_childs[record];
}


Record* Hierarchy::getRecord() const
{
    return m_record;
}


void Hierarchy::setParentHierarchy(Hierarchy* parent)
{
    m_parent = parent;
    if (m_parent && !QObject::parent())
        setParent(m_parent);
}


Hierarchy* Hierarchy::parent()
{
    return m_parent;
}


bool Hierarchy::canInsert(int position)
{
    Hierarchy    *tmp_previous_item = NULL;
    Hierarchy    *tmp_next_item = NULL;
    int            tmp_lock_previous_record_status = RECORD_STATUS_MODIFIABLE;
    int            tmp_lock_next_record_status = RECORD_STATUS_MODIFIABLE;
    int            tmp_lock_parent_record_status = RECORD_STATUS_MODIFIABLE;

    const char  *tmp_previous_parent_record_id = NULL;

    if (position < 0 || position > childCount())
        return false;

    if (isWritable() == false)
        return false;

    if (position > 0) {
        /* Dernier element de la liste */
        if (position == childCount()) {
            tmp_previous_item = child(position - 1);
            if (tmp_previous_item != NULL && tmp_previous_item->m_record && is_empty_string(tmp_previous_item->m_record->getIdentifier()) == false) {
                char            ***tmp_records = NULL;
                unsigned long    tmp_rows_count = 0;
                unsigned long    tmp_columns_count = 0;
                const char      *tmp_parent_key = tmp_previous_item->columnNameForParentItem();

                tmp_lock_previous_record_status = tmp_previous_item->m_record->lockRecordStatus();
                if (tmp_previous_item->m_record->lockRecord(true) != NOERR)
                    return false;

                /* Verifier qu'il n'existe pas un nouvel element a la suite */
                if (is_empty_string(tmp_parent_key) == FALSE) {
                    tmp_previous_parent_record_id = tmp_previous_item->m_record->getValueForKey(tmp_parent_key);
                    net_session_print_query(Session::instance().getClientSession(), "SELECT %s FROM %s WHERE %s=%s AND %s=%s;"
                                            , tmp_previous_item->m_record->getEntityDef()->m_entity_columns_names[0]
                                            , tmp_previous_item->m_record->getEntityDef()->m_entity_name
                                            , tmp_previous_item->columnNameForPreviousItem()
                                            , tmp_previous_item->m_record->getIdentifier()
                                            , tmp_parent_key
                                            , is_empty_string(tmp_previous_parent_record_id) ? "NULL" : tmp_previous_parent_record_id);
                }
                else {
                    net_session_print_query(Session::instance().getClientSession(), "SELECT %s FROM %s WHERE %s=%s;"
                                            , tmp_previous_item->m_record->getEntityDef()->m_entity_columns_names[0]
                                            , tmp_previous_item->m_record->getEntityDef()->m_entity_name
                                            , tmp_previous_item->columnNameForPreviousItem()
                                            , tmp_previous_item->m_record->getIdentifier());
                }

                tmp_records = cl_run_sql(Session::instance().getClientSession(), Session::instance().getClientSession()->m_last_query, &tmp_rows_count, &tmp_columns_count);
                if (tmp_records != NULL) {
                    cl_free_rows_columns_array(&tmp_records, tmp_rows_count, tmp_columns_count);
                    if (tmp_rows_count > 0) {
                        if (tmp_lock_previous_record_status != RECORD_STATUS_OWN_LOCK)
                            tmp_previous_item->m_record->unlockRecord();

                        return false;
                    }
                }
            }
        }
    }

    /* Traitement de l'element suivant */
    if (position < childCount()) {
        tmp_next_item = child(position);
        if (tmp_next_item != NULL && tmp_next_item->m_record) {
            tmp_lock_next_record_status = tmp_next_item->m_record->lockRecordStatus();
            if (tmp_next_item->m_record->lockRecord(true) != NOERR) {
                if (tmp_previous_item != NULL && tmp_previous_item->m_record && tmp_lock_previous_record_status != RECORD_STATUS_OWN_LOCK)
                    tmp_previous_item->m_record->unlockRecord();

                return false;
            }
        }
    }

    if (childCount() == 0 && m_record) {
        tmp_lock_parent_record_status = m_record->lockRecordStatus();
        if (m_record->lockRecord(true) != NOERR) {
            if (tmp_previous_item != NULL && tmp_previous_item->m_record && tmp_lock_previous_record_status != RECORD_STATUS_OWN_LOCK)
                tmp_previous_item->m_record->unlockRecord();

            if (tmp_next_item != NULL && tmp_next_item->m_record && tmp_lock_next_record_status != RECORD_STATUS_OWN_LOCK)
                tmp_next_item->m_record->unlockRecord();

            return false;
        }

        if (dbChildCount() > 0) {
            if (tmp_previous_item != NULL && tmp_previous_item->m_record && tmp_lock_previous_record_status != RECORD_STATUS_OWN_LOCK)
                tmp_previous_item->m_record->unlockRecord();

            if (tmp_next_item != NULL && tmp_next_item->m_record && tmp_lock_next_record_status != RECORD_STATUS_OWN_LOCK)
                tmp_next_item->m_record->unlockRecord();

            if (tmp_lock_parent_record_status != RECORD_STATUS_OWN_LOCK)
                m_record->unlockRecord();
        }
    }

    return true;
}

bool Hierarchy::canRemove(int position)
{
    Hierarchy     *tmp_item = NULL;
    Hierarchy    *tmp_previous_item = NULL;
    Hierarchy    *tmp_next_item = NULL;
    int            tmp_lock_record_status = RECORD_STATUS_MODIFIABLE;
    int            tmp_lock_previous_record_status = RECORD_STATUS_MODIFIABLE;

    if (position < 0 || position >= childCount())
        return false;

    if (isWritable() == false)
        return false;

    tmp_item = child(position);
    if (tmp_item != NULL && tmp_item->m_record) {
        tmp_lock_record_status = tmp_item->m_record->lockRecordStatus();
        if (tmp_item->m_record->lockRecord(true) != NOERR)
            return false;
    }


    /* Traitement de l'élément suivant */
    if (position + 1 < childCount()) {
        tmp_next_item = child(position + 1);
        if (tmp_next_item != NULL && tmp_next_item->m_record) {
            if (tmp_next_item->m_record->lockRecord(true) != NOERR) {
                if (tmp_item != NULL && tmp_item->m_record && tmp_lock_record_status != RECORD_STATUS_OWN_LOCK)
                    tmp_item->m_record->unlockRecord();

                if (tmp_previous_item != NULL && tmp_previous_item->m_record && tmp_lock_previous_record_status != RECORD_STATUS_OWN_LOCK)
                    tmp_previous_item->m_record->unlockRecord();

                return false;
            }
        }
    }

    return true;
}


bool Hierarchy::isChildOf(Hierarchy *in_item)
{
    Hierarchy    *tmp_parent = parent();

    if (tmp_parent == NULL)
        return false;

    if (tmp_parent == in_item)
        return true;

    return tmp_parent->isChildOf(in_item);
}


bool Hierarchy::isParentOf(Hierarchy *in_item)
{
    Hierarchy    *tmp_child = NULL;
    int            tmp_index = 0;

    for (tmp_index = 0; tmp_index < childCount(); tmp_index++) {
        tmp_child = child(tmp_index);

        if (in_item == tmp_child)
            return true;

        if (tmp_child->isParentOf(in_item))
            return true;
    }

    return false;
}


int Hierarchy::dbChildCount()
{
    return childCount();
}


Hierarchy* Hierarchy::findItemWithValueForKey(const char *in_value, const char *in_key, bool in_recursive)
{
    Hierarchy    *tmp_child = NULL;
    int        tmp_index = 0;

    for (tmp_index = 0; tmp_index < childCount(); tmp_index++) {
        tmp_child = child(tmp_index);

        if (tmp_child->m_record && compare_values(tmp_child->m_record->getValueForKey(in_key), in_value) == 0)
            return tmp_child;

        if (in_recursive) {
            tmp_child = tmp_child->findItemWithValueForKey(in_value, in_key, in_recursive);
            if (tmp_child != NULL)
                return tmp_child;
        }
    }

    return NULL;
}


int Hierarchy::indexForItem(Hierarchy *in_item)
{
    Hierarchy    *tmp_child = NULL;
    int        tmp_index = 0;

    for (tmp_index = 0; tmp_index < childCount(); ++tmp_index) {
        tmp_child = child(tmp_index);
        if (tmp_child == in_item)
            return tmp_index;
    }

    return -1;
}

bool Hierarchy::isALink()
{
    return false;
}


bool Hierarchy::isWritable()
{
    if (getRecord())
        return getRecord()->isModifiable();

    return false;
}


bool Hierarchy::isDataEditable()
{
    if (getRecord())
        return getRecord()->isModifiable();

    return false;
}


bool Hierarchy::canMove()
{
    return false;
}

//! \todo reimplement for each hierarchy kind (project, version, test...etc)
bool Hierarchy::mayPerformChildLookupForRecord(Record* in_record)
{
    if (getRecord() && in_record && !isALink()) {
        switch (getRecord()->getEntityDefSignatureId()) {
        case PROJECTS_TABLE_SIG_ID:
            switch (in_record->getEntityDefSignatureId()) {
            case PROJECTS_VERSIONS_TABLE_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID)) == 0;

            case NEEDS_TABLE_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(NEEDS_TABLE_PROJECT_ID)) == 0;

            case FEATURES_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID)) == 0;

            case RULES_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(RULES_HIERARCHY_PROJECT_ID)) == 0;

            case REQUIREMENTS_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0;

            case CAMPAIGNS_TABLE_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(CAMPAIGNS_TABLE_PROJECT_ID)) == 0;

            case TESTS_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(TESTS_HIERARCHY_PROJECT_ID)) == 0;

            default:
                break;
            }

            break;

        case PROJECTS_VERSIONS_TABLE_SIG_ID:
            switch (in_record->getEntityDefSignatureId()) {

            case FEATURES_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(FEATURES_HIERARCHY_VERSION)) == 0;

            case RULES_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(RULES_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(RULES_HIERARCHY_VERSION)) == 0;

            case REQUIREMENTS_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION)) == 0;

            case CAMPAIGNS_TABLE_SIG_ID:
                return compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(CAMPAIGNS_TABLE_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(CAMPAIGNS_TABLE_PROJECT_VERSION)) == 0;

            case TESTS_HIERARCHY_SIG_ID:
                return compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(TESTS_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(TESTS_HIERARCHY_VERSION)) == 0;

            default:
                break;
            }

            break;

        case NEEDS_TABLE_SIG_ID:
            if (in_record->getEntityDefSignatureId() == NEEDS_TABLE_SIG_ID) {
                return compare_values(getRecord()->getValueForKey(NEEDS_TABLE_PROJECT_ID), in_record->getValueForKey(NEEDS_TABLE_PROJECT_ID)) == 0;
            }

            break;


        case FEATURES_HIERARCHY_SIG_ID:
            if (in_record->getEntityDefSignatureId() == FEATURES_HIERARCHY_SIG_ID) {
                return compare_values(getRecord()->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID), in_record->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(FEATURES_HIERARCHY_VERSION), in_record->getValueForKey(FEATURES_HIERARCHY_VERSION)) == 0;
            }

            break;

        case RULES_HIERARCHY_SIG_ID:
            if (in_record->getEntityDefSignatureId() == RULES_HIERARCHY_SIG_ID) {
                return compare_values(getRecord()->getValueForKey(RULES_HIERARCHY_PROJECT_ID), in_record->getValueForKey(RULES_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(RULES_HIERARCHY_VERSION), in_record->getValueForKey(RULES_HIERARCHY_VERSION)) == 0;
            }

            break;

        case TESTS_HIERARCHY_SIG_ID:
            if (in_record->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID) {
                return compare_values(getRecord()->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION)) == 0;
            }

        case REQUIREMENTS_HIERARCHY_SIG_ID:
            if (in_record->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID) {
                return compare_values(getRecord()->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID)) == 0
                    && compare_values(getRecord()->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION), in_record->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION)) == 0;
            }

        case CAMPAIGNS_TABLE_SIG_ID:
            switch (in_record->getEntityDefSignatureId()) {

            case TESTS_CAMPAIGNS_TABLE_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(TESTS_CAMPAIGNS_TABLE_CAMPAIGN_ID)) == 0;

            case EXECUTIONS_CAMPAIGNS_TABLE_SIG_ID:
                return compare_values(getRecord()->getIdentifier(), in_record->getValueForKey(EXECUTIONS_CAMPAIGNS_TABLE_CAMPAIGN_ID)) == 0;

            }
            break;


        default:
            break;
        }
    }

    return false;
}


Qt::DropActions Hierarchy::possibleDropActionsForRecord(Record* /*in_record*/)
{
    return Qt::IgnoreAction;
}

bool Hierarchy::mayHaveCyclicRedundancy(Hierarchy * /* in_dest_item */, bool /* in_check_link */)
{
    return false;
}


QList<Record*>* Hierarchy::orginalsRecordsForRelationsEntity(Record * /* in_item */)
{
    return NULL;
}


bool Hierarchy::insertCopyOfChildren(int /*position*/, int /*count*/, Record * /*in_item*/)
{

    return false;
}


bool Hierarchy::insertChildren(int /*in_index*/, int /* count */, Record * /*in_child*/)
{
    return false;
}


bool Hierarchy::insertLinkOfChildren(int /*in_index*/, Record * /*in_item*/)
{
    return false;
}


bool Hierarchy::removeChildren(int /*position*/, int /*count*/, bool /*in_move_indic*/)
{
    return false;
}


int Hierarchy::row()
{
    if (parent())
        return parent()->indexForItem(this);

    return -1;
}


void Hierarchy::recordDestroyed(AbstractRecord* record)
{
    if (m_record == record) {
        if (m_parent)
            m_parent->removeChildHierarchyForRecord(m_record);
        m_record = NULL;
        deleteLater();
    }
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> Hierarchy::availableContextMenuActionTypes()
{
    return QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType>();
}


void Hierarchy::updateChilds(int in_old_rows_count, int in_new_rows_count)
{
    emit childrenChanged(this, in_old_rows_count, in_new_rows_count);
}

void Hierarchy::userSelectItem()
{
    // do nothing
}

Gui::Components::Widgets::AbstractColumnRecordWidget* Hierarchy::createCustomWidgetEditorForColumn(int /*column*/)
{
    // do nothing
    return NULL;
}


QList<Hierarchy*> Hierarchy::parentRecordsFromRecordsList(const QList<Hierarchy*>& in_records_list)
{
    QList<Hierarchy*>   tmp_records_list;
    bool                tmp_indic = false;
    QList<Hierarchy*>   tmp_remove_records_list;

    foreach(Hierarchy* tmp_record, in_records_list)
    {
        tmp_indic = false;

        foreach(Hierarchy* tmp_current_record, tmp_records_list)
        {
            if (tmp_record->isChildOf(tmp_current_record)) {
                tmp_indic = true;
                break;
            }

            if (tmp_record->isParentOf(tmp_current_record)) {
                tmp_remove_records_list.append(tmp_current_record);
            }
        }

        if (!tmp_indic) {
            tmp_records_list.append(tmp_record);
        }
    }

    foreach(Hierarchy* tmp_record, tmp_records_list)
    {
        if (tmp_remove_records_list.indexOf(tmp_record) >= 0) {
            tmp_records_list.removeAll(tmp_record);
        }
    }

    return tmp_records_list;
}


bool Hierarchy::applyChanges()
{
    bool tmp_result = true;

    if (m_record)
        tmp_result = m_record->saveRecord() == NOERR;

    for (int index = 0; index < childCount() && tmp_result; ++index) {
        tmp_result = child(index)->applyChanges();
    }

    return tmp_result;
}

void Hierarchy::revertChanges()
{
    if (m_record)
        m_record->revertChanges();

    for (int index = 0; index < childCount(); ++index) {
        child(index)->revertChanges();
    }

}
