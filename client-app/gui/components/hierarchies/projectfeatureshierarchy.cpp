#include "projectfeatureshierarchy.h"
#include "featurehierarchy.h"
#include "entities/feature.h"
#include "entities/featurecontent.h"
#include "entities/project.h"
#include "session.h"

#include <QIcon>

namespace Hierarchies
{


ProjectFeaturesHierarchy::ProjectFeaturesHierarchy(ProjectVersion *projectVersion) : EntityNode(projectVersion)
{
}

Hierarchy* ProjectFeaturesHierarchy::createChildHierarchy(Feature* child)
{
    return new FeatureHierarchy(child);
}

bool ProjectFeaturesHierarchy::insertCopyOfChildren(int position, int count, Record *in_item)
{
    Feature *tmp_feature = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool                tmp_return = false;

    if (in_item != NULL)
    {
        net_session_print_query(tmp_session, "select create_feature_from_feature(%s, '%s', %s, %s, %s, NULL);",
                getEntity()->project()->getIdentifier(),
                getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                "NULL",
                (position > 0 ? getEntity()->featuresHierarchy()[position -1]->getIdentifier() : "NULL"),
                in_item->getIdentifier());
        tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL)
        {
            if (tmp_rows_count == 1 && tmp_columns_count == 1)
            {
                if (is_empty_string(tmp_results[0][0]) == FALSE)
                {
                    tmp_feature = new Feature(getEntity());
                    if (tmp_feature->loadRecord(tmp_results[0][0]) == NOERR)
                    {
                        tmp_return = insertChildren(position, count, tmp_feature);
                    }
                }
            }

            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_return;
}



bool ProjectFeaturesHierarchy::insertChildren(int position, int /*count*/, Record *in_child)
{
    Feature* tmp_child_feature = NULL;
    FeatureContent* tmp_feature_content = NULL;

    int tmp_save_result = NOERR;

    if (in_child == NULL)
    {
        tmp_feature_content = new FeatureContent(getEntity());
        tmp_feature_content->setValueForKey("", FEATURES_CONTENTS_TABLE_SHORT_NAME);
        tmp_save_result = tmp_feature_content->saveRecord();
        if (tmp_save_result == NOERR)
        {
            tmp_child_feature = new Feature(getEntity());
            tmp_child_feature->setDataFromFeatureContent(tmp_feature_content);
            ProjectVersion::FeatureRelation::instance().insertChild(getEntity(), position, tmp_child_feature);
            return ProjectVersion::FeatureRelation::instance().saveChilds(getEntity()) == NOERR;
        }
        delete tmp_feature_content;
    }
    else if (in_child->getEntityDefSignatureId() == FEATURES_HIERARCHY_SIG_ID)
    {
        ProjectVersion::FeatureRelation::instance().insertChild(getEntity(), position, dynamic_cast<Feature*>(in_child));
        return ProjectVersion::FeatureRelation::instance().saveChilds(getEntity()) == NOERR;
    }

    return false;
}


bool ProjectFeaturesHierarchy::removeChildren(int position, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        ProjectVersion::FeatureRelation::instance().removeChildAtIndex(getEntity(), position, count, in_move_indic);
    else
        ProjectVersion::FeatureRelation::instance().detachChildAtIndex(getEntity(), position);

    return ProjectVersion::FeatureRelation::instance().saveChilds(getEntity()) == NOERR;
}


bool ProjectFeaturesHierarchy::isWritable()
{
    return getEntity()->canWriteFeatures();
}


QVariant ProjectFeaturesHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/features.png")));
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant("Fonctionnalités");
        break;

    default:
        return QVariant();

    }

    return QVariant();

}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> ProjectFeaturesHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportFeature;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportFeature;

    return actionsTypesList;
}


Qt::DropActions ProjectFeaturesHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case FEATURES_HIERARCHY_SIG_ID:
        if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID)) == 0){
            if (compare_values(getEntity()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(FEATURES_HIERARCHY_VERSION)) == 0){
                return Qt::CopyAction | Qt::MoveAction;
            }else {
                return Qt::CopyAction;
            }
        }else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}

}
