#ifndef PROJECTVERSIONHIERARCHY_H
#define PROJECTVERSIONHIERARCHY_H

#include "entities/projectversion.h"
#include "hierarchy.h"

namespace Hierarchies
{


class ProjectVersionHierarchy :
        public Hierarchy
{
private:
    QList<Hierarchy*>            m_generic_hierarchy_list;

public:
    ProjectVersionHierarchy(ProjectVersion* projectVersion);
    ~ProjectVersionHierarchy();

    QVariant data(int column, int role = 0);

    Hierarchy* child(int number);
    int childCount();

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

};

}
#endif // PROJECTVERSIONHIERARCHY_H
