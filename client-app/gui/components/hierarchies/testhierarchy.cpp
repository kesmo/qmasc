/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "testhierarchy.h"
#include "entities/testcontent.h"
#include "entities/action.h"
#include "entities/requirement.h"
#include "requirementhierarchy.h"
#include "entities/testrequirement.h"
#include "entities/requirementcontent.h"
#include "utilities.h"
#include "entities/project.h"
#include "entities/projectgrant.h"
#include "session.h"
#include "entities/customtestfield.h"
#include "contextmenus/hierarchycontextmenuaction.h"
#include "contextmenus/testhierarchy/insertaction.h"
#include "contextmenus/testhierarchy/removetestaction.h"
#include "contextmenus/testhierarchy/insertchildtestaction.h"
#include "contextmenus/testhierarchy/importtestsaction.h"
#include "contextmenus/testhierarchy/exporttestsaction.h"
#include "contextmenus/testhierarchy/selectlinktestsaction.h"
#include "contextmenus/testhierarchy/saveashtmltestsaction.h"
#include "contextmenus/testhierarchy/printtestsaction.h"

#include "contextmenus/expandnodeaction.h"
#include "contextmenus/collapsenodeaction.h"

#include "gui/services/testsservices.h"


#include <QFont>
#include <QIcon>
#include <QPixmap>

namespace Hierarchies
{

/**
  Constructeur
**/
TestHierarchy::TestHierarchy(Test* in_record) : Hierarchy(in_record)
{
    in_record->addChildParentalityListener(this);
}


/**
  Destructeur
**/
TestHierarchy::~TestHierarchy()
{
    test()->removeChildParentalityListener(this);
}




/**
  Renvoie le test enfant
**/
Hierarchy *TestHierarchy::child(int number)
{
    Test* tmpTest = test();

    if (tmpTest->original())
        tmpTest = tmpTest->original()->getAllChilds()[number];
    else
        tmpTest = tmpTest->getAllChilds()[number];

    Hierarchy* tmpTestHierarchy = getChildHierarchyForRecord(tmpTest);
    if (tmpTestHierarchy == NULL){
        tmpTestHierarchy = new TestHierarchy(tmpTest);
        addChildHierarchyForRecord(tmpTestHierarchy, tmpTest);
    }

    return tmpTestHierarchy;
}



/**
  Renvoie le nombre de tests enfants
**/
int TestHierarchy::childCount()
{
    Test* tmpTest = test();

    if (tmpTest->original())
        return test()->original()->getAllChilds().count();
    else
        return test()->getAllChilds().count();

}


int TestHierarchy::dbChildCount()
{
    char ***tmp_results = NULL;
    unsigned long tmp_rows_count = 0;
    unsigned long tmp_columns_count = 0;
    int tmp_records_count = -1;

    if (is_empty_string(getRecord()->getIdentifier()) == false)
    {
        /* Verifier qu'il n'existe pas un nouvel element a la suite */
        net_session_print_query(Session::instance().getClientSession(), "SELECT COUNT(%s) FROM %s WHERE %s=%s;"
                , TESTS_TABLE_TEST_ID
                , TESTS_TABLE_SIG
                , TESTS_TABLE_PARENT_TEST_ID
                , getRecord()->getIdentifier());
        tmp_results = cl_run_sql(Session::instance().getClientSession(), Session::instance().getClientSession()->m_last_query, &tmp_rows_count, &tmp_columns_count);
        if (tmp_results != NULL)
        {
            tmp_records_count = atoi(tmp_results[0][0]);
            cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
        }
    }

    return tmp_records_count;
}

/**
  Renvoie la valeur de la colonne <i>column</i>
**/
QVariant TestHierarchy::data(int /*column*/, int in_role)
{
    QFont   tmp_font;

    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        switch (getRecord()->lockRecordStatus())
        {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
            break;

        case RECORD_STATUS_BROKEN:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
            break;

        default:
            if (test()->isAutomatedGuiTest() || test()->isAutomatedBatchTest())
            {
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
            }
            else
            {
                if (childCount() > 0)
                    return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenarios.png")));
                else
                    return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/scenario.png")));
            }
        }

        break;

        // Renvoie le style de police de caracteres du item courant
    case Qt::FontRole:
        if (isALink())
            tmp_font.setItalic(true);

        return QVariant(tmp_font);
        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        if (test()->original() != NULL)
            return QVariant(QString(test()->original()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
        else
            return QVariant(QString(getRecord()->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }


    return QVariant();
}




bool TestHierarchy::insertCopyOfChildren(int in_index, int count, Record *in_item)
{
    Test         *tmp_test = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char ***tmp_results = NULL;
    unsigned long tmp_rows_count, tmp_columns_count;

    bool                tmp_return = false;

    if (in_item != NULL)
    {
        if (in_item->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_test_from_test(%s, '%s', %s, %s, %s, NULL);",
                    test()->projectVersion()->project()->getIdentifier(),
                    test()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                    test()->getIdentifier(),
                    (in_index > 0 ? test()->getAllChilds()[in_index -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_test = new Test(test());
                        if (tmp_test->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(in_index, count, tmp_test);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
        else if (in_item->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_test_from_requirement(%s, '%s', %s, %s, %s);",
                    test()->projectVersion()->project()->getIdentifier(),
                    test()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION),
                    test()->getIdentifier(),
                    (in_index > 0 ? test()->getAllChilds()[in_index -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_test = new Test(test());
                        if (tmp_test->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(in_index, count, tmp_test);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
    }

    return tmp_return;
}

/**
  Insertion d'un nouveau test enfant
**/
bool TestHierarchy::insertChildren(int in_index, int /* count */, Record *in_child)
{
    Test     *tmp_test = NULL;
    Test     *tmp_next_test = NULL;
    int         tmp_save_result = NOERR;
    TestContent* tmp_test_content = NULL;

    if (in_child == NULL)
    {
        tmp_test_content = new TestContent();
        tmp_test_content->setValueForKey("", TESTS_CONTENTS_TABLE_SHORT_NAME);
        ProjectVersion::TestContentRelation::instance().setParent(test()->projectVersion(), tmp_test_content);
        tmp_save_result = tmp_test_content->saveRecord();
        if (tmp_save_result == NOERR)
        {
            tmp_test = new Test(test()->projectVersion());
            tmp_test->setDataFromTestContent(tmp_test_content);
        }
        delete tmp_test_content;
    }
    else
        tmp_test = dynamic_cast<Test*>(in_child);

    test()->insertChild(in_index, tmp_test);
    return test()->saveChilds() == NOERR;

    return false;
}



bool TestHierarchy::insertLinkOfChildren(int in_index, Record * in_item)
{
    int tmp_return = EMPTY_OBJECT;
    Test* tmp_original_test = dynamic_cast<Test*>(in_item);

    if (in_item && tmp_original_test){
        Test* tmp_test = new Test(test());
        tmp_test->setIsALinkOf(tmp_original_test);
        tmp_return = insertChildren(in_index, 1, tmp_test);
    }

    return (tmp_return == NOERR);
}


bool TestHierarchy::isChildOfLinkTestHierarchy()
{
    Hierarchy* parentHierachy = parent();
    if (parentHierachy && parentHierachy->getRecord()
            && parentHierachy->getRecord()->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID){
        return (parentHierachy->isALink());
    }

    return false;
}


bool TestHierarchy::isALink()
{
    return test()->original() != NULL || isChildOfLinkTestHierarchy();
}


bool TestHierarchy::isWritable()
{
    return !isALink() && getRecord()->isModifiable();
}


bool TestHierarchy::canMove()
{
    return !isChildOfLinkTestHierarchy();
}



bool TestHierarchy::mayHaveCyclicRedundancy(Hierarchy *in_dest_item, bool in_check_link)
{
    Test *tmp_parent = NULL;

    if (in_check_link && test()->original() != NULL) return true;

    if (in_dest_item != NULL && in_dest_item->getRecord()->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID)
    {
        tmp_parent = dynamic_cast<Test*>(in_dest_item->getRecord());
        while (tmp_parent != NULL)
        {
            if (test()->original() != NULL && compare_values(tmp_parent->getValueForKey(TESTS_HIERARCHY_TEST_ID), test()->original()->getValueForKey(TESTS_HIERARCHY_TEST_ID)) == 0)
                return true;

            if (compare_values(tmp_parent->getValueForKey(TESTS_HIERARCHY_TEST_ID), getRecord()->getValueForKey(TESTS_HIERARCHY_TEST_ID)) == 0)
                return true;

            if (findItemWithValueForKey(tmp_parent->getValueForKey(TESTS_HIERARCHY_TEST_ID), TESTS_HIERARCHY_ORIGINAL_TEST_ID) != NULL)
                return true;

            tmp_parent = tmp_parent->parentTest();
        }
    }

    return false;
}



/**
  Supprimer un test enfant
**/
bool TestHierarchy::removeChildren(int in_index, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        test()->removeChildAtIndex(in_index);
    else
        test()->detachChildAtIndex(in_index);

    return test()->saveChilds() == NOERR;
}


QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> TestHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    if (!isChildOfLinkTestHierarchy())
    {
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertAutomatedTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildAutomatedTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SelectLinkTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::PrintTest;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SaveTestAsHtml;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveTest;
//        actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::AssociateTestsAndRequirements;
    }
    return actionsTypesList;
}


Qt::DropActions TestHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case TESTS_HIERARCHY_SIG_ID:
        if (compare_values(test()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), in_record->getValueForKey(TESTS_HIERARCHY_PROJECT_ID)) == 0){
            if (compare_values(test()->projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), in_record->getValueForKey(TESTS_HIERARCHY_VERSION)) == 0){
                return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
            }else {
                return Qt::CopyAction;
            }
        }else {
            return Qt::CopyAction;
        }


        break;
    case REQUIREMENTS_HIERARCHY_SIG_ID:
        return Qt::CopyAction;
        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}


void TestHierarchy::userSelectItem()
{
    Gui::Services::Tests::showTestInfos(test());
}


}
