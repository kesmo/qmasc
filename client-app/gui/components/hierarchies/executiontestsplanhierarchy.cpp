#include "executiontestsplanhierarchy.h"
#include "entities/projectparameter.h"
#include "entities/projectversion.h"
#include "entities/executiontest.h"
#include "entities/executioncampaignparameter.h"
#include "executiontesthierarchy.h"

namespace Hierarchies
{

ExecutionTestsPlanHierarchy::ExecutionTestsPlanHierarchy(ExecutionCampaign *executionCampaign) : EntityNode(executionCampaign)
{
}

Hierarchy* ExecutionTestsPlanHierarchy::createChildHierarchy(ExecutionTest* child)
{
    return new ExecutionTestHierarchy(child);
}

}
