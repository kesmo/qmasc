#include "insertprojectaction.h"
#include "gui/services/projectsservices.h"
#include "gui/forms/FormNewProject.h"
#include "gui/mainwindow.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectsHierarchy
{

InsertProjectAction::InsertProjectAction(QObject* parent )
    : AbstractAction(parent)
{
}

bool InsertProjectAction::exec(RecordsTreeView * /*in_record_tree_view*/)
{
    MainWindow::instance()->newProject();

    return false;
}

}
}
}
