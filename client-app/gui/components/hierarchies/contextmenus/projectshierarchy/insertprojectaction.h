#ifndef INSERTPROJECTACTION_H
#define INSERTPROJECTACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectsHierarchy
{

class InsertProjectAction : public AbstractAction
{
public:
    InsertProjectAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

};

}
}
}

#endif // INSERTPROJECTACTION_H
