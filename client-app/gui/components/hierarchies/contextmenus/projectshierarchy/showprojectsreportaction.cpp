#include "showprojectsreportaction.h"
#include "gui/services/projectsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectsHierarchy
{

ShowProjectsReportAction::ShowProjectsReportAction(QObject* parent)
    : AbstractAction(parent)
{
}

bool ShowProjectsReportAction::exec(RecordsTreeView * /*in_record_tree_view*/)
{
    Gui::Services::Projects::showProjectsStats();

    return false;
}

}
}
}
