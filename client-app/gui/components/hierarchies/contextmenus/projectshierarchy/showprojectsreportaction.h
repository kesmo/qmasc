#ifndef SHOWPROJECTSREPORTACTION_H
#define SHOWPROJECTSREPORTACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectsHierarchy
{

class ShowProjectsReportAction : public AbstractAction
{
public:
    ShowProjectsReportAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

};

}
}
}

#endif // SHOWPROJECTSREPORTACTION_H
