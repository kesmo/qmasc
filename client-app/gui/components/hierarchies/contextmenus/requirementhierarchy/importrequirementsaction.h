#ifndef IMPORTREQUIREMENTSACTION_H
#define IMPORTREQUIREMENTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

class ImportRequirementsAction : public AbstractAction
{
public:
    ImportRequirementsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // IMPORTREQUIREMENTSACTION_H
