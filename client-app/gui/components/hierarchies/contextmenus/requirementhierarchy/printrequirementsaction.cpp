#include "printrequirementsaction.h"

#include "gui/services/requirementsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

PrintRequirementsAction::PrintRequirementsAction(QObject* parent) : AbstractAction(parent)
{
}

bool PrintRequirementsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Requirements::printRequirements(in_record_tree_view->selectedRecords<Requirement>());

    return true;
}

}
}
}
