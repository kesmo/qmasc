#ifndef PRINTREQUIREMENTSACTION_H
#define PRINTREQUIREMENTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

class PrintRequirementsAction : public AbstractAction
{
public:
    PrintRequirementsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // PRINTREQUIREMENTSACTION_H
