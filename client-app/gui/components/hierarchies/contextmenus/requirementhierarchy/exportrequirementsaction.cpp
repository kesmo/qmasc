#include "exportrequirementsaction.h"

#include "gui/services/requirementsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

ExportRequirementsAction::ExportRequirementsAction(QObject* parent) : AbstractAction(parent)
{
}

bool ExportRequirementsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Requirements::exportRequirements(in_record_tree_view);

    return true;
}


}
}
}
