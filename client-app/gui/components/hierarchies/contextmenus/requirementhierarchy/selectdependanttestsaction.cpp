#include "selectdependanttestsaction.h"
#include "gui/services/requirementsservices.h"
#include "gui/services/testsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

SelectDependantTestsAction::SelectDependantTestsAction(bool in_search_project_version_only, QObject* parent) :
    AbstractAction(parent),
    m_search_project_version_only(in_search_project_version_only)
{
}

bool SelectDependantTestsAction::exec(RecordsTreeView *in_record_tree_view)
{
    QModelIndexList tmp_indexes_list = Gui::Services::Tests::dependantsTestsIndexesFromRequirementsList(in_record_tree_view, in_record_tree_view->selectedRecords<Requirement>());

    foreach(QModelIndex tmp_model_index, tmp_indexes_list)
    {
        in_record_tree_view->selectionModel()->select(tmp_model_index, QItemSelectionModel::Select);
        in_record_tree_view->scrollTo(tmp_model_index);
        in_record_tree_view->expandIndex(tmp_model_index);
    }

    return true;
}

}
}
}
