#ifndef EXPORTREQUIREMENTSACTION_H
#define EXPORTREQUIREMENTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

class ExportRequirementsAction : public AbstractAction
{
public:
    ExportRequirementsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // EXPORTREQUIREMENTSACTION_H
