#ifndef SAVEASHTMLREQUIREMENTSACTION_H
#define SAVEASHTMLREQUIREMENTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

class SaveAsHtmlRequirementsAction : public AbstractAction
{
public:
    SaveAsHtmlRequirementsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SAVEASHTMLREQUIREMENTSACTION_H
