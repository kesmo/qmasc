#include "importrequirementsaction.h"

#include "gui/services/requirementsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

ImportRequirementsAction::ImportRequirementsAction(QObject* parent) : AbstractAction(parent)
{
}

bool ImportRequirementsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Requirements::importRequirements(in_record_tree_view);

    return true;
}

}
}
}
