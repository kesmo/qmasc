#include "saveashtmlrequirementsaction.h"

#include "gui/services/requirementsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{

SaveAsHtmlRequirementsAction::SaveAsHtmlRequirementsAction(QObject *parent) : AbstractAction(parent)
{
}

bool SaveAsHtmlRequirementsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Requirements::saveRequirementsAsHtml(in_record_tree_view->selectedRecords<Requirement>());

    return true;
}


}
}
}
