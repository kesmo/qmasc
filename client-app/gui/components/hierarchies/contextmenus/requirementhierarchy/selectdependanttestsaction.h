#ifndef SELECTDEPENDANTTESTSACTION_H
#define SELECTDEPENDANTTESTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RequirementHierarchy
{


class SelectDependantTestsAction : public AbstractAction
{
public:
    SelectDependantTestsAction(bool in_search_project_version_only, QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

private:
    bool m_search_project_version_only;
};

}
}
}

#endif // SELECTDEPENDANTTESTSACTION_H
