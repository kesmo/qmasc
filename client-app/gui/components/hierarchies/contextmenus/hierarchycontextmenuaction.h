#ifndef HIERARCHYCONTEXTMENUACTION_H
#define HIERARCHYCONTEXTMENUACTION_H

#include <QtWidgets/QAction>
#include <QModelIndex>

class RecordsTreeView;

namespace Hierarchies
{
namespace ContextMenuActions
{

class AbstractAction : public QAction
{
public:


    AbstractAction(QObject *parent = 0);

    virtual bool exec(RecordsTreeView *in_record_tree_view) = 0;

};

}
}

#endif // HIERARCHYCONTEXTMENUACTION_H
