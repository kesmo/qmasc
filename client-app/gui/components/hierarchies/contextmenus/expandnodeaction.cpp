#include "expandnodeaction.h"

#include "gui/components/RecordsTreeView.h"

namespace Hierarchies
{
namespace ContextMenuActions
{

ExpandNodeAction::ExpandNodeAction(QObject *parent) : AbstractAction(parent)
{
}

bool ExpandNodeAction::exec(RecordsTreeView *in_record_tree_view)
{
    QModelIndexList indexes = in_record_tree_view->selectionModel()->selectedIndexes();

    foreach (QModelIndex index, indexes) {
        in_record_tree_view->expandIndex(index, true, true);
    }

    return true;
}

}
}
