#ifndef COLLAPSENODEACTION_H
#define COLLAPSENODEACTION_H

#include "hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{

class CollapseNodeAction : public AbstractAction
{
public:
    CollapseNodeAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}

#endif // COLLAPSENODEACTION_H
