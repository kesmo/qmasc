#include "collapsenodeaction.h"

#include "gui/components/RecordsTreeView.h"

namespace Hierarchies
{
namespace ContextMenuActions
{

CollapseNodeAction::CollapseNodeAction(QObject* parent) : AbstractAction(parent)
{
}

bool CollapseNodeAction::exec(RecordsTreeView *in_record_tree_view)
{
    QModelIndexList indexes = in_record_tree_view->selectionModel()->selectedIndexes();

    foreach (QModelIndex index, indexes) {
        in_record_tree_view->collapse(index);
    }

    return true;
}

}
}
