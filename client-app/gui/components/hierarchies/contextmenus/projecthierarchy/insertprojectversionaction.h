#ifndef INSERTPROJECTVERSIONACTION_H
#define INSERTPROJECTVERSIONACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"
#include "entities/projectversion.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{


class InsertProjectVersionAction : public AbstractAction
{
    Q_OBJECT

public:
    InsertProjectVersionAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

private slots:
    void projectVersionCreated(ProjectVersion* selectedProjectVersion);

};

}
}
}

#endif // INSERTPROJECTVERSIONACTION_H
