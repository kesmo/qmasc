#include "showprojectpropertiesaction.h"

#include "gui/services/projectsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{

ShowProjectPropertiesAction::ShowProjectPropertiesAction(QObject* parent) : AbstractAction(parent)
{
}

bool ShowProjectPropertiesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Project *project = Gui::Services::Projects::projectForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->currentIndex());

    if (project){
        Gui::Services::Projects::showProjectInfos(project);

        return true;
    }

    return false;
}


}
}
}
