/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "exportprojectaction.h"
#include "gui/services/projectsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{

ExportProjectAction::ExportProjectAction(QObject *parent) : AbstractAction(parent)
{
}

bool ExportProjectAction::exec(RecordsTreeView *in_record_tree_view)
{
    Project* project = Gui::Services::Projects::projectForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->currentIndex());
    if (project){
        Gui::Services::Projects::exportProject(project);
        return true;
    }

    return false;
}

}
}
}
