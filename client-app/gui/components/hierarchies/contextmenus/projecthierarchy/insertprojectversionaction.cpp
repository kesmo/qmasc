#include "insertprojectversionaction.h"
#include "gui/services/projectsservices.h"
#include "gui/forms/FormNewVersion.h"
#include "gui/mainwindow.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{


InsertProjectVersionAction::InsertProjectVersionAction(QObject* parent )
    : AbstractAction(parent)
{
}

bool InsertProjectVersionAction::exec(RecordsTreeView *in_record_tree_view)
{
    Project* project = Gui::Services::Projects::projectForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->selectionModel()->currentIndex());

    if (project){
        FormNewVersion    *tmp_form = new FormNewVersion(project, MainWindow::instance());

        connect(tmp_form, &FormNewVersion::versionCreated, this, &InsertProjectVersionAction::projectVersionCreated);

        tmp_form->show();

        return true;
    }

    return false;
}

void InsertProjectVersionAction::projectVersionCreated(ProjectVersion* selectedProjectVersion)
{
    Gui::Services::Projects::showProjectView(selectedProjectVersion->project()->getIdentifier(), selectedProjectVersion->getIdentifier());
}


}
}
}
