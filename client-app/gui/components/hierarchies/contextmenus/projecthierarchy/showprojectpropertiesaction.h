#ifndef SHOWPROJECTPROPERTIESACTION_H
#define SHOWPROJECTPROPERTIESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{

class ShowProjectPropertiesAction : public AbstractAction
{
public:
    ShowProjectPropertiesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

};

}
}
}

#endif // SHOWPROJECTPROPERTIESACTION_H
