#include "removeprojectaction.h"

#include "gui/services/projectsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{

RemoveProjectAction::RemoveProjectAction(QObject *parent) : AbstractAction(parent)
{
}

bool RemoveProjectAction::exec(RecordsTreeView *in_record_tree_view)
{
    Project *project = Gui::Services::Projects::projectForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->currentIndex());
    Gui::Services::Projects::removeProject(in_record_tree_view->getRecordsTreeModel(), project);

    return true;
}



}
}
}
