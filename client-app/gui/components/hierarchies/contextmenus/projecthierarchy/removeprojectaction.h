#ifndef REMOVEPROJECTACTION_H
#define REMOVEPROJECTACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectHierarchy
{

class RemoveProjectAction : public AbstractAction
{
public:
    RemoveProjectAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

};

}
}
}

#endif // REMOVEPROJECTACTION_H
