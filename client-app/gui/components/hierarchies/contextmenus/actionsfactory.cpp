/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "actionsfactory.h"

#include "testhierarchy/insertaction.h"
#include "testhierarchy/insertaction.h"
#include "testhierarchy/removetestaction.h"
#include "testhierarchy/insertchildtestaction.h"
#include "testhierarchy/importtestsaction.h"
#include "testhierarchy/exporttestsaction.h"
#include "testhierarchy/selectlinktestsaction.h"
#include "testhierarchy/saveashtmltestsaction.h"
#include "testhierarchy/printtestsaction.h"

#include "requirementhierarchy/insertrequirementaction.h"
#include "requirementhierarchy/insertchildrequirementaction.h"
#include "requirementhierarchy/removerequirementaction.h"
#include "requirementhierarchy/importrequirementsaction.h"
#include "requirementhierarchy/exportrequirementsaction.h"
#include "requirementhierarchy/saveashtmlrequirementsaction.h"
#include "requirementhierarchy/printrequirementsaction.h"
#include "requirementhierarchy/selectdependanttestsaction.h"

#include "needhierarchy/insertneedaction.h"
#include "needhierarchy/insertchildneedaction.h"
#include "needhierarchy/removeneedaction.h"
#include "needhierarchy/importneedsaction.h"
#include "needhierarchy/exportneedsaction.h"
#include "needhierarchy/saveashtmlneedsaction.h"
#include "needhierarchy/printneedsaction.h"

#include "featurehierarchy/insertfeatureaction.h"
#include "featurehierarchy/insertchildfeatureaction.h"
#include "featurehierarchy/removefeatureaction.h"
#include "featurehierarchy/importfeaturesaction.h"
#include "featurehierarchy/exportfeaturesaction.h"
#include "featurehierarchy/saveashtmlfeaturesaction.h"
#include "featurehierarchy/printfeaturesaction.h"

#include "rulehierarchy/insertruleaction.h"
#include "rulehierarchy/insertchildruleaction.h"
#include "rulehierarchy/removeruleaction.h"
#include "rulehierarchy/importrulesaction.h"
#include "rulehierarchy/exportrulesaction.h"
#include "rulehierarchy/saveashtmlrulesaction.h"
#include "rulehierarchy/printrulesaction.h"

#include "campaignhierarchy/insertcampaignaction.h"
#include "campaignhierarchy/removecampaignaction.h"

#include "projectshierarchy/insertprojectaction.h"
#include "projectshierarchy/importprojectaction.h"
#include "projectshierarchy/showprojectsreportaction.h"

#include "projecthierarchy/showprojectpropertiesaction.h"
#include "projecthierarchy/exportprojectaction.h"
#include "projecthierarchy/insertprojectversionaction.h"
#include "projecthierarchy/removeprojectaction.h"

#include "projectversionhierarchy/setasdefaultprojectversionaction.h"
#include "projectversionhierarchy/searchaction.h"
#include "projectversionhierarchy/showbugsaction.h"
#include "projectversionhierarchy/showexecutionsreportsaction.h"
#include "projectversionhierarchy/showpropertiesaction.h"
#include "projectversionhierarchy/removeprojectversionaction.h"

#include "separatoraction.h"
#include "expandnodeaction.h"
#include "collapsenodeaction.h"

#include <QApplication>
#include <QMenu>
#include <QPainter>
#include <QPixmap>

namespace Hierarchies
{
namespace ContextMenuActions
{

ActionsFactory* ActionsFactory::m_instance = NULL;
QMap<ActionsFactory::ActionType, ActionsFactory::CreateActionFunction> ActionsFactory::m_create_action_functions_map;

ActionsFactory* ActionsFactory::instance()
{
    if (m_instance == NULL){
        m_instance = new ActionsFactory();

        m_create_action_functions_map[SeparatorAction] = createSeparatorAction;

        m_create_action_functions_map[InsertTest] = createInsertTestAction;
        m_create_action_functions_map[InsertAutomatedTest] = createInsertAutomatedTestAction;
        m_create_action_functions_map[InsertChildTest] = createInsertChildTestAction;
        m_create_action_functions_map[InsertChildAutomatedTest] = createInsertChildAutomatedTestAction;
        m_create_action_functions_map[RemoveTest] = createRemoveTestAction;
        m_create_action_functions_map[ExpandTest] = createExpandTestNodeAction;
        m_create_action_functions_map[CollapseTest] = createCollapseTestNodeAction;
        m_create_action_functions_map[SelectLinkTest] = createSelectLinkTestsAction;
        m_create_action_functions_map[ImportTest] = createImportTestsAction;
        m_create_action_functions_map[ExportTest] = createExportTestsAction;
        m_create_action_functions_map[PrintTest] = createPrintTestsAction;
        m_create_action_functions_map[SaveTestAsHtml] = creatSaveAsHtmlTestsAction;

        m_create_action_functions_map[InsertRequirement] = createInsertRequirementAction;
        m_create_action_functions_map[InsertChildRequirement] = createInsertChildRequirementAction;
        m_create_action_functions_map[RemoveRequirement] = createRemoveRequirementAction;
        m_create_action_functions_map[ExpandRequirement] = createExpandRequirementNodeAction;
        m_create_action_functions_map[CollapseRequirement] = createCollapseRequirementNodeAction;
        m_create_action_functions_map[SelectDepedantTests] = createSelectDepedantTestsAction;
        m_create_action_functions_map[ImportRequirement] = createImportRequirementsAction;
        m_create_action_functions_map[ExportRequirement] = createExportRequirementsAction;
        m_create_action_functions_map[PrintRequirement] = createPrintRequirementsAction;
        m_create_action_functions_map[SaveRequirementAsHtml] = creatSaveAsHtmlRequirementsAction;

        m_create_action_functions_map[AssociateTestsAndRequirements] = creatAssociateTestsAndRequirementsAction;

        m_create_action_functions_map[InsertNeed] = createInsertNeedAction;
        m_create_action_functions_map[InsertChildNeed] = createInsertChildNeedAction;
        m_create_action_functions_map[RemoveNeed] = createRemoveNeedAction;
        m_create_action_functions_map[ExpandNeed] = createExpandNeedNodeAction;
        m_create_action_functions_map[CollapseNeed] = createCollapseNeedNodeAction;
        m_create_action_functions_map[ImportNeed] = createImportNeedsAction;
        m_create_action_functions_map[ExportNeed] = createExportNeedsAction;
        m_create_action_functions_map[PrintNeed] = createPrintNeedsAction;
        m_create_action_functions_map[SaveNeedAsHtml] = creatSaveAsHtmlNeedsAction;

        m_create_action_functions_map[InsertFeature] = createInsertFeatureAction;
        m_create_action_functions_map[InsertChildFeature] = createInsertChildFeatureAction;
        m_create_action_functions_map[RemoveFeature] = createRemoveFeatureAction;
        m_create_action_functions_map[ExpandFeature] = createExpandFeatureNodeAction;
        m_create_action_functions_map[CollapseFeature] = createCollapseFeatureNodeAction;
        m_create_action_functions_map[ImportFeature] = createImportFeaturesAction;
        m_create_action_functions_map[ExportFeature] = createExportFeaturesAction;
        m_create_action_functions_map[PrintFeature] = createPrintFeaturesAction;
        m_create_action_functions_map[SaveFeatureAsHtml] = creatSaveAsHtmlFeaturesAction;

        m_create_action_functions_map[InsertRule] = createInsertRuleAction;
        m_create_action_functions_map[InsertChildRule] = createInsertChildRuleAction;
        m_create_action_functions_map[RemoveRule] = createRemoveRuleAction;
        m_create_action_functions_map[ExpandRule] = createExpandRuleNodeAction;
        m_create_action_functions_map[CollapseRule] = createCollapseRuleNodeAction;
        m_create_action_functions_map[ImportRule] = createImportRulesAction;
        m_create_action_functions_map[ExportRule] = createExportRulesAction;
        m_create_action_functions_map[PrintRule] = createPrintRulesAction;
        m_create_action_functions_map[SaveRuleAsHtml] = creatSaveAsHtmlRulesAction;

        m_create_action_functions_map[InsertCampaign] = createInsertCampaignAction;
        m_create_action_functions_map[RemoveCampaign] = createRemoveCampaignAction;

        m_create_action_functions_map[InsertProject] = createInsertProjectAction;
        m_create_action_functions_map[ImportProject] = createImportProjectAction;
        m_create_action_functions_map[ShowProjectsReportsAction] = createProjectsReportsAction;

        m_create_action_functions_map[ShowProjectPropertiesAction] = createShowProjectPropertiesAction;
        m_create_action_functions_map[ExportProject] = createExportProjectAction;
        m_create_action_functions_map[InsertProjectVersion] = createInsertProjectVersionAction;
        m_create_action_functions_map[RemoveProject] = createRemoveProjectAction;
        m_create_action_functions_map[ShowExecutionReportsProjectAction] = createShowExecutionReportsProjectAction;

        m_create_action_functions_map[ShowProjectVersionPropertiesAction] = createShowProjectVersionPropertiesAction;
        m_create_action_functions_map[ShowProjectVersionBugsAction] = createShowProjectVersionBugsAction;
        m_create_action_functions_map[SearchProjectVersionAction] = createSearchProjectVersionAction;
        m_create_action_functions_map[SetAsDefaultProjectVersionAction] = createSetAsDefaultProjectVersionAction;
        m_create_action_functions_map[RemoveProjectVersion] = createRemoveProjectVersionAction;
    }

    return m_instance;
}


QMenu* ActionsFactory::createMenuForActionsTypes(QList<ActionType> actionsTypesList)
{
    QMenu* menu = NULL;

    if (!actionsTypesList.isEmpty())
    {
        menu = new QMenu();

        foreach(ActionType actionType, actionsTypesList){
            AbstractAction* action = createAction(actionType, menu);
            if (action){
                menu->addAction(action);
            }
        }
    }

    return menu;
}

AbstractAction* ActionsFactory::createAction(ActionType actionType, QObject* parent)
{
    CreateActionFunction tmp_function = m_create_action_functions_map[actionType];
    if (tmp_function)
        return tmp_function(parent);

    return NULL;
}

AbstractAction* ActionsFactory::createSeparatorAction(QObject* parent){
    return new ContextMenuActions::SeparatorAction(parent);
}


/*
 * TESTS
 */

AbstractAction* ActionsFactory::createInsertTestAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertTestAction = new ContextMenuActions::TestHierarchy::InsertTestAction(false, parent);
    insertTestAction->setIcon(QIcon(QPixmap(":/images/22x22/actions/insert.png")));
    insertTestAction->setText(qApp->tr("&Insérer un test..."));
    insertTestAction->setStatusTip(qApp->tr("Insérer un nouveau scénario/cas de test"));

    return insertTestAction;
}


AbstractAction* ActionsFactory::createInsertAutomatedTestAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertAutomatedTestAction = new ContextMenuActions::TestHierarchy::InsertTestAction(true, parent);
    insertAutomatedTestAction->setIcon(QIcon(QPixmap(":/images/22x22/actions/run.png")));
    insertAutomatedTestAction->setText(qApp->tr("&Insérer un test automatisé..."));
    insertAutomatedTestAction->setStatusTip(qApp->tr("Insérer un nouveau scénario/cas de test automatisé"));

    return insertAutomatedTestAction;
}

AbstractAction* ActionsFactory::createInsertChildTestAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertChildTestAction = new ContextMenuActions::TestHierarchy::InsertChildTestAction(false, parent);
    insertChildTestAction->setIcon(QIcon(QPixmap(":/images/22x22/actions/add.png")));
    insertChildTestAction->setText(qApp->tr("&Ajouter un test"));
    insertChildTestAction->setStatusTip(qApp->tr("Ajouter un test à la sélection"));

    return insertChildTestAction;
}


AbstractAction* ActionsFactory::createInsertChildAutomatedTestAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertChildAutomatedTestAction = new ContextMenuActions::TestHierarchy::InsertChildTestAction(true, parent);
    insertChildAutomatedTestAction->setIcon(QIcon(QPixmap(":/images/22x22/actions/run.png")));
    insertChildAutomatedTestAction->setText(qApp->tr("&Ajouter un test automatisé"));
    insertChildAutomatedTestAction->setStatusTip(qApp->tr("Ajouter un test automatisé à la sélection"));

    return insertChildAutomatedTestAction;
}


AbstractAction* ActionsFactory::createRemoveTestAction(QObject* parent){
    ContextMenuActions::AbstractAction *removeTestAction = new ContextMenuActions::TestHierarchy::RemoveTestAction(parent);
    removeTestAction->setIcon(QIcon(QPixmap(":/images/22x22/actions/remove.png")));
    removeTestAction->setText(qApp->tr("&Supprimer le test sélectionné"));
    removeTestAction->setStatusTip(qApp->tr("Supprimer le scénario/cas de test sélectionné"));

    return removeTestAction;
}


AbstractAction* ActionsFactory::createExpandTestNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *expandSelectedTestsAction = new ContextMenuActions::ExpandNodeAction(parent);
    expandSelectedTestsAction->setIcon(QIcon(":/images/22x22/actions/expand.png"));
    expandSelectedTestsAction->setText(qApp->tr("&Dérouler les tests sélectionnés"));
    expandSelectedTestsAction->setStatusTip(qApp->tr("Dérouler les tests sélectionnés"));

    return expandSelectedTestsAction;
}

AbstractAction* ActionsFactory::createCollapseTestNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *collapseSelectedTestsAction = new ContextMenuActions::CollapseNodeAction(parent);
    collapseSelectedTestsAction->setIcon(QIcon(":/images/22x22/actions/collapse.png"));
    collapseSelectedTestsAction->setText(qApp->tr("&Enrouler les tests sélectionnés"));
    collapseSelectedTestsAction->setStatusTip(qApp->tr("Enrouler les tests sélectionnés"));

    return collapseSelectedTestsAction;
}

AbstractAction* ActionsFactory::createSelectLinkTestsAction(QObject* parent){
    ContextMenuActions::AbstractAction *selectLinkTestsAction = new ContextMenuActions::TestHierarchy::SelectLinkTestsAction(parent);
    selectLinkTestsAction->setIcon(QIcon(":/images/22x22/actions/link.png"));
    selectLinkTestsAction->setText(qApp->tr("&Sélectionner les tests dépendants"));
    selectLinkTestsAction->setStatusTip(qApp->tr("Sélectionner les scénarios de test dépendants des scénarios sélectionnés"));

    return selectLinkTestsAction;
}

AbstractAction* ActionsFactory::createImportTestsAction(QObject* parent){
    ContextMenuActions::AbstractAction *importTestsAction = new ContextMenuActions::TestHierarchy::ImportTestsAction(parent);
    importTestsAction->setIcon(QIcon(":/images/22x22/actions/import.png"));
    importTestsAction->setText(qApp->tr("&Importer des tests"));
    importTestsAction->setStatusTip(qApp->tr("Importer des tests depuis un fichier texte"));

    return importTestsAction;
}

AbstractAction* ActionsFactory::createExportTestsAction(QObject* parent){
    ContextMenuActions::AbstractAction *exportTestsAction = new ContextMenuActions::TestHierarchy::ExportTestsAction(parent);
    exportTestsAction->setIcon(QIcon(":/images/22x22/actions/export.png"));
    exportTestsAction->setText(qApp->tr("&Export texte..."));
    exportTestsAction->setStatusTip(qApp->tr("Exporter les tests sélectionnés vers un fichier texte"));

    return exportTestsAction;
}

AbstractAction* ActionsFactory::createPrintTestsAction(QObject* parent){
    ContextMenuActions::AbstractAction *printTestsAction = new ContextMenuActions::TestHierarchy::PrintTestsAction(parent);
    printTestsAction->setIcon(QIcon(":/images/22x22/actions/pdf.png"));
    printTestsAction->setText(qApp->tr("&Export PDF..."));
    printTestsAction->setStatusTip(qApp->tr("Exporter les tests sélectionnés vers un fichier pdf"));

    return printTestsAction;
}

AbstractAction* ActionsFactory::creatSaveAsHtmlTestsAction(QObject* parent){
    ContextMenuActions::AbstractAction *saveAsHtmlTestsAction = new ContextMenuActions::TestHierarchy::SaveAsHtmlTestsAction(parent);
    saveAsHtmlTestsAction->setIcon(QIcon(":/images/22x22/actions/html.png"));
    saveAsHtmlTestsAction->setText(qApp->tr("&Export html..."));
    saveAsHtmlTestsAction->setStatusTip(qApp->tr("Exporter les tests sélectionnés vers un fichier html"));

    return saveAsHtmlTestsAction;
}

/*
 * REQUIREMENTS
 */
AbstractAction* ActionsFactory::createInsertRequirementAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertRequirementAction = new ContextMenuActions::RequirementHierarchy::InsertRequirementAction(parent);
    insertRequirementAction->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    insertRequirementAction->setText(qApp->tr("&Insérer une exigence..."));
    insertRequirementAction->setStatusTip(qApp->tr("Insérer une nouvelle exigence"));
    return insertRequirementAction;
}

AbstractAction* ActionsFactory::createInsertChildRequirementAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertChildRequirementAction = new ContextMenuActions::RequirementHierarchy::InsertChildRequirementAction(parent);
    insertChildRequirementAction->setIcon(QIcon(":/images/22x22/actions/add.png"));
    insertChildRequirementAction->setText(qApp->tr("&Ajouter une exigence"));
    insertChildRequirementAction->setStatusTip(qApp->tr("Ajouter une exigence à la sélection"));
    return insertChildRequirementAction;
}

AbstractAction* ActionsFactory::createRemoveRequirementAction(QObject* parent){
    ContextMenuActions::AbstractAction *removeRequirementAction = new ContextMenuActions::RequirementHierarchy::RemoveRequirementAction(parent);
    removeRequirementAction->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    removeRequirementAction->setText(qApp->tr("&Supprimer l'exigence sélectionnée"));
    removeRequirementAction->setStatusTip(qApp->tr("Supprimer l'exigence sélectionnée"));
    return removeRequirementAction;
}


AbstractAction* ActionsFactory::createExpandRequirementNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *expandSelectedRequirementsAction = new ContextMenuActions::ExpandNodeAction(parent);
    expandSelectedRequirementsAction->setIcon(QIcon(":/images/22x22/actions/expand.png"));
    expandSelectedRequirementsAction->setText(qApp->tr("&Dérouler les exigences sélectionnées"));
    expandSelectedRequirementsAction->setStatusTip(qApp->tr("Dérouler les exigences sélectionnées"));
    return expandSelectedRequirementsAction;
}

AbstractAction* ActionsFactory::createCollapseRequirementNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *collapseAllRequirementsAction = new ContextMenuActions::CollapseNodeAction(parent);
    collapseAllRequirementsAction->setIcon(QIcon(":/images/22x22/actions/collapse.png"));
    collapseAllRequirementsAction->setText(qApp->tr("&Enrouler les exigences sélectionnées"));
    collapseAllRequirementsAction->setStatusTip(qApp->tr("Enrouler les exigences sélectionnées"));
    return collapseAllRequirementsAction;
}

AbstractAction* ActionsFactory::createSelectDepedantTestsAction(QObject* parent){
    ContextMenuActions::AbstractAction *selectDependantsTestsAction = new ContextMenuActions::RequirementHierarchy::SelectDependantTestsAction(parent);
    selectDependantsTestsAction->setIcon(QIcon(":/images/22x22/actions/link.png"));
    selectDependantsTestsAction->setText(qApp->tr("&Sélectionner les tests dépendants"));
    selectDependantsTestsAction->setStatusTip(qApp->tr("Sélectionner les scénarios de test dépendants des exigences sélectionnées"));
    return selectDependantsTestsAction;
}

AbstractAction* ActionsFactory::createImportRequirementsAction(QObject* parent){
    ContextMenuActions::AbstractAction *importRequirementsAction = new ContextMenuActions::RequirementHierarchy::ImportRequirementsAction(parent);
    importRequirementsAction->setIcon(QIcon(":/images/22x22/actions/import.png"));
    importRequirementsAction->setText(qApp->tr("&Importer des exigences"));
    importRequirementsAction->setStatusTip(qApp->tr("Importer des exigence depuis un fichier texte"));
    return importRequirementsAction;
}

AbstractAction* ActionsFactory::createExportRequirementsAction(QObject* parent){
    ContextMenuActions::AbstractAction *exportRequirementsAction = new ContextMenuActions::RequirementHierarchy::ExportRequirementsAction(parent);
    exportRequirementsAction->setIcon(QIcon(":/images/22x22/actions/export.png"));
    exportRequirementsAction->setText(qApp->tr("&Export texte..."));
    exportRequirementsAction->setStatusTip(qApp->tr("Exporter les exigence sélectionnées vers un fichier texte"));
    return exportRequirementsAction;
}

AbstractAction* ActionsFactory::createPrintRequirementsAction(QObject* parent){
    ContextMenuActions::AbstractAction *printRequirementsAction = new ContextMenuActions::RequirementHierarchy::PrintRequirementsAction(parent);
    printRequirementsAction->setIcon(QIcon(":/images/22x22/actions/pdf.png"));
    printRequirementsAction->setText(qApp->tr("&Export PDF..."));
    printRequirementsAction->setStatusTip(qApp->tr("Exporter les exigences sélectionnées vers un fichier pdf"));
    return printRequirementsAction;
}

AbstractAction* ActionsFactory::creatSaveAsHtmlRequirementsAction(QObject* parent){
    ContextMenuActions::AbstractAction *saveAsHtmlRequirementsAction = new ContextMenuActions::RequirementHierarchy::SaveAsHtmlRequirementsAction(parent);
    saveAsHtmlRequirementsAction->setIcon(QIcon(":/images/22x22/actions/html.png"));
    saveAsHtmlRequirementsAction->setText(qApp->tr("&Export html..."));
    saveAsHtmlRequirementsAction->setStatusTip(qApp->tr("Exporter les exigences sélectionnées vers un fichier html"));
    return saveAsHtmlRequirementsAction;
}


AbstractAction* ActionsFactory::creatAssociateTestsAndRequirementsAction(QObject* parent){
    ContextMenuActions::AbstractAction *saveAsHtmlRequirementsAction = new ContextMenuActions::RequirementHierarchy::SaveAsHtmlRequirementsAction(parent);
    saveAsHtmlRequirementsAction->setIcon(QIcon(":/images/22x22/actions/html.png"));
    saveAsHtmlRequirementsAction->setText(qApp->tr("&Associer les tests et exigences sélectionnés"));
    saveAsHtmlRequirementsAction->setStatusTip(qApp->tr("Associer les tests et exigences sélectionnés"));
    return saveAsHtmlRequirementsAction;
}

/*
 * NEEDS
 */
AbstractAction* ActionsFactory::createInsertNeedAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertNeedAction = new ContextMenuActions::NeedHierarchy::InsertNeedAction(parent);
    insertNeedAction->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    insertNeedAction->setText(qApp->tr("&Insérer un besoin..."));
    insertNeedAction->setStatusTip(qApp->tr("Insérer un nouveau besoin"));
    return insertNeedAction;
}

AbstractAction* ActionsFactory::createInsertChildNeedAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertChildNeedAction = new ContextMenuActions::NeedHierarchy::InsertChildNeedAction(parent);
    insertChildNeedAction->setIcon(QIcon(":/images/22x22/actions/add.png"));
    insertChildNeedAction->setText(qApp->tr("&Ajouter un besoin"));
    insertChildNeedAction->setStatusTip(qApp->tr("Ajouter un besoin fils à la sélection"));
    return insertChildNeedAction;
}

AbstractAction* ActionsFactory::createRemoveNeedAction(QObject* parent){
    ContextMenuActions::AbstractAction *removeNeedAction = new ContextMenuActions::NeedHierarchy::RemoveNeedAction(parent);
    removeNeedAction->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    removeNeedAction->setText(qApp->tr("&Supprimer le besoin sélectionné"));
    removeNeedAction->setStatusTip(qApp->tr("Supprimer le besoin sélectionné"));
    return removeNeedAction;
}


AbstractAction* ActionsFactory::createExpandNeedNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *expandSelectedNeedsAction = new ContextMenuActions::ExpandNodeAction(parent);
    expandSelectedNeedsAction->setIcon(QIcon(":/images/22x22/actions/expand.png"));
    expandSelectedNeedsAction->setText(qApp->tr("&Dérouler les besoins sélectionnés"));
    expandSelectedNeedsAction->setStatusTip(qApp->tr("Dérouler les besoins sélectionnés"));
    return expandSelectedNeedsAction;
}

AbstractAction* ActionsFactory::createCollapseNeedNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *collapseAllNeedsAction = new ContextMenuActions::CollapseNodeAction(parent);
    collapseAllNeedsAction->setIcon(QIcon(":/images/22x22/actions/collapse.png"));
    collapseAllNeedsAction->setText(qApp->tr("&Enrouler les besoins sélectionnés"));
    collapseAllNeedsAction->setStatusTip(qApp->tr("Enrouler les besoins sélectionnés"));
    return collapseAllNeedsAction;
}

AbstractAction* ActionsFactory::createImportNeedsAction(QObject* parent){
    ContextMenuActions::AbstractAction *importNeedsAction = new ContextMenuActions::NeedHierarchy::ImportNeedsAction(parent);
    importNeedsAction->setIcon(QIcon(":/images/22x22/actions/import.png"));
    importNeedsAction->setText(qApp->tr("&Importer des besoins"));
    importNeedsAction->setStatusTip(qApp->tr("Importer des besoins depuis un fichier texte"));
    return importNeedsAction;
}

AbstractAction* ActionsFactory::createExportNeedsAction(QObject* parent){
    ContextMenuActions::AbstractAction *exportNeedsAction = new ContextMenuActions::NeedHierarchy::ExportNeedsAction(parent);
    exportNeedsAction->setIcon(QIcon(":/images/22x22/actions/export.png"));
    exportNeedsAction->setText(qApp->tr("&Export texte..."));
    exportNeedsAction->setStatusTip(qApp->tr("Exporter les besoins sélectionnés vers un fichier texte"));
    return exportNeedsAction;
}

AbstractAction* ActionsFactory::createPrintNeedsAction(QObject* parent){
    ContextMenuActions::AbstractAction *printNeedsAction = new ContextMenuActions::NeedHierarchy::PrintNeedsAction(parent);
    printNeedsAction->setIcon(QIcon(":/images/22x22/actions/pdf.png"));
    printNeedsAction->setText(qApp->tr("&Export PDF..."));
    printNeedsAction->setStatusTip(qApp->tr("Exporter les besoins sélectionnés vers un fichier pdf"));
    return printNeedsAction;
}

AbstractAction* ActionsFactory::creatSaveAsHtmlNeedsAction(QObject* parent){
    ContextMenuActions::AbstractAction *saveAsHtmlNeedsAction = new ContextMenuActions::NeedHierarchy::SaveAsHtmlNeedsAction(parent);
    saveAsHtmlNeedsAction->setIcon(QIcon(":/images/22x22/actions/html.png"));
    saveAsHtmlNeedsAction->setText(qApp->tr("&Export html..."));
    saveAsHtmlNeedsAction->setStatusTip(qApp->tr("Exporter les besoins sélectionnés vers un fichier html"));
    return saveAsHtmlNeedsAction;
}
/*
 * FEATURES
 */
AbstractAction* ActionsFactory::createInsertFeatureAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertFeatureAction = new ContextMenuActions::FeatureHierarchy::InsertFeatureAction(parent);
    insertFeatureAction->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    insertFeatureAction->setText(qApp->tr("&Insérer une fonctionnalité..."));
    insertFeatureAction->setStatusTip(qApp->tr("Insérer une nouvelle fonctionnalité"));
    return insertFeatureAction;
}

AbstractAction* ActionsFactory::createInsertChildFeatureAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertChildFeatureAction = new ContextMenuActions::FeatureHierarchy::InsertChildFeatureAction(parent);
    insertChildFeatureAction->setIcon(QIcon(":/images/22x22/actions/add.png"));
    insertChildFeatureAction->setText(qApp->tr("&Ajouter une fonctionnalité"));
    insertChildFeatureAction->setStatusTip(qApp->tr("Ajouter une fonctionnalité à la sélection"));
    return insertChildFeatureAction;
}

AbstractAction* ActionsFactory::createRemoveFeatureAction(QObject* parent){
    ContextMenuActions::AbstractAction *removeFeatureAction = new ContextMenuActions::FeatureHierarchy::RemoveFeatureAction(parent);
    removeFeatureAction->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    removeFeatureAction->setText(qApp->tr("&Supprimer la fonctionnalité sélectionnée"));
    removeFeatureAction->setStatusTip(qApp->tr("Supprimer la fonctionnalité sélectionnée"));
    return removeFeatureAction;
}


AbstractAction* ActionsFactory::createExpandFeatureNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *expandSelectedFeaturesAction = new ContextMenuActions::ExpandNodeAction(parent);
    expandSelectedFeaturesAction->setIcon(QIcon(":/images/22x22/actions/expand.png"));
    expandSelectedFeaturesAction->setText(qApp->tr("&Dérouler les fonctionnalités sélectionnées"));
    expandSelectedFeaturesAction->setStatusTip(qApp->tr("Dérouler les fonctionnalités sélectionnées"));
    return expandSelectedFeaturesAction;
}

AbstractAction* ActionsFactory::createCollapseFeatureNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *collapseAllFeaturesAction = new ContextMenuActions::CollapseNodeAction(parent);
    collapseAllFeaturesAction->setIcon(QIcon(":/images/22x22/actions/collapse.png"));
    collapseAllFeaturesAction->setText(qApp->tr("&Enrouler les fonctionnalités sélectionnées"));
    collapseAllFeaturesAction->setStatusTip(qApp->tr("Enrouler les fonctionnalités sélectionnées"));
    return collapseAllFeaturesAction;
}

AbstractAction* ActionsFactory::createImportFeaturesAction(QObject* parent){
    ContextMenuActions::AbstractAction *importFeaturesAction = new ContextMenuActions::FeatureHierarchy::ImportFeaturesAction(parent);
    importFeaturesAction->setIcon(QIcon(":/images/22x22/actions/import.png"));
    importFeaturesAction->setText(qApp->tr("&Importer des fonctionnalités"));
    importFeaturesAction->setStatusTip(qApp->tr("Importer des fonctionnalités depuis un fichier texte"));
    return importFeaturesAction;
}

AbstractAction* ActionsFactory::createExportFeaturesAction(QObject* parent){
    ContextMenuActions::AbstractAction *exportFeaturesAction = new ContextMenuActions::FeatureHierarchy::ExportFeaturesAction(parent);
    exportFeaturesAction->setIcon(QIcon(":/images/22x22/actions/export.png"));
    exportFeaturesAction->setText(qApp->tr("&Export texte..."));
    exportFeaturesAction->setStatusTip(qApp->tr("Exporter les fonctionnalités sélectionnées vers un fichier texte"));
    return exportFeaturesAction;
}

AbstractAction* ActionsFactory::createPrintFeaturesAction(QObject* parent){
    ContextMenuActions::AbstractAction *printFeaturesAction = new ContextMenuActions::FeatureHierarchy::PrintFeaturesAction(parent);
    printFeaturesAction->setIcon(QIcon(":/images/22x22/actions/pdf.png"));
    printFeaturesAction->setText(qApp->tr("&Export PDF..."));
    printFeaturesAction->setStatusTip(qApp->tr("Exporter les fonctionnalités sélectionnées vers un fichier pdf"));
    return printFeaturesAction;
}

AbstractAction* ActionsFactory::creatSaveAsHtmlFeaturesAction(QObject* parent){
    ContextMenuActions::AbstractAction *saveAsHtmlFeaturesAction = new ContextMenuActions::FeatureHierarchy::SaveAsHtmlFeaturesAction(parent);
    saveAsHtmlFeaturesAction->setIcon(QIcon(":/images/22x22/actions/html.png"));
    saveAsHtmlFeaturesAction->setText(qApp->tr("&Export html..."));
    saveAsHtmlFeaturesAction->setStatusTip(qApp->tr("Exporter les fonctionnalités sélectionnées vers un fichier html"));
    return saveAsHtmlFeaturesAction;
}
/*
 * RULES
 */
AbstractAction* ActionsFactory::createInsertRuleAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertRuleAction = new ContextMenuActions::RuleHierarchy::InsertRuleAction(parent);
    insertRuleAction->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    insertRuleAction->setText(qApp->tr("&Insérer une règle de gestion..."));
    insertRuleAction->setStatusTip(qApp->tr("Insérer une nouvelle règle de gestion"));
    return insertRuleAction;
}

AbstractAction* ActionsFactory::createInsertChildRuleAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertChildRuleAction = new ContextMenuActions::RuleHierarchy::InsertChildRuleAction(parent);
    insertChildRuleAction->setIcon(QIcon(":/images/22x22/actions/add.png"));
    insertChildRuleAction->setText(qApp->tr("&Ajouter une règle de gestion"));
    insertChildRuleAction->setStatusTip(qApp->tr("Ajouter une règle de gestion fille à la sélection"));
    return insertChildRuleAction;
}

AbstractAction* ActionsFactory::createRemoveRuleAction(QObject* parent){
    ContextMenuActions::AbstractAction *removeRuleAction = new ContextMenuActions::RuleHierarchy::RemoveRuleAction(parent);
    removeRuleAction->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    removeRuleAction->setText(qApp->tr("&Supprimer la règle de gestion sélectionnée"));
    removeRuleAction->setStatusTip(qApp->tr("Supprimer la règle de gestion sélectionnée"));
    return removeRuleAction;
}


AbstractAction* ActionsFactory::createExpandRuleNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *expandSelectedRulesAction = new ContextMenuActions::ExpandNodeAction(parent);
    expandSelectedRulesAction->setIcon(QIcon(":/images/22x22/actions/expand.png"));
    expandSelectedRulesAction->setText(qApp->tr("&Dérouler les règles de gestion sélectionnées"));
    expandSelectedRulesAction->setStatusTip(qApp->tr("Dérouler les règles de gestion sélectionnées"));
    return expandSelectedRulesAction;
}

AbstractAction* ActionsFactory::createCollapseRuleNodeAction(QObject* parent){
    ContextMenuActions::AbstractAction *collapseAllRulesAction = new ContextMenuActions::CollapseNodeAction(parent);
    collapseAllRulesAction->setIcon(QIcon(":/images/22x22/actions/collapse.png"));
    collapseAllRulesAction->setText(qApp->tr("&Enrouler les règles de gestion sélectionnées"));
    collapseAllRulesAction->setStatusTip(qApp->tr("Enrouler les règles de gestion sélectionnées"));
    return collapseAllRulesAction;
}

AbstractAction* ActionsFactory::createImportRulesAction(QObject* parent){
    ContextMenuActions::AbstractAction *importRulesAction = new ContextMenuActions::RuleHierarchy::ImportRulesAction(parent);
    importRulesAction->setIcon(QIcon(":/images/22x22/actions/import.png"));
    importRulesAction->setText(qApp->tr("&Importer des règles de gestion"));
    importRulesAction->setStatusTip(qApp->tr("Importer des règles de gestion depuis un fichier texte"));
    return importRulesAction;
}

AbstractAction* ActionsFactory::createExportRulesAction(QObject* parent){
    ContextMenuActions::AbstractAction *exportRulesAction = new ContextMenuActions::RuleHierarchy::ExportRulesAction(parent);
    exportRulesAction->setIcon(QIcon(":/images/22x22/actions/export.png"));
    exportRulesAction->setText(qApp->tr("&Export texte..."));
    exportRulesAction->setStatusTip(qApp->tr("Exporter les règles de gestion sélectionnées vers un fichier texte"));
    return exportRulesAction;
}

AbstractAction* ActionsFactory::createPrintRulesAction(QObject* parent){
    ContextMenuActions::AbstractAction *printRulesAction = new ContextMenuActions::RuleHierarchy::PrintRulesAction(parent);
    printRulesAction->setIcon(QIcon(":/images/22x22/actions/pdf.png"));
    printRulesAction->setText(qApp->tr("&Export PDF..."));
    printRulesAction->setStatusTip(qApp->tr("Exporter les règles de gestion sélectionnées vers un fichier pdf"));
    return printRulesAction;
}

AbstractAction* ActionsFactory::creatSaveAsHtmlRulesAction(QObject* parent){
    ContextMenuActions::AbstractAction *saveAsHtmlRulesAction = new ContextMenuActions::RuleHierarchy::SaveAsHtmlRulesAction(parent);
    saveAsHtmlRulesAction->setIcon(QIcon(":/images/22x22/actions/html.png"));
    saveAsHtmlRulesAction->setText(qApp->tr("&Export html..."));
    saveAsHtmlRulesAction->setStatusTip(qApp->tr("Exporter les règles de gestion sélectionnées vers un fichier html"));
    return saveAsHtmlRulesAction;
}

/*
 * CAMPAIGNS
 */

AbstractAction* ActionsFactory::createInsertCampaignAction(QObject* parent){
    ContextMenuActions::AbstractAction *insertCampaignAction = new ContextMenuActions::CampaignHierarchy::InsertCampaignAction(parent);
    insertCampaignAction->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    insertCampaignAction->setText(qApp->tr("&Nouvelle campagne..."));
    insertCampaignAction->setStatusTip(qApp->tr("Ajouter une nouvelle campagne de tests"));
    return insertCampaignAction;
}

AbstractAction* ActionsFactory::createRemoveCampaignAction(QObject* parent){
    ContextMenuActions::AbstractAction *removeCampaignAction = new ContextMenuActions::CampaignHierarchy::RemoveCampaignAction(parent);
    removeCampaignAction->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    removeCampaignAction->setText(qApp->tr("&Supprimer la campagne sélectionnée"));
    removeCampaignAction->setStatusTip(qApp->tr("Supprimer la campagne de tests sélectionnée"));
    return removeCampaignAction;
}

/*
 * PROJECTS
 */

AbstractAction* ActionsFactory::createInsertProjectAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionInsert_projet = new ContextMenuActions::ProjectsHierarchy::InsertProjectAction(parent);
    actionInsert_projet->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    actionInsert_projet->setText(qApp->tr("&Créer un nouveau projet"));
    actionInsert_projet->setStatusTip(qApp->tr("Créer un nouveau projet"));
    return actionInsert_projet;
}


AbstractAction* ActionsFactory::createImportProjectAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionImporter_projet = new ContextMenuActions::ProjectsHierarchy::ImportProjectAction(parent);
    actionImporter_projet->setIcon(QIcon(":/images/22x22/actions/import.png"));
    actionImporter_projet->setText(qApp->tr("&Importer un projet"));
    actionImporter_projet->setStatusTip(qApp->tr("Importer un projet à partir d'un fichier au format Xml"));
    return actionImporter_projet;
}

AbstractAction* ActionsFactory::createProjectsReportsAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionProjectsReports = new ContextMenuActions::ProjectsHierarchy::ShowProjectsReportAction(parent);
    actionProjectsReports->setIcon(QIcon(":/images/22x22/stats.png"));
    actionProjectsReports->setText(qApp->tr("&Statistiques des projets"));
    actionProjectsReports->setStatusTip(qApp->tr("Afficher un rapport sur les projects"));
    return actionProjectsReports;
}


AbstractAction* ActionsFactory::createInsertProjectVersionAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionInsert_projetVersion = new ContextMenuActions::ProjectHierarchy::InsertProjectVersionAction(parent);
    actionInsert_projetVersion->setIcon(QIcon(":/images/22x22/actions/insert.png"));
    actionInsert_projetVersion->setText(qApp->tr("&Créer une nouvelle version"));
    actionInsert_projetVersion->setStatusTip(qApp->tr("Créer une nouvelle version"));
    return actionInsert_projetVersion;
}


AbstractAction* ActionsFactory::createExportProjectAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionExporter_projet = new ContextMenuActions::ProjectHierarchy::ExportProjectAction(parent);
    actionExporter_projet->setIcon(QIcon(":/images/22x22/actions/export.png"));
    actionExporter_projet->setText(qApp->tr("&Exporter le projet"));
    actionExporter_projet->setStatusTip(qApp->tr("Exporter le projet sélectionné au format Xml"));
    return actionExporter_projet;
}

AbstractAction* ActionsFactory::createRemoveProjectAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionRemoveProjet = new ContextMenuActions::ProjectHierarchy::RemoveProjectAction(parent);
    actionRemoveProjet->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    actionRemoveProjet->setText(qApp->tr("&Supprimer le projet sélectionné"));
    actionRemoveProjet->setStatusTip(qApp->tr("Supprimer le projet sélectionné"));
    return actionRemoveProjet;
}


AbstractAction* ActionsFactory::createShowProjectPropertiesAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionShowProjetProperties = new ContextMenuActions::ProjectHierarchy::ShowProjectPropertiesAction(parent);
    actionShowProjetProperties->setIcon(QIcon(":/images/22x22/actions/properties.png"));
    actionShowProjetProperties->setText(qApp->tr("&Propriétés du projet"));
    actionShowProjetProperties->setStatusTip(qApp->tr("Afficher les propriétés du projet sélectionné"));
    return actionShowProjetProperties;
}

/*
 * PROJECTS VERSIONS
 */

AbstractAction* ActionsFactory::createSearchProjectVersionAction(QObject* parent){
    ContextMenuActions::AbstractAction *searchProjectAction = new ContextMenuActions::ProjectVersionHierarchy::SearchAction(parent);
    searchProjectAction->setIcon(QIcon(":/images/22x22/actions/search.png"));
    searchProjectAction->setText(qApp->tr("&Rechercher..."));
    searchProjectAction->setStatusTip(qApp->tr("Rechercher des données de scénarios, d'exigences"));
    return searchProjectAction;
}

AbstractAction* ActionsFactory::createShowProjectVersionBugsAction(QObject* parent){
    ContextMenuActions::AbstractAction *showProjectBugsAction = new ContextMenuActions::ProjectVersionHierarchy::ShowBugsAction(parent);
    showProjectBugsAction->setIcon(QIcon(":/images/22x22/bug.png"));
    showProjectBugsAction->setText(qApp->tr("&Anomalies"));
    showProjectBugsAction->setStatusTip(qApp->tr("Afficher toutes les anomalies de la version de projet sélectionnée"));
    return showProjectBugsAction;
}

AbstractAction* ActionsFactory::createShowExecutionReportsProjectAction(QObject* parent){
    ContextMenuActions::AbstractAction *executionsReportsAction = new ContextMenuActions::ProjectVersionHierarchy::ShowExecutionsReportsAction(parent);
    executionsReportsAction->setIcon(QIcon(":/images/22x22/stats.png"));
    executionsReportsAction->setText(qApp->tr("&Rapports d'exécutions"));
    executionsReportsAction->setStatusTip(qApp->tr("Afficher un rapport des campagnes d'exécutions du projet sélectionné"));
    return executionsReportsAction;
}

AbstractAction* ActionsFactory::createShowProjectVersionPropertiesAction(QObject* parent){
    ContextMenuActions::AbstractAction *showProjectPropertiesAction = new ContextMenuActions::ProjectVersionHierarchy::ShowPropertiesAction(parent);
    showProjectPropertiesAction->setIcon(QIcon(":/images/22x22/actions/properties.png"));
    showProjectPropertiesAction->setText(qApp->tr("&Propriétés de la version"));
    showProjectPropertiesAction->setStatusTip(qApp->tr("Afficher les propriétés de la version de projet sélectionnée"));
    return showProjectPropertiesAction;
}


AbstractAction* ActionsFactory::createSetAsDefaultProjectVersionAction(QObject* parent){
    ContextMenuActions::AbstractAction *setAsDefaultProjectVersionAction = new ContextMenuActions::ProjectVersionHierarchy::SetAsDefaultProjectVersionAction(parent);
    setAsDefaultProjectVersionAction->setIcon(QIcon(":/images/22x22/actions/default.png"));
    setAsDefaultProjectVersionAction->setText(qApp->tr("&Définir comme version de projet par défaut"));
    setAsDefaultProjectVersionAction->setStatusTip(qApp->tr("Définir comme version de projet par défaut"));
    return setAsDefaultProjectVersionAction;
}

AbstractAction* ActionsFactory::createRemoveProjectVersionAction(QObject* parent){
    ContextMenuActions::AbstractAction *actionRemoveProjetVersion = new ContextMenuActions::ProjectVersionHierarchy::RemoveProjectVersionAction(parent);
    actionRemoveProjetVersion->setIcon(QIcon(":/images/22x22/actions/remove.png"));
    actionRemoveProjetVersion->setText(qApp->tr("&Supprimer la version sélectionnée"));
    actionRemoveProjetVersion->setStatusTip(qApp->tr("Supprimer la version sélectionnée"));
    return actionRemoveProjetVersion;
}


ActionsFactory::ActionsFactory()
{
}

}
}
