#ifndef IMPORTRULESACTION_H
#define IMPORTRULESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

class ImportRulesAction : public AbstractAction
{
public:
    ImportRulesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // IMPORTRULESACTION_H
