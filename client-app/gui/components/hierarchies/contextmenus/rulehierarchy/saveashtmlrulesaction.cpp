#include "saveashtmlrulesaction.h"

#include "gui/services/rulesservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

SaveAsHtmlRulesAction::SaveAsHtmlRulesAction(QObject *parent) : AbstractAction(parent)
{
}

bool SaveAsHtmlRulesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Rules::saveRulesAsHtml(in_record_tree_view->selectedRecords<Rule>());

    return true;
}


}
}
}
