#ifndef SAVEASHTMLRULESACTION_H
#define SAVEASHTMLRULESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

class SaveAsHtmlRulesAction : public AbstractAction
{
public:
    SaveAsHtmlRulesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SAVEASHTMLRULESACTION_H
