#include "exportrulesaction.h"

#include "gui/services/rulesservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

ExportRulesAction::ExportRulesAction(QObject* parent) : AbstractAction(parent)
{
}

bool ExportRulesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Rules::exportRules(in_record_tree_view);

    return true;
}


}
}
}
