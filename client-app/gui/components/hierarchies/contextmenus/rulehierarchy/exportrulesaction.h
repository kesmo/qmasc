#ifndef EXPORTRULESACTION_H
#define EXPORTRULESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

class ExportRulesAction : public AbstractAction
{
public:
    ExportRulesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // EXPORTRULESACTION_H
