#include "printrulesaction.h"

#include "gui/services/rulesservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

PrintRulesAction::PrintRulesAction(QObject* parent) : AbstractAction(parent)
{
}

bool PrintRulesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Rules::printRules(in_record_tree_view->selectedRecords<Rule>());

    return true;
}

}
}
}
