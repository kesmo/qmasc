#include "importrulesaction.h"

#include "gui/services/rulesservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

ImportRulesAction::ImportRulesAction(QObject* parent) : AbstractAction(parent)
{
}

bool ImportRulesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Rules::importRules(in_record_tree_view);

    return true;
}

}
}
}
