#ifndef PRINTRULESACTION_H
#define PRINTRULESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace RuleHierarchy
{

class PrintRulesAction : public AbstractAction
{
public:
    PrintRulesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // PRINTRULESACTION_H
