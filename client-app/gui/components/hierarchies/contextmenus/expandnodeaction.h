#ifndef EXPANDNODEACTION_H
#define EXPANDNODEACTION_H

#include "hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{

class ExpandNodeAction : public AbstractAction
{
public:
    ExpandNodeAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}

#endif // EXPANDNODEACTION_H
