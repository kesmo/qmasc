/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "insertcampaignaction.h"

#include "gui/services/campaignsservices.h"
#include "gui/services/projectsservices.h"

#include "gui/forms/FormCampaignWizard.h"

#include "gui/mainwindow.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace CampaignHierarchy
{

InsertCampaignAction::InsertCampaignAction(QObject *parent) : AbstractAction(parent)
{
}

bool InsertCampaignAction::exec(RecordsTreeView *in_record_tree_view)
{
    FormCampaign_Wizard    *tmp_campaign_wizard = new FormCampaign_Wizard(Gui::Services::Projects::projectVersionForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->selectionModel()->currentIndex()), MainWindow::instance());

    m_record_tree_view = in_record_tree_view;

    connect(tmp_campaign_wizard, SIGNAL(campaignCreated(Campaign*)), MainWindow::instance(), SLOT(insertCampaign(Campaign*)));

    tmp_campaign_wizard->show();

    return true;
}


}
}
}
