#ifndef EXPORTFEATURESACTION_H
#define EXPORTFEATURESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

class ExportFeaturesAction : public AbstractAction
{
public:
    ExportFeaturesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // EXPORTFEATURESACTION_H
