#include "saveashtmlfeaturesaction.h"

#include "gui/services/featuresservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

SaveAsHtmlFeaturesAction::SaveAsHtmlFeaturesAction(QObject *parent) : AbstractAction(parent)
{
}

bool SaveAsHtmlFeaturesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Features::saveFeaturesAsHtml(in_record_tree_view->selectedRecords<Feature>());

    return true;
}


}
}
}
