#include "importfeaturesaction.h"

#include "gui/services/featuresservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

ImportFeaturesAction::ImportFeaturesAction(QObject* parent) : AbstractAction(parent)
{
}

bool ImportFeaturesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Features::importFeatures(in_record_tree_view);

    return true;
}

}
}
}
