#ifndef IMPORTFEATURESACTION_H
#define IMPORTFEATURESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

class ImportFeaturesAction : public AbstractAction
{
public:
    ImportFeaturesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // IMPORTFEATURESACTION_H
