/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "insertchildfeatureaction.h"
#include "gui/services/services.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

InsertChildFeatureAction::InsertChildFeatureAction(QObject* parent) : AbstractAction(parent)
{
}

bool InsertChildFeatureAction::exec(RecordsTreeView *in_record_tree_view)
{
    QModelIndex index = in_record_tree_view->selectionModel()->currentIndex();
    Hierarchy *tmp_item = NULL;
    int tmp_row = 0;

    if (index.isValid())
    {
        tmp_item = in_record_tree_view->getRecordsTreeModel()->getItem(index);

        if (tmp_item != NULL)
            tmp_row = tmp_item->childCount();

        if (!in_record_tree_view->getRecordsTreeModel()->insertRow(tmp_row, index))
            return false;

        Gui::Services::Global::selectIndex(in_record_tree_view, in_record_tree_view->getRecordsTreeModel()->index(tmp_row, 0, index));
    }

    return true;
}

}
}
}
