#include "printfeaturesaction.h"

#include "gui/services/featuresservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

PrintFeaturesAction::PrintFeaturesAction(QObject* parent) : AbstractAction(parent)
{
}

bool PrintFeaturesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Features::printFeatures(in_record_tree_view->selectedRecords<Feature>());

    return true;
}

}
}
}
