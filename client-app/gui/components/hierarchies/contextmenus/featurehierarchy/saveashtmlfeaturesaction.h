#ifndef SAVEASHTMLFEATURESACTION_H
#define SAVEASHTMLFEATURESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

class SaveAsHtmlFeaturesAction : public AbstractAction
{
public:
    SaveAsHtmlFeaturesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SAVEASHTMLFEATURESACTION_H
