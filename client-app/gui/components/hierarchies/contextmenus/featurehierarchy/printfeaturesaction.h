#ifndef PRINTFEATURESACTION_H
#define PRINTFEATURESACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

class PrintFeaturesAction : public AbstractAction
{
public:
    PrintFeaturesAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // PRINTFEATURESACTION_H
