#include "exportfeaturesaction.h"

#include "gui/services/featuresservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace FeatureHierarchy
{

ExportFeaturesAction::ExportFeaturesAction(QObject* parent) : AbstractAction(parent)
{
}

bool ExportFeaturesAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Features::exportFeatures(in_record_tree_view);

    return true;
}


}
}
}
