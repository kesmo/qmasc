#include "removeprojectversionaction.h"

#include "gui/services/projectsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectVersionHierarchy
{

RemoveProjectVersionAction::RemoveProjectVersionAction(QObject *parent) : AbstractAction(parent)
{
}

bool RemoveProjectVersionAction::exec(RecordsTreeView *in_record_tree_view)
{
    ProjectVersion *projectVersion = Gui::Services::Projects::projectVersionForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->currentIndex());
    Gui::Services::Projects::removeProjectVersion(in_record_tree_view->getRecordsTreeModel(), projectVersion);

    return true;
}



}
}
}
