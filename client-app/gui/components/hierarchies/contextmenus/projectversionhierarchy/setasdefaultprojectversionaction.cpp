#include "setasdefaultprojectversionaction.h"

#include "gui/services/projectsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectVersionHierarchy
{

SetAsDefaultProjectVersionAction::SetAsDefaultProjectVersionAction(QObject *parent) : AbstractAction(parent)
{
}

bool SetAsDefaultProjectVersionAction::exec(RecordsTreeView *in_record_tree_view)
{
    ProjectVersion* projectVersion = Gui::Services::Projects::projectVersionForIndex(in_record_tree_view->getRecordsTreeModel(), in_record_tree_view->currentIndex());
    if (projectVersion){
        Gui::Services::Projects::setDefaultProjectVersion(projectVersion);
    }
    return true;
}

}
}
}
