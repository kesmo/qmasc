#ifndef REMOVEPROJECTVERSIONACTION_H
#define REMOVEPROJECTVERSIONACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectVersionHierarchy
{

class RemoveProjectVersionAction : public AbstractAction
{
public:
    RemoveProjectVersionAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);

};

}
}
}

#endif // REMOVEPROJECTVERSIONACTION_H
