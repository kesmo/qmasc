#ifndef SETASDEFAULTPROJECTVERSIONACTION_H
#define SETASDEFAULTPROJECTVERSIONACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

#include "entities/projectversion.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace ProjectVersionHierarchy
{

class SetAsDefaultProjectVersionAction : public AbstractAction
{
public:
    SetAsDefaultProjectVersionAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SETASDEFAULTPROJECTVERSIONACTION_H
