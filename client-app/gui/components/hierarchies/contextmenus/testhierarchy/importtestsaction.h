#ifndef IMPORTTESTSACTION_H
#define IMPORTTESTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

class ImportTestsAction : public AbstractAction
{
public:
    ImportTestsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // IMPORTTESTSACTION_H
