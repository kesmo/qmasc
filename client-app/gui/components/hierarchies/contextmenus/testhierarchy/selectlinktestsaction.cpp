#include "selectlinktestsaction.h"
#include "gui/services/testsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

SelectLinkTestsAction::SelectLinkTestsAction(QObject *parent) : AbstractAction(parent)
{
}

bool SelectLinkTestsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Tests::selectLinkTests(in_record_tree_view, in_record_tree_view->selectedRecords<Test>());

    return true;
}

}
}
}
