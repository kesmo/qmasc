#include "saveashtmltestsaction.h"

#include "gui/services/testsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

SaveAsHtmlTestsAction::SaveAsHtmlTestsAction(QObject* parent) : AbstractAction(parent)
{
}

bool SaveAsHtmlTestsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Tests::saveTestsAsHtml(in_record_tree_view->selectedRecords<Test>());

    return true;
}


}
}
}
