#ifndef SAVEASHTMLTESTSACTION_H
#define SAVEASHTMLTESTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

class SaveAsHtmlTestsAction : public AbstractAction
{
public:
    SaveAsHtmlTestsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SAVEASHTMLTESTSACTION_H
