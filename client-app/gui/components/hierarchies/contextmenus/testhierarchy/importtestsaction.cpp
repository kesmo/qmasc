#include "importtestsaction.h"

#include "gui/services/testsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

ImportTestsAction::ImportTestsAction(QObject *parent) : AbstractAction(parent)
{
}

bool ImportTestsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Tests::importTests(in_record_tree_view);

    return true;
}

}
}
}
