#ifndef SELECTLINKTESTSACTION_H
#define SELECTLINKTESTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{


class SelectLinkTestsAction : public AbstractAction
{
public:
    SelectLinkTestsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SELECTLINKTESTSACTION_H
