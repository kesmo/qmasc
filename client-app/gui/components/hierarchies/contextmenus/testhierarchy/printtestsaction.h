#ifndef PRINTTESTSACTION_H
#define PRINTTESTSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

class PrintTestsAction : public AbstractAction
{
public:
    PrintTestsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // PRINTTESTSACTION_H
