#include "exporttestsaction.h"

#include "gui/services/testsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

ExportTestsAction::ExportTestsAction(QObject *parent) : AbstractAction(parent)
{
}

bool ExportTestsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Tests::exportTests(in_record_tree_view);

    return true;
}


}
}
}
