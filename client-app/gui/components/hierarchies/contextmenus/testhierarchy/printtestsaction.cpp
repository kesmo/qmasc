#include "printtestsaction.h"

#include "gui/services/testsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace TestHierarchy
{

PrintTestsAction::PrintTestsAction(QObject *parent) : AbstractAction(parent)
{
}

bool PrintTestsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Tests::printTests(in_record_tree_view->selectedRecords<Test>());

    return true;
}

}
}
}
