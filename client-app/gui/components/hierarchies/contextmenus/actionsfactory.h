/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef ACTIONSFACTORY_H
#define ACTIONSFACTORY_H

#include "hierarchycontextmenuaction.h"

#include <QMap>

namespace Hierarchies
{
namespace ContextMenuActions
{


class ActionsFactory
{
public:
    enum ActionType {
        // Tests actions
        InsertTest = 0x0001,
        InsertAutomatedTest = 0x0002,
        InsertChildTest = 0x0003,
        InsertChildAutomatedTest = 0x0004,
        RemoveTest = 0x1005,
        ExpandTest = 0x1006,
        CollapseTest = 0x1007,
        SelectLinkTest = 0x1008,
        ImportTest = 0x0009,
        ExportTest = 0x100A,
        PrintTest = 0x100B,
        SaveTestAsHtml = 0x100C,

        // Requirements actions
        InsertRequirement = 0x0021,
        InsertChildRequirement = 0x0022,
        RemoveRequirement = 0x1023,
        ExpandRequirement = 0x1024,
        CollapseRequirement = 0x1025,
        SelectDepedantTests = 0x1026,
        ImportRequirement = 0x0027,
        ExportRequirement = 0x1028,
        PrintRequirement = 0x1029,
        SaveRequirementAsHtml = 0x102A,

        AssociateTestsAndRequirements = 0x2F00,

        // Need
        InsertNeed = 0x0030,
        InsertChildNeed = 0x0031,
        RemoveNeed = 0x1032,
        ExpandNeed = 0x1033,
        CollapseNeed = 0x1034,
        ImportNeed = 0x0035,
        ExportNeed = 0x1036,
        PrintNeed = 0x1037,
        SaveNeedAsHtml = 0x1038,

        // Rule
        InsertRule = 0x0040,
        InsertChildRule = 0x0041,
        RemoveRule = 0x1042,
        ExpandRule = 0x1043,
        CollapseRule = 0x1044,
        ImportRule = 0x0045,
        ExportRule = 0x1046,
        PrintRule = 0x1047,
        SaveRuleAsHtml = 0x1048,

        // Feature
        InsertFeature = 0x0050,
        InsertChildFeature = 0x0051,
        RemoveFeature = 0x1052,
        ExpandFeature = 0x1053,
        CollapseFeature = 0x1054,
        ImportFeature = 0x0055,
        ExportFeature = 0x1056,
        PrintFeature = 0x1057,
        SaveFeatureAsHtml = 0x1058,

        // Campaigns actions
        InsertCampaign = 0x0060,
        RemoveCampaign = 0x1061,


        // Projects actions
        InsertProject = 0x0070,
        ImportProject = 0x0071,
        ShowProjectsReportsAction = 0x0072,

        ShowProjectPropertiesAction = 0x0080,
        ExportProject = 0x0081,
        RemoveProject = 0x0082,
        InsertProjectVersion = 0x0083,
        ShowExecutionReportsProjectAction = 0x0084,

        // Projects versions actions
        SearchProjectVersionAction = 0x0090,
        ShowProjectVersionBugsAction = 0x0091,
        ShowProjectVersionPropertiesAction = 0x0092,
        SetAsDefaultProjectVersionAction = 0x0093,
        RemoveProjectVersion = 0x0094,

        SeparatorAction = 0x0F00,

        HomogeneousMultipleTargetActionMask = 0x1000,
        HeterogeneousMultipleTargetActionMask = 0x2000,

        DisableActionMask = 0x4000
    };

    static ActionsFactory* instance();

    QMenu* createMenuForActionsTypes(QList<ActionType> actionsTypesList);

private:
    ActionsFactory();

    AbstractAction* createAction(ActionType actionType, QObject* parent = NULL);

    static ActionsFactory* m_instance;

    typedef AbstractAction* (*CreateActionFunction)(QObject* parent);

    static QMap<ActionType, CreateActionFunction> m_create_action_functions_map;

    static AbstractAction* createSeparatorAction(QObject* parent);

    static AbstractAction* createInsertTestAction(QObject* parent);
    static AbstractAction* createInsertAutomatedTestAction(QObject* parent);
    static AbstractAction* createInsertChildTestAction(QObject* parent);
    static AbstractAction* createInsertChildAutomatedTestAction(QObject* parent);
    static AbstractAction* createRemoveTestAction(QObject* parent);
    static AbstractAction* createExpandTestNodeAction(QObject* parent);
    static AbstractAction* createCollapseTestNodeAction(QObject* parent);
    static AbstractAction* createSelectLinkTestsAction(QObject* parent);
    static AbstractAction* createImportTestsAction(QObject* parent);
    static AbstractAction* createExportTestsAction(QObject* parent);
    static AbstractAction* createPrintTestsAction(QObject* parent);
    static AbstractAction* creatSaveAsHtmlTestsAction(QObject* parent);

    static AbstractAction* createInsertRequirementAction(QObject* parent);
    static AbstractAction* createInsertChildRequirementAction(QObject* parent);
    static AbstractAction* createRemoveRequirementAction(QObject* parent);
    static AbstractAction* createExpandRequirementNodeAction(QObject* parent);
    static AbstractAction* createCollapseRequirementNodeAction(QObject* parent);
    static AbstractAction* createSelectDepedantTestsAction(QObject* parent);
    static AbstractAction* createImportRequirementsAction(QObject* parent);
    static AbstractAction* createExportRequirementsAction(QObject* parent);
    static AbstractAction* createPrintRequirementsAction(QObject* parent);
    static AbstractAction* creatSaveAsHtmlRequirementsAction(QObject* parent);

    static AbstractAction* creatAssociateTestsAndRequirementsAction(QObject* parent);

    static AbstractAction* createInsertNeedAction(QObject* parent);
    static AbstractAction* createInsertChildNeedAction(QObject* parent);
    static AbstractAction* createRemoveNeedAction(QObject* parent);
    static AbstractAction* createExpandNeedNodeAction(QObject* parent);
    static AbstractAction* createCollapseNeedNodeAction(QObject* parent);
    static AbstractAction* createImportNeedsAction(QObject* parent);
    static AbstractAction* createExportNeedsAction(QObject* parent);
    static AbstractAction* createPrintNeedsAction(QObject* parent);
    static AbstractAction* creatSaveAsHtmlNeedsAction(QObject* parent);

    static AbstractAction* createInsertFeatureAction(QObject* parent);
    static AbstractAction* createInsertChildFeatureAction(QObject* parent);
    static AbstractAction* createRemoveFeatureAction(QObject* parent);
    static AbstractAction* createExpandFeatureNodeAction(QObject* parent);
    static AbstractAction* createCollapseFeatureNodeAction(QObject* parent);
    static AbstractAction* createImportFeaturesAction(QObject* parent);
    static AbstractAction* createExportFeaturesAction(QObject* parent);
    static AbstractAction* createPrintFeaturesAction(QObject* parent);
    static AbstractAction* creatSaveAsHtmlFeaturesAction(QObject* parent);

    static AbstractAction* createInsertRuleAction(QObject* parent);
    static AbstractAction* createInsertChildRuleAction(QObject* parent);
    static AbstractAction* createRemoveRuleAction(QObject* parent);
    static AbstractAction* createExpandRuleNodeAction(QObject* parent);
    static AbstractAction* createCollapseRuleNodeAction(QObject* parent);
    static AbstractAction* createImportRulesAction(QObject* parent);
    static AbstractAction* createExportRulesAction(QObject* parent);
    static AbstractAction* createPrintRulesAction(QObject* parent);
    static AbstractAction* creatSaveAsHtmlRulesAction(QObject* parent);

    static AbstractAction* createInsertCampaignAction(QObject* parent);
    static AbstractAction* createRemoveCampaignAction(QObject* parent);

    static AbstractAction* createInsertProjectAction(QObject* parent);
    static AbstractAction* createImportProjectAction(QObject* parent);
    static AbstractAction* createProjectsReportsAction(QObject* parent);

    static AbstractAction* createShowProjectPropertiesAction(QObject* parent);
    static AbstractAction* createExportProjectAction(QObject* parent);
    static AbstractAction* createInsertProjectVersionAction(QObject* parent);
    static AbstractAction* createRemoveProjectAction(QObject* parent);
    static AbstractAction* createShowExecutionReportsProjectAction(QObject* parent);

    static AbstractAction* createSearchProjectVersionAction(QObject* parent);
    static AbstractAction* createShowProjectVersionBugsAction(QObject* parent);
    static AbstractAction* createShowProjectVersionPropertiesAction(QObject* parent);
    static AbstractAction* createSetAsDefaultProjectVersionAction(QObject* parent);
    static AbstractAction* createRemoveProjectVersionAction(QObject* parent);



};

}
}

#endif // ACTIONSFACTORY_H
