#ifndef PRINTNEEDSACTION_H
#define PRINTNEEDSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

class PrintNeedsAction : public AbstractAction
{
public:
    PrintNeedsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // PRINTNEEDSACTION_H
