#include "exportneedsaction.h"

#include "gui/services/needsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

ExportNeedsAction::ExportNeedsAction(QObject* parent) : AbstractAction(parent)
{
}

bool ExportNeedsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Needs::exportNeeds(in_record_tree_view);

    return true;
}


}
}
}
