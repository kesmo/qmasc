#ifndef EXPORTNEEDSACTION_H
#define EXPORTNEEDSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

class ExportNeedsAction : public AbstractAction
{
public:
    ExportNeedsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // EXPORTNEEDSACTION_H
