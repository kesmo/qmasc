#ifndef IMPORTNEEDSACTION_H
#define IMPORTNEEDSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

class ImportNeedsAction : public AbstractAction
{
public:
    ImportNeedsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // IMPORTNEEDSACTION_H
