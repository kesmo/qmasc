#include "printneedsaction.h"

#include "gui/services/needsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

PrintNeedsAction::PrintNeedsAction(QObject* parent) : AbstractAction(parent)
{
}

bool PrintNeedsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Needs::printNeeds(in_record_tree_view->selectedRecords<Need>());

    return true;
}

}
}
}
