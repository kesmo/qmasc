#include "importneedsaction.h"

#include "gui/services/needsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

ImportNeedsAction::ImportNeedsAction(QObject* parent) : AbstractAction(parent)
{
}

bool ImportNeedsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Needs::importNeeds(in_record_tree_view);

    return true;
}

}
}
}
