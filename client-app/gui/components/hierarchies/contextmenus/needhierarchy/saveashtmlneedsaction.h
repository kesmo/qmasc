#ifndef SAVEASHTMLNEEDSACTION_H
#define SAVEASHTMLNEEDSACTION_H

#include "gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

class SaveAsHtmlNeedsAction : public AbstractAction
{
public:
    SaveAsHtmlNeedsAction(QObject* parent = NULL);

    virtual bool exec(RecordsTreeView *in_record_tree_view);
};

}
}
}

#endif // SAVEASHTMLNEEDSACTION_H
