#include "saveashtmlneedsaction.h"

#include "gui/services/needsservices.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
namespace NeedHierarchy
{

SaveAsHtmlNeedsAction::SaveAsHtmlNeedsAction(QObject *parent) : AbstractAction(parent)
{
}

bool SaveAsHtmlNeedsAction::exec(RecordsTreeView *in_record_tree_view)
{
    Gui::Services::Needs::saveNeedsAsHtml(in_record_tree_view->selectedRecords<Need>());

    return true;
}


}
}
}
