/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef HIERARCHY_H
#define HIERARCHY_H

#include <QVariant>
#include <QList>
#include <QHash>
#include <QPointer>

#include "entities/entity.h"
#include "recordlistener.h"
#include "utilities.h"
#include "contextmenus/hierarchycontextmenuaction.h"
#include "contextmenus/actionsfactory.h"

#include "gui/components/widgets/abstractcolumnrecordwidget.h"

class Hierarchy :
    public QObject,
    public RecordListener,
    public ParentalityListener
{
    Q_OBJECT

private:
    Record* m_record;

    Hierarchy *m_parent;

    QHash<Record*, Hierarchy*> m_childs;


protected:
    void addChildHierarchyForRecord(Hierarchy* child, Record* record);
    void removeChildHierarchyForRecord(Record* record);
    Hierarchy* getChildHierarchyForRecord(Record* record) const;

public:
    Hierarchy(QObject *parent = NULL);
    Hierarchy(Record* record, QObject *parent = NULL);
    virtual ~Hierarchy();

    Record* getRecord() const;

    const char *columnNameForPreviousItem(){return NULL;}
    const char *columnNameForParentItem(){return NULL;}

    /* Pure virtual functions */
    virtual QVariant data(int column, int role = 0) = 0;
    virtual int childCount() = 0;
    virtual Hierarchy* child(int number) = 0;
    /* End pure virtual functions */

    virtual bool insertChildren(int position, int count, Record *in_item = NULL);
    virtual bool insertCopyOfChildren(int position, int count, Record *in_item);
    virtual bool insertLinkOfChildren(int in_index, Record *in_item);
    virtual bool removeChildren(int position, int count, bool in_move_indic = true);

    virtual bool isALink();
    virtual bool isWritable();
    virtual bool isDataEditable();
    virtual bool canMove();
    virtual Hierarchy* parent();
    virtual bool mayPerformChildLookupForRecord(Record* in_record);

    virtual Qt::DropActions possibleDropActionsForRecord(Record* in_record);


    virtual int row();

    virtual bool canInsert(int position);
    virtual bool canRemove(int position);
    virtual bool mayHaveCyclicRedundancy(Hierarchy *in_dest_item, bool in_check_link);

    virtual int dbChildCount();

    virtual QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    static QList<Hierarchy*> parentRecordsFromRecordsList(const QList<Hierarchy*>& in_records_list);

    bool isChildOf(Hierarchy *in_item);
    bool isParentOf(Hierarchy *in_item);

    virtual Hierarchy* findItemWithValueForKey(const char *in_value, const char *in_key, bool in_recursive = true);
    virtual int indexForItem(Hierarchy *in_item);

    virtual QList<Record*>* orginalsRecordsForRelationsEntity(Record *in_item = NULL);

    void setParentHierarchy(Hierarchy* parent);

    virtual void recordDestroyed(AbstractRecord* record);

    virtual void updateChilds(int in_old_rows_count = 0, int in_new_rows_count = 0);

    virtual void userSelectItem();

    virtual Gui::Components::Widgets::AbstractColumnRecordWidget* createCustomWidgetEditorForColumn(int column);

    virtual bool applyChanges();
    virtual void revertChanges();

Q_SIGNALS:
    void childrenChanged(Hierarchy* hierarchy, int in_old_rows_count, int in_new_rows_count);

};

template<typename Rel>
class EntityNode: public Hierarchy
{
public:
    EntityNode(typename Rel::ParentClass* record, QObject *parent = NULL) : Hierarchy(record, parent)
    { }

    typename Rel::ParentClass* getEntity()
    {
        return dynamic_cast<Rel::ParentClass*>(getRecord());
    }

    virtual Hierarchy *child(int number)
    {
        Rel::ChildClass* child = Rel::instance().getChildAtIndex(getEntity(), number);
        Hierarchy* childHierarchy = getChildHierarchyForRecord(child);
        if (childHierarchy == NULL) {
            childHierarchy = createChildHierarchy(child);
            addChildHierarchyForRecord(childHierarchy, child);
        }

        return childHierarchy;

    }


    virtual int childCount()
    {
        return Rel::instance().getChilds(getEntity()).count();
    }

    virtual QVariant data(int /* column */, int /* role */)
    {
        return QVariant();
    }

protected:
    virtual Hierarchy* createChildHierarchy(typename Rel::ChildClass* child) = 0;
};

#endif // HIERARCHY_H
