#include "projectparametershierarchy.h"

namespace Hierarchies
{


ProjectParametersHierarchy::ProjectParametersHierarchy(ProjectVersion *projectVersion, QObject *parent /*= NULL*/) :
    NoChildrenHierarchy(tr("Paramètres"), QIcon(QPixmap(QString::fromUtf8(":/images/22x22/parameters.png"))), projectVersion, false)
{
}


void ProjectParametersHierarchy::userSelectItem()
{
}


}
