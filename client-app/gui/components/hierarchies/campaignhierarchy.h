#ifndef CAMPAIGNHIERARCHY_H
#define CAMPAIGNHIERARCHY_H

#include "hierarchy.h"

#include "entities/campaign.h"

namespace Hierarchies
{

class CampaignHierarchy :
    public EntityNode<Campaign::TestCampaignRelation>
{
public:
    CampaignHierarchy(Campaign* campaign, bool in_show_shilds = false, QObject *parent = NULL);
    Hierarchy* createChildHierarchy(TestCampaign* child);

    bool insertChildren(int position, int count, Record *in_item = NULL);
    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool removeChildren(int position, int count, bool in_move_indic = true);

    QVariant data(int column, int in_role = 0);

    int childCount();
    QList<Record*>* orginalsRecordsForRelationsEntity(Record *in_item = NULL);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    virtual void userSelectItem();

private:
    bool    m_show_childs;
};

}

#endif // CAMPAIGNHIERARCHY_H
