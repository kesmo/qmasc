#ifndef PROJECTS_HIERARCHY_H
#define PROJECTS_HIERARCHY_H

#include "hierarchy.h"
#include "entities/project.h"

class ProjectsHierarchy :
        public Hierarchy
{
public:
    ProjectsHierarchy();
    ~ProjectsHierarchy();

    const QList<Project*>& projects();

    virtual QVariant data(int column, int role = 0);

    virtual int childCount();
    virtual Hierarchy* child(int number);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    void addProject(Project* project);
    void removeProject(Project* project);

    bool isWritable();
    bool removeChildren(int position, int count, bool in_move_indic);

private:
    QList<Project*> m_projects_list;
    bool            m_projects_loaded;
};

#endif // PROJECTS_HIERARCHY_H
