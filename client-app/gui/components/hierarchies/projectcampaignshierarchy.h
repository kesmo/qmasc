#ifndef PROJECTCAMPAIGNSHIERARCHY_H
#define PROJECTCAMPAIGNSHIERARCHY_H

#include "hierarchy.h"

#include "entities/projectversion.h"

namespace Hierarchies
{


class ProjectCampaignsHierarchy :
    public EntityNode<ProjectVersion::CampaignRelation>
{
public:
    ProjectCampaignsHierarchy(ProjectVersion* projectVersion);
    Hierarchy* createChildHierarchy(Campaign* child);

    QVariant data(int column, int role = 0);

    bool insertChildren(int position, int /*count*/, Record *in_child);
    bool removeChildren(int position, int count, bool in_move_indic = false);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

};

}
#endif // PROJECTCAMPAIGNSHIERARCHY_H
