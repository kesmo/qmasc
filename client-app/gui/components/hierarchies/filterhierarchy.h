#ifndef FILTERHIERARCHY_H
#define FILTERHIERARCHY_H

#include "hierarchy.h"

namespace Hierarchies
{

class FilterHierarchy :
        public Hierarchy
{
public:
    FilterHierarchy(Hierarchy* parent, const QString& label, const QString& icon);

    virtual bool insertChildren(int position, int count, Record *in_item = NULL);
    virtual bool insertCopyOfChildren(int position, int count, Record *in_item);
    virtual bool removeChildren(int position, int count, bool in_move_indic = true);

    virtual bool isWritable();
    virtual bool canInsert(int position);
    virtual bool canRemove(int position);
    virtual bool canMove();

    virtual QVariant data(int column, int role = 0);

    virtual int childCount();
    virtual Hierarchy *child(int number);

private:
    QString mLabel;
    QString mIcon;

};

}
#endif // FILTERHIERARCHY_H
