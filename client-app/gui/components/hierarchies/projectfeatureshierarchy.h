#ifndef PROJECTFEATURESHIERARCHY_H
#define PROJECTFEATURESHIERARCHY_H

#include "hierarchy.h"
#include "entities/projectversion.h"


namespace Hierarchies
{

class ProjectFeaturesHierarchy :
    public EntityNode<ProjectVersion::FeatureRelation>
{
public:
    ProjectFeaturesHierarchy(ProjectVersion* projectVersion);
    Hierarchy* createChildHierarchy(Feature* child);

    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool insertChildren(int position, int count, Record *in_child = NULL);
    bool removeChildren(int position, int count, bool in_move_indic = true);

    bool isWritable();

    QVariant data(int column, int role = 0);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    Qt::DropActions possibleDropActionsForRecord(Record* in_record);

};

}

#endif // PROJECTFEATURESHIERARCHY_H
