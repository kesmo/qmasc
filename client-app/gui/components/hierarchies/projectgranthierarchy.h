#ifndef PROJECTGRANTHIERARCHY_H
#define PROJECTGRANTHIERARCHY_H

#include "hierarchy.h"

#include "entities/projectgrant.h"

namespace Hierarchies
{

class ProjectGrantHierarchy :
        public Hierarchy
{
public:
    ProjectGrantHierarchy(ProjectGrant* projectGrant);

    QVariant data(int column, int role = 0);

    Hierarchy* child(int number);
    int childCount();

    Gui::Components::Widgets::AbstractColumnRecordWidget* createCustomWidgetEditorForColumn(int column);

};

}

#endif // PROJECTGRANTHIERARCHY_H
