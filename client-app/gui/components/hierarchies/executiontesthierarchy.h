#ifndef EXECUTIONTESTHIERARCHY_H
#define EXECUTIONTESTHIERARCHY_H

#include "hierarchy.h"

#include "entities/executiontest.h"

namespace Hierarchies
{

class ExecutionTestHierarchy :
    public EntityNode<ExecutionTest::ChildRelation>
{
public:
    ExecutionTestHierarchy(ExecutionTest* executionTest);

    QVariant data(int column, int role = 0);

    Hierarchy* createChildHierarchy(ExecutionTest* child);
};

}
#endif // EXECUTIONTESTHIERARCHY_H
