/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef TEST_HIERARCHY_H
#define TEST_HIERARCHY_H

#include <QVariant>
#include "hierarchy.h"
#include "entities/test.h"

namespace Hierarchies
{
namespace ContextMenuActions
{
    class AbstractAction;
}
}

namespace Hierarchies
{
    class TestHierarchy :
            public Hierarchy
    {

    public:
        TestHierarchy(Test* in_record);

        ~TestHierarchy();

        Test* test() const {return dynamic_cast<Test*>(getRecord());}

            // Hierarchy methods
        bool insertCopyOfChildren(int position, int count, Record *in_item);
        bool insertLinkOfChildren(int in_index, Record * in_item);
        bool insertChildren(int in_index, int count, Record *in_child = NULL);
        bool removeChildren(int in_index, int count, bool in_move_indic = true);


        QVariant data(int column, int role = 0);

        bool isALink();
        bool isWritable();
        bool canMove();

        bool mayHaveCyclicRedundancy(Hierarchy *in_dest_item, bool in_check_link);

        Hierarchy* child(int number);
        int childCount();
        int dbChildCount();

        QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();
        Qt::DropActions possibleDropActionsForRecord(Record* in_record);

        bool isChildOfLinkTestHierarchy();

        virtual void userSelectItem();

    };
}


#endif // TEST_HIERARCHY_H
