#ifndef PROJECTNEEDSHIERARCHY_H
#define PROJECTNEEDSHIERARCHY_H

#include "hierarchy.h"

#include "entities/project.h"

namespace Hierarchies
{


class ProjectNeedsHierarchy :
    public EntityNode<Project::NeedRelation>
{
public:
    ProjectNeedsHierarchy(Project* project);
    Hierarchy* createChildHierarchy(Need* child);

    bool insertCopyOfChildren(int position, int count, Record *in_item);
    bool insertChildren(int position, int count, Record *in_child = NULL);
    bool removeChildren(int position, int count, bool in_move_indic = true);

    bool isWritable();

    virtual QVariant data(int column, int role = 0);

    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    Qt::DropActions possibleDropActionsForRecord(Record* in_record);

};

}

#endif // PROJECTNEEDSHIERARCHY_H
