#ifndef NOCHILDRENHIERARCHY_H
#define NOCHILDRENHIERARCHY_H

#include "hierarchy.h"

namespace Hierarchies
{


class NoChildrenHierarchy :
        public Hierarchy
{
public:
    NoChildrenHierarchy(const QString& label, const QIcon &icon, QObject *parent = NULL);
    NoChildrenHierarchy(const QString& label, const QIcon &icon, Record* record, QObject *parent = NULL);

    virtual QVariant data(int column, int role = 0);
    virtual int childCount(){return 0;}
    virtual Hierarchy* child(int /*number*/){return NULL;}

private:
    QString m_label;
    QIcon   m_icon;
};

}

#endif // NOCHILDRENHIERARCHY_H
