#include "needhierarchy.h"
#include "gui/services/needsservices.h"
#include "session.h"

namespace Hierarchies
{


NeedHierarchy::NeedHierarchy(Need *need) : EntityNode(need)
{
}

Hierarchy* NeedHierarchy::createChildHierarchy(Need* child)
{
    return new NeedHierarchy(child);
}

bool NeedHierarchy::insertCopyOfChildren(int position, int count, Record * in_item)
{
    Need         *tmp_need = NULL;
    net_session *tmp_session = Session::instance().getClientSession();
    char                ***tmp_results = NULL;

    unsigned long tmp_rows_count, tmp_columns_count;

    bool                        tmp_return = false;

    if (in_item != NULL)
    {
        if (in_item->getEntityDefSignatureId() == NEEDS_TABLE_SIG_ID)
        {
            net_session_print_query(tmp_session, "select create_need_from_need(%s, %s, %s, %s, NULL);",
                    getEntity()->project()->getIdentifier(),
                    getEntity()->getIdentifier(),
                    (position > 0 ? getEntity()->getAllChilds()[position -1]->getIdentifier() : "NULL"),
                    in_item->getIdentifier());
            tmp_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
            if (tmp_results != NULL)
            {
                if (tmp_rows_count == 1 && tmp_columns_count == 1)
                {
                    if (is_empty_string(tmp_results[0][0]) == FALSE)
                    {
                        tmp_need = new Need(getEntity());
                        if (tmp_need->loadRecord(tmp_results[0][0]) == NOERR)
                        {
                            tmp_return = insertChildren(position, count, tmp_need);
                        }
                    }
                }

                cl_free_rows_columns_array(&tmp_results, tmp_rows_count, tmp_columns_count);
            }
        }
    }

    return tmp_return;
}


bool NeedHierarchy::insertChildren(int in_index, int /* count */, Record *in_need)
{
    Need *tmp_need = NULL;

    if (in_need == NULL)
    {
        tmp_need = new Need(getEntity()->project());
    }
    else
        tmp_need = dynamic_cast<Need*>(in_need);

    tmp_need->setProject(getEntity()->project());
    getEntity()->insertChild(in_index, tmp_need);
    return getEntity()->saveChilds() == NOERR;
}


bool NeedHierarchy::removeChildren(int in_index, int count, bool in_move_indic)
{
    if (in_move_indic == false)
        getEntity()->removeChildAtIndex(in_index);
    else
        getEntity()->detachChildAtIndex(in_index);

    return getEntity()->saveChilds() == NOERR;
}


bool NeedHierarchy::isWritable()
{
    return getEntity()->project()->canWriteNeeds();
}

/**
  Renvoie la valeur de la colonne <i>column</i>
**/
QVariant NeedHierarchy::data(int /*column*/, int in_role)
{
    switch (in_role)
    {
    /* Icone */
    case Qt::DecorationRole:
        switch (getEntity()->lockRecordStatus())
        {
        case RECORD_STATUS_OUT_OF_SYNC:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/need.png")));
            break;

        case RECORD_STATUS_LOCKED:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/need.png")));
            break;

        case RECORD_STATUS_BROKEN:
            return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/need.png")));
            break;

        default:
            if(childCount() > 0)
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/needs.png")));
            else
                return QIcon(QPixmap(QString::fromUtf8(":/images/22x22/need.png")));
        }

        break;

        // Renvoie le titre du item courant
    case Qt::ToolTipRole:
    case Qt::DisplayRole:
    case Qt::EditRole:
        return QVariant(QString(getEntity()->getValueForKey(NEEDS_TABLE_SHORT_NAME)));
        break;

    default:
        return QVariant();

    }

    return QVariant();
}



QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> NeedHierarchy::availableContextMenuActionTypes()
{
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> actionsTypesList;

    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::InsertChildNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExpandNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::CollapseNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ImportNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::ExportNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::PrintNeed;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SaveNeedAsHtml;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::SeparatorAction;
    actionsTypesList << Hierarchies::ContextMenuActions::ActionsFactory::RemoveNeed;

    return actionsTypesList;
}


Qt::DropActions NeedHierarchy::possibleDropActionsForRecord(Record* in_record)
{
    switch(in_record->getEntityDef()->m_entity_signature_id){
    case NEEDS_TABLE_SIG_ID:
        if (compare_values(getRecord()->getValueForKey(PROJECTS_TABLE_PROJECT_ID), in_record->getValueForKey(NEEDS_TABLE_PROJECT_ID)) == 0){
            return Qt::CopyAction | Qt::MoveAction;
        }else {
            return Qt::CopyAction;
        }


        break;

    default:
        break;
    }

    return Qt::IgnoreAction;
}


void NeedHierarchy::userSelectItem()
{
    Gui::Services::Needs::showNeedInfos(getEntity());
}

bool NeedHierarchy::canMove()
{
    return getEntity()->project()->canWriteNeeds();
}

}
