#ifndef PROJECTHIERARCHY_H
#define PROJECTHIERARCHY_H

#include "entities/project.h"
#include "hierarchy.h"

class ProjectHierarchy :
        public Hierarchy
{
public:
    ProjectHierarchy(Project* project, QObject *parent = NULL);
    ~ProjectHierarchy();

    Project* project() const {return dynamic_cast<Project*>(getRecord());}
    virtual QVariant data(int column, int role = 0);
    virtual int childCount();
    virtual Hierarchy *child(int number);
    QList<Hierarchies::ContextMenuActions::ActionsFactory::ActionType> availableContextMenuActionTypes();

    void userSelectItem();

private:
    QList<Hierarchy*>            m_generic_hierarchy_list;

};


#endif // PROJECTHIERARCHY_H
