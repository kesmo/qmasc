/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef RECORDTREEMODEL_H
#define RECORDTREEMODEL_H

#include <QList>
#include <QKeyEvent>
#include <QAbstractItemModel>
#include "hierarchies/hierarchy.h"

class RecordsTreeModel : public QAbstractItemModel
{
    Q_OBJECT

private:
    Hierarchy              *m_root_item;

    Qt::DropActions        m_drop_actions;

    QString                    m_mime_type;

    int                    m_columns_count;

    bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());

public:
    RecordsTreeModel();
    RecordsTreeModel(Hierarchy* in_root_item);
    ~RecordsTreeModel();

    void setRootItem(Hierarchy* in_root_item);

    QVariant data(const QModelIndex &index, int role) const;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    void setSupportedDropActions(Qt::DropActions in_drop_actions);
    Qt::DropActions supportedDropActions() const;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
    bool dropRecords(QList<Record *> in_records_list, Qt::DropAction action, int row, const QModelIndex &parent);

    QStringList mimeTypes() const;
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    bool insertItem(int position, const QModelIndex &parent, Record *in_item);
    bool insertLinkForItem(int position, const QModelIndex &parent, Record *in_item);
    bool insertCopyOfItem(int position, const QModelIndex &parent, Record *in_item);
    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    bool insertRow(int row, const QModelIndex &parent = QModelIndex());
    bool removeItem(int position, const QModelIndex &parent = QModelIndex(), bool in_move_indic = false);

    Hierarchy* getItem(const QModelIndex &index = QModelIndex()) const;

    void setMimeType(QString in_mime_type);
    const QString& getMimeType()const { return m_mime_type; }

    QModelIndexList modelIndexesForItemsWithValueForKey(int in_signature, const char *in_value, const char *in_key, const QModelIndex &in_parent = QModelIndex()) const;
    QModelIndexList modelIndexesForItemsWithValuesForKeys(int in_signature, QList<const char *> in_values, QList<const char *> in_keys, const QModelIndex &in_parent = QModelIndex()) const;
    QModelIndexList modelIndexesForRecord(Record *in_record, const QModelIndex &in_parent = QModelIndex()) const;
    QModelIndex modelIndexForRecord(Record *in_record, bool in_by_ref = true, const QModelIndex &in_parent = QModelIndex()) const;

    void setColumnsCount(int count);

    virtual bool submit();
    virtual void revert();

    public Q_SLOTS:
    void updateHierarchy(Hierarchy* hierarchy, int in_old_rows_count, int in_new_rows_count);

Q_SIGNALS:
    void cyclicRedundancy();

    void indexCreated(const QModelIndex& index) const;
};

#endif // RECORDTREEMODEL_H
