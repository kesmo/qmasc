#ifndef PROJECTVERSIONFILTERMODEL_H
#define PROJECTVERSIONFILTERMODEL_H

#include <QAbstractProxyModel>

class ProjectVersionFilterModel : public QAbstractProxyModel
{
    Q_OBJECT
public:
    explicit ProjectVersionFilterModel(QObject *parent = 0);

signals:

public slots:

};

#endif // PROJECTVERSIONFILTERMODEL_H
