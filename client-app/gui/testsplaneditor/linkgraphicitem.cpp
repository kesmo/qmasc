#include "linkgraphicitem.h"
#include "linkpointgraphicitem.h"
#include "entities/testsplan/testsplan.h"

#include <qmath.h>
#include <QPainterPathStroker>
#include <QGraphicsScene>

LinkGraphicItem::LinkGraphicItem(GraphicLink *graphicLink, QGraphicsItem *parent) :
    AbstractLinkGraphicItem(GraphicItem::GraphicLinkRelation::instance().getParent(graphicLink), parent),
    m_graphic_link(graphicLink)
{
}


LinkGraphicItem::~LinkGraphicItem()
{
}

void LinkGraphicItem::addStandardPoints(){
    LinkPointGraphicItem* tmp_start_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::StartPoint);
    LinkPointGraphicItem* tmp_end_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_start_point);

    m_points << tmp_start_point << tmp_end_point;
}

void LinkGraphicItem::setStandardPointsPosition(){
    m_points[1]->setPos(m_points[0]->pos() + QPointF(0, 40));
    update();
}


QPainterPath LinkGraphicItem::shape() const{
    QPainterPath tmp_path;
    QPainterPathStroker tmp_stroker;

    if (m_points.count() == 2){
        tmp_path.moveTo(m_points[0]->pos());

        for (int tmp_index = 1; tmp_index < m_points.count(); ++tmp_index) {
            tmp_path.lineTo(m_points[tmp_index]->pos());
        }
    }

    return tmp_stroker.createStroke(tmp_path);
}


void LinkGraphicItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/){

    if (m_points.count() == 2){
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

        QPolygonF tmp_polygon;

        for (int tmp_index = 0; tmp_index < m_points.count(); ++tmp_index) {
            tmp_polygon << m_points[tmp_index]->pos();
        }

        if (isSelected()){
            painter->setPen(QPen(Qt::blue, 2));
            painter->setBrush(Qt::blue);
        }else if (m_is_highlighted){
            painter->setPen(QPen(Qt::gray, 2));
            painter->setBrush(Qt::gray);
        }else{
            painter->setPen(QPen(Qt::black, 2));
            painter->setBrush(Qt::black);
        }

        painter->drawPolygon(tmp_polygon);

    //    painter->setPen(Qt::red);
    //    painter->setBrush(Qt::NoBrush);
    //    painter->drawRect(boundingRect());
   }
}


int LinkGraphicItem::save(){
    bool new_item = is_empty_string(m_GraphicItem->getIdentifier()) == TRUE;
    m_GraphicItem->setValueForKey(QString(GRAPHICS_ITEMS_TYPE_LINK).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_TYPE);
    int result = AbstractLinkGraphicItem::save();
    if (result == NOERR){
        m_graphic_link->setValueForKey(TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem)->getIdentifier(), GRAPHICS_LINKS_TABLE_TEST_PLAN_ID);
        m_graphic_link->setValueForKey(m_GraphicItem->getIdentifier(), GRAPHICS_LINKS_TABLE_GRAPHIC_ITEM_ID);
        if (new_item)
            result = m_graphic_link->insertRecord();
        else
            result = m_graphic_link->saveRecord();
    }

    return result;
}
