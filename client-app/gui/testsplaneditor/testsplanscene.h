#ifndef TESTSPLANSCENE_H
#define TESTSPLANSCENE_H

#include "entities/testsplan/testsplan.h"
#include "entities/testsplan/graphicpoint.h"
#include "abstractgraphicitem.h"
#include "abstractlinkgraphicitem.h"
#include "linkablegraphicitem.h"

#include <QtWidgets/QGraphicsScene>

class IfGraphicItem;
class LinkGraphicItem;
class SwitchGraphicItem;
class LoopGraphicItem;

class TestsPlanScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit TestsPlanScene(TestsPlan *in_test_plan, QObject *parent = 0);
    ~TestsPlanScene();

    IfGraphicItem* addIfGraphicItem();
    LinkGraphicItem* addLinkGraphicItem();
    SwitchGraphicItem* addSwitchGraphicItem();
    LoopGraphicItem* addLoopGraphicItem();

    void removeLinkableGraphicItem(LinkableGraphicItem* item);
    void removeLinkGraphicItem(AbstractLinkGraphicItem* item);

    int save();

signals:
    
public slots:

protected:
    virtual void dropEvent(QGraphicsSceneDragDropEvent* event);
    void dragEnterEvent(QGraphicsSceneDragDropEvent * event);
    void dragLeaveEvent(QGraphicsSceneDragDropEvent * event);
    void dragMoveEvent(QGraphicsSceneDragDropEvent * event);

    void findAndSetPointsForItem(const QList<GraphicPoint*>& pointsList, AbstractLinkGraphicItem *graphicItem);

    void addLinkableGraphicItem(LinkableGraphicItem* item);
    void addAbstractLinkGraphicItem(AbstractLinkGraphicItem* item);

private:
    TestsPlan* m_tests_plan;

    QList<LinkableGraphicItem*>         m_linkable_items;
    QList<AbstractLinkGraphicItem*>     m_link_items;

    QList<GraphicItem*>                 m_removed_items;

    void initialize();
};

#endif // TESTSPLANSCENE_H
