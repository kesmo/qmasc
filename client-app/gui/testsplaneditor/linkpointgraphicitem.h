#ifndef LINKPOINTGRAPHICITEM_H
#define LINKPOINTGRAPHICITEM_H

#include "abstractlinkgraphicitem.h"
#include "abstractgraphicitem.h"

#include "linkablegraphicitem.h"

#include "entities/testsplan/graphicpoint.h"

#include <QGraphicsItem>

class LinkPointGraphicItem : public AbstractGraphicItem
{
public:
    enum Type {
        StartPoint = GRAPHICS_POINTS_TYPE_START,
        NodePoint = GRAPHICS_POINTS_TYPE_NODE,
        EndPoint = GRAPHICS_POINTS_TYPE_END
    };

    explicit LinkPointGraphicItem(GraphicPoint* graphicPoint, AbstractLinkGraphicItem* in_parent_link, Type type, LinkPointGraphicItem* previousPoint = NULL);

    LinkPointGraphicItem(QGraphicsItem* parent = 0);
    ~LinkPointGraphicItem();

    QRectF boundingRect() const;

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    bool isConnectablePoint() const;

    GraphicPoint* getPoint() const{return m_graphic_point;}

    AbstractLinkGraphicItem *getLink() const;
    Type getType() const {return m_type;}
    LinkPointGraphicItem* getPreviousPoint() const {return m_previous_point;}
    QList<LinkPointGraphicItem*> getNextPoints() const {return m_next_points;}
    LinkableGraphicItem* getConnectedItem() const {return m_connected_item;}

    void setConnectedItem(LinkableGraphicItem* item, bool updatePos = true);
    void updateConnection();

    static LinkPointGraphicItem* createPointForItem(GraphicItem* graphicItem, AbstractLinkGraphicItem* in_parent_link, Type type, LinkPointGraphicItem* previousPoint = NULL);

    int save();

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    void updateConnectedPoints();

private:
    AbstractLinkGraphicItem* m_parent_link;
    GraphicPoint*            m_graphic_point;

    bool    m_drag;
    LinkableGraphicItem* m_drag_hover_item;
    LinkPointGraphicItem* m_previous_point;
    QList<LinkPointGraphicItem*> m_next_points;
    LinkableGraphicItem* m_connected_item;

    Type    m_type;

    bool m_connection_updating;
};

#endif // LINKPOINTGRAPHICITEM_H
