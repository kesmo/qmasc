#ifndef TESTGRAPHICITEM_H
#define TESTGRAPHICITEM_H

#include "linkablegraphicitem.h"
#include "entities/testsplan/graphictest.h"

class TestGraphicItem : public LinkableGraphicItem
{
public:
    TestGraphicItem(GraphicTest* graphicTest, QGraphicsItem *parent = 0);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual bool mayHaveInput() const;
    virtual bool mayHaveOutput() const;

    QPainterPath shape() const;

    virtual int save();

private:
    GraphicTest* m_graphic_test;
};

#endif // TESTGRAPHICITEM_H
