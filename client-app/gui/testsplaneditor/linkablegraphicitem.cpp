#include "linkablegraphicitem.h"
#include "linkpointgraphicitem.h"

#include "linkgraphicitem.h"
#include "ifgraphicitem.h"
#include "linkpointgraphicitem.h"
#include "switchgraphicitem.h"
#include "loopgraphicitem.h"

#include "testsplanscene.h"

#include <QMenu>
#include <QAction>
#include <QGraphicsScene>
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsSceneContextMenuEvent>

LinkableGraphicItem::LinkableGraphicItem(GraphicItem* graphicItem, QGraphicsItem *parent) :
    AbstractGraphicItem(graphicItem, parent)
{
}


LinkableGraphicItem::~LinkableGraphicItem()
{
    foreach(LinkPointGraphicItem* tmp_connected_point, m_connected_points_links){
        tmp_connected_point->setConnectedItem(NULL);
    }
}

void LinkableGraphicItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu tmp_menu;
    QAction *tmp_add_output_action = NULL;
    QAction *tmp_add_if_action = NULL;
    QAction *tmp_add_switch_action = NULL;
    QAction *tmp_add_loop_action = NULL;
    QAction *tmp_remove_action = NULL;
    QAction *tmp_foreground_action = NULL;
    QAction *tmp_background_action = NULL;

    if (!(event->modifiers() & Qt::ControlModifier))
        scene()->clearSelection();

    setSelected(true);

    if (mayHaveOutput()){
        tmp_add_output_action = tmp_menu.addAction("Ajouter un lien");
        tmp_add_if_action = tmp_menu.addAction("Ajouter une condition de sortie");
        tmp_add_switch_action = tmp_menu.addAction("Ajouter une condition multiple de sortie");
        tmp_add_loop_action = tmp_menu.addAction("Ajouter une boucle");

        if (haveOutput()){
            tmp_add_output_action->setEnabled(false);
            tmp_add_if_action->setEnabled(false);
            tmp_add_switch_action->setEnabled(false);
            tmp_add_loop_action->setEnabled(false);
        }
    }

    tmp_foreground_action = tmp_menu.addAction("Avant-plan");
    tmp_background_action = tmp_menu.addAction("Arrière-plan");
    tmp_remove_action = tmp_menu.addAction("Supprimer");

    if (!tmp_menu.isEmpty()){
        QAction *tmp_selected_action = tmp_menu.exec(event->screenPos());
        if (tmp_selected_action){
            if (tmp_selected_action == tmp_add_output_action){
                LinkGraphicItem* tmp_link = dynamic_cast<TestsPlanScene*>(scene())->addLinkGraphicItem();
                tmp_link->getStartPoint()->setConnectedItem(this);
                tmp_link->setStandardPointsPosition();

            }else if (tmp_selected_action == tmp_add_if_action){
                IfGraphicItem* tmp_if = dynamic_cast<TestsPlanScene*>(scene())->addIfGraphicItem();
                tmp_if->getStartPoint()->setConnectedItem(this);
                tmp_if->setStandardPointsPosition();

            }else if (tmp_selected_action == tmp_add_switch_action){
                SwitchGraphicItem* tmp_switch = dynamic_cast<TestsPlanScene*>(scene())->addSwitchGraphicItem();
                tmp_switch->getStartPoint()->setConnectedItem(this);
                tmp_switch->setStandardPointsPosition();

            }else if (tmp_selected_action == tmp_add_loop_action){
                LoopGraphicItem* tmp_loop = dynamic_cast<TestsPlanScene*>(scene())->addLoopGraphicItem();
                tmp_loop->getStartPoint()->setConnectedItem(this);
                tmp_loop->setStandardPointsPosition();

            }else if (tmp_selected_action == tmp_remove_action){

                dynamic_cast<TestsPlanScene*>(scene())->removeLinkableGraphicItem(this);

            }else if (tmp_selected_action == tmp_foreground_action){

                setZValue(zValue() + 1);

            }else if (tmp_selected_action == tmp_background_action){
                setZValue(zValue() - 1);
            }
        }
    }

}


QVariant LinkableGraphicItem::itemChange(GraphicsItemChange change, const QVariant &value){
    if (change == ItemPositionHasChanged) {

        foreach(LinkPointGraphicItem* tmp_connected_point, m_connected_points_links){
            tmp_connected_point->updateConnection();
        }
        scene()->update();
    }
    return QGraphicsItem::itemChange(change, value);

}


QPointF LinkableGraphicItem::calculateConnectionPoint(LinkPointGraphicItem* in_point_link) const{
    QPointF tmp_point;
    LinkPointGraphicItem* tmp_next_point_link = NULL;


    if (in_point_link->getType() == LinkPointGraphicItem::StartPoint){

        tmp_next_point_link = in_point_link->getNextPoints().first();

    }else {
        tmp_next_point_link = in_point_link->getPreviousPoint();
    }

    if (tmp_next_point_link){
        QPointF tmp_center = mapToScene(shape().boundingRect().center());

        qreal angle = 0.0;
        if (tmp_next_point_link->pos().isNull())
            angle = 0.25;
        else
            angle = angletBetween(tmp_center, tmp_next_point_link->parentItem()->mapToScene(tmp_next_point_link->pos())) / 360;

        tmp_point = shape().pointAtPercent(angle);
    }

    return mapToItem(in_point_link->parentItem(), tmp_point);
}


void LinkableGraphicItem::addConnectedPoint(LinkPointGraphicItem* in_link_point){
    if (!m_connected_points_links.contains(in_link_point)){
        m_connected_points_links.append(in_link_point);
    }
}

void LinkableGraphicItem::removeConnectedPoint(LinkPointGraphicItem* in_link_point){
    m_connected_points_links.removeOne(in_link_point);
}


bool LinkableGraphicItem::haveOutput() const{
    foreach(LinkPointGraphicItem* point, m_connected_points_links){
        if (point->getType() == LinkPointGraphicItem::StartPoint)
            return true;
    }

    return false;
}
