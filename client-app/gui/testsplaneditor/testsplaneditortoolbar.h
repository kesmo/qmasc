#ifndef TESTSPLANEDITORTOOLBAR_H
#define TESTSPLANEDITORTOOLBAR_H

#include <QToolBar>
#include <QAction>

class TestsPlanEditorToolBar : public QToolBar
{
    Q_OBJECT

public:
    TestsPlanEditorToolBar(QWidget *parent = 0);

signals:
    void selectActionSelected();
    void linkActionSelected();
    void conditionActionSelected();
    void switchActionSelected();
    void loopActionSelected();

private:
    QActionGroup* m_actions_group;
    QAction* m_select_action;
    QAction* m_add_link_action;
    QAction* m_add_if_action;
    QAction* m_add_switch_action;
    QAction* m_add_loop_action;

};

#endif // TESTSPLANEDITORTOOLBAR_H
