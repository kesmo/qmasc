#include "ifgraphicitem.h"
#include "entities/testsplan/graphicpoint.h"
#include "entities/testsplan/testsplan.h"
#include "linkpointgraphicitem.h"

#include <QPainter>
#include <QGraphicsScene>

IfGraphicItem::IfGraphicItem(GraphicIf *graphicIf, QGraphicsItem *parent) :
    AbstractLinkGraphicItem(GraphicItem::GraphicIfRelation::instance().getParent(graphicIf), parent),
    m_graphic_if(graphicIf)
{

}

void IfGraphicItem::addStandardPoints(){
    LinkPointGraphicItem* tmp_start_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::StartPoint);
    LinkPointGraphicItem* tmp_node_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::NodePoint, tmp_start_point);
    LinkPointGraphicItem* tmp_end_point1 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_node_point);
    LinkPointGraphicItem* tmp_end_point2 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_node_point);

    m_points << tmp_start_point << tmp_node_point << tmp_end_point1 << tmp_end_point2;
}

void IfGraphicItem::setStandardPointsPosition(){
    m_points[1]->setPos(m_points[0]->pos() + QPointF(0, 40));
    m_points[2]->setPos(m_points[1]->pos() + QPointF(-20, 40));
    m_points[3]->setPos(m_points[1]->pos() + QPointF(20, 40));
    update();
}


QPainterPath IfGraphicItem::shape() const{
    QPainterPath tmp_path;
    QPainterPathStroker tmp_stroker;

    if (m_points.count() == 4){
        tmp_path.moveTo(m_points[0]->pos());
        tmp_path.lineTo(m_points[1]->pos());
        tmp_path.lineTo(m_points[2]->pos());
        tmp_path.moveTo(m_points[1]->pos());
        tmp_path.lineTo(m_points[3]->pos());
    }

    return tmp_stroker.createStroke(tmp_path);
}


void IfGraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    if (m_points.count() == 4){
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

        if (isSelected()){
            painter->setPen(QPen(Qt::blue, 2));
            painter->setBrush(Qt::blue);
        }else if (m_is_highlighted){
            painter->setPen(QPen(Qt::gray, 2));
            painter->setBrush(Qt::gray);
        }else{
            painter->setPen(QPen(Qt::black, 2));
            painter->setBrush(Qt::black);
        }

        painter->drawLine(m_points[0]->pos(), m_points[1]->pos());
        painter->drawLine(m_points[1]->pos(), m_points[2]->pos());
        painter->drawLine(m_points[1]->pos(), m_points[3]->pos());

    //    painter->setPen(Qt::red);
    //    painter->setBrush(Qt::NoBrush);
    //    painter->drawRect(boundingRect());
    }
}


int IfGraphicItem::save(){
    bool new_item = is_empty_string(m_GraphicItem->getIdentifier()) == TRUE;
    m_GraphicItem->setValueForKey(QString(GRAPHICS_ITEMS_TYPE_IF).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_TYPE);
    int result = AbstractLinkGraphicItem::save();
    if (result == NOERR){
        m_graphic_if->setValueForKey(TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem)->getIdentifier(), GRAPHICS_IFS_TABLE_TEST_PLAN_ID);
        m_graphic_if->setValueForKey(m_GraphicItem->getIdentifier(), GRAPHICS_IFS_TABLE_GRAPHIC_ITEM_ID);
        if (new_item)
            result = m_graphic_if->insertRecord();
        else
            result = m_graphic_if->saveRecord();
    }

    return result;
}
