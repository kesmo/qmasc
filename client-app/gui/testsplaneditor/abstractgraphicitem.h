#ifndef ABSTRACTGRAPHICITEM_H
#define ABSTRACTGRAPHICITEM_H

#include "entities/testsplan/graphicitem.h"

#include <QtWidgets/QGraphicsItem>

class LinkPointGraphicItem;

class AbstractGraphicItem : public QGraphicsItem
{
public:
    AbstractGraphicItem(GraphicItem* graphicItem, QGraphicsItem *parent = 0);
    ~AbstractGraphicItem();

    virtual QRectF boundingRect() const;

    void setHighlighted(bool highlighted);
    bool isHighlighted() const {return m_is_highlighted;}

    GraphicItem* getGraphicItem() const{return m_GraphicItem;}

    virtual int save();

protected:

    QRectF  m_bounding_rect;
    bool    m_is_highlighted;

protected:

    GraphicItem* m_GraphicItem;
};


float angletBetween(const QPointF& in_point1, const QPointF& in_point2);
float radiantAngletBetween(const QPointF& in_point1, const QPointF& in_point2);

#endif // ABSTRACTGRAPHICITEM_H
