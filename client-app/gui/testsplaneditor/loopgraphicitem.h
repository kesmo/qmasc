#ifndef LOOPGRAPHICITEM_H
#define LOOPGRAPHICITEM_H

#include "abstractlinkgraphicitem.h"
#include "entities/testsplan/graphicloop.h"

class LoopGraphicItem : public AbstractLinkGraphicItem
{
public:
    LoopGraphicItem(GraphicLoop* graphicLoop, QGraphicsItem *parent = 0);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual void addStandardPoints();
    virtual void setStandardPointsPosition();

    QRectF boundingRect() const;

    QPainterPath shape() const;

    int save();

private:
    GraphicLoop* m_graphic_loop;
};

#endif // LOOPGRAPHICITEM_H
