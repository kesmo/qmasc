#include "testsplaneditortoolbar.h"

TestsPlanEditorToolBar::TestsPlanEditorToolBar(QWidget *parent) : QToolBar(parent)
{
    m_actions_group = new QActionGroup(this);
    m_actions_group->setExclusive(true);

    m_select_action = m_actions_group->addAction(QIcon(":/images/tests-editor/cursor.png"), tr("Sélectionner"));
    m_select_action->setCheckable(true);
    m_select_action->setChecked(true);
    connect(m_select_action, SIGNAL(triggered()), this, SIGNAL(selectActionSelected()));

    m_add_link_action = m_actions_group->addAction(QIcon(":/images/tests-editor/link.png"), tr("Ajouter des liens"));
    m_add_link_action->setCheckable(true);
    connect(m_add_link_action, SIGNAL(triggered()), this, SIGNAL(linkActionSelected()));

    m_add_if_action = m_actions_group->addAction(QIcon(":/images/tests-editor/output.png"), tr("Ajouter des conditions"));
    m_add_if_action->setCheckable(true);
    connect(m_add_if_action, SIGNAL(triggered()), this, SIGNAL(conditionActionSelected()));

    m_add_switch_action = m_actions_group->addAction(QIcon(":/images/tests-editor/multiple-output.png"), tr("Ajouter des switch"));
    m_add_switch_action->setCheckable(true);
    connect(m_add_switch_action, SIGNAL(triggered()), this, SIGNAL(switchActionSelected()));

    m_add_loop_action = m_actions_group->addAction(QIcon(":/images/tests-editor/loop.png"), tr("Ajouter des boucles"));
    m_add_loop_action->setCheckable(true);
    connect(m_add_loop_action, SIGNAL(triggered()), this, SIGNAL(loopActionSelected()));

    addAction(m_select_action);
    addAction(m_add_link_action);
    addAction(m_add_if_action);
    addAction(m_add_switch_action);
    addAction(m_add_loop_action);

}
