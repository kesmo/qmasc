#ifndef ABSTRACTLINKGRAPHICITEM_H
#define ABSTRACTLINKGRAPHICITEM_H

#include "entities/testsplan/graphicitem.h"
#include "abstractgraphicitem.h"

#include <QPainter>
#include <QPainterPath>
#include <QtWidgets/QGraphicsItem>

class LinkPointGraphicItem;

class AbstractLinkGraphicItem : public AbstractGraphicItem
{
public:
    AbstractLinkGraphicItem(GraphicItem* graphicItem, QGraphicsItem* parent = 0);
    ~AbstractLinkGraphicItem();

    QRectF boundingRect() const;

    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0) = 0;

    virtual QPainterPath shape() const = 0;

    virtual void addStandardPoints();
    virtual void setStandardPointsPosition();
    virtual void setPoints(const QList<LinkPointGraphicItem*>& points);

    LinkPointGraphicItem* getStartPoint() const;
    QList<LinkPointGraphicItem*> getPoints() const;
    QList<LinkPointGraphicItem*> getEndPoints() const;

    int save();

protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant & value);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

    QList<LinkPointGraphicItem*>    m_points;
};

#endif // ABSTRACTLINKGRAPHICITEM_H
