#include "abstractgraphicitem.h"
#include "entities/testsplan/testsplan.h"


#include <qmath.h>


float angletBetween(const QPointF& in_point1, const QPointF& in_point2){
    if (in_point1.x() == in_point2.x()){
        if (in_point1.y() > in_point2.y())
            return -90;
        else
            return 90;
    }

    float tmp_m = (in_point1.y() - in_point2.y()) / (in_point1.x() - in_point2.x());
    float tmp_angle = qAtan(tmp_m) * 180 / M_PI;

    if (in_point1.x() > in_point2.x())
        tmp_angle -= 180;

    if (tmp_angle < 0){
        if (tmp_angle != -90){
            tmp_angle += 360;
        }
        else
            tmp_angle = 90;
    }

    return tmp_angle;
}


float radiantAngletBetween(const QPointF& in_point1, const QPointF& in_point2){

    if (in_point1.x() == in_point2.x()){
        if (in_point1.y() > in_point2.y())
            return -float(M_PI) / 2;
        else
            return float(M_PI) / 2;
    }

    float tmp_m = (in_point1.y() - in_point2.y()) / (in_point1.x() - in_point2.x());
    float tmp_angle = qAtan(tmp_m);

    if (in_point1.x() > in_point2.x())
        tmp_angle -= float(M_PI);

    if (tmp_angle < 0){
        if (tmp_angle != -float(M_PI) / 2){
            tmp_angle += 2 * float(M_PI);
        }
        else
            tmp_angle = float(M_PI) / 2;
    }

    return tmp_angle;
}


AbstractGraphicItem::AbstractGraphicItem(GraphicItem* graphicItem, QGraphicsItem *parent) :
    QGraphicsItem(parent),
    m_GraphicItem(graphicItem),
    m_is_highlighted(false)
{
    setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemSendsGeometryChanges);
}


AbstractGraphicItem::~AbstractGraphicItem(){
}

QRectF AbstractGraphicItem::boundingRect() const{
    return m_bounding_rect;
}

void AbstractGraphicItem::setHighlighted(bool in_highlight_item){
    m_is_highlighted = in_highlight_item;
    update();
}


int AbstractGraphicItem::save(){

    TestsPlan* testPlan = TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem);
    GraphicItem* parentItem = GraphicItem::ParentGraphicItemRelation::instance().getParent(m_GraphicItem);

    m_GraphicItem->setValueForKey(testPlan->getIdentifier(), GRAPHICS_ITEMS_TABLE_TEST_PLAN_ID);
    if (parentItem)
        m_GraphicItem->setValueForKey(parentItem->getIdentifier(), GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID);
    else
        m_GraphicItem->setValueForKey(NULL, GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID);

    m_GraphicItem->setValueForKey(QString::number(pos().x()).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_X);
    m_GraphicItem->setValueForKey(QString::number(pos().y()).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_Y);
    m_GraphicItem->setValueForKey(QString::number(zValue()).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_Z_VALUE);

    return m_GraphicItem->saveRecord();
}

