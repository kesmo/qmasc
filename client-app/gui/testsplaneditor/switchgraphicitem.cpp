#include "switchgraphicitem.h"
#include "linkpointgraphicitem.h"
#include "entities/testsplan/testsplan.h"

#include <QPainter>

SwitchGraphicItem::SwitchGraphicItem(GraphicSwitch *graphicSwitch, QGraphicsItem *parent) :
    AbstractLinkGraphicItem(GraphicItem::GraphicSwitchRelation::instance().getParent(graphicSwitch), parent),
    m_graphic_switch(graphicSwitch)
{
}


void SwitchGraphicItem::addStandardPoints(){

    LinkPointGraphicItem* tmp_start_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::StartPoint);
    LinkPointGraphicItem* tmp_main_node_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::NodePoint, tmp_start_point);

    LinkPointGraphicItem* tmp_node_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::NodePoint, tmp_main_node_point);
    LinkPointGraphicItem* tmp_end_point1 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_node_point);

    LinkPointGraphicItem* tmp_node_point2 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::NodePoint, tmp_main_node_point);
    LinkPointGraphicItem* tmp_end_point2 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_node_point2);

    LinkPointGraphicItem* tmp_node_point3 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::NodePoint, tmp_main_node_point);
    LinkPointGraphicItem* tmp_end_point3 = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_node_point3);

    m_points << tmp_start_point << tmp_main_node_point
              << tmp_node_point << tmp_end_point1
              << tmp_node_point2 << tmp_end_point2
              << tmp_node_point3 << tmp_end_point3;
}

void SwitchGraphicItem::setStandardPointsPosition(){

    qreal step = 20;
    int nbpoints = (m_points.count() - 2) / 2;
    qreal length = (nbpoints - 1) * step;

    m_points[1]->setPos(m_points[0]->pos() + QPointF(0, 40));

    for(int index = 2; index < m_points.count(); index += 2){
        qreal rx = -(length / 2) + (index - 2) * step / 2;
        m_points[index]->setPos(m_points[1]->pos() + QPointF(rx, 0));
        m_points[index + 1]->setPos(m_points[1]->pos() + QPointF(rx, 40));
    }

    update();
}


QPainterPath SwitchGraphicItem::shape() const{
    QPainterPath tmp_path;
    QPainterPathStroker tmp_stroker;

    if (m_points.count() > 2){
        tmp_path.moveTo(m_points[0]->pos());
        tmp_path.lineTo(m_points[1]->pos());

        for(int index = 2; index < m_points.count(); index += 2){
            tmp_path.moveTo(m_points[1]->pos());
            tmp_path.lineTo(m_points[index]->pos());
            tmp_path.lineTo(m_points[index + 1]->pos());
        }
    }

    return tmp_stroker.createStroke(tmp_path);
}


void SwitchGraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    if (m_points.count() > 2){
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

        if (isSelected()){
            painter->setPen(QPen(Qt::blue, 2));
            painter->setBrush(Qt::blue);
        }else if (m_is_highlighted){
            painter->setPen(QPen(Qt::gray, 2));
            painter->setBrush(Qt::gray);
        }else{
            painter->setPen(QPen(Qt::black, 2));
            painter->setBrush(Qt::black);
        }

        painter->drawLine(m_points[0]->pos(), m_points[1]->pos());

        for(int index = 2; index < m_points.count(); index += 2){
            painter->drawLine(m_points[1]->pos(), m_points[index]->pos());
            painter->drawLine(m_points[index]->pos(), m_points[index + 1]->pos());
        }
    }

}


int SwitchGraphicItem::save(){
    bool new_item = is_empty_string(m_GraphicItem->getIdentifier()) == TRUE;
    m_GraphicItem->setValueForKey(QString(GRAPHICS_ITEMS_TYPE_SWITCH).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_TYPE);
    int result = AbstractLinkGraphicItem::save();
    if (result == NOERR){
        m_graphic_switch->setValueForKey(TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem)->getIdentifier(), GRAPHICS_SWITCHES_TABLE_TEST_PLAN_ID);
        m_graphic_switch->setValueForKey(m_GraphicItem->getIdentifier(), GRAPHICS_SWITCHES_TABLE_GRAPHIC_ITEM_ID);
        if (new_item)
            result = m_graphic_switch->insertRecord();
        else
            result = m_graphic_switch->saveRecord();
    }

    return result;
}
