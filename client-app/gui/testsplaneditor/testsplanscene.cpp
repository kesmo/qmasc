#include "testsplanscene.h"
#include "entities/test.h"
#include "entities/projectversion.h"
#include "entities/campaign.h"

#include "testgraphicitem.h"
#include "linkgraphicitem.h"
#include "ifgraphicitem.h"
#include "switchgraphicitem.h"
#include "loopgraphicitem.h"
#include "linkpointgraphicitem.h"
#include "linkablegraphicitem.h"

#include "entities/testsplan/graphicpoint.h"

#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>

TestsPlanScene::TestsPlanScene(TestsPlan *in_test_plan, QObject *parent) :
    QGraphicsScene(parent),
    m_tests_plan(in_test_plan)
{
    initialize();
}


TestsPlanScene::~TestsPlanScene(){

}

void TestsPlanScene::initialize(){
    const QList<GraphicItem*>& itemsList = TestsPlan::GraphicItemRelation::instance().getChilds(m_tests_plan);
    const QList<GraphicTest*>& testsList = TestsPlan::GraphicTestRelation::instance().getChilds(m_tests_plan);
    const QList<GraphicLink*>& linksList = TestsPlan::GraphicLinkRelation::instance().getChilds(m_tests_plan);
    const QList<GraphicIf*>& ifsList = TestsPlan::GraphicIfRelation::instance().getChilds(m_tests_plan);
    const QList<GraphicLoop*>& loopsList = TestsPlan::GraphicLoopRelation::instance().getChilds(m_tests_plan);
    const QList<GraphicSwitch*>& switchesList = TestsPlan::GraphicSwitchRelation::instance().getChilds(m_tests_plan);
    const QList<GraphicPoint*>& pointsList = TestsPlan::GraphicPointRelation::instance().getChilds(m_tests_plan);

    GraphicItem* mainItem = NULL;

    foreach(GraphicTest* graphicItem, testsList){
        mainItem = TestsPlan::GraphicItemRelation::instance().getUniqueChildWithValueForKey(m_tests_plan, graphicItem->getValueForKey(GRAPHICS_TESTS_TABLE_GRAPHIC_ITEM_ID), GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID);
        if (mainItem){
            Campaign* campaign = Campaign::TestsPlanRelation::instance().getParent(m_tests_plan);
            if (campaign){
                const QList<Test*> tests = ProjectVersion::TestRelation::instance().getChilds(campaign->projectVersion());
                Test *tmp_test = Test::findTestWithId(tests, graphicItem->getValueForKey(GRAPHICS_TESTS_TABLE_TEST_ID));
                if (tmp_test){
                    Test::GraphicTestRelation::instance().addChild(tmp_test, graphicItem);
                    GraphicItem::GraphicTestRelation::instance().addChild(mainItem, graphicItem);
                    TestGraphicItem* tmp_graphic_item = new TestGraphicItem(graphicItem);
                    addItem(tmp_graphic_item);
                    tmp_graphic_item->setPos(mainItem->getX(), mainItem->getY());
                    tmp_graphic_item->setZValue(mainItem->getZValue());
                    m_linkable_items.append(tmp_graphic_item);
                }
            }
        }
    }

    foreach(GraphicLink* graphicItem, linksList){
        mainItem = TestsPlan::GraphicItemRelation::instance().getUniqueChildWithValueForKey(m_tests_plan, graphicItem->getValueForKey(GRAPHICS_LINKS_TABLE_GRAPHIC_ITEM_ID), GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID);
        if (mainItem){
            GraphicItem::GraphicLinkRelation::instance().addChild(mainItem, graphicItem);
            LinkGraphicItem* tmp_graphic_item = new LinkGraphicItem(graphicItem);
            addItem(tmp_graphic_item);
            tmp_graphic_item->setPos(mainItem->getX(), mainItem->getY());
            tmp_graphic_item->setZValue(mainItem->getZValue());
            findAndSetPointsForItem(pointsList, tmp_graphic_item);
            m_link_items.append(tmp_graphic_item);
        }
    }

    foreach(GraphicIf* graphicItem, ifsList){
        mainItem = TestsPlan::GraphicItemRelation::instance().getUniqueChildWithValueForKey(m_tests_plan, graphicItem->getValueForKey(GRAPHICS_IFS_TABLE_GRAPHIC_ITEM_ID), GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID);
        if (mainItem){
            GraphicItem::GraphicIfRelation::instance().addChild(mainItem, graphicItem);
            IfGraphicItem* tmp_graphic_item = new IfGraphicItem(graphicItem);
            addItem(tmp_graphic_item);
            tmp_graphic_item->setPos(mainItem->getX(), mainItem->getY());
            tmp_graphic_item->setZValue(mainItem->getZValue());
            findAndSetPointsForItem(pointsList, tmp_graphic_item);
            m_link_items.append(tmp_graphic_item);
        }
    }

    foreach(GraphicLoop* graphicItem, loopsList){
        mainItem = TestsPlan::GraphicItemRelation::instance().getUniqueChildWithValueForKey(m_tests_plan, graphicItem->getValueForKey(GRAPHICS_LOOPS_TABLE_GRAPHIC_ITEM_ID), GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID);
        if (mainItem){
            GraphicItem::GraphicLoopRelation::instance().addChild(mainItem, graphicItem);
            LoopGraphicItem* tmp_graphic_item = new LoopGraphicItem(graphicItem);
            addItem(tmp_graphic_item);
            tmp_graphic_item->setPos(mainItem->getX(), mainItem->getY());
            tmp_graphic_item->setZValue(mainItem->getZValue());
            findAndSetPointsForItem(pointsList, tmp_graphic_item);
            m_link_items.append(tmp_graphic_item);
        }
    }

    foreach(GraphicSwitch* graphicItem, switchesList){
        mainItem = TestsPlan::GraphicItemRelation::instance().getUniqueChildWithValueForKey(m_tests_plan, graphicItem->getValueForKey(GRAPHICS_SWITCHES_TABLE_GRAPHIC_ITEM_ID), GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID);
        if (mainItem){
            GraphicItem::GraphicSwitchRelation::instance().addChild(mainItem, graphicItem);
            SwitchGraphicItem* tmp_graphic_item = new SwitchGraphicItem(graphicItem);
            addItem(tmp_graphic_item);
            tmp_graphic_item->setPos(mainItem->getX(), mainItem->getY());
            tmp_graphic_item->setZValue(mainItem->getZValue());
            findAndSetPointsForItem(pointsList, tmp_graphic_item);
            m_link_items.append(tmp_graphic_item);
        }
    }
}

void TestsPlanScene::findAndSetPointsForItem(const QList<GraphicPoint*>& pointsList, AbstractLinkGraphicItem* graphicItem){
    QList<GraphicPoint*> unorderedItemPointsList = GraphicPoint::findEntitiesWithValueForKey(pointsList, graphicItem->getGraphicItem()->getIdentifier(), GRAPHICS_POINTS_TABLE_LINK_GRAPHIC_ITEM_ID);
    if(unorderedItemPointsList.isEmpty()){
        graphicItem->addStandardPoints();
        graphicItem->setStandardPointsPosition();
    }else{
        QList<GraphicPoint*> orderedPointsList = GraphicPoint::orderedRecords(unorderedItemPointsList, GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID);
        QList<LinkPointGraphicItem*> finalPoints;
        LinkPointGraphicItem* previousLinkPoint = NULL;
        GraphicPoint* previousPoint = NULL;
        QMap<GraphicPoint*, LinkPointGraphicItem*> mapPoints;
        foreach (GraphicPoint* graphicPoint, orderedPointsList) {
            GraphicItem* mainItem = TestsPlan::GraphicItemRelation::instance().getUniqueChildWithValueForKey(m_tests_plan, graphicPoint->getValueForKey(GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID), GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID);
            if (mainItem){

                GraphicItem::GraphicPointRelation::instance().addChild(mainItem, graphicPoint);

                GraphicItem::ParentGraphicItemRelation::instance().addChild(graphicItem->getGraphicItem(), mainItem);

                previousLinkPoint = NULL;
                previousPoint = GraphicPoint::findEntityWithValueForKey(orderedPointsList,
                                                                        graphicPoint->getValueForKey(GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID),
                                                                        GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID);
                if (previousPoint){
                    previousLinkPoint = mapPoints[previousPoint];
                }

                LinkPointGraphicItem* point = new LinkPointGraphicItem(graphicPoint, graphicItem, (LinkPointGraphicItem::Type)graphicPoint->getValueForKey(GRAPHICS_POINTS_TABLE_TYPE)[0], previousLinkPoint);
                point->setPos(mainItem->getX(), mainItem->getY());
                point->setZValue(mainItem->getZValue());
                finalPoints.append(point);
                mapPoints[graphicPoint] = point;
            }
        }

        graphicItem->setPoints(finalPoints);
    }

    foreach(LinkPointGraphicItem* point, graphicItem->getPoints()){
        foreach(LinkableGraphicItem* item, m_linkable_items){
            if (compare_values(point->getPoint()->getValueForKey(GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID),
                               item->getGraphicItem()->getValueForKey(GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID)) == 0){
                point->setConnectedItem(item);
            }
        }
    }
}

void TestsPlanScene::addLinkableGraphicItem(LinkableGraphicItem* item){
    m_linkable_items.append(item);
}

void TestsPlanScene::addAbstractLinkGraphicItem(AbstractLinkGraphicItem* item){
    m_link_items.append(item);
}


void TestsPlanScene::dragEnterEvent(QGraphicsSceneDragDropEvent * event)
{
    const QMimeData *tmp_mime_data = event->mimeData();
    if (tmp_mime_data != NULL && tmp_mime_data->hasFormat(DEFAULT_RECORD_MIME_TYPE)){
        event->setDropAction(Qt::CopyAction);
        event->accept();
    }else
        QGraphicsScene::dragEnterEvent(event);
}


void TestsPlanScene::dragLeaveEvent(QGraphicsSceneDragDropEvent * event)
{
    const QMimeData *tmp_mime_data = event->mimeData();
    if (tmp_mime_data != NULL && tmp_mime_data->hasFormat(DEFAULT_RECORD_MIME_TYPE)){
        event->setDropAction(Qt::CopyAction);
        event->accept();
    }else
        QGraphicsScene::dragLeaveEvent(event);
}


void TestsPlanScene::dragMoveEvent(QGraphicsSceneDragDropEvent * event)
{
    const QMimeData *tmp_mime_data = event->mimeData();
    if (tmp_mime_data != NULL && tmp_mime_data->hasFormat(DEFAULT_RECORD_MIME_TYPE)){
        event->setDropAction(Qt::CopyAction);
        event->accept();
    }else
        QGraphicsScene::dragMoveEvent(event);

}

void TestsPlanScene::dropEvent(QGraphicsSceneDragDropEvent* event)
{
    const QMimeData *tmp_mime_data = event->mimeData();
    entity_def *tmp_entity_def = NULL;
    int             tmp_start_index = 0;

    Record       *tmp_generic_record = NULL;
    Test                *tmp_test = NULL;

    if (tmp_mime_data != NULL && tmp_mime_data->hasFormat(DEFAULT_RECORD_MIME_TYPE) && m_tests_plan)
    {
        QByteArray tmp_bytes_array = tmp_mime_data->data(DEFAULT_RECORD_MIME_TYPE);

        if (!tmp_bytes_array.isEmpty())
        {
            event->setDropAction(Qt::CopyAction);

            QList<QString> tmp_records_list = QString(tmp_bytes_array).split(RECORD_SEPARATOR_CHAR);
            for (int tmp_index = 0; tmp_index < tmp_records_list.count(); tmp_index++)
            {
                tmp_start_index = tmp_records_list[tmp_index].indexOf(SEPARATOR_CHAR);
                if (tmp_start_index >= 0)
                {
                    std::string tmp_std_string  = tmp_records_list[tmp_index].section(SEPARATOR_CHAR, 0, 0).toStdString();
                    if (get_table_def(atoi(tmp_std_string.c_str()), &tmp_entity_def) == NOERR)
                    {
                        // Accept only tests to be dropped
                        if (tmp_entity_def->m_entity_signature_id == TESTS_HIERARCHY_SIG_ID)
                        {
                            tmp_generic_record = new Record(tmp_entity_def);
                            if (tmp_generic_record != NULL)
                            {
                                tmp_generic_record->deserialize(tmp_records_list[tmp_index]);
                                Campaign* campaign = Campaign::TestsPlanRelation::instance().getParent(m_tests_plan);
                                if (campaign){
                                    const QList<Test*> tests = ProjectVersion::TestRelation::instance().getChilds(campaign->projectVersion());
                                    tmp_test = Test::findTestWithId(tests, tmp_generic_record->getIdentifier());
                                    if (tmp_test){
                                        GraphicTest* graphicTest = new GraphicTest();
                                        GraphicItem* graphicItem = new GraphicItem();

                                        Test::GraphicTestRelation::instance().addChild(tmp_test, graphicTest);

                                        GraphicItem::GraphicTestRelation::instance().addChild(graphicItem, graphicTest);

                                        TestsPlan::GraphicItemRelation::instance().addChild(m_tests_plan, graphicItem);

                                        TestsPlan::GraphicTestRelation::instance().addChild(m_tests_plan, graphicTest);

                                        TestGraphicItem* tmp_test_item = new TestGraphicItem(graphicTest);
                                        addItem(tmp_test_item);
                                        tmp_test_item->setPos(event->scenePos());

                                        m_linkable_items.append(tmp_test_item);
                                    }
                                }

                                delete tmp_generic_record;
                            }
                        }
                    }
                }
            }
        }
    }
    else
        QGraphicsScene::dropEvent(event);

}


IfGraphicItem* TestsPlanScene::addIfGraphicItem(){

    GraphicIf*  graphicIf = new GraphicIf();
    GraphicItem*  graphicItem = new GraphicItem();

    GraphicItem::GraphicIfRelation::instance().addChild(graphicItem, graphicIf);

    TestsPlan::GraphicItemRelation::instance().addChild(m_tests_plan, graphicItem);

    TestsPlan::GraphicIfRelation::instance().addChild(m_tests_plan, graphicIf);

    IfGraphicItem* tmp_graphic_item = new IfGraphicItem(graphicIf);
    addItem(tmp_graphic_item);
    tmp_graphic_item->addStandardPoints();

    addAbstractLinkGraphicItem(tmp_graphic_item);

    return tmp_graphic_item;
}


LinkGraphicItem* TestsPlanScene::addLinkGraphicItem(){

    GraphicLink*  graphicLink = new GraphicLink();
    GraphicItem*  graphicItem = new GraphicItem();

    GraphicItem::GraphicLinkRelation::instance().addChild(graphicItem, graphicLink);

    TestsPlan::GraphicItemRelation::instance().addChild(m_tests_plan, graphicItem);

    TestsPlan::GraphicLinkRelation::instance().addChild(m_tests_plan, graphicLink);

    LinkGraphicItem* tmp_graphic_item = new LinkGraphicItem(graphicLink);
    addItem(tmp_graphic_item);
    tmp_graphic_item->addStandardPoints();

    addAbstractLinkGraphicItem(tmp_graphic_item);

    return tmp_graphic_item;
}


SwitchGraphicItem* TestsPlanScene::addSwitchGraphicItem(){

    GraphicSwitch*  graphicSwitch = new GraphicSwitch();
    GraphicItem*  graphicItem = new GraphicItem();

    GraphicItem::GraphicSwitchRelation::instance().addChild(graphicItem, graphicSwitch);

    TestsPlan::GraphicItemRelation::instance().addChild(m_tests_plan, graphicItem);

    TestsPlan::GraphicSwitchRelation::instance().addChild(m_tests_plan, graphicSwitch);

    SwitchGraphicItem* tmp_graphic_item = new SwitchGraphicItem(graphicSwitch);
    addItem(tmp_graphic_item);
    tmp_graphic_item->addStandardPoints();

    addAbstractLinkGraphicItem(tmp_graphic_item);

    return tmp_graphic_item;
}


LoopGraphicItem* TestsPlanScene::addLoopGraphicItem(){

    GraphicLoop*  graphicLoop = new GraphicLoop();
    GraphicItem*  graphicItem = new GraphicItem();

    GraphicItem::GraphicLoopRelation::instance().addChild(graphicItem, graphicLoop);

    TestsPlan::GraphicItemRelation::instance().addChild(m_tests_plan, graphicItem);

    TestsPlan::GraphicLoopRelation::instance().addChild(m_tests_plan, graphicLoop);

    LoopGraphicItem* tmp_graphic_item = new LoopGraphicItem(graphicLoop);
    addItem(tmp_graphic_item);
    tmp_graphic_item->addStandardPoints();

    addAbstractLinkGraphicItem(tmp_graphic_item);

    return tmp_graphic_item;
}


void TestsPlanScene::removeLinkableGraphicItem(LinkableGraphicItem* item){
    m_linkable_items.removeAll(item);
    m_removed_items.append(item->getGraphicItem());
    delete item;
}


void TestsPlanScene::removeLinkGraphicItem(AbstractLinkGraphicItem* item){
    m_link_items.removeAll(item);
    m_removed_items.append(item->getGraphicItem());
    delete item;
}

int TestsPlanScene::save(){
    m_tests_plan->setValueForKey(Campaign::TestsPlanRelation::instance().getParent(m_tests_plan)->getIdentifier(), TESTS_PLANS_TABLE_CAMPAIGN_ID);
    int result = m_tests_plan->saveRecord();

    if (result == NOERR){
        foreach(LinkableGraphicItem* item, m_linkable_items){
            result = item->save();
            if (result != NOERR)
                break;
        }

        if (result == NOERR){
            foreach(AbstractLinkGraphicItem* item, m_link_items){
                result = item->save();
                if (result != NOERR)
                    break;
            }


            if (result == NOERR){
                foreach(GraphicItem* item, m_removed_items){
                    result = item->deleteRecord();
                    if (result != NOERR)
                        break;
                }
            }
        }
    }

    return result;
}
