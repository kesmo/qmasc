#include "testsplangraphicsview.h"

#include <QDebug>

TestsPlanGraphicsView::TestsPlanGraphicsView(QWidget *parent)
    : QGraphicsView(parent),
      mRubberBand(NULL)
{
}

TestsPlanGraphicsView::~TestsPlanGraphicsView(){
    delete mRubberBand;
}

void TestsPlanGraphicsView::wheelEvent(QWheelEvent * event)
{
    qreal numDegrees = event->delta() / 8;

    qreal scaleLevel = 1 + (4 * numDegrees / 360);

    scale(scaleLevel, scaleLevel);

    event->accept();

}


void TestsPlanGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() & Qt::RightButton || event->modifiers() & Qt::ShiftModifier){
        viewport()->setCursor(Qt::CrossCursor);
        mRubberBandOrigin = event->pos();
        if (!mRubberBand)
            mRubberBand = new QRubberBand(QRubberBand::Rectangle, this);
        mRubberBand->setGeometry(QRect(mRubberBandOrigin, QSize()));
        mRubberBand->show();

        event->accept();
    }else{
        QGraphicsView::mousePressEvent(event);
    }
}


void TestsPlanGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if (mRubberBand){
        mRubberBand->setGeometry(QRect(mRubberBandOrigin, event->pos()).normalized());
        event->accept();
    }else{
        QGraphicsView::mouseMoveEvent(event);
    }
}


void TestsPlanGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (mRubberBand){
        viewport()->unsetCursor();
        QPainterPath path;
        QPointF     finalPoint = mapToScene(event->pos());


        path.addRect(QRectF(mapToScene(mRubberBandOrigin), finalPoint));
        scene()->setSelectionArea(path, Qt::ContainsItemShape, transform());

        delete mRubberBand;
        mRubberBand = NULL;

        event->accept();
    }else{
        QGraphicsView::mouseReleaseEvent(event);
    }
}
