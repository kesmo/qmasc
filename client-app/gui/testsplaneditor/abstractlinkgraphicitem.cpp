#include "abstractlinkgraphicitem.h"
#include "linkpointgraphicitem.h"
#include "testsplanscene.h"

#include <qmath.h>
#include <QPainterPathStroker>
#include <QGraphicsScene>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>

AbstractLinkGraphicItem::AbstractLinkGraphicItem(GraphicItem *graphicItem, QGraphicsItem *parent) :
    AbstractGraphicItem(graphicItem, parent)
{
}


AbstractLinkGraphicItem::~AbstractLinkGraphicItem()
{
}


QRectF AbstractLinkGraphicItem::boundingRect() const{
    return childrenBoundingRect();
}


QVariant AbstractLinkGraphicItem::itemChange(GraphicsItemChange change, const QVariant & value)
{
    if (change == ItemPositionChange){
        foreach(LinkPointGraphicItem* point, m_points){
            if (point && point->getConnectedItem() && !(point->isSelected() && point->getConnectedItem()->isSelected()) && !collidesWithItem(point->getConnectedItem(), Qt::ContainsItemBoundingRect)){
                qDebug() << Q_FUNC_INFO << point->getType();
                point->setConnectedItem(NULL);
            }
        }
    }
    else if (change == ItemSelectedHasChanged){
        m_is_highlighted = isSelected();
    }

    return QGraphicsItem::itemChange(change, value);
}

LinkPointGraphicItem* AbstractLinkGraphicItem::getStartPoint() const{
    if (!m_points.isEmpty() && m_points.first()->getType() == LinkPointGraphicItem::StartPoint)
        return m_points.first();

    return NULL;
}


void AbstractLinkGraphicItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu tmp_menu;
    QAction *tmp_remove_action = NULL;
    QAction *tmp_foreground_action = NULL;
    QAction *tmp_background_action = NULL;
    QAction *tmp_properties_action = NULL;

    if (!(event->modifiers() & Qt::ControlModifier))
        scene()->clearSelection();

    setSelected(true);

    tmp_foreground_action = tmp_menu.addAction("Avant-plan");
    tmp_background_action = tmp_menu.addAction("Arrière-plan");
    tmp_remove_action = tmp_menu.addAction("Supprimer");
    tmp_properties_action = tmp_menu.addAction("Propriétés");

    if (!tmp_menu.isEmpty()){
        QAction *tmp_selected_action = tmp_menu.exec(event->screenPos());
        if (tmp_selected_action){
            if (tmp_selected_action == tmp_remove_action){

                dynamic_cast<TestsPlanScene*>(scene())->removeLinkGraphicItem(this);

            }else if (tmp_selected_action == tmp_foreground_action){

                setZValue(zValue() + 1);

            }else if (tmp_selected_action == tmp_background_action){
                setZValue(zValue() - 1);
            }else if (tmp_selected_action == tmp_properties_action){

            }
        }
    }

}

QList<LinkPointGraphicItem*> AbstractLinkGraphicItem::getPoints() const {
    return m_points;
}


QList<LinkPointGraphicItem*> AbstractLinkGraphicItem::getEndPoints() const {
    QList<LinkPointGraphicItem*>    endPoints;

    foreach(LinkPointGraphicItem* point, m_points){
        if (point->getType() == LinkPointGraphicItem::EndPoint)
            endPoints << point;
    }

    return endPoints;
}


void AbstractLinkGraphicItem::addStandardPoints(){

}

void AbstractLinkGraphicItem::setStandardPointsPosition(){

}

void AbstractLinkGraphicItem::setPoints(const QList<LinkPointGraphicItem *> &points){
    m_points = points;
    update();
}


int AbstractLinkGraphicItem::save(){

    int tmp_result = AbstractGraphicItem::save();

    foreach(LinkPointGraphicItem* point, m_points){
        tmp_result = point->save();
        if (tmp_result != NOERR)
            break;
    }

    return tmp_result;
}

