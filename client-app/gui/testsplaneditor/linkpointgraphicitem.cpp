#include "linkpointgraphicitem.h"
#include "entities/testsplan/graphicpoint.h"
#include "entities/testsplan/testsplan.h"

#include <qmath.h>
#include <QGraphicsScene>
#include <QDebug>
#include <QMimeData>
#include <QDrag>
#include <QGraphicsSceneMouseEvent>
#include <QCursor>

LinkPointGraphicItem::LinkPointGraphicItem(GraphicPoint *graphicPoint, AbstractLinkGraphicItem* in_parent_link, Type type, LinkPointGraphicItem* previousPoint) :
    AbstractGraphicItem(GraphicItem::GraphicPointRelation::instance().getParent(graphicPoint), in_parent_link),
    m_parent_link(in_parent_link),
    m_drag(false),
    m_drag_hover_item(NULL),
    m_type(type),
    m_previous_point(previousPoint),
    m_connected_item(NULL),
    m_connection_updating(false),
    m_graphic_point(graphicPoint)
{
    if (m_previous_point)
        m_previous_point->m_next_points.append(this);

    setZValue(m_parent_link->zValue() + 1);
}

LinkPointGraphicItem* LinkPointGraphicItem::createPointForItem(GraphicItem* graphicItem, AbstractLinkGraphicItem* in_parent_link, Type type, LinkPointGraphicItem* previousPoint){
    GraphicPoint*   graphicPoint = new GraphicPoint();
    GraphicItem*    graphicPointItem = new GraphicItem();
    TestsPlan* testPlan = TestsPlan::GraphicItemRelation::instance().getParent(graphicItem);

    TestsPlan::GraphicItemRelation::instance().addChild(testPlan, graphicPointItem);

    TestsPlan::GraphicPointRelation::instance().addChild(testPlan, graphicPoint);

    GraphicItem::GraphicPointRelation::instance().addChild(graphicPointItem, graphicPoint);

    GraphicItem::ParentGraphicItemRelation::instance().addChild(graphicItem, graphicPointItem);

    return new LinkPointGraphicItem(graphicPoint, in_parent_link, type, previousPoint);
}

LinkPointGraphicItem::~LinkPointGraphicItem(){
}


QRectF LinkPointGraphicItem::boundingRect() const{
    return QRectF(-10, -10, 20, 20);
}


void LinkPointGraphicItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/){

    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);


    if (isSelected() || m_parent_link->isSelected()){
        painter->setPen(QPen(Qt::blue, 2));
        painter->setBrush(Qt::blue);
    }else if (m_parent_link->isHighlighted()){
        painter->setPen(QPen(Qt::gray, 2));
        painter->setBrush(Qt::gray);
    }else{
        painter->setPen(QPen(Qt::black, 2));
        painter->setBrush(Qt::black);
    }

    // Last point : draw an arrow
    if (m_type == EndPoint && m_previous_point != NULL){

        // Draw the arrow (triangle)
        QPainterPath path;
        path.moveTo(QPointF(4, 0));
        path.lineTo(QPointF(-4, 4 ));
        path.lineTo(QPointF(-4, -4));
        path.closeSubpath();

        painter->save();
        painter->rotate(angletBetween(m_previous_point->pos(), pos()));
        painter->drawPath(path);
        painter->restore();

    } else if (m_type == NodePoint){
        painter->drawEllipse(QRectF(-4, -4, 8, 8));
        // Draw a rect
    } else {
        painter->drawRect(QRectF(-4, -4, 8, 8));
    }
}

QVariant LinkPointGraphicItem::itemChange(GraphicsItemChange change, const QVariant &value){
    if ((change == ItemPositionHasChanged) && m_parent_link && scene()) {

        m_connection_updating = true;

        if (m_drag){

            QList<QGraphicsItem*> tmp_items = collidingItems();
            if (!tmp_items.isEmpty()){
                LinkableGraphicItem* tmp_abstract_item = NULL;
                foreach (QGraphicsItem* tmp_item, tmp_items) {
                    tmp_abstract_item = dynamic_cast<LinkableGraphicItem*>(tmp_item);
                    if (tmp_abstract_item)
                        break;
                }
                if (tmp_abstract_item != m_drag_hover_item){
                    if (m_drag_hover_item)
                        m_drag_hover_item->setHighlighted(false);

                    if (getType() != NodePoint){
                        if (m_connected_item == tmp_abstract_item || getType() != StartPoint || !(tmp_abstract_item && tmp_abstract_item->haveOutput())){
                            m_drag_hover_item = tmp_abstract_item;
                            if (m_drag_hover_item)
                                m_drag_hover_item->setHighlighted(true);
                        }
                    }
                }
            }else if (m_drag_hover_item){
                m_drag_hover_item->setHighlighted(false);
                m_drag_hover_item = NULL;
            }
        }

        updateConnectedPoints();
    }
    else if (change == ItemSelectedHasChanged){
        m_parent_link->setHighlighted(isSelected());
    }

    return QGraphicsItem::itemChange(change, value);
}


bool LinkPointGraphicItem::isConnectablePoint() const{
    return m_type == StartPoint || m_type == EndPoint;
}


AbstractLinkGraphicItem *LinkPointGraphicItem::getLink() const{
    return m_parent_link;
}

void LinkPointGraphicItem::setConnectedItem(LinkableGraphicItem *item, bool updatePos){
    if (m_connected_item != item){
        if (isConnectablePoint()){
            if (m_connected_item)
                m_connected_item->removeConnectedPoint(this);

            m_connected_item = item;
            if (m_connected_item)
                m_connected_item->addConnectedPoint(this);

        }
    }
    if (updatePos)
        updateConnection();
}

void LinkPointGraphicItem::updateConnection(){

    if (m_connected_item){
        // m_connection_updating is used to avoid infinitive recursive calls
        if (m_connection_updating){
            m_connection_updating = false;
        }else{
            QPointF newPos = m_connected_item->calculateConnectionPoint(this);
            if (newPos != pos()){
                setPos(newPos);
            }
        }
    }
}

void LinkPointGraphicItem::updateConnectedPoints(){

    foreach(LinkPointGraphicItem* nextPoint, m_next_points){
        nextPoint->updateConnection();
    }

    if (getPreviousPoint())
        getPreviousPoint()->updateConnection();

    scene()->update();
}

void LinkPointGraphicItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    m_drag = true;
    setCursor(Qt::ClosedHandCursor);
    QGraphicsItem::mousePressEvent(event);
}


void LinkPointGraphicItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    m_drag = false;
    m_connection_updating = false;
    setConnectedItem(m_drag_hover_item);
    if (m_drag_hover_item){
        m_drag_hover_item->setHighlighted(false);
        m_drag_hover_item->update();
        m_drag_hover_item = NULL;
    }

    setCursor(Qt::OpenHandCursor);
    QGraphicsItem::mouseReleaseEvent(event);
}


void LinkPointGraphicItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
}


int LinkPointGraphicItem::save(){
    bool new_item = is_empty_string(m_GraphicItem->getIdentifier()) == TRUE;
    m_GraphicItem->setValueForKey(QString(GRAPHICS_ITEMS_TYPE_POINT).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_TYPE);
    int result = AbstractGraphicItem::save();
    if (result == NOERR){
        m_graphic_point->setValueForKey(TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem)->getIdentifier(), GRAPHICS_POINTS_TABLE_TEST_PLAN_ID);
        m_graphic_point->setValueForKey(m_GraphicItem->getIdentifier(), GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID);
        m_graphic_point->setValueForKey(m_GraphicItem->getValueForKey(GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID), GRAPHICS_POINTS_TABLE_LINK_GRAPHIC_ITEM_ID);
        m_graphic_point->setValueForKey(QString(QChar(getType())).toStdString().c_str(), GRAPHICS_POINTS_TABLE_TYPE);

        if (m_connected_item && m_connected_item->getGraphicItem())
            m_graphic_point->setValueForKey(m_connected_item->getGraphicItem()->getIdentifier(), GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID);
        else
            m_graphic_point->setValueForKey(NULL, GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID);

        if (m_previous_point){
            result = m_previous_point->save();
            if (result == NOERR)
                m_graphic_point->setValueForKey(m_previous_point->getGraphicItem()->getIdentifier(), GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID);
        }

        if (result == NOERR){
            if (new_item)
                result = m_graphic_point->insertRecord();
            else
                result = m_graphic_point->saveRecord();
        }
    }

    return result;
}
