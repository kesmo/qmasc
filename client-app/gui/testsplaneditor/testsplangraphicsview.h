#ifndef TESTSPLANGRAPHICSVIEW_H
#define TESTSPLANGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>
#include <QRubberBand>

class TestsPlanGraphicsView : public QGraphicsView
{
public:
    TestsPlanGraphicsView(QWidget *parent = 0);
    ~TestsPlanGraphicsView();

protected:
    virtual void wheelEvent(QWheelEvent * event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

    QPoint mRubberBandOrigin;
    QRubberBand *mRubberBand;
};

#endif // TESTSPLANGRAPHICSVIEW_H
