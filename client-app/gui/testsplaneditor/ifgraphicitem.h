#ifndef IFGRAPHICITEM_H
#define IFGRAPHICITEM_H

#include "abstractgraphicitem.h"
#include "abstractlinkgraphicitem.h"
#include "entities/testsplan/graphicif.h"

class IfGraphicItem : public AbstractLinkGraphicItem
{
public:
    IfGraphicItem(GraphicIf* graphicIf, QGraphicsItem *parent = 0);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual void addStandardPoints();
    virtual void setStandardPointsPosition();

    QPainterPath shape() const;

    int save();

private:
    GraphicIf* m_graphic_if;
};

#endif // IFGRAPHICITEM_H
