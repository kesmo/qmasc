#include "testgraphicitem.h"
#include "entities/test.h"
#include "entities/testsplan/testsplan.h"

#include <QPainter>
#include <QFontMetrics>
#include <QStyleOptionGraphicsItem>

TestGraphicItem::TestGraphicItem(GraphicTest *graphicTest, QGraphicsItem *parent) :
    LinkableGraphicItem(GraphicItem::GraphicTestRelation::instance().getParent(graphicTest), parent),
    m_graphic_test(graphicTest)
{
    Test* test = Test::GraphicTestRelation::instance().getParent(graphicTest);
    if (test)
        m_bounding_rect = QFontMetrics(QFont()).boundingRect(test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)).adjusted(-2, -2, 2, 2);
    m_bounding_rect |= QRectF(-64, -64, 128, 64);
}

bool TestGraphicItem::mayHaveInput() const{
    return true;
}


bool TestGraphicItem::mayHaveOutput() const{
    return true;
}


QPainterPath TestGraphicItem::shape() const{
    QPainterPath tmp_path;

    tmp_path.addEllipse(m_bounding_rect);

    return tmp_path;
}


void TestGraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    Test* test = Test::GraphicTestRelation::instance().getParent(m_graphic_test);

    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    if (isSelected())
        painter->setPen(QPen(Qt::blue, 2));
    else if (m_is_highlighted)
        painter->setPen(QPen(Qt::darkGreen, 2));
    else
        painter->setPen(QPen(Qt::black, 2));

    painter->setBrush(Qt::white);

    QFont fonte;
    fonte.setPointSize(11);
    painter->drawEllipse(m_bounding_rect);
    painter->setFont(fonte);
    painter->drawText(m_bounding_rect, Qt::AlignCenter | Qt::AlignVCenter, test ? test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME) : "Inconnu");

}


int TestGraphicItem::save(){
    bool new_item = is_empty_string(m_GraphicItem->getIdentifier()) == TRUE;
    m_GraphicItem->setValueForKey(QString(GRAPHICS_ITEMS_TYPE_TEST).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_TYPE);
    int result = AbstractGraphicItem::save();
    if (result == NOERR){
        m_graphic_test->setValueForKey(TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem)->getIdentifier(), GRAPHICS_TESTS_TABLE_TEST_PLAN_ID);
        m_graphic_test->setValueForKey(m_GraphicItem->getIdentifier(), GRAPHICS_TESTS_TABLE_GRAPHIC_ITEM_ID);

        Test* tmp_test = Test::GraphicTestRelation::instance().getParent(m_graphic_test);
        m_graphic_test->setValueForKey(tmp_test ? tmp_test->getIdentifier() : NULL, GRAPHICS_TESTS_TABLE_TEST_ID);
        if (new_item)
            result = m_graphic_test->insertRecord();
        else
            result = m_graphic_test->saveRecord();
    }

    return result;
}
