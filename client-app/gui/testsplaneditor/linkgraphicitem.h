#ifndef LINKGRAPHICLINK_H
#define LINKGRAPHICLINK_H

#include "abstractlinkgraphicitem.h"
#include "entities/testsplan/graphiclink.h"

class LinkGraphicItem : public AbstractLinkGraphicItem
{
public:
    LinkGraphicItem(GraphicLink* graphicLink, QGraphicsItem* parent = 0);
    ~LinkGraphicItem();


    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    virtual void addStandardPoints();
    virtual void setStandardPointsPosition();

    QPainterPath shape() const;

    int save();

private:
    GraphicLink* m_graphic_link;

};

#endif // LINKGRAPHICLINK_H
