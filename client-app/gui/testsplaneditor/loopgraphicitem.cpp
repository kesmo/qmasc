#include "loopgraphicitem.h"
#include "linkpointgraphicitem.h"
#include "entities/testsplan/testsplan.h"

#include <qmath.h>

#include <QPainter>

LoopGraphicItem::LoopGraphicItem(GraphicLoop *graphicLoop, QGraphicsItem *parent) :
    AbstractLinkGraphicItem(GraphicItem::GraphicLoopRelation::instance().getParent(graphicLoop), parent),
    m_graphic_loop(graphicLoop)
{
}


void LoopGraphicItem::addStandardPoints(){
    LinkPointGraphicItem* tmp_start_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::StartPoint);
    LinkPointGraphicItem* tmp_end_point = LinkPointGraphicItem::createPointForItem(m_GraphicItem, this, LinkPointGraphicItem::EndPoint, tmp_start_point);
    m_points << tmp_start_point << tmp_end_point;
}

void LoopGraphicItem::setStandardPointsPosition(){
    m_points[1]->setPos(m_points[0]->pos() + QPointF(0, 80));
    update();

}

QRectF LoopGraphicItem::boundingRect() const{
    return shape().boundingRect();
}

QPainterPath LoopGraphicItem::shape() const{
    QPainterPath tmp_path;
    QPainterPathStroker tmp_stroker;

    if (m_points.count() == 2){
        float tmp_angle = radiantAngletBetween(m_points[0]->pos(), m_points[1]->pos());

        QPointF center(m_points[0]->x() + qCos(tmp_angle) * 20, m_points[0]->y() + qSin(tmp_angle) * 20);
        QPointF point(m_points[0]->x() + qCos(tmp_angle) * 40, m_points[0]->y() + qSin(tmp_angle) * 40);

        tmp_path.addEllipse(center, 20, 20);
        tmp_path.moveTo(point);
        tmp_path.lineTo(m_points[1]->pos());
    }
    return tmp_stroker.createStroke(tmp_path);
}


void LoopGraphicItem::paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/, QWidget * /*widget*/)
{
    if (m_points.count() == 2){
        painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

        QPen pen;
        if (isSelected()){
            pen = QPen(Qt::blue, 2);
        }else if (m_is_highlighted){
            pen = QPen(Qt::gray, 2);
        }else{
            pen = QPen(Qt::black, 2);
        }

        pen.setStyle(Qt::DotLine);
        painter->setPen(pen);
        painter->setBrush(Qt::NoBrush);

        float tmp_angle = radiantAngletBetween(m_points[0]->pos(), m_points[1]->pos());

        QPointF center(m_points[0]->x() + qCos(tmp_angle) * 20, m_points[0]->y() + qSin(tmp_angle) * 20);
        QPointF point(m_points[0]->x() + qCos(tmp_angle) * 40, m_points[0]->y() + qSin(tmp_angle) * 40);

        painter->drawEllipse(center, 20, 20);

        if (isSelected()){
            painter->setPen(QPen(Qt::blue, 2));
            painter->setBrush(Qt::blue);
        }else if (m_is_highlighted){
            painter->setPen(QPen(Qt::gray, 2));
            painter->setBrush(Qt::gray);
        }else{
            painter->setPen(QPen(Qt::black, 2));
            painter->setBrush(Qt::black);
        }

        // Draw an arrow (triangle)
        QPainterPath path;
        path.moveTo(QPointF(4, 0));
        path.lineTo(QPointF(-4, 4 ));
        path.lineTo(QPointF(-4, -4));
        path.closeSubpath();

        painter->save();
        painter->translate(point);
        painter->rotate(-90 + tmp_angle * 180 / M_PI);
        painter->drawPath(path);
        painter->restore();

        painter->drawLine(point, m_points[1]->pos());
    }
}


int LoopGraphicItem::save(){
    bool new_item = is_empty_string(m_GraphicItem->getIdentifier()) == TRUE;
    m_GraphicItem->setValueForKey(QString(GRAPHICS_ITEMS_TYPE_LOOP).toStdString().c_str(), GRAPHICS_ITEMS_TABLE_TYPE);
    int result = AbstractLinkGraphicItem::save();
    if (result == NOERR){
        m_graphic_loop->setValueForKey(TestsPlan::GraphicItemRelation::instance().getParent(m_GraphicItem)->getIdentifier(), GRAPHICS_LOOPS_TABLE_TEST_PLAN_ID);
        m_graphic_loop->setValueForKey(m_GraphicItem->getIdentifier(), GRAPHICS_LOOPS_TABLE_GRAPHIC_ITEM_ID);
        if (new_item)
            result = m_graphic_loop->insertRecord();
        else
            result = m_graphic_loop->saveRecord();
    }

    return result;
}
