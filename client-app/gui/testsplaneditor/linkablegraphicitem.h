#ifndef LINKABLEGRAPHICITEM_H
#define LINKABLEGRAPHICITEM_H

#include "abstractgraphicitem.h"

class LinkableGraphicItem : public AbstractGraphicItem
{
public:
    LinkableGraphicItem(GraphicItem* graphicItem, QGraphicsItem *parent = 0);
    ~LinkableGraphicItem();

    virtual bool mayHaveInput() const = 0;
    virtual bool mayHaveOutput() const = 0;

    QPointF calculateConnectionPoint(LinkPointGraphicItem* in_point_link) const;

    void addConnectedPoint(LinkPointGraphicItem* in_link_point);
    void removeConnectedPoint(LinkPointGraphicItem* in_link_point);

    bool haveOutput() const;

protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

protected:
    QList<LinkPointGraphicItem*>     m_connected_points_links;
};

#endif // LINKABLEGRAPHICITEM_H
