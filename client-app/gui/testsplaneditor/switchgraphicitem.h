#ifndef SWITCHGRAPHICITEM_H
#define SWITCHGRAPHICITEM_H

#include "abstractlinkgraphicitem.h"
#include "entities/testsplan/graphicswitch.h"

class SwitchGraphicItem : public AbstractLinkGraphicItem
{
public:
    SwitchGraphicItem(GraphicSwitch* graphicSwitch, QGraphicsItem *parent = 0);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    virtual void addStandardPoints();
    virtual void setStandardPointsPosition();

    QPainterPath shape() const;

    int save();

private:
    GraphicSwitch* m_graphic_switch;
};

#endif // SWITCHGRAPHICITEM_H
