/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "mainwindow.h"
#include "gui/forms/FormLogon.h"
#include "gui/forms/FormNewProject.h"
#include "gui/forms/FormProjectSelection.h"
#include "gui/forms/FormManageUsers.h"
#include "gui/forms/FormRequirement.h"
#include "gui/forms/FormCampaign.h"
#include "gui/forms/FormCampaignWizard.h"
#include "gui/forms/FormSearchProject.h"
#include "gui/forms/FormChangePassword.h"
#include "gui/forms/FormOptions.h"
#include "gui/forms/FormDataImport.h"
#include "gui/forms/FormDataExport.h"
#include "gui/forms/FormManageCustomfields.h"
#include "gui/components/hierarchies/projectshierarchy.h"

#include "entities/projectgrant.h"

#include "client.h"
#include "session.h"

#include "entities/projectparameter.h"


#include "../client-launcher/utils/ProcessUtils.h"

#include "services/projectsservices.h"
#include "services/testsservices.h"
#include "services/requirementsservices.h"
#include "services/campaignsservices.h"
#include "services/services.h"

#include <QNetworkProxy>
#include <QProgressBar>
#include <QMdiArea>
#include <QProgressDialog>
#include <QSignalMapper>
#include <QMdiSubWindow>
#include <QDockWidget>
#include <QMenu>
#include <QAction>
#include <QStatusBar>
#include <QMenuBar>
#include <QFileInfo>

QProgressDialog *m_progress_dialog = NULL;

void updateProgressBar(unsigned long long int in_index, unsigned long long int in_count, unsigned long long int in_step, const char* in_message)
{
#ifndef __APPLE__
    if (m_progress_dialog == NULL) {
        m_progress_dialog = new QProgressDialog("Loading...", "Cancel", 0, 100);
        QProgressBar *tmp_progress_bar = new QProgressBar(m_progress_dialog);
        m_progress_dialog->setAttribute(Qt::WA_DeleteOnClose);
        tmp_progress_bar->setFormat("%v/%m");
        //m_progress_dialog->setWindowModality(Qt::ApplicationModal);
        m_progress_dialog->setBar(tmp_progress_bar);
        m_progress_dialog->show();
    }

    m_progress_dialog->setLabelText(in_message);

    if (in_count > 0)
        m_progress_dialog->setMaximum(in_count);

    if (in_step > 0)
        m_progress_dialog->setValue(m_progress_dialog->value() + in_step);
    else
        m_progress_dialog->setValue(in_index);

    qApp->processEvents();

    if (m_progress_dialog->maximum() > 0
        && m_progress_dialog->value() >= m_progress_dialog->maximum()) {
        m_progress_dialog->close();
        delete m_progress_dialog;
        m_progress_dialog = NULL;
    }
#endif
}


MainWindow* MainWindow::m_unique_instance = NULL;

/**
  Constructeur
*/
MainWindow::MainWindow() : QMainWindow(),
    m_keepalive_timer_id(0),
    m_dock_search_widget(NULL),
    m_search_table_widget(NULL)
{
    Q_ASSERT(m_unique_instance == NULL);

    m_unique_instance = this;

//#ifndef Q_WS_MAC
//#if defined(NETSCAPE_PLUGIN)
//    loadStyleSheet(false);
//#else
//    loadStyleSheet(true);
//#endif
//#endif

    setWindowIcon(QIcon(QPixmap(QString::fromUtf8(":/images/client.png"))));

    mdiArea = new QMdiArea(this);
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setCentralWidget(mdiArea);
    windowMapper = new QSignalMapper(this);
    connect(windowMapper, SIGNAL(mapped(QWidget*)), this, SLOT(setActiveSubWindow(QWidget*)));

    setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::North);

    setWindowTitle("qmasc");

    //TODO
    //createSearchWidget();
    createMenus();
    createToolBar();
    createStatusBar();


    readSettings();

#ifdef Q_WS_MAC
    setUnifiedTitleAndToolBarOnMac(true);
#endif

    m_previous_ping_status = NOERR;

    // Lecture de la configuration
    Session::instance().initialize();
}


MainWindow::~MainWindow()
{
    delete m_search_table_widget;
    delete m_dock_search_widget;

    delete mdiArea;
    delete windowMapper;
}


void MainWindow::setActiveSubWindow(QWidget *in_window)
{
    QMdiSubWindow* tmp_child_window = dynamic_cast<QMdiSubWindow*>(in_window);

    if (tmp_child_window) {
        mdiArea->setActiveSubWindow(tmp_child_window);
    }
}

void MainWindow::loadStyleSheet(bool loadFile)
{
    QFile     tmp_css_file("standard.css");


    // Gestion de la feuille de style
    if (loadFile && tmp_css_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qApp->setStyleSheet(tmp_css_file.readAll());
        tmp_css_file.close();
    }
    else {
        tmp_css_file.setFileName(":/standard.css");
        if (tmp_css_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            qApp->setStyleSheet(tmp_css_file.readAll());
            tmp_css_file.close();
        }
    }

}

void MainWindow::createSearchWidget()
{
    // Recherche
    m_search_table_widget = new QTableWidget();
    m_search_table_widget->setColumnCount(2);
    m_search_table_widget->setColumnWidth(0, 24);
    m_search_table_widget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    m_search_table_widget->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_search_table_widget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_search_table_widget->horizontalHeader()->setVisible(false);
    m_search_table_widget->verticalHeader()->setVisible(false);
    m_search_table_widget->verticalHeader()->setDefaultSectionSize(18);
    m_search_table_widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);

    connect(m_search_table_widget, &QTableWidget::doubleClicked, this, &MainWindow::showSearchItem);
    connect(m_search_table_widget, &QTableWidget::itemSelectionChanged, this, &MainWindow::searchItemsSelectionChanged);

    m_dock_search_widget->setObjectName("search_dock_widget");
    m_dock_search_widget->setWindowTitle(tr("Résultats de la recherche"));
    m_dock_search_widget->setWidget(m_search_table_widget);
}


void MainWindow::createToolBar()
{
    QList<QAction*> tmp_menu_connection;
    QList<QAction*> tmp_menu_fichier;

    mainToolBar = new QToolBar();
    mainToolBar->setObjectName("mainToolBar");

    tmp_menu_connection.append(actionConnection);
    tmp_menu_connection.append(actionDeconnection);

    tmp_menu_fichier.append(actionNewProject);
    tmp_menu_fichier.append(actionOpenProject);

    tmp_menu_fichier.append(actionEnregistrerFenetreCourante);

    mainToolBar->addActions(tmp_menu_connection);
    mainToolBar->addSeparator();
    mainToolBar->addActions(tmp_menu_fichier);
    mainToolBar->addSeparator();

    toolbarSeparatorAboutAction = new QAction(this);
    toolbarSeparatorAboutAction->setSeparator(true);

    mainToolBar->addActions(menuHelp->actions());

    addToolBar(mainToolBar);
}


void MainWindow::createStatusBar()
{
    statusBar = new QStatusBar(this);
    setStatusBar(statusBar);
}


void MainWindow::createMenus()
{
#ifdef Q_WS_MAC
    menuBar = new QMenuBar(0);
#else
    menuBar = new QMenuBar(this);
#endif

    /* Menu fichier */
    actionConnection = new QAction(this);
    connect(actionConnection, &QAction::triggered, this, &MainWindow::logon);

    actionChangePassword = new QAction(this);
    connect(actionChangePassword, &QAction::triggered, this, &MainWindow::changePassword);
    actionChangePassword->setEnabled(false);

    actionDeconnection = new QAction(this);
    actionDeconnection->setEnabled(false);
    connect(actionDeconnection, &QAction::triggered, this, &MainWindow::logoff);

    actionNewProject = new QAction(this);
    actionNewProject->setEnabled(false);
    connect(actionNewProject, &QAction::triggered, this, &MainWindow::newProject);

    actionOpenProject = new QAction(this);
    actionOpenProject->setEnabled(false);
    connect(actionOpenProject, &QAction::triggered, this, &MainWindow::openProject);

    actionEnregistrerFenetreCourante = new QAction(this);
    actionEnregistrerFenetreCourante->setEnabled(false);
    connect(actionEnregistrerFenetreCourante, &QAction::triggered, this, &MainWindow::saveCurrentChildWindow);

    actionFermerFenetreCourante = new QAction(this);
    actionFermerFenetreCourante->setEnabled(false);
    connect(actionFermerFenetreCourante, &QAction::triggered, mdiArea, &QMdiArea::closeActiveSubWindow);

    actionFermerFenetres = new QAction(this);
    actionFermerFenetres->setEnabled(false);
    connect(actionFermerFenetres, &QAction::triggered, mdiArea, &QMdiArea::closeAllSubWindows);

    actionQuitter = new QAction(this);
    connect(actionQuitter, &QAction::triggered, qApp, &QApplication::closeAllWindows);

    menuFichier = new QMenu(menuBar);
    menuFichier->addAction(actionConnection);
    menuFichier->addAction(actionChangePassword);
    menuFichier->addAction(actionDeconnection);
    menuFichier->addSeparator();
    menuFichier->addAction(actionNewProject);
    menuFichier->addAction(actionOpenProject);
    menuFichier->addAction(actionEnregistrerFenetreCourante);
    menuFichier->addAction(actionFermerFenetreCourante);
    menuFichier->addAction(actionFermerFenetres);
    menuFichier->addSeparator();
    menuFichier->addAction(actionQuitter);

    /* Menu Administration */
    manageUsersAction = new QAction(this);
    connect(manageUsersAction, &QAction::triggered, this, &MainWindow::manageUsers);

    projectsReportsAction = new QAction(this);
    connect(projectsReportsAction, &QAction::triggered, this, &MainWindow::projectsReports);

    manageCustomFieldsMenu = new QMenu(this);

    manageCustomTestsFieldsAction = new QAction(manageCustomFieldsMenu);
    manageCustomRequirementsFieldsAction = new QAction(manageCustomFieldsMenu);

    manageCustomFieldsMenu->addAction(manageCustomTestsFieldsAction);
    manageCustomFieldsMenu->addAction(manageCustomRequirementsFieldsAction);

    connect(manageCustomTestsFieldsAction, &QAction::triggered, this, &MainWindow::manageCustomTestsFields);
    connect(manageCustomRequirementsFieldsAction, &QAction::triggered, this, &MainWindow::manageCustomRequirementsFields);

    menuAdministration = new QMenu(menuBar);
    menuAdministration->setEnabled(false);
    menuAdministration->addAction(manageUsersAction);
    menuAdministration->addAction(projectsReportsAction);
    menuAdministration->addMenu(manageCustomFieldsMenu);

    // Menu outils
    actionShowOptions = new QAction(this);
    connect(actionShowOptions, &QAction::triggered, this, &MainWindow::showOption);

    menuSelectLanguage = new QMenu(this);
    createLanguageMenu();

    menuOutils = new QMenu(menuBar);
    menuOutils->addAction(actionShowOptions);
    menuOutils->addMenu(menuSelectLanguage);
    //menuOutils->setEnabled(false);

    // Fenetres
    menuChildsWindows = new QMenu(menuBar);
    connect(menuChildsWindows, &QMenu::aboutToShow, this, &MainWindow::menuChildsWindowsAboutToShow);

    closeWindowAction = new QAction(this);
    connect(closeWindowAction, &QAction::triggered, mdiArea, &QMdiArea::closeActiveSubWindow);

    closeAllWindowsAction = new QAction(this);
    connect(closeAllWindowsAction, &QAction::triggered, mdiArea, &QMdiArea::closeAllSubWindows);

    tileWindowAction = new QAction(this);
    connect(tileWindowAction, &QAction::triggered, mdiArea, &QMdiArea::tileSubWindows);

    cascadeWindowsAction = new QAction(this);
    connect(cascadeWindowsAction, &QAction::triggered, mdiArea, &QMdiArea::cascadeSubWindows);

    nextWindowAction = new QAction(this);
    connect(nextWindowAction, &QAction::triggered, mdiArea, &QMdiArea::activateNextSubWindow);

    previousWindowAction = new QAction(this);
    connect(previousWindowAction, &QAction::triggered, mdiArea, &QMdiArea::activatePreviousSubWindow);

    separatorWindowAction = new QAction(this);
    separatorWindowAction->setSeparator(true);

    // Panels
    showSearchPanelAction = new QAction(this);
    showSearchPanelAction->setCheckable(true);
    connect(showSearchPanelAction, &QAction::triggered, this, &MainWindow::toogleSearchPanel);

    /* Menu Aide */
    aboutAction = new QAction(this);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::about);

    menuHelp = new QMenu(menuBar);
    menuHelp->addAction(aboutAction);

    // Ajout des menus à la barre de menus
    menuBar->addAction(menuFichier->menuAction());
    //menuBar->addAction(menuAdministration->menuAction());
    menuBar->addAction(menuOutils->menuAction());
    menuBar->addAction(menuChildsWindows->menuAction());
    menuBar->addAction(menuHelp->menuAction());

    menuChildsWindowsAboutToShow();

    setMenuBar(menuBar);
}


void MainWindow::createLanguageMenu()
{
    QActionGroup* tmp_languages_actions = new QActionGroup(menuSelectLanguage);
    tmp_languages_actions->setExclusive(true);

    connect(tmp_languages_actions, &QActionGroup::triggered, this, &MainWindow::slotLanguageChanged);

    // format systems language
    QString defaultLocale = QLocale::system().name();
    defaultLocale.truncate(defaultLocale.lastIndexOf('_'));

    QDir dir(":/languages");
    QStringList fileNames = dir.entryList(QStringList("client_*.qm"));

    fileNames << "client_fr.qm";

    for (int i = 0; i < fileNames.size(); ++i) {
        // get locale extracted by filename
        QString locale;
        locale = fileNames[i];
        locale.truncate(locale.lastIndexOf('.'));
        locale.remove(0, locale.indexOf('_') + 1);

        QString lang = QLocale::languageToString(QLocale(locale).language());

        QAction *action = new QAction(lang, this);
        action->setIcon(QIcon(":/images/22x22/" + locale + ".png"));
        action->setCheckable(true);
        action->setData(locale);

        menuSelectLanguage->addAction(action);
        tmp_languages_actions->addAction(action);

        // set default translators and language checked
        if (defaultLocale == locale) {
            action->setChecked(true);
        }
    }
}


void MainWindow::slotLanguageChanged(QAction* action)
{
    if (action != NULL) {
        // load the language dependant on the action content
        loadLanguage(action->data().toString());
    }
}

void MainWindow::retranslateUi()
{
    actionConnection->setIcon(QIcon(":/images/22x22/server.png"));
    actionConnection->setText(tr("Connexion..."));
    actionConnection->setShortcut(tr("Ctrl+K"));
    actionConnection->setStatusTip(tr("Connexion au référentiel d'exigences et de tests"));

    actionDeconnection->setIcon(QIcon(":/images/22x22/disconnect.png"));
    if (actionDeconnection->isEnabled() && Session::instance().getClientSession() != NULL && is_empty_string(Session::instance().getClientSession()->m_username) == FALSE)
        actionDeconnection->setText(tr("Déconn&exion") + " " + Session::instance().getClientSession()->m_username);
    else
        actionDeconnection->setText(tr("Déconn&exion"));

    actionDeconnection->setShortcut(tr("Ctrl+E"));
    actionDeconnection->setStatusTip(tr("Déconnexion du référentiel"));

    actionNewProject->setIcon(QIcon(":/images/22x22/new.png"));
    actionNewProject->setText(tr("&Nouveau projet"));
    actionNewProject->setShortcut(tr("Ctrl+N"));
    actionNewProject->setStatusTip(tr("Nouveau projet"));

    actionOpenProject->setIcon(QIcon(":/images/22x22/open.png"));
    actionOpenProject->setText(tr("&Ouvrir un projet"));
    actionOpenProject->setShortcut(tr("Ctrl+O"));
    actionOpenProject->setStatusTip(tr("Ouvrir un projet"));

    actionEnregistrerFenetreCourante->setIcon(QIcon(":/images/22x22/save.png"));
    actionEnregistrerFenetreCourante->setText(tr("&Enregistrer"));
    actionEnregistrerFenetreCourante->setShortcut(tr("Ctrl+S"));
    actionEnregistrerFenetreCourante->setStatusTip(tr("Enregistrer"));

    actionFermerFenetreCourante->setIcon(QIcon(":/images/22x22/close.png"));
    actionFermerFenetreCourante->setText(tr("&Fermer"));
    actionFermerFenetres->setShortcut(tr("Ctrl+W"));
    actionFermerFenetreCourante->setStatusTip(tr("Fermer"));

    actionFermerFenetres->setIcon(QIcon(":/images/22x22/close.png"));
    actionFermerFenetres->setText(tr("&Fermer toutes les fenêtres"));
    actionFermerFenetres->setShortcut(tr("Ctrl+Shift+W"));
    actionFermerFenetres->setStatusTip(tr("Fermer toutes les fenêtres"));

    actionQuitter->setIcon(QIcon(":/images/22x22/quit.png"));
    actionQuitter->setText(tr("&Quitter"));
    actionQuitter->setShortcut(tr("Ctrl+Q"));

    menuFichier->setTitle(tr("&Fichier"));

    manageUsersAction->setIcon(QIcon(":/images/22x22/users.png"));
    manageUsersAction->setText(tr("&Gestion des utilisateurs..."));
    manageUsersAction->setStatusTip(tr("Gestion des utilisateurs"));

    projectsReportsAction->setIcon(QIcon(":/images/22x22/stats.png"));
    projectsReportsAction->setText(tr("&Rapports de projets"));
    projectsReportsAction->setStatusTip(tr("Afficher un rapport des projets du référentiel"));

    manageCustomFieldsMenu->setIcon(QIcon(":/images/22x22/document.png"));
    manageCustomFieldsMenu->setTitle(tr("&Gestion des champs personnalisés"));
    manageCustomFieldsMenu->setStatusTip(tr("Gestion des champs personnalisés"));

    manageCustomTestsFieldsAction->setIcon(QIcon(":/images/22x22/document.png"));
    manageCustomTestsFieldsAction->setText(tr("Champs personnalisés de &tests"));
    manageCustomTestsFieldsAction->setStatusTip(tr("Gestion des champs personnalisés de tests"));

    manageCustomRequirementsFieldsAction->setIcon(QIcon(":/images/22x22/document.png"));
    manageCustomRequirementsFieldsAction->setText(tr("Champs personnalisés d'&exigences"));
    manageCustomRequirementsFieldsAction->setStatusTip(tr("Gestion des champs personnalisés d'exigences"));

    menuAdministration->setTitle(tr("&Administration"));

    actionChangePassword->setIcon(QIcon(":/images/22x22/login.png"));
    actionChangePassword->setText(tr("Changer de &mot de passe"));
    actionChangePassword->setStatusTip(tr("Changer de mot de passe"));

    actionShowOptions->setIcon(QIcon(":/images/parameters.png"));
    actionShowOptions->setText(tr("&Options"));
    actionShowOptions->setStatusTip(tr("Afficher les options de l'application"));

    menuSelectLanguage->setStatusTip(tr("Choisir une nouvelle langue"));
    menuSelectLanguage->setTitle(tr("&Langues"));

    menuOutils->setTitle(tr("&Outils"));

    menuChildsWindows->setTitle(tr("&Fenêtres"));

    closeWindowAction->setText(tr("&Fermer"));
    closeWindowAction->setStatusTip(tr("Fermer la fenêtre active"));

    closeAllWindowsAction->setText(tr("Fermer &toutes les fenêtres"));
    closeAllWindowsAction->setStatusTip(tr("Fermer toutes les fenêtres"));

    tileWindowAction->setText(tr("&Côte à côte"));
    tileWindowAction->setStatusTip(tr("Disposer les fenêtres côte à côte"));

    cascadeWindowsAction->setText(tr("&Cascade"));
    cascadeWindowsAction->setStatusTip(tr("Mettre les fenêtres en cascade"));

    nextWindowAction->setText(tr("&Suivante"));
    nextWindowAction->setStatusTip(tr("Fenêtre suivante"));

    previousWindowAction->setText(tr("&Précédente"));
    previousWindowAction->setStatusTip(tr("Fenêtre précédente"));

    showSearchPanelAction->setText(tr("Panneau de recherche"));
    showSearchPanelAction->setStatusTip(tr("Afficher/masquer le panneau de recherche"));

    aboutAction->setIcon(QIcon(":/images/22x22/a_propos.png"));
    aboutAction->setText(tr("&A propos..."));
    aboutAction->setStatusTip(tr("A propos de R.T.M.R"));

    menuHelp->setTitle(tr("&?"));

}

void MainWindow::menuChildsWindowsAboutToShow()
{
    QList<QMdiSubWindow*>           tmp_subWindowsList = mdiArea->subWindowList();
    QAction                         *tmp_action = NULL;

    QAction *tmpSeparatorWindowAction = new QAction(this);

    tmpSeparatorWindowAction->setSeparator(true);

    menuChildsWindows->clear();
    menuChildsWindows->addAction(closeWindowAction);
    menuChildsWindows->addAction(closeAllWindowsAction);
    menuChildsWindows->addSeparator();
    menuChildsWindows->addAction(tileWindowAction);
    menuChildsWindows->addAction(cascadeWindowsAction);
    menuChildsWindows->addSeparator();
    menuChildsWindows->addAction(nextWindowAction);
    menuChildsWindows->addAction(previousWindowAction);
    menuChildsWindows->addAction(tmpSeparatorWindowAction);

    menuChildsWindows->addAction(showSearchPanelAction);
    showSearchPanelAction->setChecked(isSearchPanelVisible());

    menuChildsWindows->addAction(separatorWindowAction);

    separatorWindowAction->setVisible(!tmp_subWindowsList.isEmpty());

    foreach(QMdiSubWindow *tmp_window, tmp_subWindowsList)
    {
        tmp_action = menuChildsWindows->addAction(tmp_window->windowTitle());
        tmp_action->setCheckable(true);
        tmp_action->setChecked(tmp_window->isActiveWindow());
        connect(tmp_action, SIGNAL(triggered()), windowMapper, SLOT(map()));

        windowMapper->setMapping(tmp_action, tmp_window);
    }
}



/**
  Affichage de la boite de dialogue de connection
**/
void MainWindow::logon()
{
    FormLogon      *tmp_formlogon = new FormLogon(this);

    connect(tmp_formlogon, &FormLogon::accepted, this, &MainWindow::setConnected);

    tmp_formlogon->show();
}

/**
  Déconnexion
**/
void MainWindow::logoff()
{
    setDisconnected();
    Session::instance().disconnect();
}


/**
  Affichage de la boite de dialogue de creation d'un nouveau projet
**/
void MainWindow::newProject()
{
    FormNewProject      *tmp_formproject = new FormNewProject(NULL, this);

    connect(tmp_formproject, &FormNewProject::projectSelected, this, &MainWindow::projectSelected);

    tmp_formproject->show();
}

void MainWindow::openProject()
{
    FormProjectSelection      *tmp_formproject = new FormProjectSelection(this);

    connect(tmp_formproject, &FormProjectSelection::projectSelected, this, &MainWindow::projectSelected);

    tmp_formproject->show();
}


/**
  Connexion active
**/
void MainWindow::setConnected()
{
    net_session *tmp_session = Session::instance().getClientSession();

    actionDeconnection->setText(tr("Déconn&exion") + " " + tmp_session->m_username);

    actionConnection->setEnabled(false);
    actionChangePassword->setEnabled(true);
    actionDeconnection->setEnabled(true);

    actionNewProject->setEnabled(true);
    actionOpenProject->setEnabled(true);

    if (Session::instance().currentUserRoles().contains("admin_role")) {
        menuBar->insertAction(menuHelp->menuAction(), menuAdministration->menuAction());
        menuAdministration->setEnabled(true);

        foreach(QAction *tmp_action, menuAdministration->actions())
        {
            tmp_action->setEnabled(true);
        }
    }
    else {
        menuAdministration->setEnabled(false);
        menuBar->removeAction(menuAdministration->menuAction());
    }

    m_keepalive_timer_id = startTimer(QSettings().value("server_keepalive_interval", 10).toInt() * 1000);
}


/**
  Connection inactive
**/
void MainWindow::setDisconnected()
{
    if (m_keepalive_timer_id > 0)
        killTimer(m_keepalive_timer_id);

    actionDeconnection->setText(tr("Déconn&exion"));

    actionConnection->setEnabled(true);
    actionChangePassword->setEnabled(false);
    actionDeconnection->setEnabled(false);

    actionNewProject->setEnabled(false);
    actionOpenProject->setEnabled(false);

    menuAdministration->setEnabled(false);
    menuBar->removeAction(menuAdministration->menuAction());

    if (m_search_table_widget)
        m_search_table_widget->clear();

    foreach(QMdiSubWindow *window, mdiArea->subWindowList()) {
        window->close();
    }
}


void MainWindow::checkServerConnection()
{
    int tmp_status = cl_ping_server(Session::instance().getClientSession());
    if (tmp_status != NOERR && m_previous_ping_status == NOERR) {
        QMessageBox::critical(this, tr("Connexion avec le serveur perdue..."), tr("La connexion avec le serveur est interrompue."));
    }
    m_previous_ping_status = tmp_status;
}


void MainWindow::updateActiveSubWindowMenu()
{
    if (mdiArea->activeSubWindow()) {
        actionEnregistrerFenetreCourante->setText(tr("&Enregistrer %1").arg(mdiArea->activeSubWindow()->windowTitle()));
        actionEnregistrerFenetreCourante->setEnabled(true);

        actionFermerFenetreCourante->setText(tr("&Fermer %1").arg(mdiArea->activeSubWindow()->windowTitle()));
        actionFermerFenetreCourante->setEnabled(true);
    }
    else {
        actionEnregistrerFenetreCourante->setText(tr("&Enregistrer"));
        actionEnregistrerFenetreCourante->setEnabled(false);

        actionFermerFenetreCourante->setText(tr("&Fermer"));
        actionFermerFenetreCourante->setEnabled(false);
    }

    actionFermerFenetres->setEnabled(!mdiArea->subWindowList().isEmpty());
}


void MainWindow::readSettings()
{
    QSettings settings;
    QPoint pos = settings.value("pos", QPoint(0, 0)).toPoint();
    QSize size = settings.value("size", QSize(800, 600)).toSize();

    QString tmp_prefered_language(settings.value("prefered_language", "").toString());

    move(pos);
    resize(size);

    ProcessUtils::readAndSetApplicationProxySettings();

    // Chargement de la langue du système par défaut
    loadLanguage(QLocale::system().name().section('_', 0, 0));

    if (!tmp_prefered_language.isEmpty()) {
        foreach(QAction *action, menuSelectLanguage->actions())
        {
            if (tmp_prefered_language.compare(action->data().toString()) == 0) {
                loadLanguage(tmp_prefered_language);
                action->setChecked(true);
            }
        }
    }
}

void MainWindow::writeSettings()
{
    QSettings settings;

    // Main window
    settings.setValue("pos", pos());
    settings.setValue("size", size());

    if (m_dock_search_widget) {
        if (m_dock_search_widget->isVisible())
            settings.setValue("dock_search_area", (int)dockWidgetArea(m_dock_search_widget));

        settings.setValue("dock_search_visibility", m_dock_search_widget->isVisible());

        QList<QDockWidget*> dock_search_tabs = tabifiedDockWidgets(m_dock_search_widget);
    }

}


void MainWindow::timerEvent(QTimerEvent * event)
{
    if (event->timerId() == m_keepalive_timer_id) {
        checkServerConnection();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    bool tmp_all_windows_closed = true;
    foreach(QMdiSubWindow *window, mdiArea->subWindowList())
    {
        if (window->close()) {
            delete window->widget();
        }
        else {
            tmp_all_windows_closed = false;
            break;
        }
    }

    if (tmp_all_windows_closed) {
        writeSettings();
        event->accept();
    }
    else
        event->ignore();
}


void MainWindow::changeEvent(QEvent* event)
{
    if (0 != event) {
        switch (event->type()) {
            // this event is send if a translator is loaded
        case QEvent::LanguageChange:
            retranslateUi();
            break;
            // this event is send, if the system, language changes
        case QEvent::LocaleChange:
        {
            QString locale = QLocale::system().name();
            locale.truncate(locale.lastIndexOf('_'));

        }
        break;
        default:
            break;
        }
    }

    QMainWindow::changeEvent(event);
}


void MainWindow::loadLanguage(const QString& in_language)
{
    Session::instance().loadLanguage(in_language);
    menuSelectLanguage->setIcon(QIcon(":/images/22x22/" + Session::instance().getLangage() + ".png"));
}


/**
  Affichage de la fenetre de gestion des utilisateurs
**/
void MainWindow::manageUsers()
{
    FormManageUsers   *tmp_users_form = new FormManageUsers(this);

    tmp_users_form->show();
}


/**
  Affichage de la fenetre de rapports des projets
**/
void MainWindow::projectsReports()
{
    Gui::Services::Projects::showProjectsStats();
}


/**
  Affichage de la fenetre de gestion des champs personnalisés
**/
void MainWindow::manageCustomTestsFields()
{
    FormManageCustomFields   *tmp_customfields_form = new FormManageCustomFields(CUSTOM_FIELDS_TEST, this);

    tmp_customfields_form->show();
}


void MainWindow::manageCustomRequirementsFields()
{
    FormManageCustomFields   *tmp_customfields_form = new FormManageCustomFields(CUSTOM_FIELDS_REQUIERMENT, this);

    tmp_customfields_form->show();
}





void MainWindow::about()
{
    char tmp_server_version[12];
    long            tmp_api_protocol_version = cl_api_protocol_version();
    QString tmp_about_msg = "<b>R.T.M.R</b><BR>" + tr("Version du client : ")
        + qApp->applicationVersion();

    tmp_about_msg += "<br>" + tr("Version du protocole : %1.%2.%3.%4")
        .arg(PROTOCOL_VERSION >> 24)
        .arg((PROTOCOL_VERSION >> 16) & 0xFF)
        .arg((PROTOCOL_VERSION >> 8) & 0xFF)
        .arg(PROTOCOL_VERSION & 0xFF);

#ifdef GIT_COMMIT
#define QUOTE(string)   #string
#define TO_STRING(string)   QUOTE(string)
    tmp_about_msg += "<br>" + tr("Révision : ") + TO_STRING(GIT_COMMIT);
#else
#ifdef APP_REVISION
    if (is_empty_string(APP_REVISION) == FALSE)
        tmp_about_msg += "<br>" + tr("Révision : ") + QString(APP_REVISION);
#endif
#endif

    tmp_about_msg += "<br>Qt version : ";
    tmp_about_msg += QT_VERSION_STR;

    tmp_about_msg += "<br><br><b>" + tr("Bibliothèque") + "</b><BR>" + tr("Version du protocole de l'API : %1.%2.%3.%4")
        .arg(tmp_api_protocol_version >> 24)
        .arg((tmp_api_protocol_version >> 16) & 0xFF)
        .arg((tmp_api_protocol_version >> 8) & 0xFF)
        .arg(tmp_api_protocol_version & 0xFF);

    if (Session::instance().getClientSession() != NULL) {
        if (cl_get_server_infos(Session::instance().getClientSession()) == NOERR) {
            net_get_field(NET_MESSAGE_TYPE_INDEX + 1, Session::instance().getClientSession()->m_response, tmp_server_version, SEPARATOR_CHAR);
            tmp_about_msg += "<br><br><b>" + tr("Serveur") + "</b><br>" + tr("Version du serveur : ") + tmp_server_version;

            net_get_field(NET_MESSAGE_TYPE_INDEX + 3, Session::instance().getClientSession()->m_response, tmp_server_version, SEPARATOR_CHAR);
            tmp_about_msg += "<br>" + tr("Version de la base de données : ") + ProjectVersion::formatProjectVersionNumber(tmp_server_version);

            net_get_field(NET_MESSAGE_TYPE_INDEX + 2, Session::instance().getClientSession()->m_response, tmp_server_version, SEPARATOR_CHAR);
            tmp_about_msg += "<br>" + tr("Version du protocole : ") + tmp_server_version;
            tmp_about_msg += "<br>" + tr("Serveur") + " : " + Session::instance().getClientSession()->m_hostname;
            tmp_about_msg += "<br>" + tr("Port") + " : " + QString::number(Session::instance().getClientSession()->m_host_port);

            net_get_field(NET_MESSAGE_TYPE_INDEX + 4, Session::instance().getClientSession()->m_response, tmp_server_version, SEPARATOR_CHAR);
            tmp_about_msg += "<br>" + tr("Version du serveur de données : ") + ProjectVersion::formatProjectVersionNumber(tmp_server_version);
        }
    }

    if (Session::instance().externalsModules().count() > 0) {
        tmp_about_msg += "<br><table><tr><th>" + tr("Module") + "</th><th>" + tr("Type") + "</th><th>" + tr("Version") + "</th><th>" + tr("Informations") + "</th></tr>";
        QMap<QString, ClientModule*> tmp_module_map;
        foreach(tmp_module_map, Session::instance().externalsModules().values())
        {
            foreach(ClientModule* tmp_module, tmp_module_map.values())
            {
                tmp_about_msg += "<tr><td>" + tmp_module->getModuleName() + "</td><td>" + TR_CUSTOM_MESSAGE(ClientModule::getTypeDescriptionForModule(tmp_module).toStdString().c_str()) + "</td>";
                tmp_about_msg += "<td align=\"center\">" + tmp_module->getModuleVersion() + "</td><td><a href=\"" + tmp_module->getModuleInformationsUrl() + "\">" + tmp_module->getModuleInformationsUrl() + "</a></td></tr>";
            }
        }
        tmp_about_msg += "</table>";
    }

    QMessageBox::about(
        this,
        tr("A propos de R.T.M.R"),
        tmp_about_msg);
}


/**
  Affichage de la fenetre de changement de mot de passe
**/
void MainWindow::changePassword()
{
    QString tmp_username = cl_current_user(Session::instance().getClientSession());
    FormChangePassword *tmp_form = new FormChangePassword(tmp_username, this);

    tmp_form->show();
}


/**
  Affichage de la fenetre de gestion des options de l'application
**/
void MainWindow::showOption()
{
    FormOptions *tmp_form = new FormOptions(this);

    tmp_form->show();
}


void MainWindow::saveCurrentChildWindow()
{
    QMdiSubWindow* tmp_child_window = mdiArea->currentSubWindow();
    if (tmp_child_window) {
        AbstractProjectWidget* tmp__widget = dynamic_cast<AbstractProjectWidget*>(tmp_child_window->widget());
        if (tmp__widget)
            tmp__widget->save();
    }
}


void MainWindow::toogleSearchPanel()
{
    if (m_dock_search_widget)
        m_dock_search_widget->setVisible(!m_dock_search_widget->isVisible());
}


void MainWindow::showCylicRedundancyAlert()
{
    QApplication::restoreOverrideCursor();
    QMessageBox::warning(this, tr("Redondance cyclique..."),
                         tr("Un des scénarios que vous voulez insérer :") \
                         + "<ul><li>" + tr("est un lien vers un autre scénario") + "</li>" \
                         + "<li>" + tr("fait directement ou indirectement référence au scénario de destination.") + "</li></ul>");
}


bool MainWindow::isSearchPanelVisible()
{
    return m_dock_search_widget && m_dock_search_widget->isVisible();
}

QMdiSubWindow* MainWindow::createMdiChildForRecord(QWidget* child)
{
    QMdiSubWindow* tmp_mdi_child = new QMdiSubWindow(this);
    tmp_mdi_child->setWidget(child);
    tmp_mdi_child->setAttribute(Qt::WA_DeleteOnClose);
    mdiArea->addSubWindow(tmp_mdi_child);
    connect(child, &QWidget::destroyed, tmp_mdi_child, &QMdiSubWindow::close);
    connect(tmp_mdi_child, &QMdiSubWindow::destroyed, this, &MainWindow::updateActiveSubWindowMenu);
    return tmp_mdi_child;
}


void MainWindow::searchItemsSelectionChanged()
{
    //TODO
    //Record                  *tmp_record = NULL;
    //QModelIndex tmp_model_index;

    //m_projects_tree_view->selectionModel()->select(m_projects_tree_view->selectionModel()->selection(), QItemSelectionModel::Deselect);

    //foreach(QTableWidgetItem *tmp_item, m_search_table_widget->selectedItems())
    //{
    //    tmp_record = (Record*)tmp_item->data(Qt::UserRole).value<void*>();
    //    tmp_model_index = m_projects_tree_model->modelIndexForRecord(tmp_record);
    //    if (tmp_model_index.isValid()) {
    //        m_projects_tree_view->selectionModel()->select(tmp_model_index, QItemSelectionModel::Select);
    //        m_projects_tree_view->scrollTo(tmp_model_index);
    //        m_projects_tree_view->expandIndex(tmp_model_index);
    //    }
    //}
}


/**
  Afficher les informations de l'item sélectionné dans la liste de recherche
**/
void MainWindow::showSearchItem(QModelIndex in_model_index)
{
    //TODO
    //QTableWidgetItem *tmp_item = NULL;

    //if (in_model_index.isValid()) {
    //    tmp_item = m_search_table_widget->item(in_model_index.row(), in_model_index.column());
    //    if (tmp_item != NULL) {
    //        Gui::Services::Global::showRecordInfos(m_projects_tree_view, (Record*)tmp_item->data(Qt::UserRole).value<void*>());
    //    }
    //}
}




/**
  Affichage de la fenetre de recherche
**/
void MainWindow::searchProject()
{
    //TODO
    //ProjectVersion* projectVersion = Gui::Services::Projects::projectVersionForIndex(m_projects_tree_view->getRecordsTreeModel(), m_projects_tree_view->selectionModel()->currentIndex());
    //if (!projectVersion)
    //    projectVersion = Session::instance().getDefaultProjectVersion();

    //if (projectVersion) {
    //    FormSearchProject *tmp_form = new FormSearchProject(projectVersion, this);

    //    connect(tmp_form, SIGNAL(foundRecords(const QList<Record*> &)), this, &MainWindow::showSearchResults(const QList<Record*>&)));
    //    tmp_form->show();
    //}
    //else {
    //    QMessageBox::warning(this, tr("Aucun projet sélectionné"), tr("Vous devez sélectionner une version de projet ou en définir une par défaut pour pouvoir faire des recherches par contenu."));
    //}
}



void MainWindow::showSearchResults(const QList<Record*> & out_records_list)
{
    int tmp_index = 0;

    m_dock_search_widget->show();
    m_search_table_widget->setRowCount(out_records_list.count());

    foreach(Record *tmp_record, out_records_list)
    {
        setSearchRecordAtIndex(tmp_record, tmp_index++);
    }
}


void MainWindow::setSearchRecordAtIndex(Record *in_record, int in_index)
{
    QTableWidgetItem        *tmp_icon_widget_item = NULL;
    QTableWidgetItem        *tmp_widget_item = NULL;

    tmp_icon_widget_item = new QTableWidgetItem;
    tmp_widget_item = new QTableWidgetItem;

    if (in_record != NULL) {
        if (in_record->getEntityDefSignatureId() == TESTS_HIERARCHY_SIG_ID) {
            tmp_icon_widget_item->setIcon(QIcon(":/images/22x22/pellicule.png"));
            tmp_widget_item->setText(in_record->getValueForKey(TESTS_HIERARCHY_SHORT_NAME));
        }
        else if (in_record->getEntityDefSignatureId() == REQUIREMENTS_HIERARCHY_SIG_ID) {
            tmp_icon_widget_item->setIcon(QIcon(":/images/22x22/notes.png"));
            tmp_widget_item->setText(in_record->getValueForKey(REQUIREMENTS_HIERARCHY_SHORT_NAME));
        }
    }
    else {
        tmp_widget_item->setText(tr("Aucune données trouvées."));
    }

    tmp_icon_widget_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_record));
    m_search_table_widget->setItem(in_index, 0, tmp_icon_widget_item);

    tmp_widget_item->setData(Qt::UserRole, QVariant::fromValue<void*>((void*)in_record));
    m_search_table_widget->setItem(in_index, 1, tmp_widget_item);
}


void MainWindow::projectSelected(const QString &projectId, const QString &projectVersionId)
{
    Gui::Services::Projects::showProjectView(projectId, projectVersionId);
}


#if defined(NETSCAPE_PLUGIN)
QTNPFACTORY_BEGIN("Qt-based RTMR Plugin", "A Qt-based NSAPI plug-in for RTMR client application");
QTNPCLASS(MainWindow)
QTNPFACTORY_END()

#ifdef QAXSERVER
#include <ActiveQt/QAxFactory>
QAXFACTORY_BEGIN("{aa3216bf-7e20-482c-84c6-06167bacb616}", "{08538ca5-eb7a-4f24-a3c4-a120c6e04dc4}")
QAXCLASS(MainWindow)
QAXFACTORY_END()
#endif

#endif

