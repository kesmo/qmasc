# -------------------------------------------------
# Project created by QtCreator 2009-08-22T07:23:20
# -------------------------------------------------
TARGET = rtmrapp
QT += network xml widgets printsupport
TEMPLATE = app

DEFINES += RTMR_LIBRARY_IMPORTS
DEFINES += AUTOMATION_LIB
DEFINES += AUTOMATION_LIBRARY_IMPORTS

#DEFINES += ELEVATED_UAC


win32 {
    contains(DEFINES, ELEVATED_UAC) {
        CONFIG -= embed_manifest_exe
        Debug{
            QMAKE_POST_LINK += $$quote(mt.exe -nologo -manifest \"$${PWD}\\rtmrapp.exe.manifest\" -outputresource:\"$${PWD}\\Debug/rtmrapp.exe\")
        } else {
            QMAKE_POST_LINK += $$quote(mt.exe -nologo -manifest \"$${PWD}\\rtmrapp.exe.manifest\" -outputresource:\"$${PWD}\\Release/rtmrapp.exe\")
        }
    }
}
include (../common/common.pri)
unix:target.path = $$BINDIR
mac:ICON = images/client.icns
win32:RC_FILE = resources.rc
SOURCES += gui/forms/form_user_projects_grants.cpp \
    gui/forms/form_bug.cpp \
    gui/forms/form_options.cpp \
    gui/forms/form_executions_reports.cpp \
    objects/Trinome.cpp \
    objects/TrinomeArray.cpp \
    objects/DataObject.cpp \
    objects/Comparable.cpp \
    gui/forms/form_project_version.cpp \
    gui/components/record_text_edit_toolbar.cpp \
    gui/components/record_text_edit_widget.cpp \
    gui/components/record_text_edit.cpp \
    gui/components/record_text_edit_container.cpp \
    gui/components/records_table_widget.cpp \
    gui/components/records_tree_model.cpp \
    gui/components/records_tree_view.cpp \
    gui/forms/form_campaign.cpp \
    gui/forms/form_campaign_wizard.cpp \
    gui/forms/form_execution_campaign.cpp \
    gui/forms/form_execution_test.cpp \
    gui/forms/form_logon.cpp \
    gui/forms/form_manage_users.cpp \
    gui/forms/form_new_user.cpp \
    gui/forms/form_new_version.cpp \
    gui/forms/form_new_project.cpp \
    gui/forms/form_project_selection.cpp \
    gui/forms/form_requirement.cpp \
    gui/forms/form_test.cpp \
    gui/mainwindow.cpp \
    main.cpp \
    imaging/GradientColor.cpp \
    imaging/Chart.cpp \
    imaging/Image.cpp \
    imaging/Rectangle3D.cpp \
    imaging/GraphicImage.cpp \
    gui/forms/form_projects_reports.cpp \
    gui/forms/form_search_project.cpp \
    gui/forms/form_change_password.cpp \
    gui/forms/form_execution_bugs.cpp \
    gui/forms/form_data_import.cpp \
    gui/forms/form_project_bugs.cpp \
    gui/forms/form_data_export.cpp \
    ../client-launcher/utils/process_utils.cpp \
    gui/components/test_action_attachments_manager.cpp \
    gui/components/abstract_project_widget.cpp \
    gui/components/records_table_model.cpp \
    gui/forms/form_manage_customfields.cpp \
    gui/forms/form_customfielddesc.cpp \
    gui/components/records_table_view.cpp \
    gui/components/custom_fields_controls_manager.cpp \
    gui/components/abstractdataselector.cpp \
    gui/testsplaneditor/abstractgraphicitem.cpp \
    gui/testsplaneditor/linkgraphicitem.cpp \
    automation/AbstractAutomationProcessor.cpp \
    automation/BatchAutomationProcessor.cpp \
    gui/testsplaneditor/testsplanscene.cpp \
    gui/testsplaneditor/testgraphicitem.cpp \
    gui/testsplaneditor/loopgraphicitem.cpp \
    gui/testsplaneditor/switchgraphicitem.cpp \
    gui/testsplaneditor/ifgraphicitem.cpp \
    gui/testsplaneditor/linkpointgraphicitem.cpp \
    gui/testsplaneditor/testsplaneditortoolbar.cpp \
    gui/testsplaneditor/testsplangraphicsview.cpp \
    gui/forms/form_projects_compare.cpp \
    gui/components/hierarchies/needhierarchy.cpp \
    gui/components/hierarchies/projects_hierarchy.cpp \
    gui/components/hierarchies/generichierarchy.cpp \
    gui/components/hierarchies/projecthierarchy.cpp \
    gui/components/hierarchies/filterhierarchy.cpp \
    gui/components/hierarchies/projectversionhierarchy.cpp \
    gui/components/hierarchies/campaignhierarchy.cpp \
    gui/components/hierarchies/testcampaignhierarchy.cpp \
    gui/components/hierarchies/executiontesthierarchy.cpp \
    gui/components/hierarchies/executioncampaignhierarchy.cpp \
    gui/components/hierarchies/projecttestshierarchy.cpp \
    gui/components/hierarchies/projectrequirementshierarchy.cpp \
    gui/components/hierarchies/projectcampaignshierarchy.cpp \
    gui/components/hierarchies/projectfeatureshierarchy.cpp \
    gui/components/hierarchies/projectruleshierarchy.cpp \
    gui/components/hierarchies/projectversionshierarchy.cpp \
    gui/components/hierarchies/projectneedshierarchy.cpp \
    gui/components/hierarchies/testhierarchy.cpp \
    gui/components/hierarchies/requirementhierarchy.cpp \
    gui/components/hierarchies/hierarchy.cpp \
    gui/services/services.cpp \
    gui/services/testsservices.cpp \
    gui/services/requirementsservices.cpp \
    gui/services/campaignsservices.cpp \
    gui/services/projectsservices.cpp \
    gui/services/hierarchycontextmenusservices.cpp \
    gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.cpp \
    gui/components/importexportcontext.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/insertaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/removetestaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/insertchildtestaction.cpp \
    gui/components/hierarchies/contextmenus/expandnodeaction.cpp \
    gui/components/hierarchies/contextmenus/collapsenodeaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/selectlinktestsaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/importtestsaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/exporttestsaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/printtestsaction.cpp \
    gui/components/hierarchies/contextmenus/testhierarchy/saveashtmltestsaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/selectdependanttestsaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/saveashtmlrequirementsaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/removerequirementaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/printrequirementsaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/insertrequirementaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/insertchildrequirementaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/importrequirementsaction.cpp \
    gui/components/hierarchies/contextmenus/requirementhierarchy/exportrequirementsaction.cpp \
    gui/components/hierarchies/contextmenus/campaignhierarchy/removecampaignaction.cpp \
    gui/components/hierarchies/contextmenus/campaignhierarchy/insertcampaignaction.cpp \
    gui/components/hierarchies/contextmenus/actionsfactory.cpp \
    gui/components/hierarchies/contextmenus/separatoraction.cpp \
    gui/components/hierarchies/contextmenus/projecthierarchy/exportprojectaction.cpp \
    gui/components/hierarchies/contextmenus/projectshierarchy/importprojectaction.cpp \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/showpropertiesaction.cpp \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/showexecutionsreportsaction.cpp \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/showbugsaction.cpp \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/searchaction.cpp \
    gui/components/hierarchies/contextmenus/projecthierarchy/insertprojectversionaction.cpp \
    gui/components/hierarchies/contextmenus/projectshierarchy/insertprojectaction.cpp \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/setasdefaultprojectversionaction.cpp \
    gui/components/hierarchies/contextmenus/projecthierarchy/removeprojectaction.cpp \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/removeprojectversionaction.cpp \
    gui/forms/form_feature.cpp \
    gui/forms/form_need.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/saveashtmlfeaturesaction.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/removefeatureaction.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/printfeaturesaction.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/insertfeatureaction.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/insertchildfeatureaction.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/importfeaturesaction.cpp \
    gui/components/hierarchies/contextmenus/featurehierarchy/exportfeaturesaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/saveashtmlneedsaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/removeneedaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/printneedsaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/insertneedaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/insertchildneedaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/importneedsaction.cpp \
    gui/components/hierarchies/contextmenus/needhierarchy/exportneedsaction.cpp \
    gui/services/needsservices.cpp \
    gui/services/featuresservices.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/saveashtmlrulesaction.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/removeruleaction.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/printrulesaction.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/insertruleaction.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/insertchildruleaction.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/importrulesaction.cpp \
    gui/components/hierarchies/contextmenus/rulehierarchy/exportrulesaction.cpp \
    gui/services/rulesservices.cpp \
    gui/forms/form_rule.cpp \
    gui/components/hierarchies/featurehierarchy.cpp \
    gui/components/hierarchies/rulehierarchy.cpp \
    gui/forms/form_project.cpp \
    gui/components/hierarchies/contextmenus/projecthierarchy/showprojectpropertiesaction.cpp \
    gui/components/hierarchies/projectparametershierarchy.cpp \
    gui/components/hierarchies/nochildrenhierarchy.cpp \
    gui/components/hierarchies/userprojectsgrantshierarchy.cpp \
    gui/components/hierarchies/userprojectgrantshierarchy.cpp \
    gui/components/hierarchies/projectgranthierarchy.cpp \
    gui/components/widgets/abstractcolumnrecordwidget.cpp \
    gui/components/widgets/radiobuttons.cpp \
    gui/components/projectversionfiltermodel.cpp \
    gui/components/hierarchies/contextmenus/projectshierarchy/showprojectsreportaction.cpp \
    automation/GuiAutomationRecordProcessor.cpp \
    automation/GuiAutomationPlaybackProcessor.cpp \
    gui/testsplaneditor/abstractlinkgraphicitem.cpp \
    gui/testsplaneditor/linkablegraphicitem.cpp \
    gui/components/hierarchies/executiontestsplanhierarchy.cpp

HEADERS += gui/forms/form_user_projects_grants.h \
    gui/forms/form_bug.h \
    gui/forms/form_options.h \
    gui/forms/form_executions_reports.h \
    objects/Trinome.h \
    objects/TrinomeArray.h \
    objects/DataObject.h \
    objects/Comparable.h \
    gui/forms/form_project_version.h \
    gui/components/record_text_edit.h \
    gui/components/record_text_edit_container.h \
    gui/components/records_table_widget.h \
    gui/components/records_tree_model.h \
    gui/components/records_tree_view.h \
    gui/forms/form_campaign.h \
    gui/forms/form_campaign_wizard.h \
    gui/forms/form_execution_campaign.h \
    gui/forms/form_execution_test.h \
    gui/forms/form_logon.h \
    gui/forms/form_manage_users.h \
    gui/forms/form_new_user.h \
    gui/forms/form_new_version.h \
    gui/forms/form_new_project.h \
    gui/forms/form_project_selection.h \
    gui/forms/form_test.h \
    gui/mainwindow.h \
    gui/forms/form_requirement.h \
    gui/components/record_text_edit_toolbar.h \
    gui/components/record_text_edit_widget.h \
    imaging/GradientColor.h \
    imaging/Chart.h \
    imaging/Chain.h \
    imaging/Image.h \
    imaging/Rectangle3D.h \
    imaging/GraphicImage.h \
    gui/forms/form_projects_reports.h \
    gui/forms/form_search_project.h \
    gui/forms/form_change_password.h \
    gui/forms/form_execution_bugs.h \
    gui/forms/form_data_import.h \
    gui/forms/form_project_bugs.h \
    gui/forms/form_data_export.h \
    ../client-modules/clientmodule.h \
    ../client-launcher/utils/process_utils.h \
    gui/components/test_action_attachments_manager.h \
    gui/components/abstract_project_widget.h \
    gui/components/records_table_model.h \
    gui/forms/form_manage_customfields.h \
    gui/forms/form_customfielddesc.h \
    gui/components/records_table_view.h \
    gui/components/custom_fields_controls_manager.h \
    gui/components/abstractdataselector.h \
    gui/testsplaneditor/abstractgraphicitem.h \
    gui/testsplaneditor/linkgraphicitem.h \
    automation/AbstractAutomationProcessor.h \
    automation/BatchAutomationProcessor.h \
    gui/testsplaneditor/testsplanscene.h \
    gui/testsplaneditor/testgraphicitem.h \
    gui/testsplaneditor/loopgraphicitem.h \
    gui/testsplaneditor/switchgraphicitem.h \
    gui/testsplaneditor/ifgraphicitem.h \
    gui/testsplaneditor/linkpointgraphicitem.h \
    gui/testsplaneditor/testsplaneditortoolbar.h \
    gui/testsplaneditor/testsplangraphicsview.h \
    gui/forms/form_projects_compare.h \
    gui/components/hierarchies/needhierarchy.h \
    gui/components/hierarchies/projects_hierarchy.h \
    gui/components/hierarchies/generichierarchy.h \
    gui/components/hierarchies/projecthierarchy.h \
    gui/components/hierarchies/filterhierarchy.h \
    gui/components/hierarchies/projectversionhierarchy.h \
    gui/components/hierarchies/campaignhierarchy.h \
    gui/components/hierarchies/testcampaignhierarchy.h \
    gui/components/hierarchies/executiontesthierarchy.h \
    gui/components/hierarchies/executioncampaignhierarchy.h \
    gui/components/hierarchies/projecttestshierarchy.h \
    gui/components/hierarchies/projectrequirementshierarchy.h \
    gui/components/hierarchies/projectcampaignshierarchy.h \
    gui/components/hierarchies/projectfeatureshierarchy.h \
    gui/components/hierarchies/projectruleshierarchy.h \
    gui/components/hierarchies/projectversionshierarchy.h \
    gui/components/hierarchies/projectneedshierarchy.h \
    gui/components/hierarchies/testhierarchy.h \
    gui/components/hierarchies/requirementhierarchy.h \
    gui/components/hierarchies/hierarchy.h \
    gui/services/services.h \
    gui/services/testsservices.h \
    gui/services/requirementsservices.h \
    gui/services/campaignsservices.h \
    gui/services/projectsservices.h \
    gui/services/hierarchycontextmenusservices.h \
    gui/components/hierarchies/contextmenus/hierarchycontextmenuaction.h \
    gui/components/importexportcontext.h \
    gui/components/hierarchies/contextmenus/testhierarchy/insertaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/removetestaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/insertchildtestaction.h \
    gui/components/hierarchies/contextmenus/expandnodeaction.h \
    gui/components/hierarchies/contextmenus/collapsenodeaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/selectlinktestsaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/importtestsaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/exporttestsaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/printtestsaction.h \
    gui/components/hierarchies/contextmenus/testhierarchy/saveashtmltestsaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/selectdependanttestsaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/saveashtmlrequirementsaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/removerequirementaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/printrequirementsaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/insertrequirementaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/insertchildrequirementaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/importrequirementsaction.h \
    gui/components/hierarchies/contextmenus/requirementhierarchy/exportrequirementsaction.h \
    gui/components/hierarchies/contextmenus/campaignhierarchy/removecampaignaction.h \
    gui/components/hierarchies/contextmenus/campaignhierarchy/insertcampaignaction.h \
    gui/components/hierarchies/contextmenus/actionsfactory.h \
    gui/components/hierarchies/contextmenus/separatoraction.h \
    gui/components/hierarchies/contextmenus/projecthierarchy/exportprojectaction.h \
    gui/components/hierarchies/contextmenus/projectshierarchy/importprojectaction.h \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/showpropertiesaction.h \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/showexecutionsreportsaction.h \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/showbugsaction.h \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/searchaction.h \
    gui/components/hierarchies/contextmenus/projecthierarchy/insertprojectversionaction.h \
    gui/components/hierarchies/contextmenus/projectshierarchy/insertprojectaction.h \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/setasdefaultprojectversionaction.h \
    gui/components/hierarchies/contextmenus/projecthierarchy/removeprojectaction.h \
    gui/components/hierarchies/contextmenus/projectversionhierarchy/removeprojectversionaction.h \
    gui/forms/form_feature.h \
    gui/forms/form_need.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/saveashtmlfeaturesaction.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/removefeatureaction.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/printfeaturesaction.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/insertfeatureaction.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/insertchildfeatureaction.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/importfeaturesaction.h \
    gui/components/hierarchies/contextmenus/featurehierarchy/exportfeaturesaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/saveashtmlneedsaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/removeneedaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/printneedsaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/insertneedaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/insertchildneedaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/importneedsaction.h \
    gui/components/hierarchies/contextmenus/needhierarchy/exportneedsaction.h \
    gui/services/needsservices.h \
    gui/services/featuresservices.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/saveashtmlrulesaction.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/removeruleaction.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/printrulesaction.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/insertruleaction.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/insertchildruleaction.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/importrulesaction.h \
    gui/components/hierarchies/contextmenus/rulehierarchy/exportrulesaction.h \
    gui/services/rulesservices.h \
    gui/forms/form_rule.h \
    gui/components/hierarchies/featurehierarchy.h \
    gui/components/hierarchies/rulehierarchy.h \
    gui/forms/form_project.h \
    gui/components/hierarchies/contextmenus/projecthierarchy/showprojectpropertiesaction.h \
    gui/components/hierarchies/projectparametershierarchy.h \
    gui/components/hierarchies/nochildrenhierarchy.h \
    gui/components/hierarchies/userprojectsgrantshierarchy.h \
    gui/components/hierarchies/userprojectgrantshierarchy.h \
    gui/components/hierarchies/projectgranthierarchy.h \
    gui/components/widgets/abstractcolumnrecordwidget.h \
    gui/components/widgets/radiobuttons.h \
    gui/components/projectversionfiltermodel.h \
    gui/components/hierarchies/contextmenus/projectshierarchy/showprojectsreportaction.h \
    automation/GuiAutomationRecordProcessor.h \
    automation/GuiAutomationPlaybackProcessor.h \
    gui/testsplaneditor/abstractlinkgraphicitem.h \
    gui/testsplaneditor/linkablegraphicitem.h \
    gui/components/hierarchies/executiontestsplanhierarchy.h

FORMS += gui/forms/Form_User_Projects_Grants.ui \
    gui/forms/Form_Bug.ui \
    gui/forms/Form_Options.ui \
    gui/forms/Form_Executions_Reports.ui \
    gui/forms/Form_Project_Version.ui \
    gui/forms/Form_Campaign.ui \
    gui/forms/Form_Campaign_Wizard.ui \
    gui/forms/Form_Execution_Action.ui \
    gui/forms/Form_Execution_Campaign.ui \
    gui/forms/Form_Execution_Test.ui \
    gui/forms/Form_Logon.ui \
    gui/forms/Form_Manage_Users.ui \
    gui/forms/Form_New_User.ui \
    gui/forms/Form_New_Version.ui \
    gui/forms/Form_New_Project.ui \
    gui/forms/Form_Project_Selection.ui \
    gui/forms/Form_Requirement.ui \
    gui/forms/Form_Test.ui \
    gui/forms/Form_Projects_Reports.ui \
    gui/forms/Form_Search_Project.ui \
    gui/forms/Form_Change_Password.ui \
    gui/forms/Form_Execution_Bugs.ui \
    gui/forms/Form_Data_Import.ui \
    gui/forms/Form_Project_Bugs.ui \
    gui/forms/Form_Data_Export.ui \
    gui/forms/Form_Manage_CustomFields.ui \
    gui/forms/Form_CustomFieldDesc.ui \
    gui/forms/Form_Projects_Compare.ui \
    gui/forms/Form_Feature.ui \
    gui/forms/Form_Need.ui \
    gui/forms/Form_Rule.ui \
    gui/forms/Form_Project.ui

INCLUDEPATH += ../client-lib \
    ../common \
    ../automation-lib \
    ../client-modules \
    ../client-entities
LIBS += -L../client-entities -lrtmr -lclient-entities
debug {
    LIBS += -L../client-lib/Debug \
        -L../automation-lib/Debug \
        -L../client-entities/Debug \
	-L../client-lib

    win32:PRE_TARGETDEPS += ../client-entities/Debug/client-entities.lib
    unix:PRE_TARGETDEPS += ../client-entities/libclient-entities.a

} else {
    LIBS += -L../client-lib/Release \
        -L../automation-lib/Release \
        -L../client-entities/Release \
	-L../client-lib

    win32:PRE_TARGETDEPS += ../client-entities/Release/client-entities.lib
    unix:PRE_TARGETDEPS += ../client-entities/libclient-entities.a

}
mac:LIBS += -L../client-lib
win32:contains(DEFINES, AUTOMATION_LIB) {
    LIBS += -lautomation-lib
    DEFINES += GUI_AUTOMATION_ACTIVATED
}
win32:LIBS += -lpsapi \
    -lUser32
#win32:DEFINES += PSAPI_VERSION=1
mac:LIBS += -framework \
    Carbon
unix:LIBS += -lproc
RESOURCES += resources.qrc
OBJECTS_DIR = build
MOC_DIR = build
OTHER_FILES += standard.css \
    resources.rc \
    rtmrapp.exe.manifest \
    abstractprojectwidget_stylesheet.css
