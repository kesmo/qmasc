#ifndef GUIAUTOMATIONPLAYBACKPROCESSOR_H
#define GUIAUTOMATIONPLAYBACKPROCESSOR_H

#include "AbstractAutomationProcessor.h"
#include "entities/automatedaction.h"
#include "entities/automatedactionvalidation.h"

typedef struct _log_message_list log_message_list;

class GuiAutomationPlaybackProcessor : public AbstractAutomationProcessor
{
    Q_OBJECT

public:
    GuiAutomationPlaybackProcessor(QObject* parent = 0);
    GuiAutomationPlaybackProcessor(QList<AutomatedAction*> in_automated_actions_list, const QString& in_command, const QStringList& in_command_parameters, QObject* parent = 0);

protected slots:
    void startPlaybackSystemEvents();
    void stopPlaybackSystemEvents();

private:
    QList<AutomatedAction*> m_automated_actions_list;
    bool m_is_automated_program_playbacking;

#ifdef GUI_AUTOMATION_ACTIVATED

    log_message_list* m_automation_playback_messages;

#endif


};

#endif // GUIAUTOMATIONPLAYBACKPROCESSOR_H
