#include "BatchAutomationProcessor.h"

BatchAutomationProcessor::BatchAutomationProcessor(QObject* parent) :
    AbstractAutomationProcessor(parent)
{
}


BatchAutomationProcessor::BatchAutomationProcessor(const QString &in_command, const QStringList &in_command_parameters, QObject* parent) :
    AbstractAutomationProcessor(in_command, in_command_parameters, parent)
{
}
