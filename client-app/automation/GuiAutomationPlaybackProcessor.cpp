#ifdef GUI_AUTOMATION_ACTIVATED
    #include "automation.h"
#endif

#include "session.h"
#include "GuiAutomationPlaybackProcessor.h"
#include "entities/automatedactionwindowhierarchy.h"

#ifdef GUI_AUTOMATION_ACTIVATED

static int freeWindowHierarchy(windows_hierarchy** in_window_hierarchy_list)
{
    if (in_window_hierarchy_list && (*in_window_hierarchy_list))
    {
        freeWindowHierarchy(&(*in_window_hierarchy_list)->m_child_window);
        free(*in_window_hierarchy_list);
        *in_window_hierarchy_list = NULL;
    }

    return NOERR;
}


static int freeMessagesList(log_message_list** in_messages_list)
{
    if (in_messages_list && (*in_messages_list))
    {
        freeMessagesList(&(*in_messages_list)->m_next_message);
        freeWindowHierarchy(&(*in_messages_list)->m_window_hierarchy);
        free(*in_messages_list);
        *in_messages_list = NULL;
    }
    return NOERR;
}


static int DefaultMessageCallback(HWND in_window_id, bool in_must_be_active_window, char* in_error_msg)
{
    HWND tmp_active_window = GetForegroundWindow();

    LOG_TRACE(Session::instance().getClientSession(), "%s : Active window id=%p, current window id=%p\n", __FUNCTION__, tmp_active_window, in_window_id);
    if (in_must_be_active_window && tmp_active_window != in_window_id)
    {
        WINDOWINFO tmp_window_info;
        CHAR tmp_window_class_name[1024];
        CHAR tmp_window_name[1024];
        CHAR tmp_cwindow_class_name[1024];
        CHAR tmp_cwindow_name[1024];

        if (GetWindowInfo(tmp_active_window, &tmp_window_info) == TRUE){
            GetClassNameA(tmp_active_window, tmp_window_class_name, 1024);
            GetWindowTextA(tmp_active_window, tmp_window_name, 1024);

            if (GetWindowInfo(in_window_id, &tmp_window_info) == TRUE){
                GetClassNameA(in_window_id, tmp_cwindow_class_name, 1024);
                GetWindowTextA(in_window_id, tmp_cwindow_name, 1024);

                sprintf_s(in_error_msg, LARGE_TEXT_SIZE, "DefaultMessageCallback : target window %p (class=%s name=%s) is not the active window %p (class=%s name=%s).\n",
                        in_window_id,
                        tmp_cwindow_class_name,
                        tmp_cwindow_name,
                        tmp_active_window,
                        tmp_window_class_name,
                        tmp_window_name
                        );
            }else{
                sprintf_s(in_error_msg, LARGE_TEXT_SIZE, "DefaultMessageCallback : target window %p (unknow class/name) is not the active window %p (class=%s name=%s).\n",
                        in_window_id,
                        tmp_active_window,
                        tmp_window_class_name,
                        tmp_window_name
                        );
            }
        }else{
            sprintf_s(in_error_msg, LARGE_TEXT_SIZE, "DefaultMessageCallback : target window %p (unknow class/name) is not the active window %p (unknow class/name).\n",
                    in_window_id,
                    tmp_active_window
                    );
        }

        return -1;
    }

    return NOERR;
}


static int RunMessageCallbacks(log_message_list* in_log_message, HWND in_window_id, char* in_error_msg)
{
    int tmp_return = NOERR;

    if (in_log_message && in_log_message->m_custom_data_ptr){
        AutomatedAction* tmp_automated_action = static_cast<AutomatedAction*>(in_log_message->m_custom_data_ptr);
        if (tmp_automated_action){
            const QList<AutomatedActionValidation*> &tmp_validations = tmp_automated_action->getAllAutomatedActionValidations();

            ClientModule        *tmp_module = NULL;
            AutomationModule    *tmp_automation_module = NULL;
            AutomationCallbackFunction *tmp_callback = NULL;
            AutomatedActionValidation* tmp_validation = NULL;
            QMap < QString, ClientModule*> tmp_modules_map = Session::instance().externalsModules().value(ClientModule::AutomationPlugin);

            windows_hierarchy* tmp_current_window = in_log_message->m_window_hierarchy;
            while(tmp_current_window && tmp_current_window->m_window_id != in_window_id){
                tmp_current_window = tmp_current_window->m_child_window;
            }

            if (!tmp_validations.isEmpty()){
                for(int tmp_index = 0;  tmp_index < tmp_validations.count() && tmp_return == NOERR; ++tmp_index){

                    tmp_validation = tmp_validations[tmp_index];
                    tmp_module = tmp_modules_map[tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME)];
                    if (tmp_module){
                        if (tmp_module->getModuleVersion() >= QString(tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION))){
                            tmp_automation_module = static_cast<AutomationModule*>(tmp_module);
                            if (tmp_automation_module){
                                QMap<QString, AutomationCallbackFunction*> tmp_module_functions_list = tmp_automation_module->getFunctionsMap();
                                tmp_callback = tmp_module_functions_list[tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME)];
                                if (tmp_callback && tmp_callback->getEntryPoint()){
                                    tmp_return = tmp_callback->getEntryPoint()((WId)in_window_id, tmp_current_window && tmp_current_window->m_is_active == TRUE, tmp_validation, in_error_msg, NULL);
                                }else{
                                    sprintf_s(in_error_msg, LARGE_TEXT_SIZE, "RunMessageCallbacks : The function %s of the module %s is not available.\n",
                                            tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME),
                                            tmp_module->getModuleName().toStdString().c_str());
                                    return -1;
                                }
                            }
                        }else{
                            sprintf_s(in_error_msg, LARGE_TEXT_SIZE, "RunMessageCallbacks : The version %s of the module %s is too old (must be >= %s).\n",
                                    tmp_module->getModuleVersion().toStdString().c_str(),
                                    tmp_module->getModuleName().toStdString().c_str(),
                                    tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION));
                            return -1;
                        }
                    }else{
                        sprintf_s(in_error_msg, LARGE_TEXT_SIZE, "RunMessageCallbacks : There is no module named %s.\n",
                                tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME));
                        return -1;
                    }
                }
            }else{
                return DefaultMessageCallback(in_window_id, tmp_current_window && tmp_current_window->m_is_active == TRUE, in_error_msg);
            }
        }
    }

    return tmp_return;
}


static int AutomationCallbackErrorFunction(const char* in_error_msg)
{
    qCritical() << Q_FUNC_INFO << QString::fromLocal8Bit(in_error_msg);

    return NOERR;
}
#endif

GuiAutomationPlaybackProcessor::GuiAutomationPlaybackProcessor(QObject* parent) :
    AbstractAutomationProcessor(parent),
    m_is_automated_program_playbacking(false)
#ifdef GUI_AUTOMATION_ACTIVATED
    , m_automation_playback_messages(NULL)
#endif
{
    connect(this, SIGNAL(processStart()), this, SLOT(startPlaybackSystemEvents()));
    connect(this, SIGNAL(processStop(int)), this, SLOT(stopPlaybackSystemEvents()));
}


GuiAutomationPlaybackProcessor::GuiAutomationPlaybackProcessor(QList<AutomatedAction*> in_automated_actions_list, const QString &in_command, const QStringList &in_command_parameters, QObject* parent) :
    AbstractAutomationProcessor(in_command, in_command_parameters, parent),
    m_automated_actions_list(in_automated_actions_list),
    m_is_automated_program_playbacking(false)
#ifdef GUI_AUTOMATION_ACTIVATED
    , m_automation_playback_messages(NULL)
#endif
{
    connect(this, SIGNAL(processStart()), this, SLOT(startPlaybackSystemEvents()));
    connect(this, SIGNAL(processStop(int)), this, SLOT(stopPlaybackSystemEvents()));
}


void GuiAutomationPlaybackProcessor::startPlaybackSystemEvents()
{
#ifdef GUI_AUTOMATION_ACTIVATED

    log_message_list* tmp_current_message = NULL;
    windows_hierarchy* tmp_window_hierarchy = NULL;
    windows_hierarchy* tmp_child_window_hierarchy = NULL;

    QString tmp_windows_id;
    QStringList tmp_windows_ids;
    int tmp_windows_count = 0;
    int tmp_window_index = 0;

    m_automation_playback_messages = (log_message_list*)malloc(sizeof(log_message_list));
    tmp_current_message = m_automation_playback_messages;
    memset(tmp_current_message, 0, sizeof(log_message_list));
    tmp_current_message->m_message_type = None;

    foreach(AutomatedAction* tmp_automated_action, m_automated_actions_list)
    {
        QStringList tmp_message_data(QString(tmp_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA)).split(AutomatedAction::FieldSeparator));

        // Liste des fenetres
        tmp_window_hierarchy = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
        memset(tmp_window_hierarchy, 0, sizeof(windows_hierarchy));
        tmp_child_window_hierarchy = tmp_window_hierarchy;

        tmp_windows_id = tmp_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_WINDOW_ID);
        tmp_windows_ids = tmp_windows_id.split(AutomatedAction::WindowSeparator, QString::SkipEmptyParts);
        tmp_windows_count = tmp_windows_ids.count();
        tmp_window_index = 0;

        foreach(QString tmp_window, tmp_windows_ids){
            QStringList tmp_window_info = tmp_window.split(AutomatedAction::FieldSeparator);
            if (tmp_window_info.count() == 9){
                tmp_child_window_hierarchy->m_window_atom = tmp_window_info[0].toInt();
                strcpy_s(tmp_child_window_hierarchy->m_window_class_name, 1024, tmp_window_info[1].toLocal8Bit().data());
                strcpy_s(tmp_child_window_hierarchy->m_window_name, 1024, tmp_window_info[2].toLocal8Bit().data());
                tmp_child_window_hierarchy->m_ctrl_id = tmp_window_info[3].toInt();
                tmp_child_window_hierarchy->m_is_active = tmp_window_info[4].toInt();
                tmp_child_window_hierarchy->m_window_rect.left = tmp_window_info[5].toInt();
                tmp_child_window_hierarchy->m_window_rect.top = tmp_window_info[6].toInt();
                tmp_child_window_hierarchy->m_window_rect.right = tmp_window_info[7].toInt();
                tmp_child_window_hierarchy->m_window_rect.bottom = tmp_window_info[8].toInt();

                if (tmp_window_index < tmp_windows_count - 1){
                    tmp_child_window_hierarchy->m_child_window = (windows_hierarchy*)malloc(sizeof(windows_hierarchy));
                    memset(tmp_child_window_hierarchy->m_child_window, 0, sizeof(windows_hierarchy));
                    tmp_child_window_hierarchy = tmp_child_window_hierarchy->m_child_window;
                }
            }

            ++tmp_window_index;
        }
        tmp_current_message->m_window_hierarchy = tmp_window_hierarchy;

        tmp_current_message->m_message_type = (EventMessageType)atoi(tmp_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE));
        if (tmp_message_data.count() == 3)
        {
            switch (tmp_current_message->m_message_type)
            {
            case Keyboard:
                tmp_current_message->m_message = tmp_message_data[0].toInt();
                tmp_current_message->m_keyboard_vk = tmp_message_data[1].toInt();
                tmp_current_message->m_keyboard_repeat = tmp_message_data[2].toInt();

                break;

            case Mouse:
                tmp_current_message->m_message = tmp_message_data[0].toInt();
                tmp_current_message->m_mouse_x = tmp_message_data[1].toInt();
                tmp_current_message->m_mouse_y = tmp_message_data[2].toInt();
                break;
            }
        }

        tmp_current_message->m_time = atol(tmp_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY));

        tmp_current_message->m_callback = &RunMessageCallbacks;
        tmp_current_message->m_custom_data_ptr = tmp_automated_action;

        tmp_current_message->m_next_message = (log_message_list*)malloc(sizeof(log_message_list));
        memset(tmp_current_message->m_next_message, 0, sizeof(log_message_list));
        tmp_current_message = tmp_current_message->m_next_message;
        tmp_current_message->m_message_type = None;
    }


    int tmp_result = automation_start_playback(Session::instance().getClientSession(), m_automation_playback_messages, m_process.pid()->dwProcessId, m_process.pid()->dwThreadId, &AutomationCallbackErrorFunction);
    if (tmp_result != NOERR)
    {
        emit processError(QString::fromLocal8Bit(Session::instance().getClientSession()->m_last_error_msg));
    }
    else
    {
        m_is_automated_program_playbacking = true;
    }


#endif

}


void GuiAutomationPlaybackProcessor::stopPlaybackSystemEvents()
{
#ifdef GUI_AUTOMATION_ACTIVATED
    if (m_is_automated_program_playbacking)
    {
        int tmp_result = automation_stop_playback(Session::instance().getClientSession());
        if (tmp_result != NOERR)
        {
            emit processError(QString::fromLocal8Bit(Session::instance().getClientSession()->m_last_error_msg));
        }


        freeMessagesList(&m_automation_playback_messages);
    }

    m_is_automated_program_playbacking = false;

#endif
}

