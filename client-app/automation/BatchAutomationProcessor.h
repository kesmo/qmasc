#ifndef BATCHAUTOMATIONPROCESSOR_H
#define BATCHAUTOMATIONPROCESSOR_H

#include "AbstractAutomationProcessor.h"

class BatchAutomationProcessor : public AbstractAutomationProcessor
{
public:
    BatchAutomationProcessor(QObject* parent = 0);
    BatchAutomationProcessor(const QString& in_command, const QStringList& in_command_parameters, QObject* parent = 0);
};

#endif // BATCHAUTOMATIONPROCESSOR_H
