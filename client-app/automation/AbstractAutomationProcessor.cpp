#include "AbstractAutomationProcessor.h"
#include <QFile>
#include <QMessageBox>

AbstractAutomationProcessor::AbstractAutomationProcessor(QObject* parent)
    : QObject(parent),
      m_return_code_parameter(NULL),
      m_output_string_parameter(NULL)
{
}


AbstractAutomationProcessor::AbstractAutomationProcessor(const QString &in_command, const QStringList &in_command_parameters, QObject* parent)
    : QObject(parent),
      m_return_code_parameter(NULL),
      m_output_string_parameter(NULL)
{
    m_command = in_command;
    m_parameters = in_command_parameters;
}

AbstractAutomationProcessor& AbstractAutomationProcessor::operator=(const AbstractAutomationProcessor& other)
{
    m_command = other.m_command;
    m_parameters = other.m_parameters;
    m_return_code_parameter = NULL;
    m_output_string_parameter = NULL;

    return *this;
}


void AbstractAutomationProcessor::launchProcess(Parameter* out_return_code, Parameter* out_output_string)
{
    m_return_code_parameter = out_return_code;
    m_output_string_parameter = out_output_string;

    if (m_command.isEmpty())
    {
        emit processError(tr("Le nom de l'executable est vide."));
    }
    else
    {
        if (!QFile::exists(m_command))
        {
            emit processError(tr("L'executable '%1' n'existe pas.").arg(m_command));
        }
        else
        {
            stopProcess();
            connect(&m_process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(manageError(QProcess::ProcessError)));
            connect(&m_process, SIGNAL(finished(int)), this, SLOT(stopProcess(int)));
            connect(&m_process, SIGNAL(readyReadStandardOutput()), this, SLOT(readBatchProcessStandardOutput()));
            connect(&m_process, SIGNAL(readyReadStandardError()), this, SLOT(readBatchProcessStandardError()));

            m_process.start(m_command, m_parameters);
            if(m_process.waitForStarted()){
                qDebug() << Q_FUNC_INFO << m_command << m_parameters;
                emit processStart();
            }
        }
    }
}



void AbstractAutomationProcessor::stopProcess(int in_return_code, bool notify)
{
    if (m_return_code_parameter){
        m_return_code_parameter->setValue(QString::number(in_return_code).toStdString().c_str());
    }

    if (m_process.state() == QProcess::Running)
        m_process.terminate();

    if (notify)
        emit processStop(in_return_code);

    m_process.disconnect();
}


void AbstractAutomationProcessor::manageError(QProcess::ProcessError)
{
    emit processError(tr("Le progamme '%1' n'a pû être démarré correctement : %2.").arg(m_command).arg(m_process.errorString()));
}


void AbstractAutomationProcessor::readBatchProcessStandardOutput()
{
    QByteArray  tmp_output = m_process.readAllStandardOutput();

    if (m_output_string_parameter){
        m_output_string_parameter->setValue(tmp_output.constData());
    }

    emit processStandardOutput(QString::fromLocal8Bit(tmp_output));
}



void AbstractAutomationProcessor::readBatchProcessStandardError()
{
    QByteArray  tmp_output = m_process.readAllStandardError();

    if (m_output_string_parameter){
        m_output_string_parameter->setValue(tmp_output.constData());
    }

    emit processStandardError(QString::fromLocal8Bit(tmp_output));
}
