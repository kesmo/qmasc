#ifndef GUIAUTOMATIONRECORDPROCESSOR_H
#define GUIAUTOMATIONRECORDPROCESSOR_H

#include "AbstractAutomationProcessor.h"
#include "entities/automatedaction.h"
#include "entities/automatedactionvalidation.h"

typedef struct _log_message_list log_message_list;

class GuiAutomationRecordProcessor : public AbstractAutomationProcessor
{
    Q_OBJECT

public:
    GuiAutomationRecordProcessor(QObject* parent = 0);
    GuiAutomationRecordProcessor(const QString& in_command, const QStringList& in_command_parameters, QObject* parent = 0);

protected slots:
    void startRecordSystemEvents();
    void stopRecordSystemEvents();

signals:
    void actionsRecorded(QList<AutomatedAction*>);

private:
    bool m_is_automated_program_recording;

};

#endif // GUIAUTOMATIONRECORDPROCESSOR_H
