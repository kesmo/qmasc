#ifndef ABSTRACTAUTOMATIONPROCESSOR_H
#define ABSTRACTAUTOMATIONPROCESSOR_H

#include "parameter.h"

#include <QProcess>

class AbstractAutomationProcessor : public QObject
{
    Q_OBJECT

public:
    AbstractAutomationProcessor(QObject* parent = 0);
    AbstractAutomationProcessor(const QString& in_command, const QStringList& in_command_parameters, QObject *parent = 0);

    AbstractAutomationProcessor& operator=(const AbstractAutomationProcessor& other);

    virtual void launchProcess(Parameter* out_return_code = NULL, Parameter* out_output_string = NULL);

    const QProcess& getProcess() const {return m_process;}

signals:
    void processStart();
    void processStop(int);
    void processError(const QString&);

    void processStandardOutput(const QString&);
    void processStandardError(const QString&);

public Q_SLOTS:
    void readBatchProcessStandardOutput();
    void readBatchProcessStandardError();
    void manageError(QProcess::ProcessError);
    virtual void stopProcess(int in_return_code = 0, bool notify = true);

protected:
    QProcess    m_process;
    QString     m_command;
    QStringList m_parameters;

    Parameter*  m_return_code_parameter;
    Parameter*  m_output_string_parameter;
};

#endif // ABSTRACTAUTOMATIONPROCESSOR_H
