#ifdef GUI_AUTOMATION_ACTIVATED
    #include "automation.h"
#endif

#include "session.h"
#include "GuiAutomationRecordProcessor.h"
#include "entities/automatedactionwindowhierarchy.h"

#ifdef GUI_AUTOMATION_ACTIVATED

static int AutomationCallbackErrorFunction(const char* in_error_msg)
{
    qCritical() << Q_FUNC_INFO << QString::fromLocal8Bit(in_error_msg);

    return NOERR;
}
#endif

GuiAutomationRecordProcessor::GuiAutomationRecordProcessor(QObject* parent) :
    AbstractAutomationProcessor(parent),
    m_is_automated_program_recording(false)
{
    connect(this, SIGNAL(processStart()), this, SLOT(startRecordSystemEvents()));
    connect(this, SIGNAL(processStop(int)), this, SLOT(stopRecordSystemEvents()));
}


GuiAutomationRecordProcessor::GuiAutomationRecordProcessor(const QString &in_command, const QStringList &in_command_parameters, QObject* parent) :
    AbstractAutomationProcessor(in_command, in_command_parameters, parent),
    m_is_automated_program_recording(false)
{
    connect(this, SIGNAL(processStart()), this, SLOT(startRecordSystemEvents()));
    connect(this, SIGNAL(processStop(int)), this, SLOT(stopRecordSystemEvents()));
}


void GuiAutomationRecordProcessor::startRecordSystemEvents()
{
    qDebug() << Q_FUNC_INFO;
#ifdef GUI_AUTOMATION_ACTIVATED

    int tmp_result = automation_start_record(Session::instance().getClientSession(), m_process.pid()->dwProcessId, m_process.pid()->dwThreadId, &AutomationCallbackErrorFunction, false);
    if (tmp_result != NOERR)
    {
        emit processError(QString::fromLocal8Bit(Session::instance().getClientSession()->m_last_error_msg));
    }
    else
    {
        m_is_automated_program_recording = true;
    }

#endif
}



void GuiAutomationRecordProcessor::stopRecordSystemEvents()
{
#ifdef GUI_AUTOMATION_ACTIVATED

    if (m_is_automated_program_recording)
    {
        log_message_list* tmp_messages = automation_stop_record(Session::instance().getClientSession());
        log_message_list* tmp_current_message = tmp_messages;
        int tmp_actions_count = 0;
        AutomatedAction* tmp_automated_action = NULL;
        QString tmp_windows_id;
        windows_hierarchy* tmp_window = NULL;
        QList<AutomatedAction*> tmp_actions;

        while (tmp_current_message != NULL)
        {
            tmp_automated_action = new AutomatedAction();
            tmp_automated_action->setValueForKey(QString::number(tmp_current_message->m_message_type).toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE);
            tmp_automated_action->setValueForKey(QString::number(tmp_current_message->m_time).toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY);

            if (tmp_current_message->m_window_hierarchy)
            {
                tmp_automated_action->setAutomatedActionWindowHierarchys(AutomatedActionWindowHierarchy::fromWindowHierarchyStructure(tmp_current_message->m_window_hierarchy));

                tmp_window = tmp_current_message->m_window_hierarchy;
                tmp_windows_id = "";
                while(tmp_window){
                    tmp_windows_id += QString("%1%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16%17%18")
                            .arg(tmp_window->m_window_atom)
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(QString::fromLocal8Bit(tmp_window->m_window_class_name))
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(QString::fromLocal8Bit(tmp_window->m_window_name))
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(tmp_window->m_ctrl_id)
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(tmp_window->m_is_active)
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(tmp_window->m_window_rect.left)
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(tmp_window->m_window_rect.top)
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(tmp_window->m_window_rect.right)
                            .arg(AutomatedAction::FieldSeparator)
                            .arg(tmp_window->m_window_rect.bottom)
                            .arg(AutomatedAction::WindowSeparator);

                    tmp_window = tmp_window->m_child_window;
                }
                tmp_automated_action->setValueForKey(tmp_windows_id.toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_WINDOW_ID);


                switch(tmp_current_message->m_message_type)
                {
                case Keyboard:
                    tmp_automated_action->setValueForKey(QString("%1%2%3%4%5")
                                                         .arg(tmp_current_message->m_message)
                                                         .arg(AutomatedAction::FieldSeparator)
                                                         .arg(tmp_current_message->m_keyboard_vk)
                                                         .arg(AutomatedAction::FieldSeparator)
                                                         .arg(tmp_current_message->m_keyboard_repeat).toStdString().c_str()
                                                         , AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
                    break;

                case Mouse:
                    tmp_automated_action->setValueForKey(QString("%1%2%3%4%5")
                                                         .arg(tmp_current_message->m_message)
                                                         .arg(AutomatedAction::FieldSeparator)
                                                         .arg(tmp_current_message->m_mouse_x)
                                                         .arg(AutomatedAction::FieldSeparator)
                                                         .arg(tmp_current_message->m_mouse_y).toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
                    break;
                }

            }
            tmp_actions.append(tmp_automated_action);

            tmp_current_message = tmp_current_message->m_next_message;
            ++tmp_actions_count;
        }

        free_messages_list(&tmp_messages);

        emit actionsRecorded(tmp_actions);
    }

    m_is_automated_program_recording = false;


#endif

}
