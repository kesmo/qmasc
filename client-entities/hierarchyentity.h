#ifndef HIERARCHYENTITY_H
#define HIERARCHYENTITY_H

#include "entities/entity.h"

namespace Entities
{

template <class E, const entity_def *D, const char *PreviousColumn, const char *ParentColumn>
class HierarchyEntity:
    public Entity<E, D>
{
public:
    HierarchyEntity() : Entity<E, D>() {}

    ADD_NAMED_RELATION(Child, E, E, ParentColumn, PreviousColumn)

    const char *columnNameForPreviousItem() const
    {
        return PreviousColumn;
    }

    const char *columnNameForParentItem() const
    {
        return ParentColumn;
    }

    void setParent(E* parent)
    {
        return ChildRelation::instance().setParent(parent, dynamic_cast<E*>(this));
    }

    E* getParent()
    {
        return ChildRelation::instance().getParent(dynamic_cast<E*>(this));
    }

    static bool isChildOf(E *in_child, E *in_item)
    {
        E *tmp_parent = ChildRelation::instance().getParent(in_child);

        if (tmp_parent == NULL)
            return false;

        if (tmp_parent == in_item)
            return true;

        return isChildOf(in_child, tmp_parent);
    }


    static bool isParentOf(E *in_parent, E *in_item)
    {
        foreach(E *tmp_child, ChildRelation::instance().getChilds(in_parent))
        {
            if (in_item == tmp_child)
                return true;

            if (isParentOf(in_parent, tmp_child))
                return true;
        }

        return false;
    }

    static QList<E*> parentRecordsFromRecordsList(const QList<E*>& in_records_list)
    {
        QList<E*>   tmp_records_list;
        bool        tmp_indic = false;
        QList<E*>   tmp_remove_records_list;

        foreach(E* tmp_record, in_records_list)
        {
            tmp_indic = false;

            foreach(E* tmp_current_record, tmp_records_list)
            {
                if (isChildOf(tmp_record, tmp_current_record)) {
                    tmp_indic = true;
                    break;
                }

                if (isParentOf(tmp_record, tmp_current_record)) {
                    tmp_remove_records_list.append(tmp_current_record);
                }
            }

            if (!tmp_indic) {
                tmp_records_list.append(tmp_record);
            }
        }

        foreach(E* tmp_record, tmp_records_list)
        {
            if (tmp_remove_records_list.indexOf(tmp_record) >= 0) {
                tmp_records_list.removeAll(tmp_record);
            }
        }

        return tmp_records_list;
    }
};

}

#endif // HIERARCHYENTITY_H
