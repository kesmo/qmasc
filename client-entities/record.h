/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef RECORD_H
#define RECORD_H

#include <QVariant>
#include <QTextStream>
#include <QTextCodec>
#include <QDebug>

#include <string.h>
#if defined (__APPLE__)
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <stdlib.h>
#include "netcommon.h"
#include "client.h"
#include "utilities.h"
#include "entities.h"
#include "session.h"
#include "abstractrecord.h"

#define DEFAULT_RECORD_MIME_TYPE "application/qmasc-records"

class RecordListener;


class Record :
        public AbstractRecord
{
    private:
        /* Tableau des champs */
        const entity_def* m_entity_def;
        char    **m_columns_record;
        char    **m_originals_columns_record;

    protected:

        int     m_record_status;

        Record(char **in_columns_record);

        int compareField(Record *in_record, const char *in_field_name) const;

    public:

        enum SearchComparison
        {
            EqualTo = 0,
            LowerThan,
            UpperThan,
            LowerOrEqualTo,
            UpperOrEqualTo
        };

        Record(const entity_def* in_entity_def);

        virtual ~Record();

        virtual void destroy();

        const char* getIdentifier() const;

        int setValueForKey(const char* in_new_value, const char* in_key);
        void setValues(char** in_new_values);

        int setValueForKeyAtIndex(const char* in_new_value, unsigned int in_key_index);

        const char* getValueForKey(const char* in_key) const;

        const char* getOriginalValueForKey(const char* in_key) const;

        const char* getValueForKeyAtIndex(unsigned int in_key_index) const;

        const entity_def* getEntityDef() const;

        int getEntityDefSignatureId() const;

        int loadRecord(const char *in_primary_key_value);
        int loadRecordFromWhereClause(const char* in_where_clause);

        int deleteRecord();
        static int deleteRecords(int in_signature_id, char* in_where_clause);

        virtual void applyChanges();
        virtual void revertChanges();

        virtual int saveRecord(Record *in_original_record);
        virtual int saveRecord();

        int insertRecord();

        template < class T >
        static const char* matchingValueInRecordsList(const QList<T*>& in_records, const char *in_discriminant_key, const char *in_discriminant_value, const char *in_search_key)
        {
            const char        *tmp_discrimant_key_value = NULL;

            foreach (T* tmp_record, in_records)
            {
                tmp_discrimant_key_value = tmp_record->getValueForKey(in_discriminant_key);
                if (compare_values(tmp_discrimant_key_value, in_discriminant_value) == 0)
                {
                    return tmp_record->getValueForKey(in_search_key);
                }
            }

            return NULL;
        }

        template < class T >
        static int indexOfMatchingValueInRecordsList(const QList<T*>& in_records, const char *in_discriminant_key, const char *in_discriminant_value)
        {
            const char        *tmp_discrimant_key_value = NULL;
            int     tmp_index = 0;

            foreach (T* tmp_record, in_records)
            {
                tmp_discrimant_key_value = tmp_record->getValueForKey(in_discriminant_key);
                if (compare_values(tmp_discrimant_key_value, in_discriminant_value) == 0)
                {
                    return tmp_index;
                }
                tmp_index++;
            }

            return -1;
        }



        /**
     * Charger plusieurs enregistrements de la base de donnees
     * @param in_record_class
     * @param in_signature
     * @param in_where_clause
     * @return
     */
        template < class T >
        static QList<T*> loadRecords(net_session *in_session, const entity_def *in_entity_def, const char* in_where_clause, const char* in_order_by_clause, net_callback_fct *in_callback = NULL)
        {
            char***         tmp_rows = NULL;
            T*             tmp_record = NULL;
            QList<T*> result;

            unsigned long   tmp_records_count = 0;


            tmp_rows = cl_load_records(in_session, in_entity_def->m_entity_signature_id , in_where_clause, in_order_by_clause, &tmp_records_count, in_callback);
            if (tmp_rows != NULL)
            {
                for (unsigned long tmp_index = 0; tmp_index < tmp_records_count; tmp_index++)
                {
                    tmp_record = new T;
                    for (unsigned long tmp_column_index = 0; tmp_column_index < in_entity_def->m_entity_columns_count; tmp_column_index++)
                    {
                        tmp_record->m_columns_record[tmp_column_index] = tmp_rows[tmp_index][tmp_column_index];
                    }
                    tmp_record->applyChanges();
                    result.append(tmp_record);
                }

                cl_free_rows_columns_array(&tmp_rows, tmp_records_count, 0);
            }

            return result;
        }

        template < class T >
        static T** freeRecords(T** in_records, unsigned long in_records_count)
        {
            if (in_records != NULL)
            {
                for (unsigned long tmp_index = 0; tmp_index < in_records_count; tmp_index++)
                {
                    if (in_records[tmp_index] != NULL)
                        delete in_records[tmp_index];
                }
                delete in_records;
            }

            return NULL;
        }


        int compareTo(Record *in_record) const;

        int compareTo(Record *in_record, const char *in_column_name) const;

        template<class T>
        static QList < T* > orderedRecords(const QList < T* > &in_records, const char *in_previous_key)
        {
            QList < T* >        tmp_records_list;

            const char                  *tmp_previous_id = NULL;
            const char                  *tmp_last_id = NULL;


            do {
                tmp_last_id = tmp_previous_id;
                foreach (T* tmp_record, in_records) {
                    if (compare_values(tmp_previous_id, tmp_record->getValueForKey(in_previous_key)) == 0)
                    {
                        tmp_records_list.append(tmp_record);
                        tmp_previous_id = tmp_record->getIdentifier();

                        break;
                    }
                }
            }
            while (is_empty_string(tmp_previous_id) == FALSE && tmp_last_id != tmp_previous_id);

            foreach(T* tmp_record, in_records) {
                if (tmp_records_list.indexOf(tmp_record) < 0) {
                    tmp_records_list.append(tmp_record);
                }
            }

            return tmp_records_list;
        }


        template<class T>
        static QList<T*> readDataFromDevice(QTextStream& in_text_stream, QList<char>& in_fields_sep, QList<char>& in_records_sep, QList<const char*>& in_field_names)
        {
            QChar     tmp_char;
            char     tmp_text_separator = '\"';

            int tmp_field_char_index = 0;
            int tmp_record_char_index = 0;

            int     tmp_column_number = 0;
            int     tmp_row_number = 0;

            QByteArray     tmp_str;
            QByteArray     tmp_field_sep_str;
            QByteArray     tmp_record_sep_str;

            bool     tmp_first_char_of_field = true;
            bool     tmp_first_char_is_quote = false;
            bool     tmp_previous_char_is_quote = false;

            QList<T*>     tmp_records_list;
            T     *tmp_record = NULL;


            if (!in_text_stream.atEnd())
                in_text_stream >> tmp_char;

            while (!in_text_stream.atEnd())
            {
                // Gestion des double-quotes
                if (tmp_char==tmp_text_separator)
                {
                    if (tmp_first_char_of_field)
                    {
                        tmp_first_char_is_quote = true;
                    }
                    else
                    {
                        if (tmp_previous_char_is_quote)
                        {
                            tmp_str += tmp_char;
                            tmp_previous_char_is_quote = false;
                        }
                        else
                            tmp_previous_char_is_quote = true;
                    }

                    tmp_first_char_of_field = false;
                    if (!in_text_stream.atEnd())
                        in_text_stream >> tmp_char;
                }
                // Separateur de champs
                else if (tmp_char == in_fields_sep[0])
                {
                    tmp_first_char_of_field = false;
                    tmp_field_sep_str = QByteArray();

                    while (!in_text_stream.atEnd() && tmp_field_char_index < in_fields_sep.length() && tmp_char == in_fields_sep[tmp_field_char_index])
                    {
                        tmp_field_sep_str += tmp_char;
                        tmp_field_char_index++;
                        if (!in_text_stream.atEnd())
                            in_text_stream >> tmp_char;
                    }

                    if (tmp_field_char_index < in_fields_sep.length())
                    {
                        tmp_str += tmp_field_sep_str;
                    }
                    // Enregistrement du champs
                    else
                    {
                        if (!tmp_first_char_is_quote || (tmp_first_char_is_quote && tmp_previous_char_is_quote))
                        {
                            if (tmp_column_number == 0)
                            {
                                tmp_record = new T();
                                tmp_records_list.append(tmp_record);
                            }

                            if (tmp_column_number < in_field_names.length())
                            {
                                tmp_record->setValueForKey(tmp_str, in_field_names[tmp_column_number]);
                            }

                            tmp_column_number++;

                            tmp_str.clear();

                            tmp_first_char_is_quote = false;
                            tmp_first_char_of_field = true;
                        }
                        else
                        {
                            tmp_str += tmp_field_sep_str;
                        }
                    }

                    tmp_previous_char_is_quote = false;
                    tmp_field_char_index = 0;
                }
                // Separateur d'enregistrements
                else if (tmp_char == in_records_sep[0])
                {
                    tmp_first_char_of_field = false;
                    tmp_record_sep_str = QByteArray();

                    while (!in_text_stream.atEnd() && tmp_record_char_index < in_records_sep.length() && tmp_char == in_records_sep[tmp_record_char_index])
                    {
                        tmp_record_sep_str += tmp_char;
                        tmp_record_char_index++;
                        if (!in_text_stream.atEnd())
                            in_text_stream >> tmp_char;
                    }

                    if (tmp_record_char_index < in_records_sep.length())
                    {
                        tmp_str += tmp_record_sep_str;
                    }
                    // Enregistrement du champs
                    else
                    {
                        if (!tmp_first_char_is_quote || (tmp_first_char_is_quote && tmp_previous_char_is_quote))
                        {
                            if (tmp_column_number < in_field_names.length())
                            {
                                if (tmp_record == NULL)
                                {
                                    tmp_record = new T();
                                    tmp_records_list.append(tmp_record);
                                }
                                tmp_record->setValueForKey(tmp_str, in_field_names[tmp_column_number]);
                            }
                            tmp_row_number++;

                            tmp_column_number = 0;
                            tmp_str.clear();

                            tmp_first_char_is_quote = false;
                            tmp_first_char_of_field = true;
                        }
                        else
                        {
                            tmp_str += tmp_record_sep_str;
                        }
                    }

                    tmp_previous_char_is_quote = false;
                    tmp_record_char_index = 0;
                }
                else
                {
                    tmp_previous_char_is_quote = false;
                    tmp_first_char_of_field = false;
                    tmp_str += tmp_char;
                    if (!in_text_stream.atEnd())
                        in_text_stream >> tmp_char;
                }
            }

            if (tmp_str.isEmpty() == false)
            {
                if (tmp_column_number < in_field_names.length())
                {
                    if (tmp_record == NULL)
                    {
                        tmp_record = new T();
                        tmp_records_list.append(tmp_record);
                    }

                    tmp_record->setValueForKey(tmp_str, in_field_names[tmp_column_number]);
                }
            }

            return tmp_records_list;
        }

        template < class T >
        static int saveOrderedRecordList(const QList < T * >& in_records_list, const char *in_previous_key)
        {
            int     tmp_result = NOERR;
            T       *tmp_previous_record = NULL;
            T       *tmp_record = NULL;

            for (int tmp_index = 0; tmp_index < in_records_list.count(); tmp_index++)
            {
                tmp_record = in_records_list[tmp_index];
                if (tmp_record != NULL)
                {
                    if (is_empty_string(in_previous_key) == FALSE)
                    {
                        if (tmp_previous_record != NULL)
                            tmp_record->setValueForKey(tmp_previous_record->getIdentifier(), in_previous_key);
                        else
                            tmp_record->setValueForKey(NULL, in_previous_key);
                    }

                    tmp_result = tmp_record->saveRecord();
                    if (tmp_result != NOERR)
                        break;

                    tmp_previous_record = tmp_record;
                }
            }

            return tmp_result;
        }


        int lockRecordStatus() const;

        int setRecordStatus(int in_record_status);

        int lockRecord(bool in_check_sync);

        int unlockRecord();

        QString serialize() const;

        void deserialize(const QString & in_serialized_record);

        virtual bool hasChangedValues();

        int getMaxValueLengthForKey(const char* in_key) const;

};



template <class T> inline QList<Record*> castToRecordsList(const QList<T*>& in_records_list)
{
    QList<Record*>   tmp_list;
    Record*          tmp_cast_record = NULL;

    foreach (T *tmp_record, in_records_list)
    {
        tmp_cast_record = dynamic_cast<Record*>(tmp_record);
        if (tmp_cast_record != NULL)
            tmp_list << tmp_cast_record;
    }

    return tmp_list;
}

#endif // RECORD_H
