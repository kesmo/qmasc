/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef SESSION_H
#define SESSION_H

#include "netcommon.h"
#include "client.h"
#include "singleton.h"
#include <clientmodule.h>

#include <QTranslator>
#include <QCoreApplication>

#define TR_SERVER_MESSAGES_CONTEXT "Server_Messages"
#define TR_CUSTOM_MESSAGES_CONTEXT "Custom_Messages"

#define TR_SERVER_MESSAGE(message)  QCoreApplication::translate(TR_SERVER_MESSAGES_CONTEXT, message)

#define TR_CUSTOM_MESSAGE(message)  QCoreApplication::translate(TR_CUSTOM_MESSAGES_CONTEXT, message)

class Record;

namespace Entities
{
class User;
class RequirementCategory;
class ActionResult;
class TestResult;
class Status;
class TestType;
class CustomFieldDesc;
}

using namespace Entities;

class Session :
    public Singleton<Session>
{
public:

    Session();
    ~Session();
    void initialize();

    int connect(const QString &server, int port, const QString &username, const QString &password, bool debugMode, const QString &logFile, int logLevel);
    void disconnect();

    void loadLanguage(const QString& in_language);

    bool addExternalModule(ClientModule *in_external_module);
    const QMap<ClientModule::ModuleType, QMap< QString, ClientModule* > > & externalsModules() const { return m_externals_modules; }

    void loadCustomFields();

    User* user() const;
    const QList<RequirementCategory*> & requirementsCategories() const;
    const QList<TestResult*> &  testsResults() const;
    const QList<ActionResult*> &  actionsResults() const;
    const QList<Status*> &  requirementsStatus() const;
    const QList<TestType*> &  testsTypes() const;
    const QList<CustomFieldDesc*> &  customTestsFieldsDesc() const;
    const QList<CustomFieldDesc*> &  customRequirementsFieldsDesc() const;
    const QList<QString> &  currentUserRoles() const;

    void setLastErrorMessage(const char* in_error_mess);
    void setLastErrorMessage(const int in_error_id);
    QString getLastErrorMessage() const;
    QString getErrorMessage(const int in_error_id) const;
    net_session* getClientSession() const;
    const QString& getLangage() const;
    const QString& getApplicationVersion() const;

    QMap<QString, QPair<QString, QString> > getBugtrackersCredentials() const;
    void setBugtrackerCredential(QString in_module_name, QPair<QString, QString> in_login_password);

    AutomationCallbackFunction* getAutomationCallbackFunction(const QString &moduleName, const QString &moduleVersion, const QString &functionName);

    void setDefaultProjectVersionId(const QString &id);
    QString getDefaultProjectVersionId() const;

private:

    QMap<ClientModule::ModuleType, QMap< QString, ClientModule* > >      m_externals_modules;

    net_session                  *m_session;

    User                     *m_user;
    QList<RequirementCategory*>                 m_requirements_categories_list;
    QList<TestResult*>                 m_tests_results_list;
    QList<ActionResult*>                 m_actions_results_list;
    QList<Status*>                 m_requirements_status_list;
    QList<TestType*>                 m_tests_types_list;
    QMap<QString, QPair<QString, QString> > m_bugtrackers_credentials;
    QList<CustomFieldDesc*> m_custom_tests_fields_desc;
    QList<CustomFieldDesc*> m_custom_requirements_fields_desc;
    QList<QString>                     m_user_roles;

    /* Traduction */
    QString         m_lang;
    QTranslator     m_qt_translator;
    QTranslator     m_client_translator;
    QTranslator     m_server_translator;

    QString m_app_version;

    QString m_default_project_version_id;

    void readUserSettings();
    int loadUserInfos(const char* in_username);
    void unloadUserInfos();

};

#endif // SESSION_H
