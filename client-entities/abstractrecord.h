#ifndef ABSTRACTRECORD_H
#define ABSTRACTRECORD_H

#include <QList>

class RecordListener;

class AbstractRecord {

private:

    bool m_is_removing;

    QList<RecordListener*>  m_record_listeners_list;

protected:
    void aboutToBeDestroyed();

public:
    AbstractRecord();
    virtual ~AbstractRecord();

    void addRecordListener(RecordListener *recordListener);
    void removeRecordListener(RecordListener *recordListener);

    virtual int saveRecord();
    virtual int deleteRecord();

    virtual bool isModifiable();

    bool isRemoving() const;
};

#endif // ABSTRACTRECORD_H
