/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "session.h"
#include "entities/user.h"
#include "entities/requirementcontent.h"
#include "entities/requirementcategory.h"
#include "entities/executionaction.h"
#include "entities/executiontest.h"
#include "entities/customfielddesc.h"
#include "entities/customfielddesc.h"
#include "entities/projectversion.h"
#include "entities/actionresult.h"
#include "entities/testresult.h"
#include "entities/testtype.h"
#include "entities/status.h"
#include "entitiesmodel.h"

#include <QSettings>

Session::Session()
{
    m_user = NULL;
    m_session = NULL;

    Relations::EntitiesModel::instance().initialize();
}


Session::~Session()
{
    unloadUserInfos();
    cl_disconnect(&m_session);
}


void Session::initialize()
{
#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QTextCodec::setCodecForLocale(ForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
#endif

    qApp->setApplicationVersion(APP_VERSION);

    readUserSettings();
}

int Session::connect(const QString &server, int port, const QString &username, const QString &password, bool debugMode, const QString &logFile, int logLevel)
{
    int tmp_return_result = NOERR;

    QSettings tmp_settings;

    tmp_return_result = cl_connect(
        &m_session,
        server.toStdString().c_str(),
        port, username.toStdString().c_str(),
        password.toStdString().c_str(),
        debugMode,
        logFile.toStdString().c_str(),
        logLevel, PROTOCOL_VERSION,
        tmp_settings.value("server_recv_timeout", 30).toInt(),
        tmp_settings.value("server_send_timeout", 30).toInt());

    if (tmp_return_result == NOERR) {
        QList< QMap < QString, ClientModule*> > tmp_modules_list = externalsModules().values();
        QMap < QString, ClientModule*> tmp_modules_map;

        foreach(tmp_modules_map, tmp_modules_list) {
            foreach(ClientModule *tmp_module, tmp_modules_map) {
                tmp_module->initModuleFromSession(m_session);
            }
        }

        tmp_return_result = loadUserInfos(username.toStdString().c_str());
        if (tmp_return_result == NOERR) {
            return tmp_return_result;
        }
    }

    disconnect();
    return tmp_return_result;
}

void Session::disconnect()
{
    cl_disconnect(&m_session);
    unloadUserInfos();

    Relations::EntitiesModel::instance().logAvailableEntities();
}

void Session::readUserSettings()
{
    QSettings tmp_settings;

    /* Couleurs */
    QColor tmp_test_execution_ok_color = tmp_settings.value("test_execution_ok_color", QVariant(QColor(Qt::darkGreen))).value<QColor>();
    QColor tmp_test_execution_ko_color = tmp_settings.value("test_execution_ko_color", QVariant(QColor(Qt::red))).value<QColor>();
    QColor tmp_test_execution_incompleted_color = tmp_settings.value("test_execution_incompleted_color", QVariant(QColor(Qt::blue))).value<QColor>();
    QColor tmp_test_execution_by_passed_color = tmp_settings.value("test_execution_by_passed_color", QVariant(QColor(Qt::darkGray))).value<QColor>();

    ExecutionTest::initDefaultColors(
        tmp_test_execution_ok_color,
        tmp_test_execution_ko_color,
        tmp_test_execution_incompleted_color,
        tmp_test_execution_by_passed_color);

    /* Bugtrackers */
    foreach(ClientModule *tmp_module, externalsModules().value(ClientModule::BugtrackerPlugin).values())
    {
        QString tmp_module_name = tmp_module->getModuleName();
        QString tmp_setting_prefix = tmp_module_name.toLower().trimmed();
        std::string tmp_std_string = tmp_settings.value(tmp_setting_prefix + "_password", "").toString().toStdString();
        const char* tmp_crypted_pwd = decrypt_str(tmp_std_string.c_str());

        m_bugtrackers_credentials[tmp_module_name] = QPair<QString, QString>(tmp_settings.value(tmp_setting_prefix + "_login", "").toString(),
                                                                             tmp_crypted_pwd);

        delete tmp_crypted_pwd;
    }

}


/**
  Charger l'utilisateur connecte
**/
int Session::loadUserInfos(const char* in_username)
{
    int                         tmp_result = NOERR;
    QList<User*>                 tmp_users;

    unsigned long           tmp_user_roles_count = 0;
    char                    ***tmp_user_roles = NULL;

    unloadUserInfos();

    net_session_print_where_clause(m_session, "%s='%s'", USERS_TABLE_USERNAME, in_username);

    tmp_users = User::loadRecordsList(m_session->m_where_clause_buffer);
    if (tmp_users.size() == 1) {
        m_user = tmp_users.first();
    }
    else {
        qDeleteAll(tmp_users);
        m_user = NULL;
        return DB_UNKNOW_USER_ERROR;
    }

    m_requirements_categories_list = RequirementCategory::loadRecordsList();

    m_tests_results_list = TestResult::loadRecordsList();

    m_actions_results_list = ActionResult::loadRecordsList();

    m_requirements_status_list = Status::loadRecordsList();

    m_tests_types_list = TestType::loadRecordsList();

    loadCustomFields();

    // Roles utilisateur
    tmp_user_roles = cl_get_user_roles(m_session, in_username, &tmp_user_roles_count);
    if (tmp_user_roles != NULL) {
        for (unsigned long tmp_index = 0; tmp_index < tmp_user_roles_count; ++tmp_index) {
            m_user_roles.append(tmp_user_roles[tmp_index][0]);
        }
        cl_free_rows_columns_array(&tmp_user_roles, tmp_user_roles_count, 1);
    }

    return tmp_result;
}

void Session::loadCustomFields()
{
    net_session_print_where_clause(m_session, "%s='%s'", CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ENTITY, CUSTOM_FIELDS_TEST);
    m_custom_tests_fields_desc = CustomFieldDesc::loadRecordsList(m_session->m_where_clause_buffer);

    net_session_print_where_clause(m_session, "%s='%s'", CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ENTITY, CUSTOM_FIELDS_REQUIERMENT);
    m_custom_requirements_fields_desc = CustomFieldDesc::loadRecordsList(m_session->m_where_clause_buffer);
}


/**
  Renvoie l'utilisateur connecte
**/
User* Session::user() const
{
    return m_user;
}

void Session::unloadUserInfos()
{
    delete m_user;
    m_user = NULL;

    qDeleteAll(m_requirements_categories_list);
    m_requirements_categories_list.clear();

    qDeleteAll(m_tests_results_list);
    m_tests_results_list.clear();

    qDeleteAll(m_actions_results_list);
    m_actions_results_list.clear();

    qDeleteAll(m_requirements_status_list);
    m_requirements_status_list.clear();

    qDeleteAll(m_tests_types_list);
    m_tests_types_list.clear();

    qDeleteAll(m_custom_tests_fields_desc);
    m_custom_tests_fields_desc.clear();

    qDeleteAll(m_custom_requirements_fields_desc);
    m_custom_requirements_fields_desc.clear();

    m_user_roles.clear();
}



bool Session::addExternalModule(ClientModule *in_external_module)
{
    if (in_external_module != NULL) {
        if (!m_externals_modules[in_external_module->getModuleType()].contains(in_external_module->getModuleName())) {
            m_externals_modules[in_external_module->getModuleType()].insert(in_external_module->getModuleName(), in_external_module);
            return true;
        }
    }

    return false;
}




void Session::loadLanguage(const QString& in_language)
{

    if (m_lang != in_language) {
        QSettings settings;

        settings.setValue("prefered_language", in_language);

        m_lang = in_language;

        QLocale locale = QLocale(m_lang);
        QLocale::setDefault(locale);

        qApp->removeTranslator(&m_qt_translator);
        if (m_qt_translator.load(":/languages/qt_" + in_language))
            qApp->installTranslator(&m_qt_translator);

        qApp->removeTranslator(&m_server_translator);
        if (m_server_translator.load(QString(":/languages/server_") + in_language))
            qApp->installTranslator(&m_server_translator);
        else if (in_language != "fr" && m_server_translator.load(QString(":/languages/server_en")))
            qApp->installTranslator(&m_server_translator);

        qApp->removeTranslator(&m_client_translator);
        if (m_client_translator.load(QString(":/languages/client_") + in_language))
            qApp->installTranslator(&m_client_translator);
        else if (in_language != "fr" && m_client_translator.load(QString(":/languages/client_en")))
            qApp->installTranslator(&m_client_translator);
    }
}

AutomationCallbackFunction* Session::getAutomationCallbackFunction(const QString &moduleName, const QString &moduleVersion, const QString &functionName)
{
    ClientModule        *tmp_module = NULL;
    AutomationModule    *tmp_automation_module = NULL;
    QMap < QString, ClientModule*> tmp_modules_map = externalsModules().value(ClientModule::AutomationPlugin);

    tmp_module = tmp_modules_map[moduleName];
    if (tmp_module) {
        if (tmp_module->getModuleVersion() >= moduleVersion) {
            tmp_automation_module = static_cast<AutomationModule*>(tmp_module);
            if (tmp_automation_module) {
                QMap<QString, AutomationCallbackFunction*> tmp_module_functions_map = tmp_automation_module->getFunctionsMap();
                if (tmp_module_functions_map.contains(functionName))
                    return tmp_module_functions_map[functionName];
            }
        }
    }
    return NULL;
}


void Session::setDefaultProjectVersionId(const QString &id)
{
    m_default_project_version_id = id;
}


QString Session::getDefaultProjectVersionId() const
{
    return m_default_project_version_id;
}

const QList<RequirementCategory*> & Session::requirementsCategories() const
{
    return m_requirements_categories_list;
}

const QList<TestResult*> &  Session::testsResults() const
{
    return m_tests_results_list;
}

const QList<ActionResult*> &  Session::actionsResults() const
{
    return m_actions_results_list;
}

const QList<Status*> &  Session::requirementsStatus() const
{
    return m_requirements_status_list;
}

const QList<TestType*> &  Session::testsTypes() const
{
    return m_tests_types_list;
}

const QList<CustomFieldDesc*> &  Session::customTestsFieldsDesc() const
{
    return m_custom_tests_fields_desc;
}

const QList<CustomFieldDesc*> &  Session::customRequirementsFieldsDesc() const
{
    return m_custom_requirements_fields_desc;
}

const QList<QString> &  Session::currentUserRoles() const
{
    return m_user_roles;
}

void Session::setLastErrorMessage(const char* in_error_mess)
{
    net_session_print_error(m_session, "%s\n", in_error_mess);
}

void Session::setLastErrorMessage(const int in_error_id)
{
    net_session_print_error(m_session, "%s\n", cl_get_error_message(m_session, in_error_id));
}

QString Session::getLastErrorMessage() const
{
    return TR_SERVER_MESSAGE(m_session->m_last_error_msg);
}

QString Session::getErrorMessage(const int in_error_id) const
{
    return TR_SERVER_MESSAGE(cl_get_error_message(m_session, in_error_id));
}

net_session* Session::getClientSession() const
{
    return m_session;
}

const QString& Session::getLangage() const
{
    return m_lang;
}

const QString& Session::getApplicationVersion() const
{
    return m_app_version;
}

QMap<QString, QPair<QString, QString> > Session::getBugtrackersCredentials() const
{
    return m_bugtrackers_credentials;
}

void Session::setBugtrackerCredential(QString in_module_name, QPair<QString, QString> in_login_password)
{
    m_bugtrackers_credentials[in_module_name] = in_login_password;
}
