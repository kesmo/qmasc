/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef XMLPROJECTDATAS_H_
#define XMLPROJECTDATAS_H_

#include <QHash>

namespace Entities
{

class Test;
class Requirement;
class TestContent;
class RequirementContent;
class Action;
class AutomatedAction;
class TestRequirement;
class ProjectVersion;

class XmlProjectDatas
{
public:
    XmlProjectDatas();
    virtual ~XmlProjectDatas();

    const char* getNewTestContentIdentifier(const char *in_identifier);
    QString getNewTestContentOriginalIdentifier(TestContent *in_test_content, const char *in_original_identifier);
    const char* getNewRequirementContentIdentifier(const char *in_identifier);
    QString getNewRequirementContentOriginalIdentifier(const char *in_original_identifier);
    const char* getNewTestIdentifier(ProjectVersion* in_project_version, const char *in_identifier);
    const char* getNewRequirementIdentifier(ProjectVersion* in_project_version, const char *in_identifier);

    QHash<QString, Test*>            m_tests_dict;
    QHash<QString, Requirement*>    m_requirements_dict;

    QHash<QString, TestContent*>            m_tests_contents_dict;
    QHash<QString, QString>                m_originals_tests_contents_dict;
    QHash<QString, RequirementContent*>        m_requirements_contents_dict;
    QHash<QString, QString>                m_originals_requirements_contents_dict;

    QHash<QString, QList<Action*> >            m_actions_dict;
    QHash<QString, QList<AutomatedAction*> >            m_automated_actions_dict;
    QHash<QString, QList<TestRequirement*> >    m_tests_requirements_dict;

    QList<TestContent*>                    m_tests_contents_list;
    QList<RequirementContent*>                m_requirements_contents_list;
    QList<ProjectVersion*>                m_versions_list;

    bool m_is_valid;

};
}

#endif /* XMLPROJECTDATAS_H_ */
