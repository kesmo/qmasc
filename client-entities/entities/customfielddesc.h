/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef CUSTOMFIELDDESC_H
#define CUSTOMFIELDDESC_H

#include "entity.h"
#include "customtestfield.h"
#include "customrequirementfield.h"

static const QString TimeFormat =  "hh:mm:ss";
static const QString DateFormat =  "yyyy/MM/dd";
static const QString DateTimeFormat =  "yyyy/MM/dd hh:mm:ss";

namespace Entities
{
class CustomFieldDesc :
    public Entity<CustomFieldDesc, &custom_fields_desc_table_def>
{
public:
    CustomFieldDesc();

    ADD_RELATION(CustomFieldDesc, CustomTestField, CUSTOM_TEST_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID);
    ADD_RELATION(CustomFieldDesc, CustomRequirementField, CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID);
};
}

#endif // CUSTOMFIELDDESC_H
