/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifdef GUI_AUTOMATION_ACTIVATED
#include "automation.h"
#endif

#include "automatedactionwindowhierarchy.h"

const char AUTOMATED_ACTION_WINDOW_HIERARCHY_TEXT[] = "text";
const char AUTOMATED_ACTION_WINDOW_HIERARCHY_CLASS[] = "class";
const char AUTOMATED_ACTION_WINDOW_HIERARCHY_IS_VISIBLE[] = "visible";

static const char *AUTOMATED_ACTION_WINDOW_HIERARCHY_COLUMNS[] = {
    AUTOMATED_ACTION_WINDOW_HIERARCHY_TEXT,
    AUTOMATED_ACTION_WINDOW_HIERARCHY_CLASS,
    AUTOMATED_ACTION_WINDOW_HIERARCHY_IS_VISIBLE,
    NULL
};

extern const entity_def automated_action_window_hierarchy_table_def = {
    0,
    NULL,
    NULL,
    NULL,
    AUTOMATED_ACTION_WINDOW_HIERARCHY_COLUMNS,
    NULL,
    NULL,
    sizeof(AUTOMATED_ACTION_WINDOW_HIERARCHY_COLUMNS) / sizeof(char*) - 1
};


namespace Entities
{
AutomatedActionWindowHierarchy::AutomatedActionWindowHierarchy()
{
}


#ifdef GUI_AUTOMATION_ACTIVATED

QList<AutomatedActionWindowHierarchy*> AutomatedActionWindowHierarchy::fromWindowHierarchyStructure(windows_hierarchy* in_window_hierarchy)
{
    QList<AutomatedActionWindowHierarchy*>  tmp_windows_list;

    if (in_window_hierarchy)
    {
        windows_hierarchy* tmp_next_window_hiererchy = in_window_hierarchy;
        AutomatedActionWindowHierarchy* tmp_window = NULL;

        // Next windows
        while (tmp_next_window_hiererchy){

            tmp_window = new AutomatedActionWindowHierarchy();
            tmp_window->setValueForKey(tmp_next_window_hiererchy->m_window_name, AUTOMATED_ACTION_WINDOW_HIERARCHY_TEXT);
            tmp_window->setValueForKey(tmp_next_window_hiererchy->m_window_class_name, AUTOMATED_ACTION_WINDOW_HIERARCHY_CLASS);
            tmp_window->setValueForKey(tmp_next_window_hiererchy->m_is_visible == TRUE ? YES : NO, AUTOMATED_ACTION_WINDOW_HIERARCHY_IS_VISIBLE);

            if (tmp_next_window_hiererchy->m_first_child_window)
                AutomatedActionWindowHierarchy::ParentAutomatedActionWindowHierarchyRelation::instance().setChilds(tmp_window, AutomatedActionWindowHierarchy::fromWindowHierarchyStructure(tmp_next_window_hiererchy->m_first_child_window));

            tmp_windows_list.append(tmp_window);


            tmp_next_window_hiererchy = tmp_next_window_hiererchy->m_next_window;
        }
    }
    return tmp_windows_list;
}

#endif
}
