#ifndef REQUIREMENTCATEGORY_ENTITY_H
#define REQUIREMENTCATEGORY_ENTITY_H

#include "entity.h"

namespace Entities
{

class RequirementCategory:
    public Entity<RequirementCategory, &requirements_categories_table_def>
{
public:
    RequirementCategory();
};

}

#endif // REQUIREMENTCATEGORY_ENTITY_H
