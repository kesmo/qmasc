#include "feature.h"
#include "projectversion.h"
#include "featurecontent.h"
#include "session.h"

namespace Entities
{

/**
  Constructeur
**/
BasicFeature::BasicFeature() : Entity()
{
}

Feature::Feature()
{
}


Feature::Feature(ProjectVersion* in_project_version)
{
    setProjectVersion(in_project_version);
}


Feature::Feature(Feature* in_parent)
{
    if (in_parent != NULL) {
        setParent(in_parent);
        setProjectVersion(in_parent->projectVersion());
    }
}

void Feature::setProjectVersion(ProjectVersion* in_project_version)
{
    if (in_project_version != NULL)
    {
        ProjectVersion::FeatureRelation::instance().setParent(in_project_version, this);
        if (is_empty_string(getIdentifier()))
        {
            setValueForKey(projectVersion()->getIdentifier(), FEATURES_TABLE_PROJECT_VERSION_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), FEATURES_TABLE_PROJECT_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), FEATURES_TABLE_VERSION);
        }
    }
}


ProjectVersion* Feature::projectVersion()
{
    return ProjectVersion::FeatureRelation::instance().getParent(this);
}

Feature* Feature::parentFeature()
{
    return getParent();
}


void Feature::setDataFromFeatureContent(FeatureContent *in_feature_content)
{
    if (in_feature_content != NULL)
    {
        setValueForKey(in_feature_content->getIdentifier(), FEATURES_HIERARCHY_FEATURE_CONTENT_ID);
        setValueForKey(in_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME), FEATURES_HIERARCHY_SHORT_NAME);
        setValueForKey(in_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_VERSION), FEATURES_HIERARCHY_CONTENT_VERSION);
        setValueForKey(in_feature_content->getValueForKey(FEATURES_CONTENTS_TABLE_STATUS), FEATURES_HIERARCHY_STATUS);
    }
}


int Feature::saveRecord()
{
    int tmp_result = NOERR;

    BasicFeature tmp_feature_record;

    tmp_feature_record.setValueForKey(getOriginalValueForKey(FEATURES_HIERARCHY_PARENT_FEATURE_ID), FEATURES_TABLE_PARENT_FEATURE_ID);
    tmp_feature_record.setValueForKey(getOriginalValueForKey(FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID), FEATURES_TABLE_PREVIOUS_FEATURE_ID);
    tmp_feature_record.setValueForKey(getOriginalValueForKey(FEATURES_HIERARCHY_PROJECT_VERSION_ID), FEATURES_TABLE_PROJECT_VERSION_ID);
    tmp_feature_record.setValueForKey(getOriginalValueForKey(FEATURES_HIERARCHY_PROJECT_ID), FEATURES_TABLE_PROJECT_ID);
    tmp_feature_record.setValueForKey(getOriginalValueForKey(FEATURES_HIERARCHY_FEATURE_CONTENT_ID), FEATURES_TABLE_FEATURE_CONTENT_ID);
    tmp_feature_record.setValueForKey(getOriginalValueForKey(FEATURES_HIERARCHY_VERSION), FEATURES_TABLE_VERSION);
    tmp_feature_record.applyChanges();

    if (parentFeature() != NULL)
    {
        if (is_empty_string(parentFeature()->getIdentifier()))
        {
            tmp_result = parentFeature()->saveRecord();
            if (tmp_result == NOERR)
                setValueForKey(parentFeature()->getIdentifier(), FEATURES_HIERARCHY_PARENT_FEATURE_ID);
            else
                return tmp_result;
        }
        else
            setValueForKey(parentFeature()->getIdentifier(), FEATURES_HIERARCHY_PARENT_FEATURE_ID);

        setValueForKey(parentFeature()->getValueForKey(FEATURES_HIERARCHY_PROJECT_VERSION_ID), FEATURES_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(parentFeature()->getValueForKey(FEATURES_HIERARCHY_PROJECT_ID), FEATURES_HIERARCHY_PROJECT_ID);
        setValueForKey(parentFeature()->getValueForKey(FEATURES_HIERARCHY_VERSION), FEATURES_HIERARCHY_VERSION);
    }
    else
        setValueForKey(NULL, FEATURES_HIERARCHY_PARENT_FEATURE_ID);

    if (projectVersion() != NULL)
    {
        setValueForKey(projectVersion()->getIdentifier(), FEATURES_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), FEATURES_HIERARCHY_PROJECT_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), FEATURES_HIERARCHY_VERSION);
    }

    tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_PARENT_FEATURE_ID), FEATURES_TABLE_PARENT_FEATURE_ID);
    tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID), FEATURES_TABLE_PREVIOUS_FEATURE_ID);
    tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_PROJECT_ID), FEATURES_TABLE_PROJECT_ID);
    tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_PROJECT_VERSION_ID), FEATURES_TABLE_PROJECT_VERSION_ID);
    tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_FEATURE_CONTENT_ID), FEATURES_TABLE_FEATURE_CONTENT_ID);
    tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_VERSION), FEATURES_TABLE_VERSION);

    if (is_empty_string(getIdentifier()))
    {
        tmp_result = tmp_feature_record.insertRecord();
        if (tmp_result == NOERR)
        {
            setValueForKey(tmp_feature_record.getIdentifier(), FEATURES_HIERARCHY_FEATURE_ID);
            applyChanges();
        }
    }
    else
    {
        tmp_feature_record.setValueForKey(getValueForKey(FEATURES_HIERARCHY_FEATURE_ID), FEATURES_TABLE_FEATURE_ID);
        tmp_result = tmp_feature_record.saveRecord();
        if (tmp_result == NOERR)
        {
            applyChanges();
        }
    }

    return tmp_result;
}

int Feature::deleteRecord()
{
    int tmp_result = NOERR;

    if (is_empty_string(getIdentifier()) == FALSE)
    {
        BasicFeature tmp_feature;
        tmp_feature.setValueForKey(getIdentifier(), FEATURES_TABLE_FEATURE_ID);
        tmp_result = tmp_feature.deleteRecord();
    }

    return tmp_result;
}

bool Feature::isModifiable()
{
    if (projectVersion() == NULL)
        return false;

    return projectVersion()->canWriteFeatures();
}



}
