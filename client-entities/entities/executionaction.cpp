/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "executionaction.h"
#include "netcommon.h"
#include "session.h"
#include "action.h"

namespace Entities
{
ExecutionAction::ExecutionAction() : Entity()
{
}

ExecutionAction::~ExecutionAction()
{
}

int ExecutionAction::saveRecord()
{
    int tmp_return = NOERR;

    if (is_empty_string(getIdentifier()))
    {
        Action *tmp_action = Action::ExecutionActionRelation::instance().getParent(this);
        if (tmp_action != NULL)
            setValueForKey(tmp_action->getIdentifier(), EXECUTIONS_ACTIONS_TABLE_ACTION_ID);

        tmp_return = Entity::saveRecord();
    }
    else
        tmp_return = Entity::saveRecord();

    return tmp_return;
}
}
