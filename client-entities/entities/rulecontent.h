#ifndef RULECONTENT_H
#define RULECONTENT_H

#include "rule.h"

namespace Entities
{
class RuleContent :
    public Entity<RuleContent, &rules_contents_table_def>
{
public:
    RuleContent();

    RuleContent* copy() const;

    ADD_RELATION(RuleContent, Rule, RULES_TABLE_RULE_CONTENT_ID, RULES_TABLE_PREVIOUS_RULE_ID);
};
}

#endif // RULECONTENT_H
