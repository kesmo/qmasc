/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "testrequirement.h"
#include "entities.h"
#include "utilities.h"
#include "session.h"


namespace Entities
{

/**
  Constructeur
**/
TestRequirement::TestRequirement() : Entity()
{
}



TestRequirement::~TestRequirement()
{
}


void TestRequirement::writeXml(QXmlStreamWriter & in_xml_writer)
{
    in_xml_writer.writeStartElement("testRequirement");
    in_xml_writer.writeAttribute("testContentId", getValueForKey(TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID));
    in_xml_writer.writeAttribute("originalRequirementContentId", getValueForKey(TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID));
    in_xml_writer.writeEndElement();
}


bool TestRequirement::readXml(QXmlStreamReader & in_xml_reader)
{
    setValueForKey(in_xml_reader.attributes().value("testContentId").toString().toStdString().c_str(), TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
    setValueForKey(in_xml_reader.attributes().value("originalRequirementContentId").toString().toStdString().c_str(), TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);

    in_xml_reader.skipCurrentElement();

    return true;
}


int TestRequirement::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int tmp_result = NOERR;
    const char *tmp_test_content_id = getValueForKey(TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
    const char *tmp_original_requirement_content_id = getValueForKey(TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);

    QString tmp_std_string = in_xml_datas.getNewRequirementContentOriginalIdentifier(tmp_original_requirement_content_id);
    setValueForKey(in_xml_datas.getNewTestContentIdentifier(tmp_test_content_id), TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
    if (tmp_std_string.isEmpty())
        setValueForKey(NULL, TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
    else
        setValueForKey(tmp_std_string.toStdString().c_str(), TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);

    tmp_result = saveRecord();
    if (tmp_result != NOERR) {
        LOG_ERROR(Session::instance().getClientSession(), "IMPORT TestRequirement ====> %s\n", cl_get_error_message(Session::instance().getClientSession(), tmp_result));
    }

    return tmp_result;
}

}
