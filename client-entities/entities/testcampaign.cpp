/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "testcampaign.h"
#include "utilities.h"
#include "xmlprojectdatas.h"
#include "test.h"
#include "campaign.h"

namespace Entities
{

//IMPLEMENTS_1_TO_N_RELATION_NAMED(TestCampaign, TestCampaign, ChildTestCampaign)

TestCampaign::TestCampaign()
{
}


/**
  Destructeur
**/
TestCampaign::~TestCampaign()
{
}


int TestCampaign::saveRecord()
{
    int tmp_return = NOERR;

    Test *tmp_test = Test::TestCampaignRelation::instance().getParent(this);

    if (tmp_test != NULL)
    {
        if (is_empty_string(tmp_test->getIdentifier()))
        {
            tmp_return = tmp_test->saveRecord();
            if (tmp_return == NOERR)
                setValueForKey(tmp_test->getIdentifier(), TESTS_CAMPAIGNS_TABLE_TEST_ID);
            else
                return tmp_return;
        }
        else
            setValueForKey(tmp_test->getIdentifier(), TESTS_CAMPAIGNS_TABLE_TEST_ID);
    }
    else
        setValueForKey(NULL, TESTS_CAMPAIGNS_TABLE_TEST_ID);

    if (tmp_return == NOERR)
    {
        setValueForKey(Campaign::TestCampaignRelation::instance().getParent(this)->getIdentifier(), TESTS_CAMPAIGNS_TABLE_CAMPAIGN_ID);
        tmp_return = Entity::saveRecord();
    }

    return tmp_return;
}


void TestCampaign::writeXml(QXmlStreamWriter & in_xml_writer)
{
    in_xml_writer.writeStartElement("campaignTest");
    in_xml_writer.writeAttribute("testId", getValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_ID));
    in_xml_writer.writeEndElement();
}


bool TestCampaign::readXml(QXmlStreamReader & in_xml_reader)
{
    QString            tmp_test_id = in_xml_reader.attributes().value("testId").toString();

    setValueForKey(tmp_test_id.toStdString().c_str(), TESTS_CAMPAIGNS_TABLE_TEST_ID);
    in_xml_reader.skipCurrentElement();

    return true;
}


int TestCampaign::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int            tmp_result = NOERR;
    const char        *tmp_test_id = getValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_ID);

    if (is_empty_string(getOriginalValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID)) == TRUE) {
        Campaign *campaign = Campaign::TestCampaignRelation::instance().getParent(this);
        if (campaign != NULL) {
            setValueForKey(campaign->getIdentifier(), TESTS_CAMPAIGNS_TABLE_CAMPAIGN_ID);
            setValueForKey(in_xml_datas.getNewTestIdentifier(campaign->projectVersion(), tmp_test_id), TESTS_CAMPAIGNS_TABLE_TEST_ID);
        }

        tmp_result = insertRecord();
    }

    return tmp_result;
}

}
