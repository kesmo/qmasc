/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "projectparameter.h"
#include "utilities.h"
#include "project.h"

namespace Entities
{

ProjectParameter::ProjectParameter()
{
}


ProjectParameter::ProjectParameter(Project *in_project)
{
    Project::ProjectParameterRelation::instance().setParent(in_project, this);
}

Project *ProjectParameter::project() {
    return Project::ProjectParameterRelation::instance().getParent(this);
}

int ProjectParameter::saveRecord()
{
    int    tmp_return = NOERR;

    if (is_empty_string(getIdentifier())) {
        Project *project = Project::ProjectParameterRelation::instance().getParent(this);
        if (project != NULL)
            setValueForKey(project->getIdentifier(), PROJECTS_PARAMETERS_TABLE_PROJECT_ID);

        tmp_return = Entity::insertRecord();
    }
    else
        tmp_return = Entity::saveRecord();

    return tmp_return;
}


const char* ProjectParameter::name()
{
    return getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME);
}


const char* ProjectParameter::value()
{
    return getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE);
}

}
