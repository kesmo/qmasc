#ifndef TESTTYPE_ENTITY_H
#define TESTTYPE_ENTITY_H

#include "entity.h"

namespace Entities
{

class TestType:
    public Entity<TestType, &tests_types_table_def>
{
public:
    TestType();
};

}

#endif // TESTTYPE_ENTITY_H
