/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef PROJECTVERSION_H
#define PROJECTVERSION_H


#include <QList>
#include <QString>

#include "testcontent.h"
#include "requirementcontent.h"
#include "campaign.h"
#include "featurecontent.h"
#include "rulecontent.h"
#include "projectversionparameter.h"
#include "bug.h"

#include "xmlprojectdatas.h"

namespace Entities
{
class Project;

class ProjectVersion:
    public Entity<ProjectVersion, &projects_versions_table_def>
{

public:
    ProjectVersion();
    ProjectVersion(Project *in_project);

    Project *project();

    const QList < Test* >& testsHierarchy();
    const QList < Requirement* >& requirementsHierarchy();
    const QList < Campaign* >& campaignsList();
    const QList < Feature* >& featuresHierarchy();
    const QList < Rule* >& rulesHierarchy();
    const QList < ProjectVersionParameter* >& parametersHierarchy();

    int saveRecord();

    void writeXml(QXmlStreamWriter & in_xml_writer);
    bool readXml(QXmlStreamReader & in_xml_reader);
    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    static QString formatProjectVersionNumber(const char *in_long_version_number);

    QList<Bug*> loadBugs();

    bool canWriteTests();
    bool canWriteRequirements();
    bool canWriteExecutionsCampaigns();
    bool canWriteFeatures();
    bool canWriteRules();

    ADD_RELATION(ProjectVersion, TestContent, TESTS_CONTENTS_TABLE_PROJECT_VERSION_ID);
    ADD_RELATION(ProjectVersion, Test, TESTS_TABLE_PROJECT_VERSION_ID, TESTS_HIERARCHY_PREVIOUS_TEST_ID, TESTS_HIERARCHY_PARENT_TEST_ID);
    ADD_RELATION(ProjectVersion, RequirementContent, REQUIREMENTS_CONTENTS_TABLE_PROJECT_VERSION_ID);
    ADD_RELATION(ProjectVersion, Requirement, REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID, REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID, REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);
    ADD_RELATION(ProjectVersion, Campaign, CAMPAIGNS_TABLE_PROJECT_VERSION_ID);
    ADD_RELATION(ProjectVersion, Feature, FEATURES_HIERARCHY_PROJECT_VERSION_ID, FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID, FEATURES_HIERARCHY_PARENT_FEATURE_ID);
    ADD_RELATION(ProjectVersion, FeatureContent, FEATURES_CONTENTS_TABLE_PROJECT_VERSION_ID);
    ADD_RELATION(ProjectVersion, Rule, RULES_HIERARCHY_PROJECT_VERSION_ID, RULES_HIERARCHY_PREVIOUS_RULE_ID, RULES_HIERARCHY_PARENT_RULE_ID);
    ADD_RELATION(ProjectVersion, RuleContent, RULES_CONTENTS_TABLE_PROJECT_VERSION_ID);
    ADD_RELATION(ProjectVersion, ProjectVersionParameter, PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_VERSION_ID, nullptr, PROJECTS_VERSIONS_PARAMETERS_TABLE_PARENT_GROUP_PARAMETER_ID);

};

}

#endif // PROJECTVERSION_H
