/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef TESTCONTENT_H
#define TESTCONTENT_H

#include <QVariant>
#include "action.h"
#include "automatedaction.h"
#include "testrequirement.h"
#include "executionrequirement.h"
#include "customtestfield.h"
#include "testcontentfile.h"
#include "test.h"
#include "xmlprojectdatas.h"

namespace Entities
{
class XmlProjectDatas;

class TestContent :
    public Entity<TestContent, &tests_contents_table_def>
{
private:

public:
    TestContent();

    int saveRecord();
    TestContent* copy();

    TestContent* previousTestContent();
    TestContent* nextTestContent();

    bool isAutomatedGuiTest() const;
    bool isAutomatedBatchTest() const;

    void writeXml(QXmlStreamWriter & in_xml_writer);
    bool readXml(QXmlStreamReader & in_xml_reader, XmlProjectDatas & in_xml_datas);

    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    static TestContent* loadLastTestContentForVersion(const char *in_original_test_content_id, const char *in_project_version);

    QString getPriorityLabel();

    ADD_RELATION(TestContent, Action, ACTIONS_TABLE_TEST_CONTENT_ID, ACTIONS_TABLE_PREVIOUS_ACTION_ID);
    ADD_NAMED_RELATION(LinkedAction, TestContent, Action, ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);
    ADD_RELATION(TestContent, AutomatedAction, AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID, AUTOMATED_ACTIONS_TABLE_PREVIOUS_ACTION_ID);
    ADD_RELATION(TestContent, TestRequirement, TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
    ADD_RELATION(TestContent, ExecutionRequirement, EXECUTIONS_REQUIREMENTS_TABLE_TEST_CONTENT_ID);
    ADD_RELATION(TestContent, CustomTestField, CUSTOM_TEST_FIELDS_TABLE_TEST_CONTENT_ID);
    ADD_RELATION(TestContent, TestContentFile, TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_ID);
    ADD_RELATION(TestContent, Test, TESTS_TABLE_TEST_CONTENT_ID, TESTS_TABLE_PREVIOUS_TEST_ID);
};

}

#endif // TESTCONTENT_H
