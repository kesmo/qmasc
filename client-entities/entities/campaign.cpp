/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "project.h"
#include "projectgrant.h"
#include "test.h"
#include "campaign.h"
#include "testcampaign.h"
#include "executioncampaign.h"
#include "utilities.h"
#include "requirement.h"
#include "projectversion.h"
#include "testsplan/testsplan.h"
#include "session.h"
#include "user.h"

namespace Entities
{

Campaign::Campaign()
{
}


Campaign::Campaign(ProjectVersion *in_project)
{
    ProjectVersion::CampaignRelation::instance().setParent(in_project, this);
}

ProjectVersion* Campaign::projectVersion()
{
    return ProjectVersion::CampaignRelation::instance().getParent(this);
}


const QList < TestCampaign* >& Campaign::testsList(){
    return Campaign::TestCampaignRelation::instance().getChilds(this);
}

void Campaign::insertTestAtIndex(Test *in_test, int in_index)
{
    TestCampaign *tmp_campaign_test = NULL;

    if (in_test != NULL)
    {
        tmp_campaign_test = new TestCampaign();

        Test::TestCampaignRelation::instance().setParent(in_test, tmp_campaign_test);
        TestCampaignRelation::instance().insertChild(this, in_index, tmp_campaign_test);
    }
}


int Campaign::saveRecord()
{
    int     tmp_result = NOERR;

    if (is_empty_string(getIdentifier()))
    {
        setValueForKey(projectVersion()->getIdentifier(), CAMPAIGNS_TABLE_PROJECT_VERSION_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), CAMPAIGNS_TABLE_PROJECT_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), CAMPAIGNS_TABLE_PROJECT_VERSION);
        tmp_result = Entity::insertRecord();
    }
    else
    {
        tmp_result = Entity::saveRecord();
    }

    return tmp_result;
}



void Campaign::writeXml(QXmlStreamWriter & in_xml_writer)
{
    QList<TestCampaign*>    tmp_tests_list;

    in_xml_writer.writeStartElement("campaign");
    in_xml_writer.writeAttribute("name", getValueForKey(CAMPAIGNS_TABLE_SHORT_NAME));
    in_xml_writer.writeTextElement("description", getValueForKey(CAMPAIGNS_TABLE_DESCRIPTION));

    tmp_tests_list = Campaign::TestCampaignRelation::instance().getChilds(this);
    if (tmp_tests_list.count() > 0)
    {
        in_xml_writer.writeStartElement("campaignTests");
        foreach(TestCampaign *tmp_test, tmp_tests_list)
        {
            tmp_test->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
    }

    in_xml_writer.writeEndElement();
}


bool Campaign::readXml(QXmlStreamReader & in_xml_reader)
{
    TestCampaign        *tmp_test = NULL;

    QString            tmp_name = in_xml_reader.attributes().value("name").toString();
    QString            tmp_text;

    setValueForKey(tmp_name.toStdString().c_str(), CAMPAIGNS_TABLE_SHORT_NAME);

    while (in_xml_reader.readNextStartElement())
    {
        // Description
        if (in_xml_reader.name() == "description")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), CAMPAIGNS_TABLE_DESCRIPTION);
        }
        else if (in_xml_reader.name() == "campaignTests")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "campaignTest")
                {
                    tmp_test = new TestCampaign();
                    tmp_test->readXml(in_xml_reader);
                    Campaign::TestCampaignRelation::instance().appendChild(this, tmp_test);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else
        {
            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
            in_xml_reader.skipCurrentElement();
        }
    }

    return true;
}


int Campaign::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int        tmp_result = NOERR;
    const char    *tmp_previous_test_campaign_id = NULL;

    if (is_empty_string(getOriginalValueForKey(CAMPAIGNS_TABLE_CAMPAIGN_ID)) == TRUE)
    {
        tmp_result = saveRecord();
        if (tmp_result == NOERR)
        {
            foreach(TestCampaign *tmp_test, testsList())
            {
                tmp_test->setValueForKey(tmp_previous_test_campaign_id, TESTS_CAMPAIGNS_TABLE_PREVIOUS_TEST_CAMPAIGN_ID);
                if (tmp_test->saveFromXmlProjectDatas(in_xml_datas) == NOERR)
                    tmp_previous_test_campaign_id = tmp_test->getIdentifier();
            }
        }
    }

    return tmp_result;
}


bool Campaign::isModifiable()
{
    if (projectVersion() == NULL || projectVersion()->project() == NULL)
        return false;

    QList<const char *> values;
    QList<const char *> keys;

    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(projectVersion()->project(),
                                                                                                           QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(CAMPAIGNS_TABLE_SIG_ID).toStdString().c_str(),
                                                                                                           QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}
}
