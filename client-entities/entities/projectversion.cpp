/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "projectgrant.h"
#include "projectversion.h"
#include "test.h"
#include "campaign.h"
#include "project.h"

#include "client.h"
#include "entities.h"
#include "utilities.h"

#include "feature.h"
#include "rule.h"

#include "session.h"
#include "user.h"

#include <string.h>
#if defined (__APPLE__)
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <QIcon>

namespace Entities
{

/**
  Constructeur
**/
ProjectVersion::ProjectVersion()
{
}


/**
  Constructeur
**/
ProjectVersion::ProjectVersion(Project *in_project)
{
    Project::ProjectVersionRelation::instance().setParent(in_project, this);
}

Project *ProjectVersion::project()
{
    return Project::ProjectVersionRelation::instance().getParent(this);
}

const QList < Test* >& ProjectVersion::testsHierarchy()
{
    return ProjectVersion::TestRelation::instance().getChilds(this);
}


const QList < Requirement* >& ProjectVersion::requirementsHierarchy()
{
    return ProjectVersion::RequirementRelation::instance().getChilds(this);
}


const QList < Campaign* >& ProjectVersion::campaignsList()
{
    return ProjectVersion::CampaignRelation::instance().getChilds(this);
}


const QList < Feature* >& ProjectVersion::featuresHierarchy()
{
    return ProjectVersion::FeatureRelation::instance().getChilds(this);
}


const QList < Rule* >& ProjectVersion::rulesHierarchy()
{
    return ProjectVersion::RuleRelation::instance().getChilds(this);
}

const QList < ProjectVersionParameter* >& ProjectVersion::parametersHierarchy()
{
    return ProjectVersion::ProjectVersionParameterRelation::instance().getChilds(this);
}


int ProjectVersion::saveRecord()
{
    int tmp_return = NOERR;

    if (is_empty_string(getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID)))
    {
        if (project() != NULL)
            setValueForKey(project()->getIdentifier(), PROJECTS_VERSIONS_TABLE_PROJECT_ID);

        tmp_return = Entity::insertRecord();
    }
    else
        tmp_return = Entity::saveRecord();

    return tmp_return;
}

void ProjectVersion::writeXml(QXmlStreamWriter & in_xml_writer)
{
    QList < Requirement* >    tmp_requirements_list;
    QList < Test* >        tmp_tests_list;
    QList < Campaign* >            tmp_campaigns_list;

    in_xml_writer.writeStartElement("version");
    in_xml_writer.writeAttribute("number", getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

    in_xml_writer.writeTextElement("description", getValueForKey(PROJECTS_VERSIONS_TABLE_DESCRIPTION));

    tmp_requirements_list = requirementsHierarchy();
    if (tmp_requirements_list.count() > 0)
    {
        in_xml_writer.writeStartElement("requirements");
        foreach(Requirement *tmp_requirement, tmp_requirements_list)
        {
            tmp_requirement->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
        qDeleteAll(tmp_requirements_list);
    }

    tmp_tests_list = testsHierarchy();
    if (tmp_tests_list.count() > 0)
    {
        in_xml_writer.writeStartElement("tests");
        foreach(Test *tmp_test, tmp_tests_list)
        {
            tmp_test->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
        qDeleteAll(tmp_tests_list);
    }

    tmp_campaigns_list = ProjectVersion::CampaignRelation::instance().getChilds(this);
    if (tmp_campaigns_list.count() > 0)
    {
        in_xml_writer.writeStartElement("campaigns");
        foreach(Campaign *tmp_campaign, tmp_campaigns_list)
        {
            tmp_campaign->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
    }

    in_xml_writer.writeEndElement();
}


QString ProjectVersion::formatProjectVersionNumber(const char *in_long_version_number)
{
    QString    tmp_version_string;
    QStringList tmp_version_numbers;

    if (is_empty_string(in_long_version_number) == FALSE)
    {
        tmp_version_numbers = QString(in_long_version_number).split(".");
        if (tmp_version_numbers.count() > 0)
            tmp_version_string += QString::number(tmp_version_numbers[0].toInt());
        if (tmp_version_numbers.count() > 1)
            tmp_version_string += "." + QString::number(tmp_version_numbers[1].toInt());
        if (tmp_version_numbers.count() > 2)
            tmp_version_string += "." + QString::number(tmp_version_numbers[2].toInt());
        if (tmp_version_numbers.count() > 3)
            tmp_version_string += "." + QString::number(tmp_version_numbers[3].toInt());
    }

    return tmp_version_string;
}


bool ProjectVersion::readXml(QXmlStreamReader & in_xml_reader)
{
    Requirement    *tmp_requirement = NULL;
    Test        *tmp_test = NULL;
    Campaign            *tmp_campaign = NULL;

    QString        tmp_text;
    QString        tmp_version = in_xml_reader.attributes().value("number").toString();

    const char            *tmp_previous_test_id = NULL;
    const char            *tmp_previous_requirement_id = NULL;

    setValueForKey(tmp_version.toStdString().c_str(), PROJECTS_VERSIONS_TABLE_VERSION);

    while (in_xml_reader.readNextStartElement())
    {
        // Description
        if (in_xml_reader.name() == "description")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), PROJECTS_VERSIONS_TABLE_DESCRIPTION);
        }
        else if (in_xml_reader.name() == "tests")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "test")
                {
                    tmp_test = new Test(this);
                    tmp_test->setValueForKey(tmp_previous_test_id, TESTS_HIERARCHY_PREVIOUS_TEST_ID);
                    tmp_test->readXml(in_xml_reader);
                    tmp_previous_test_id = tmp_test->getIdentifier();
                    ProjectVersion::TestRelation::instance().setParent(this, tmp_test);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else if (in_xml_reader.name() == "requirements")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "requirement")
                {
                    tmp_requirement = new Requirement(this);
                    tmp_requirement->setValueForKey(tmp_previous_requirement_id, REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID);
                    tmp_requirement->readXml(in_xml_reader);
                    tmp_previous_requirement_id = tmp_requirement->getIdentifier();

                    ProjectVersion::RequirementRelation::instance().setParent(this, tmp_requirement);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else if (in_xml_reader.name() == "campaigns")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "campaign")
                {
                    tmp_campaign = new Campaign(this);
                    tmp_campaign->readXml(in_xml_reader);
                    ProjectVersion::CampaignRelation::instance().setParent(this, tmp_campaign);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else
        {
            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
            in_xml_reader.skipCurrentElement();
        }
    }

    return true;
}


int ProjectVersion::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int        tmp_return = NOERR;

    tmp_return = saveRecord();
    if (tmp_return == NOERR)
    {
        foreach(Requirement *tmp_requirement, requirementsHierarchy())
        {
            tmp_requirement->saveFromXmlProjectDatas(in_xml_datas);
        }

        foreach(Test *tmp_test, testsHierarchy())
        {
            tmp_test->saveFromXmlProjectDatas(in_xml_datas);
        }

        foreach(Campaign *tmp_campaign, ProjectVersion::CampaignRelation::instance().getChilds(this))
        {
            tmp_campaign->saveFromXmlProjectDatas(in_xml_datas);
        }
    }

    return tmp_return;
}


QList<Bug*> ProjectVersion::loadBugs()
{
    Bug                    **tmp_bugs = NULL;
    unsigned long            tmp_bugs_count = 0;
    net_session                *tmp_session = Session::instance().getClientSession();
    unsigned long            tmp_index = 0;
    QList<Bug*>                tmp_bugs_list;

    net_session_print_query(tmp_session, "%s IN (SELECT %s FROM %s,%s WHERE %s.%s=%s.%s AND %s.%s=%s AND %s.%s='%s')",
            BUGS_TABLE_EXECUTION_TEST_ID,
            EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID,
            EXECUTIONS_TESTS_TABLE_SIG,
            TESTS_TABLE_SIG,
            TESTS_TABLE_SIG,
            TESTS_TABLE_TEST_ID,
            EXECUTIONS_TESTS_TABLE_SIG,
            EXECUTIONS_TESTS_TABLE_TEST_ID,
            TESTS_TABLE_SIG,
            TESTS_TABLE_PROJECT_ID,
            getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID),
            TESTS_TABLE_SIG,
            TESTS_TABLE_VERSION,
            getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION));

    tmp_bugs_list = Bug::loadRecordsList(tmp_session->m_last_query, BUGS_TABLE_CREATION_DATE);

    return tmp_bugs_list;
}


bool ProjectVersion::canWriteTests()
{
    if (project() == NULL)
        return false;

    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(project(),
                                                                                                           QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(TESTS_HIERARCHY_SIG_ID).toStdString().c_str(),
                                                                                                           QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);

}



bool ProjectVersion::canWriteRequirements()
{
    if (project() == NULL)
        return false;

    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(project(),
                                                                                                   QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(REQUIREMENTS_HIERARCHY_SIG_ID).toStdString().c_str(),
                                                                                                   QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}

bool ProjectVersion::canWriteExecutionsCampaigns()
{
    if (project() == NULL)
        return false;

    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(project(),
                                                                                                           QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(EXECUTIONS_CAMPAIGNS_TABLE_SIG_ID).toStdString().c_str(),
                                                                                                           QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}


bool ProjectVersion::canWriteFeatures()
{
    if (project() == NULL)
        return false;

    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(project(),
                                                                                                           QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(FEATURES_HIERARCHY_SIG_ID).toStdString().c_str(),
                                                                                                           QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}


bool ProjectVersion::canWriteRules()
{
    if (project() == NULL)
        return false;

    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(project(),
                                                                                                           QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(RULES_HIERARCHY_SIG_ID).toStdString().c_str(),
                                                                                                           QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}


}
