/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "test.h"
#include "testcontent.h"
#include "action.h"
#include "requirement.h"
#include "testrequirement.h"
#include "requirementcontent.h"
#include "utilities.h"
#include "projectgrant.h"
#include "session.h"
#include "customtestfield.h"
#include "xmlprojectdatas.h"
#include "campaign.h"
#include "testcampaign.h"
#include "testsplan/graphictest.h"

#include <QFont>
#include <QIcon>
#include <QPixmap>
#include <QDebug>

namespace Entities
{

/**
  Constructeur
**/
BasicTest::BasicTest() : Entity()
{
}


/**
  Constructeur
**/
Test::Test()
{
}


/**
  Constructeur
**/
Test::Test(ProjectVersion *in_project)
{
    setProjectVersion(in_project);
}


/**
  Constructeur
**/
Test::Test(Test* in_parent)
{
    if (in_parent != NULL) {
        setParent(in_parent);
        setProjectVersion(in_parent->projectVersion());
    }
}


/**
  Destructeur
**/
Test::~Test()
{
}


ProjectVersion* Test::projectVersion()
{
    return ProjectVersion::TestRelation::instance().getParent(this);
}


void Test::setProjectVersion(ProjectVersion* in_project_version)
{
    if (in_project_version != NULL)
    {
        ProjectVersion::TestRelation::instance().setParent(in_project_version, this);
        if (is_empty_string(getIdentifier())) {
            setValueForKey(projectVersion()->getIdentifier(), TESTS_HIERARCHY_PROJECT_VERSION_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), TESTS_HIERARCHY_PROJECT_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), TESTS_HIERARCHY_VERSION);
        }
    }
}

Test* Test::findTestWithId(const QList<Test*>& in_tests_list, const char* in_test_id, bool in_recursive)
{
    Test *tmp_found_test = NULL;

    foreach(Test *tmp_test, in_tests_list)
    {
        if (compare_values(tmp_test->getIdentifier(), in_test_id) == 0)
            return tmp_test;

        if (in_recursive)
        {
            tmp_found_test = findTestWithId(tmp_test->getAllChilds(), in_test_id);
            if (tmp_found_test != NULL)
                return tmp_found_test;
        }
    }

    return NULL;
}




int Test::setDataFromRequirement(Requirement *in_requirement)
{
    Test            *tmp_child_test = NULL;
    Requirement        *tmp_child_requirement = NULL;
    RequirementContent    *tmp_requirement_content = NULL;
    TestRequirement    *tmp_test_requirement = NULL;
    TestContent          *tmp_test_content = NULL;

    int                     tmp_result = NOERR;

    if (in_requirement != NULL)
    {
        tmp_requirement_content = RequirementContent::getEntity(in_requirement->getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID));
        if (tmp_requirement_content)
        {
            tmp_test_content = new TestContent();
            ProjectVersion::TestContentRelation::instance().setParent(projectVersion(), tmp_test_content);
            tmp_test_content->setValueForKey(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME), TESTS_CONTENTS_TABLE_SHORT_NAME);
            tmp_test_content->setValueForKey(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION), TESTS_CONTENTS_TABLE_DESCRIPTION);
            tmp_test_content->setValueForKey(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL), TESTS_CONTENTS_TABLE_PRIORITY_LEVEL);
            tmp_test_content->setValueForKey(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID), TESTS_CONTENTS_TABLE_CATEGORY_ID);

            setValueForKey(tmp_test_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME), TESTS_HIERARCHY_SHORT_NAME);
            setValueForKey(tmp_test_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL), TESTS_HIERARCHY_PRIORITY_LEVEL);
            setValueForKey(tmp_test_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID), TESTS_HIERARCHY_CATEGORY_ID);
            setValueForKey(tmp_test_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION), TESTS_HIERARCHY_CONTENT_VERSION);

            tmp_test_requirement = new TestRequirement();
            tmp_test_requirement->setValueForKey(tmp_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_ORIGINAL_REQUIREMENT_CONTENT_ID), TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);

            for(int tmp_index = 0; tmp_index < in_requirement->getAllChilds().count() && tmp_result == NOERR; tmp_index++)
            {
                tmp_child_requirement = in_requirement->getAllChilds()[tmp_index];
                if (tmp_child_requirement != NULL)
                {
                    tmp_child_test = new Test(this);
                    tmp_result = tmp_child_test->setDataFromRequirement(tmp_child_requirement);
                    if (tmp_result == NOERR) {
                        appendChild(tmp_child_test);
                    }
                }
            }
        }
    }

    return tmp_result;
}


Test* Test::parentTest()
{
    return getParent();
}


bool Test::isModifiable()
{
    if (original() != NULL || projectVersion() == NULL)
        return false;


    return projectVersion()->canWriteTests();
}


int Test::deleteRecord()
{
    int        tmp_result = NOERR;

    if (is_empty_string(getIdentifier()) == FALSE)
    {
        BasicTest tmp_test;
        tmp_test.setValueForKey(getIdentifier(), TESTS_TABLE_TEST_ID);
        tmp_result = tmp_test.deleteRecord();
    }

    return tmp_result;
}


void Test::setIsALinkOf(Test* original)
{
    if (original) {
        Test::OriginalTestRelation::instance().setParent(original, this);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), TESTS_HIERARCHY_TEST_CONTENT_ID);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_SHORT_NAME), TESTS_HIERARCHY_SHORT_NAME);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_CATEGORY_ID), TESTS_HIERARCHY_CATEGORY_ID);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_PRIORITY_LEVEL), TESTS_HIERARCHY_PRIORITY_LEVEL);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_STATUS), TESTS_HIERARCHY_STATUS);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED), TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_TYPE), TESTS_HIERARCHY_TEST_CONTENT_TYPE);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_CONTENT_VERSION), TESTS_HIERARCHY_CONTENT_VERSION);
        setValueForKey(original->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID), TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID);
    }
}

Test* Test::original()
{
    Test        *tmp_test = Test::OriginalTestRelation::instance().getParent(this);
    Test        *tmp_original = tmp_test;

    while (tmp_test != NULL && (tmp_original = Test::OriginalTestRelation::instance().getParent(tmp_test)) != NULL)
        tmp_test = tmp_original;

    return tmp_test;
}



/**
  Enregistrer le test
**/
int Test::saveRecord()
{
    int     tmp_result = NOERR;
    Test    *tmp_original = Test::OriginalTestRelation::instance().getParent(this);
    BasicTest    tmp_test_record;
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_PROJECT_VERSION_ID), TESTS_TABLE_PROJECT_VERSION_ID);
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID), TESTS_TABLE_ORIGINAL_TEST_ID);
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_PARENT_TEST_ID), TESTS_TABLE_PARENT_TEST_ID);
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_PREVIOUS_TEST_ID), TESTS_TABLE_PREVIOUS_TEST_ID);
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_PROJECT_ID), TESTS_TABLE_PROJECT_ID);
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), TESTS_TABLE_TEST_CONTENT_ID);
    tmp_test_record.setValueForKey(getOriginalValueForKey(TESTS_HIERARCHY_VERSION), TESTS_TABLE_VERSION);
    tmp_test_record.applyChanges();

    if (tmp_original == NULL)
    {
        setValueForKey(NULL, TESTS_HIERARCHY_ORIGINAL_TEST_ID);
    }
    else
    {

        // Enregistrer le test original
        if (is_empty_string(tmp_original->getIdentifier()))
        {
            tmp_result = tmp_original->saveRecord();
            if (tmp_result == NOERR)
                setValueForKey(tmp_original->getIdentifier(), TESTS_HIERARCHY_ORIGINAL_TEST_ID);
            else
                return tmp_result;
        }
        else
            setValueForKey(tmp_original->getIdentifier(), TESTS_HIERARCHY_ORIGINAL_TEST_ID);
    }

    // Enregistrer le test parent
    if (parentTest() != NULL)
    {
        if (is_empty_string(parentTest()->getIdentifier()))
        {
            tmp_result = parentTest()->saveRecord();
            if (tmp_result == NOERR)
                setValueForKey(parentTest()->getIdentifier(), TESTS_HIERARCHY_PARENT_TEST_ID);
            else
                return tmp_result;
        }
        else
            setValueForKey(parentTest()->getIdentifier(), TESTS_HIERARCHY_PARENT_TEST_ID);

        setValueForKey(parentTest()->getValueForKey(TESTS_HIERARCHY_PROJECT_VERSION_ID), TESTS_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(parentTest()->getValueForKey(TESTS_HIERARCHY_PROJECT_ID), TESTS_HIERARCHY_PROJECT_ID);
        setValueForKey(parentTest()->getValueForKey(TESTS_HIERARCHY_VERSION), TESTS_HIERARCHY_VERSION);
    }
    else
        setValueForKey(NULL, TESTS_HIERARCHY_PARENT_TEST_ID);

    if (projectVersion() != NULL)
    {
        setValueForKey(projectVersion()->getIdentifier(), TESTS_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), TESTS_HIERARCHY_PROJECT_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), TESTS_HIERARCHY_VERSION);
    }

    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PROJECT_VERSION_ID), TESTS_TABLE_PROJECT_VERSION_ID);
    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID), TESTS_TABLE_ORIGINAL_TEST_ID);
    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PARENT_TEST_ID), TESTS_TABLE_PARENT_TEST_ID);
    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PREVIOUS_TEST_ID), TESTS_TABLE_PREVIOUS_TEST_ID);
    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PROJECT_ID), TESTS_TABLE_PROJECT_ID);
    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), TESTS_TABLE_TEST_CONTENT_ID);
    tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_VERSION), TESTS_TABLE_VERSION);

    if (is_empty_string(getIdentifier()))
    {
        tmp_result = tmp_test_record.insertRecord();
        if (tmp_result == NOERR)
        {
            setValueForKey(tmp_test_record.getIdentifier(), TESTS_HIERARCHY_TEST_ID);
            applyChanges();
        }
    }
    else
    {
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_TEST_ID), TESTS_TABLE_TEST_ID);
        tmp_result = tmp_test_record.saveRecord();
        if (tmp_result == NOERR)
        {
            applyChanges();
        }
    }

    return tmp_result;
}


void Test::searchFieldWithValue(QList<Record*> *in_found_list, const char* in_field_name, const char* in_field_value, bool in_recursive, int in_comparison_value)
{
    int        tmp_comparison = 0;

    tmp_comparison = compare_values(getValueForKey(in_field_name), in_field_value);
    if ((tmp_comparison == 0 && (in_comparison_value == EqualTo || in_comparison_value == LowerOrEqualTo || in_comparison_value == UpperOrEqualTo))
    || (tmp_comparison < 0 && (in_comparison_value == LowerThan || in_comparison_value == LowerOrEqualTo))
    || (tmp_comparison > 0 && (in_comparison_value == UpperThan || in_comparison_value == UpperOrEqualTo)))
    {
        in_found_list->append(this);
    }

    if (in_recursive)
    {
        foreach(Test *tmp_child, getAllChilds())
        {
            tmp_child->searchFieldWithValue(in_found_list, in_field_name, in_field_value, in_recursive, in_comparison_value);
        }
    }
}


void Test::writeXml(QXmlStreamWriter & in_xml_writer)
{
    Test        *tmp_original = Test::OriginalTestRelation::instance().getParent(this);

    in_xml_writer.writeStartElement("test");

    if (tmp_original != NULL) {
        in_xml_writer.writeAttribute("id", getIdentifier());
        in_xml_writer.writeAttribute("originalId", tmp_original->getIdentifier());
        in_xml_writer.writeAttribute("testContentId", tmp_original->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID));
    }
    else {
        in_xml_writer.writeAttribute("id", getIdentifier());
        in_xml_writer.writeAttribute("testContentId", getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID));

        if (getAllChilds().count() > 0)
        {
            in_xml_writer.writeStartElement("tests");
            foreach(Test *tmp_test, getAllChilds())
            {
                tmp_test->writeXml(in_xml_writer);
            }
            in_xml_writer.writeEndElement();
        }

    }

    in_xml_writer.writeEndElement();
}


bool Test::readXml(QXmlStreamReader & in_xml_reader)
{
    Test        *tmp_test = NULL;

    QString            tmp_id = in_xml_reader.attributes().value("id").toString();
    QString            tmp_original_id = in_xml_reader.attributes().value("originalId").toString();
    QString            tmp_test_content_id = in_xml_reader.attributes().value("testContentId").toString();

    const char            *tmp_previous_test_id = NULL;

    setValueForKey(tmp_id.toStdString().c_str(), TESTS_HIERARCHY_TEST_ID);
    setValueForKey(tmp_original_id.toStdString().c_str(), TESTS_HIERARCHY_ORIGINAL_TEST_ID);
    setValueForKey(tmp_test_content_id.toStdString().c_str(), TESTS_HIERARCHY_TEST_CONTENT_ID);

    while (in_xml_reader.readNextStartElement())
    {
        if (in_xml_reader.name() == "tests")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "test")
                {
                    tmp_test = new Test(this);
                    tmp_test->setValueForKey(tmp_id.toStdString().c_str(), TESTS_HIERARCHY_PARENT_TEST_ID);
                    tmp_test->setValueForKey(tmp_previous_test_id, TESTS_HIERARCHY_PREVIOUS_TEST_ID);
                    tmp_test->readXml(in_xml_reader);
                    tmp_previous_test_id = tmp_test->getIdentifier();
                    tmp_test->setParent(this);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else
        {
            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
            in_xml_reader.skipCurrentElement();
        }
    }

    return true;
}


int Test::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int            tmp_result = NOERR;
    QString        tmp_test_id = QString(getIdentifier());
    const char        *tmp_parent_test_id = getValueForKey(TESTS_HIERARCHY_PARENT_TEST_ID);
    const char        *tmp_original_test_id = getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID);
    const char        *tmp_previous_test_id = getValueForKey(TESTS_HIERARCHY_PREVIOUS_TEST_ID);
    const char        *tmp_test_content_id = getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID);
    BasicTest        tmp_test_record;

    if (is_empty_string(getOriginalValueForKey(TESTS_HIERARCHY_TEST_ID)) == TRUE)
    {
        setValueForKey(NULL, TESTS_HIERARCHY_TEST_ID);

        if (projectVersion() != NULL)
        {
            setValueForKey(projectVersion()->getIdentifier(), TESTS_HIERARCHY_PROJECT_VERSION_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), TESTS_HIERARCHY_PROJECT_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), TESTS_HIERARCHY_VERSION);
        }

        if (is_empty_string(tmp_parent_test_id) == FALSE)
            setValueForKey(in_xml_datas.getNewTestIdentifier(projectVersion(), tmp_parent_test_id), TESTS_HIERARCHY_PARENT_TEST_ID);

        if (is_empty_string(tmp_original_test_id) == FALSE)
            setValueForKey(in_xml_datas.getNewTestIdentifier(projectVersion(), tmp_original_test_id), TESTS_HIERARCHY_ORIGINAL_TEST_ID);

        if (is_empty_string(tmp_previous_test_id) == FALSE)
            setValueForKey(in_xml_datas.getNewTestIdentifier(projectVersion(), tmp_previous_test_id), TESTS_HIERARCHY_PREVIOUS_TEST_ID);

        setValueForKey(in_xml_datas.getNewTestContentIdentifier(tmp_test_content_id), TESTS_HIERARCHY_TEST_CONTENT_ID);

        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_ID), TESTS_TABLE_ORIGINAL_TEST_ID);
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PARENT_TEST_ID), TESTS_TABLE_PARENT_TEST_ID);
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PREVIOUS_TEST_ID), TESTS_TABLE_PREVIOUS_TEST_ID);
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PROJECT_ID), TESTS_TABLE_PROJECT_ID);
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_PROJECT_VERSION_ID), TESTS_TABLE_PROJECT_VERSION_ID);
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), TESTS_TABLE_TEST_CONTENT_ID);
        tmp_test_record.setValueForKey(getValueForKey(TESTS_HIERARCHY_VERSION), TESTS_TABLE_VERSION);

        tmp_result = tmp_test_record.insertRecord();
        if (tmp_result == NOERR)
        {
            in_xml_datas.m_tests_dict.insert(tmp_test_id, this);

            setValueForKey(tmp_test_record.getIdentifier(), TESTS_HIERARCHY_TEST_ID);
            applyChanges();

            foreach(Test *tmp_test, getAllChilds())
            {
                tmp_test->saveFromXmlProjectDatas(in_xml_datas);
            }
        }
    }


    return tmp_result;
}


QList<Bug*> Test::loadBugs()
{
    net_session                *tmp_session = Session::instance().getClientSession();
    QList<Bug*>                tmp_bugs_list;

    net_session_print_query(tmp_session, "%s IN (SELECT %s FROM %s WHERE %s=%s)",
    BUGS_TABLE_EXECUTION_TEST_ID,
    EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID,
    EXECUTIONS_TESTS_TABLE_SIG,
    EXECUTIONS_TESTS_TABLE_TEST_ID,
    getIdentifier());

    tmp_bugs_list = Bug::loadRecordsList(tmp_session->m_last_query, BUGS_TABLE_CREATION_DATE);

    return tmp_bugs_list;
}

void Test::setDataFromTestContent(TestContent* in_test_content)
{
    if (in_test_content != NULL)
    {
        setValueForKey(in_test_content->getIdentifier(), TESTS_HIERARCHY_TEST_CONTENT_ID);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME), TESTS_HIERARCHY_SHORT_NAME);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID), TESTS_HIERARCHY_CATEGORY_ID);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL), TESTS_HIERARCHY_PRIORITY_LEVEL);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_VERSION), TESTS_HIERARCHY_CONTENT_VERSION);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_STATUS), TESTS_HIERARCHY_STATUS);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID), TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_TYPE), TESTS_HIERARCHY_TEST_CONTENT_TYPE);
        setValueForKey(in_test_content->getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATED), TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED);
    }
}



bool Test::isAutomatedGuiTest() const
{
    return (compare_values(getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED), TEST_CONTENT_TABLE_AUTOMATED_GUI) == 0);
}



bool Test::isAutomatedBatchTest() const
{
    return (compare_values(getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED), TEST_CONTENT_TABLE_AUTOMATED_BATCH) == 0);
}


}


