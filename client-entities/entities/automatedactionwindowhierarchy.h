/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef AUTOMATEDACTIONWINDOWHIERARCHY_H
#define AUTOMATEDACTIONWINDOWHIERARCHY_H

#include "entities/entity.h"

extern const char AUTOMATED_ACTION_WINDOW_HIERARCHY_TEXT[];
extern const char AUTOMATED_ACTION_WINDOW_HIERARCHY_CLASS[];
extern const char AUTOMATED_ACTION_WINDOW_HIERARCHY_IS_VISIBLE[];

extern const entity_def automated_action_window_hierarchy_table_def;

typedef struct _windows_hierarchy windows_hierarchy;

namespace Entities
{
class AutomatedActionWindowHierarchy :
    public Entity<AutomatedActionWindowHierarchy, &automated_action_window_hierarchy_table_def>
{

public:
    AutomatedActionWindowHierarchy();

#ifdef GUI_AUTOMATION_ACTIVATED
    static QList<AutomatedActionWindowHierarchy*> fromWindowHierarchyStructure(windows_hierarchy* in_window_hierarchy);
#endif

    ADD_NAMED_RELATION(ParentAutomatedActionWindowHierarchy, AutomatedActionWindowHierarchy, AutomatedActionWindowHierarchy);

};

}

#endif // AUTOMATEDACTIONWINDOWHIERARCHY_H
