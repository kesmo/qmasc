/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "requirement.h"
#include "test.h"
#include "requirementcontent.h"
#include "projectgrant.h"

#include "utilities.h"
#include "entities.h"
#include "session.h"
#include "xmlprojectdatas.h"

#include <QTextDocument>
#include <QIcon>
#include <QPixmap>

namespace Entities
{

/**
  Constructeur
**/
BasicRequirement::BasicRequirement() : Entity()
{
}

/**
  Constructeur
**/
Requirement::Requirement()
{
}


/**
  Constructeur
**/
Requirement::Requirement(ProjectVersion *in_project)
{
    setProjectVersion(in_project);
}


/**
  Constructeur
**/
Requirement::Requirement(Requirement *in_parent)
{
    setParent(in_parent);
    if (in_parent != NULL) {
        setProjectVersion(in_parent->projectVersion());
    }
}


/**
  Destructeur
**/
Requirement::~Requirement()
{
}


ProjectVersion* Requirement::projectVersion()
{
    return ProjectVersion::RequirementRelation::instance().getParent(this);
}


void Requirement::setProjectVersion(ProjectVersion* in_project_version)
{
    if (in_project_version != NULL) {
        ProjectVersion::RequirementRelation::instance().setParent(in_project_version, this);
        if (is_empty_string(getIdentifier())) {
            setValueForKey(projectVersion()->getIdentifier(), REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), REQUIREMENTS_HIERARCHY_PROJECT_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), REQUIREMENTS_HIERARCHY_VERSION);
        }
    }
}


Requirement* Requirement::parentRequirement()
{
    return getParent();
}

int Requirement::saveRecord()
{
    int            tmp_result = NOERR;
    BasicRequirement        tmp_requirement_record;

    tmp_requirement_record.setValueForKey(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID), REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID);
    tmp_requirement_record.setValueForKey(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID), REQUIREMENTS_TABLE_PREVIOUS_REQUIREMENT_ID);
    tmp_requirement_record.setValueForKey(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), REQUIREMENTS_TABLE_PROJECT_ID);
    tmp_requirement_record.setValueForKey(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID), REQUIREMENTS_TABLE_PROJECT_VERSION_ID);
    tmp_requirement_record.setValueForKey(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID), REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
    tmp_requirement_record.setValueForKey(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_VERSION), REQUIREMENTS_TABLE_VERSION);
    tmp_requirement_record.applyChanges();

    if (parentRequirement() != NULL) {
        if (is_empty_string(parentRequirement()->getIdentifier())) {
            tmp_result = parentRequirement()->saveRecord();
            if (tmp_result == NOERR)
                setValueForKey(parentRequirement()->getIdentifier(), REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);
            else
                return tmp_result;
        }
        else
            setValueForKey(parentRequirement()->getIdentifier(), REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);

        setValueForKey(parentRequirement()->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID), REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(parentRequirement()->getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), REQUIREMENTS_HIERARCHY_PROJECT_ID);
        setValueForKey(parentRequirement()->getValueForKey(REQUIREMENTS_HIERARCHY_VERSION), REQUIREMENTS_HIERARCHY_VERSION);
    }
    else
        setValueForKey(NULL, REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);

    if (projectVersion() != NULL) {
        setValueForKey(projectVersion()->getIdentifier(), REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), REQUIREMENTS_HIERARCHY_PROJECT_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), REQUIREMENTS_HIERARCHY_VERSION);
    }

    tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID), REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID);
    tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID), REQUIREMENTS_TABLE_PREVIOUS_REQUIREMENT_ID);
    tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), REQUIREMENTS_TABLE_PROJECT_ID);
    tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID), REQUIREMENTS_TABLE_PROJECT_VERSION_ID);
    tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID), REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
    tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_VERSION), REQUIREMENTS_TABLE_VERSION);

    if (is_empty_string(getIdentifier())) {
        tmp_result = tmp_requirement_record.insertRecord();
        if (tmp_result == NOERR) {
            setValueForKey(tmp_requirement_record.getIdentifier(), REQUIREMENTS_HIERARCHY_REQUIREMENT_ID);
            applyChanges();
        }
    }
    else {
        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_ID), REQUIREMENTS_TABLE_REQUIREMENT_ID);
        tmp_result = tmp_requirement_record.saveRecord();
        if (tmp_result == NOERR) {
            applyChanges();
        }
    }

    return tmp_result;
}


void Requirement::searchFieldWithValue(QList<Record*> *in_found_list, const char* in_field_name, const char* in_field_value, bool in_recursive, int in_comparison_value)
{
    int        tmp_comparison = 0;

    tmp_comparison = compare_values(getValueForKey(in_field_name), in_field_value);
    if ((tmp_comparison == 0 && (in_comparison_value == EqualTo || in_comparison_value == LowerOrEqualTo || in_comparison_value == UpperOrEqualTo))
        || (tmp_comparison < 0 && (in_comparison_value == LowerThan || in_comparison_value == LowerOrEqualTo))
        || (tmp_comparison > 0 && (in_comparison_value == UpperThan || in_comparison_value == UpperOrEqualTo))) {
        in_found_list->append(this);
    }

    if (in_recursive) {
        foreach(Requirement *tmp_child, getAllChilds())
        {
            tmp_child->searchFieldWithValue(in_found_list, in_field_name, in_field_value, in_recursive, in_comparison_value);
        }
    }
}


Requirement* Requirement::findRequirementWithId(const QList<Requirement*>& in_requirements_list, const char* in_requirement_id, bool in_recursive)
{
    return findRequirementWithValueForKey(in_requirements_list, in_requirement_id, REQUIREMENTS_HIERARCHY_REQUIREMENT_ID, in_recursive);
}


int Requirement::deleteRecord()
{
    int tmp_result = NOERR;

    if (is_empty_string(getIdentifier()) == FALSE) {
        BasicRequirement tmp_requirement;
        tmp_requirement.setValueForKey(getIdentifier(), REQUIREMENTS_TABLE_REQUIREMENT_ID);
        tmp_result = tmp_requirement.deleteRecord();
    }

    return tmp_result;
}


Requirement* Requirement::findRequirementWithValueForKey(const QList<Requirement*>& in_requirements_list, const char* in_value, const char* in_key, bool in_recursive)
{
    Requirement *tmp_found_requirement = NULL;

    foreach(Requirement *tmp_requirement, in_requirements_list)
    {
        if (compare_values(tmp_requirement->getValueForKey(in_key), in_value) == 0)
            return tmp_requirement;

        if (in_recursive) {
            tmp_found_requirement = findRequirementWithValueForKey(tmp_requirement->getAllChilds(), in_value, in_key, true);
            if (tmp_found_requirement != NULL)
                return tmp_found_requirement;
        }
    }

    return NULL;
}


void Requirement::writeXml(QXmlStreamWriter & in_xml_writer)
{
    in_xml_writer.writeStartElement("requirement");
    in_xml_writer.writeAttribute("id", getIdentifier());
    in_xml_writer.writeAttribute("requirementContentId", getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID));

    if (getAllChilds().count() > 0) {
        in_xml_writer.writeStartElement("requirements");
        foreach(Requirement *tmp_requirement, getAllChilds())
        {
            tmp_requirement->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
    }

    in_xml_writer.writeEndElement();
}


bool Requirement::readXml(QXmlStreamReader & in_xml_reader)
{
    Requirement    *tmp_requirement = NULL;

    QString            tmp_id = in_xml_reader.attributes().value("id").toString();
    QString            tmp_test_content_id = in_xml_reader.attributes().value("requirementContentId").toString();

    const char            *tmp_previous_requirement_id = NULL;

    setValueForKey(tmp_id.toStdString().c_str(), REQUIREMENTS_HIERARCHY_REQUIREMENT_ID);
    setValueForKey(tmp_test_content_id.toStdString().c_str(), REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID);

    while (in_xml_reader.readNextStartElement()) {
        if (in_xml_reader.name() == "requirements") {
            while (in_xml_reader.readNextStartElement()) {
                if (in_xml_reader.name() == "requirement") {
                    tmp_requirement = new Requirement(this);
                    tmp_requirement->setValueForKey(tmp_id.toStdString().c_str(), REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);
                    tmp_requirement->setValueForKey(tmp_previous_requirement_id, REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID);
                    tmp_requirement->readXml(in_xml_reader);
                    tmp_previous_requirement_id = tmp_requirement->getIdentifier();
                    setParent(tmp_requirement);
                }
                else {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else {
            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
            in_xml_reader.skipCurrentElement();
        }
    }

    return true;
}


int Requirement::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int            tmp_result = NOERR;
    QString        tmp_requirement_id = QString(getIdentifier());
    const char        *tmp_requirement_content_id = getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID);
    const char        *tmp_parent_requirement_id = getValueForKey(REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);
    const char        *tmp_previous_requirement_id = getValueForKey(REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID);
    BasicRequirement   tmp_requirement_record;

    if (is_empty_string(getOriginalValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_ID)) == TRUE) {
        setValueForKey(NULL, REQUIREMENTS_HIERARCHY_REQUIREMENT_ID);

        if (projectVersion() != NULL) {
            setValueForKey(projectVersion()->getIdentifier(), REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), REQUIREMENTS_HIERARCHY_PROJECT_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), REQUIREMENTS_HIERARCHY_VERSION);
        }

        if (is_empty_string(tmp_parent_requirement_id) == FALSE)
            setValueForKey(in_xml_datas.getNewRequirementIdentifier(projectVersion(), tmp_parent_requirement_id), REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID);

        if (is_empty_string(tmp_previous_requirement_id) == FALSE)
            setValueForKey(in_xml_datas.getNewRequirementIdentifier(projectVersion(), tmp_previous_requirement_id), REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID);

        setValueForKey(in_xml_datas.getNewRequirementContentIdentifier(tmp_requirement_content_id), REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID);

        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID), REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID);
        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID), REQUIREMENTS_TABLE_PREVIOUS_REQUIREMENT_ID);
        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_ID), REQUIREMENTS_TABLE_PROJECT_ID);
        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID), REQUIREMENTS_TABLE_PROJECT_VERSION_ID);
        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID), REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
        tmp_requirement_record.setValueForKey(getValueForKey(REQUIREMENTS_HIERARCHY_VERSION), REQUIREMENTS_TABLE_VERSION);

        tmp_result = tmp_requirement_record.insertRecord();
        if (tmp_result == NOERR) {
            in_xml_datas.m_requirements_dict.insert(tmp_requirement_id, this);

            setValueForKey(tmp_requirement_record.getIdentifier(), REQUIREMENTS_HIERARCHY_REQUIREMENT_ID);
            applyChanges();

            foreach(Requirement *tmp_requirement, getAllChilds())
            {
                tmp_requirement->saveFromXmlProjectDatas(in_xml_datas);
            }
        }
    }

    return tmp_result;
}


QList<Test*> Requirement::dependantsTests()
{
    net_session         *tmp_session = Session::instance().getClientSession();
    char                *tmp_query = tmp_session->m_last_query;
    char                ***tmp_query_results = NULL;
    unsigned long       tmp_rows_count = 0;
    unsigned long       tmp_columns_count = 0;

    Test       *tmp_test = NULL;
    QList<Test*>     tmp_final_tests_list;

    ProjectVersion*          tmp_project_version_hierarchy = projectVersion();


    net_session_print_query(tmp_session, "SELECT %s FROM %s WHERE %s=%s;"
                         , TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID
                         , TESTS_REQUIREMENTS_TABLE_SIG
                         , TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID
                         , getValueForKey(REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID));

    tmp_query_results = cl_run_sql(tmp_session, tmp_session->m_last_query, &tmp_rows_count, &tmp_columns_count);
    if (tmp_query_results != NULL) {
        for (unsigned long tmp_index = 0; tmp_index < tmp_rows_count; tmp_index++) {
            tmp_test = ProjectVersion::TestRelation::instance().getUniqueChildWithValueForKey(tmp_project_version_hierarchy, tmp_query_results[tmp_index][0], TESTS_HIERARCHY_TEST_CONTENT_ID);
            if (tmp_test != NULL) {
                while (tmp_test->original() != NULL && tmp_test->parentTest() != NULL) {
                    tmp_test = tmp_test->parentTest();
                }

                if (tmp_final_tests_list.indexOf(tmp_test) < 0)
                    tmp_final_tests_list.append(tmp_test);
            }
        }
        cl_free_rows_columns_array(&tmp_query_results, tmp_rows_count, tmp_columns_count);
    }

    return tmp_final_tests_list;
}


void Requirement::setDataFromRequirementContent(RequirementContent *in_requirement_content)
{
    if (in_requirement_content != NULL) {
        setValueForKey(in_requirement_content->getIdentifier(), REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID);
        setValueForKey(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME), REQUIREMENTS_HIERARCHY_SHORT_NAME);
        setValueForKey(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID), REQUIREMENTS_HIERARCHY_CATEGORY_ID);
        setValueForKey(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL), REQUIREMENTS_HIERARCHY_PRIORITY_LEVEL);
        setValueForKey(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_VERSION), REQUIREMENTS_HIERARCHY_CONTENT_VERSION);
        setValueForKey(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_STATUS), REQUIREMENTS_HIERARCHY_STATUS);
        setValueForKey(in_requirement_content->getValueForKey(REQUIREMENTS_CONTENTS_TABLE_ORIGINAL_REQUIREMENT_CONTENT_ID), REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID);
    }
}

bool Requirement::isModifiable()
{
    if (projectVersion() == NULL)
        return false;

    return projectVersion()->canWriteRequirements();
}

}
