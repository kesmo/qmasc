#ifndef TESTRESULT_ENTITY_H
#define TESTRESULT_ENTITY_H

#include "entity.h"

namespace Entities
{

class TestResult:
    public Entity<TestResult, &tests_results_table_def>
{
public:
    TestResult();
};

}

#endif // TESTRESULT_ENTITY_H
