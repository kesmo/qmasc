/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "action.h"
#include "utilities.h"
#include "testcontent.h"

namespace Entities
{
/**
  Constructeur
**/
Action::Action() : Entity()
{
}


Action::~Action()
{
}

int Action::saveRecord()
{
    int tmp_return = NOERR;

    if (is_empty_string(getIdentifier())) {
        TestContent *test_content = TestContent::ActionRelation::instance().getParent(this);
        if (test_content != NULL)
            setValueForKey(test_content->getIdentifier(), ACTIONS_TABLE_TEST_CONTENT_ID);

        tmp_return = Entity::saveRecord();
    }
    else
        tmp_return = Entity::saveRecord();

    return tmp_return;
}


bool Action::loadAssociatedActionsForVersion(const char *in_project_version, const char *in_original_test_content_id)
{
    QList<Action*>  tmp_associated_actions_list;
    const char      *tmp_original_test_content_id = NULL;

    TestContent *test_content = TestContent::ActionRelation::instance().getParent(this);

    if (is_empty_string(in_original_test_content_id) == FALSE)
        tmp_original_test_content_id = in_original_test_content_id;
    else if (test_content != NULL)
        tmp_original_test_content_id = test_content->getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID);

    if (is_empty_string(getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID)) == FALSE && is_empty_string(tmp_original_test_content_id) == FALSE) {
        m_associated_test_actions.clear();

        tmp_associated_actions_list = loadAssociatedTestActionsForVersion(in_project_version);
        if (tmp_associated_actions_list.count() > 0) {
            foreach(Action *tmp_associated_action, tmp_associated_actions_list)
            {
                if (!tmp_associated_action->appendAssociatedTestActionsToAction(m_associated_test_actions, tmp_original_test_content_id, in_project_version)) {
                    m_associated_test_actions.clear();
                    return false;
                }
            }
        }
    }

    return true;
}


QList<Action*> Action::loadAssociatedTestActionsForVersion(const char *in_project_version)
{
    QList<Action*>       tmp_actions;

    TestContent *tmp_test_content = TestContent::loadLastTestContentForVersion(getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID), in_project_version);

    if (tmp_test_content != NULL) {
        tmp_actions = TestContent::ActionRelation::instance().getChilds(tmp_test_content);
        delete tmp_test_content;
    }

    return tmp_actions;
}


bool Action::setAssociatedTest(Test *in_test)
{
    QList<Action*>  tmp_associated_actions_list;
    TestContent     *tmp_test_content = NULL;

    TestContent *test_content = TestContent::ActionRelation::instance().getParent(this);

    if (in_test != NULL &&
        compare_values(in_test->getValueForKey(TESTS_HIERARCHY_TEST_CONTENT_ID), getValueForKey(ACTIONS_TABLE_TEST_CONTENT_ID)) != 0 &&
        (test_content == NULL || compare_values(in_test->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID), test_content->getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID)) != 0)) {
        m_associated_test_actions.clear();
        setValueForKey(in_test->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID), ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);
        setValueForKey(in_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME), ACTIONS_TABLE_SHORT_NAME);

        tmp_test_content = TestContent::TestRelation::instance().getParent(in_test);
        if (tmp_test_content) {
            tmp_associated_actions_list = TestContent::ActionRelation::instance().getChilds(tmp_test_content);
            if (tmp_associated_actions_list.count() > 0) {
                foreach(Action *tmp_associated_action, tmp_associated_actions_list)
                {
                    if (!tmp_associated_action->appendAssociatedTestActionsToAction(m_associated_test_actions, test_content->getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID), in_test->getValueForKey(TESTS_HIERARCHY_VERSION))) {
                        m_associated_test_actions.clear();
                        setValueForKey(NULL, ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);
                        return false;
                    }
                }
            }
        }
    }
    else
        return false;

    return true;
}


bool Action::appendAssociatedTestActionsToAction(QList<Action*> & in_actions_list, const char *in_original_test_id, const char *in_project_version)
{
    QList<Action*>  tmp_associated_actions_list;

    TestContent *test_content = TestContent::ActionRelation::instance().getParent(this);

    if (test_content == NULL || compare_values(in_original_test_id, test_content->getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID)) != 0) {
        if (is_empty_string(getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID)) == FALSE) {
            if (compare_values(in_original_test_id, getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID)) != 0) {
                tmp_associated_actions_list = loadAssociatedTestActionsForVersion(in_project_version);
                if (tmp_associated_actions_list.count() > 0) {
                    foreach(Action *tmp_associated_action, tmp_associated_actions_list)
                    {
                        if (!tmp_associated_action->appendAssociatedTestActionsToAction(in_actions_list, in_original_test_id, in_project_version))
                            return false;
                    }
                }
            }
            else {
                return false;
            }
        }
        else
            in_actions_list.append(this);
    }
    else
        return false;

    return true;
}

QList<Action*> Action::associatedTestActions()
{
    return m_associated_test_actions;
}

}
