/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef REQUIREMENTCONTENT_H
#define REQUIREMENTCONTENT_H

#include "requirement.h"
#include "projectversion.h"
#include "testrequirement.h"
#include "executionrequirement.h"

namespace Entities
{
class XmlProjectDatas;
class TestRequirement;
class Requirement;
class CustomRequirementField;

class RequirementContent :
    public Entity<RequirementContent, &requirements_contents_table_def>
{
public:
    RequirementContent();

    RequirementContent* copy();
    int saveRecord();

    RequirementContent* previousRequirementContent();
    RequirementContent* nextRequirementContent();

    void writeXml(QXmlStreamWriter & in_xml_writer);
    bool readXml(QXmlStreamReader & in_xml_reader);
    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    static RequirementContent* loadLastRequirementContentForVersion(const char *in_original_requirement_content_id, const char *in_project_version);

    QString getPriorityLabel();

    ADD_RELATION(RequirementContent, Requirement, REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);
    ADD_RELATION(RequirementContent, ExecutionRequirement, EXECUTIONS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT__ID);
    ADD_RELATION(RequirementContent, CustomRequirementField, CUSTOM_REQUIREMENT_FIELDS_TABLE_REQUIREMENT_CONTENT_ID);
    ADD_RELATION(RequirementContent, TestRequirement, TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID);

};

}

#endif // REQUIREMENT_H
