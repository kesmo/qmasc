#ifndef FEATURECONTENT_H
#define FEATURECONTENT_H

#include "feature.h"
#include "rulecontent.h"
#include "requirementcontent.h"

namespace Entities
{
class ProjectVersion;

class FeatureContent :
    public Entity<FeatureContent, &features_contents_table_def>
{
public:
    FeatureContent();
    FeatureContent(ProjectVersion *in_project_version);

    void setProjectVersion(ProjectVersion* in_project_version);

    FeatureContent* copy() const;

    ADD_RELATION(FeatureContent, Feature, FEATURES_TABLE_FEATURE_CONTENT_ID);
    ADD_RELATION(FeatureContent, RuleContent, RULES_CONTENTS_TABLE_FEATURE_CONTENT_ID);
    ADD_RELATION(FeatureContent, RequirementContent, REQUIREMENTS_CONTENTS_TABLE_FEATURE_CONTENT_ID);
};
}

#endif // FEATURECONTENT_H
