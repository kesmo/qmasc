#ifndef GRAPHICTEST_H
#define GRAPHICTEST_H

#include "entities/entity.h"

namespace Entities
{

class GraphicTest :
    public Entity<GraphicTest, &graphics_tests_table_def>
{
public:
    GraphicTest();
};

}


#endif // GRAPHICTEST_H
