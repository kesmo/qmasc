#ifndef GRAPHICITEM_H
#define GRAPHICITEM_H

#include "entities/entity.h"
#include "entities-def.h"

#include "graphicif.h"
#include "graphiclink.h"
#include "graphicloop.h"
#include "graphicswitch.h"
#include "graphictest.h"
#include "graphicpoint.h"

namespace Entities
{
class GraphicItem :
    public Entity<GraphicItem, &graphics_items_table_def>
{
public:

    enum Type {
        If = GRAPHICS_ITEMS_TYPE_IF,
        Link = GRAPHICS_ITEMS_TYPE_LINK,
        Loop = GRAPHICS_ITEMS_TYPE_LOOP,
        Switch = GRAPHICS_ITEMS_TYPE_SWITCH,
        Test = GRAPHICS_ITEMS_TYPE_TEST,
        Point = GRAPHICS_ITEMS_TYPE_POINT
    } ;

    GraphicItem();

    const float getX() const;
    const float getY() const;
    const int getZValue() const;

    const Type getType() const;

    ADD_NAMED_RELATION(ParentGraphicItem, GraphicItem, GraphicItem, GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID);
    ADD_RELATION(GraphicItem, GraphicPoint, GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID, GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID);
    ADD_NAMED_RELATION(LinkGraphicItem, GraphicItem, GraphicPoint, GRAPHICS_POINTS_TABLE_LINK_GRAPHIC_ITEM_ID);
    ADD_NAMED_RELATION(ConnectedGraphicItem, GraphicItem, GraphicPoint, GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID);
    ADD_RELATION(GraphicItem, GraphicIf, GRAPHICS_IFS_TABLE_GRAPHIC_ITEM_ID);
    ADD_RELATION(GraphicItem, GraphicLink, GRAPHICS_LINKS_TABLE_GRAPHIC_ITEM_ID);
    ADD_RELATION(GraphicItem, GraphicLoop, GRAPHICS_LOOPS_TABLE_GRAPHIC_ITEM_ID);
    ADD_RELATION(GraphicItem, GraphicSwitch, GRAPHICS_SWITCHES_TABLE_GRAPHIC_ITEM_ID);
    ADD_RELATION(GraphicItem, GraphicTest, GRAPHICS_TESTS_TABLE_GRAPHIC_ITEM_ID);

};

}


#endif // GRAPHICITEM_H
