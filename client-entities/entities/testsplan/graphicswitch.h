#ifndef GRAPHICSWITCH_H
#define GRAPHICSWITCH_H

#include "entities/entity.h"

namespace Entities
{
class GraphicSwitch :
    public Entity<GraphicSwitch, &graphics_switches_table_def>
{
public:
    GraphicSwitch();
};

}

#endif // GRAPHICSWITCH_H
