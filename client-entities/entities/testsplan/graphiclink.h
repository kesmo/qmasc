#ifndef GRAPHICLINK_H
#define GRAPHICLINK_H

#include "entities/entity.h"

namespace Entities
{

class GraphicLink :
    public Entity<GraphicLink, &graphics_links_table_def>
{
public:
    GraphicLink();
};

}


#endif // GRAPHICLINK_H
