#ifndef GRAPHICPOINT_H
#define GRAPHICPOINT_H

#include "entities/entity.h"

namespace Entities
{
class GraphicItem;

class GraphicPoint :
    public Entity<GraphicPoint, &graphics_points_table_def>
{
public:
    GraphicPoint();

    ADD_NAMED_RELATION(PreviousGraphicPoint, GraphicPoint, GraphicPoint, GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID);
};

}

#endif // GRAPHICPOINT_H
