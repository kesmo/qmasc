#include "graphicitem.h"

#include "graphicif.h"
#include "graphiclink.h"
#include "graphicloop.h"
#include "graphicswitch.h"
#include "graphictest.h"
#include "graphicpoint.h"

namespace Entities
{

GraphicItem::GraphicItem()
{
}

const float GraphicItem::getX() const{
    return atof(getValueForKey(GRAPHICS_ITEMS_TABLE_X));
}


const float GraphicItem::getY() const{
    return atof(getValueForKey(GRAPHICS_ITEMS_TABLE_Y));
}


const int GraphicItem::getZValue() const{
    const char* zvalue = getValueForKey(GRAPHICS_ITEMS_TABLE_Z_VALUE);

    if (is_empty_string(zvalue) == FALSE)
        return atoi(zvalue);

    return 0;
}


const GraphicItem::Type GraphicItem::getType() const{
    return (Type)atoi(getValueForKey(GRAPHICS_ITEMS_TABLE_TYPE));
}

}
