#ifndef GRAPHICIF_H
#define GRAPHICIF_H

#include "entities/entity.h"

namespace Entities
{

class GraphicIf :
    public Entity<GraphicIf, &graphics_ifs_table_def>
{
public:
    GraphicIf();
};

}

#endif // GRAPHICIF_H
