#ifndef GRAPHICLOOP_H
#define GRAPHICLOOP_H

#include "entities/entity.h"

namespace Entities
{

class GraphicLoop :
    public Entity<GraphicLoop, &graphics_loops_table_def>
{
public:
    GraphicLoop();
};

}


#endif // GRAPHICLOOP_H
