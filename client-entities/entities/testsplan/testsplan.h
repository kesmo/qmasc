#ifndef TESTSPLAN_H
#define TESTSPLAN_H

#include "graphicitem.h"
#include "entities/executioncampaign.h"

namespace Entities
{

class TestsPlan:
    public Entity<TestsPlan, &tests_plans_table_def>
{
public:
    TestsPlan();

    ADD_RELATION(TestsPlan, GraphicItem, GRAPHICS_ITEMS_TABLE_TEST_PLAN_ID);
    ADD_RELATION(TestsPlan, GraphicPoint, GRAPHICS_POINTS_TABLE_TEST_PLAN_ID, GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID);
    ADD_RELATION(TestsPlan, GraphicTest, GRAPHICS_TESTS_TABLE_TEST_PLAN_ID);
    ADD_RELATION(TestsPlan, GraphicIf, GRAPHICS_IFS_TABLE_TEST_PLAN_ID);
    ADD_RELATION(TestsPlan, GraphicSwitch, GRAPHICS_SWITCHES_TABLE_TEST_PLAN_ID);
    ADD_RELATION(TestsPlan, GraphicLoop, GRAPHICS_LOOPS_TABLE_TEST_PLAN_ID);
    ADD_RELATION(TestsPlan, GraphicLink, GRAPHICS_LINKS_TABLE_TEST_PLAN_ID);
    ADD_RELATION(TestsPlan, ExecutionCampaign, EXECUTIONS_CAMPAIGNS_TABLE_TEST_PLAN_ID);

};

}


#endif // TESTSPLAN_H
