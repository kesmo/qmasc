/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef REQUIREMENT_H
#define REQUIREMENT_H

#include "customrequirementfield.h"
#include "hierarchyentity.h"

#include <QXmlStreamWriter>

namespace Entities
{
class RequirementContent;
class ProjectVersion;
class XmlProjectDatas;
class Test;

class BasicRequirement :
    public Entity<BasicRequirement, &requirements_table_def>
{
public:
    BasicRequirement();
};


class Requirement :
    public HierarchyEntity<Requirement, &requirements_hierarchy_def, REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID, REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID>
{

public:
    Requirement();
    Requirement(ProjectVersion *in_project);
    Requirement(Requirement *in_parent);
    ~Requirement();

    ProjectVersion* projectVersion();
    void setProjectVersion(ProjectVersion* in_project_version);

    Requirement* parentRequirement();

    static Requirement* findRequirementWithId(const QList<Requirement*>& in_requirements_list, const char* in_requirement_id, bool in_recursive = true);
    static Requirement* findRequirementWithValueForKey(const QList<Requirement*>& in_requirements_list, const char* in_value, const char* in_key, bool in_recursive = true);

    void searchFieldWithValue(QList<Record*> *in_found_list, const char* in_field_name, const char* in_field_value, bool in_recursive, int in_comparison_value = Record::EqualTo);

    bool isModifiable();

    int saveRecord();
    int deleteRecord();

    void writeXml(QXmlStreamWriter & in_xml_writer);
    bool readXml(QXmlStreamReader & in_xml_reader);
    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    QList<Test*> dependantsTests();

    void setDataFromRequirementContent(RequirementContent *in_requirement_content);
};

}

#endif // REQUIREMENT_H
