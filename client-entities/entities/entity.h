#ifndef ENTITY_H
#define ENTITY_H

#include "record.h"
#include "relation.h"

namespace Entities
{

template <class E, const entity_def *D>
class Entity :
    public Record
{
public:
    Entity() : Record(D) {
#if _MSC_VER >= 1900 // > Visual Studio 2015 version 14.0
        static_assert(std::is_base_of<Entity, E>::value, "E template parameter must be an Entity");
#endif
        QString id(getIdentifier());
        m_entities.insert(id, this);
    }

    virtual ~Entity() {
        QString id(getIdentifier());
        if (m_entities.remove(id, this) == 0)
            m_entities.remove(QString(), this);
    }

    virtual void applyChanges() Q_DECL_OVERRIDE {
        QString oldid;
        Record::applyChanges();
        QString newid(getIdentifier());

        if (oldid != newid) {
            m_entities.remove(oldid, this);
        }

        if (!m_entities.contains(newid, this)) {
            if (m_entities.count(newid) > 0) {
                qWarning() << Q_FUNC_INFO << newid << "already added from other object" << m_entities.count(newid);
            }
            m_entities.insert(newid, this);
        }
    }

    static void logAvailableEntities() {
        foreach(QString id, m_entities.uniqueKeys()) {
            qDebug() << D->m_entity_name << "Id:" << id << ", count:" << m_entities.count(id);
        }
    }

    static QList<Record*> entities() {
        QList<Record*> result;
        foreach(Entity* entity, m_entities.values()) {
            result.append(entity);
        }
        return result;
    }

    static E *loadEntity(const QString &id, int *return_code = nullptr, bool deleteOnError = true) {
        E *entity = new E();
        int rc = entity->loadRecord(id.toStdString().c_str());
        if (rc != NOERR && deleteOnError) {
            delete entity;
            entity = nullptr;
        }

        if (return_code) {
            *return_code = rc;
        }

        return entity;
    }

    static E *getEntity(const QString &id, int *return_code = nullptr, bool deleteOnError = true) {
        QList<Entity*> entities = m_entities.values(id);
        E *entity = nullptr;
        if (!entities.isEmpty()) {
            if (entities.count() > 1) {
                qWarning() << Q_FUNC_INFO << id << "multiple entities for same id, count:" << entities.count();
                return nullptr;
            }

            entity = dynamic_cast<E*>(entities.first());
            if (entity->hasChangedValues()) {
                qWarning() << Q_FUNC_INFO << id << "entity has changed values";
                return nullptr;
            }
        }
        else {
            entity = loadEntity(id, return_code, deleteOnError);
        }

        return entity;
    }

    static QList<E*> loadRecordsList(const char* in_where_clause = NULL,
                                     const char* in_order_by_clause = NULL,
                                     net_callback_fct *in_callback = NULL) {
        return loadRecords<E>(Session::instance().getClientSession(), D, in_where_clause, in_order_by_clause, in_callback);
    }

    static QList<E*> loadAndOrderRecordsList(const char *in_previous_key, const char* in_where_clause = NULL, const char* in_order_by_clause = NULL, net_callback_fct *in_callback = NULL) {
        QList<E*>   tmp_return_list = Record::loadRecords<E>(Session::instance().getClientSession(), D, in_where_clause, in_order_by_clause, in_callback);
        if (in_previous_key)
            return orderedRecords(tmp_return_list, in_previous_key);
        else
            return tmp_return_list;
    }


    static int indexForKey(const char *in_key) {
        for (unsigned int tmp_index = 0; tmp_index < D->m_entity_columns_count; ++tmp_index)
        {
            if (compare_values(D->m_entity_columns_names[tmp_index], in_key) == 0)
                return tmp_index;
        }
    
        return -1;
    }
    
    static E* findEntityWithValueForKey(const QList<E*> &in_entities_list, const char *in_value, const char *in_key) {
        foreach(E* tmp_entity, in_entities_list) {
            if (tmp_entity && compare_values(tmp_entity->getValueForKey(in_key), in_value) == 0)
                return tmp_entity;
        }
    
        return NULL;
    }
    
    static QList<E*> findEntitiesWithValueForKey(const QList<E*> &in_entities_list, const char *in_value, const char *in_key) {
        QList<E*> resultList;
    
        foreach(E* tmp_entity, in_entities_list) {
            if (tmp_entity && compare_values(tmp_entity->getValueForKey(in_key), in_value) == 0)
                resultList.append(tmp_entity);
        }
    
        return resultList;
    }

    static const entity_def* getEntityDefinition() {
        return D;
    }

private:
    static QMultiHash<QString, Entity<E, D>*> m_entities;
};

template <class E, const entity_def *D>
QMultiHash<QString, Entity<E, D>*> Entity<E, D>::m_entities;

}

#endif // ENTITY_H
