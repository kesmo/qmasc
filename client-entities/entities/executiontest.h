/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef EXECUTIONTEST_H
#define EXECUTIONTEST_H

#include "hierarchyentity.h"
#include "executionaction.h"
#include "executiontestparameter.h"
#include "bug.h"

#include <QColor>

namespace Entities
{
class ExecutionTest;
class ExecutionCampaign;
class Test;

class ExecutionTest :
    public HierarchyEntity<ExecutionTest, &executions_tests_table_def, EXECUTIONS_TESTS_TABLE_PREVIOUS_EXECUTION_TEST_ID, EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID>
{

public:
    static QColor OK_COLOR;
    static QColor KO_COLOR;
    static QColor INCOMPLETED_COLOR;
    static QColor BY_PASSED_COLOR;

    static void initDefaultColors(QColor in_ok, QColor in_ko, QColor in_incomplete, QColor in_bypass){
        OK_COLOR = in_ok;
        KO_COLOR = in_ko;
        INCOMPLETED_COLOR = in_incomplete;
        BY_PASSED_COLOR = in_bypass;
    }

    static const QColor & okColor(){return OK_COLOR;}
    static const QColor & koColor(){return KO_COLOR;}
    static const QColor & incompleteColor(){return INCOMPLETED_COLOR;}
    static const QColor & byPassedColor(){return BY_PASSED_COLOR;}

    ExecutionTest();
    ExecutionTest(ExecutionCampaign *in_execution_campaign);
    ~ExecutionTest();

    ExecutionCampaign* executionCampaign();
    void setExecutionCampaign(ExecutionCampaign *in_execution_campaign);

    void createExecutionTestHierarchyForTest(Test *in_test);

    Test* projectTest();
    void setProjectTest(Test *in_project_test);

    int saveExecutionsActions();
    const QList<ExecutionAction*>& actions();
    ExecutionAction* actionAtIndex(int in_index);

    int saveExecutionsParameters();
    QList<ExecutionTestParameter *> inheritedParameters();
    const QList<ExecutionTestParameter*>& parameters();
    ExecutionTestParameter* parameterAtIndex(int in_index);
    void addExecutionParameter(ExecutionTestParameter* in_parameter);

    void updateTestResult(bool in_recursive = true);

    float executionCoverageRate();
    float executionValidatedRate();
    float executionInValidatedRate();
    float executionBypassedRate();
    float executionIncompleteRate();
    float executionRateForResults(const char* in_test_result_id, const char* in_action_result_id);
    int executionCountForResult(const char *in_result);

    QString toFragmentHtml(QString suffix);

    int saveRecord();

    QList<Bug*> loadBugs();

    int indexForActionWithValueForKey(const char *in_value, const char *in_key);

    bool hasChangedValues();

    void synchronizeFromTestDatas(QList<Test*> in_tests_list);

    ADD_RELATION(ExecutionTest, ExecutionAction, EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID, EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID);
    ADD_RELATION(ExecutionTest, Bug, BUGS_TABLE_EXECUTION_TEST_ID);
    ADD_RELATION(ExecutionTest, ExecutionTestParameter, EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_ID);
};
}

#endif // EXECUTIONTEST_H
