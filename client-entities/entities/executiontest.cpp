/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "executiontest.h"
#include "executionaction.h"
#include "executioncampaign.h"
#include "executioncampaignparameter.h"
#include "executiontestparameter.h"
#include "session.h"
#include "action.h"
#include "test.h"
#include "testcontent.h"

#include <QTextDocument>

namespace Entities
{

QColor ExecutionTest::OK_COLOR;
QColor ExecutionTest::KO_COLOR;
QColor ExecutionTest::INCOMPLETED_COLOR;
QColor ExecutionTest::BY_PASSED_COLOR;

ExecutionTest::ExecutionTest()
{
}


ExecutionTest::ExecutionTest(ExecutionCampaign *in_execution_campaign)
{
    setExecutionCampaign(in_execution_campaign);
}

/**
  Destructeur
**/
ExecutionTest::~ExecutionTest()
{
}

ExecutionCampaign* ExecutionTest::executionCampaign()
{
    return ExecutionCampaign::ExecutionTestRelation::instance().getParent(this);
}

void ExecutionTest::setExecutionCampaign(ExecutionCampaign *in_execution_campaign)
{
    ExecutionCampaign::ExecutionTestRelation::instance().addChild(in_execution_campaign, this);
}

Test* ExecutionTest::projectTest()
{
    return Test::ExecutionTestRelation::instance().getParent(this);
}

void ExecutionTest::setProjectTest(Test *in_project_test)
{
    Test::ExecutionTestRelation::instance().setParent(in_project_test, this);
    if (in_project_test != NULL)
        setValueForKey(in_project_test->getIdentifier(), EXECUTIONS_TESTS_TABLE_TEST_ID);
}

void ExecutionTest::createExecutionTestHierarchyForTest(Test *in_test)
{
    ExecutionTest        *tmp_execution_test = NULL;
    ExecutionAction        *tmp_execution_action = NULL;
    QList<Action*>           tmp_actions;
    Test               *tmp_test = NULL;

    if (in_test != NULL) {
        setProjectTest(in_test);

        setValueForKey(CLIENT_MACRO_NOW, EXECUTIONS_TESTS_TABLE_EXECUTION_DATE);
        setValueForKey(EXECUTION_TEST_BYPASSED, EXECUTIONS_TESTS_TABLE_RESULT_ID);

        auto childs = in_test->getAllChilds();
        foreach(Test *tmp_child_test, childs)
        {
            tmp_execution_test = new ExecutionTest(executionCampaign());
            addChild(tmp_execution_test);
            tmp_execution_test->createExecutionTestHierarchyForTest(tmp_child_test);
        }

        Test *tmp_project_test = Test::ExecutionTestRelation::instance().getParent(this);
        if (tmp_project_test != NULL) {
            if (tmp_project_test->original() != NULL)
                tmp_test = tmp_project_test->original();
            else
                tmp_test = tmp_project_test;

            TestContent *test_content = TestContent::TestRelation::instance().getParent(tmp_project_test);
            if (test_content)
                tmp_actions = TestContent::ActionRelation::instance().getChilds(test_content);

            foreach(Action *tmp_action, tmp_actions) {
                tmp_action->loadAssociatedActionsForVersion(tmp_test->getValueForKey(TESTS_HIERARCHY_VERSION), tmp_test->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID));
                if (tmp_action->associatedTestActions().count() > 0) {
                    foreach(Action *tmp_associated_action, tmp_action->associatedTestActions()) {
                        tmp_execution_action = new ExecutionAction();
                        tmp_execution_action->setValueForKey(EXECUTION_ACTION_BYPASSED, EXECUTIONS_ACTIONS_TABLE_RESULT_ID);
                        Action::ExecutionActionRelation::instance().setParent(tmp_associated_action, tmp_execution_action);
                        ExecutionTest::ExecutionActionRelation::instance().appendChild(this, tmp_execution_action);
                    }
                }
                else {
                    tmp_execution_action = new ExecutionAction();
                    tmp_execution_action->setValueForKey(EXECUTION_ACTION_BYPASSED, EXECUTIONS_ACTIONS_TABLE_RESULT_ID);
                    Action::ExecutionActionRelation::instance().setParent(tmp_action, tmp_execution_action);
                    ExecutionTest::ExecutionActionRelation::instance().appendChild(this, tmp_execution_action);
                }
            }

            qDeleteAll(tmp_actions);
        }
    }
}


/**
  Enregistrer les actions
**/
int ExecutionTest::saveExecutionsActions()
{
    int                 tmp_result = NOERR;
    ExecutionAction   *tmp_previous_action = NULL;
    ExecutionAction   *tmp_action = NULL;

    const QList<ExecutionAction*>& tmp_actions_list = actions();

    for (int tmp_index = 0; tmp_index < tmp_actions_list.count() && tmp_result == NOERR; tmp_index++) {
        tmp_action = tmp_actions_list[tmp_index];
        if (tmp_action != NULL) {
            tmp_action->setValueForKey(getIdentifier(), EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID);

            if (tmp_previous_action != NULL)
                tmp_action->setValueForKey(tmp_previous_action->getIdentifier(), EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID);
            else
                tmp_action->setValueForKey(NULL, EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID);

            tmp_result = tmp_action->saveRecord();
        }

        tmp_previous_action = tmp_action;
    }

    return tmp_result;
}


const QList<ExecutionAction*>& ExecutionTest::actions()
{
    return ExecutionTest::ExecutionActionRelation::instance().getChilds(this);
}

ExecutionAction* ExecutionTest::actionAtIndex(int in_index)
{
    return ExecutionTest::ExecutionActionRelation::instance().getChildAtIndex(this, in_index);
}

/**
  Enregistrer les paramètres d'exécutions
**/
int ExecutionTest::saveExecutionsParameters()
{
    int                tmp_result = NOERR;
    ExecutionTestParameter   *tmp_parameter = NULL;

    const QList<ExecutionTestParameter*>& tmp_parameters_list = parameters();

    for (int tmp_index = 0; tmp_index < tmp_parameters_list.count() && tmp_result == NOERR; tmp_index++) {
        tmp_parameter = tmp_parameters_list[tmp_index];
        if (tmp_parameter != NULL) {
            tmp_parameter->setValueForKey(getIdentifier(), EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_ID);
            tmp_result = tmp_parameter->saveRecord();
        }
    }

    return tmp_result;
}

void ExecutionTest::updateTestResult(bool in_recursive)
{
    if (executionBypassedRate() == 1.0)
        setValueForKey(EXECUTION_TEST_BYPASSED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
    else if (executionValidatedRate() == 1.0)
        setValueForKey(EXECUTION_TEST_VALIDATED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
    else if (executionInValidatedRate() > 0.0)
        setValueForKey(EXECUTION_TEST_INVALIDATED, EXECUTIONS_TESTS_TABLE_RESULT_ID);
    else if (executionCoverageRate() < 1.0)
        setValueForKey(EXECUTION_TEST_INCOMPLETED, EXECUTIONS_TESTS_TABLE_RESULT_ID);

    if (in_recursive) {
        foreach(ExecutionTest* tmp_child_execution_test, getAllChilds())
        {
            tmp_child_execution_test->updateTestResult(in_recursive);
        }
    }
}


int ExecutionTest::saveRecord()
{
    int tmp_result = NOERR;

    if (is_empty_string(getIdentifier())) {
        // Associer le parent
        ExecutionTest *parent = getParent();
        if (parent != NULL) {
            if (is_empty_string(parent->getIdentifier())) {
                tmp_result = parent->saveRecord();
                if (tmp_result == NOERR)
                    setValueForKey(parent->getIdentifier(), EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID);
                else
                    return tmp_result;
            }
            else
                setValueForKey(parent->getIdentifier(), EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID);
        }
        else
            setValueForKey(NULL, EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID);

        // Associer le test du projet
        Test *tmp_project_test = Test::ExecutionTestRelation::instance().getParent(this);
        if (tmp_project_test != NULL) {
            if (is_empty_string(tmp_project_test->getIdentifier())) {
                tmp_result = tmp_project_test->saveRecord();
                if (tmp_result == NOERR)
                    setValueForKey(tmp_project_test->getIdentifier(), EXECUTIONS_TESTS_TABLE_TEST_ID);
                else
                    return tmp_result;
            }
            else
                setValueForKey(tmp_project_test->getIdentifier(), EXECUTIONS_TESTS_TABLE_TEST_ID);
        }
        else
            setValueForKey(NULL, EXECUTIONS_TESTS_TABLE_TEST_ID);

        //Associer l'execution de la campagne
        if (executionCampaign() != NULL) {
            setValueForKey(executionCampaign()->getIdentifier(), EXECUTIONS_TESTS_TABLE_EXECUTION_CAMPAIGN_ID);
        }

        if (tmp_result == NOERR)
            tmp_result = Entity::insertRecord();
    }
    else
        tmp_result = Entity::saveRecord();

    if (tmp_result == NOERR) {
        // Sauvegarder les test enfants
        tmp_result = saveChilds();
        if (tmp_result == NOERR) {
            // Sauvegarder les actions
            tmp_result = saveExecutionsActions();
            if (tmp_result == NOERR) {
                // Sauvegarder les paramètres d'exécutions
                tmp_result = saveExecutionsParameters();
            }

        }

    }

    return tmp_result;
}


float ExecutionTest::executionCoverageRate()
{
    return (1.0 - executionBypassedRate());
}


float ExecutionTest::executionRateForResults(const char* in_test_result_id, const char* in_action_result_id)
{
    float    tmp_count = 0;

    float    tmp_childs_result = 0;
    float    tmp_actions_result = 0;

    float    tmp_actions_executions_count = 0;

    float    tmp_result = 0;

    const char    *tmp_result_id = NULL;
    const QList<ExecutionAction*>& tmp_actions_list = actions();

    if (getAllChilds().isEmpty() && tmp_actions_list.isEmpty()) {
        tmp_result_id = getValueForKey(EXECUTIONS_TESTS_TABLE_RESULT_ID);
        if (is_empty_string(tmp_result_id) == FALSE && compare_values(tmp_result_id, in_test_result_id) == 0)
            return 1;
        else
            return 0;
    }

    if (getAllChilds().isEmpty() == false) {
        tmp_count += 1;
        foreach(ExecutionTest *tmp_child_execution_test, getAllChilds())
        {
            tmp_childs_result += tmp_child_execution_test->executionRateForResults(in_test_result_id, in_action_result_id);
        }

        tmp_childs_result = tmp_childs_result / getAllChilds().count();
    }

    if (tmp_actions_list.isEmpty() == false) {
        tmp_count += 1;
        foreach(ExecutionAction *tmp_execution_action, tmp_actions_list)
        {
            tmp_result_id = tmp_execution_action->getValueForKey(EXECUTIONS_ACTIONS_TABLE_RESULT_ID);
            if (is_empty_string(tmp_result_id) == FALSE && compare_values(tmp_result_id, in_action_result_id) == 0) {
                tmp_actions_executions_count++;
            }
        }
        tmp_actions_result = (tmp_actions_executions_count / tmp_actions_list.count());
    }

    tmp_result = (tmp_childs_result + tmp_actions_result) / tmp_count;

    return tmp_result;

}




float ExecutionTest::executionValidatedRate()
{
    return executionRateForResults(EXECUTION_TEST_VALIDATED, EXECUTION_ACTION_VALIDATED);
}


float ExecutionTest::executionInValidatedRate()
{
    return executionRateForResults(EXECUTION_TEST_INVALIDATED, EXECUTION_ACTION_INVALIDATED);
}


float ExecutionTest::executionBypassedRate()
{
    return executionRateForResults(EXECUTION_TEST_BYPASSED, EXECUTION_ACTION_BYPASSED);
}


float ExecutionTest::executionIncompleteRate()
{
    return executionRateForResults(EXECUTION_TEST_INCOMPLETED, EXECUTION_ACTION_BYPASSED);
}


int ExecutionTest::executionCountForResult(const char *in_result)
{
    int tmp_count = 0;

    if (getAllChilds().isEmpty() == false) {
        foreach(ExecutionTest *tmp_child_execution_test, getAllChilds())
        {
            tmp_count += tmp_child_execution_test->executionCountForResult(in_result);
        }
    }
    else {
        if (compare_values(getValueForKey(EXECUTIONS_TESTS_TABLE_RESULT_ID), in_result) == 0)
            tmp_count = 1;
    }

    return tmp_count;
}


QString ExecutionTest::toFragmentHtml(QString suffix)
{
    QString        tmp_html_content = QString();
    int            tmp_index = 0;
    QString        tmp_coverage_rate = "", validated_rate = "", invalidated_rate = "", bypassed_rate = "";

    float    tmp_test_execution_coverage = executionCoverageRate() * 100;
    float    tmp_test_execution_validated = executionValidatedRate() * 100;
    float    tmp_test_execution_invalidated = executionInValidatedRate() * 100;
    float    tmp_test_execution_bypassed = executionBypassedRate() * 100;

    if (tmp_test_execution_coverage > 0)
        tmp_coverage_rate = QString::number(tmp_test_execution_coverage, 'f', 0) + " %";

    if (tmp_test_execution_validated > 0)
        validated_rate = QString::number(tmp_test_execution_validated, 'f', 0) + " %";

    if (tmp_test_execution_invalidated > 0)
        invalidated_rate = QString::number(tmp_test_execution_invalidated, 'f', 0) + " %";

    if (tmp_test_execution_bypassed > 0)
        bypassed_rate = QString::number(tmp_test_execution_bypassed, 'f', 0) + " %";

    Test *tmp_project_test = Test::ExecutionTestRelation::instance().getParent(this);
    tmp_html_content += "<tr><td>" + suffix + " " + (tmp_project_test ? QString(tmp_project_test->getValueForKey(TESTS_HIERARCHY_SHORT_NAME)) : QString()) + "</td>";
    tmp_html_content += "<td align=\"right\">" + tmp_coverage_rate + "</td>";
    tmp_html_content += "<td align=\"right\">" + validated_rate + "</td>";
    tmp_html_content += "<td align=\"right\">" + invalidated_rate + "</td>";
    tmp_html_content += "<td align=\"right\">" + bypassed_rate + "</td></tr>";

    foreach(ExecutionTest *tmp_execution_test, getAllChilds())
    {
        tmp_index++;
        tmp_html_content += tmp_execution_test->toFragmentHtml(suffix + QString::number(tmp_index) + ".");
    }

    return tmp_html_content;
}


QList<Bug*> ExecutionTest::loadBugs()
{
    QList<Bug*>                tmp_bugs_list;

    net_session_print_query(Session::instance().getClientSession(), "%s=%s", BUGS_TABLE_EXECUTION_TEST_ID, getIdentifier());

    tmp_bugs_list = Bug::loadRecordsList(Session::instance().getClientSession()->m_last_query, BUGS_TABLE_CREATION_DATE);
    return tmp_bugs_list;
}



int ExecutionTest::indexForActionWithValueForKey(const char *in_value, const char *in_key)
{
    int        tmp_index = 0;
    const QList<ExecutionAction*>& tmp_actions_list = actions();

    for (tmp_index = 0; tmp_index < tmp_actions_list.count(); tmp_index++) {
        if (compare_values(tmp_actions_list[tmp_index]->getValueForKey(in_key), in_value) == 0)
            return tmp_index;
    }

    return -1;
}


QList<ExecutionTestParameter *> ExecutionTest::inheritedParameters()
{
    QList<ExecutionTestParameter*>          tmp_params_list = QList<ExecutionTestParameter*>(parameters());
    ExecutionTest                           *tmp_parent_test = getParent();
    ExecutionTestParameter                  *tmp_param = NULL;

    while (tmp_parent_test != NULL) {
        foreach(ExecutionTestParameter *tmp_parent_param, tmp_parent_test->parameters())
        {
            tmp_param = Parameter::parameterForParamName<ExecutionTestParameter>(tmp_params_list, tmp_parent_param->name());
            if (tmp_param == NULL)
                tmp_params_list.append(tmp_parent_param);
        }
        tmp_parent_test = tmp_parent_test->getParent();
    }

    return tmp_params_list;
}

const QList<ExecutionTestParameter*>& ExecutionTest::parameters()
{
    return ExecutionTest::ExecutionTestParameterRelation::instance().getChilds(this);
}

ExecutionTestParameter* ExecutionTest::parameterAtIndex(int in_index)
{
    return ExecutionTest::ExecutionTestParameterRelation::instance().getChildAtIndex(this, in_index);
}

void ExecutionTest::addExecutionParameter(ExecutionTestParameter* in_parameter)
{
    ExecutionTest::ExecutionTestParameterRelation::instance().addChild(this, in_parameter);
}

bool ExecutionTest::hasChangedValues()
{
    if (!Entity::hasChangedValues()) {
        foreach(ExecutionTest *tmp_test, getAllChilds())
        {
            if (tmp_test->hasChangedValues())
                return true;
        }

        foreach(ExecutionAction *tmp_action, actions())
        {
            if (tmp_action->hasChangedValues())
                return true;
        }

        foreach(ExecutionTestParameter *tmp_param, parameters())
        {
            if (tmp_param->hasChangedValues())
                return true;
        }

        return false;
    }

    return true;
}


void ExecutionTest::synchronizeFromTestDatas(QList<Test*> in_tests_list)
{
    ExecutionAction                    *tmp_execution_action = NULL;
    int                                tmp_test_action_index = 0;
    int                                tmp_execution_action_index = 0;
    QList<Action*>                    tmp_actions;
    QList<Action*>                    tmp_all_actions;

    ExecutionTest                        *tmp_execution_test = NULL;
    int                                tmp_test_index = 0;
    int                                tmp_execution_test_index = 0;
    Test               *tmp_test = NULL;

    Test *tmp_project_test = Test::ExecutionTestRelation::instance().getParent(this);
    if (tmp_project_test != NULL) {
        if (tmp_project_test->original() != NULL)
            tmp_test = tmp_project_test->original();
        else
            tmp_test = tmp_project_test;

        TestContent *test_content = TestContent::TestRelation::instance().getParent(tmp_project_test);
        if (test_content)
            tmp_actions = TestContent::ActionRelation::instance().getChilds(test_content);

        foreach(Action *tmp_action, tmp_actions) {
            tmp_action->loadAssociatedActionsForVersion(tmp_test->getValueForKey(TESTS_HIERARCHY_VERSION), tmp_test->getValueForKey(TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID));
            if (tmp_action->associatedTestActions().count() > 0) {
                foreach(Action *tmp_associated_action, tmp_action->associatedTestActions()) {
                    tmp_all_actions.append(tmp_associated_action);
                }
            }
            else {
                tmp_all_actions.append(tmp_action);
            }
        }
    }

    for (tmp_test_action_index = 0; tmp_test_action_index < tmp_all_actions.count(); tmp_test_action_index++) {
        tmp_execution_action_index = indexForActionWithValueForKey(tmp_all_actions[tmp_test_action_index]->getValueForKey(ACTIONS_TABLE_ACTION_ID), EXECUTIONS_ACTIONS_TABLE_ACTION_ID);
        if (tmp_execution_action_index < 0) {
            tmp_execution_action = new ExecutionAction();
            Action::ExecutionActionRelation::instance().setParent(tmp_all_actions[tmp_test_action_index], tmp_execution_action);
            tmp_execution_action->setValueForKey(EXECUTION_ACTION_BYPASSED, EXECUTIONS_ACTIONS_TABLE_RESULT_ID);
            ExecutionTest::ExecutionActionRelation::instance().insertChild(this, tmp_test_action_index, tmp_execution_action);
        }
        else if (tmp_execution_action_index != tmp_test_action_index) {
            ExecutionTest::ExecutionActionRelation::instance().moveChild(this, tmp_execution_action_index, tmp_test_action_index);
        }
    }

    for (tmp_test_action_index = tmp_all_actions.count(); tmp_test_action_index < actions().count(); tmp_test_action_index++) {
        removeExecutionActionAtIndex(tmp_test_action_index);
    }

    for (tmp_test_index = 0; tmp_test_index < in_tests_list.count(); tmp_test_index++) {
        tmp_execution_test_index = ExecutionTest::ExecutionActionRelation::instance().indexForChildWithValueForKey(this, in_tests_list[tmp_test_index]->getValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_ID), EXECUTIONS_TESTS_TABLE_TEST_ID);
        if (tmp_execution_test_index < 0) {
            tmp_execution_test = new ExecutionTest();
            tmp_execution_test->createExecutionTestHierarchyForTest(in_tests_list[tmp_test_index]);
            ExecutionCampaign::ExecutionTestRelation::instance().addChild(ExecutionCampaign::ExecutionTestRelation::instance().getParent(this), tmp_execution_test);
            insertChild(tmp_test_index, tmp_execution_test);
        }
        else if (tmp_execution_test_index != tmp_test_index) {
            moveChild(tmp_execution_test_index, tmp_test_index);
            getAllChilds()[tmp_test_index]->synchronizeFromTestDatas(in_tests_list[tmp_test_index]->getAllChilds());
        }
        else
            getAllChilds()[tmp_test_index]->synchronizeFromTestDatas(in_tests_list[tmp_test_index]->getAllChilds());
    }

    for (tmp_test_index = in_tests_list.count(); tmp_test_index < getAllChilds().count();) {
        removeChildAtIndex(tmp_test_index);
    }
}

}
