/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef TEST_H
#define TEST_H

#include <QVariant>
#include <QXmlStreamWriter>

#include "bug.h"
#include "customtestfield.h"
#include "testsplan/graphictest.h"
#include "testcampaign.h"
#include "executiontest.h"
#include "hierarchyentity.h"

namespace Entities
{
class ProjectVersion;
class Requirement;
class XmlProjectDatas;
class Test;
class TestCampaign;
class GraphicTest;
class ExecutionTest;

class BasicTest:
    public Entity<BasicTest, &tests_table_def>
{
public:
    BasicTest();
};

class Test:
    public HierarchyEntity<Test, &tests_hierarchy_def, TESTS_HIERARCHY_PREVIOUS_TEST_ID, TESTS_HIERARCHY_PARENT_TEST_ID>
{

public:
    Test();
    Test(ProjectVersion *in_project);
    Test(Test* in_parent);

    ~Test();

    ProjectVersion* projectVersion();
    void setProjectVersion(ProjectVersion* in_project_version);

    Test* parentTest();

    void setIsALinkOf(Test* original);
    Test* original();

    static Test* findTestWithId(const QList<Test*>& in_tests_list, const char* in_test_id, bool in_recursive = true);

    int setDataFromRequirement(Requirement *in_requirement);
    void setDataFromTestContent(TestContent* in_test_content);

    void searchFieldWithValue(QList<Record*> *in_found_list, const char* in_field_name, const char* in_field_value, bool in_recursive, int in_comparison_value = Record::EqualTo);

    bool isModifiable();

    int saveRecord();

    int deleteRecord();

    void writeXml(QXmlStreamWriter & in_xml_writer);
    bool readXml(QXmlStreamReader & in_xml_reader);
    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    QList<Bug*> loadBugs();

    bool isAutomatedGuiTest() const;
    bool isAutomatedBatchTest() const;

    ADD_NAMED_RELATION(OriginalTest, Test, Test, TESTS_TABLE_ORIGINAL_TEST_ID);
    ADD_RELATION(Test, GraphicTest, GRAPHICS_TESTS_TABLE_TEST_ID);
    ADD_RELATION(Test, TestCampaign, TESTS_CAMPAIGNS_TABLE_TEST_ID);
    ADD_RELATION(Test, ExecutionTest, EXECUTIONS_TESTS_TABLE_TEST_ID);

};

}

#endif // TEST_H
