/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef PROJECT_H
#define PROJECT_H

#include "entities/entity.h"
#include "projectparameter.h"
#include "projectversion.h"
#include "need.h"
#include "projectgrant.h"
#include "xmlprojectdatas.h"

#include <QXmlStreamWriter>

namespace Entities
{
class TestContent;
class RequirementContent;
class FeatureContent;
class RuleContent;
class Feature;
class Rule;

class Project :
    public Entity<Project, &projects_table_def>
{
public:
    Project();
    virtual ~Project();

    const QList < Need* >& needsHierarchy();

    const QList<ProjectGrant*>& projectGrants();

    QList < TestContent* > loadProjectTestsContents(net_callback_fct *in_callback = NULL);
    QList < RequirementContent* > loadProjectRequirementsContents(net_callback_fct *in_callback = NULL);

    void setProjectGrantsUsernameFilter(const QString& username);
    QString usernameFilter() const;

    QStringList parametersNames();
    const char* paramValueForParamName(const char *in_param_name);

    int saveRecord();

    void writeXml(QXmlStreamWriter & in_xml_writer, net_callback_fct *in_callback = NULL);
    XmlProjectDatas readXml(QXmlStreamReader & in_xml_reader);
    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    bool canWriteNeeds();

    ProjectGrant* createProjectGrantForEntity(const entity_def *in_entity, const char *in_rights);
    void createProjectGrants(const char* in_rights);

    bool isModifiable();

    ADD_RELATION(Project, ProjectParameter, PROJECTS_PARAMETERS_TABLE_PROJECT_ID);
    ADD_RELATION(Project, ProjectVersion, PROJECTS_VERSIONS_TABLE_PROJECT_ID);
    ADD_RELATION(Project, Need, NEEDS_TABLE_PROJECT_ID, NEEDS_TABLE_PREVIOUS_NEED_ID);
    ADD_RELATION(Project, ProjectGrant, PROJECTS_GRANTS_TABLE_PROJECT_ID);

private:
    QString m_project_grants_username_filter;
};

}

#endif // PROJECT_H
