#ifndef FEATURE_H
#define FEATURE_H

#include "hierarchyentity.h"

namespace Entities
{
class ProjectVersion;
class FeatureContent;

class BasicFeature :
    public Entity<BasicFeature, &features_table_def>
{
public:
    BasicFeature();
};


class Feature :
        public HierarchyEntity<Feature, &features_hierarchy_def, FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID, FEATURES_HIERARCHY_PARENT_FEATURE_ID>
{
public:

    Feature();
    Feature(ProjectVersion* in_project_version);
    Feature(Feature* in_parent);

    const char *columnNameForPreviousItem(){return FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID;}
    const char *columnNameForParentItem(){return FEATURES_HIERARCHY_PARENT_FEATURE_ID;}

    void setProjectVersion(ProjectVersion* in_project_version);
    ProjectVersion* projectVersion();

    Feature* parentFeature();

    void setDataFromFeatureContent(FeatureContent *in_feature_content);
    int saveRecord();
    int deleteRecord();

    bool isModifiable();
};

}

#endif // FEATURE_H
