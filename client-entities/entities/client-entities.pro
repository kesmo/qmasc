#-------------------------------------------------
#
# Project created by QtCreator 2012-12-27T17:30:59
#
#-------------------------------------------------

#QT       -= gui
QT += network \
    xml

TARGET = client-entities
TEMPLATE = lib
CONFIG += staticlib

include (../common/common.pri)
DEFINES += RTMR_LIBRARY_IMPORTS
DEFINES += AUTOMATION_LIB
DEFINES += AUTOMATION_LIBRARY_IMPORTS

LIBS += -lrtmr
Debug {
    wni32:QMAKE_CFLAGS_DEBUG += /Zi
    LIBS += -L../client-lib/Debug
}else {
    LIBS += -L../client-lib/Release
}

INCLUDEPATH += . ../automation-lib \

win32:contains(DEFINES, AUTOMATION_LIB) {
    LIBS += -lautomation-lib
    DEFINES += GUI_AUTOMATION_ACTIVATED
}

SOURCES += record.cpp \
    action.cpp \
    campaign.cpp \
    executionaction.cpp \
    executioncampaign.cpp \
    executionrequirement.cpp \
    executiontest.cpp \
    project.cpp \
    projectversion.cpp \
    requirement.cpp \
    requirementcontent.cpp \
    test.cpp \
    testcampaign.cpp \
    testcontent.cpp \
    testrequirement.cpp \
    user.cpp \
    genericrecord.cpp \
    projectgrant.cpp \
    testcontentfile.cpp \
    projectparameter.cpp \
    executioncampaignparameter.cpp \
    xmlprojectdatas.cpp \
    bug.cpp \
    parameter.cpp \
    executiontestparameter.cpp \
    automatedaction.cpp \
    customfielddesc.cpp \
    customfield.cpp \
    customtestfield.cpp \
    customrequirementfield.cpp \
    automatedactionvalidation.cpp \
    session.cpp \
    parent.cpp \
    automatedactionwindowhierarchy.cpp \
    need.cpp \
    feature.cpp \
    featurecontent.cpp \
    rule.cpp \
    rulecontent.cpp \
    child.cpp \
    garbagecollector.cpp \
    hierarchyentity.cpp \
    relations/entitydescription.cpp \
    relations/dualrelation.cpp \
    relations/onetoonerelation.cpp \
    relations/onetonrelation.cpp \
    relations/hierarchicalrelations.cpp \
    relations/entitiesmodel.cpp \
    relations/entities/entity.cpp \
    relations/entities/hierarchyentity.cpp \
    relations/entities/descriptions/projectdescription.cpp \
    relations/entities/descriptions/ruledescription.cpp \
    relations/entities/instances/projectentity.cpp \
    relations/entities/instances/ruleentity.cpp \
    projectversionparameter.cpp \
    testsplan/testsplan.cpp \
    testsplan/graphicitem.cpp \
    testsplan/graphictest.cpp \
    testsplan/graphicif.cpp \
    testsplan/graphicswitch.cpp \
    testsplan/graphicloop.cpp \
    testsplan/graphiclink.cpp \
    recordlistener.cpp \
    abstractrecord.cpp \
    testsplan/graphicpoint.cpp \
    relations/entities/descriptions/undefine.cpp

HEADERS += genericrecord.h \
    projectgrant.h \
    testcontentfile.h \
    projectparameter.h \
    record.h \
    action.h \
    campaign.h \
    executionaction.h \
    executioncampaign.h \
    executionrequirement.h \
    executiontest.h \
    project.h \
    projectversion.h \
    requirement.h \
    requirementcontent.h \
    test.h \
    testcampaign.h \
    testcontent.h \
    testrequirement.h \
    user.h \
    executioncampaignparameter.h \
    xmlprojectdatas.h \
    bug.h \
    parameter.h \
    executiontestparameter.h \
    automatedaction.h \
    customfielddesc.h \
    customfield.h \
    customtestfield.h \
    customrequirementfield.h \
    automatedactionvalidation.h \
    session.h \
    parent.h \
    automatedactionwindowhierarchy.h \
    need.h \
    feature.h \
    featurecontent.h \
    rule.h \
    rulecontent.h \
    child.h \
    garbagecollector.h \
    hierarchyentity.h \
    relations/entitydescription.h \
    relations/dualrelation.h \
    relations/onetoonerelation.h \
    relations/onetonrelation.h \
    relations/hierarchicalrelations.h \
    relations/entitiesmodel.h \
    relations/entities/entity.h \
    relations/entities/hierarchyentity.h \
    relations/entities/descriptions/projectdescription.h \
    relations/entities/descriptions/ruledescription.h \
    relations/entities/instances/projectentity.h \
    relations/entities/instances/ruleentity.h \
    projectversionparameter.h \
    testsplan/testsplan.h \
    testsplan/graphicitem.h \
    testsplan/graphictest.h \
    testsplan/graphicif.h \
    testsplan/graphicswitch.h \
    testsplan/graphicloop.h \
    testsplan/graphiclink.h \
    recordlistener.h \
    abstractrecord.h \
    testsplan/graphicpoint.h \
    relations/entities/descriptions/undefine.h

INCLUDEPATH +=  ../client-lib \
    ../client-modules \
    ../common
