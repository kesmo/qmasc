#ifndef STATUS_ENTITY_H
#define STATUS_ENTITY_H

#include "entity.h"

namespace Entities
{

class Status:
    public Entity<Status, &status_table_def>
{
public:
    Status();
};

}

#endif // STATUS_ENTITY_H
