#include "need.h"
#include "session.h"
#include "project.h"

namespace Entities
{

Need::Need()
{
}

Need::Need(Project *in_project)
{
    setProject(in_project);
}

Need::Need(Need *in_parent)
{
    if (in_parent != NULL) {
        setParent(in_parent);
        setProject(in_parent->project());
    }
}

Project* Need::project()
{
    return Project::NeedRelation::instance().getParent(this);
}


Need* Need::parentNeed()
{
    return getParent();
}


void Need::setProject(Project* in_project)
{
    if (in_project != NULL)
    {
        Project::NeedRelation::instance().setParent(in_project, this);
        if (is_empty_string(getIdentifier())) {
            setValueForKey(in_project->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), NEEDS_TABLE_PROJECT_ID);
        }
    }
}

int Need::saveRecord()
{
    int            tmp_result = NOERR;

    if (project() != NULL)
    {
        setValueForKey(project()->getValueForKey(PROJECTS_TABLE_PROJECT_ID), NEEDS_TABLE_PROJECT_ID);
    }

    if (parentNeed() != NULL)
    {
        if (is_empty_string(parentNeed()->getIdentifier()))
        {
            tmp_result = parentNeed()->saveRecord();
            if (tmp_result == NOERR)
                setValueForKey(parentNeed()->getIdentifier(), NEEDS_TABLE_PARENT_NEED_ID);
            else
                return tmp_result;
        }
        else
            setValueForKey(parentNeed()->getIdentifier(), NEEDS_TABLE_PARENT_NEED_ID);

        setValueForKey(parentNeed()->getValueForKey(NEEDS_TABLE_PROJECT_ID), NEEDS_TABLE_PROJECT_ID);
    }
    else
        setValueForKey(NULL, NEEDS_TABLE_PARENT_NEED_ID);


    if (is_empty_string(getIdentifier()))
    {
        tmp_result = insertRecord();
    }
    else
    {
        tmp_result = Entity::saveRecord();
    }

    return tmp_result;
}


bool Need::isModifiable()
{
    if (project() == NULL)
        return false;

    return project()->canWriteNeeds();
}

}
