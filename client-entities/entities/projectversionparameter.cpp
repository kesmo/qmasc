#include "projectversionparameter.h"
#include "projectversion.h"
#include "utilities.h"
#include "session.h"

namespace Entities
{

ProjectVersionParameter::ProjectVersionParameter()
{
}


ProjectVersionParameter::ProjectVersionParameter(ProjectVersion *in_project_version)
{
    ProjectVersion::ProjectVersionParameterRelation::instance().setParent(in_project_version, this);
}

void ProjectVersionParameter::setProjectVersion(ProjectVersion* in_project_version)
{
    if (in_project_version != NULL)
    {
        ProjectVersion::ProjectVersionParameterRelation::instance().setParent(in_project_version, this);
        if (is_empty_string(getIdentifier())) {
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID), PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_VERSION_ID);
        }
    }
}

ProjectVersion *ProjectVersionParameter::projectVersion()
{
    return ProjectVersion::ProjectVersionParameterRelation::instance().getParent(this);
}

int ProjectVersionParameter::saveRecord()
{
    int    tmp_return = NOERR;

    if (is_empty_string(getIdentifier()))
    {
        ProjectVersion *projectVersion = ProjectVersion::ProjectVersionParameterRelation::instance().getParent(this);

        if (projectVersion != NULL)
            setValueForKey(projectVersion->getIdentifier(), PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_VERSION_ID);

        tmp_return = Entity::insertRecord();
    }
        else
            tmp_return = Entity::saveRecord();

    return tmp_return;
}


const char* ProjectVersionParameter::name()
{
    return getValueForKey(PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_NAME);
}


const char* ProjectVersionParameter::value()
{
    return getValueForKey(PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_VALUE);
}


ProjectVersionParameter* ProjectVersionParameter::parentProjectVersionParameter()
{
    return getParent();
}

}
