#ifndef ACTIONRESULT_ENTITY_H
#define ACTIONRESULT_ENTITY_H

#include "entity.h"

namespace Entities
{
class ActionResult:
    public Entity<ActionResult, &actions_results_table_def>
{
public:
    ActionResult();
};

}

#endif // ACTIONRESULT_ENTITY_H
