#include "rulecontent.h"

namespace Entities
{

RuleContent::RuleContent()
{
}

RuleContent* RuleContent::copy() const
{
    RuleContent *tmp_copy = new RuleContent();

    tmp_copy->setValueForKey(getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME),RULES_CONTENTS_TABLE_SHORT_NAME);
    tmp_copy->setValueForKey(getValueForKey(RULES_CONTENTS_TABLE_DESCRIPTION),RULES_CONTENTS_TABLE_DESCRIPTION);

    return tmp_copy;
}

}
