/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef AUTOMATEDACTION_H
#define AUTOMATEDACTION_H

#include "entities/entity.h"
#include "automatedactionvalidation.h"
#include "automatedactionwindowhierarchy.h"

namespace Entities
{
QVariant FunctionAutomatedActionMessageType(Record* record, int role);
QVariant FunctionAutomatedActionMessageData(Record* record, int role);
QVariant FunctionAutomatedActionMessageCallback(Record* record, int role);
QVariant FunctionAutomatedActionMessageDelay(Record* record, int role);

class AutomatedAction :
    public Entity<AutomatedAction, &automated_actions_table_def>
{
    public:

        static const char      FieldSeparator = 31;
        static const char      WindowSeparator = 28;

        AutomatedAction();
        ~AutomatedAction();

        int saveRecord();

        ADD_RELATION(AutomatedAction, AutomatedActionValidation, AUTOMATED_ACTIONS_TABLE_ACTION_ID, AUTOMATED_ACTIONS_VALIDATIONS_TABLE_PREVIOUS_VALIDATION_ID);
        ADD_RELATION(AutomatedAction, AutomatedActionWindowHierarchy);
};

}

#endif // AUTOMATEDACTION_H
