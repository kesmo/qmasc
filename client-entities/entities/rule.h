#ifndef RULE_H
#define RULE_H

#include "hierarchyentity.h"
#include "entities/entity.h"

namespace Entities
{
class ProjectVersion;
class RuleContent;

class BasicRule :
    public Entity<BasicRule, &rules_table_def>
{
public:
    BasicRule();
};



class Rule :
        public HierarchyEntity<Rule, &rules_hierarchy_def, RULES_HIERARCHY_PREVIOUS_RULE_ID, RULES_HIERARCHY_PARENT_RULE_ID>
{
public:
    Rule();
    Rule(ProjectVersion *in_project_version);
    Rule(Rule *in_parent);

    void setProjectVersion(ProjectVersion* in_project_version);
    ProjectVersion* projectVersion();

    Rule* parentRule();

    void setDataFromRuleContent(RuleContent *in_rule_content);

    int saveRecord();
    int deleteRecord();

    bool isModifiable();
};

}

#endif // RULE_H
