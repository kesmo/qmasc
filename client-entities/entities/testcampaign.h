/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef TESTCAMPAIGN_H
#define TESTCAMPAIGN_H

#include "entities/entity.h"
#include "xmlprojectdatas.h"

#include <QXmlStreamWriter>

namespace Entities
{
class TestCampaign :
    public Entity<TestCampaign, &tests_campaigns_table_def>
{
public:
    TestCampaign();
    ~TestCampaign();

    int saveRecord();

    void writeXml(QXmlStreamWriter & in_xml_writer);
    bool readXml(QXmlStreamReader & in_xml_reader);
    int saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas);

    ADD_NAMED_RELATION(PreviousTestCampaign, TestCampaign, TestCampaign, TESTS_CAMPAIGNS_TABLE_PREVIOUS_TEST_CAMPAIGN_ID);
};

}

#endif // TESTCAMPAIGN_H
