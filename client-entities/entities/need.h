#ifndef NEED_H
#define NEED_H

#include "hierarchyentity.h"
#include "featurecontent.h"

namespace Entities
{
class Project;

class Need :
    public HierarchyEntity<Need, &needs_table_def, NEEDS_TABLE_PREVIOUS_NEED_ID, NEEDS_TABLE_PARENT_NEED_ID>
{
public:
    Need();
    Need(Project *in_project);
    Need(Need *in_parent);

    void setProject(Project* in_project);
    Project* project();

    Need* parentNeed();

    int saveRecord();

    bool isModifiable();

    ADD_RELATION(Need, FeatureContent, FEATURES_CONTENTS_TABLE_NEED_ID);
};

}

#endif // NEED_H
