/*****************************************************************************
 Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

 This file is part of R.T.M.R.

 R.T.M.R is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 R.T.M.R is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/

#include "project.h"
#include "requirement.h"
#include "requirementcontent.h"
#include "testcontent.h"
#include "projectgrant.h"
#include "client.h"
#include "entities.h"

#include "projectparameter.h"
#include "projectversion.h"
#include "need.h"
#include "projectgrant.h"
#include "feature.h"
#include "rule.h"
#include "campaign.h"
#include "session.h"
#include "executioncampaign.h"
#include "user.h"

#include <string.h>
#if defined (__APPLE__)
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#include <QPixmap>
#include <QIcon>

namespace Entities
{

/**
 Constructeur
 **/
Project::Project() :
    Entity(),
    m_project_grants_username_filter(Session::instance().getClientSession()->m_username)
{
}

Project::~Project()
{
}

const QList<ProjectGrant*>& Project::projectGrants()
{
    return Project::ProjectGrantRelation::instance().getChilds(this);
}


QList<TestContent*> Project::loadProjectTestsContents(net_callback_fct *in_callback)
{
    QList<TestContent*> tmp_tests_contents_list;

    net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s AND %s IN (SELECT %s FROM %s)", TESTS_CONTENTS_TABLE_PROJECT_ID,
                                   getIdentifier(), TESTS_CONTENTS_TABLE_TEST_CONTENT_ID, TESTS_TABLE_TEST_CONTENT_ID, TESTS_TABLE_SIG);

    tmp_tests_contents_list = TestContent::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer, TESTS_CONTENTS_TABLE_VERSION, in_callback);

    return tmp_tests_contents_list;
}

QList<RequirementContent*> Project::loadProjectRequirementsContents(net_callback_fct *in_callback)
{
    QList<RequirementContent*> tmp_requirements_contents_list;

    net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s AND %s IN (SELECT %s FROM %s)",
                                   REQUIREMENTS_CONTENTS_TABLE_PROJECT_ID, getIdentifier(),
                                   REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_CONTENT_ID, REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID,
                                   REQUIREMENTS_TABLE_SIG);

    tmp_requirements_contents_list = RequirementContent::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer, REQUIREMENTS_CONTENTS_TABLE_VERSION, in_callback);

    return tmp_requirements_contents_list;
}

void Project::setProjectGrantsUsernameFilter(const QString& username)
{
    m_project_grants_username_filter = username;
}

QString Project::usernameFilter() const
{
    return m_project_grants_username_filter;;
}

QStringList Project::parametersNames()
{
    QStringList param_names_list;
    foreach (ProjectParameter *tmp_param, Project::ProjectParameterRelation::instance().getChilds(this)) {
        if (is_empty_string(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME) == false)
            param_names_list << tmp_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME);
    }

    return param_names_list;
}

const char* Project::paramValueForParamName(const char *in_param_name)
{
    foreach (ProjectParameter *tmp_param, Project::ProjectParameterRelation::instance().getChilds(this)) {
        if (compare_values(tmp_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME), in_param_name) == 0)
            return tmp_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE);
    }

    return NULL;
}

int Project::saveRecord()
{
    int tmp_return = NOERR;

    tmp_return = Entity::saveRecord();

    return tmp_return;
}


void Project::writeXml(QXmlStreamWriter & in_xml_writer, net_callback_fct *in_callback)
{
    QList<TestContent*> tmp_tests_list;
    QList<RequirementContent*> tmp_requirements_list;
    char tmp_server_version[12];

    in_xml_writer.writeStartElement("rtmr");
    in_xml_writer.writeAttribute("client", APP_VERSION);
    in_xml_writer.writeAttribute("protocol", QString("%1.%2.%3.%4") .arg(PROTOCOL_VERSION >> 24) .arg((PROTOCOL_VERSION
                                                                                                       >> 16) & 0xFF) .arg((PROTOCOL_VERSION >> 8) & 0xFF) .arg(PROTOCOL_VERSION & 0xFF));

    if (cl_get_server_infos(Session::instance().getClientSession()) == NOERR)
    {
        net_get_field(NET_MESSAGE_TYPE_INDEX + 1, Session::instance().getClientSession()->m_response, tmp_server_version, SEPARATOR_CHAR);
        in_xml_writer.writeAttribute("server", tmp_server_version);
    }

    in_xml_writer.writeStartElement("project");
    in_xml_writer.writeAttribute("name", getValueForKey(PROJECTS_TABLE_SHORT_NAME));

    in_xml_writer.writeTextElement("description", getValueForKey(PROJECTS_TABLE_DESCRIPTION));

    // Paramètres de projet
    const QList<ProjectParameter*>& tmp_parameters = Project::ProjectParameterRelation::instance().getChilds(this);
    if (tmp_parameters.count() > 0)
    {
        in_xml_writer.writeStartElement("parameters");
        foreach(ProjectParameter *tmp_parameter, tmp_parameters)
        {
            in_xml_writer.writeStartElement("parameter");
            in_xml_writer.writeAttribute("name", tmp_parameter->getValueForKey(
                                             PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME));
            in_xml_writer.writeAttribute("value", tmp_parameter->getValueForKey(
                                             PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE));
            in_xml_writer.writeEndElement();
        }
        in_xml_writer.writeEndElement();
    }

    // Contenus d'exigences
    tmp_requirements_list = loadProjectRequirementsContents();
    if (tmp_requirements_list.count() > 0)
    {
        in_xml_writer.writeStartElement("requirementsContents");
        foreach(RequirementContent *tmp_requirement, tmp_requirements_list)
        {
            tmp_requirement->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
        qDeleteAll(tmp_requirements_list);
    }

    // Contenus de tests
    tmp_tests_list = loadProjectTestsContents(in_callback);
    if (tmp_tests_list.count() > 0)
    {
        in_xml_writer.writeStartElement("testsContents");
        foreach(TestContent *tmp_test, tmp_tests_list)
        {
            tmp_test->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
        qDeleteAll(tmp_tests_list);
    }

    // Versions
    const QList<ProjectVersion*>& tmp_versions = Project::ProjectVersionRelation::instance().getChilds(this);
    if (tmp_versions.count() > 0)
    {
        in_xml_writer.writeStartElement("versions");
        foreach(ProjectVersion *tmp_version, tmp_versions)
        {
            tmp_version->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
    }

    // Fin de la balise project
    in_xml_writer.writeEndElement();

    // Fin de la balise client
    in_xml_writer.writeEndElement();
}

XmlProjectDatas Project::readXml(QXmlStreamReader & in_xml_reader)
{
    const char *tmp_protocol_version = NULL;
    const char *tmp_ptr = NULL;
    char tmp_db_major_version[4];
    char tmp_db_medium_version[4];
    QString tmp_project_name;
    QString tmp_string;


    ProjectParameter *tmp_parameter = NULL;
    TestContent *tmp_test = NULL;
    RequirementContent *tmp_requirement = NULL;
    ProjectVersion *tmp_version = NULL;

    XmlProjectDatas tmp_xml_datas;
    tmp_xml_datas.m_is_valid = false;

    if (in_xml_reader.readNextStartElement())
    {
        if (in_xml_reader.name() == "rtmr")
        {
            std::string tmp_std_string =  in_xml_reader.attributes().value("protocol").toString().toStdString();
            tmp_protocol_version = tmp_std_string.c_str();
            if (is_empty_string(tmp_protocol_version) == FALSE)
            {
                tmp_ptr = tmp_protocol_version;
                tmp_ptr = net_get_field(0, tmp_ptr, tmp_db_major_version, '.');
                tmp_ptr = net_get_field(0, tmp_ptr, tmp_db_medium_version, '.');

                if (is_empty_string(tmp_db_major_version) == FALSE && is_empty_string(tmp_db_medium_version) == FALSE
                        && atoi(tmp_db_major_version) == (PROTOCOL_VERSION >> 24) && atoi(tmp_db_medium_version)
                        == ((PROTOCOL_VERSION >> 16) & 0xFF))
                {
                    if (in_xml_reader.readNextStartElement() && in_xml_reader.name() == "project")
                    {
                        tmp_project_name = in_xml_reader.attributes().value("name").toString() + " (Import du "
                                + QDateTime::currentDateTime().toString("dddd dd MMMM yyyy à hh:mm") + ")";
                        if (tmp_project_name.isEmpty() == false)
                        {
                            setValueForKey(tmp_project_name.toStdString().c_str(), PROJECTS_TABLE_SHORT_NAME);

                            while (in_xml_reader.readNextStartElement())
                            {
                                // Description
                                if (in_xml_reader.name() == "description")
                                {
                                    tmp_string = in_xml_reader.readElementText();
                                    setValueForKey(tmp_string.toStdString().c_str(), PROJECTS_TABLE_DESCRIPTION);
                                }
                                // Paramètres de projet
                                else if (in_xml_reader.name() == "parameters")
                                {
                                    while (in_xml_reader.readNextStartElement())
                                    {
                                        if (in_xml_reader.name() == "parameter")
                                        {
                                            tmp_parameter = new ProjectParameter(this);
                                            Project::ProjectParameterRelation::instance().setParent(this, tmp_parameter);

                                            tmp_parameter->setValueForKey(
                                                        in_xml_reader.attributes().value("name").toString().toStdString().c_str(),
                                                        PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME);
                                            tmp_parameter->setValueForKey(
                                                        in_xml_reader.attributes().value("value").toString().toStdString().c_str(),
                                                        PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE);
                                            in_xml_reader.skipCurrentElement();
                                        }
                                        else
                                        {
                                            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                                            in_xml_reader.skipCurrentElement();
                                        }
                                    }
                                }
                                // Contenus de tests
                                else if (in_xml_reader.name() == "testsContents")
                                {
                                    while (in_xml_reader.readNextStartElement())
                                    {
                                        if (in_xml_reader.name() == "testContent")
                                        {
                                            tmp_test = new TestContent();
                                            tmp_test->setValueForKey(getIdentifier(),
                                                                     TESTS_CONTENTS_TABLE_PROJECT_ID);
                                            tmp_test->readXml(in_xml_reader, tmp_xml_datas);
                                            tmp_xml_datas.m_tests_contents_list.append(tmp_test);
                                        }
                                        else
                                        {
                                            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                                            in_xml_reader.skipCurrentElement();
                                        }
                                    }
                                }
                                // Contenus d'exigences
                                else if (in_xml_reader.name() == "requirementsContents")
                                {
                                    while (in_xml_reader.readNextStartElement())
                                    {
                                        if (in_xml_reader.name() == "requirementContent")
                                        {
                                            tmp_requirement = new RequirementContent();
                                            tmp_requirement->setValueForKey(getIdentifier(),
                                                                            REQUIREMENTS_CONTENTS_TABLE_PROJECT_ID);
                                            tmp_requirement->readXml(in_xml_reader);
                                            tmp_xml_datas.m_requirements_contents_list.append(tmp_requirement);
                                        }
                                        else
                                        {
                                            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                                            in_xml_reader.skipCurrentElement();
                                        }
                                    }
                                }
                                // Versions
                                else if (in_xml_reader.name() == "versions")
                                {
                                    while (in_xml_reader.readNextStartElement())
                                    {
                                        if (in_xml_reader.name() == "version")
                                        {
                                            tmp_version = new ProjectVersion(this);
                                            tmp_version->readXml(in_xml_reader);
                                            tmp_xml_datas.m_versions_list.append(tmp_version);
                                        }
                                        else
                                        {
                                            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                                            in_xml_reader.skipCurrentElement();
                                        }
                                    }
                                }
                                else
                                {
                                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                                    in_xml_reader.skipCurrentElement();
                                }
                            }

                            tmp_xml_datas.m_is_valid = true;
                        }
                        else
                        {
                            net_session_print_error(Session::instance().getClientSession(),
                                                    "Tag <b>project</b> doesn't have attribute <i>name</i>.\n");
                        }
                    }
                    else
                    {
                        net_session_print_error(Session::instance().getClientSession(),
                                                "Tag <b>project</b> must be direct child of <b>rtmr</b> tag.\n");
                    }
                }
                else
                {
                    net_session_print_error(Session::instance().getClientSession(), "The version of the xml file isn't compliant.\n");
                }
            }
            else
            {
                net_session_print_error(Session::instance().getClientSession(),
                                        "Tag <b>rtmr</b> doesn't have attribute <i>protocol</i>.\n");
            }
        }
        else
        {
            net_session_print_error(Session::instance().getClientSession(),
                                    "Tag <b>rtmr</b> must be root of xml document.\n");
        }
    }

    return tmp_xml_datas;
}

int Project::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int result = saveRecord();

    if (result == NOERR) {
        // Droits sur le projet
        createProjectGrants(PROJECT_GRANT_WRITE);

        // Traitement post-lecture
        foreach(ProjectVersion* tmp_project_version, in_xml_datas.m_versions_list) {
            result = tmp_project_version->saveFromXmlProjectDatas(in_xml_datas);
            if (result != NOERR)
                return result;
        }
    }

    return result;
}

const QList < Need* >& Project::needsHierarchy()
{
    return Project::NeedRelation::instance().getChilds(this);
}


bool Project::canWriteNeeds()
{
    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(this,
                                                                                                           QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(NEEDS_TABLE_SIG_ID).toStdString().c_str(),
                                                                                                           QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}

bool Project::isModifiable()
{
    ProjectGrant* projectGrant = Project::ProjectGrantRelation::instance().getUniqueChildWithValuesForKeys(this,
                                                                                                   QList<const char*>() << Session::instance().user()->getValueForKey(USERS_TABLE_USERNAME) << QString::number(PROJECTS_TABLE_SIG_ID).toStdString().c_str(),
                                                                                                   QList<const char*>() << PROJECTS_GRANTS_TABLE_USERNAME << PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);

    return projectGrant && (compare_values(projectGrant->getValueForKey(PROJECTS_GRANTS_TABLE_RIGHTS), PROJECT_GRANT_WRITE) == 0) && (m_record_status == RECORD_STATUS_MODIFIABLE || m_record_status == RECORD_STATUS_OWN_LOCK);
}


ProjectGrant* Project::createProjectGrantForEntity(const entity_def* in_entity, const char* in_rights)
{
    ProjectGrant* tmp_project_grants = new ProjectGrant();
    tmp_project_grants->setValueForKey(getIdentifier(), PROJECTS_GRANTS_TABLE_PROJECT_ID);
    tmp_project_grants->setValueForKey(m_project_grants_username_filter.toStdString().c_str(), PROJECTS_GRANTS_TABLE_USERNAME);
    tmp_project_grants->setValueForKey(QString::number(in_entity->m_entity_signature_id).toStdString().c_str(), PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID);
    tmp_project_grants->setValueForKey(in_rights, PROJECTS_GRANTS_TABLE_RIGHTS);

    return tmp_project_grants;
}

void Project::createProjectGrants(const char* in_rights)
{
    ProjectGrant* tmp_project_grants = createProjectGrantForEntity(Project::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(Test::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(Requirement::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(Campaign::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(ExecutionCampaign::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(Need::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(Feature::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);

    tmp_project_grants = createProjectGrantForEntity(Rule::getEntityDefinition(), in_rights);
    Project::ProjectGrantRelation::instance().setParent(this, tmp_project_grants);
}

}
