/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "testcontent.h"
#include "utilities.h"
#include "requirement.h"
#include "session.h"
#include "automatedactionvalidation.h"
#include "projectversion.h"

namespace Entities
{

/**
  Constructeur
**/
TestContent::TestContent() : Entity()
{
}


bool TestContent::isAutomatedGuiTest() const
{
    return (compare_values(getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATED), TEST_CONTENT_TABLE_AUTOMATED_GUI) == 0);
}



bool TestContent::isAutomatedBatchTest() const
{
    return (compare_values(getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATED), TEST_CONTENT_TABLE_AUTOMATED_BATCH) == 0);
}

int TestContent::saveRecord()
{
    int tmp_return = NOERR;

    if (is_empty_string(getIdentifier()))
    {
        ProjectVersion *tmp_project_version = ProjectVersion::TestContentRelation::instance().getParent(this);
        if (tmp_project_version != NULL)
        {
            setValueForKey(tmp_project_version->getIdentifier(), TESTS_CONTENTS_TABLE_PROJECT_VERSION_ID);
            setValueForKey(tmp_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), TESTS_CONTENTS_TABLE_PROJECT_ID);
            setValueForKey(tmp_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), TESTS_CONTENTS_TABLE_VERSION);
        }

        tmp_return = Entity::saveRecord();
    }
    else
        tmp_return = Entity::saveRecord();

    return tmp_return;
}


TestContent* TestContent::copy()
{
    TestContent          *tmp_copy = new TestContent();

    tmp_copy->setValueForKey(getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME),TESTS_CONTENTS_TABLE_SHORT_NAME);
    tmp_copy->setValueForKey(getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION),TESTS_CONTENTS_TABLE_DESCRIPTION);
    tmp_copy->setValueForKey(getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL),TESTS_CONTENTS_TABLE_PRIORITY_LEVEL);
    tmp_copy->setValueForKey(getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID),TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID);

    return tmp_copy;
}


TestContent* TestContent::previousTestContent()
{
    TestContent            *tmp_previous_test = NULL;
    QList<TestContent*> tmp_project_tests;

    net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s AND %s<'%s' ORDER BY %s DESC LIMIT 1",
    TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID,
    getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID),
    TESTS_CONTENTS_TABLE_VERSION,
    getValueForKey(TESTS_CONTENTS_TABLE_VERSION),
    TESTS_CONTENTS_TABLE_VERSION);

    tmp_project_tests = TestContent::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer);
    if (tmp_project_tests.count() == 1) {
        tmp_previous_test = tmp_project_tests[0];
    }
    else {
        qDeleteAll(tmp_project_tests);
    }

    return tmp_previous_test;
}


TestContent* TestContent::nextTestContent()
{
    TestContent            *tmp_next_test = NULL;
    QList<TestContent*> tmp_project_tests;

    net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s AND %s>'%s' ORDER BY %s ASC LIMIT 1",
    TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID,
    getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID),
    TESTS_CONTENTS_TABLE_VERSION,
    getValueForKey(TESTS_CONTENTS_TABLE_VERSION),
    TESTS_CONTENTS_TABLE_VERSION);

    tmp_project_tests = TestContent::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer);
    if (tmp_project_tests.count() == 1) {
        tmp_next_test = tmp_project_tests[0];
    }
    else {
        qDeleteAll(tmp_project_tests);
    }

    return tmp_next_test;
}


TestContent* TestContent::loadLastTestContentForVersion(const char *in_original_test_content_id, const char *in_project_version)
{
    TestContent            *tmp_test_content = NULL;
    QList<TestContent*> tmp_project_tests;

    net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s AND %s<='%s' ORDER BY %s DESC LIMIT 1",
    TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID,
    in_original_test_content_id,
    TESTS_CONTENTS_TABLE_VERSION,
    in_project_version,
    TESTS_CONTENTS_TABLE_VERSION);

    tmp_project_tests = TestContent::loadRecordsList(Session::instance().getClientSession()->m_where_clause_buffer);
    if (tmp_project_tests.count() == 1) {
        tmp_test_content = tmp_project_tests[0];
    }
    else {
        qDeleteAll(tmp_project_tests);
    }

    return tmp_test_content;
}



QString TestContent::getPriorityLabel()
{
    switch(QString(getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL)).toInt())
    {
    case 1:
            return TR_CUSTOM_MESSAGE("Nulle");
            break;
    case 2:
            return TR_CUSTOM_MESSAGE("Faible");
            break;
    case 3:
            return TR_CUSTOM_MESSAGE("Elevée");
            break;
    case 4:
            return TR_CUSTOM_MESSAGE("Moyenne");
            break;
    case 5:
            return TR_CUSTOM_MESSAGE("Critique");
            break;
    }

    return QString();}

void TestContent::writeXml(QXmlStreamWriter & in_xml_writer)
{
    QList<Action*>        tmp_actions_list;
    QList<AutomatedAction*>        tmp_automated_actions_list;
    QList<TestRequirement*>    tmp_requirements_list;

    in_xml_writer.writeStartElement("testContent");

    in_xml_writer.writeAttribute("id", getIdentifier());
    in_xml_writer.writeAttribute("originalTestContentId", getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID));
    in_xml_writer.writeAttribute("version", getValueForKey(TESTS_CONTENTS_TABLE_VERSION));
    in_xml_writer.writeAttribute("name", getValueForKey(TESTS_CONTENTS_TABLE_SHORT_NAME));
    in_xml_writer.writeAttribute("type", getValueForKey(TESTS_CONTENTS_TABLE_TYPE));
    in_xml_writer.writeAttribute("category", getValueForKey(TESTS_CONTENTS_TABLE_CATEGORY_ID));
    in_xml_writer.writeAttribute("limit_test_case", getValueForKey(TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE));
    in_xml_writer.writeAttribute("priority", getValueForKey(TESTS_CONTENTS_TABLE_PRIORITY_LEVEL));
    in_xml_writer.writeAttribute("status", getValueForKey(TESTS_CONTENTS_TABLE_STATUS));
    in_xml_writer.writeAttribute("automated", getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATED));
    in_xml_writer.writeTextElement("automation_command", getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND));
    in_xml_writer.writeTextElement("automation_command_parameters", getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS));
    in_xml_writer.writeTextElement("automation_command_return_code_variable", getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE));
    in_xml_writer.writeTextElement("automation_command_stdout_variable", getValueForKey(TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE));
    in_xml_writer.writeTextElement("description", getValueForKey(TESTS_CONTENTS_TABLE_DESCRIPTION));

    if (isAutomatedGuiTest())
    {
        tmp_automated_actions_list = TestContent::AutomatedActionRelation::instance().getChilds(this);
        if (tmp_automated_actions_list.count() > 0)
        {
            in_xml_writer.writeStartElement("automated_actions");
            foreach(AutomatedAction *tmp_action, tmp_automated_actions_list)
            {
                in_xml_writer.writeStartElement("automated_action");
                in_xml_writer.writeTextElement("window_id", tmp_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_WINDOW_ID));
                in_xml_writer.writeTextElement("message_type", tmp_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE));
                in_xml_writer.writeTextElement("message_data", tmp_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA));
                in_xml_writer.writeTextElement("message_time_delay", tmp_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY));

                QList<AutomatedActionValidation*> tmp_automated_action_validations_list = AutomatedAction::AutomatedActionValidationRelation::instance().getChilds(tmp_action);
                if (tmp_automated_action_validations_list.count() > 0)
                {
                    in_xml_writer.writeStartElement("validations");
                    foreach(AutomatedActionValidation *tmp_validation, tmp_automated_action_validations_list)
                    {
                        in_xml_writer.writeStartElement("validation");
                        in_xml_writer.writeAttribute("module_name", tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME));
                        in_xml_writer.writeAttribute("module_version", tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION));
                        in_xml_writer.writeAttribute("module_function_name", tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME));
                        in_xml_writer.writeAttribute("module_function_parameters", tmp_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_PARAMETERS));
                        in_xml_writer.writeEndElement();
                    }
                    in_xml_writer.writeEndElement();
                    qDeleteAll(tmp_automated_action_validations_list);
                }

                in_xml_writer.writeEndElement();
            }
            in_xml_writer.writeEndElement();
            qDeleteAll(tmp_automated_actions_list);
        }
    }
    else if (!isAutomatedBatchTest())
    {
        tmp_actions_list = TestContent::ActionRelation::instance().getChilds(this);
        if (tmp_actions_list.count() > 0)
        {
            in_xml_writer.writeStartElement("actions");
            foreach(Action *tmp_action, tmp_actions_list)
            {
                in_xml_writer.writeStartElement("action");
                in_xml_writer.writeAttribute("name", tmp_action->getValueForKey(ACTIONS_TABLE_SHORT_NAME));
                in_xml_writer.writeAttribute("linkOriginalTestContentId", tmp_action->getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID));
                in_xml_writer.writeTextElement("description", tmp_action->getValueForKey(ACTIONS_TABLE_DESCRIPTION));
                in_xml_writer.writeTextElement("result", tmp_action->getValueForKey(ACTIONS_TABLE_WAIT_RESULT));
                in_xml_writer.writeEndElement();
            }
            in_xml_writer.writeEndElement();
            qDeleteAll(tmp_actions_list);
        }
    }

    tmp_requirements_list = TestContent::TestRequirementRelation::instance().getChilds(this);
    if (tmp_requirements_list.count() > 0)
    {
        in_xml_writer.writeStartElement("testsRequirements");
        foreach(TestRequirement *tmp_test_requirement, tmp_requirements_list)
        {
            tmp_test_requirement->writeXml(in_xml_writer);
        }
        in_xml_writer.writeEndElement();
        qDeleteAll(tmp_requirements_list);
    }

    in_xml_writer.writeEndElement();
}


bool TestContent::readXml(QXmlStreamReader & in_xml_reader, XmlProjectDatas & in_xml_datas)
{
    QString        tmp_id = in_xml_reader.attributes().value("id").toString();
    QString        tmp_original_test_content_id = in_xml_reader.attributes().value("originalTestContentId").toString();
    QString        tmp_version = in_xml_reader.attributes().value("version").toString();
    QString        tmp_name = in_xml_reader.attributes().value("name").toString();
    QString        tmp_type = in_xml_reader.attributes().value("type").toString();
    QString        tmp_category = in_xml_reader.attributes().value("category").toString();
    QString        tmp_limit = in_xml_reader.attributes().value("limit_test_case").toString();
    QString        tmp_priority = in_xml_reader.attributes().value("priority").toString();
    QString        tmp_status = in_xml_reader.attributes().value("status").toString();
    QString        tmp_automated = in_xml_reader.attributes().value("automated").toString();

    QString        tmp_text;

    QList<Action*>    tmp_actions_list;
    QList<AutomatedAction*>    tmp_automated_actions_list;
    QList<TestRequirement*>    tmp_tests_requirements_list;
    Action        *tmp_action = NULL;
    AutomatedAction        *tmp_automated_action = NULL;
    AutomatedActionValidation        *tmp_automated_action_validation = NULL;
    TestRequirement    *tmp_test_requirement = NULL;

    setValueForKey(tmp_id.toStdString().c_str(), TESTS_CONTENTS_TABLE_TEST_CONTENT_ID);
    setValueForKey(tmp_original_test_content_id.toStdString().c_str(), TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID);
    setValueForKey(tmp_version.toStdString().c_str(), TESTS_CONTENTS_TABLE_VERSION);
    setValueForKey(tmp_name.toStdString().c_str(), TESTS_CONTENTS_TABLE_SHORT_NAME);
    setValueForKey(tmp_type.toStdString().c_str(), TESTS_CONTENTS_TABLE_TYPE);
    setValueForKey(tmp_category.toStdString().c_str(), TESTS_CONTENTS_TABLE_CATEGORY_ID);
    setValueForKey(tmp_limit.toStdString().c_str(), TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE);
    setValueForKey(tmp_priority.toStdString().c_str(), TESTS_CONTENTS_TABLE_PRIORITY_LEVEL);
    setValueForKey(tmp_status.toStdString().c_str(), TESTS_CONTENTS_TABLE_STATUS);
    setValueForKey(tmp_automated.toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATED);

    while (in_xml_reader.readNextStartElement())
    {
        // Description
        if (in_xml_reader.name() == "description")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), TESTS_CONTENTS_TABLE_DESCRIPTION);
        }
        else if (in_xml_reader.name() == "automation_command")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND);
        }
        else if (in_xml_reader.name() == "automation_command_parameters")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS);
        }
        else if (in_xml_reader.name() == "automation_command_return_code_variable")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE);
        }
        else if (in_xml_reader.name() == "automation_command_stdout_variable")
        {
            tmp_text = in_xml_reader.readElementText();
            setValueForKey(tmp_text.toStdString().c_str(), TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE);
        }
        else if (in_xml_reader.name() == "actions")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "action")
                {
                    tmp_action = new Action();
                    tmp_action->setValueForKey(in_xml_reader.attributes().value("name").toString().toStdString().c_str(), ACTIONS_TABLE_SHORT_NAME);
                    tmp_action->setValueForKey(in_xml_reader.attributes().value("linkOriginalTestContentId").toString().toStdString().c_str(), ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);

                    while (in_xml_reader.readNextStartElement())
                    {
                        if (in_xml_reader.name() == "description")
                        {
                            tmp_text = in_xml_reader.readElementText();
                            tmp_action->setValueForKey(tmp_text.toStdString().c_str(), ACTIONS_TABLE_DESCRIPTION);
                        }
                        else if (in_xml_reader.name() == "result")
                        {
                            tmp_text = in_xml_reader.readElementText();
                            tmp_action->setValueForKey(tmp_text.toStdString().c_str(), ACTIONS_TABLE_WAIT_RESULT);
                        }
                        else
                        {
                            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                            in_xml_reader.skipCurrentElement();
                        }
                    }
                    tmp_actions_list.append(tmp_action);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else if (in_xml_reader.name() == "automated_actions")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "automated_action")
                {
                    tmp_automated_action = new AutomatedAction();
                    tmp_automated_action->setValueForKey(in_xml_reader.attributes().value("id").toString().toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_ACTION_ID);

                    while (in_xml_reader.readNextStartElement())
                    {
                        if (in_xml_reader.name() == "window_id")
                        {
                            tmp_text = in_xml_reader.readElementText();
                            tmp_automated_action->setValueForKey(tmp_text.toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_WINDOW_ID);
                        }
                        else if (in_xml_reader.name() == "message_type")
                        {
                            tmp_text = in_xml_reader.readElementText();
                            tmp_automated_action->setValueForKey(tmp_text.toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE);
                        }
                        else if (in_xml_reader.name() == "message_data")
                        {
                            tmp_text = in_xml_reader.readElementText();
                            tmp_automated_action->setValueForKey(tmp_text.toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
                        }
                        else if (in_xml_reader.name() == "message_time_delay")
                        {
                            tmp_text = in_xml_reader.readElementText();
                            tmp_automated_action->setValueForKey(tmp_text.toStdString().c_str(), AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY);
                        }
                        else if (in_xml_reader.name() == "validations")
                        {
                            while (in_xml_reader.readNextStartElement())
                            {
                                if (in_xml_reader.name() == "validation")
                                {
                                    tmp_automated_action_validation = new AutomatedActionValidation();
                                    AutomatedAction::AutomatedActionValidationRelation::instance().setParent(tmp_automated_action, tmp_automated_action_validation);
                                    tmp_automated_action_validation->setValueForKey(in_xml_reader.attributes().value("module_name").toString().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME);
                                    tmp_automated_action_validation->setValueForKey(in_xml_reader.attributes().value("module_version").toString().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION);
                                    tmp_automated_action_validation->setValueForKey(in_xml_reader.attributes().value("module_function_name").toString().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME);
                                    tmp_automated_action_validation->setValueForKey(in_xml_reader.attributes().value("module_function_parameters").toString().toStdString().c_str(), AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_PARAMETERS);
                                }
                                else
                                {
                                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                                }
                                in_xml_reader.skipCurrentElement();
                            }
                        }
                        else
                        {
                            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                            in_xml_reader.skipCurrentElement();
                        }
                    }
                    tmp_automated_actions_list.append(tmp_automated_action);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }

        else if (in_xml_reader.name() == "testsRequirements")
        {
            while (in_xml_reader.readNextStartElement())
            {
                if (in_xml_reader.name() == "testRequirement")
                {
                    tmp_test_requirement = new TestRequirement();
                    tmp_test_requirement->readXml(in_xml_reader);
                    tmp_tests_requirements_list.append(tmp_test_requirement);
                }
                else
                {
                    LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
                    in_xml_reader.skipCurrentElement();
                }
            }
        }
        else
        {
            LOG_ERROR(Session::instance().getClientSession(), "Unknow tag (%s) line %lli.\n", in_xml_reader.name().toString().toStdString().c_str(), in_xml_reader.lineNumber());
            in_xml_reader.skipCurrentElement();
        }
    }

    in_xml_datas.m_actions_dict.insert(getIdentifier(), tmp_actions_list);
    in_xml_datas.m_automated_actions_dict.insert(getIdentifier(), tmp_automated_actions_list);
    in_xml_datas.m_tests_requirements_dict.insert(getIdentifier(), tmp_tests_requirements_list);

    return true;
}


int TestContent::saveFromXmlProjectDatas(XmlProjectDatas & in_xml_datas)
{
    int        tmp_result = NOERR;
    QString        tmp_test_content_id = QString(getIdentifier());
    QString        tmp_original_test_content_id = QString(getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID));
    QList<Action*>    tmp_actions_list;
    QList<AutomatedAction*>    tmp_automated_actions_list;
    QList<TestRequirement*>    tmp_tests_requirements_list;
    QString        tmp_id_str;

    const char    *tmp_previous_action_id = NULL;

    if (is_empty_string(getOriginalValueForKey(TESTS_CONTENTS_TABLE_TEST_CONTENT_ID)) == TRUE)
    {
        tmp_id_str= in_xml_datas.getNewTestContentOriginalIdentifier(this, getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID));
        setValueForKey(NULL, TESTS_CONTENTS_TABLE_TEST_CONTENT_ID);
        if (tmp_id_str.isEmpty())
            setValueForKey(NULL, TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID);
        else
            setValueForKey(tmp_id_str.toStdString().c_str(), TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID);

        tmp_result = saveRecord();
        if (tmp_result == NOERR)
        {
            in_xml_datas.m_tests_contents_dict.insert(tmp_test_content_id, this);

            if (tmp_id_str.isEmpty() && is_empty_string(getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID)) == FALSE)
            {
                in_xml_datas.m_originals_tests_contents_dict.insert(tmp_original_test_content_id, getValueForKey(TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID));
            }

            if (in_xml_datas.m_actions_dict.contains(tmp_test_content_id))
            {
                tmp_actions_list = in_xml_datas.m_actions_dict.value(tmp_test_content_id);
                tmp_previous_action_id = NULL;

                foreach(Action *tmp_action, tmp_actions_list)
                {
                    tmp_id_str = in_xml_datas.getNewTestContentOriginalIdentifier(NULL, tmp_action->getValueForKey(ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID));
                    if (tmp_id_str.isEmpty())
                        tmp_action->setValueForKey(NULL, ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);
                    else
                        tmp_action->setValueForKey(tmp_id_str.toStdString().c_str(), ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID);

                    tmp_action->setValueForKey(tmp_previous_action_id, ACTIONS_TABLE_PREVIOUS_ACTION_ID);
                    tmp_action->setValueForKey(getIdentifier(), ACTIONS_TABLE_TEST_CONTENT_ID);
                    if (tmp_action->saveRecord() == NOERR)
                        tmp_previous_action_id = tmp_action->getIdentifier();
                    else
                      LOG_ERROR(Session::instance().getClientSession(), "%s\n", Session::instance().getClientSession()->m_last_error_msg);
                }
            }

            if (in_xml_datas.m_automated_actions_dict.contains(tmp_test_content_id))
            {
                tmp_automated_actions_list = in_xml_datas.m_automated_actions_dict.value(tmp_test_content_id);
                tmp_previous_action_id = NULL;

                foreach(AutomatedAction *tmp_automated_action, tmp_automated_actions_list)
                {
                    tmp_id_str = in_xml_datas.getNewTestContentOriginalIdentifier(NULL, tmp_automated_action->getValueForKey(AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID));
                    tmp_automated_action->setValueForKey(getIdentifier(), AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID);

                    tmp_automated_action->setValueForKey(tmp_previous_action_id, AUTOMATED_ACTIONS_TABLE_PREVIOUS_ACTION_ID);

                    if (tmp_automated_action->saveRecord() == NOERR)
                        tmp_previous_action_id = tmp_automated_action->getIdentifier();
                    else
                      LOG_ERROR(Session::instance().getClientSession(), "%s\n", Session::instance().getClientSession()->m_last_error_msg);
                }
            }


            if (in_xml_datas.m_tests_requirements_dict.contains(tmp_test_content_id))
            {
                tmp_tests_requirements_list = in_xml_datas.m_tests_requirements_dict.value(tmp_test_content_id);

                foreach(TestRequirement *tmp_test_requirement, tmp_tests_requirements_list)
                {
                    tmp_test_requirement->saveFromXmlProjectDatas(in_xml_datas);
                }
            }
        }
    }

    return tmp_result;
}

}
