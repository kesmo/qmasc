/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "executioncampaign.h"
#include "campaign.h"
#include "projectparameter.h"
#include "project.h"
#include "projectversion.h"
#include "test.h"
#include "testcampaign.h"
#include "campaign.h"
#include "session.h"
#include "testsplan/testsplan.h"
#include "testsplan/graphicpoint.h"
#include "testsplan/graphictest.h"

namespace Entities
{

ExecutionCampaign::ExecutionCampaign()
{
}


ExecutionCampaign::ExecutionCampaign(Campaign *in_campaign)
{
    Campaign::ExecutionCampaignRelation::instance().setParent(in_campaign, this);
}

ExecutionCampaign *ExecutionCampaign::create(Campaign *in_campaign)
{
    ExecutionTest                    *tmp_execution_test = NULL;
    ExecutionCampaign                *tmp_execution_campaign = new ExecutionCampaign(in_campaign);
    ExecutionCampaignParameter      *tmp_execution_param = NULL;

    tmp_execution_campaign->setValueForKey(CLIENT_MACRO_NOW, EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE);

    const QList<TestCampaign*> &tests_list = Campaign::TestCampaignRelation::instance().getChilds(in_campaign);
    foreach(TestCampaign *tmp_test_campaign, tests_list)
    {
        tmp_execution_test = new ExecutionTest(tmp_execution_campaign);
        tmp_execution_test->createExecutionTestHierarchyForTest(Test::TestCampaignRelation::instance().getParent(tmp_test_campaign));
        ExecutionCampaign::ExecutionTestRelation::instance().appendChild(tmp_execution_campaign, tmp_execution_test);
    }

    if (in_campaign != NULL && in_campaign->projectVersion() != NULL && in_campaign->projectVersion()->project() != NULL) {
        foreach(ProjectParameter *tmp_project_param, Project::ProjectParameterRelation::instance().getChilds(in_campaign->projectVersion()->project()))
        {
            tmp_execution_param = new ExecutionCampaignParameter();
            tmp_execution_param->setValueForKey(tmp_project_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME);
            tmp_execution_param->setValueForKey(tmp_project_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_VALUE);
            tmp_execution_campaign->addParameter(tmp_execution_param);
        }
    }

    return tmp_execution_campaign;
}


ExecutionCampaign *ExecutionCampaign::createFromTestsPlan(Campaign *in_campaign)
{
    ExecutionTest                    *tmp_execution_test = NULL;
    ExecutionCampaign                *tmp_execution_campaign = new ExecutionCampaign(in_campaign);
    ExecutionCampaignParameter      *tmp_execution_param = NULL;
    TestsPlan                       *tmp_tests_plan = Campaign::TestsPlanRelation::instance().getChildAtIndex(in_campaign, 0);

    tmp_execution_campaign->setValueForKey(CLIENT_MACRO_NOW, EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE);

    if (tmp_tests_plan) {
        const QList<GraphicTest*>& testsList = TestsPlan::GraphicTestRelation::instance().getChilds(tmp_tests_plan);
        const QList<GraphicPoint*>& pointsList = TestsPlan::GraphicPointRelation::instance().getChilds(tmp_tests_plan);
        QString str(QChar(GRAPHICS_POINTS_TYPE_END));
        QList<GraphicPoint*> endPointsList = GraphicPoint::findEntitiesWithValueForKey(pointsList, str.toLocal8Bit().data(), GRAPHICS_POINTS_TABLE_TYPE);

        foreach(GraphicTest *tmp_graphic_test, testsList)
        {
            GraphicPoint* connectedEndPoint = GraphicPoint::findEntityWithValueForKey(endPointsList, tmp_graphic_test->getIdentifier(), GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID);
            if (!connectedEndPoint) {
                Test* tmp_test = Test::GraphicTestRelation::instance().getParent(tmp_graphic_test);
                if (tmp_test) {
                    tmp_execution_test = new ExecutionTest(tmp_execution_campaign);
                    tmp_execution_test->createExecutionTestHierarchyForTest(tmp_test);
                    ExecutionCampaign::ExecutionTestRelation::instance().setParent(tmp_execution_campaign, tmp_execution_test);
                    break;
                }
            }
        }
    }

    if (in_campaign != NULL && in_campaign->projectVersion() != NULL && in_campaign->projectVersion()->project() != NULL) {
        foreach(ProjectParameter *tmp_project_param, Project::ProjectParameterRelation::instance().getChilds(in_campaign->projectVersion()->project()))
        {
            tmp_execution_param = new ExecutionCampaignParameter();
            tmp_execution_param->setValueForKey(tmp_project_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME);
            tmp_execution_param->setValueForKey(tmp_project_param->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_VALUE);
            tmp_execution_campaign->addParameter(tmp_execution_param);
        }
    }

    return tmp_execution_campaign;
}


Campaign* ExecutionCampaign::campaign()
{
    return Campaign::ExecutionCampaignRelation::instance().getParent(this);
}


void ExecutionCampaign::addParameter(ExecutionCampaignParameter *in_parameter)
{
    ExecutionCampaign::ExecutionCampaignParameterRelation::instance().addChild(this, in_parameter);
}

void ExecutionCampaign::removeParameter(ExecutionCampaignParameter *in_parameter)
{
    ExecutionCampaign::ExecutionCampaignParameterRelation::instance().removeChild(this, in_parameter);
}

const QList < ExecutionCampaignParameter* >& ExecutionCampaign::parametersList()
{
    return ExecutionCampaign::ExecutionCampaignParameterRelation::instance().getChilds(this);
}

int ExecutionCampaign::saveRecord()
{
    int                             tmp_result = NOERR;
    ExecutionCampaignParameter*        tmp_param = NULL;

    setValueForKey(campaign()->getIdentifier(), EXECUTIONS_CAMPAIGNS_TABLE_CAMPAIGN_ID);

    tmp_result = Record::saveRecord();
    if (tmp_result == NOERR) {
        // Mettre à jour les resultats des tests
        const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
        foreach(ExecutionTest* tmp_child_execution_test, tests_list)
        {
            tmp_child_execution_test->updateTestResult();
        }

        // Sauver la liste des tests
        tmp_result = saveOrderedRecordList(tests_list, EXECUTIONS_TESTS_TABLE_PREVIOUS_EXECUTION_TEST_ID);
        if (tmp_result == NOERR) {
            // Sauver la liste des parametres d'executions
            for (int tmp_index = 0; tmp_index < parametersList().count() && tmp_result == NOERR; tmp_index++) {
                tmp_param = parametersList()[tmp_index];
                if (tmp_param != NULL) {
                    tmp_param->setValueForKey(getIdentifier(), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_ID);
                    tmp_result = tmp_param->saveRecord();
                }
            }
        }
    }

    return tmp_result;
}


bool ExecutionCampaign::hasChangedValues()
{
    if (!Entity::hasChangedValues()) {
        const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
        foreach(ExecutionTest* tmp_child_execution_test, tests_list)
        {
            if (tmp_child_execution_test->hasChangedValues())
                return true;
        }

        foreach(ExecutionCampaignParameter* tmp_param, parametersList())
        {
            if (tmp_param->hasChangedValues())
                return true;
        }
        return false;
    }

    return true;
}

float ExecutionCampaign::executionCoverageRate()
{
    float    tmp_result = 0;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
    if (tests_list.count() > 0) {
        foreach(ExecutionTest *tmp_execution_test, tests_list)
        {
            tmp_result += tmp_execution_test->executionCoverageRate();
        }
        tmp_result = tmp_result / tests_list.count();
    }

    return tmp_result;
}

float ExecutionCampaign::executionValidatedRate()
{
    float    tmp_result = 0;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
    if (tests_list.count() > 0) {
        foreach(ExecutionTest *tmp_execution_test, tests_list)
        {
            tmp_result += tmp_execution_test->executionValidatedRate();
        }
        tmp_result = tmp_result / tests_list.count();
    }

    return tmp_result;
}

float ExecutionCampaign::executionInValidatedRate()
{
    float    tmp_result = 0;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
    if (tests_list.count() > 0) {
        foreach(ExecutionTest *tmp_execution_test, tests_list)
        {
            tmp_result += tmp_execution_test->executionInValidatedRate();
        }
        tmp_result = tmp_result / tests_list.count();
    }

    return tmp_result;
}

float ExecutionCampaign::executionBypassedRate()
{
    float    tmp_result = 0;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
    if (tests_list.count() > 0) {
        foreach(ExecutionTest *tmp_execution_test, tests_list)
        {
            tmp_result += tmp_execution_test->executionBypassedRate();
        }
        tmp_result = tmp_result / tests_list.count();
    }

    return tmp_result;
}


int ExecutionCampaign::executionCountForResult(const char *in_result)
{
    int tmp_count = 0;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
    if (tests_list.isEmpty() == false) {
        foreach(ExecutionTest *tmp_execution_test, tests_list)
        {
            tmp_count += tmp_execution_test->executionCountForResult(in_result);
        }
    }

    return tmp_count;
}


int ExecutionCampaign::indexForParameterWithValueForKey(const char *in_value, const char *in_key)
{
    int        tmp_index = 0;

    for (tmp_index = 0; tmp_index < parametersList().count(); tmp_index++) {
        if (compare_values(parametersList()[tmp_index]->getValueForKey(in_key), in_value) == 0)
            return tmp_index;
    }

    return -1;
}


void ExecutionCampaign::synchronizeFromCampaignDatas()
{
    ExecutionTest                    *tmp_execution_test = NULL;
    int                        tmp_test_campaign_index = 0;
    int                        tmp_execution_test_index = 0;

    QList < TestCampaign* >            tmp_tests_campaign_list = campaign()->testsList();

    QMap< QString, int >            tmp_map;
    QString                    tmp_id;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);

    for (tmp_test_campaign_index = 0; tmp_test_campaign_index < tmp_tests_campaign_list.count(); tmp_test_campaign_index++) {
        tmp_id = QString(tmp_tests_campaign_list[tmp_test_campaign_index]->getValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_ID));
        tmp_map[tmp_id] = 0;
    }

    for (tmp_test_campaign_index = 0; tmp_test_campaign_index < tmp_tests_campaign_list.count(); tmp_test_campaign_index++) {
        tmp_id = QString(tmp_tests_campaign_list[tmp_test_campaign_index]->getValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_ID));

        Test *tmp_test = Test::TestCampaignRelation::instance().getParent(tmp_tests_campaign_list[tmp_test_campaign_index]);

        tmp_execution_test_index = indexForItemWithValueForKeyAtPosition(tmp_tests_campaign_list[tmp_test_campaign_index]->getValueForKey(TESTS_CAMPAIGNS_TABLE_TEST_ID), EXECUTIONS_TESTS_TABLE_TEST_ID, tmp_map[tmp_id]);
        if (tmp_execution_test_index < 0) {
            tmp_execution_test = new ExecutionTest(this);
            tmp_execution_test->createExecutionTestHierarchyForTest(tmp_test);
            ExecutionCampaign::ExecutionTestRelation::instance().insertChild(this, tmp_test_campaign_index, tmp_execution_test);
        }
        else {
            tmp_map[tmp_id] = tmp_map[tmp_id] + 1;

            if (tmp_execution_test_index != tmp_test_campaign_index) {
                ExecutionCampaign::ExecutionTestRelation::instance().moveChild(this, tmp_execution_test_index, tmp_test_campaign_index);
                tests_list[tmp_test_campaign_index]->synchronizeFromTestDatas(tmp_test->getAllChilds());
            }
            else {
                tests_list[tmp_test_campaign_index]->synchronizeFromTestDatas(tmp_test->getAllChilds());
            }
        }
    }


    for (tmp_test_campaign_index = tmp_tests_campaign_list.count(); tmp_test_campaign_index < tests_list.count(); /* no increment */) {
        tmp_execution_test = tests_list[tmp_test_campaign_index];
        ExecutionCampaign::ExecutionTestRelation::instance().removeChild(this, tmp_execution_test);
    }

    synchronizeFromProjectParameters();
}


void ExecutionCampaign::synchronizeFromProjectParameters()
{
    int                                tmp_parameter_index = 0;
    int                                tmp_execution_parameter_index = 0;

    QList < ProjectParameter* >        tmp_parameters = Project::ProjectParameterRelation::instance().getChilds(campaign()->projectVersion()->project());
    ExecutionCampaignParameter*        tmp_parameter = NULL;

    for (tmp_parameter_index = 0; tmp_parameter_index < tmp_parameters.count(); tmp_parameter_index++) {
        tmp_execution_parameter_index = indexForParameterWithValueForKey(tmp_parameters[tmp_parameter_index]->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME);
        if (tmp_execution_parameter_index < 0) {
            tmp_parameter = new ExecutionCampaignParameter();
            tmp_parameter->setValueForKey(tmp_parameters[tmp_parameter_index]->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME);
            tmp_parameter->setValueForKey(tmp_parameters[tmp_parameter_index]->getValueForKey(PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE), EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_VALUE);
            ExecutionCampaign::ExecutionCampaignParameterRelation::instance().addChild(this, tmp_parameter);
        }
    }
}



int ExecutionCampaign::indexForItemWithValueForKeyAtPosition(const char *in_value, const char *in_key, int position)
{
    int        tmp_index = 0;
    int        tmp_count = 0;

    const QList<ExecutionTest*> &tests_list = ExecutionCampaign::ExecutionTestRelation::instance().getChilds(this);
    foreach(ExecutionTest* tmp_child, tests_list)
    {
        if (compare_values(tmp_child->getValueForKey(in_key), in_value) == 0) {
            if (tmp_count == position)
                return tmp_index;

            ++tmp_count;
        }
        ++tmp_index;
    }

    return -1;
}

}
