/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef ACTION_H
#define ACTION_H

#include "entities/entity.h"
#include "executionaction.h"

namespace Entities
{
class Test;

class Action :
    public Entity<Action, &actions_table_def>
{
private:
    QList<Action*>  m_associated_test_actions;

    QList<Action*> loadAssociatedTestActionsForVersion(const char *in_project_version);
    bool appendAssociatedTestActionsToAction(QList<Action*> & in_actions_list, const char *in_original_test_id, const char *in_project_version);

public:
    Action();
    ~Action();

    int saveRecord();

    bool loadAssociatedActionsForVersion(const char *in_project_version, const char *in_original_test_content_id = NULL);
    bool setAssociatedTest(Test *in_test);
    QList<Action*> associatedTestActions();

    ADD_RELATION(Action, ExecutionAction, EXECUTIONS_ACTIONS_TABLE_ACTION_ID);


};

}

#endif // ACTION_H
