#include "featurecontent.h"
#include "projectversion.h"

namespace Entities
{

FeatureContent::FeatureContent()
{
}


FeatureContent::FeatureContent(ProjectVersion *in_project_version)
{
    setProjectVersion(in_project_version);
}


void FeatureContent::setProjectVersion(ProjectVersion* in_project_version)
{
    if (in_project_version != NULL)
    {
        ProjectVersion::FeatureContentRelation::instance().setParent(in_project_version, this);;
        if (is_empty_string(getIdentifier()))
        {
            setValueForKey(in_project_version->getIdentifier(), FEATURES_CONTENTS_TABLE_PROJECT_VERSION_ID);
            setValueForKey(in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), FEATURES_CONTENTS_TABLE_PROJECT_ID);
            setValueForKey(in_project_version->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), FEATURES_CONTENTS_TABLE_VERSION);
        }
    }
}

FeatureContent* FeatureContent::copy() const
{
    FeatureContent *tmp_copy = new FeatureContent();

    tmp_copy->setValueForKey(getValueForKey(FEATURES_CONTENTS_TABLE_SHORT_NAME),FEATURES_CONTENTS_TABLE_SHORT_NAME);
    tmp_copy->setValueForKey(getValueForKey(FEATURES_CONTENTS_TABLE_DESCRIPTION),FEATURES_CONTENTS_TABLE_DESCRIPTION);

    return tmp_copy;
}

}
