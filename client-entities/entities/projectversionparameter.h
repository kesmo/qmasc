#ifndef PROJECTVERSIONPARAMETER_H
#define PROJECTVERSIONPARAMETER_H

#include "hierarchyentity.h"
#include "parameter.h"

namespace Entities
{
class ProjectVersion;

class ProjectVersionParameter:
    public HierarchyEntity<ProjectVersionParameter, &projects_versions_parameters_table_def, nullptr, PROJECTS_VERSIONS_PARAMETERS_TABLE_PARENT_GROUP_PARAMETER_ID>,
    public Parameter
{

public:
    ProjectVersionParameter();
    ProjectVersionParameter(ProjectVersion *in_project_version);

    void setProjectVersion(ProjectVersion* in_project_version);
    ProjectVersion *projectVersion();

    int saveRecord();

    const char* name();
    const char* value();

    void setValue(const char* value){setValueForKey(value, PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_VALUE);}

    ProjectVersionParameter* parentProjectVersionParameter();
};

}

#endif // PROJECTVERSIONPARAMETER_H
