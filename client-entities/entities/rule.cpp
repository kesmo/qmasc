#include "rule.h"
#include "projectversion.h"
#include "rulecontent.h"
#include "session.h"

namespace Entities
{

/**
  Constructeur
**/
BasicRule::BasicRule() : Entity()
{
}


Rule::Rule()
{
}

Rule::Rule(ProjectVersion *in_project_version)
{
    setProjectVersion(in_project_version);
}


Rule::Rule(Rule *in_parent)
{
    if (in_parent != NULL)
    {
        setParent(in_parent);
        setProjectVersion(in_parent->projectVersion());
    }

}

void Rule::setProjectVersion(ProjectVersion* in_project_version)
{
    if (in_project_version != NULL)
    {
        ProjectVersion::RuleRelation::instance().setParent(in_project_version, this);
        if (is_empty_string(getIdentifier())) {
            setValueForKey(projectVersion()->getIdentifier(), RULES_TABLE_PROJECT_VERSION_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), RULES_TABLE_PROJECT_ID);
            setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), RULES_TABLE_VERSION);
        }
    }
}

ProjectVersion* Rule::projectVersion()
{
    return ProjectVersion::RuleRelation::instance().getParent(this);
}

Rule* Rule::parentRule()
{
    return getParent();
}

void Rule::setDataFromRuleContent(RuleContent *in_rule_content)
{
    if (in_rule_content != NULL)
    {
        setValueForKey(in_rule_content->getIdentifier(), RULES_HIERARCHY_RULE_CONTENT_ID);
        setValueForKey(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_SHORT_NAME), RULES_HIERARCHY_SHORT_NAME);
        setValueForKey(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_VERSION), RULES_HIERARCHY_CONTENT_VERSION);
        setValueForKey(in_rule_content->getValueForKey(RULES_CONTENTS_TABLE_STATUS), RULES_HIERARCHY_STATUS);
    }
}



int Rule::saveRecord()
{
    int tmp_result = NOERR;
    BasicRule tmp_rule_record;
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PARENT_RULE_ID), RULES_TABLE_PARENT_RULE_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PREVIOUS_RULE_ID), RULES_TABLE_PREVIOUS_RULE_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PROJECT_ID), RULES_TABLE_PROJECT_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PROJECT_VERSION_ID), RULES_TABLE_PROJECT_VERSION_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_RULE_CONTENT_ID), RULES_TABLE_RULE_CONTENT_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_VERSION), RULES_TABLE_VERSION);
    tmp_rule_record.applyChanges();

    if (parentRule() != NULL)
    {
        if (is_empty_string(parentRule()->getIdentifier()))
        {
            tmp_result = parentRule()->saveRecord();
            if (tmp_result == NOERR)
                setValueForKey(parentRule()->getIdentifier(), RULES_HIERARCHY_PARENT_RULE_ID);
            else
                return tmp_result;
        }
        else
            setValueForKey(parentRule()->getIdentifier(), RULES_HIERARCHY_PARENT_RULE_ID);

        setValueForKey(parentRule()->getValueForKey(RULES_HIERARCHY_PROJECT_VERSION_ID), RULES_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(parentRule()->getValueForKey(RULES_HIERARCHY_PROJECT_ID), RULES_HIERARCHY_PROJECT_ID);
        setValueForKey(parentRule()->getValueForKey(RULES_HIERARCHY_VERSION), RULES_HIERARCHY_VERSION);
    }
    else
        setValueForKey(NULL, RULES_HIERARCHY_PARENT_RULE_ID);

    if (projectVersion() != NULL)
    {
        setValueForKey(projectVersion()->getIdentifier(), RULES_HIERARCHY_PROJECT_VERSION_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_PROJECT_ID), RULES_HIERARCHY_PROJECT_ID);
        setValueForKey(projectVersion()->getValueForKey(PROJECTS_VERSIONS_TABLE_VERSION), RULES_HIERARCHY_VERSION);
    }

    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PARENT_RULE_ID), RULES_TABLE_PARENT_RULE_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PREVIOUS_RULE_ID), RULES_TABLE_PREVIOUS_RULE_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PROJECT_ID), RULES_TABLE_PROJECT_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_PROJECT_VERSION_ID), RULES_TABLE_PROJECT_VERSION_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_RULE_CONTENT_ID), RULES_TABLE_RULE_CONTENT_ID);
    tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_VERSION), RULES_TABLE_VERSION);

    if (is_empty_string(getIdentifier()))
    {
        tmp_result = tmp_rule_record.insertRecord();
        if (tmp_result == NOERR)
        {
            setValueForKey(tmp_rule_record.getIdentifier(), RULES_HIERARCHY_RULE_ID);
            applyChanges();
        }
    }
    else
    {
        tmp_rule_record.setValueForKey(getValueForKey(RULES_HIERARCHY_RULE_ID), RULES_TABLE_RULE_ID);
        tmp_result = tmp_rule_record.saveRecord();
        if (tmp_result == NOERR)
        {
            applyChanges();
        }
    }

    return tmp_result;
}

int Rule::deleteRecord()
{
    int tmp_result = NOERR;

    if (is_empty_string(getIdentifier()) == FALSE)
    {
        BasicRule tmp_rule;
        tmp_rule.setValueForKey(getIdentifier(), RULES_TABLE_RULE_ID);
        tmp_result = tmp_rule.deleteRecord();
    }

    return tmp_result;
}


bool Rule::isModifiable()
{
    if (projectVersion() == NULL)
        return false;

    return projectVersion()->canWriteRules();
}


}
