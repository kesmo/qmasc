/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef EXECUTIONCAMPAIGN_H
#define EXECUTIONCAMPAIGN_H

#include "executiontest.h"
#include "executioncampaignparameter.h"
#include "executionrequirement.h"

namespace Entities
{
class Campaign;

class ExecutionCampaign :
    public Entity<ExecutionCampaign, &executions_campaigns_table_def>
{
private:

    virtual int indexForItemWithValueForKeyAtPosition(const char *in_value, const char *in_key, int position);

public:
    ExecutionCampaign();
    ExecutionCampaign(Campaign *in_campaign);

    static ExecutionCampaign* create(Campaign *in_campaign);
    static ExecutionCampaign *createFromTestsPlan(Campaign *in_campaign);

    Campaign* campaign();

    void addParameter(ExecutionCampaignParameter *in_parameter);
    void removeParameter(ExecutionCampaignParameter *in_parameter);
    const QList<ExecutionCampaignParameter *> &parametersList();

    float executionCoverageRate();
    float executionValidatedRate();
    float executionInValidatedRate();
    float executionBypassedRate();
    int executionCountForResult(const char *in_result);

    int saveRecord();

    int indexForParameterWithValueForKey(const char *in_value, const char *in_key);

    bool hasChangedValues();

    void synchronizeFromCampaignDatas();
    void synchronizeFromProjectParameters();

    ADD_RELATION(ExecutionCampaign, ExecutionTest, EXECUTIONS_TESTS_TABLE_EXECUTION_CAMPAIGN_ID, EXECUTIONS_TESTS_TABLE_PREVIOUS_EXECUTION_TEST_ID, EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID);
    ADD_RELATION(ExecutionCampaign, ExecutionRequirement, EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_CAMPAIGN_ID);
    ADD_RELATION(ExecutionCampaign, ExecutionCampaignParameter, EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_ID);
};

}

#endif // EXECUTIONCAMPAIGN_H
