/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "automatedaction.h"
#include "testcontent.h"
#include "testcontentfile.h"
#include "session.h"

namespace Entities
{

static QString getKeyLabel(int scancode)
{

    int vkcode = 0;

#if (defined(_WINDOWS) || defined (WIN32))

    vkcode = MapVirtualKey(scancode, MAPVK_VSC_TO_VK_EX);
    if (vkcode == 0)
        vkcode = scancode;

    // 0 to 9
    if (vkcode >= 0x30 && vkcode <= 0x39)
        return QString("[%1]").arg(QChar('0' + vkcode - 0x30));

    // 0 to 9 : numpad
    if (vkcode >= 0x60 && vkcode <= 0x69)
        return QString("[%1]").arg(QChar('0' + vkcode - 0x60));

    // A to Z
    if (vkcode >= 0x41 && vkcode <= 0x5A)
        return QString("[%1]").arg(QChar('A' + vkcode - 0x41));

    // F1 to F24
    if (vkcode >= VK_F1 && vkcode <= VK_F24)
        return QString("[F%1]").arg(QString::number(vkcode - VK_F1 + 1));

    switch (vkcode) {
    case VK_BACK:
        return "[Backspace]";
    case VK_TAB:
        return "[Tabulation]";
    case VK_CLEAR:
        return "[Suppr]";
    case VK_RETURN:
        return "[Entrée]";
    case VK_SHIFT:
        return "[Majuscule]";
    case VK_LSHIFT:
        return "[Majuscule gauche]";
    case VK_RSHIFT:
        return "[Majuscule droite]";
    case VK_CONTROL:
        return "[Ctrl]";
    case VK_LCONTROL:
        return "[Ctrl gauche]";
    case VK_RCONTROL:
        return "[Ctrl droite]";
    case VK_MENU:
        return "[Alt]";
    case VK_LMENU:
        return "[Alt gauche]";
    case VK_RMENU:
        return "[Alt droite]";
    case VK_PAUSE:
        return "[Pause]";
    case VK_CAPITAL:
        return "[Majuscule]";
    case VK_ESCAPE:
        return "[Echap]";
    case VK_SPACE:
        return "[Espace]";
    case VK_PRIOR:
        return "[Page up]";
    case VK_NEXT:
        return "[Page down]";
    case VK_END:
        return "[Fin]";
    case VK_LEFT:
        return "[Flêche gauche]";
    case VK_UP:
        return "[Flêche haut]";
    case VK_RIGHT:
        return "[Flêche droite]";
    case VK_DOWN:
        return "[Flêche bas]";
    case VK_SELECT:
        return "[Select]";
    case VK_PRINT:
        return "[Impr]";
    case VK_INSERT:
        return "[Inser]";
    case VK_DELETE:
        return "[Suppr]";
    case VK_NUMLOCK:
        return "[Verr Num]";
    case VK_SCROLL:
        return "[Scroll]";

    }
#endif

    return QString("[%1]").arg(vkcode);
}



QVariant FunctionAutomatedActionMessageDelay(Record* record, int role)
{
    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
    case Qt::ToolTipRole:
        int tmp_delay = atoi(record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY));
        return TR_CUSTOM_MESSAGE("%1 sec %2 ms").arg(tmp_delay / 1000).arg(tmp_delay % 1000);
        break;
    }

    return QVariant();
}


QVariant FunctionAutomatedActionMessageCallback(Record* record, int role)
{

    AutomatedAction* tmp_automated_action = dynamic_cast<AutomatedAction*>(record);
    AutomatedActionValidation* tmp_automated_action_validation = NULL;

    if (tmp_automated_action) {
        QList<AutomatedActionValidation*> tmp_automated_action_validations = AutomatedAction::AutomatedActionValidationRelation::instance().getChilds(tmp_automated_action);
        QString tmp_text;

        switch (role) {
        case Qt::DisplayRole:
        case Qt::EditRole:
            if (tmp_automated_action_validations.isEmpty()) {
                tmp_text = "DefaultMessageCallback";
            }
            else {
                for (int tmp_index = 0; tmp_index < tmp_automated_action_validations.count(); ++tmp_index) {
                    tmp_automated_action_validation = tmp_automated_action_validations[tmp_index];
                    if (tmp_index < tmp_automated_action_validations.count() - 1)
                        tmp_text += QString("%1\n")
                        .arg(tmp_automated_action_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME));
                    else
                        tmp_text += QString("%1")
                        .arg(tmp_automated_action_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME));
                }
            }
            return tmp_text;

        case Qt::ToolTipRole:
            if (tmp_automated_action_validations.isEmpty()) {
                tmp_text = TR_CUSTOM_MESSAGE("<p><b>DefaultMessageCallback</b> : fonction par défaut qui vérifie que la fenêtre active est la même que celle enregistrée lors de l'exécution de l'action</p>");
            }
            else {
                foreach(AutomatedActionValidation* tmp_automated_action_validation, tmp_automated_action_validations)
                {
                    QString tmp_module_name(tmp_automated_action_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME));
                    QString tmp_module_version(tmp_automated_action_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION));
                    QString tmp_function_name(tmp_automated_action_validation->getValueForKey(AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME));
                    AutomationCallbackFunction* tmp_callback = Session::instance().getAutomationCallbackFunction(tmp_module_name, tmp_module_version, tmp_function_name);
                    tmp_text += TR_CUSTOM_MESSAGE("<p><b>%1</b> (module %2 version %3) : %4</p>")
                        .arg(tmp_function_name)
                        .arg(tmp_module_name)
                        .arg(tmp_module_version)
                        .arg(tmp_callback ? tmp_callback->getDescription() : TR_CUSTOM_MESSAGE("<i>pas de description</i>"));
                }
            }
            return tmp_text;
        }
    }

    return QVariant();
}

QVariant FunctionAutomatedActionMessageData(Record* record, int role)
{
    switch (role) {
    case Qt::ToolTipRole:
        return record->getValueForKey(AUTOMATED_ACTIONS_TABLE_WINDOW_ID);
        break;
    case Qt::DisplayRole:
    case Qt::EditRole:
        switch ((EventMessageType)atoi(record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE))) {
        case Keyboard:
        case Mouse:
        {
            QStringList tmp_data_values = QString(record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA)).split(AutomatedAction::FieldSeparator);
            if (tmp_data_values.count() == 3) {
                switch (atoi(tmp_data_values[0].toStdString().c_str())) {
                    // Mouse
                case EVT_MSG_MOUSE_BUTTON_LEFT_DOWN:
                    return TR_CUSTOM_MESSAGE("Appuyer bouton gauche en x=%1 et y=%2").arg(tmp_data_values[1]).arg(tmp_data_values[2]);
                case EVT_MSG_MOUSE_BUTTON_LEFT_UP:
                    return TR_CUSTOM_MESSAGE("Relâcher bouton gauche en x=%1 et y=%2").arg(tmp_data_values[1]).arg(tmp_data_values[2]);
                case EVT_MSG_MOUSE_BUTTON_RIGHT_DOWN:
                    return TR_CUSTOM_MESSAGE("Appui bouton droit en x=%1 et y=%2").arg(tmp_data_values[1]).arg(tmp_data_values[2]);
                case EVT_MSG_MOUSE_BUTTON_RIGHT_UP:
                    return TR_CUSTOM_MESSAGE("Relâcher bouton droit en x=%1 et y=%2").arg(tmp_data_values[1]).arg(tmp_data_values[2]);
                case EVT_MSG_MOUSE_MOVE:
                    return TR_CUSTOM_MESSAGE("Déplacer en x=%1 et y=%2").arg(tmp_data_values[1]).arg(tmp_data_values[2]);

                    // Keyboard
                case EVT_MSG_KEYBOARD_KEY_DOWN:
                    return TR_CUSTOM_MESSAGE("Appuyer touche %1 (%2)").arg(getKeyLabel(tmp_data_values[1].toInt())).arg(tmp_data_values[2]);
                case EVT_MSG_KEYBOARD_KEY_UP:
                    return TR_CUSTOM_MESSAGE("Relâcher touche %1 (%2)").arg(getKeyLabel(tmp_data_values[1].toInt())).arg(tmp_data_values[2]);
                case EVT_MSG_KEYBOARD_SYSTEM_KEY_DOWN:
                    return TR_CUSTOM_MESSAGE("Appuyer touche système %1 (%2)").arg(getKeyLabel(tmp_data_values[1].toInt())).arg(tmp_data_values[2]);
                case EVT_MSG_KEYBOARD_SYSTEM_KEY_UP:
                    return TR_CUSTOM_MESSAGE("Relâcher touche système %1 (%2)").arg(getKeyLabel(tmp_data_values[1].toInt())).arg(tmp_data_values[2]);
                }
            }

            return record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA);
            break;
        }
        case None:
        case StartRecord:
        case StopRecord:
        default:
            break;
        }

        break;
    }

    return QVariant();
}


QVariant FunctionAutomatedActionMessageType(Record* record, int role)
{
    switch (role) {
    case Qt::DisplayRole:
    case Qt::EditRole:
    case Qt::ToolTipRole:
        switch ((EventMessageType)atoi(record->getValueForKey(AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE))) {
        case StartRecord:
            return TR_CUSTOM_MESSAGE("Début");
        case StopRecord:
            return TR_CUSTOM_MESSAGE("Fin");
        case Keyboard:
            return TR_CUSTOM_MESSAGE("Clavier");
        case Mouse:
            return TR_CUSTOM_MESSAGE("Souris");
        case None:
        default:
            return TR_CUSTOM_MESSAGE("Inconnu");
        }

        break;
    }

    return QVariant();
}


AutomatedAction::AutomatedAction() :
    Entity()
{
}


AutomatedAction::~AutomatedAction()
{
}

int AutomatedAction::saveRecord()
{
    int tmp_return = NOERR;

    if (is_empty_string(getIdentifier())) {
        TestContent *test_content = TestContent::AutomatedActionRelation::instance().getParent(this);
        if (test_content != NULL)
            setValueForKey(test_content->getIdentifier(), AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID);

        tmp_return = Entity::saveRecord();
    }
    else
        tmp_return = Entity::saveRecord();

    if (tmp_return == NOERR) {
        foreach(AutomatedActionValidation* tmp_validation, AutomatedAction::AutomatedActionValidationRelation::instance().getChilds(this))
        {
            AutomatedAction::AutomatedActionValidationRelation::instance().appendChild(this, tmp_validation);
        }

        tmp_return = AutomatedAction::AutomatedActionValidationRelation::instance().saveChilds(this);
    }

    return tmp_return;
}

}
