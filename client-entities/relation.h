#ifndef RELATION_H
#define RELATION_H

#include "singleton.h"
#include "recordlistener.h"
#include "session.h"

#include <assert.h>

#include <QMap>
#include <QList>
#include <QDebug>

#define IMPLEMENT_RELATION(RelName, object, B) \
    const QList<B*>& getAll##RelName##s(const char* in_order_by_clause = NULL, net_callback_fct* in_callback = NULL) {\
        return RelName##Relation::instance().getChilds(object, in_order_by_clause, in_callback);\
    }\
    void add##RelName(B* child) {\
        RelName##Relation::instance().addChild(object, child);\
    }\
    void append##RelName(B* child) {\
        RelName##Relation::instance().appendChild(object, child);\
    }\
    void insert##RelName(int index, B* child) {\
        RelName##Relation::instance().insertChild(object, index, child);\
    }\
    void remove##RelName(B* child) {\
        RelName##Relation::instance().removeChild(object, child);\
    }\
    void detach##RelName##AtIndex(int position) {\
        RelName##Relation::instance().detachChildAtIndex(object, position);\
    }\
    void remove##RelName##AtIndex(int position, int count = 1, bool in_move_indic = false) {\
        RelName##Relation::instance().removeChildAtIndex(object, position, count, in_move_indic);\
    }\
    void removeAll##RelName##s() {\
        RelName##Relation::instance().removeAllChilds(object);\
    }\
    void set##RelName##s(const QList<B*> &in_childs) {\
        RelName##Relation::instance().setChilds(object, in_childs);\
    }\
    void move##RelName(int from, int to) {\
        RelName##Relation::instance().moveChild(object, from, to);\
    }\
    int save##RelName##s() {\
        return RelName##Relation::instance().saveChilds(object);\
    }\
    void reset##RelName##s(bool in_back_to_originals_childs) {\
        RelName##Relation::instance().reset(object, in_back_to_originals_childs);\
    }\
    B* get##RelName##AtIndex(int index) {\
        return RelName##Relation::instance().getChildAtIndex(object, index);\
    }\
    B* getUnique##RelName##WithValueForKey(const char *in_value, const char *in_key) {\
        return RelName##Relation::instance().getUniqueChildWithValueForKey(object, in_value, in_key);\
    }\
    QList<B*> get##RelName##sWithValueForKey(const char *in_value, const char *in_key) {\
        return RelName##Relation::instance().getChildrenWithValueForKey(object, in_value, in_key);\
    }\
    B* getUnique##RelName##WithValuesForKeys(const QList<const char *>& in_values, const QList<const char *>& in_keys) {\
        return RelName##Relation::instance().getUniqueChildWithValuesForKeys(object, in_values, in_keys);\
    }\
    QList<B*> get##RelName##sWithValuesForKeys(const QList<const char *>& in_values, const QList<const char *>& in_keys) {\
        return RelName##Relation::instance().getChildrenWithValuesForKeys(object, in_values, in_keys);\
    }\
    int indexFor##RelName##WithValueForKey(const char *in_value, const char *in_key) {\
        return RelName##Relation::instance().indexForChildWithValueForKey(object, in_value, in_key);\
    }\
    void add##RelName##ParentalityListener(ParentalityListener* parentalityListener) {\
        RelName##Relation::instance().addParentalityListener(object, parentalityListener);\
    }\
    void remove##RelName##ParentalityListener(ParentalityListener* parentalityListener) {\
        RelName##Relation::instance().removeParentalityListener(object, parentalityListener);\
    }\

#define ADD_NAMED_RELATION(RelName, A, B, ...) \
    typedef Relation<A, B, __VA_ARGS__> RelName##Relation;\
    IMPLEMENT_RELATION(RelName, dynamic_cast<A*>(this), B)

#define ADD_RELATION(A, B, ...) \
    ADD_NAMED_RELATION(B, A, B, __VA_ARGS__)

namespace Entities
{
inline void deletePointer(AbstractRecord* in_ptr, RecordListener *recordListener)
{
    if (in_ptr && !in_ptr->isRemoving()) {
        if (recordListener)
            in_ptr->removeRecordListener(recordListener);
        delete in_ptr;
    }
}

class ParentalityListener
{
public:
    virtual void updateChilds(int in_old_rows_count = 0, int in_new_rows_count = 0) = 0;
};

template<typename A, typename B, const char *ForeignKey = nullptr, const char *PreviousKey = nullptr, const char *ParentKey = nullptr>
class Relation :
    public Singleton< Relation<A, B, ForeignKey, PreviousKey, ParentKey> >
{
private:
    typedef Relation<A, B, ForeignKey, PreviousKey, ParentKey> RelationAB;

    template<typename A, typename B, const char *ForeignKey = nullptr, const char *PreviousKey = nullptr, const char *ParentKey = nullptr>
    class ParentRecordListener :
        public RecordListener
    {
    public:
        void recordDestroyed(AbstractRecord* record) Q_DECL_OVERRIDE {
            RelationAB::instance().parentDestroyed(static_cast<A*>(record));
        }
    };
    ParentRecordListener<A, B, ForeignKey, PreviousKey, ParentKey> m_parentListener;

    template<typename A, typename B, const char *ForeignKey = nullptr, const char *PreviousKey = nullptr, const char *ParentKey = nullptr>
    class ChildRecordListener :
        public RecordListener
    {
    public:
        void recordDestroyed(AbstractRecord* record) Q_DECL_OVERRIDE {
            RelationAB::instance().childDestroyed(static_cast<B*>(record));
        }
    };
    ChildRecordListener<A, B, ForeignKey, PreviousKey, ParentKey> m_childListener;

    struct ParentCache {
        QList<B*> m_children;
        QList<B*> m_originalChildren;
        QList<ParentalityListener*> m_parentalityListeners;
        bool m_childrenLoaded;
    };

    QMap<B*, A* > m_parents;
    QMap<A*, ParentCache > m_parentCaches;

    void parentDestroyed(A* parent) {
        if (parent) {
            reset(parent, false);
            QList<B*> children = m_parents.keys();
            foreach(B* child, children) {
                if (parent == m_parents[child]) {
                    m_parents.remove(child);
                }
            }
        }
    }

    void childDestroyed(B *child) {
        if (child) {
            A *parent = m_parents.value(child);
            if (parent) {
                if (m_parentCaches.contains(parent)) {
                    if (!is_empty_string(ParentKey) || !is_empty_string(PreviousKey)) {
                        qWarning() << Q_FUNC_INFO << "Child entity deleted on leveled or ordered children list: use removeChild instead";
                    }
                    ParentCache &children = m_parentCaches[parent];

                    children.m_children.removeOne(child);
                    children.m_originalChildren.removeOne(child);

                    foreach(ParentalityListener* tmp_listener, children.m_parentalityListeners) {
                        tmp_listener->updateChilds(children.m_children.count() + 1, children.m_children.count());
                    }
                }
            }
            m_parents.remove(child);
        }
    }

    void detachChild(A* parent, B* child)
    {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry" << parent << child;
            return;
        }

        if (m_parentCaches.contains(parent)) {
            ParentCache &children = m_parentCaches[parent];
            children.m_children.removeOne(child);
            foreach(ParentalityListener* tmp_listener, children.m_parentalityListeners) {
                tmp_listener->updateChilds(children.m_children.count() + 1, children.m_children.count());
            }
        }
        m_parents.remove(child);
    }

    void _addChild(A* parent, B* child)
    {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry" << parent << child;
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        if (!children.m_children.contains(child)) {
            if (children.m_childrenLoaded
                || (is_empty_string(ParentKey) && is_empty_string(PreviousKey))) {
                children.m_children.append(child);

                foreach(ParentalityListener* tmp_listener, children.m_parentalityListeners) {
                    tmp_listener->updateChilds(children.m_children.count() - 1, children.m_children.count());
                }
            }
            else {
                qWarning() << "Ordered children not loaded";
            }
            setParent(parent, child);
        }
        else {
            qWarning() << "Child already added";
        }
    }


    void commit(A* parent) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry" << parent;
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        foreach(B *child, children.m_originalChildren) {
            if (!children.m_children.contains(child)) {
                deletePointer(child, &m_childListener);
            }
        }

        children.m_originalChildren = children.m_children;
    }

protected:
    void reloadChilds(A* parent, const char* in_order_by_clause = NULL, net_callback_fct* in_callback = NULL)
    {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

        reset(parent, false);

        loadChilds(parent, in_order_by_clause, in_callback);
        commit(parent);
    }

    void loadChilds(A* parent, const char* in_order_by_clause = NULL, net_callback_fct *in_callback = NULL)
    {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

        ParentCache &children = m_parentCaches[parent];
        children.m_childrenLoaded = true;

        if (is_empty_string(ParentKey))
            net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s", ForeignKey, parent->getIdentifier());
        else
            net_session_print_where_clause(Session::instance().getClientSession(), "%s=%s and %s is null", ForeignKey, parent->getIdentifier(), ParentKey);

        children.m_children = B::loadAndOrderRecordsList(PreviousKey,
                                                         Session::instance().getClientSession()->m_where_clause_buffer,
                                                         in_order_by_clause,
                                                         in_callback);
        foreach(B *child, children.m_children)
        {
            setParent(parent, child);
        }
    }

public:
    typedef A ParentClass;
    typedef B ChildClass;

    Relation() {}

    void setParent(A* parent, B* child)
    {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry" << parent << child;
            return;
        }

        A *oldParent = m_parents.value(child);
        if (oldParent) {
            if (oldParent != parent)
                detachChild(oldParent, child);
            else
                return;
        }
        m_parents[child] = parent;
        parent->addRecordListener(&m_parentListener);
        child->addRecordListener(&m_childListener);
    }

    const QList<B*>& getChilds(A* parent, const char* in_order_by_clause = NULL, net_callback_fct* in_callback = NULL) {
        assert(parent);

        if (!m_parentCaches[parent].m_childrenLoaded) {
            reloadChilds(parent, in_order_by_clause, in_callback);
        }

        return m_parentCaches[parent].m_children;
    }

    void addChild(A* parent, B* child) {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry" << parent << child;
            return;
        }

#if _MSC_VER > 1900 // > Visual Studio 2015 version 14.0
        static_assert(ParentKey == nullptr, "Can not add child on leveled children list: use appendChild instead");
        static_assert(PreviousKey == nullptr, "Can not add child on ordered children list: use appendChild instead");
#endif

        if (!is_empty_string(ParentKey) || !is_empty_string(PreviousKey)) {
            qWarning() << Q_FUNC_INFO << "Can not add child on leveled or ordered children list: use appendChild instead";
            setParent(parent, child);
            return;
        }

        _addChild(parent, child);
    }

    void appendChild(A* parent, B* child) {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

#if _MSC_VER > 1900 // > Visual Studio 2015 version 14.0
        static_assert(PreviousKey != nullptr, "Can not append on unordered children list: use addChild instead");
#endif

        if (is_empty_string(PreviousKey)) {
            qWarning() << Q_FUNC_INFO << "Can not append on unordered children list: use addChild instead";
            setParent(parent, child);
            return;
        }

        _addChild(parent, child);
    }

    void insertChild(A* parent, int index, B* child) {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

#if _MSC_VER > 1900 // > Visual Studio 2015 version 14.0
        static_assert(PreviousKey != nullptr, "Can not append on unordered children list: use addChild instead");
#endif

        if (is_empty_string(PreviousKey)) {
            qWarning() << Q_FUNC_INFO << "Can not insert on unordered children list: use addChild instead";
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        if (!children.m_children.contains(child)) {
            if (children.m_childrenLoaded) {
                children.m_children.insert(index, child);
                setParent(parent, child);
            }
            else {
                qWarning() << "Ordered children not loaded";
            }
        }
        else {
            qWarning() << "Child already added";
        }
    }

    void removeChild(A* parent, B* child) {
        if (!parent || !child) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        if (children.m_children.contains(child)) {
            if (children.m_childrenLoaded
                || (is_empty_string(ParentKey) && is_empty_string(PreviousKey))) {
                children.m_children.removeOne(child);
                foreach(ParentalityListener* tmp_listener, children.m_parentalityListeners) {
                    tmp_listener->updateChilds(children.m_children.count() + 1, children.m_children.count());
                }
                if (!children.m_originalChildren.contains(child)) {
                    deletePointer(child, &m_childListener);
                }
            }
            else {
                qWarning() << "Ordered children not loaded";
            }
        }
    }

    void detachChildAtIndex(A* parent, int position)
    {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

#if _MSC_VER > 1900 // > Visual Studio 2015 version 14.0
        static_assert(PreviousKey != nullptr, "Can not append on unordered children list: use addChild instead");
#endif

        if (is_empty_string(PreviousKey)) {
            qWarning() << "Can not detach on unordered children list";
            return;
        }

        ParentCache &children = m_parentCaches[parent];
        if (children.m_childrenLoaded) {
            if (position < children.m_children.count()) {
                B *tmp_child = children.m_children.at(position);
                detachChild(parent, tmp_child);
            }
            else {
                qWarning() << "child index out of bound";
            }
        }
        else {
            qWarning() << "Ordered children not loaded";
        }
    }

    void removeChildAtIndex(A* parent, int position, int count = 1, bool in_move_indic = false) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

#if _MSC_VER > 1900 // > Visual Studio 2015 version 14.0
        static_assert(PreviousKey != nullptr, "Can not remove on unordered children list: use removeChild instead");
#endif

        if (is_empty_string(PreviousKey)) {
            qWarning() << Q_FUNC_INFO "Can not remove on unordered children list";
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        if (children.m_childrenLoaded) {
            for (int tmp_index = position; (tmp_index < position + count) && tmp_index < children.m_children.count(); tmp_index++) {
                B *child = children.m_children.takeAt(position);

                if (in_move_indic == false) {
                    if (!children.m_originalChildren.contains(child)) {
                        deletePointer(child, &m_childListener);
                    }
                }
            }
        }
        else {
            qWarning() << "Ordered children not loaded";
        }
    }

    void removeAllChilds(A* parent) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        if (children.m_childrenLoaded) {
            int count = children.m_children.count();
            foreach(B* tmp_child, children.m_children) {
                if (!children.m_originalChildren.contains(tmp_child)) {
                    deletePointer(tmp_child, &m_childListener);
                }
            }
            children.m_children.clear();
            foreach(ParentalityListener* tmp_listener, children.m_parentalityListeners) {
                tmp_listener->updateChilds(count, 0);
            }
        }
        else {
            qWarning() << "Children not loaded";
        }
    }

    void setChilds(A* parent, const QList<B*> &in_childs) {
        removeAllChilds(parent);
        foreach(B *child, in_childs) {
            addChild(parent, child);
        }
    }

    void moveChild(A* parent, int from, int to) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

        m_parentCaches[parent].m_children.move(from, to);
    }

    int saveChilds(A* parent) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return EMPTY_OBJECT;
        }

        int tmp_return_result = NOERR;
        const char* tmp_previous_identifier = NULL;
        ParentCache &children = m_parentCaches[parent];
        B *child = nullptr;

        for (int tmp_index = 0; tmp_index < children.m_children.count() && tmp_return_result == NOERR; ++tmp_index) {
            child = children.m_children[tmp_index];
            if (!is_empty_string(ParentKey))
                child->setValueForKey(parent->getIdentifier(), ForeignKey);
            if (!is_empty_string(PreviousKey))
                child->setValueForKey(tmp_previous_identifier, PreviousKey);
            tmp_return_result = child->saveRecord();
            tmp_previous_identifier = child->getIdentifier();
        }

        for (int tmp_index = 0; tmp_index < children.m_originalChildren.count() && tmp_return_result == NOERR; ++tmp_index) {
            child = children.m_originalChildren[tmp_index];
            if (!children.m_children.contains(child)) {
                A *otherParent = m_parents.value(child);
                if (otherParent) {
                    if (otherParent == parent) {
                        tmp_return_result = child->deleteRecord();
                    }
                    else {
                        child->setValueForKey(otherParent->getIdentifier(), ForeignKey);
                    }
                }
                else {
                    child->setValueForKey(NULL, ForeignKey);
                }
            }
        }

        if (tmp_return_result == NOERR)
            commit(parent);

        return tmp_return_result;
    }

    void reset(A* parent, bool in_back_to_originals_childs) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return;
        }

        ParentCache &children = m_parentCaches[parent];

        int tmp_old_rows_count = children.m_originalChildren.count();
        QList<B*> pointersToDelete;

        foreach(B* tmp_child, children.m_children) {
            if (!children.m_originalChildren.contains(tmp_child)
                && m_parents.value(tmp_child) == parent) {
                pointersToDelete.append(tmp_child);
            }
        }

        if (in_back_to_originals_childs) {
            children.m_children = children.m_originalChildren;
            foreach(B *child, children.m_children) {
                setParent(parent, child);
            }
        }
        else {

            foreach(B* tmp_child, children.m_originalChildren) {
                pointersToDelete.append(tmp_child);
            }

            children.m_children.clear();
            children.m_originalChildren.clear();
            children.m_childrenLoaded = false;
        }

        foreach(ParentalityListener* tmp_listener, children.m_parentalityListeners) {
            tmp_listener->updateChilds(tmp_old_rows_count, children.m_children.count());
        }

        foreach(B* tmp_child, pointersToDelete) {
            deletePointer(tmp_child, &m_childListener);
        }

        if (!in_back_to_originals_childs) {
            m_parentCaches.remove(parent);
        }
    }

    B* getChildAtIndex(A* parent, int index) {
        const QList<B*>& childList = getChilds(parent);
        if (index >= 0 && index < childList.size()) {
            return childList[index];
        }

        return NULL;
    }

    B* getUniqueChildWithValueForKey(A* parent, const char *in_value, const char *in_key) {
        QList<B*> result(getChildrenWithValueForKey(parent, in_value, in_key));
        if (result.count() == 1)
            return result.first();

        return NULL;
    }

    QList<B*> getChildrenWithValueForKey(A* parent, const char *in_value, const char *in_key) {
        const QList<B*>& childList = getChilds(parent);
        QList<B*> result;

        foreach(B* tmp_child, childList) {
            if (compare_values(tmp_child->getValueForKey(in_key), in_value) == 0)
                result.append(tmp_child);
        }

        return result;
    }

    B* getUniqueChildWithValuesForKeys(A* parent, const QList<const char *>& in_values, const QList<const char *>& in_keys)
    {
        QList<B*> result(getChildrenWithValuesForKeys(parent, in_values, in_keys));
        if (result.count() == 1)
            return result.first();

        return NULL;
    }

    QList<B*> getChildrenWithValuesForKeys(A* parent, const QList<const char *>& in_values, const QList<const char *>& in_keys)
    {
        const QList<B*>& childList = getChilds(parent);
        QList<B*> result;

        if (in_values.count() == in_keys.count()) {
            foreach(B* tmp_child, childList)
            {
                bool found = false;
                for(int tmp_value_index = 0; tmp_value_index < in_values.count(); ++tmp_value_index)
                {
                    found = (compare_values(tmp_child->getValueForKey(in_keys[tmp_value_index]), in_values[tmp_value_index]) == 0);
                    if (!found) {
                        break;
                    }
                }
                if (found) {
                    result.append(tmp_child);
                }
            }
        }

        return result;
    }

    int indexForChildWithValueForKey(A* parent, const char *in_value, const char *in_key) {
        if (!parent) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return -1;
        }

        int tmp_index = 0;
        const QList<B*>& childList = getChilds(parent);

        foreach(B* tmp_child, childList) {
            if (compare_values(tmp_child->getValueForKey(in_key), in_value) == 0)
                return tmp_index;

            ++tmp_index;
        }

        return -1;
    }

    A* getParent(B* child) {
        if (!child) {
            qWarning() << Q_FUNC_INFO << "null entry";
            return nullptr;
        }

        A *parentA = m_parents.value(child);
        if (!parentA && !is_empty_string(ParentKey)) {
            B *parentB = Relation<B, B, ParentKey, PreviousKey>::instance().getParent(child);
            if (parentB) {
                parentA = getParent(parentB);
                if (parentA)
                    setParent(parentA, child);
            }
        }

        if (!parentA && !is_empty_string(ForeignKey)) {
            const char *pkA = child->getValueForKey(ForeignKey);
            if (!is_empty_string(pkA)) {
                parentA = A::getEntity(pkA);
                if (parentA)
                    setParent(parentA, child);
            }
        }

        return parentA;
    }

    void addParentalityListener(A* parent, ParentalityListener* parentalityListener) {
        ParentCache &children = m_parentCaches[parent];

        if (parentalityListener && !children.m_parentalityListeners.contains(parentalityListener))
            children.m_parentalityListeners.append(parentalityListener);
    }

    void removeParentalityListener(A* parent, ParentalityListener* parentalityListener) {
        ParentCache &children = m_parentCaches[parent];
        if (parent)
            children.m_parentalityListeners.removeAll(parentalityListener);
    }

};

}

#endif // RELATION_H
