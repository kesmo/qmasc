#include "entitiesmodel.h"
#include "entities/project.h"
#include "entities/projectgrant.h"
#include "entities/projectparameter.h"
#include "entities/projectversion.h"
#include "entities/projectversionparameter.h"
#include "entities/requirementcategory.h"
#include "entities/requirement.h"
#include "entities/requirementcontent.h"
#include "entities/test.h"
#include "entities/testcontent.h"
#include "entities/testresult.h"
#include "entities/testrequirement.h"
#include "entities/action.h"
#include "entities/actionresult.h"
#include "entities/status.h"
#include "entities/testtype.h"
#include "entities/customfielddesc.h"
#include "entities/campaign.h"
#include "entities/testcampaign.h"
#include "entities/executioncampaign.h"
#include "entities/executionaction.h"
#include "entities/executiontest.h"
#include "entities/executiontestparameter.h"
#include "entities/executioncampaignparameter.h"
#include "entities/executionrequirement.h"
#include "entities/need.h"
#include "entities/feature.h"
#include "entities/featurecontent.h"
#include "entities/rule.h"
#include "entities/rulecontent.h"
#include "entities/automatedaction.h"
#include "entities/automatedactionvalidation.h"
#include "entities/customrequirementfield.h"
#include "entities/testsplan/testsplan.h"
#include "entities/testsplan/graphicpoint.h"
#include "entities/testsplan/graphicloop.h"
#include "entities/testsplan/graphicswitch.h"
#include "entities/testsplan/graphicif.h"
#include "entities/testsplan/graphiclink.h"
#include "entities/testsplan/graphictest.h"

#include <assert.h>

namespace Relations
{
void EntitiesModel::initialize()
{
    EntityFactory<PROJECTS_TABLE_SIG_ID, Project>::instance();
    EntityFactory<PROJECTS_VERSIONS_TABLE_SIG_ID, ProjectVersion>::instance();
    EntityFactory<TESTS_CONTENTS_TABLE_SIG_ID, TestContent>::instance();
    EntityFactory<TESTS_TABLE_SIG_ID, BasicTest>::instance();
    EntityFactory<ACTIONS_TABLE_SIG_ID, Action>::instance();
    EntityFactory<REQUIREMENTS_CONTENTS_TABLE_SIG_ID, RequirementContent>::instance();
    EntityFactory<REQUIREMENTS_TABLE_SIG_ID, BasicRequirement>::instance();
    EntityFactory<TESTS_REQUIREMENTS_TABLE_SIG_ID, TestRequirement>::instance();
    EntityFactory<PROJECTS_PARAMETERS_TABLE_SIG_ID, ProjectParameter>::instance();
    EntityFactory<AUTOMATED_ACTIONS_TABLE_SIG_ID, AutomatedAction>::instance();
    EntityFactory<REQUIREMENTS_CATEGORIES_TABLE_SIG_ID, RequirementCategory>::instance();
    EntityFactory<CAMPAIGNS_TABLE_SIG_ID, Campaign>::instance();
    EntityFactory<TESTS_CAMPAIGNS_TABLE_SIG_ID, TestCampaign>::instance();
    EntityFactory<EXECUTIONS_CAMPAIGNS_TABLE_SIG_ID, ExecutionCampaign>::instance();
    EntityFactory<EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_SIG_ID, ExecutionCampaignParameter>::instance();
    EntityFactory<EXECUTIONS_TESTS_TABLE_SIG_ID, ExecutionTest>::instance();
    EntityFactory<TESTS_RESULTS_TABLE_SIG_ID, TestResult>::instance();
    EntityFactory<EXECUTIONS_ACTIONS_TABLE_SIG_ID, ExecutionAction>::instance();
    EntityFactory<ACTIONS_RESULTS_TABLE_SIG_ID, ActionResult>::instance();
    EntityFactory<EXECUTIONS_REQUIREMENTS_TABLE_SIG_ID, ExecutionRequirement>::instance();
    EntityFactory<TESTS_CONTENTS_FILES_TABLE_SIG_ID, TestContent>::instance();
    EntityFactory<PROJECTS_GRANTS_TABLE_SIG_ID, ProjectGrant>::instance();
    EntityFactory<TESTS_HIERARCHY_SIG_ID, Test>::instance();
    EntityFactory<REQUIREMENTS_HIERARCHY_SIG_ID, Requirement>::instance();
    EntityFactory<BUGS_TABLE_SIG_ID, Bug>::instance();
    EntityFactory<STATUS_TABLE_SIG_ID, Status>::instance();
    EntityFactory<TESTS_TYPES_TABLE_SIG_ID, TestType>::instance();
    EntityFactory<EXECUTIONS_TESTS_PARAMETERS_TABLE_SIG_ID, ExecutionTestParameter>::instance();
    EntityFactory<AUTOMATED_ACTIONS_VALIDATIONS_TABLE_SIG_ID, AutomatedActionValidation>::instance();
    EntityFactory<CUSTOM_FIELDS_DESC_TABLE_SIG_ID, CustomFieldDesc>::instance();
    EntityFactory<CUSTOM_TEST_FIELDS_TABLE_SIG_ID, CustomTestField>::instance();
    EntityFactory<CUSTOM_REQUIREMENT_FIELDS_TABLE_SIG_ID, CustomRequirementField>::instance();
    EntityFactory<NEEDS_TABLE_SIG_ID, Need>::instance();
    EntityFactory<FEATURES_CONTENTS_TABLE_SIG_ID, FeatureContent>::instance();
    EntityFactory<FEATURES_TABLE_SIG_ID, Feature>::instance();
    //EntityFactory<FEATURES_HIERARCHY_SIG_ID, Feature>::instance();
    EntityFactory<RULES_CONTENTS_TABLE_SIG_ID, RuleContent>::instance();
    EntityFactory<RULES_TABLE_SIG_ID, Rule>::instance();
    //EntityFactory<RULES_HIERARCHY_SIG_ID, Rule>::instance();
    EntityFactory<TESTS_PLANS_TABLE_SIG_ID, TestsPlan>::instance();
    EntityFactory<GRAPHICS_ITEMS_TABLE_SIG_ID, GraphicItem>::instance();
    EntityFactory<GRAPHICS_POINTS_TABLE_SIG_ID, GraphicPoint>::instance();
    EntityFactory<GRAPHICS_TESTS_TABLE_SIG_ID, GraphicTest>::instance();
    EntityFactory<GRAPHICS_IFS_TABLE_SIG_ID, GraphicIf>::instance();
    EntityFactory<GRAPHICS_SWITCHES_TABLE_SIG_ID, GraphicSwitch>::instance();
    EntityFactory<GRAPHICS_LOOPS_TABLE_SIG_ID, GraphicLoop>::instance();
    EntityFactory<GRAPHICS_LINKS_TABLE_SIG_ID, GraphicLink>::instance();
    EntityFactory<PROJECTS_VERSIONS_PARAMETERS_TABLE_SIG_ID, ProjectVersionParameter>::instance();
}

void EntitiesModel::addFactory(int id, AbstractEntityFactory *factory)
{
    m_factories.insert(id, factory);
}

Record* EntitiesModel::createEntity(int in_signature)
{
    if (m_factories.contains(in_signature)) {
        return m_factories.value(in_signature)->createEntity();
    }

    qWarning() << Q_FUNC_INFO << "Unknown entity factory" << in_signature;

    return NULL;
}

void EntitiesModel::logAvailableEntities()
{
    foreach(AbstractEntityFactory *factory, m_factories.values()) {
        factory->logAvailableEntities();
    }
}

void EntitiesModel::createEntitiesLoggerContext(const QString &name)
{
    qDebug() << Q_FUNC_INFO << name;
    QList<Record*> list;
    foreach(AbstractEntityFactory *factory, m_factories.values()) {
        list.append(factory->getAllEntities());
    }

    m_loggerContext[name] = list;
}

void EntitiesModel::removeEntitiesLoggerContext(const QString &name)
{
    qDebug() << Q_FUNC_INFO << name;
    QList<Record*> list;
    foreach(AbstractEntityFactory *factory, m_factories.values()) {
        list.append(factory->getAllEntities());
    }

    QString msg;
    foreach(Record* record, list)
    {
        if (m_loggerContext[name].indexOf(record) < 0)
            msg.append(QString("%1 (%2), ").arg(record->getEntityDef()->m_entity_name).arg(record->getIdentifier()));
    }

    if (!msg.isEmpty())
        qDebug() << Q_FUNC_INFO << "New records (not freed):" << msg;

    QList<Record*> oldrecords;
    msg.clear();
    foreach(Record* record, m_loggerContext[name])
    {
        if (list.indexOf(record) < 0)
            msg.append(QString("%1, ").arg((quintptr)record));
    }

    if (!msg.isEmpty())
        qDebug() << Q_FUNC_INFO << "Old records (freed):" << msg;

    m_loggerContext.remove(name);
}


}
