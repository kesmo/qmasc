#ifndef RECORDLISTENER_H
#define RECORDLISTENER_H

class AbstractRecord;

class RecordListener {
public:
    RecordListener();
    virtual ~RecordListener();
    virtual void recordDestroyed(AbstractRecord* record) = 0;
};


#endif // RECORDLISTENER_H
