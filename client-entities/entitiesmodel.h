#ifndef ENTITIESMODEL_H
#define ENTITIESMODEL_H

#include "singleton.h"
#include "entities/entity.h"

namespace Relations
{
class AbstractEntityFactory
{
public:
    virtual Record* createEntity() = 0;

    virtual void logAvailableEntities() = 0;

    virtual QList<Record*> getAllEntities() = 0;
};

template<int I, typename E>
class EntityFactory:
    public Singleton< EntityFactory<I, E> >,
    public AbstractEntityFactory
{
public:
    EntityFactory() {
        EntitiesModel::instance().addFactory(I, this);
    }

    E* createEntity() {
        return new E();
    }

    void logAvailableEntities() {
        E::logAvailableEntities();
    }

    QList<Record*> getAllEntities()
    {
        return E::entities();
    }

};

class EntitiesModel:
    public Singleton<EntitiesModel>
{
public:
    void initialize();
    void addFactory(int id, AbstractEntityFactory *factory);
    Record* createEntity(int id);

    void logAvailableEntities();

    void createEntitiesLoggerContext(const QString &name);
    void removeEntitiesLoggerContext(const QString &name);

private:
    QMap<int, AbstractEntityFactory*> m_factories;
    QMap<QString, QList<Record*> > m_loggerContext;
};

}

#endif // ENTITIESMODEL_H
