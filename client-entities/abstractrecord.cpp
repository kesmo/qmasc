#include "abstractrecord.h"
#include "recordlistener.h"

#include "constants.h"

#include <QDebug>

AbstractRecord::AbstractRecord() {
    m_is_removing = false;
}

AbstractRecord::~AbstractRecord()
{
    aboutToBeDestroyed();
}

void AbstractRecord::aboutToBeDestroyed()
{
    if (!m_is_removing) {
        m_is_removing = true;
        foreach(RecordListener *tmp_listener, m_record_listeners_list) {
            tmp_listener->recordDestroyed(this);
        }
        m_record_listeners_list.clear();
    }
}

void AbstractRecord::addRecordListener(RecordListener *recordListener)
{
    if (!m_is_removing && !m_record_listeners_list.contains(recordListener))
        m_record_listeners_list.append(recordListener);
}

void AbstractRecord::removeRecordListener(RecordListener *recordListener)
{
    if (!m_is_removing)
        m_record_listeners_list.removeAll(recordListener);
}


int AbstractRecord::saveRecord()
{
    return NOERR;
}

int AbstractRecord::deleteRecord()
{
    return NOERR;
}

bool AbstractRecord::isModifiable()
{
    return true;
}

bool AbstractRecord::isRemoving() const
{
    return m_is_removing;
}
