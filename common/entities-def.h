/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

/** \file entities.h
* This file contains the definition for all entities.<br>
* Each entity corresponds to a database table or a database view, with their own columns definitions.
*/
#ifndef ENTITIES_DEF_H_
#define ENTITIES_DEF_H_

#include "constants.h"

#define            RECORD_STATUS_OWN_LOCK               1
#define            RECORD_STATUS_MODIFIABLE             0
#define            RECORD_STATUS_OUT_OF_SYNC           -1
#define            RECORD_STATUS_LOCKED                -2
#define            RECORD_STATUS_BROKEN                -3

/**
* Base structure for entity definition
*/
typedef struct _entity_def
{
    //! Entity signature identifier
    const int           m_entity_signature_id;
    //! Entity name (equals to table/view name)
    const char          *m_entity_name;
    //! \a m_primary_key may be NULL if the primary key of the entity is the first column of \a m_entity_columns_names
    const char          **m_primary_key;
    //! Sequence generator of the primary key
    const char          *m_pk_seq_name;
    //! Array of columns names
    const char          **m_entity_columns_names;
    //! Array of columns formats (string, number, timestamp)
    const char          **m_entity_columns_formats;
    //! Array of columns sizes
    const unsigned int  *m_entity_columns_sizes;
    //! Number of columns
    const unsigned int  m_entity_columns_count;
}
entity_def;

enum TABLE_SIG_IDS
{
    // Administration tables
    FILES_TABLE_SIG_ID = 100,
    USERS_TABLE_SIG_ID, // 101
    GROUPS_TABLE_SIG_ID, // 102 -- not used
    SHARED_FILES_TABLE_SIG_ID, // 103 -- not used
    USERS_GROUPS_TABLE_SIG_ID, // 104 -- not used
    CONTACTS_TABLE_SIG_ID, // 105 -- not used
    CONTACTS_GROUPS_TABLE_SIG_ID, // 106 -- not used
    LDAP_USERS_SIG_ID, // 107

    // Data tables
    PROJECTS_TABLE_SIG_ID = 200,
    PROJECTS_VERSIONS_TABLE_SIG_ID, // 201
    TESTS_CONTENTS_TABLE_SIG_ID, // 202
    TESTS_TABLE_SIG_ID, // 203
    ACTIONS_TABLE_SIG_ID, // 204
    REQUIREMENTS_CONTENTS_TABLE_SIG_ID, // 205
    REQUIREMENTS_TABLE_SIG_ID, // 206
    AUTOMATED_ACTIONS_TABLE_SIG_ID, // 207
    TESTS_REQUIREMENTS_TABLE_SIG_ID, // 208,
    CAMPAIGNS_TABLE_SIG_ID, // 209
    TESTS_CAMPAIGNS_TABLE_SIG_ID, // 210
    EXECUTIONS_CAMPAIGNS_TABLE_SIG_ID, // 211
    EXECUTIONS_TESTS_TABLE_SIG_ID, // 212
    EXECUTIONS_ACTIONS_TABLE_SIG_ID, // 213
    EXECUTIONS_REQUIREMENTS_TABLE_SIG_ID, // 214
    PROJECTS_PARAMETERS_TABLE_SIG_ID, // 215
    PROJECTS_GRANTS_TABLE_SIG_ID, // 216
    TESTS_HIERARCHY_SIG_ID, // 217
    REQUIREMENTS_HIERARCHY_SIG_ID, // 218
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_SIG_ID, // 219
    TESTS_CONTENTS_FILES_TABLE_SIG_ID, // 220
    BUGS_TABLE_SIG_ID, // 221
    EXECUTIONS_TESTS_PARAMETERS_TABLE_SIG_ID, // 222
    /* since version 1.11.00 */
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_SIG_ID, // 223
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_SIG_ID, // 224
    CUSTOM_FIELDS_DESC_TABLE_SIG_ID, // 225
    CUSTOM_TEST_FIELDS_TABLE_SIG_ID, // 226
    CUSTOM_REQUIREMENT_FIELDS_TABLE_SIG_ID, // 227
    /* since version 1.12.00 */
    NEEDS_TABLE_SIG_ID, // 228
    FEATURES_CONTENTS_TABLE_SIG_ID, // 229
    FEATURES_TABLE_SIG_ID, // 230
    RULES_CONTENTS_TABLE_SIG_ID, // 231
    RULES_TABLE_SIG_ID, // 232
    TESTS_PLANS_TABLE_SIG_ID, // 233
    GRAPHICS_ITEMS_TABLE_SIG_ID, // 234
    GRAPHICS_TESTS_TABLE_SIG_ID, // 235
    GRAPHICS_IFS_TABLE_SIG_ID, // 236
    GRAPHICS_SWITCHES_TABLE_SIG_ID, // 237
    GRAPHICS_LOOPS_TABLE_SIG_ID, // 238
    GRAPHICS_LINKS_TABLE_SIG_ID, // 239
    GRAPHICS_POINTS_TABLE_SIG_ID, // 240
    RULES_HIERARCHY_SIG_ID, // 241
    FEATURES_HIERARCHY_SIG_ID, // 242
    PROJECTS_VERSIONS_PARAMETERS_TABLE_SIG_ID, // 243

    // Static tables
    REQUIREMENTS_CATEGORIES_TABLE_SIG_ID = 300,
    TESTS_RESULTS_TABLE_SIG_ID, // 301
    ACTIONS_RESULTS_TABLE_SIG_ID, // 302
    STATUS_TABLE_SIG_ID, // 303
    TESTS_TYPES_TABLE_SIG_ID // 304
};

#ifdef __cplusplus
extern "C"
{
#endif

    /*-------------------------------------------------------------------
     * Données des documents
     -------------------------------------------------------------------*/
    extern const char FILES_TABLE_SIG[];

    extern const char FILES_TABLE_FILE_ID_SEQ[];
    extern const char FILES_TABLE_FILE_ID[];
    extern const char FILES_TABLE_BASENAME[];
    extern const char FILES_TABLE_PARENT_ID[];
    extern const char FILES_TABLE_USER_ID[];
    extern const char FILES_TABLE_GROUP_ID[];
    extern const char FILES_TABLE_STATUS[];
    extern const char FILES_TABLE_FILE_TYPE[];
    extern const char FILES_TABLE_SHARE_NAME[];

    typedef struct _file
    {
        char    *file_id;
        char    *basename;
        char    *parent_id;
        char    *user_id;
        char    *group_id;
        char    *status;
        char    *file_type;
        char    *share_name;
    }file;

    extern const entity_def files_table_def;

    /*-------------------------------------------------------------------
     * Données des utilisateurs
     -------------------------------------------------------------------*/
    extern const char LDAP_USERS_DN[];
    extern const char LDAP_USERS_GIVEN_NAME[];
    extern const char LDAP_USERS_SN[];
    extern const char LDAP_USERS_UID[];
    extern const char LDAP_USERS_USER_PASSWORD[];
    extern const char LDAP_USERS_UID_NUMBER[];
    extern const char LDAP_USERS_GID_NUMBER[];
    extern const char LDAP_USERS_HOME_DIR[];
    extern const char LDAP_USERS_CN[];
    extern const char LDAP_USERS_MAIL[];

    extern const char *LDAP_USERS_ATTRIBUTES_NAMES[];

    extern const entity_def ldap_users_table_def;

    extern const char USERS_TABLE_SIG[];

    extern const char USERS_TABLE_USER_ID_SEQ[];
    extern const char USERS_TABLE_USER_ID[];
    extern const char USERS_TABLE_USERNAME[];
    extern const char USERS_TABLE_GROUP_ID[];
    extern const char USERS_TABLE_EMAIL[];
    extern const char USERS_TABLE_STATUS[];
#ifndef _RTMR
    extern const char USERS_TABLE_CREATION_DATE[];
    extern const char USERS_TABLE_THEME[];
    extern const char USERS_TABLE_WINDOWS_COLOR[];
    extern const char USERS_TABLE_SHOW_TOOLTIPS[];
#endif

    typedef struct _user
    {
        char    *user_id;
        char    *username;
        char    *group_id;
        char    *email;
        char    *password;
        char    *status;
#ifndef _RTMR
        char    *creation_date;
        char    *theme;
        char    *windows_color;
        char    *show_tooltips;
#endif
    }user;

    extern const entity_def users_table_def;

    /*-------------------------------------------------------------------
     * Données des groupes
     -------------------------------------------------------------------*/
    extern const char GROUPS_TABLE_SIG[];

    extern const char GROUPS_TABLE_GROUP_ID_SEQ[];
    extern const char GROUPS_TABLE_GROUP_ID[];
    extern const char GROUPS_TABLE_GROUP_NAME[];
    extern const char GROUPS_TABLE_OWNER_ID[];
    extern const char GROUPS_TABLE_PARENT_ID[];

    typedef struct _group
    {
        char    *group_id;
        char    *group_name;
        char    *owner_id;
        char    *parent_id;
    }group;

    extern const entity_def groups_table_def;

    /*-------------------------------------------------------------------
     * Données des dossiers partagés
     -------------------------------------------------------------------*/
    extern const char SHARED_FILES_TABLE_SIG[];
    extern const char SHARED_FILES_TABLE_FILE_ID[];
    extern const char SHARED_FILES_TABLE_GROUP_ID[];

    typedef struct _share
    {
        char    *file_id;
        char    *group_id;
    }share;

    extern const entity_def shared_files_table_def;

    /*-------------------------------------------------------------------
     * Données concernant l'appartenance des utilisateurs à des groupes
     -------------------------------------------------------------------*/
    extern const char USERS_GROUPS_TABLE_SIG[];
    extern const char USERS_GROUPS_TABLE_USER_ID[];
    extern const char USERS_GROUPS_TABLE_GROUP_ID[];

    typedef struct _user_group
    {
        char    *user_id;
        char    *group_id;
    }user_group;

    extern const entity_def users_groups_table_def;

    /*-------------------------------------------------------------------
     * Données des contacts
     -------------------------------------------------------------------*/
    extern const char CONTACTS_TABLE_SIG[];
    extern const char CONTACTS_TABLE_CONTACT_ID_SEQ[];
    extern const char CONTACTS_TABLE_CONTACT_ID[];
    extern const char CONTACTS_TABLE_OWNER_ID[];
    extern const char CONTACTS_TABLE_NAME[];
    extern const char CONTACTS_TABLE_FIRSTNAME[];
    extern const char CONTACTS_TABLE_COMPANY[];
    extern const char CONTACTS_TABLE_EMAIL_1[];
    extern const char CONTACTS_TABLE_EMAIL_2[];
    extern const char CONTACTS_TABLE_EMAIL_3[];
    extern const char CONTACTS_TABLE_PHONE_MOBILE[];
    extern const char CONTACTS_TABLE_PHONE_WORK[];
    extern const char CONTACTS_TABLE_PHONE_HOME[];

    typedef struct _contact
    {
        char    *contact_id;
        char    *owner_id;
        char    *name;
        char    *fisrtname;
        char    *company;
        char    *email1;
        char    *email2;
        char    *email3;
        char    *phone_mobile;
        char    *phone_work;
        char    *phone_home;
    }contact;

    extern const entity_def contacts_table_def;

    /*-------------------------------------------------------------------
     * Donneees concernant l'appartenance des contacts a des groupes
     -------------------------------------------------------------------*/
    extern const char CONTACTS_GROUPS_TABLE_SIG[];
    extern const char CONTACTS_GROUPS_TABLE_CONTACT_ID[];
    extern const char CONTACTS_GROUPS_TABLE_GROUP_ID[];

    typedef struct _contact_group
    {
        char    *contact_id;
        char    *group_id;
    }contact_group;

    extern const entity_def contacts_groups_table_def;

    /*-------------------------------------------------------------------
     *                                 PROJETS
     -------------------------------------------------------------------*/
     //! Projects table name
    extern const char PROJECTS_TABLE_SIG[];
    extern const char PROJECTS_TABLE_PROJECT_ID_SEQ[];
    //! Projects table signature identifier
    extern const char PROJECTS_TABLE_PROJECT_ID[];
    extern const char PROJECTS_TABLE_OWNER_ID[];
    extern const char PROJECTS_TABLE_SHORT_NAME[];
    extern const char PROJECTS_TABLE_DESCRIPTION[];

    extern const entity_def projects_table_def;

    /*-------------------------------------------------------------------
     *                 PARAMETRES DE PROJETS
     -------------------------------------------------------------------*/
     //! Parameters projects table name
    extern const char PROJECTS_PARAMETERS_TABLE_SIG[];
    extern const char PROJECTS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID_SEQ[];
    extern const char PROJECTS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID[];
    extern const char PROJECTS_PARAMETERS_TABLE_PROJECT_ID[];
    extern const char PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME[];
    extern const char PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE[];

    extern const entity_def projects_parameters_table_def;

    /*-------------------------------------------------------------------
     *                                 VERSIONS DES PROJETS
     -------------------------------------------------------------------*/
     //! Projects versions table name
    extern const char PROJECTS_VERSIONS_TABLE_SIG[];
    extern const char PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID_SEQ[];
    extern const char PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID[];
    extern const char PROJECTS_VERSIONS_TABLE_PROJECT_ID[];
    extern const char PROJECTS_VERSIONS_TABLE_VERSION[];
    extern const char PROJECTS_VERSIONS_TABLE_DESCRIPTION[];
    extern const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE[];
    extern const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST[];
    extern const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL[];
    extern const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID[];
    extern const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION[];

    extern const entity_def projects_versions_table_def;
    /*-------------------------------------------------------------------
     *                 PARAMETRES DE VERSION DE PROJETS
     -------------------------------------------------------------------*/
     //! Parameters version projects table name
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_SIG[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID_SEQ[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_VERSION_ID[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PARENT_GROUP_PARAMETER_ID[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_IS_GROUP[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_NAME[];
    extern const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_VALUE[];

    extern const entity_def projects_versions_parameters_table_def;

    /*-------------------------------------------------------------------
     *                                 TESTS_CONTENTS
     -------------------------------------------------------------------*/
     //! Tests contents table name
    extern const char TESTS_CONTENTS_TABLE_SIG[];
    extern const char TESTS_CONTENTS_TABLE_TEST_CONTENET_ID_SEQ[];
    extern const char TESTS_CONTENTS_TABLE_TEST_CONTENT_ID[];
    extern const char TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID[];
    extern const char TESTS_CONTENTS_TABLE_PROJECT_ID[];
    extern const char TESTS_CONTENTS_TABLE_VERSION[];
    extern const char TESTS_CONTENTS_TABLE_PROJECT_VERSION_ID[];
    extern const char TESTS_CONTENTS_TABLE_SHORT_NAME[];
    extern const char TESTS_CONTENTS_TABLE_DESCRIPTION[];
    extern const char TESTS_CONTENTS_TABLE_PRIORITY_LEVEL[];
    extern const char TESTS_CONTENTS_TABLE_CATEGORY_ID[];
    extern const char TESTS_CONTENTS_TABLE_STATUS[];
    extern const char TESTS_CONTENTS_TABLE_AUTOMATED[];
    extern const char TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND[];
    extern const char TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS[];
    extern const char TESTS_CONTENTS_TABLE_TYPE[];
    extern const char TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE[];
    extern const char TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE[];
    extern const char TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE[];

    extern const char TEST_CONTENT_TYPE_NOMINAL[];
    extern const char TEST_CONTENT_TYPE_ALTERNATE[];
    extern const char TEST_CONTENT_TYPE_EXCEPTION[];

    extern const char TEST_CONTENT_TABLE_NOT_AUTOMATED[];
    extern const char TEST_CONTENT_TABLE_AUTOMATED_GUI[];
    extern const char TEST_CONTENT_TABLE_AUTOMATED_BATCH[];

    extern const entity_def tests_contents_table_def;

    /*-------------------------------------------------------------------
     *                                 TESTS_CONTENTS_FILES
     -------------------------------------------------------------------*/
     //! Tests attachments table name
    extern const char TESTS_CONTENTS_FILES_TABLE_SIG[];
    extern const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILE_ID_SIG[];
    extern const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILE_ID[];
    extern const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_ID[];
    extern const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILENAME[];
    extern const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_LO_ID[];

    extern const entity_def tests_contents_files_table_def;

    /*-------------------------------------------------------------------
     *                                 TESTS
     -------------------------------------------------------------------*/
     //! Tests table name
    extern const char TESTS_TABLE_SIG[];
    extern const char TESTS_TABLE_TEST_ID_SEQ[];
    extern const char TESTS_TABLE_TEST_ID[];
    extern const char TESTS_TABLE_ORIGINAL_TEST_ID[];
    extern const char TESTS_TABLE_PARENT_TEST_ID[];
    extern const char TESTS_TABLE_PREVIOUS_TEST_ID[];
    extern const char TESTS_TABLE_PROJECT_ID[];
    extern const char TESTS_TABLE_VERSION[];
    extern const char TESTS_TABLE_PROJECT_VERSION_ID[];
    extern const char TESTS_TABLE_TEST_CONTENT_ID[];

    extern const entity_def tests_table_def;

    /*-------------------------------------------------------------------
     *                                 ACTIONS
     -------------------------------------------------------------------*/
     //! Tests cases actions table name
    extern const char ACTIONS_TABLE_SIG[];
    extern const char ACTIONS_TABLE_ACTION_ID_SEQ[];
    extern const char ACTIONS_TABLE_ACTION_ID[];
    extern const char ACTIONS_TABLE_PREVIOUS_ACTION_ID[];
    extern const char ACTIONS_TABLE_TEST_CONTENT_ID[];
    extern const char ACTIONS_TABLE_SHORT_NAME[];
    extern const char ACTIONS_TABLE_DESCRIPTION[];
    extern const char ACTIONS_TABLE_WAIT_RESULT[];
    extern const char ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID[];

    extern const entity_def actions_table_def;

    /*-------------------------------------------------------------------
     *                         AUTOMATED ACTIONS
     -------------------------------------------------------------------*/
     //! Automated actions table name
    extern const char AUTOMATED_ACTIONS_TABLE_SIG[];
    extern const char AUTOMATED_ACTIONS_TABLE_ACTION_ID_SEQ[];
    extern const char AUTOMATED_ACTIONS_TABLE_ACTION_ID[];
    extern const char AUTOMATED_ACTIONS_TABLE_PREVIOUS_ACTION_ID[];
    extern const char AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID[];
    extern const char AUTOMATED_ACTIONS_TABLE_WINDOW_ID[];
    extern const char AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE[];
    extern const char AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA[];
    extern const char AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY[];

    typedef enum
    {
        None,
        Keyboard,
        Mouse,
        StartRecord,
        StopRecord
    }
    EventMessageType;

#if (defined(_WINDOWS) || defined(WIN32))
#define        EVT_MSG_MOUSE_BUTTON_LEFT_DOWN        WM_LBUTTONDOWN
#define        EVT_MSG_MOUSE_BUTTON_LEFT_UP        WM_LBUTTONUP
#define        EVT_MSG_MOUSE_WHEEL_VERTICAL        WM_MOUSEWHEEL
#define        EVT_MSG_MOUSE_WHEEL_HORIZONTAL        WM_MOUSEHWHEEL
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_DOWN        WM_RBUTTONDOWN
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_UP        WM_RBUTTONUP
#define        EVT_MSG_MOUSE_MOVE            WM_MOUSEMOVE
#define        EVT_MSG_KEYBOARD_KEY_DOWN        WM_KEYDOWN
#define        EVT_MSG_KEYBOARD_KEY_UP            WM_KEYUP
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_DOWN    WM_SYSKEYDOWN
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_UP        WM_SYSKEYUP
#else
#define        EVT_MSG_MOUSE_BUTTON_LEFT_DOWN        0
#define        EVT_MSG_MOUSE_BUTTON_LEFT_UP        1
#define        EVT_MSG_MOUSE_WHEEL_VERTICAL        2
#define        EVT_MSG_MOUSE_WHEEL_HORIZONTAL        3
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_DOWN        4
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_UP        5
#define        EVT_MSG_MOUSE_MOVE            6
#define        EVT_MSG_KEYBOARD_KEY_DOWN        7
#define        EVT_MSG_KEYBOARD_KEY_UP            8
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_DOWN    9
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_UP        10
#endif

    extern const entity_def automated_actions_table_def;

    /*-------------------------------------------------------------------
     *                     AUTOMATED ACTIONS VALIDATIONS
     -------------------------------------------------------------------*/
     //! Automated actions validations table name
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_SIG[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_ID_SEQ[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_ID[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_PREVIOUS_VALIDATION_ID[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_AUTOMATED_ACTION_ID[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME[];
    extern const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_PARAMETERS[];

    extern const entity_def automated_actions_validations_table_def;

    /*-------------------------------------------------------------------
     *                         CONTENUS D'EXIGENCES
     -------------------------------------------------------------------*/
     //! Requirements contents table name
    extern const char REQUIREMENTS_CONTENTS_TABLE_SIG[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_ID_SEQ[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_CONTENT_ID[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_ORIGINAL_REQUIREMENT_CONTENT_ID[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_PROJECT_ID[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_VERSION[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_PROJECT_VERSION_ID[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_STATUS[];
    extern const char REQUIREMENTS_CONTENTS_TABLE_FEATURE_CONTENT_ID[];

    extern const entity_def requirements_contents_table_def;

    /*-------------------------------------------------------------------
     *                                 EXIGENCES
     -------------------------------------------------------------------*/
     //! Requirements table name
    extern const char REQUIREMENTS_TABLE_SIG[];
    extern const char REQUIREMENTS_TABLE_REQUIREMENT_ID_SEQ[];
    extern const char REQUIREMENTS_TABLE_REQUIREMENT_ID[];
    extern const char REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID[];
    extern const char REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID[];
    extern const char REQUIREMENTS_TABLE_PREVIOUS_REQUIREMENT_ID[];
    extern const char REQUIREMENTS_TABLE_PROJECT_ID[];
    extern const char REQUIREMENTS_TABLE_VERSION[];
    extern const char REQUIREMENTS_TABLE_PROJECT_VERSION_ID[];

    extern const entity_def requirements_table_def;

    /*-------------------------------------------------------------------
     *                             CATEGORIES D'EXIGENCES
     -------------------------------------------------------------------*/
     //! Requirements categories table name
    extern const char REQUIREMENTS_CATEGORIES_TABLE_SIG[];
    extern const char REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID[];
    extern const char REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL[];

    extern const entity_def requirements_categories_table_def;

    /*-------------------------------------------------------------------
     *                                 EXIGENCES DE TESTS
     -------------------------------------------------------------------*/
     //! Requirements and tests associations table name
    extern const char TESTS_REQUIREMENTS_TABLE_SIG[];
    extern const char TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID[];
    extern const char TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID[];

    extern const entity_def tests_requirements_table_def;

    /*-------------------------------------------------------------------
     *                                 CAMPAGNES
     -------------------------------------------------------------------*/
     //! Campaigns table name
    extern const char CAMPAIGNS_TABLE_SIG[];
    extern const char CAMPAIGNS_TABLE_CAMPAIGN_ID_SEQ[];
    extern const char CAMPAIGNS_TABLE_CAMPAIGN_ID[];
    extern const char CAMPAIGNS_TABLE_PROJECT_ID[];
    extern const char CAMPAIGNS_TABLE_PROJECT_VERSION[];
    extern const char CAMPAIGNS_TABLE_PROJECT_VERSION_ID[];
    extern const char CAMPAIGNS_TABLE_SHORT_NAME[];
    extern const char CAMPAIGNS_TABLE_DESCRIPTION[];

    extern const entity_def campaigns_table_def;

    /*-------------------------------------------------------------------
     *                         TESTS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Tests campaigns table name
    extern const char TESTS_CAMPAIGNS_TABLE_SIG[];
    extern const char TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID_SEQ[];
    extern const char TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID[];
    extern const char TESTS_CAMPAIGNS_TABLE_CAMPAIGN_ID[];
    extern const char TESTS_CAMPAIGNS_TABLE_TEST_ID[];
    extern const char TESTS_CAMPAIGNS_TABLE_PREVIOUS_TEST_CAMPAIGN_ID[];

    extern const entity_def tests_campaigns_table_def;

    /*-------------------------------------------------------------------
     *                     EXECUTIONS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions campaigns table name
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_SIG[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID_SEQ[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_CAMPAIGN_ID[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_REVISION[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_USER_ID[];
    extern const char EXECUTIONS_CAMPAIGNS_TABLE_TEST_PLAN_ID[];

    extern const entity_def executions_campaigns_table_def;


    /*-------------------------------------------------------------------
     *                 PARAMETRES D'EXECUTIONS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions campaigns parameters table name
    extern const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_SIG[];
    extern const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_ID_SEQ[];
    extern const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_PARAMETER_ID[];
    extern const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_ID[];
    extern const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME[];
    extern const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_VALUE[];

    extern const entity_def executions_campaigns_parameters_table_def;

    /*-------------------------------------------------------------------
     *             EXECUTIONS DE TESTS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions tests campaigns name
    extern const char EXECUTIONS_TESTS_TABLE_SIG[];
    extern const char EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID_SEQ[];
    extern const char EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID[];
    extern const char EXECUTIONS_TESTS_TABLE_EXECUTION_CAMPAIGN_ID[];
    extern const char EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID[];
    extern const char EXECUTIONS_TESTS_TABLE_PREVIOUS_EXECUTION_TEST_ID[];
    extern const char EXECUTIONS_TESTS_TABLE_TEST_ID[];
    extern const char EXECUTIONS_TESTS_TABLE_EXECUTION_DATE[];
    extern const char EXECUTIONS_TESTS_TABLE_RESULT_ID[];
    extern const char EXECUTIONS_TESTS_TABLE_COMMENTS[];
    extern const char EXECUTIONS_TESTS_TABLE_AUTOMATION_RETURN_CODE[];
    extern const char EXECUTIONS_TESTS_TABLE_AUTOMATION_STDOUT[];
    extern const char EXECUTIONS_TESTS_TABLE_USER_ID[];

    extern const entity_def executions_tests_table_def;

    /*-------------------------------------------------------------------
     *                 PARAMETRES D'EXECUTIONS DE TESTS
     -------------------------------------------------------------------*/
     //! Executions parameters table name
    extern const char EXECUTIONS_TESTS_PARAMETERS_TABLE_SIG[];
    extern const char EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_ID_SEQ[];
    extern const char EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_PARAMETER_ID[];
    extern const char EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_ID[];
    extern const char EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_NAME[];
    extern const char EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE[];

    extern const entity_def executions_tests_parameters_table_def;

    /*-------------------------------------------------------------------
     *     RESULTATS D'EXECUTIONS DE TESTS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions tests results table name
    extern const char TESTS_RESULTS_TABLE_SIG[];
    extern const char TESTS_RESULTS_TABLE_RESULT_ID[];
    extern const char TESTS_RESULTS_TABLE_DESCRIPTION[];

    extern const char EXECUTION_TEST_VALIDATED[];
    extern const char EXECUTION_TEST_INVALIDATED[];
    extern const char EXECUTION_TEST_BYPASSED[];
    extern const char EXECUTION_TEST_INCOMPLETED[];

    extern const entity_def tests_results_table_def;

    /*-------------------------------------------------------------------
     *     EXECUTIONS D'ACTIONS DE TESTS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions actions name
    extern const char EXECUTIONS_ACTIONS_TABLE_SIG[];
    extern const char EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID_SEQ[];
    extern const char EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID[];
    extern const char EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID[];
    extern const char EXECUTIONS_ACTIONS_TABLE_ACTION_ID[];
    extern const char EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID[];
    extern const char EXECUTIONS_ACTIONS_TABLE_RESULT_ID[];
    extern const char EXECUTIONS_ACTIONS_TABLE_COMMENTS[];

    extern const entity_def executions_actions_table_def;

    /*-------------------------------------------------------------------
     *     EXECUTIONS D'ACTIONS AUTOMATISEES DE TESTS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions actions name
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_SIG[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID_SEQ[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_ACTION_ID[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_RESULT_ID[];
    extern const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_COMMENTS[];

    extern const entity_def automated_executions_actions_table_def;

    /*-------------------------------------------------------------------
     *     RESULTATS D'EXECUTIONS D'ACTIONS DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Excutions actions results table name
    extern const char ACTIONS_RESULTS_TABLE_SIG[];
    extern const char ACTIONS_RESULTS_TABLE_RESULT_ID[];
    extern const char ACTIONS_RESULTS_TABLE_DESCRIPTION[];

    extern const char EXECUTION_ACTION_VALIDATED[];
    extern const char EXECUTION_ACTION_INVALIDATED[];
    extern const char EXECUTION_ACTION_BYPASSED[];

    extern const entity_def actions_results_table_def;
    /*-------------------------------------------------------------------
     *     EXECUTIONS D'EXIGENCES DE CAMPAGNES
     -------------------------------------------------------------------*/
     //! Executions requirements table name
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_SIG[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_ACTION_ID_SEQ[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_REQUIREMENT_ID[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_CAMPAIGN_ID[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_TEST_CONTENT_ID[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT__ID[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_RESULT_ID[];
    extern const char EXECUTIONS_REQUIREMENTS_TABLE_COMMENTS[];

    extern const entity_def executions_requirements_table_def;

    /*-------------------------------------------------------------------
     *                                 DROITS SUR LES PROJETS
     -------------------------------------------------------------------*/
     //! Projects grants table name
    extern const char PROJECTS_GRANTS_TABLE_SIG[];
    extern const char PROJECTS_GRANTS_TABLE_PROJECT_GRANT_ID_SEQ[];
    extern const char PROJECTS_GRANTS_TABLE_PROJECT_GRANT_ID[];
    extern const char PROJECTS_GRANTS_TABLE_PROJECT_ID[];
    extern const char PROJECTS_GRANTS_TABLE_USERNAME[];
    extern const char PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID[];
    extern const char PROJECTS_GRANTS_TABLE_RIGHTS[];

    extern const char PROJECT_GRANT_NONE[];
    extern const char PROJECT_GRANT_READ[];
    extern const char PROJECT_GRANT_WRITE[];

    extern const entity_def projects_grants_table_def;

    /*-------------------------------------------------------------------
     *                             HIERARCHIE DE TESTS
     -------------------------------------------------------------------*/
     //! Tests hierarchy view name
    extern const char TESTS_HIERARCHY_SIG[];
    extern const char TESTS_HIERARCHY_TEST_ID[];
    extern const char TESTS_HIERARCHY_ORIGINAL_TEST_ID[];
    extern const char TESTS_HIERARCHY_PARENT_TEST_ID[];
    extern const char TESTS_HIERARCHY_PREVIOUS_TEST_ID[];
    extern const char TESTS_HIERARCHY_PROJECT_VERSION_ID[];
    extern const char TESTS_HIERARCHY_PROJECT_ID[];
    extern const char TESTS_HIERARCHY_VERSION[];
    extern const char TESTS_HIERARCHY_TEST_CONTENT_ID[];
    extern const char TESTS_HIERARCHY_SHORT_NAME[];
    extern const char TESTS_HIERARCHY_CATEGORY_ID[];
    extern const char TESTS_HIERARCHY_PRIORITY_LEVEL[];
    extern const char TESTS_HIERARCHY_CONTENT_VERSION[];
    extern const char TESTS_HIERARCHY_STATUS[];
    extern const char TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID[];
    extern const char TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED[];
    extern const char TESTS_HIERARCHY_TEST_CONTENT_TYPE[];

    extern const entity_def tests_hierarchy_def;

    /*-------------------------------------------------------------------
     *                                 HIERARCHIE D'EXIGENCES
     -------------------------------------------------------------------*/
     //! Requirements hierarchy view name
    extern const char REQUIREMENTS_HIERARCHY_SIG[];
    extern const char REQUIREMENTS_HIERARCHY_REQUIREMENT_ID[];
    extern const char REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID[];
    extern const char REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID[];
    extern const char REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID[];
    extern const char REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID[];
    extern const char REQUIREMENTS_HIERARCHY_PROJECT_ID[];
    extern const char REQUIREMENTS_HIERARCHY_VERSION[];
    extern const char REQUIREMENTS_HIERARCHY_SHORT_NAME[];
    extern const char REQUIREMENTS_HIERARCHY_CATEGORY_ID[];
    extern const char REQUIREMENTS_HIERARCHY_PRIORITY_LEVEL[];
    extern const char REQUIREMENTS_HIERARCHY_CONTENT_VERSION[];
    extern const char REQUIREMENTS_HIERARCHY_STATUS[];
    extern const char REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID[];

    extern const entity_def requirements_hierarchy_def;

    /*-------------------------------------------------------------------
     *     ANOMALIES
     -------------------------------------------------------------------*/
     //! Bugs table name
    extern const char BUGS_TABLE_SIG[];
    extern const char BUGS_TABLE_BUG_ID_SEQ[];
    extern const char BUGS_TABLE_BUG_ID[];
    extern const char BUGS_TABLE_EXECUTION_TEST_ID[];
    extern const char BUGS_TABLE_EXECUTION_ACTION_ID[];
    extern const char BUGS_TABLE_CREATION_DATE[];
    extern const char BUGS_TABLE_SHORT_NAME[];
    extern const char BUGS_TABLE_PRIORITY[];
    extern const char BUGS_TABLE_SEVERITY[];
    extern const char BUGS_TABLE_REPRODUCIBILITY[];
    extern const char BUGS_TABLE_PLATFORM[];
    extern const char BUGS_TABLE_SYSTEM[];
    extern const char BUGS_TABLE_DESCRIPTION[];
    extern const char BUGS_TABLE_BUGTRACKER_BUG_ID[];
    extern const char BUGS_TABLE_STATUS[];

    extern const char BUG_STATUS_OPENED[];
    extern const char BUG_STATUS_CLOSED[];

    extern const entity_def bugs_table_def;

    /*-------------------------------------------------------------------
     *         STATUS DES CONTENUS D'EXIGENCES ET DES CONTENUS DE TESTS
     -------------------------------------------------------------------*/
     //! Requirements and tests status table name
    extern const char STATUS_TABLE_SIG[];
    extern const char STATUS_TABLE_STATUS_ID[];
    extern const char STATUS_TABLE_STATUS_LABEL[];

    extern const entity_def status_table_def;

    /*-------------------------------------------------------------------
     *         TYPES DE TESTS
     -------------------------------------------------------------------*/
     //! Tests types table name
    extern const char TESTS_TYPES_TABLE_SIG[];
    extern const char TESTS_TYPES_TABLE_TEST_TYPE_ID[];
    extern const char TESTS_TYPES_TABLE_TEST_TYPE_LABEL[];

    extern const entity_def tests_types_table_def;

    /*-------------------------------------------------------------------
     *         DEFINITION DES CHAMPS PERSONNALISES
     -------------------------------------------------------------------*/
     //! Custom fields table name
    extern const char CUSTOM_FIELDS_DESC_TABLE_SIG[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_ID_SEQ[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ID[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ENTITY[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TAB_NAME[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_LABEL[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TYPE[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_MANDATORY[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_DEFAULT_VALUE[];
    extern const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_VALUES[];

    extern const char CUSTOM_FIELDS_TEST[];
    extern const char CUSTOM_FIELDS_REQUIERMENT[];

    extern const char CUSTOM_FIELDS_PLAIN_TEXT[];
    extern const char CUSTOM_FIELDS_RICH_TEXT[];
    extern const char CUSTOM_FIELDS_CHECKBOX[];
    extern const char CUSTOM_FIELDS_LIST[];
    extern const char CUSTOM_FIELDS_INTEGER[];
    extern const char CUSTOM_FIELDS_FLOAT[];
    extern const char CUSTOM_FIELDS_TIME[];
    extern const char CUSTOM_FIELDS_DATE[];
    extern const char CUSTOM_FIELDS_DATETIME[];

    extern const entity_def custom_fields_desc_table_def;

    /*-------------------------------------------------------------------
     *              CHAMPS PERSONNALISES DE TEST
     -------------------------------------------------------------------*/
     //! Custom test fields table name
    extern const char CUSTOM_TEST_FIELDS_TABLE_SIG[];
    extern const char CUSTOM_TEST_FIELDS_TABLE_ID_SEQ[];
    extern const char CUSTOM_TEST_FIELDS_TABLE_CUSTOM_TEST_FIELD_ID[];
    extern const char CUSTOM_TEST_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID[];
    extern const char CUSTOM_TEST_FIELDS_TABLE_TEST_CONTENT_ID[];
    extern const char CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE[];

    extern const entity_def custom_test_fields_table_def;

    /*-------------------------------------------------------------------
    *              CHAMPS PERSONNALISES D'EXIGENCE
    -------------------------------------------------------------------*/
    //! Custom requirement fields table name
    extern const char CUSTOM_REQUIREMENT_FIELDS_TABLE_SIG[];
    extern const char CUSTOM_REQUIREMENT_FIELDS_TABLE_ID_SEQ[];
    extern const char CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_REQUIREMENT_FIELD_ID[];
    extern const char CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID[];
    extern const char CUSTOM_REQUIREMENT_FIELDS_TABLE_REQUIREMENT_CONTENT_ID[];
    extern const char CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE[];

    extern const entity_def custom_requirement_fields_table_def;

    /*-------------------------------------------------------------------
    *              EXPRESSION DE BESOIN
    -------------------------------------------------------------------*/
    //! Needs table name
    extern const char NEEDS_TABLE_SIG[];
    extern const char NEEDS_TABLE_ID_SEQ[];
    extern const char NEEDS_TABLE_NEED_ID[];
    extern const char NEEDS_TABLE_PARENT_NEED_ID[];
    extern const char NEEDS_TABLE_PREVIOUS_NEED_ID[];
    extern const char NEEDS_TABLE_PROJECT_ID[];
    extern const char NEEDS_TABLE_CREATION_DATE[];
    extern const char NEEDS_TABLE_SHORT_NAME[];
    extern const char NEEDS_TABLE_DESCRIPTION[];
    extern const char NEEDS_TABLE_DESCRIPTION_PLAIN_TEXT[];
    extern const char NEEDS_TABLE_STATUS[];

    extern const entity_def needs_table_def;

    /*-------------------------------------------------------------------
    *              CONTENUS DE FONCTIONNALITES
    -------------------------------------------------------------------*/
    //! Contents feature table name
    extern const char FEATURES_CONTENTS_TABLE_SIG[];
    extern const char FEATURES_CONTENTS_TABLE_ID_SEQ[];
    extern const char FEATURES_CONTENTS_TABLE_FEATURE_CONTENT_ID[];
    extern const char FEATURES_CONTENTS_TABLE_PROJECT_ID[];
    extern const char FEATURES_CONTENTS_TABLE_VERSION[];
    extern const char FEATURES_CONTENTS_TABLE_PROJECT_VERSION_ID[];
    extern const char FEATURES_CONTENTS_TABLE_NEED_ID[];
    extern const char FEATURES_CONTENTS_TABLE_SHORT_NAME[];
    extern const char FEATURES_CONTENTS_TABLE_DESCRIPTION[];
    extern const char FEATURES_CONTENTS_TABLE_DESCRIPTION_PLAIN_TEXT[];
    extern const char FEATURES_CONTENTS_TABLE_STATUS[];

    extern const entity_def features_contents_table_def;

    /*-------------------------------------------------------------------
    *              FONCTIONNALITES
    -------------------------------------------------------------------*/
    //! Features table name
    extern const char FEATURES_TABLE_SIG[];
    extern const char FEATURES_TABLE_ID_SEQ[];
    extern const char FEATURES_TABLE_FEATURE_ID[];
    extern const char FEATURES_TABLE_FEATURE_CONTENT_ID[];
    extern const char FEATURES_TABLE_PARENT_FEATURE_ID[];
    extern const char FEATURES_TABLE_PREVIOUS_FEATURE_ID[];
    extern const char FEATURES_TABLE_PROJECT_ID[];
    extern const char FEATURES_TABLE_VERSION[];
    extern const char FEATURES_TABLE_PROJECT_VERSION_ID[];

    extern const entity_def features_table_def;

    /*-------------------------------------------------------------------
     *             HIERARCHIE DE FONCTIONNALITES
     -------------------------------------------------------------------*/
     //! Features hierarchy view name
    extern const char        FEATURES_HIERARCHY_SIG[];
    extern const char        FEATURES_HIERARCHY_FEATURE_ID[];
    extern const char        FEATURES_HIERARCHY_FEATURE_CONTENT_ID[];
    extern const char        FEATURES_HIERARCHY_PARENT_FEATURE_ID[];
    extern const char        FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID[];
    extern const char        FEATURES_HIERARCHY_PROJECT_VERSION_ID[];
    extern const char        FEATURES_HIERARCHY_PROJECT_ID[];
    extern const char        FEATURES_HIERARCHY_VERSION[];
    extern const char        FEATURES_HIERARCHY_SHORT_NAME[];
    extern const char        FEATURES_HIERARCHY_CONTENT_VERSION[];
    extern const char        FEATURES_HIERARCHY_STATUS[];

    extern const entity_def features_hierarchy_def;

    /*-------------------------------------------------------------------
    *              CONTENUS DES REGLES DE GESTION
    -------------------------------------------------------------------*/
    //! Contents rule table name
    extern const char RULES_CONTENTS_TABLE_SIG[];
    extern const char RULES_CONTENTS_TABLE_ID_SEQ[];
    extern const char RULES_CONTENTS_TABLE_RULE_CONTENT_ID[];
    extern const char RULES_CONTENTS_TABLE_PROJECT_ID[];
    extern const char RULES_CONTENTS_TABLE_VERSION[];
    extern const char RULES_CONTENTS_TABLE_PROJECT_VERSION_ID[];
    extern const char RULES_CONTENTS_TABLE_FEATURE_CONTENT_ID[];
    extern const char RULES_CONTENTS_TABLE_SHORT_NAME[];
    extern const char RULES_CONTENTS_TABLE_DESCRIPTION[];
    extern const char RULES_CONTENTS_TABLE_DESCRIPTION_PLAIN_TEXT[];
    extern const char RULES_CONTENTS_TABLE_STATUS[];

    extern const entity_def rules_contents_table_def;

    /*-------------------------------------------------------------------
    *              REGLES DE GESTION
    -------------------------------------------------------------------*/
    //! Rules table name
    extern const char RULES_TABLE_SIG[];
    extern const char RULES_TABLE_ID_SEQ[];
    extern const char RULES_TABLE_RULE_ID[];
    extern const char RULES_TABLE_RULE_CONTENT_ID[];
    extern const char RULES_TABLE_PARENT_RULE_ID[];
    extern const char RULES_TABLE_PREVIOUS_RULE_ID[];
    extern const char RULES_TABLE_PROJECT_ID[];
    extern const char RULES_TABLE_VERSION[];
    extern const char RULES_TABLE_PROJECT_VERSION_ID[];

    extern const entity_def rules_table_def;

    /*-------------------------------------------------------------------
     *             HIERARCHIE DE REGLES DE GESTION
     -------------------------------------------------------------------*/
     //! Rules hierarchy view name
    extern const char        RULES_HIERARCHY_SIG[];
    extern const char        RULES_HIERARCHY_RULE_ID[];
    extern const char        RULES_HIERARCHY_RULE_CONTENT_ID[];
    extern const char        RULES_HIERARCHY_PARENT_RULE_ID[];
    extern const char        RULES_HIERARCHY_PREVIOUS_RULE_ID[];
    extern const char        RULES_HIERARCHY_PROJECT_VERSION_ID[];
    extern const char        RULES_HIERARCHY_PROJECT_ID[];
    extern const char        RULES_HIERARCHY_VERSION[];
    extern const char        RULES_HIERARCHY_SHORT_NAME[];
    extern const char        RULES_HIERARCHY_CONTENT_VERSION[];
    extern const char        RULES_HIERARCHY_STATUS[];

    extern const entity_def rules_hierarchy_def;

    /*-------------------------------------------------------------------
     *                         PLANS DE TESTS
     -------------------------------------------------------------------*/
     //! Tests plan table name
    extern const char        TESTS_PLANS_TABLE_SIG[];
    extern const char        TESTS_PLANS_TABLE_TEST_PLAN_ID_SEQ[];
    extern const char        TESTS_PLANS_TABLE_TEST_PLAN_ID[];
    extern const char        TESTS_PLANS_TABLE_CAMPAIGN_ID[];

    extern const entity_def tests_plans_table_def;

    /*-------------------------------------------------------------------
     *                         OBJETS GRAPHIQUES DE PLANS DE TESTS
     -------------------------------------------------------------------*/
     //! Graphics items table name
    extern const char        GRAPHICS_ITEMS_TABLE_SIG[];
    extern const char        GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID_SEQ[];
    extern const char        GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_ITEMS_TABLE_TEST_PLAN_ID[];
    extern const char        GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_ITEMS_TABLE_TYPE[];
    extern const char        GRAPHICS_ITEMS_TABLE_X[];
    extern const char        GRAPHICS_ITEMS_TABLE_Y[];
    extern const char        GRAPHICS_ITEMS_TABLE_Z_VALUE[];
    extern const char        GRAPHICS_ITEMS_TABLE_COMMENTS[];
    extern const char        GRAPHICS_ITEMS_TABLE_DATA[];

#define GRAPHICS_ITEMS_TYPE_TEST 'T'
#define GRAPHICS_ITEMS_TYPE_LINK 'L'
#define GRAPHICS_ITEMS_TYPE_IF 'I'
#define GRAPHICS_ITEMS_TYPE_SWITCH 'S'
#define GRAPHICS_ITEMS_TYPE_LOOP 'W'
#define GRAPHICS_ITEMS_TYPE_POINT 'P'

    extern const entity_def graphics_items_table_def;

    /*-------------------------------------------------------------------
     *                         POINTS GRAPHIQUES DE PLANS DE TESTS
     -------------------------------------------------------------------*/
     //! Graphics items table name
    extern const char        GRAPHICS_POINTS_TABLE_SIG[];
    extern const char        GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_POINTS_TABLE_TEST_PLAN_ID[];
    extern const char        GRAPHICS_POINTS_TABLE_LINK_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID[];
    extern const char        GRAPHICS_POINTS_TABLE_TYPE[];
    extern const char        GRAPHICS_POINTS_TABLE_DATA[];

#define GRAPHICS_POINTS_TYPE_START 'S'
#define GRAPHICS_POINTS_TYPE_NODE 'N'
#define GRAPHICS_POINTS_TYPE_END 'E'

    extern const entity_def graphics_points_table_def;

    /*-------------------------------------------------------------------
     *                         TESTS GRAPHIQUES
     -------------------------------------------------------------------*/
     //! Graphics tests table name
    extern const char        GRAPHICS_TESTS_TABLE_SIG[];
    extern const char        GRAPHICS_TESTS_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_TESTS_TABLE_TEST_PLAN_ID[];
    extern const char        GRAPHICS_TESTS_TABLE_TEST_ID[];
    extern const char        GRAPHICS_TESTS_TABLE_RETURN_CODE_VAR_NAME[];
    extern const char        GRAPHICS_TESTS_TABLE_OUTPUT_VAR_NAME[];

    extern const entity_def graphics_tests_table_def;

    /*-------------------------------------------------------------------
     *                         IFS GRAPHIQUES
     -------------------------------------------------------------------*/
     //! Graphics ifs table name
    extern const char        GRAPHICS_IFS_TABLE_SIG[];
    extern const char        GRAPHICS_IFS_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_IFS_TABLE_TEST_PLAN_ID[];
    extern const char        GRAPHICS_IFS_TABLE_CONDITION[];

    extern const entity_def graphics_ifs_table_def;

    /*-------------------------------------------------------------------
     *                         SWITCHS GRAPHIQUES
     -------------------------------------------------------------------*/
     //! Graphics switches table name
    extern const char        GRAPHICS_SWITCHES_TABLE_SIG[];
    extern const char        GRAPHICS_SWITCHES_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_SWITCHES_TABLE_TEST_PLAN_ID[];
    extern const char        GRAPHICS_SWITCHES_TABLE_CONDITION[];

    extern const entity_def graphics_switches_table_def;

    /*-------------------------------------------------------------------
     *                         BOUCLES GRAPHIQUES
     -------------------------------------------------------------------*/
     //! Graphics loops table name
    extern const char        GRAPHICS_LOOPS_TABLE_SIG[];
    extern const char        GRAPHICS_LOOPS_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_LOOPS_TABLE_TEST_PLAN_ID[];
    extern const char        GRAPHICS_LOOPS_TABLE_CONDITION[];

    extern const entity_def graphics_loops_table_def;

    /*-------------------------------------------------------------------
     *                         LIENS GRAPHIQUES
     -------------------------------------------------------------------*/
     //! Graphics links table name
    extern const char        GRAPHICS_LINKS_TABLE_SIG[];
    extern const char        GRAPHICS_LINKS_TABLE_GRAPHIC_ITEM_ID[];
    extern const char        GRAPHICS_LINKS_TABLE_TEST_PLAN_ID[];

    extern const entity_def graphics_links_table_def;

#ifdef __cplusplus
}
#endif


#endif /*ENTITIES_DEF_H_*/
