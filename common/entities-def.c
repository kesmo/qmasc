/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "entities-def.h"

/*-------------------------------------------------------------------
 * Données des documents
 -------------------------------------------------------------------*/
const char FILES_TABLE_SIG[] = "files_table";

const char FILES_TABLE_FILE_ID_SEQ[] = "files_file_id_seq";
const char FILES_TABLE_FILE_ID[] = "file_id";
const char FILES_TABLE_BASENAME[] = "basename";
const char FILES_TABLE_PARENT_ID[] = "parent_id";
const char FILES_TABLE_USER_ID[] = "user_id";
const char FILES_TABLE_GROUP_ID[] = "group_id";
const char FILES_TABLE_STATUS[] = "status";
const char FILES_TABLE_FILE_TYPE[] = "file_type";
const char FILES_TABLE_SHARE_NAME[] = "share_name";

const char *FILES_TABLE_COLUMNS_NAMES[] = {
    FILES_TABLE_FILE_ID,
    FILES_TABLE_BASENAME,
    FILES_TABLE_PARENT_ID,
    FILES_TABLE_USER_ID,
    FILES_TABLE_GROUP_ID,
    FILES_TABLE_STATUS,
    FILES_TABLE_FILE_TYPE,
    FILES_TABLE_SHARE_NAME,
    NULL
};

const char *FILES_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def files_table_def = {
    FILES_TABLE_SIG_ID,
    FILES_TABLE_SIG,
    NULL,
    FILES_TABLE_FILE_ID_SEQ,
    FILES_TABLE_COLUMNS_NAMES,
    FILES_TABLE_COLUMNS_FORMATS,
    NULL,
    sizeof(FILES_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 * Données des utilisateurs
 -------------------------------------------------------------------*/
const char LDAP_USERS_DN[] = "dn";
const char LDAP_USERS_GIVEN_NAME[] = "givenName";
const char LDAP_USERS_SN[] = "sn";
const char LDAP_USERS_UID[] = "uid";
const char LDAP_USERS_USER_PASSWORD[] = "userPassword";
const char LDAP_USERS_UID_NUMBER[] = "uidNumber";
const char LDAP_USERS_GID_NUMBER[] = "gidNumber";
const char LDAP_USERS_HOME_DIR[] = "homeDirectory";
const char LDAP_USERS_CN[] = "cn";
const char LDAP_USERS_MAIL[] = "mail";

const char *LDAP_USERS_ATTRIBUTES_NAMES[] = {
    LDAP_USERS_DN,
    LDAP_USERS_GIVEN_NAME,
    LDAP_USERS_SN,
    LDAP_USERS_UID,
    LDAP_USERS_USER_PASSWORD,
    LDAP_USERS_UID_NUMBER,
    LDAP_USERS_GID_NUMBER,
    LDAP_USERS_HOME_DIR,
    LDAP_USERS_CN,
    LDAP_USERS_MAIL,
    NULL
};

const entity_def ldap_users_table_def = {
    LDAP_USERS_SIG_ID,
    NULL,
    NULL,
    NULL,
    LDAP_USERS_ATTRIBUTES_NAMES,
    NULL,
    NULL,
    sizeof(LDAP_USERS_ATTRIBUTES_NAMES)/sizeof(char*) - 1
};

const char USERS_TABLE_SIG[] = "users_table";

const char USERS_TABLE_USER_ID_SEQ[] = "users_user_id_seq";
const char USERS_TABLE_USER_ID[] = "user_id";
const char USERS_TABLE_USERNAME[] = "username";
const char USERS_TABLE_GROUP_ID[] = "group_id";
const char USERS_TABLE_EMAIL[] = "email";
const char USERS_TABLE_STATUS[] = "status";
#ifndef _RTMR
const char USERS_TABLE_CREATION_DATE[] = "creation_date";
const char USERS_TABLE_THEME[] = "theme";
const char USERS_TABLE_WINDOWS_COLOR[] = "windows_color";
const char USERS_TABLE_SHOW_TOOLTIPS[] = "show_tooltips";
#endif

const char *USERS_TABLE_COLUMNS_NAMES[] = {
    USERS_TABLE_USER_ID,
    USERS_TABLE_USERNAME,
    USERS_TABLE_GROUP_ID,
    USERS_TABLE_EMAIL,
    USERS_TABLE_STATUS,
#ifndef _RTMR
    USERS_TABLE_CREATION_DATE,
    USERS_TABLE_THEME,
    USERS_TABLE_WINDOWS_COLOR,
    USERS_TABLE_SHOW_TOOLTIPS,
#endif
    NULL
};

const char *USERS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
#ifndef _RTMR
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
#endif
    NULL
};

const entity_def users_table_def = {
    USERS_TABLE_SIG_ID,
    USERS_TABLE_SIG,
    NULL,
    USERS_TABLE_USER_ID_SEQ,
    USERS_TABLE_COLUMNS_NAMES,
    USERS_TABLE_COLUMNS_FORMATS,
    NULL,
    sizeof(USERS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 * Données des groupes
 -------------------------------------------------------------------*/
const char GROUPS_TABLE_SIG[] = "groups_table";

const char GROUPS_TABLE_GROUP_ID_SEQ[] = "groups_group_id_seq";
const char GROUPS_TABLE_GROUP_ID[] = "group_id";
const char GROUPS_TABLE_GROUP_NAME[] = "group_name";
const char GROUPS_TABLE_OWNER_ID[] = "owner_id";
const char GROUPS_TABLE_PARENT_ID[] = "parent_id";

const char *GROUPS_TABLE_COLUMNS_NAMES[] = {
    GROUPS_TABLE_GROUP_ID,
    GROUPS_TABLE_GROUP_NAME,
    GROUPS_TABLE_OWNER_ID,
    GROUPS_TABLE_PARENT_ID,
    NULL
};

const char *GROUPS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

const entity_def groups_table_def = {
    GROUPS_TABLE_SIG_ID,
    GROUPS_TABLE_SIG,
    NULL,
    GROUPS_TABLE_GROUP_ID_SEQ,
    GROUPS_TABLE_COLUMNS_NAMES,
    GROUPS_TABLE_COLUMNS_FORMATS,
    NULL,
    sizeof(GROUPS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 * Données des dossiers partagés
 -------------------------------------------------------------------*/
const char SHARED_FILES_TABLE_SIG[] = "shared_files_table";
const char SHARED_FILES_TABLE_FILE_ID[] = "file_id";
const char SHARED_FILES_TABLE_GROUP_ID[] = "group_id";

const char *SHARED_FILES_TABLE_COLUMNS_NAMES[] = {
    SHARED_FILES_TABLE_FILE_ID,
    SHARED_FILES_TABLE_GROUP_ID,
    NULL
};

const char *SHARED_FILES_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

const char *SHARED_FILES_TABLE_PRIMARY_KEY[] = {
    SHARED_FILES_TABLE_FILE_ID,
    SHARED_FILES_TABLE_GROUP_ID,
    NULL
};

const entity_def shared_files_table_def = {
    SHARED_FILES_TABLE_SIG_ID,
    SHARED_FILES_TABLE_SIG,
    SHARED_FILES_TABLE_PRIMARY_KEY,
    NULL,
    SHARED_FILES_TABLE_COLUMNS_NAMES,
    SHARED_FILES_TABLE_COLUMNS_FORMATS,
    NULL,
    sizeof(SHARED_FILES_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 * Données concernant l'appartenance des utilisateurs à des groupes
 -------------------------------------------------------------------*/
const char USERS_GROUPS_TABLE_SIG[] = "users_groups_table";
const char USERS_GROUPS_TABLE_USER_ID[] = "user_id";
const char USERS_GROUPS_TABLE_GROUP_ID[] = "group_id";

const char *USERS_GROUPS_TABLE_COLUMNS_NAMES[] = {
    USERS_GROUPS_TABLE_USER_ID,
    USERS_GROUPS_TABLE_GROUP_ID,
    NULL
};

const char *USERS_GROUPS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

const char *USERS_GROUPS_TABLE_PRIMARY_KEY[] = {
    USERS_GROUPS_TABLE_USER_ID,
    USERS_GROUPS_TABLE_GROUP_ID,
    NULL
};

const entity_def users_groups_table_def = {
    USERS_GROUPS_TABLE_SIG_ID,
    USERS_GROUPS_TABLE_SIG,
    USERS_GROUPS_TABLE_PRIMARY_KEY,
    NULL,
    USERS_GROUPS_TABLE_COLUMNS_NAMES,
    USERS_GROUPS_TABLE_COLUMNS_FORMATS,
    NULL,
    sizeof(USERS_GROUPS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 * Données des contacts
 -------------------------------------------------------------------*/
const char CONTACTS_TABLE_SIG[] = "contacts_table";
const char CONTACTS_TABLE_CONTACT_ID_SEQ[] = "contacts_contact_id_seq";
const char CONTACTS_TABLE_CONTACT_ID[] = "contact_id";
const char CONTACTS_TABLE_OWNER_ID[] = "owner_id";
const char CONTACTS_TABLE_NAME[] = "name";
const char CONTACTS_TABLE_FIRSTNAME[] = "firstname";
const char CONTACTS_TABLE_COMPANY[] = "company";
const char CONTACTS_TABLE_EMAIL_1[] = "email1";
const char CONTACTS_TABLE_EMAIL_2[] = "email2";
const char CONTACTS_TABLE_EMAIL_3[] = "email3";
const char CONTACTS_TABLE_PHONE_MOBILE[] = "phone_mobile";
const char CONTACTS_TABLE_PHONE_WORK[] = "phone_work";
const char CONTACTS_TABLE_PHONE_HOME[] = "phone_home";

const char *CONTACTS_TABLE_COLUMNS_NAMES[] = {
    CONTACTS_TABLE_CONTACT_ID,
    CONTACTS_TABLE_OWNER_ID,
    CONTACTS_TABLE_NAME,
    CONTACTS_TABLE_FIRSTNAME,
    CONTACTS_TABLE_COMPANY,
    CONTACTS_TABLE_EMAIL_1,
    CONTACTS_TABLE_EMAIL_2,
    CONTACTS_TABLE_EMAIL_3,
    CONTACTS_TABLE_PHONE_MOBILE,
    CONTACTS_TABLE_PHONE_WORK,
    CONTACTS_TABLE_PHONE_HOME,
    NULL
};

const char *CONTACTS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def contacts_table_def = {
    CONTACTS_TABLE_SIG_ID,
    CONTACTS_TABLE_SIG,
    NULL,
    CONTACTS_TABLE_CONTACT_ID_SEQ,
    CONTACTS_TABLE_COLUMNS_NAMES,
    CONTACTS_TABLE_COLUMNS_FORMATS,
    NULL,
    sizeof(CONTACTS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 * Donneees concernant l'appartenance des contacts a des groupes
 -------------------------------------------------------------------*/
const char CONTACTS_GROUPS_TABLE_SIG[] = "contacts_groups_table";
const char CONTACTS_GROUPS_TABLE_CONTACT_ID[] = "contact_id";
const char CONTACTS_GROUPS_TABLE_GROUP_ID[] = "group_id";

const char *CONTACTS_GROUPS_COLUMNS_NAMES[] = {
    CONTACTS_GROUPS_TABLE_CONTACT_ID,
    CONTACTS_GROUPS_TABLE_GROUP_ID,
    NULL
};

const char *CONTACTS_GROUPS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

const char *CONTACTS_GROUPS_PRIMARY_KEY[] = {
    CONTACTS_GROUPS_TABLE_CONTACT_ID,
    CONTACTS_GROUPS_TABLE_GROUP_ID,
    NULL
};

const entity_def contacts_groups_table_def = {
    CONTACTS_GROUPS_TABLE_SIG_ID,
    CONTACTS_GROUPS_TABLE_SIG,
    CONTACTS_GROUPS_PRIMARY_KEY,
    NULL,
    CONTACTS_GROUPS_COLUMNS_NAMES,
    CONTACTS_GROUPS_COLUMNS_FORMATS,
    NULL,
    sizeof(CONTACTS_GROUPS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 PROJETS
 -------------------------------------------------------------------*/
//! Projects table name
const char PROJECTS_TABLE_SIG[] = "projects_table";
const char PROJECTS_TABLE_PROJECT_ID_SEQ[] = "projects_project_id_seq";
//! Projects table signature identifier
const char PROJECTS_TABLE_PROJECT_ID[] = "project_id";
const char PROJECTS_TABLE_OWNER_ID[] = "owner_id";
const char PROJECTS_TABLE_SHORT_NAME[] = "short_name";
const char PROJECTS_TABLE_DESCRIPTION[] = "description";

const char *PROJECTS_COLUMNS_NAMES[] = {
    PROJECTS_TABLE_PROJECT_ID,
    PROJECTS_TABLE_OWNER_ID,
    PROJECTS_TABLE_SHORT_NAME,
    PROJECTS_TABLE_DESCRIPTION,
    NULL
};

const char *PROJECTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int PROJECTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE
};

const entity_def projects_table_def = {
    PROJECTS_TABLE_SIG_ID,
    PROJECTS_TABLE_SIG,
    NULL,
    PROJECTS_TABLE_PROJECT_ID_SEQ,
    PROJECTS_COLUMNS_NAMES,
    PROJECTS_COLUMNS_FORMATS,
    PROJECTS_COLUMNS_SIZES,
    sizeof(PROJECTS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                 PARAMETRES DE PROJETS
 -------------------------------------------------------------------*/
//! Parameters projects table name
const char PROJECTS_PARAMETERS_TABLE_SIG[] = "projects_parameters_table";
const char PROJECTS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID_SEQ[] = "projects_parameters_project_parameter_id_seq";
const char PROJECTS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID[] = "project_parameter_id";
const char PROJECTS_PARAMETERS_TABLE_PROJECT_ID[] = "project_id";
const char PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME[] = "parameter_name";
const char PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE[] = "parameter_value";

const char *PROJECTS_PARAMETERS_COLUMNS_NAMES[] = {
    PROJECTS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID,
    PROJECTS_PARAMETERS_TABLE_PROJECT_ID,
    PROJECTS_PARAMETERS_TABLE_PARAMETER_NAME,
    PROJECTS_PARAMETERS_TABLE_PARAMETER_VALUE,
    NULL
};

const char *PROJECTS_PARAMETERS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int PROJECTS_PARAMETERS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    LMEDIUM_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE
};


const entity_def projects_parameters_table_def = {
    PROJECTS_PARAMETERS_TABLE_SIG_ID,
    PROJECTS_PARAMETERS_TABLE_SIG,
    NULL,
    PROJECTS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID_SEQ,
    PROJECTS_PARAMETERS_COLUMNS_NAMES,
    PROJECTS_PARAMETERS_COLUMNS_FORMATS,
    PROJECTS_PARAMETERS_COLUMNS_SIZES,
    sizeof(PROJECTS_PARAMETERS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 VERSIONS DES PROJETS
 -------------------------------------------------------------------*/
//! Projects versions table name
const char PROJECTS_VERSIONS_TABLE_SIG[] = "projects_versions_table";
const char PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID_SEQ[] = "projects_versions_project_version_id_seq";
const char PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char PROJECTS_VERSIONS_TABLE_PROJECT_ID[] = "project_id";
const char PROJECTS_VERSIONS_TABLE_VERSION[] = "version";
const char PROJECTS_VERSIONS_TABLE_DESCRIPTION[] = "description";
const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE[] = "bug_tracker_type";
const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST[] = "bug_tracker_host";
const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL[] = "bug_tracker_url";
const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID[] = "bug_tracker_project_id";
const char PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION[] = "bug_tracker_project_version";

const char *PROJECTS_VERSIONS_COLUMNS_NAMES[] = {
    PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID,
    PROJECTS_VERSIONS_TABLE_PROJECT_ID,
    PROJECTS_VERSIONS_TABLE_VERSION,
    PROJECTS_VERSIONS_TABLE_DESCRIPTION,
    PROJECTS_VERSIONS_TABLE_BUG_TRACKER_TYPE,
    PROJECTS_VERSIONS_TABLE_BUG_TRACKER_HOST,
    PROJECTS_VERSIONS_TABLE_BUG_TRACKER_URL,
    PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_ID,
    PROJECTS_VERSIONS_TABLE_BUG_TRACKER_PROJECT_VERSION,
    NULL
};

const char *PROJECTS_VERSIONS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int PROJECTS_VERSIONS_PRIMARY_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    LLARGE_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SHORT_TEXT_SIZE
};

const entity_def projects_versions_table_def = {
    PROJECTS_VERSIONS_TABLE_SIG_ID,
    PROJECTS_VERSIONS_TABLE_SIG,
    NULL,
    PROJECTS_VERSIONS_TABLE_PROJECT_VERSION_ID_SEQ,
    PROJECTS_VERSIONS_COLUMNS_NAMES,
    PROJECTS_VERSIONS_COLUMNS_FORMATS,
    PROJECTS_VERSIONS_PRIMARY_SIZES,
    sizeof(PROJECTS_VERSIONS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                 PARAMETRES DE VERSION DE PROJETS
 -------------------------------------------------------------------*/
//! Parameters version projects table name
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_SIG[] = "projects_versions_parameters_table";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID_SEQ[] = "projects_versions_parameters_project_version_parameter_id_seq";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID[] = "project_version_parameter_id";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PARENT_GROUP_PARAMETER_ID[] = "parent_group_parameter_id";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_IS_GROUP[] = "is_group";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_NAME[] = "parameter_name";
const char PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_VALUE[] = "parameter_value";

const char *PROJECTS_VERSIONS_PARAMETERS_COLUMNS_NAMES[] = {
    PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_VERSION_ID,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_PARENT_GROUP_PARAMETER_ID,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_IS_GROUP,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_NAME,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_PARAMETER_VALUE,
    NULL
};

const char *PROJECTS_VERSIONS_PARAMETERS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int PROJECTS_VERSIONS_PARAMETERS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    1,
    LMEDIUM_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE
};


const entity_def projects_versions_parameters_table_def = {
    PROJECTS_VERSIONS_PARAMETERS_TABLE_SIG_ID,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_SIG,
    NULL,
    PROJECTS_VERSIONS_PARAMETERS_TABLE_PROJECT_PARAMETER_ID_SEQ,
    PROJECTS_VERSIONS_PARAMETERS_COLUMNS_NAMES,
    PROJECTS_VERSIONS_PARAMETERS_COLUMNS_FORMATS,
    PROJECTS_VERSIONS_PARAMETERS_COLUMNS_SIZES,
    sizeof(PROJECTS_VERSIONS_PARAMETERS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 TESTS_CONTENTS
 -------------------------------------------------------------------*/
//! Tests contents table name
const char TESTS_CONTENTS_TABLE_SIG[] = "tests_contents_table";
const char TESTS_CONTENTS_TABLE_TEST_CONTENET_ID_SEQ[] = "tests_contents_test_content_id_seq";
const char TESTS_CONTENTS_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID[] = "original_test_content_id";
const char TESTS_CONTENTS_TABLE_PROJECT_ID[] = "project_id";
const char TESTS_CONTENTS_TABLE_VERSION[] = "version";
const char TESTS_CONTENTS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char TESTS_CONTENTS_TABLE_SHORT_NAME[] = "short_name";
const char TESTS_CONTENTS_TABLE_DESCRIPTION[] = "description";
const char TESTS_CONTENTS_TABLE_PRIORITY_LEVEL[] = "priority_level";
const char TESTS_CONTENTS_TABLE_CATEGORY_ID[] = "category_id";
const char TESTS_CONTENTS_TABLE_STATUS[] = "status";
const char TESTS_CONTENTS_TABLE_AUTOMATED[] = "automated";
const char TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND[] = "automation_command";
const char TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS[] = "automation_command_parameters";
const char TESTS_CONTENTS_TABLE_TYPE[] = "type";
const char TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE[] = "limit_test_case";
const char TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE[] = "automation_command_return_code_variable";
const char TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE[] = "automation_command_stdout_variable";

const char TEST_CONTENT_TYPE_NOMINAL[] = "N";
const char TEST_CONTENT_TYPE_ALTERNATE[] = "A";
const char TEST_CONTENT_TYPE_EXCEPTION[] = "E";

const char TEST_CONTENT_TABLE_NOT_AUTOMATED[] = NO;
const char TEST_CONTENT_TABLE_AUTOMATED_GUI[] = YES;
const char TEST_CONTENT_TABLE_AUTOMATED_BATCH[] = "B";


const char *TESTS_CONTENTS_COLUMNS_NAMES[] = {
    TESTS_CONTENTS_TABLE_TEST_CONTENT_ID,
    TESTS_CONTENTS_TABLE_ORIGINAL_TEST_CONTENT_ID,
    TESTS_CONTENTS_TABLE_PROJECT_VERSION_ID,
    TESTS_CONTENTS_TABLE_PROJECT_ID,
    TESTS_CONTENTS_TABLE_VERSION,
    TESTS_CONTENTS_TABLE_SHORT_NAME,
    TESTS_CONTENTS_TABLE_DESCRIPTION,
    TESTS_CONTENTS_TABLE_PRIORITY_LEVEL,
    TESTS_CONTENTS_TABLE_CATEGORY_ID,
    TESTS_CONTENTS_TABLE_STATUS,
    TESTS_CONTENTS_TABLE_AUTOMATED,
    TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND,
    TESTS_CONTENTS_TABLE_AUTOMATION_COMMAND_PARAMETERS,
    TESTS_CONTENTS_TABLE_TYPE,
    TESTS_CONTENTS_TABLE_LIMIT_TEST_CASE,
    TESTS_CONTENTS_TABLE_AUTOMATION_RETURN_CODE_VARIABLE,
    TESTS_CONTENTS_TABLE_AUTOMATION_STDOUT_VARIABLE,
    NULL
};

const char *TESTS_CONTENTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int TESTS_CONTENTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    1,
    1,
    1,
    1,
    SLARGE_TEXT_SIZE,
    SLARGE_TEXT_SIZE,
    1,
    1,
    LMEDIUM_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE
};


const entity_def tests_contents_table_def = {
    TESTS_CONTENTS_TABLE_SIG_ID,
    TESTS_CONTENTS_TABLE_SIG,
    NULL,
    TESTS_CONTENTS_TABLE_TEST_CONTENET_ID_SEQ,
    TESTS_CONTENTS_COLUMNS_NAMES,
    TESTS_CONTENTS_COLUMNS_FORMATS,
    TESTS_CONTENTS_COLUMNS_SIZES,
    sizeof(TESTS_CONTENTS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                                 TESTS_CONTENTS_FILES
 -------------------------------------------------------------------*/
//! Tests attachments table name
const char TESTS_CONTENTS_FILES_TABLE_SIG[] = "tests_contents_files_table";
const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILE_ID_SIG[] = "tests_contents_files_test_content_file_id_seq";
const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILE_ID[] = "test_content_file_id";
const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILENAME[] = "test_content_filename";
const char TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_LO_ID[] = "test_content_lo_oid";

const char *TESTS_CONTENTS_FILES_COLUMNS_NAMES[] = {
    TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILE_ID,
    TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_ID,
    TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILENAME,
    TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_LO_ID,
    NULL
};

const char *TESTS_CONTENTS_FILES_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int TESTS_CONTENTS_FILES_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    LMEDIUM_TEXT_SIZE,
    PRIMARY_KEY_SIZE
};


const entity_def tests_contents_files_table_def = {
    TESTS_CONTENTS_FILES_TABLE_SIG_ID,
    TESTS_CONTENTS_FILES_TABLE_SIG,
    NULL,
    TESTS_CONTENTS_FILES_TABLE_TEST_CONTENT_FILE_ID_SIG,
    TESTS_CONTENTS_FILES_COLUMNS_NAMES,
    TESTS_CONTENTS_FILES_COLUMNS_FORMATS,
    TESTS_CONTENTS_FILES_COLUMNS_SIZES,
    sizeof(TESTS_CONTENTS_FILES_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                                 TESTS
 -------------------------------------------------------------------*/
//! Tests table name
const char TESTS_TABLE_SIG[] = "tests_table";
const char TESTS_TABLE_TEST_ID_SEQ[] = "tests_test_id_seq";
const char TESTS_TABLE_TEST_ID[] = "test_id";
const char TESTS_TABLE_ORIGINAL_TEST_ID[] = "original_test_id";
const char TESTS_TABLE_PARENT_TEST_ID[] = "parent_test_id";
const char TESTS_TABLE_PREVIOUS_TEST_ID[] = "previous_test_id";
const char TESTS_TABLE_PROJECT_ID[] = "project_id";
const char TESTS_TABLE_VERSION[] = "version";
const char TESTS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char TESTS_TABLE_TEST_CONTENT_ID[] = "test_content_id";

const char *TESTS_COLUMNS_NAMES[] = {
    TESTS_TABLE_TEST_ID,
    TESTS_TABLE_ORIGINAL_TEST_ID,
    TESTS_TABLE_PARENT_TEST_ID,
    TESTS_TABLE_PREVIOUS_TEST_ID,
    TESTS_TABLE_PROJECT_VERSION_ID,
    TESTS_TABLE_PROJECT_ID,
    TESTS_TABLE_VERSION,
    TESTS_TABLE_TEST_CONTENT_ID,
    NULL
};

const char *TESTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int TESTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    PRIMARY_KEY_SIZE
};

const entity_def tests_table_def = {
    TESTS_TABLE_SIG_ID,
    TESTS_TABLE_SIG,
    NULL,
    TESTS_TABLE_TEST_ID_SEQ,
    TESTS_COLUMNS_NAMES,
    TESTS_COLUMNS_FORMATS,
    TESTS_COLUMNS_SIZES,
    sizeof(TESTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 ACTIONS
 -------------------------------------------------------------------*/
//! Tests cases actions table name
const char ACTIONS_TABLE_SIG[] = "actions_table";
const char ACTIONS_TABLE_ACTION_ID_SEQ[] = "actions_action_id_seq";
const char ACTIONS_TABLE_ACTION_ID[] = "action_id";
const char ACTIONS_TABLE_PREVIOUS_ACTION_ID[] = "previous_action_id";
const char ACTIONS_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char ACTIONS_TABLE_SHORT_NAME[] = "short_name";
const char ACTIONS_TABLE_DESCRIPTION[] = "description";
const char ACTIONS_TABLE_WAIT_RESULT[] = "wait_result";
const char ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID[] = "link_original_test_content_id";

const char *ACTIONS_COLUMNS_NAMES[] = {
    ACTIONS_TABLE_ACTION_ID,
    ACTIONS_TABLE_PREVIOUS_ACTION_ID,
    ACTIONS_TABLE_TEST_CONTENT_ID,
    ACTIONS_TABLE_SHORT_NAME,
    ACTIONS_TABLE_DESCRIPTION,
    ACTIONS_TABLE_WAIT_RESULT,
    ACTIONS_TABLE_LINK_ORIGINAL_TEST_CONTENT_ID,
    NULL
};

const char *ACTIONS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int ACTIONS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    PRIMARY_KEY_SIZE
};

const entity_def actions_table_def = {
    ACTIONS_TABLE_SIG_ID,
    ACTIONS_TABLE_SIG,
    NULL,
    ACTIONS_TABLE_ACTION_ID_SEQ,
    ACTIONS_COLUMNS_NAMES,
    ACTIONS_COLUMNS_FORMATS,
    ACTIONS_COLUMNS_SIZES,
    sizeof(ACTIONS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                         AUTOMATED ACTIONS
 -------------------------------------------------------------------*/
//! Automated actions table name
const char AUTOMATED_ACTIONS_TABLE_SIG[] = "automated_actions_table";
const char AUTOMATED_ACTIONS_TABLE_ACTION_ID_SEQ[] = "automated_actions_action_id_seq";
const char AUTOMATED_ACTIONS_TABLE_ACTION_ID[] = "automated_action_id";
const char AUTOMATED_ACTIONS_TABLE_PREVIOUS_ACTION_ID[] = "previous_automated_action_id";
const char AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char AUTOMATED_ACTIONS_TABLE_WINDOW_ID[] = "window_id";
const char AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE[] = "message_type";
const char AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA[] = "message_data";
const char AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY[] = "message_time_delay";

#if (defined(_WINDOWS) || defined(WIN32))
#define        EVT_MSG_MOUSE_BUTTON_LEFT_DOWN        WM_LBUTTONDOWN
#define        EVT_MSG_MOUSE_BUTTON_LEFT_UP        WM_LBUTTONUP
#define        EVT_MSG_MOUSE_WHEEL_VERTICAL        WM_MOUSEWHEEL
#define        EVT_MSG_MOUSE_WHEEL_HORIZONTAL        WM_MOUSEHWHEEL
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_DOWN        WM_RBUTTONDOWN
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_UP        WM_RBUTTONUP
#define        EVT_MSG_MOUSE_MOVE            WM_MOUSEMOVE
#define        EVT_MSG_KEYBOARD_KEY_DOWN        WM_KEYDOWN
#define        EVT_MSG_KEYBOARD_KEY_UP            WM_KEYUP
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_DOWN    WM_SYSKEYDOWN
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_UP        WM_SYSKEYUP
#else
#define        EVT_MSG_MOUSE_BUTTON_LEFT_DOWN        0
#define        EVT_MSG_MOUSE_BUTTON_LEFT_UP        1
#define        EVT_MSG_MOUSE_WHEEL_VERTICAL        2
#define        EVT_MSG_MOUSE_WHEEL_HORIZONTAL        3
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_DOWN        4
#define        EVT_MSG_MOUSE_BUTTON_RIGHT_UP        5
#define        EVT_MSG_MOUSE_MOVE            6
#define        EVT_MSG_KEYBOARD_KEY_DOWN        7
#define        EVT_MSG_KEYBOARD_KEY_UP            8
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_DOWN    9
#define        EVT_MSG_KEYBOARD_SYSTEM_KEY_UP        10
#endif

const char *AUTOMATED_ACTIONS_COLUMNS_NAMES[] = {
    AUTOMATED_ACTIONS_TABLE_ACTION_ID,
    AUTOMATED_ACTIONS_TABLE_PREVIOUS_ACTION_ID,
    AUTOMATED_ACTIONS_TABLE_TEST_CONTENT_ID,
    AUTOMATED_ACTIONS_TABLE_WINDOW_ID,
    AUTOMATED_ACTIONS_TABLE_MESSAGE_TYPE,
    AUTOMATED_ACTIONS_TABLE_MESSAGE_DATA,
    AUTOMATED_ACTIONS_TABLE_MESSAGE_TIME_DELAY,
    NULL
};

const char *AUTOMATED_ACTIONS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int AUTOMATED_ACTIONS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    S1LARGE_TEXT_SIZE,
    NUMBER_SIZE,
    S1LARGE_TEXT_SIZE,
    NUMBER_SIZE
};

const entity_def automated_actions_table_def = {
    AUTOMATED_ACTIONS_TABLE_SIG_ID,
    AUTOMATED_ACTIONS_TABLE_SIG,
    NULL,
    AUTOMATED_ACTIONS_TABLE_ACTION_ID_SEQ,
    AUTOMATED_ACTIONS_COLUMNS_NAMES,
    AUTOMATED_ACTIONS_COLUMNS_FORMATS,
    AUTOMATED_ACTIONS_COLUMNS_SIZES,
    sizeof(AUTOMATED_ACTIONS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                     AUTOMATED ACTIONS VALIDATIONS
 -------------------------------------------------------------------*/
//! Automated actions validations table name
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_SIG[] = "automated_actions_validations_table";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_ID_SEQ[] = "automated_actions_validations_validation_id_seq";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_ID[] = "validation_id";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_PREVIOUS_VALIDATION_ID[] = "previous_validation_id";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_AUTOMATED_ACTION_ID[] = "automated_action_id";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME[] = "module_name";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION[] = "module_version";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME[] = "module_function_name";
const char AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_PARAMETERS[] = "module_function_parameters";

const char *AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_NAMES[] = {
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_ID,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_PREVIOUS_VALIDATION_ID,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_AUTOMATED_ACTION_ID,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_NAME,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_VERSION,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_NAME,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_MODULE_FUNCTION_PARAMETERS,
    NULL
};

const char *AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SMEDIUM_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE,
};

const entity_def automated_actions_validations_table_def = {
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_SIG_ID,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_SIG,
    NULL,
    AUTOMATED_ACTIONS_VALIDATIONS_TABLE_VALIDATION_ID_SEQ,
    AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_NAMES,
    AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_FORMATS,
    AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_SIZES,
    sizeof(AUTOMATED_ACTIONS_VALIDATIONS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                         CONTENUS D'EXIGENCES
 -------------------------------------------------------------------*/
//! Requirements contents table name
const char REQUIREMENTS_CONTENTS_TABLE_SIG[] = "requirements_contents_table";
const char REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_ID_SEQ[] = "requirements_contents_requirement_content_id_seq";
const char REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_CONTENT_ID[] = "requirement_content_id";
const char REQUIREMENTS_CONTENTS_TABLE_ORIGINAL_REQUIREMENT_CONTENT_ID[] = "original_requirement_content_id";
const char REQUIREMENTS_CONTENTS_TABLE_PROJECT_ID[] = "project_id";
const char REQUIREMENTS_CONTENTS_TABLE_VERSION[] = "version";
const char REQUIREMENTS_CONTENTS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME[] = "short_name";
const char REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION[] = "description";
const char REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID[] = "category_id";
const char REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL[] = "priority_level";
const char REQUIREMENTS_CONTENTS_TABLE_STATUS[] = "status";
const char REQUIREMENTS_CONTENTS_TABLE_FEATURE_CONTENT_ID[] = "feature_content_id";

const char *REQUIREMENTS_CONTENTS_COLUMNS_NAMES[] = {
    REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_CONTENT_ID,
    REQUIREMENTS_CONTENTS_TABLE_ORIGINAL_REQUIREMENT_CONTENT_ID,
    REQUIREMENTS_CONTENTS_TABLE_PROJECT_VERSION_ID,
    REQUIREMENTS_CONTENTS_TABLE_PROJECT_ID,
    REQUIREMENTS_CONTENTS_TABLE_VERSION,
    REQUIREMENTS_CONTENTS_TABLE_SHORT_NAME,
    REQUIREMENTS_CONTENTS_TABLE_DESCRIPTION,
    REQUIREMENTS_CONTENTS_TABLE_CATEGORY_ID,
    REQUIREMENTS_CONTENTS_TABLE_PRIORITY_LEVEL,
    REQUIREMENTS_CONTENTS_TABLE_STATUS,
    REQUIREMENTS_CONTENTS_TABLE_FEATURE_CONTENT_ID,
    NULL
};

const char *REQUIREMENTS_CONTENTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int REQUIREMENTS_CONTENTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    1,
    1,
    1,
    PRIMARY_KEY_SIZE
};


const entity_def requirements_contents_table_def = {
    REQUIREMENTS_CONTENTS_TABLE_SIG_ID,
    REQUIREMENTS_CONTENTS_TABLE_SIG,
    NULL,
    REQUIREMENTS_CONTENTS_TABLE_REQUIREMENT_ID_SEQ,
    REQUIREMENTS_CONTENTS_COLUMNS_NAMES,
    REQUIREMENTS_CONTENTS_COLUMNS_FORMATS,
    REQUIREMENTS_CONTENTS_COLUMNS_SIZES,
    sizeof(REQUIREMENTS_CONTENTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 EXIGENCES
 -------------------------------------------------------------------*/
//! Requirements table name
const char REQUIREMENTS_TABLE_SIG[] = "requirements_table";
const char REQUIREMENTS_TABLE_REQUIREMENT_ID_SEQ[] = "requirements_requirement_id_seq";
const char REQUIREMENTS_TABLE_REQUIREMENT_ID[] = "requirement_id";
const char REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID[] = "requirement_content_id";
const char REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID[] = "parent_requirement_id";
const char REQUIREMENTS_TABLE_PREVIOUS_REQUIREMENT_ID[] = "previous_requirement_id";
const char REQUIREMENTS_TABLE_PROJECT_ID[] = "project_id";
const char REQUIREMENTS_TABLE_VERSION[] = "version";
const char REQUIREMENTS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";

const char *REQUIREMENTS_COLUMNS_NAMES[] = {
    REQUIREMENTS_TABLE_REQUIREMENT_ID,
    REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID,
    REQUIREMENTS_TABLE_PARENT_REQUIREMENT_ID,
    REQUIREMENTS_TABLE_PREVIOUS_REQUIREMENT_ID,
    REQUIREMENTS_TABLE_PROJECT_VERSION_ID,
    REQUIREMENTS_TABLE_PROJECT_ID,
    REQUIREMENTS_TABLE_VERSION,
    NULL
};

const char *REQUIREMENTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int REQUIREMENTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12
};


const entity_def requirements_table_def = {
    REQUIREMENTS_TABLE_SIG_ID,
    REQUIREMENTS_TABLE_SIG,
    NULL,
    REQUIREMENTS_TABLE_REQUIREMENT_ID_SEQ,
    REQUIREMENTS_COLUMNS_NAMES,
    REQUIREMENTS_COLUMNS_FORMATS,
    REQUIREMENTS_COLUMNS_SIZES,
    sizeof(REQUIREMENTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                             CATEGORIES D'EXIGENCES
 -------------------------------------------------------------------*/
//! Requirements categories table name
const char REQUIREMENTS_CATEGORIES_TABLE_SIG[] = "requirements_categories_table";
const char REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID[] = "category_id";
const char REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL[] = "category_label";

const char *REQUIREMENTS_CATEGORIES_COLUMNS_NAMES[] = {
    REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_ID,
    REQUIREMENTS_CATEGORIES_TABLE_CATEGORY_LABEL,
    NULL
};

const char *REQUIREMENTS_CATEGORIES_COLUMNS_FORMATS[] = {
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def requirements_categories_table_def = {
    REQUIREMENTS_CATEGORIES_TABLE_SIG_ID,
    REQUIREMENTS_CATEGORIES_TABLE_SIG,
    NULL,
    NULL,
    REQUIREMENTS_CATEGORIES_COLUMNS_NAMES,
    REQUIREMENTS_CATEGORIES_COLUMNS_FORMATS,
    NULL,
    sizeof(REQUIREMENTS_CATEGORIES_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 EXIGENCES DE TESTS
 -------------------------------------------------------------------*/
//! Requirements and tests associations table name
const char TESTS_REQUIREMENTS_TABLE_SIG[] = "tests_requirements_table";
const char TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID[] = "requirement_content_id";

const char *TESTS_REQUIREMENTS_COLUMNS_NAMES[] = {
    TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID,
    TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID,
    NULL
};

const char *TESTS_REQUIREMENTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int TESTS_REQUIREMENTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE
};


const char *TESTS_REQUIREMENTS_PRIMARY_KEY[] = {
    TESTS_REQUIREMENTS_TABLE_TEST_CONTENT_ID,
    TESTS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT_ID,
    NULL
};

const entity_def tests_requirements_table_def = {
    TESTS_REQUIREMENTS_TABLE_SIG_ID,
    TESTS_REQUIREMENTS_TABLE_SIG,
    TESTS_REQUIREMENTS_PRIMARY_KEY,
    NULL,
    TESTS_REQUIREMENTS_COLUMNS_NAMES,
    TESTS_REQUIREMENTS_COLUMNS_FORMATS,
    TESTS_REQUIREMENTS_COLUMNS_SIZES,
    sizeof(TESTS_REQUIREMENTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 CAMPAGNES
 -------------------------------------------------------------------*/
//! Campaigns table name
const char CAMPAIGNS_TABLE_SIG[] = "campaigns_table";
const char CAMPAIGNS_TABLE_CAMPAIGN_ID_SEQ[] = "campaigns_campaign_id_seq";
const char CAMPAIGNS_TABLE_CAMPAIGN_ID[] = "campaign_id";
const char CAMPAIGNS_TABLE_PROJECT_ID[] = "project_id";
const char CAMPAIGNS_TABLE_PROJECT_VERSION[] = "version";
const char CAMPAIGNS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char CAMPAIGNS_TABLE_SHORT_NAME[] = "short_name";
const char CAMPAIGNS_TABLE_DESCRIPTION[] = "description";

const char *CAMPAIGNS_COLUMNS_NAMES[] = {
    CAMPAIGNS_TABLE_CAMPAIGN_ID,
    CAMPAIGNS_TABLE_PROJECT_VERSION_ID,
    CAMPAIGNS_TABLE_PROJECT_ID,
    CAMPAIGNS_TABLE_PROJECT_VERSION,
    CAMPAIGNS_TABLE_SHORT_NAME,
    CAMPAIGNS_TABLE_DESCRIPTION,
    NULL
};

const char *CAMPAIGNS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int CAMPAIGNS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE
};


const entity_def campaigns_table_def = {
    CAMPAIGNS_TABLE_SIG_ID,
    CAMPAIGNS_TABLE_SIG,
    NULL,
    CAMPAIGNS_TABLE_CAMPAIGN_ID_SEQ,
    CAMPAIGNS_COLUMNS_NAMES,
    CAMPAIGNS_COLUMNS_FORMATS,
    CAMPAIGNS_COLUMNS_SIZES,
    sizeof(CAMPAIGNS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                         TESTS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Tests campaigns table name
const char TESTS_CAMPAIGNS_TABLE_SIG[] = "tests_campaigns_table";
const char TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID_SEQ[] = "tests_campaigns_test_campaign_id_seq";
const char TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID[] = "test_campaign_id";
const char TESTS_CAMPAIGNS_TABLE_CAMPAIGN_ID[] = "campaign_id";
const char TESTS_CAMPAIGNS_TABLE_TEST_ID[] = "test_id";
const char TESTS_CAMPAIGNS_TABLE_PREVIOUS_TEST_CAMPAIGN_ID[] = "previous_test_campaign_id";

const char *TESTS_CAMPAIGNS_COLUMNS_NAMES[] = {
    TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID,
    TESTS_CAMPAIGNS_TABLE_CAMPAIGN_ID,
    TESTS_CAMPAIGNS_TABLE_TEST_ID,
    TESTS_CAMPAIGNS_TABLE_PREVIOUS_TEST_CAMPAIGN_ID,
    NULL
};

const char *TESTS_CAMPAIGNS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int TESTS_CAMPAIGNS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE
};


const entity_def tests_campaigns_table_def = {
    TESTS_CAMPAIGNS_TABLE_SIG_ID,
    TESTS_CAMPAIGNS_TABLE_SIG,
    NULL,
    TESTS_CAMPAIGNS_TABLE_TEST_CAMPAIGN_ID_SEQ,
    TESTS_CAMPAIGNS_COLUMNS_NAMES,
    TESTS_CAMPAIGNS_COLUMNS_FORMATS,
    TESTS_CAMPAIGNS_COLUMNS_SIZES,
    sizeof(TESTS_CAMPAIGNS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                     EXECUTIONS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions campaigns table name
const char EXECUTIONS_CAMPAIGNS_TABLE_SIG[] = "executions_campaigns_table";
const char EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID_SEQ[] = "executions_campaigns_execution_campaign_id_seq";
const char EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID[] = "execution_campaign_id";
const char EXECUTIONS_CAMPAIGNS_TABLE_CAMPAIGN_ID[] = "campaign_id";
const char EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE[] = "execution_date";
const char EXECUTIONS_CAMPAIGNS_TABLE_REVISION[] = "revision";
const char EXECUTIONS_CAMPAIGNS_TABLE_USER_ID[] = "user_id";
const char EXECUTIONS_CAMPAIGNS_TABLE_TEST_PLAN_ID[] = "test_plan_id";

const char *EXECUTIONS_CAMPAIGNS_COLUMNS_NAMES[] = {
    EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID,
    EXECUTIONS_CAMPAIGNS_TABLE_CAMPAIGN_ID,
    EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_DATE,
    EXECUTIONS_CAMPAIGNS_TABLE_REVISION,
    EXECUTIONS_CAMPAIGNS_TABLE_USER_ID,
    EXECUTIONS_CAMPAIGNS_TABLE_TEST_PLAN_ID,
    NULL
};

const char *EXECUTIONS_CAMPAIGNS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    DATE_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int EXECUTIONS_CAMPAIGNS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    DATE_SIZE,
    24,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE
};



const entity_def executions_campaigns_table_def = {
    EXECUTIONS_CAMPAIGNS_TABLE_SIG_ID,
    EXECUTIONS_CAMPAIGNS_TABLE_SIG,
    NULL,
    EXECUTIONS_CAMPAIGNS_TABLE_EXECUTION_CAMPAIGN_ID_SEQ,
    EXECUTIONS_CAMPAIGNS_COLUMNS_NAMES,
    EXECUTIONS_CAMPAIGNS_COLUMNS_FORMATS,
    EXECUTIONS_CAMPAIGNS_COLUMNS_SIZES,
    sizeof(EXECUTIONS_CAMPAIGNS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                 PARAMETRES D'EXECUTIONS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions campaigns parameters table name
const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_SIG[] = "executions_campaigns_parameters_table";
const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_ID_SEQ[] = "executions_campaigns_parameters_parameter_id_seq";
const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_PARAMETER_ID[] = "execution_campaign_parameter_id";
const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_ID[] = "execution_campaign_id";
const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME[] = "parameter_name";
const char EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_VALUE[] = "parameter_value";

const char *EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_NAMES[] = {
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_PARAMETER_ID,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_EXECUTION_CAMPAIGN_ID,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_NAME,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_VALUE,
    NULL
};

const char *EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    LMEDIUM_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE
};


const entity_def executions_campaigns_parameters_table_def = {
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_SIG_ID,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_SIG,
    NULL,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_TABLE_PARAMETER_ID_SEQ,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_NAMES,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_FORMATS,
    EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_SIZES,
    sizeof(EXECUTIONS_CAMPAIGNS_PARAMETERS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *             EXECUTIONS DE TESTS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions tests campaigns name
const char EXECUTIONS_TESTS_TABLE_SIG[] = "executions_tests_table";
const char EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID_SEQ[] = "executions_tests_execution_test_id_seq";
const char EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID[] = "execution_test_id";
const char EXECUTIONS_TESTS_TABLE_EXECUTION_CAMPAIGN_ID[] = "execution_campaign_id";
const char EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID[] = "parent_execution_test_id";
const char EXECUTIONS_TESTS_TABLE_PREVIOUS_EXECUTION_TEST_ID[] = "previous_execution_test_id";
const char EXECUTIONS_TESTS_TABLE_TEST_ID[] = "test_id";
const char EXECUTIONS_TESTS_TABLE_EXECUTION_DATE[] = "execution_date";
const char EXECUTIONS_TESTS_TABLE_RESULT_ID[] = "result_id";
const char EXECUTIONS_TESTS_TABLE_COMMENTS[] = "comments";
const char EXECUTIONS_TESTS_TABLE_AUTOMATION_RETURN_CODE[] = "automation_command_return_code";
const char EXECUTIONS_TESTS_TABLE_AUTOMATION_STDOUT[] = "automation_command_stdout";
const char EXECUTIONS_TESTS_TABLE_USER_ID[] = "user_id";

const char *EXECUTIONS_TESTS_COLUMNS_NAMES[] = {
    EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID,
    EXECUTIONS_TESTS_TABLE_EXECUTION_CAMPAIGN_ID,
    EXECUTIONS_TESTS_TABLE_PARENT_EXECUTION_TEST_ID,
    EXECUTIONS_TESTS_TABLE_PREVIOUS_EXECUTION_TEST_ID,
    EXECUTIONS_TESTS_TABLE_TEST_ID,
    EXECUTIONS_TESTS_TABLE_EXECUTION_DATE,
    EXECUTIONS_TESTS_TABLE_RESULT_ID,
    EXECUTIONS_TESTS_TABLE_COMMENTS,
    EXECUTIONS_TESTS_TABLE_AUTOMATION_RETURN_CODE,
    EXECUTIONS_TESTS_TABLE_AUTOMATION_STDOUT,
    EXECUTIONS_TESTS_TABLE_USER_ID,
    NULL
};

const char *EXECUTIONS_TESTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    DATE_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int EXECUTIONS_TESTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    DATE_SIZE,
    1,
    LLARGE_TEXT_SIZE,
    NUMBER_SIZE,
    LLARGE_TEXT_SIZE,
    PRIMARY_KEY_SIZE,
};



const entity_def executions_tests_table_def = {
    EXECUTIONS_TESTS_TABLE_SIG_ID,
    EXECUTIONS_TESTS_TABLE_SIG,
    NULL,
    EXECUTIONS_TESTS_TABLE_EXECUTION_TEST_ID_SEQ,
    EXECUTIONS_TESTS_COLUMNS_NAMES,
    EXECUTIONS_TESTS_COLUMNS_FORMATS,
    EXECUTIONS_TESTS_COLUMNS_SIZES,
    sizeof(EXECUTIONS_TESTS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                 PARAMETRES D'EXECUTIONS DE TESTS
 -------------------------------------------------------------------*/
//! Executions parameters table name
const char EXECUTIONS_TESTS_PARAMETERS_TABLE_SIG[] = "executions_tests_parameters_table";
const char EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_ID_SEQ[] = "executions_tests_parameters_parameter_id_seq";
const char EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_PARAMETER_ID[] = "execution_test_parameter_id";
const char EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_ID[] = "execution_test_id";
const char EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_NAME[] = "parameter_name";
const char EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE[] = "parameter_value";

const char *EXECUTIONS_TESTS_PARAMETERS_COLUMNS_NAMES[] = {
    EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_PARAMETER_ID,
    EXECUTIONS_TESTS_PARAMETERS_TABLE_EXECUTION_TEST_ID,
    EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_NAME,
    EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_VALUE,
    NULL
};

const char *EXECUTIONS_TESTS_PARAMETERS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int EXECUTIONS_TESTS_PARAMETERS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    LMEDIUM_TEXT_SIZE,
    LMEDIUM_TEXT_SIZE
};


const entity_def executions_tests_parameters_table_def = {
    EXECUTIONS_TESTS_PARAMETERS_TABLE_SIG_ID,
    EXECUTIONS_TESTS_PARAMETERS_TABLE_SIG,
    NULL,
    EXECUTIONS_TESTS_PARAMETERS_TABLE_PARAMETER_ID_SEQ,
    EXECUTIONS_TESTS_PARAMETERS_COLUMNS_NAMES,
    EXECUTIONS_TESTS_PARAMETERS_COLUMNS_FORMATS,
    EXECUTIONS_TESTS_PARAMETERS_COLUMNS_SIZES,
    sizeof(EXECUTIONS_TESTS_PARAMETERS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *     RESULTATS D'EXECUTIONS DE TESTS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions tests results table name
const char TESTS_RESULTS_TABLE_SIG[] = "tests_results_table";
const char TESTS_RESULTS_TABLE_RESULT_ID[] = "result_id";
const char TESTS_RESULTS_TABLE_DESCRIPTION[] = "description";

const char EXECUTION_TEST_VALIDATED[] = "0";
const char EXECUTION_TEST_INVALIDATED[] = "1";
const char EXECUTION_TEST_BYPASSED[] = "2";
const char EXECUTION_TEST_INCOMPLETED[] = "3";

const char *TESTS_RESULTS_COLUMNS_NAMES[] = {
    TESTS_RESULTS_TABLE_RESULT_ID,
    TESTS_RESULTS_TABLE_DESCRIPTION,
    NULL
};

const char *TESTS_RESULTS_COLUMNS_FORMATS[] = {
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def tests_results_table_def = {
    TESTS_RESULTS_TABLE_SIG_ID,
    TESTS_RESULTS_TABLE_SIG,
    NULL,
    NULL,
    TESTS_RESULTS_COLUMNS_NAMES,
    TESTS_RESULTS_COLUMNS_FORMATS,
    NULL,
    sizeof(TESTS_RESULTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *     EXECUTIONS D'ACTIONS DE TESTS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions actions name
const char EXECUTIONS_ACTIONS_TABLE_SIG[] = "executions_actions_table";
const char EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID_SEQ[] = "executions_actions_execution_action_id_seq";
const char EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID[] = "execution_action_id";
const char EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID[] = "execution_test_id";
const char EXECUTIONS_ACTIONS_TABLE_ACTION_ID[] = "action_id";
const char EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID[] = "previous_execution_action_id";
const char EXECUTIONS_ACTIONS_TABLE_RESULT_ID[] = "result_id";
const char EXECUTIONS_ACTIONS_TABLE_COMMENTS[] = "comments";

const char *EXECUTIONS_ACTIONS_COLUMNS_NAMES[] = {
    EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID,
    EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID,
    EXECUTIONS_ACTIONS_TABLE_ACTION_ID,
    EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID,
    EXECUTIONS_ACTIONS_TABLE_RESULT_ID,
    EXECUTIONS_ACTIONS_TABLE_COMMENTS,
    NULL
};

const char *EXECUTIONS_ACTIONS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int EXECUTIONS_ACTIONS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    1,
    LLARGE_TEXT_SIZE
};


const entity_def executions_actions_table_def = {
    EXECUTIONS_ACTIONS_TABLE_SIG_ID,
    EXECUTIONS_ACTIONS_TABLE_SIG,
    NULL,
    EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID_SEQ,
    EXECUTIONS_ACTIONS_COLUMNS_NAMES,
    EXECUTIONS_ACTIONS_COLUMNS_FORMATS,
    EXECUTIONS_ACTIONS_COLUMNS_SIZES,
    sizeof(EXECUTIONS_ACTIONS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *     EXECUTIONS D'ACTIONS AUTOMATISEES DE TESTS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions actions name
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_SIG[] = "automated_executions_actions_table";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID_SEQ[] = "automated_executions_actions_execution_action_id_seq";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID[] = "automated_execution_action_id";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID[] = "execution_test_id";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_ACTION_ID[] = "automated_action_id";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID[] = "previous_automated_execution_action_id";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_RESULT_ID[] = "result_id";
const char AUTOMATED_EXECUTIONS_ACTIONS_TABLE_COMMENTS[] = "comments";

const char *AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_NAMES[] = {
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_TEST_ID,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_ACTION_ID,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_PREVIOUS_EXECUTION_ACTION_ID,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_RESULT_ID,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_COMMENTS,
    NULL
};

const char *AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    1,
    LLARGE_TEXT_SIZE
};


const entity_def automated_executions_actions_table_def = {
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_SIG_ID,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_SIG,
    NULL,
    AUTOMATED_EXECUTIONS_ACTIONS_TABLE_EXECUTION_ACTION_ID_SEQ,
    AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_NAMES,
    AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_FORMATS,
    AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_SIZES,
    sizeof(AUTOMATED_EXECUTIONS_ACTIONS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *     RESULTATS D'EXECUTIONS D'ACTIONS DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Excutions actions results table name
const char ACTIONS_RESULTS_TABLE_SIG[] = "actions_results_table";
const char ACTIONS_RESULTS_TABLE_RESULT_ID[] = "result_id";
const char ACTIONS_RESULTS_TABLE_DESCRIPTION[] = "description";

const char EXECUTION_ACTION_VALIDATED[] = "0";
const char EXECUTION_ACTION_INVALIDATED[] = "1";
const char EXECUTION_ACTION_BYPASSED[] = "2";

const char *ACTIONS_RESULTS_COLUMNS_NAMES[] = {
    ACTIONS_RESULTS_TABLE_RESULT_ID,
    ACTIONS_RESULTS_TABLE_DESCRIPTION,
    NULL
};

const char *ACTIONS_RESULTS_COLUMNS_FORMATS[] = {
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def actions_results_table_def = {
    ACTIONS_RESULTS_TABLE_SIG_ID,
    ACTIONS_RESULTS_TABLE_SIG,
    NULL,
    NULL,
    ACTIONS_RESULTS_COLUMNS_NAMES,
    ACTIONS_RESULTS_COLUMNS_FORMATS,
    NULL,
    sizeof(ACTIONS_RESULTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *     EXECUTIONS D'EXIGENCES DE CAMPAGNES
 -------------------------------------------------------------------*/
//! Executions requirements table name
const char EXECUTIONS_REQUIREMENTS_TABLE_SIG[] = "executions_requirements_table";
const char EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_ACTION_ID_SEQ[] = "executions_requirements_execution_requirement_seq";
const char EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_REQUIREMENT_ID[] = "execution_requirement_id";
const char EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_CAMPAIGN_ID[] = "execution_campaign_id";
const char EXECUTIONS_REQUIREMENTS_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char EXECUTIONS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT__ID[] = "requirement_content_id";
const char EXECUTIONS_REQUIREMENTS_TABLE_RESULT_ID[] = "result_id";
const char EXECUTIONS_REQUIREMENTS_TABLE_COMMENTS[] = "comments";

const char *EXECUTIONS_REQUIREMENTS_COLUMNS_NAMES[] = {
    EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_REQUIREMENT_ID,
    EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_CAMPAIGN_ID,
    EXECUTIONS_REQUIREMENTS_TABLE_TEST_CONTENT_ID,
    EXECUTIONS_REQUIREMENTS_TABLE_REQUIREMENT_CONTENT__ID,
    EXECUTIONS_REQUIREMENTS_TABLE_RESULT_ID,
    EXECUTIONS_REQUIREMENTS_TABLE_COMMENTS,
    NULL
};

const char *EXECUTIONS_REQUIREMENTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int EXECUTIONS_REQUIREMENTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    1,
    LARGE_TEXT_SIZE
};

const entity_def executions_requirements_table_def = {
    EXECUTIONS_REQUIREMENTS_TABLE_SIG_ID,
    EXECUTIONS_REQUIREMENTS_TABLE_SIG,
    NULL,
    EXECUTIONS_REQUIREMENTS_TABLE_EXECUTION_ACTION_ID_SEQ,
    EXECUTIONS_REQUIREMENTS_COLUMNS_NAMES,
    EXECUTIONS_REQUIREMENTS_COLUMNS_FORMATS,
    EXECUTIONS_REQUIREMENTS_COLUMNS_SIZES,
    sizeof(EXECUTIONS_REQUIREMENTS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                                 DROITS SUR LES PROJETS
 -------------------------------------------------------------------*/
//! Projects grants table name
const char PROJECTS_GRANTS_TABLE_SIG[] = "projects_grants_table";
const char PROJECTS_GRANTS_TABLE_PROJECT_GRANT_ID_SEQ[] = "projects_grants_project_grant_id_seq";
const char PROJECTS_GRANTS_TABLE_PROJECT_GRANT_ID[] = "project_grant_id";
const char PROJECTS_GRANTS_TABLE_PROJECT_ID[] = "project_id";
const char PROJECTS_GRANTS_TABLE_USERNAME[] = "username";
const char PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID[] = "table_signature_id";
const char PROJECTS_GRANTS_TABLE_RIGHTS[] = "rights";

const char PROJECT_GRANT_NONE[] = "N";
const char PROJECT_GRANT_READ[] = "R";
const char PROJECT_GRANT_WRITE[] = "W";

const char *PROJECTS_GRANTS_COLUMNS_NAMES[] = {
    PROJECTS_GRANTS_TABLE_PROJECT_GRANT_ID,
    PROJECTS_GRANTS_TABLE_PROJECT_ID,
    PROJECTS_GRANTS_TABLE_USERNAME,
    PROJECTS_GRANTS_TABLE_TABLE_SIGNATURE_ID,
    PROJECTS_GRANTS_TABLE_RIGHTS,
    NULL
};

const char *PROJECTS_GRANTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int PROJECTS_GRANTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SHORT_TEXT_SIZE,
    PRIMARY_KEY_SIZE,
    1
};


const entity_def projects_grants_table_def = {
    PROJECTS_GRANTS_TABLE_SIG_ID,
    PROJECTS_GRANTS_TABLE_SIG,
    NULL,
    PROJECTS_GRANTS_TABLE_PROJECT_GRANT_ID_SEQ,
    PROJECTS_GRANTS_COLUMNS_NAMES,
    PROJECTS_GRANTS_COLUMNS_FORMATS,
    PROJECTS_GRANTS_COLUMNS_SIZES,
    sizeof(PROJECTS_GRANTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                             HIERARCHIE DE TESTS
 -------------------------------------------------------------------*/
//! Tests hierarchy view name
const char TESTS_HIERARCHY_SIG[] = "tests_hierarchy";
const char TESTS_HIERARCHY_TEST_ID[] = "test_id";
const char TESTS_HIERARCHY_ORIGINAL_TEST_ID[] = "original_test_id";
const char TESTS_HIERARCHY_PARENT_TEST_ID[] = "parent_test_id";
const char TESTS_HIERARCHY_PREVIOUS_TEST_ID[] = "previous_test_id";
const char TESTS_HIERARCHY_PROJECT_VERSION_ID[] = "project_version_id";
const char TESTS_HIERARCHY_PROJECT_ID[] = "project_id";
const char TESTS_HIERARCHY_VERSION[] = "version";
const char TESTS_HIERARCHY_TEST_CONTENT_ID[] = "test_content_id";
const char TESTS_HIERARCHY_SHORT_NAME[] = "short_name";
const char TESTS_HIERARCHY_CATEGORY_ID[] = "category_id";
const char TESTS_HIERARCHY_PRIORITY_LEVEL[] = "priority_level";
const char TESTS_HIERARCHY_CONTENT_VERSION[] = "content_version";
const char TESTS_HIERARCHY_STATUS[] = "status";
const char TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID[] = "original_test_content_id";
const char TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED[] = "content_automated";
const char TESTS_HIERARCHY_TEST_CONTENT_TYPE[] = "content_type";

const char *TESTS_HIERARCHY_COLUMNS_NAMES[] = {
    TESTS_HIERARCHY_TEST_ID,
    TESTS_HIERARCHY_ORIGINAL_TEST_ID,
    TESTS_HIERARCHY_PARENT_TEST_ID,
    TESTS_HIERARCHY_PREVIOUS_TEST_ID,
    TESTS_HIERARCHY_PROJECT_VERSION_ID,
    TESTS_HIERARCHY_PROJECT_ID,
    TESTS_HIERARCHY_VERSION,
    TESTS_HIERARCHY_TEST_CONTENT_ID,
    TESTS_HIERARCHY_SHORT_NAME,
    TESTS_HIERARCHY_CATEGORY_ID,
    TESTS_HIERARCHY_PRIORITY_LEVEL,
    TESTS_HIERARCHY_CONTENT_VERSION,
    TESTS_HIERARCHY_STATUS,
    TESTS_HIERARCHY_ORIGINAL_TEST_CONTENT_ID,
    TESTS_HIERARCHY_TEST_CONTENT_AUTOMATED,
    TESTS_HIERARCHY_TEST_CONTENT_TYPE,
    NULL
};

const char *TESTS_HIERARCHY_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int TESTS_HIERARCHY_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    PRIMARY_KEY_SIZE,
    SHORT_TEXT_SIZE,
    1,
    1,
    12,
    1,
    PRIMARY_KEY_SIZE,
    1,
    1
};

const entity_def tests_hierarchy_def = {
    TESTS_HIERARCHY_SIG_ID,
    TESTS_HIERARCHY_SIG,
    NULL,
    NULL,
    TESTS_HIERARCHY_COLUMNS_NAMES,
    TESTS_HIERARCHY_COLUMNS_FORMATS,
    TESTS_HIERARCHY_COLUMNS_SIZES,
    sizeof(TESTS_HIERARCHY_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                                 HIERARCHIE D'EXIGENCES
 -------------------------------------------------------------------*/
//! Requirements hierarchy view name
const char REQUIREMENTS_HIERARCHY_SIG[] = "requirements_hierarchy";
const char REQUIREMENTS_HIERARCHY_REQUIREMENT_ID[] = "requirement_id";
const char REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID[] = "requirement_content_id";
const char REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID[] = "parent_requirement_id";
const char REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID[] = "previous_requirement_id";
const char REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID[] = "project_version_id";
const char REQUIREMENTS_HIERARCHY_PROJECT_ID[] = "project_id";
const char REQUIREMENTS_HIERARCHY_VERSION[] = "version";
const char REQUIREMENTS_HIERARCHY_SHORT_NAME[] = "short_name";
const char REQUIREMENTS_HIERARCHY_CATEGORY_ID[] = "category_id";
const char REQUIREMENTS_HIERARCHY_PRIORITY_LEVEL[] = "priority_level";
const char REQUIREMENTS_HIERARCHY_CONTENT_VERSION[] = "content_version";
const char REQUIREMENTS_HIERARCHY_STATUS[] = "status";
const char REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID[] = "original_requirement_content_id";

const char *REQUIREMENTS_HIERARCHY_COLUMNS_NAMES[] = {
    REQUIREMENTS_HIERARCHY_REQUIREMENT_ID,
    REQUIREMENTS_HIERARCHY_REQUIREMENT_CONTENT_ID,
    REQUIREMENTS_HIERARCHY_PARENT_REQUIREMENT_ID,
    REQUIREMENTS_HIERARCHY_PREVIOUS_REQUIREMENT_ID,
    REQUIREMENTS_HIERARCHY_PROJECT_VERSION_ID,
    REQUIREMENTS_HIERARCHY_PROJECT_ID,
    REQUIREMENTS_HIERARCHY_VERSION,
    REQUIREMENTS_HIERARCHY_SHORT_NAME,
    REQUIREMENTS_HIERARCHY_CATEGORY_ID,
    REQUIREMENTS_HIERARCHY_PRIORITY_LEVEL,
    REQUIREMENTS_HIERARCHY_CONTENT_VERSION,
    REQUIREMENTS_HIERARCHY_STATUS,
    REQUIREMENTS_HIERARCHY_ORIGINAL_REQUIREMENT_CONTENT_ID,
    NULL
};

const char *REQUIREMENTS_HIERARCHY_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int REQUIREMENTS_HIERARCHY_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    SHORT_TEXT_SIZE,
    1,
    1,
    12,
    1,
    PRIMARY_KEY_SIZE
};


const entity_def requirements_hierarchy_def = {
    REQUIREMENTS_HIERARCHY_SIG_ID,
    REQUIREMENTS_HIERARCHY_SIG,
    NULL,
    NULL,
    REQUIREMENTS_HIERARCHY_COLUMNS_NAMES,
    REQUIREMENTS_HIERARCHY_COLUMNS_FORMATS,
    REQUIREMENTS_HIERARCHY_COLUMNS_SIZES,
    sizeof(REQUIREMENTS_HIERARCHY_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *     ANOMALIES
 -------------------------------------------------------------------*/
//! Bugs table name
const char BUGS_TABLE_SIG[] = "bugs_table";
const char BUGS_TABLE_BUG_ID_SEQ[] = "bugs_bug_id_seq";
const char BUGS_TABLE_BUG_ID[] = "bug_id";
const char BUGS_TABLE_EXECUTION_TEST_ID[] = "execution_test_id";
const char BUGS_TABLE_EXECUTION_ACTION_ID[] = "execution_action_id";
const char BUGS_TABLE_CREATION_DATE[] = "creation_date";
const char BUGS_TABLE_SHORT_NAME[] = "short_name";
const char BUGS_TABLE_PRIORITY[] = "priority";
const char BUGS_TABLE_SEVERITY[] = "severity";
const char BUGS_TABLE_REPRODUCIBILITY[] = "reproducibility";
const char BUGS_TABLE_PLATFORM[] = "platform";
const char BUGS_TABLE_SYSTEM[] = "system";
const char BUGS_TABLE_DESCRIPTION[] = "description";
const char BUGS_TABLE_BUGTRACKER_BUG_ID[] = "bugtracker_bug_id";
const char BUGS_TABLE_STATUS[] = "status";

const char BUG_STATUS_OPENED[] = "O";
const char BUG_STATUS_CLOSED[] = "C";

const char *BUGS_COLUMNS_NAMES[] = {
    BUGS_TABLE_BUG_ID,
    BUGS_TABLE_EXECUTION_TEST_ID,
    BUGS_TABLE_EXECUTION_ACTION_ID,
    BUGS_TABLE_CREATION_DATE,
    BUGS_TABLE_SHORT_NAME,
    BUGS_TABLE_PRIORITY,
    BUGS_TABLE_SEVERITY,
    BUGS_TABLE_REPRODUCIBILITY,
    BUGS_TABLE_PLATFORM,
    BUGS_TABLE_SYSTEM,
    BUGS_TABLE_DESCRIPTION,
    BUGS_TABLE_BUGTRACKER_BUG_ID,
    BUGS_TABLE_STATUS,
    NULL
};

const char *BUGS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    DATE_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int BUGS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    DATE_SIZE,
    SMEDIUM_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    SHORT_TEXT_SIZE,
    1
};


const entity_def bugs_table_def = {
    BUGS_TABLE_SIG_ID,
    BUGS_TABLE_SIG,
    NULL,
    BUGS_TABLE_BUG_ID_SEQ,
    BUGS_COLUMNS_NAMES,
    BUGS_COLUMNS_FORMATS,
    BUGS_COLUMNS_SIZES,
    sizeof(BUGS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *         STATUS DES CONTENUS D'EXIGENCES ET DES CONTENUS DE TESTS
 -------------------------------------------------------------------*/
//! Requirements and tests status table name
const char STATUS_TABLE_SIG[] = "status_table";
const char STATUS_TABLE_STATUS_ID[] = "status_id";
const char STATUS_TABLE_STATUS_LABEL[] = "status_label";

const char *STATUS_COLUMNS_NAMES[] = {
    STATUS_TABLE_STATUS_ID,
    STATUS_TABLE_STATUS_LABEL,
    NULL
};

const char *STATUS_COLUMNS_FORMATS[] = {
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def status_table_def = {
    STATUS_TABLE_SIG_ID,
    STATUS_TABLE_SIG,
    NULL,
    NULL,
    STATUS_COLUMNS_NAMES,
    STATUS_COLUMNS_FORMATS,
    NULL,
    sizeof(STATUS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *         TYPES DE TESTS
 -------------------------------------------------------------------*/
//! Tests types table name
const char TESTS_TYPES_TABLE_SIG[] = "tests_types_table";
const char TESTS_TYPES_TABLE_TEST_TYPE_ID[] = "test_type_id";
const char TESTS_TYPES_TABLE_TEST_TYPE_LABEL[] = "test_type_label";

const char *TESTS_TYPES_COLUMNS_NAMES[] = {
    TESTS_TYPES_TABLE_TEST_TYPE_ID,
    TESTS_TYPES_TABLE_TEST_TYPE_LABEL,
    NULL
};

const char *TESTS_TYPES_COLUMNS_FORMATS[] = {
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

const entity_def tests_types_table_def = {
    TESTS_TYPES_TABLE_SIG_ID,
    TESTS_TYPES_TABLE_SIG,
    NULL,
    NULL,
    TESTS_TYPES_COLUMNS_NAMES,
    TESTS_TYPES_COLUMNS_FORMATS,
    NULL,
    sizeof(TESTS_TYPES_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *         DEFINITION DES CHAMPS PERSONNALISES
 -------------------------------------------------------------------*/
//! Custom fields table name
const char CUSTOM_FIELDS_DESC_TABLE_SIG[] = "custom_fields_desc_table";
const char CUSTOM_FIELDS_DESC_TABLE_ID_SEQ[] = "custom_fields_desc_id_seq";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ID[] = "custom_field_desc_id";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ENTITY[] = "entity_type";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TAB_NAME[] = "tab_name";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_LABEL[] = "field_label";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TYPE[] = "field_type";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_MANDATORY[] = "mandatory";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_DEFAULT_VALUE[] = "default_value";
const char CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_VALUES[] = "comma_separated_values";


const char CUSTOM_FIELDS_TEST[] = "T";
const char CUSTOM_FIELDS_REQUIERMENT[] = "R";

const char CUSTOM_FIELDS_PLAIN_TEXT[] = "P";
const char CUSTOM_FIELDS_RICH_TEXT[] = "R";
const char CUSTOM_FIELDS_CHECKBOX[] = "C";
const char CUSTOM_FIELDS_LIST[] = "L";
const char CUSTOM_FIELDS_INTEGER[] = "I";
const char CUSTOM_FIELDS_FLOAT[] = "F";
const char CUSTOM_FIELDS_TIME[] = "H";
const char CUSTOM_FIELDS_DATE[] = "J";
const char CUSTOM_FIELDS_DATETIME[] = "D";

const char *CUSTOM_FIELDS_DESC_COLUMNS_NAMES[] = {
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ID,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_ENTITY,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TAB_NAME,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_LABEL,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_TYPE,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_MANDATORY,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_DEFAULT_VALUE,
    CUSTOM_FIELDS_DESC_TABLE_CUSTOM_FIELD_DESC_VALUES,
    NULL
};

const char *CUSTOM_FIELDS_DESC_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int CUSTOM_FIELDS_DESC_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    1,
    SMEDIUM_TEXT_SIZE,
    SMEDIUM_TEXT_SIZE,
    1,
    1,
    LLARGE_TEXT_SIZE,
    LLARGE_TEXT_SIZE
};


const entity_def custom_fields_desc_table_def = {
    CUSTOM_FIELDS_DESC_TABLE_SIG_ID,
    CUSTOM_FIELDS_DESC_TABLE_SIG,
    NULL,
    CUSTOM_FIELDS_DESC_TABLE_ID_SEQ,
    CUSTOM_FIELDS_DESC_COLUMNS_NAMES,
    CUSTOM_FIELDS_DESC_COLUMNS_FORMATS,
    CUSTOM_FIELDS_DESC_COLUMNS_SIZES,
    sizeof(CUSTOM_FIELDS_DESC_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *              CHAMPS PERSONNALISES DE TEST
 -------------------------------------------------------------------*/
//! Custom test fields table name
const char  CUSTOM_TEST_FIELDS_TABLE_SIG[] = "custom_test_fields_table";
const char  CUSTOM_TEST_FIELDS_TABLE_ID_SEQ[] = "custom_test_fields_id_seq";
const char  CUSTOM_TEST_FIELDS_TABLE_CUSTOM_TEST_FIELD_ID[] = "custom_test_field_id";
const char  CUSTOM_TEST_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID[] = "custom_field_desc_id";
const char  CUSTOM_TEST_FIELDS_TABLE_TEST_CONTENT_ID[] = "test_content_id";
const char  CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE[] = "field_value";

const char *CUSTOM_TEST_FIELDS_COLUMNS_NAMES[] = {
    CUSTOM_TEST_FIELDS_TABLE_CUSTOM_TEST_FIELD_ID,
    CUSTOM_TEST_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID,
    CUSTOM_TEST_FIELDS_TABLE_TEST_CONTENT_ID,
    CUSTOM_TEST_FIELDS_TABLE_FIELD_VALUE,
    NULL
};

const char *CUSTOM_TEST_FIELDS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int CUSTOM_TEST_FIELDS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    LLARGE_TEXT_SIZE
};


const entity_def custom_test_fields_table_def = {
    CUSTOM_TEST_FIELDS_TABLE_SIG_ID,
    CUSTOM_TEST_FIELDS_TABLE_SIG,
    NULL,
    CUSTOM_TEST_FIELDS_TABLE_ID_SEQ,
    CUSTOM_TEST_FIELDS_COLUMNS_NAMES,
    CUSTOM_TEST_FIELDS_COLUMNS_FORMATS,
    CUSTOM_TEST_FIELDS_COLUMNS_SIZES,
    sizeof(CUSTOM_TEST_FIELDS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
*              CHAMPS PERSONNALISES D'EXIGENCE
-------------------------------------------------------------------*/
//! Custom requirement fields table name
const char  CUSTOM_REQUIREMENT_FIELDS_TABLE_SIG[] = "custom_requirement_fields_table";
const char  CUSTOM_REQUIREMENT_FIELDS_TABLE_ID_SEQ[] = "custom_requirement_fields_id_seq";
const char  CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_REQUIREMENT_FIELD_ID[] = "custom_requirement_field_id";
const char  CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID[] = "custom_field_desc_id";
const char  CUSTOM_REQUIREMENT_FIELDS_TABLE_REQUIREMENT_CONTENT_ID[] = "requirement_content_id";
const char  CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE[] = "field_value";

const char *CUSTOM_REQUIREMENT_FIELDS_COLUMNS_NAMES[] = {
    CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_REQUIREMENT_FIELD_ID,
    CUSTOM_REQUIREMENT_FIELDS_TABLE_CUSTOM_FIELD_DESC_ID,
    CUSTOM_REQUIREMENT_FIELDS_TABLE_REQUIREMENT_CONTENT_ID,
    CUSTOM_REQUIREMENT_FIELDS_TABLE_FIELD_VALUE,
    NULL
};

const char *CUSTOM_REQUIREMENT_FIELDS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int CUSTOM_REQUIREMENT_FIELDS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    LLARGE_TEXT_SIZE
};


const entity_def custom_requirement_fields_table_def = {
    CUSTOM_REQUIREMENT_FIELDS_TABLE_SIG_ID,
    CUSTOM_REQUIREMENT_FIELDS_TABLE_SIG,
    NULL,
    CUSTOM_REQUIREMENT_FIELDS_TABLE_ID_SEQ,
    CUSTOM_REQUIREMENT_FIELDS_COLUMNS_NAMES,
    CUSTOM_REQUIREMENT_FIELDS_COLUMNS_FORMATS,
    CUSTOM_REQUIREMENT_FIELDS_COLUMNS_SIZES,
    sizeof(CUSTOM_REQUIREMENT_FIELDS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
*              EXPRESSION DE BESOIN
-------------------------------------------------------------------*/
//! Needs table name
const char  NEEDS_TABLE_SIG[] = "needs_table";
const char  NEEDS_TABLE_ID_SEQ[] = "needs_need_id_seq";
const char  NEEDS_TABLE_NEED_ID[] = "need_id";
const char  NEEDS_TABLE_PARENT_NEED_ID[] = "parent_need_id";
const char  NEEDS_TABLE_PREVIOUS_NEED_ID[] = "previous_need_id";
const char  NEEDS_TABLE_PROJECT_ID[] = "project_id";
const char  NEEDS_TABLE_CREATION_DATE[] = "creation_date";
const char  NEEDS_TABLE_SHORT_NAME[] = "short_name";
const char  NEEDS_TABLE_DESCRIPTION[] = "description";
const char  NEEDS_TABLE_DESCRIPTION_PLAIN_TEXT[] = "description_plain_text";
const char  NEEDS_TABLE_STATUS[] = "status";

const char *NEEDS_TABLE_COLUMNS_NAMES[] = {
    NEEDS_TABLE_NEED_ID,
    NEEDS_TABLE_PARENT_NEED_ID,
    NEEDS_TABLE_PREVIOUS_NEED_ID,
    NEEDS_TABLE_PROJECT_ID,
    NEEDS_TABLE_CREATION_DATE,
    NEEDS_TABLE_SHORT_NAME,
    NEEDS_TABLE_DESCRIPTION,
    NEEDS_TABLE_DESCRIPTION_PLAIN_TEXT,
    NEEDS_TABLE_STATUS,
    NULL
};

const char *NEEDS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    DATE_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int NEEDS_TABLE_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    DATE_SIZE,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    1
};


const entity_def needs_table_def = {
    NEEDS_TABLE_SIG_ID,
    NEEDS_TABLE_SIG,
    NULL,
    NEEDS_TABLE_ID_SEQ,
    NEEDS_TABLE_COLUMNS_NAMES,
    NEEDS_TABLE_COLUMNS_FORMATS,
    NEEDS_TABLE_COLUMNS_SIZES,
    sizeof(NEEDS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
*              CONTENUS DE FONCTIONNALITES
-------------------------------------------------------------------*/
//! Contents feature table name
const char  FEATURES_CONTENTS_TABLE_SIG[] = "features_contents_table";
const char  FEATURES_CONTENTS_TABLE_ID_SEQ[] = "features_contents_feature_content_id_seq";
const char  FEATURES_CONTENTS_TABLE_FEATURE_CONTENT_ID[] = "feature_content_id";
const char  FEATURES_CONTENTS_TABLE_PROJECT_ID[] = "project_id";
const char  FEATURES_CONTENTS_TABLE_VERSION[] = "version";
const char  FEATURES_CONTENTS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char  FEATURES_CONTENTS_TABLE_NEED_ID[] = "need_id";
const char  FEATURES_CONTENTS_TABLE_SHORT_NAME[] = "short_name";
const char  FEATURES_CONTENTS_TABLE_DESCRIPTION[] = "description";
const char  FEATURES_CONTENTS_TABLE_DESCRIPTION_PLAIN_TEXT[] = "description_plain_text";
const char  FEATURES_CONTENTS_TABLE_STATUS[] = "status";

const char *FEATURES_CONTENTS_TABLE_COLUMNS_NAMES[] = {
    FEATURES_CONTENTS_TABLE_FEATURE_CONTENT_ID,
    FEATURES_CONTENTS_TABLE_PROJECT_VERSION_ID,
    FEATURES_CONTENTS_TABLE_PROJECT_ID,
    FEATURES_CONTENTS_TABLE_VERSION,
    FEATURES_CONTENTS_TABLE_NEED_ID,
    FEATURES_CONTENTS_TABLE_SHORT_NAME,
    FEATURES_CONTENTS_TABLE_DESCRIPTION,
    FEATURES_CONTENTS_TABLE_DESCRIPTION_PLAIN_TEXT,
    FEATURES_CONTENTS_TABLE_STATUS,
    NULL
};

const char *FEATURES_CONTENTS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int FEATURES_CONTENTS_TABLE_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    1
};


const entity_def features_contents_table_def = {
    FEATURES_CONTENTS_TABLE_SIG_ID,
    FEATURES_CONTENTS_TABLE_SIG,
    NULL,
    FEATURES_CONTENTS_TABLE_ID_SEQ,
    FEATURES_CONTENTS_TABLE_COLUMNS_NAMES,
    FEATURES_CONTENTS_TABLE_COLUMNS_FORMATS,
    FEATURES_CONTENTS_TABLE_COLUMNS_SIZES,
    sizeof(FEATURES_CONTENTS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
*              FONCTIONNALITES
-------------------------------------------------------------------*/
//! Features table name
const char  FEATURES_TABLE_SIG[] = "features_table";
const char  FEATURES_TABLE_ID_SEQ[] = "features_feature_id_seq";
const char  FEATURES_TABLE_FEATURE_ID[] = "feature_id";
const char  FEATURES_TABLE_FEATURE_CONTENT_ID[] = "feature_content_id";
const char  FEATURES_TABLE_PARENT_FEATURE_ID[] = "parent_feature_id";
const char  FEATURES_TABLE_PREVIOUS_FEATURE_ID[] = "previous_feature_id";
const char  FEATURES_TABLE_PROJECT_ID[] = "project_id";
const char  FEATURES_TABLE_VERSION[] = "version";
const char  FEATURES_TABLE_PROJECT_VERSION_ID[] = "project_version_id";

const char *FEATURES_TABLE_COLUMNS_NAMES[] = {
    FEATURES_TABLE_FEATURE_ID,
    FEATURES_TABLE_FEATURE_CONTENT_ID,
    FEATURES_TABLE_PARENT_FEATURE_ID,
    FEATURES_TABLE_PREVIOUS_FEATURE_ID,
    FEATURES_TABLE_PROJECT_VERSION_ID,
    FEATURES_TABLE_PROJECT_ID,
    FEATURES_TABLE_VERSION,
    NULL
};

const char *FEATURES_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int FEATURES_TABLE_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12
};


const entity_def features_table_def = {
    FEATURES_TABLE_SIG_ID,
    FEATURES_TABLE_SIG,
    NULL,
    FEATURES_TABLE_ID_SEQ,
    FEATURES_TABLE_COLUMNS_NAMES,
    FEATURES_TABLE_COLUMNS_FORMATS,
    FEATURES_TABLE_COLUMNS_SIZES,
    sizeof(FEATURES_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *             HIERARCHIE DE FONCTIONNALITES
 -------------------------------------------------------------------*/
//! Features hierarchy view name
const char FEATURES_HIERARCHY_SIG[] = "features_hierarchy";
const char FEATURES_HIERARCHY_FEATURE_ID[] = "feature_id";
const char FEATURES_HIERARCHY_FEATURE_CONTENT_ID[] = "feature_content_id";
const char FEATURES_HIERARCHY_PARENT_FEATURE_ID[] = "parent_feature_id";
const char FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID[] = "previous_feature_id";
const char FEATURES_HIERARCHY_PROJECT_VERSION_ID[] = "project_version_id";
const char FEATURES_HIERARCHY_PROJECT_ID[] = "project_id";
const char FEATURES_HIERARCHY_VERSION[] = "version";
const char FEATURES_HIERARCHY_SHORT_NAME[] = "short_name";
const char FEATURES_HIERARCHY_CONTENT_VERSION[] = "content_version";
const char FEATURES_HIERARCHY_STATUS[] = "status";

const char *FEATURES_HIERARCHY_COLUMNS_NAMES[] = {
    FEATURES_HIERARCHY_FEATURE_ID,
    FEATURES_HIERARCHY_FEATURE_CONTENT_ID,
    FEATURES_HIERARCHY_PARENT_FEATURE_ID,
    FEATURES_HIERARCHY_PREVIOUS_FEATURE_ID,
    FEATURES_HIERARCHY_PROJECT_VERSION_ID,
    FEATURES_HIERARCHY_PROJECT_ID,
    FEATURES_HIERARCHY_VERSION,
    FEATURES_HIERARCHY_SHORT_NAME,
    FEATURES_HIERARCHY_CONTENT_VERSION,
    FEATURES_HIERARCHY_STATUS,
    NULL
};

const char *FEATURES_HIERARCHY_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int FEATURES_HIERARCHY_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    SHORT_TEXT_SIZE,
    12,
    1
};


const entity_def features_hierarchy_def = {
    FEATURES_HIERARCHY_SIG_ID,
    FEATURES_HIERARCHY_SIG,
    NULL,
    NULL,
    FEATURES_HIERARCHY_COLUMNS_NAMES,
    FEATURES_HIERARCHY_COLUMNS_FORMATS,
    FEATURES_HIERARCHY_COLUMNS_SIZES,
    sizeof(FEATURES_HIERARCHY_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
*              CONTENUS DES REGLES DE GESTION
-------------------------------------------------------------------*/
//! Contents rule table name
const char  RULES_CONTENTS_TABLE_SIG[] = "rules_contents_table";
const char  RULES_CONTENTS_TABLE_ID_SEQ[] = "rules_contents_rule_content_id_seq";
const char  RULES_CONTENTS_TABLE_RULE_CONTENT_ID[] = "rule_content_id";
const char  RULES_CONTENTS_TABLE_PROJECT_ID[] = "project_id";
const char  RULES_CONTENTS_TABLE_VERSION[] = "version";
const char  RULES_CONTENTS_TABLE_PROJECT_VERSION_ID[] = "project_version_id";
const char  RULES_CONTENTS_TABLE_FEATURE_CONTENT_ID[] = "feature_content_id";
const char  RULES_CONTENTS_TABLE_SHORT_NAME[] = "short_name";
const char  RULES_CONTENTS_TABLE_DESCRIPTION[] = "description";
const char  RULES_CONTENTS_TABLE_DESCRIPTION_PLAIN_TEXT[] = "description_plain_text";
const char  RULES_CONTENTS_TABLE_STATUS[] = "status";

const char *RULES_CONTENTS_TABLE_COLUMNS_NAMES[] = {
    RULES_CONTENTS_TABLE_RULE_CONTENT_ID,
    RULES_CONTENTS_TABLE_PROJECT_VERSION_ID,
    RULES_CONTENTS_TABLE_PROJECT_ID,
    RULES_CONTENTS_TABLE_VERSION,
    RULES_CONTENTS_TABLE_FEATURE_CONTENT_ID,
    RULES_CONTENTS_TABLE_SHORT_NAME,
    RULES_CONTENTS_TABLE_DESCRIPTION,
    RULES_CONTENTS_TABLE_DESCRIPTION_PLAIN_TEXT,
    RULES_CONTENTS_TABLE_STATUS,
    NULL
};

const char *RULES_CONTENTS_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int RULES_CONTENTS_TABLE_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    LLARGE_TEXT_SIZE,
    1
};


const entity_def rules_contents_table_def = {
    RULES_CONTENTS_TABLE_SIG_ID,
    RULES_CONTENTS_TABLE_SIG,
    NULL,
    RULES_CONTENTS_TABLE_ID_SEQ,
    RULES_CONTENTS_TABLE_COLUMNS_NAMES,
    RULES_CONTENTS_TABLE_COLUMNS_FORMATS,
    RULES_CONTENTS_TABLE_COLUMNS_SIZES,
    sizeof(RULES_CONTENTS_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
*              REGLES DE GESTION
-------------------------------------------------------------------*/
//! Rules table name
const char  RULES_TABLE_SIG[] = "rules_table";
const char  RULES_TABLE_ID_SEQ[] = "rules_rule_id_seq";
const char  RULES_TABLE_RULE_ID[] = "rule_id";
const char  RULES_TABLE_RULE_CONTENT_ID[] = "rule_content_id";
const char  RULES_TABLE_PARENT_RULE_ID[] = "parent_rule_id";
const char  RULES_TABLE_PREVIOUS_RULE_ID[] = "previous_rule_id";
const char  RULES_TABLE_PROJECT_ID[] = "project_id";
const char  RULES_TABLE_VERSION[] = "version";
const char  RULES_TABLE_PROJECT_VERSION_ID[] = "project_version_id";

const char *RULES_TABLE_COLUMNS_NAMES[] = {
    RULES_TABLE_RULE_ID,
    RULES_TABLE_RULE_CONTENT_ID,
    RULES_TABLE_PARENT_RULE_ID,
    RULES_TABLE_PREVIOUS_RULE_ID,
    RULES_TABLE_PROJECT_VERSION_ID,
    RULES_TABLE_PROJECT_ID,
    RULES_TABLE_VERSION,
    NULL
};

const char *RULES_TABLE_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int RULES_TABLE_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12
};


const entity_def rules_table_def = {
    RULES_TABLE_SIG_ID,
    RULES_TABLE_SIG,
    NULL,
    RULES_TABLE_ID_SEQ,
    RULES_TABLE_COLUMNS_NAMES,
    RULES_TABLE_COLUMNS_FORMATS,
    RULES_TABLE_COLUMNS_SIZES,
    sizeof(RULES_TABLE_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *             HIERARCHIE DE REGLES DE GESTION
 -------------------------------------------------------------------*/
//! Rules hierarchy view name
const char RULES_HIERARCHY_SIG[] = "rules_hierarchy";
const char RULES_HIERARCHY_RULE_ID[] = "rule_id";
const char RULES_HIERARCHY_RULE_CONTENT_ID[] = "rule_content_id";
const char RULES_HIERARCHY_PARENT_RULE_ID[] = "parent_rule_id";
const char RULES_HIERARCHY_PREVIOUS_RULE_ID[] = "previous_rule_id";
const char RULES_HIERARCHY_PROJECT_VERSION_ID[] = "project_version_id";
const char RULES_HIERARCHY_PROJECT_ID[] = "project_id";
const char RULES_HIERARCHY_VERSION[] = "version";
const char RULES_HIERARCHY_SHORT_NAME[] = "short_name";
const char RULES_HIERARCHY_CONTENT_VERSION[] = "content_version";
const char RULES_HIERARCHY_STATUS[] = "status";

const char *RULES_HIERARCHY_COLUMNS_NAMES[] = {
    RULES_HIERARCHY_RULE_ID,
    RULES_HIERARCHY_RULE_CONTENT_ID,
    RULES_HIERARCHY_PARENT_RULE_ID,
    RULES_HIERARCHY_PREVIOUS_RULE_ID,
    RULES_HIERARCHY_PROJECT_VERSION_ID,
    RULES_HIERARCHY_PROJECT_ID,
    RULES_HIERARCHY_VERSION,
    RULES_HIERARCHY_SHORT_NAME,
    RULES_HIERARCHY_CONTENT_VERSION,
    RULES_HIERARCHY_STATUS,
    NULL
};

const char *RULES_HIERARCHY_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int RULES_HIERARCHY_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    12,
    SHORT_TEXT_SIZE,
    12,
    1
};


const entity_def rules_hierarchy_def = {
    RULES_HIERARCHY_SIG_ID,
    RULES_HIERARCHY_SIG,
    NULL,
    NULL,
    RULES_HIERARCHY_COLUMNS_NAMES,
    RULES_HIERARCHY_COLUMNS_FORMATS,
    RULES_HIERARCHY_COLUMNS_SIZES,
    sizeof(RULES_HIERARCHY_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                         PLANS DE TESTS
 -------------------------------------------------------------------*/
//! Tests plan table name
const char TESTS_PLANS_TABLE_SIG[] = "tests_plans_table";
const char TESTS_PLANS_TABLE_TEST_PLAN_ID_SEQ[] = "tests_plans_id_seq";
const char TESTS_PLANS_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char TESTS_PLANS_TABLE_CAMPAIGN_ID[] = "campaign_id";

const char *TESTS_PLANS_COLUMNS_NAMES[] = {
    TESTS_PLANS_TABLE_TEST_PLAN_ID,
    TESTS_PLANS_TABLE_CAMPAIGN_ID,
    NULL
};

const char *TESTS_PLANS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int TESTS_PLANS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE
};


const entity_def tests_plans_table_def = {
    TESTS_PLANS_TABLE_SIG_ID,
    TESTS_PLANS_TABLE_SIG,
    NULL,
    TESTS_PLANS_TABLE_TEST_PLAN_ID_SEQ,
    TESTS_PLANS_COLUMNS_NAMES,
    TESTS_PLANS_COLUMNS_FORMATS,
    TESTS_PLANS_COLUMNS_SIZES,
    sizeof(TESTS_PLANS_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                         OBJETS GRAPHIQUES DE PLANS DE TESTS
 -------------------------------------------------------------------*/
//! Graphics items table name
const char GRAPHICS_ITEMS_TABLE_SIG[] = "graphics_items_table";
const char GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID_SEQ[] = "graphics_items_id_seq";
const char GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_ITEMS_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID[] = "parent_graphic_item_id";
const char GRAPHICS_ITEMS_TABLE_TYPE[] = "type";
const char GRAPHICS_ITEMS_TABLE_X[] = "x";
const char GRAPHICS_ITEMS_TABLE_Y[] = "y";
const char GRAPHICS_ITEMS_TABLE_Z_VALUE[] = "z_value";
const char GRAPHICS_ITEMS_TABLE_COMMENTS[] = "comments";
const char GRAPHICS_ITEMS_TABLE_DATA[] = "data";

const char *GRAPHICS_ITEMS_COLUMNS_NAMES[] = {
    GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_ITEMS_TABLE_TEST_PLAN_ID,
    GRAPHICS_ITEMS_TABLE_PARENT_GRAPHIC_ITEM_ID,
    GRAPHICS_ITEMS_TABLE_TYPE,
    GRAPHICS_ITEMS_TABLE_X,
    GRAPHICS_ITEMS_TABLE_Y,
    GRAPHICS_ITEMS_TABLE_Z_VALUE,
    GRAPHICS_ITEMS_TABLE_COMMENTS,
    GRAPHICS_ITEMS_TABLE_DATA,
    NULL
};

const char *GRAPHICS_ITEMS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_ITEMS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    1,
    NUMBER_SIZE,
    NUMBER_SIZE,
    NUMBER_SIZE,
    SMEDIUM_TEXT_SIZE,
    S1LARGE_TEXT_SIZE
};


const entity_def graphics_items_table_def = {
    GRAPHICS_ITEMS_TABLE_SIG_ID,
    GRAPHICS_ITEMS_TABLE_SIG,
    NULL,
    GRAPHICS_ITEMS_TABLE_GRAPHIC_ITEM_ID_SEQ,
    GRAPHICS_ITEMS_COLUMNS_NAMES,
    GRAPHICS_ITEMS_COLUMNS_FORMATS,
    GRAPHICS_ITEMS_COLUMNS_SIZES,
    sizeof(GRAPHICS_ITEMS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                         POINTS GRAPHIQUES DE PLANS DE TESTS
 -------------------------------------------------------------------*/
//! Graphics items table name
const char GRAPHICS_POINTS_TABLE_SIG[] = "graphics_points_table";
const char GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_POINTS_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char GRAPHICS_POINTS_TABLE_LINK_GRAPHIC_ITEM_ID[] = "link_graphic_item_id";
const char GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID[] = "connected_graphic_item_id";
const char GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID[] = "previous_graphic_point_item_id";
const char GRAPHICS_POINTS_TABLE_TYPE[] = "type";
const char GRAPHICS_POINTS_TABLE_DATA[] = "data";

const char *GRAPHICS_POINTS_COLUMNS_NAMES[] = {
    GRAPHICS_POINTS_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_POINTS_TABLE_TEST_PLAN_ID,
    GRAPHICS_POINTS_TABLE_LINK_GRAPHIC_ITEM_ID,
    GRAPHICS_POINTS_TABLE_CONNECTED_GRAPHIC_ITEM_ID,
    GRAPHICS_POINTS_TABLE_PREVIOUS_GRAPHIC_POINT_ITEM_ID,
    GRAPHICS_POINTS_TABLE_TYPE,
    GRAPHICS_POINTS_TABLE_DATA,
    NULL
};

const char *GRAPHICS_POINTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_POINTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    1,
    LMEDIUM_TEXT_SIZE
};


const entity_def graphics_points_table_def = {
    GRAPHICS_POINTS_TABLE_SIG_ID,
    GRAPHICS_POINTS_TABLE_SIG,
    NULL,
    NULL,
    GRAPHICS_POINTS_COLUMNS_NAMES,
    GRAPHICS_POINTS_COLUMNS_FORMATS,
    GRAPHICS_POINTS_COLUMNS_SIZES,
    sizeof(GRAPHICS_POINTS_COLUMNS_NAMES)/sizeof(char*) - 1
};

/*-------------------------------------------------------------------
 *                         TESTS GRAPHIQUES
 -------------------------------------------------------------------*/
//! Graphics tests table name
const char GRAPHICS_TESTS_TABLE_SIG[] = "graphics_tests_table";
const char GRAPHICS_TESTS_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_TESTS_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char GRAPHICS_TESTS_TABLE_TEST_ID[] = "test_id";
const char GRAPHICS_TESTS_TABLE_RETURN_CODE_VAR_NAME[] = "return_code_var_name";
const char GRAPHICS_TESTS_TABLE_OUTPUT_VAR_NAME[] = "output_var_name";

const char *GRAPHICS_TESTS_COLUMNS_NAMES[] = {
    GRAPHICS_TESTS_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_TESTS_TABLE_TEST_PLAN_ID,
    GRAPHICS_TESTS_TABLE_TEST_ID,
    GRAPHICS_TESTS_TABLE_RETURN_CODE_VAR_NAME,
    GRAPHICS_TESTS_TABLE_OUTPUT_VAR_NAME,
    NULL
};

const char *GRAPHICS_TESTS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_TESTS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE,
    SMEDIUM_TEXT_SIZE
};


const entity_def graphics_tests_table_def = {
    GRAPHICS_TESTS_TABLE_SIG_ID,
    GRAPHICS_TESTS_TABLE_SIG,
    NULL,
    NULL,
    GRAPHICS_TESTS_COLUMNS_NAMES,
    GRAPHICS_TESTS_COLUMNS_FORMATS,
    GRAPHICS_TESTS_COLUMNS_SIZES,
    sizeof(GRAPHICS_TESTS_COLUMNS_NAMES)/sizeof(char*) - 1
};



/*-------------------------------------------------------------------
 *                         IFS GRAPHIQUES
 -------------------------------------------------------------------*/
//! Graphics ifs table name
const char GRAPHICS_IFS_TABLE_SIG[] = "graphics_ifs_table";
const char GRAPHICS_IFS_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_IFS_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char GRAPHICS_IFS_TABLE_CONDITION[] = "condition";

const char *GRAPHICS_IFS_COLUMNS_NAMES[] = {
    GRAPHICS_IFS_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_IFS_TABLE_TEST_PLAN_ID,
    GRAPHICS_IFS_TABLE_CONDITION,
    NULL
};

const char *GRAPHICS_IFS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_IFS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE
};


const entity_def graphics_ifs_table_def = {
    GRAPHICS_IFS_TABLE_SIG_ID,
    GRAPHICS_IFS_TABLE_SIG,
    NULL,
    NULL,
    GRAPHICS_IFS_COLUMNS_NAMES,
    GRAPHICS_IFS_COLUMNS_FORMATS,
    GRAPHICS_IFS_COLUMNS_SIZES,
    sizeof(GRAPHICS_IFS_COLUMNS_NAMES)/sizeof(char*) - 1
};



/*-------------------------------------------------------------------
 *                         SWITCHS GRAPHIQUES
 -------------------------------------------------------------------*/
//! Graphics switches table name
const char GRAPHICS_SWITCHES_TABLE_SIG[] = "graphics_switches_table";
const char GRAPHICS_SWITCHES_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_SWITCHES_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char GRAPHICS_SWITCHES_TABLE_CONDITION[] = "condition";

const char *GRAPHICS_SWITCHES_COLUMNS_NAMES[] = {
    GRAPHICS_SWITCHES_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_SWITCHES_TABLE_TEST_PLAN_ID,
    GRAPHICS_SWITCHES_TABLE_CONDITION,
    NULL
};

const char *GRAPHICS_SWITCHES_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    STRING_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_SWITCHES_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
    SMEDIUM_TEXT_SIZE
};


const entity_def graphics_switches_table_def = {
    GRAPHICS_SWITCHES_TABLE_SIG_ID,
    GRAPHICS_SWITCHES_TABLE_SIG,
    NULL,
    NULL,
    GRAPHICS_SWITCHES_COLUMNS_NAMES,
    GRAPHICS_SWITCHES_COLUMNS_FORMATS,
    GRAPHICS_SWITCHES_COLUMNS_SIZES,
    sizeof(GRAPHICS_SWITCHES_COLUMNS_NAMES)/sizeof(char*) - 1
};


/*-------------------------------------------------------------------
 *                         BOUCLES GRAPHIQUES
 -------------------------------------------------------------------*/
//! Graphics loops table name
const char GRAPHICS_LOOPS_TABLE_SIG[] = "graphics_loops_table";
const char GRAPHICS_LOOPS_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_LOOPS_TABLE_TEST_PLAN_ID[] = "test_plan_id";
const char GRAPHICS_LOOPS_TABLE_CONDITION[] = "condition";

const char *GRAPHICS_LOOPS_COLUMNS_NAMES[] = {
    GRAPHICS_LOOPS_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_LOOPS_TABLE_TEST_PLAN_ID,
    NULL
};

const char *GRAPHICS_LOOPS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_LOOPS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE
};


const entity_def graphics_loops_table_def = {
    GRAPHICS_LOOPS_TABLE_SIG_ID,
    GRAPHICS_LOOPS_TABLE_SIG,
    NULL,
    NULL,
    GRAPHICS_LOOPS_COLUMNS_NAMES,
    GRAPHICS_LOOPS_COLUMNS_FORMATS,
    GRAPHICS_LOOPS_COLUMNS_SIZES,
    sizeof(GRAPHICS_LOOPS_COLUMNS_NAMES)/sizeof(char*) - 1
};



/*-------------------------------------------------------------------
 *                         LIENS GRAPHIQUES
 -------------------------------------------------------------------*/
//! Graphics links table name
const char GRAPHICS_LINKS_TABLE_SIG[] = "graphics_links_table";
const char GRAPHICS_LINKS_TABLE_GRAPHIC_ITEM_ID[] = "graphic_item_id";
const char GRAPHICS_LINKS_TABLE_TEST_PLAN_ID[] = "test_plan_id";

const char *GRAPHICS_LINKS_COLUMNS_NAMES[] = {
    GRAPHICS_LINKS_TABLE_GRAPHIC_ITEM_ID,
    GRAPHICS_LINKS_TABLE_TEST_PLAN_ID,
    NULL
};

const char *GRAPHICS_LINKS_COLUMNS_FORMATS[] = {
    NUMBER_FORMAT,
    NUMBER_FORMAT,
    NULL
};

static const unsigned int GRAPHICS_LINKS_COLUMNS_SIZES[] = {
    PRIMARY_KEY_SIZE,
    PRIMARY_KEY_SIZE,
};


const entity_def graphics_links_table_def = {
    GRAPHICS_LINKS_TABLE_SIG_ID,
    GRAPHICS_LINKS_TABLE_SIG,
    NULL,
    NULL,
    GRAPHICS_LINKS_COLUMNS_NAMES,
    GRAPHICS_LINKS_COLUMNS_FORMATS,
    GRAPHICS_LINKS_COLUMNS_SIZES,
    sizeof(GRAPHICS_LINKS_COLUMNS_NAMES)/sizeof(char*) - 1
};
