#ifndef SINGLETON_H_
#define SINGLETON_H_

template <typename T>
class Singleton
{
public:

    static T& instance() {
        static T theInstance;
        return theInstance;
    }

protected:
    Singleton() {}
    Singleton(const Singleton &other) {}
    Singleton(T &&/*other*/) {}
    virtual ~Singleton() {}
    T& operator=(const T &/*other*/) { return *this; }
};

#endif
