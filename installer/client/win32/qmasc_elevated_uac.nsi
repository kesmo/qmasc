;Based on Start Menu Folder Selection Example Script from NSIS Modern User Interface Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

  !include LogicLib.nsh
 
!ifndef IPersistFile
!define IPersistFile {0000010b-0000-0000-c000-000000000046}
!endif
!ifndef CLSID_ShellLink
!define CLSID_ShellLink {00021401-0000-0000-C000-000000000046}
!define IID_IShellLinkA {000214EE-0000-0000-C000-000000000046}
!define IID_IShellLinkW {000214F9-0000-0000-C000-000000000046}
!define IShellLinkDataList {45e2b4ae-b1c3-11d0-b92f-00a0c90312e1}
	!ifdef NSIS_UNICODE
	!define IID_IShellLink ${IID_IShellLinkW}
	!else
	!define IID_IShellLink ${IID_IShellLinkA}
	!endif
!endif
 
 
 
Function ShellLinkSetRunAs
System::Store S
pop $9
System::Call "ole32::CoCreateInstance(g'${CLSID_ShellLink}',i0,i1,g'${IID_IShellLink}',*i.r1)i.r0"
${If} $0 = 0
	System::Call "$1->0(g'${IPersistFile}',*i.r2)i.r0" ;QI
	${If} $0 = 0
		System::Call "$2->5(w '$9',i 0)i.r0" ;Load
		${If} $0 = 0
			System::Call "$1->0(g'${IShellLinkDataList}',*i.r3)i.r0" ;QI
			${If} $0 = 0
				System::Call "$3->6(*i.r4)i.r0" ;GetFlags
				${If} $0 = 0
					System::Call "$3->7(i $4|0x2000)i.r0" ;SetFlags ;SLDF_RUNAS_USER
					${If} $0 = 0
						System::Call "$2->6(w '$9',i1)i.r0" ;Save
					${EndIf}
				${EndIf}
				System::Call "$3->2()" ;Release
			${EndIf}
		System::Call "$2->2()" ;Release
		${EndIf}
	${EndIf}
	System::Call "$1->2()" ;Release
${EndIf}
push $0
System::Store L
FunctionEnd

;--------------------------------
;General
  !define VERSION 1.11.0

  ;Name and file
  Name "R.T.M.R"
  OutFile "..\..\..\www\downloads\Windows\rtmr_${VERSION}-win32-installer-elevated_uac.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\R.T.M.R"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\R.T.M.R" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

  !define MUI_ICON "..\..\..\client-app\images\client.ico"
  !define MUI_UNICON "..\..\..\client-app\images\client.ico"
;--------------------------------
;Variables

  Var rtmrFolder

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "..\..\..\client-app\LICENCE.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\R.T.M.R" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "R.T.M.R"
  
  !insertmacro MUI_PAGE_STARTMENU Application $rtmrFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

  !define MUI_FINISHPAGE_RUN "$INSTDIR\rtmr.exe"
  !define MUI_FINISHPAGE_RUN_TEXT "Launch R.T.M.R Client Application"
  !insertmacro MUI_PAGE_FINISH
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "!R.T.M.R client" MainSection

  SetOutPath "$INSTDIR"
  SectionIn RO
  
  ;ADD YOUR OWN FILES HERE...
  File "..\..\..\client-launcher\Release\rtmr.exe"
  File "..\..\..\client-app\Release\rtmrapp.exe"
  File "..\..\..\client-lib\Release\rtmr.dll"
  File "..\..\..\automation-lib\Release\automation-lib.dll"
  File "QtCore4.dll"
  File "QtGui4.dll"
  File "QtNetwork4.dll"
  File "QtXml4.dll"
  File "ssleay32.dll"
  File "libeay32.dll"
  File "7za.exe"

  ;Store installation folder
  WriteRegStr HKCU "Software\R.T.M.R" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$rtmrFolder"
    CreateShortCut "$SMPROGRAMS\$rtmrFolder\R.T.M.R Client.lnk" "$INSTDIR\rtmr.exe"
	push "$SMPROGRAMS\$rtmrFolder\R.T.M.R Client.lnk"
	call ShellLinkSetRunAs

    CreateShortCut "$SMPROGRAMS\$rtmrFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

SectionGroup "Modules" ModulesSection
SectionGroup "Bugtrackers" BugtrackersSection
Section "Bugzilla" BugzillaSection
  SetOutPath "$INSTDIR\modules"
  File "..\..\..\client-modules\bugzillabt\release\bugzillabt.cmo"
SectionEnd
Section "Mantis" MantisSection
  SetOutPath "$INSTDIR\modules"
  File "..\..\..\client-modules\mantisbt\release\mantisbt.cmo"
SectionEnd
Section "Redmine" RedmineSection
  SetOutPath "$INSTDIR\modules"
  File "..\..\..\client-modules\redminebt\release\redminebt.cmo"
SectionEnd
Section "Jira" JiraSection
  SetOutPath "$INSTDIR\modules"
  File "..\..\..\client-modules\jirabt\release\jirabt.cmo"
SectionEnd
SectionGroupEnd
SectionGroup "Automation" AutomationSection
Section "Standard" StandardAutomationSection
  SetOutPath "$INSTDIR\modules"
  File "..\..\..\client-modules\std-automation\release\std-automation.cmo"
SectionEnd
SectionGroupEnd
SectionGroupEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_MainSection ${LANG_ENGLISH} "Client rtmr"
  LangString DESC_ModulesSection ${LANG_ENGLISH} "External modules"
  LangString DESC_BugtrackersSection ${LANG_ENGLISH} "External bugtrackers"
  LangString DESC_BugzillaSection ${LANG_ENGLISH} "Bugzilla bugtracker plugin"
  LangString DESC_MantisSection ${LANG_ENGLISH} "Mantis bugtracker plugin"
  LangString DESC_RedmineSection ${LANG_ENGLISH} "Redmine bugtracker plugin"
  LangString DESC_JiraSection ${LANG_ENGLISH} "Jira bugtracker plugin"
  LangString DESC_AutomationSection ${LANG_ENGLISH} "External automation modules"
  LangString DESC_StandardAutomationSection ${LANG_ENGLISH} "Standard automation plugin"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${MainSection} $(DESC_MainSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${ModulesSection} $(DESC_ModulesSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${BugtrackersSection} $(DESC_BugtrackersSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${BugzillaSection} $(DESC_BugzillaSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${MantisSection} $(DESC_MantisSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${RedmineSection} $(DESC_RedmineSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${JiraSection} $(DESC_JiraSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${AutomationSection} $(DESC_AutomationSection)
    !insertmacro MUI_DESCRIPTION_TEXT ${StandardAutomationSection} $(DESC_StandardAutomationSection)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r "$INSTDIR"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $rtmrFolder
    
  Delete "$SMPROGRAMS\$rtmrFolder\R.T.M.R Client.lnk"
  Delete "$SMPROGRAMS\$rtmrFolder\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$rtmrFolder"
  
  DeleteRegKey /ifempty HKCU "Software\R.T.M.R"

SectionEnd