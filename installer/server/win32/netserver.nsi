;Based on Start Menu Folder Selection Example Script from NSIS Modern User Interface Written by Joost Verburg

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin
;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"
  !include "INIStrNS.nsh"

;--------------------------------
;General
  !define VERSION 1.8.0

  ;Name and file
  Name "R.T.M.R Server"
  OutFile "..\..\..\www\downloads\Windows\netserver_${VERSION}-win32-installer.exe"
  
  ;Default installation folder
  InstallDir "$PROGRAMFILES\neterver"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKLM "Software\R.T.M.R Server" ""

  !define MUI_ICON "..\..\..\client-app\images\client.ico"
  !define MUI_UNICON "..\..\..\client-app\images\client.ico"
  
;--------------------------------
;Variables

  Var netserverFolder

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "..\..\..\netserver\LICENCE.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\R.T.M.R Server" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "R.T.M.R Server"
  
  !insertmacro MUI_PAGE_STARTMENU Application $netserverFolder
  
  !insertmacro MUI_PAGE_INSTFILES
  
  ; Trim
;   Removes leading & trailing whitespace from a string
; Usage:
;   Push 
;   Call Trim
;   Pop 
Function Trim
	Exch $R1 ; Original string
	Push $R2
 
Loop:
	StrCpy $R2 "$R1" 1
	StrCmp "$R2" " " TrimLeft
	StrCmp "$R2" "$\r" TrimLeft
	StrCmp "$R2" "$\n" TrimLeft
	StrCmp "$R2" "$\t" TrimLeft
	GoTo Loop2
TrimLeft:	
	StrCpy $R1 "$R1" "" 1
	Goto Loop
 
Loop2:
	StrCpy $R2 "$R1" 1 -1
	StrCmp "$R2" " " TrimRight
	StrCmp "$R2" "$\r" TrimRight
	StrCmp "$R2" "$\n" TrimRight
	StrCmp "$R2" "$\t" TrimRight
	GoTo Done
TrimRight:	
	StrCpy $R1 "$R1" -1
	Goto Loop2
 
Done:
	Pop $R2
	Exch $R1
FunctionEnd

; Usage:
; ${Trim} $trimmedString $originalString
 
!define Trim "!insertmacro Trim"
 
!macro Trim ResultVar String
  Push "${String}"
  Call Trim
  Pop "${ResultVar}"
!macroend


!include LogicLib.nsh
 
!ifndef IPersistFile
!define IPersistFile {0000010b-0000-0000-c000-000000000046}
!endif
!ifndef CLSID_ShellLink
!define CLSID_ShellLink {00021401-0000-0000-C000-000000000046}
!define IID_IShellLinkA {000214EE-0000-0000-C000-000000000046}
!define IID_IShellLinkW {000214F9-0000-0000-C000-000000000046}
!define IShellLinkDataList {45e2b4ae-b1c3-11d0-b92f-00a0c90312e1}
	!ifdef NSIS_UNICODE
	!define IID_IShellLink ${IID_IShellLinkW}
	!else
	!define IID_IShellLink ${IID_IShellLinkA}
	!endif
!endif
 
 
 
Function ShellLinkSetRunAs
System::Store S
pop $9
System::Call "ole32::CoCreateInstance(g'${CLSID_ShellLink}',i0,i1,g'${IID_IShellLink}',*i.r1)i.r0"
${If} $0 = 0
	System::Call "$1->0(g'${IPersistFile}',*i.r2)i.r0" ;QI
	${If} $0 = 0
		System::Call "$2->5(w '$9',i 0)i.r0" ;Load
		${If} $0 = 0
			System::Call "$1->0(g'${IShellLinkDataList}',*i.r3)i.r0" ;QI
			${If} $0 = 0
				System::Call "$3->6(*i.r4)i.r0" ;GetFlags
				${If} $0 = 0
					System::Call "$3->7(i $4|0x2000)i.r0" ;SetFlags ;SLDF_RUNAS_USER
					${If} $0 = 0
						System::Call "$2->6(w '$9',i1)i.r0" ;Save
					${EndIf}
				${EndIf}
				System::Call "$3->2()" ;Release
			${EndIf}
		System::Call "$2->2()" ;Release
		${EndIf}
	${EndIf}
	System::Call "$1->2()" ;Release
${EndIf}
push $0
System::Store L
FunctionEnd


Var DIALOG
Var HEADLINE
Var PSQL_EXEC_LABEL
Var PSQL_EXEC
Var PSQL_EXEC_PATH
Var TEXT

Var DB_HOST_LABEL
Var DB_PORT_LABEL
Var DB_SCHEMA_LABEL
Var LOGIN_LABEL
Var PASSWORD_LABEL
Var DB_HOST
Var DB_HOST_VALUE
Var DB_PORT
Var DB_PORT_VALUE
Var DB_SCHEMA
Var DB_SCHEMA_VALUE
Var LOGIN
Var LOGIN_VALUE
Var PASSWORD
Var PASSWORD_VALUE
Var UPGRADE_BUTTON

Var RTMR_DATABASE_VERSION_VALUE
Var RTMR_DATABASE_COUNT

Var RTMR_DATABASE_UPGRADE_SCRIPT

Var POSTGRES_ERROR_MESSAGE
Var POSTGRES_LOG_FILE

Function InstallDatabasePage

	; Mise � jour de la base de donn�es  
	StrCpy $0 0
	loop:

		EnumRegKey $1 HKLM "SOFTWARE\PostgreSQL\Installations" $0
		StrCmp $1 "" done
		IntOp $0 $0 + 1

		ReadRegStr $2 HKLM "SOFTWARE\PostgreSQL\Installations\$1" "Base Directory" 
		StrCmp $2 "" loop

		StrCpy $PSQL_EXEC_PATH $2\bin\psql.exe
		IfFileExists "$PSQL_EXEC_PATH" postgresinsalled loop

	done:
	
	StrCpy $0 0
	loop:

		EnumRegKey $1 HKLM "SOFTWARE\Wow6432Node\PostgreSQL\Installations" $0
		StrCmp $1 "" done64
		IntOp $0 $0 + 1

		ReadRegStr $2 HKLM "SOFTWARE\Wow6432Node\PostgreSQL\Installations\$1" "Base Directory" 
		StrCmp $2 "" loop

		StrCpy $PSQL_EXEC_PATH $2\bin\psql.exe
		IfFileExists "$PSQL_EXEC_PATH" postgresinsalled loop

	done64:
	
	; postgresql n'est pas install�
	!insertmacro MUI_HEADER_TEXT "PostgreSQL service" "The PostgreSQL service doesn't seem to be installed."

	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 1

	nsDialogs::Create 1018
	Pop $DIALOG
	
	${NSD_CreateLabel} 0 0 100% 12u "In order to install your R.T.M.R database, first install PostgreSQL service."
	${NSD_CreateLabel} 0 20 100% 12u "To achieve this, visit :"
	${NSD_CreateLink} 0 40 100% 12u "http://www.postgresql.org/download/windows"
	Pop $R9
	${NSD_OnClick} $R9 onManualInstallClick
	${NSD_CreateLabel} 0 60 100% 12u "Then launch again the netserver installer (current program)."

	Goto showdialog

	; postgresql est install�
	postgresinsalled:
	!insertmacro MUI_HEADER_TEXT "Install/update PostgreSQL database" "Enter necessary information to install/update your R.T.M.R database"

	GetDlgItem $0 $HWNDPARENT 1
	EnableWindow $0 1

	nsDialogs::Create 1018
	Pop $DIALOG

	${NSD_CreateLabel} 0 0 80 12u "Psql executable"
	Pop $PSQL_EXEC_LABEL
	${NSD_CreateText} 100 0 480 12u "$PSQL_EXEC_PATH"
	Pop $PSQL_EXEC

	${NSD_CreateLabel} 0 32 80 12u "Host"
	Pop $DB_HOST_LABEL
	${NSD_CreateText} 100 32 100 12u "localhost"
	Pop $DB_HOST
	${NSD_CreateLabel} 210 32 40 12u "Port"
	Pop $DB_PORT_LABEL
	${NSD_CreateText} 250 32 100 12u "5432"
	Pop $DB_PORT

	${NSD_CreateLabel} 0 64 80 12u "Schema"
	Pop $DB_SCHEMA_LABEL
	${NSD_CreateText} 100 64 100 12u "rtmr"
	Pop $DB_SCHEMA

	${NSD_CreateLabel} 0 96 80 12u "Login"
	Pop $LOGIN_LABEL
	${NSD_CreateText} 100 96 100 12u "postgres"
	Pop $LOGIN

	${NSD_CreateLabel} 0 128 80 12u "Password"
	Pop $PASSWORD_LABEL
	${NSD_CreatePassword} 100 128 100 12u ""
	Pop $PASSWORD

	${NSD_CreateButton} 0 160 120 16u "Install/Upgrade"
	Pop $UPGRADE_BUTTON
	GetFunctionAddress $0 UpdateDatabase
	nsDialogs::OnClick $UPGRADE_BUTTON $0

	SendMessage $HWNDPARENT ${WM_NEXTDLGCTL} $PSQL_EXEC 1

	System::Call shlwapi::SHAutoComplete(i$PSQL_EXEC,i${SHACF_FILESYSTEM})

	showdialog:
	
	nsDialogs::Show

FunctionEnd

Function onManualInstallClick
    ExecShell "open" "http://www.postgresql.org/download/windows" 
FunctionEnd	

Function show_postgres_error
	Pop $R2
	Pop $R1
	Push $R3
	${switch} $R1
		${case} 1
			StrCpy $R3 "PostgreSQL returns a fatal error (out of memory, file not found...)."
			${Break}
		${case} 2
			StrCpy $R3 "PostgreSQL returns a connection error (check your parameters)."
			${Break}
		${case} 3
			StrCpy $R3 "PostgreSQL returns a script error (see log file $R2)."
			${Break}
	${EndSwitch}
	Push $R3
	Exch 3
	Pop $R1
	Pop $R3
	Pop $R2
FunctionEnd	

!define ShowPostgresError "!insertmacro ShowPostgresError"

!macro ShowPostgresError ErrorMessage ErrorCode LogFile
  Push "${ErrorCode}"
  Push "${LogFile}"
  Call show_postgres_error
  Pop "${ErrorMessage}"
!macroend

	Function UpdateDatabase

		${NSD_GetText} $DB_HOST $DB_HOST_VALUE
		${NSD_GetText} $DB_PORT $DB_PORT_VALUE
		${NSD_GetText} $DB_SCHEMA $DB_SCHEMA_VALUE
		${NSD_GetText} $LOGIN $LOGIN_VALUE
		${NSD_GetText} $PASSWORD $PASSWORD_VALUE
		
		StrCmp $PASSWORD_VALUE "" 0 +3
		MessageBox MB_OK "Password is empty."
		Goto done
		
		FileOpen $1 "$INSTDIR\sql\postgres\check_rtmr_database.bat" "w"
		FileWrite $1 "@ECHO OFF$\r$\n"
		FileWrite $1 "set PGPASSWORD=$PASSWORD_VALUE$\r$\n"
		FileWrite $1 "$\"$PSQL_EXEC_PATH$\" --no-psqlrc --host=$DB_HOST_VALUE --port=$DB_PORT_VALUE --username=$LOGIN_VALUE --command=$\"select count(*) as name from pg_catalog.pg_database where datname=$\'$DB_SCHEMA_VALUE$\';$\"$\r$\n"
		FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
		FileClose $1
		ExecWait '"$INSTDIR\sql\postgres\check_rtmr_database.bat" > $\"$INSTDIR\logs\databases.txt$\"' $0
		Delete "$INSTDIR\sql\postgres\check_rtmr_database.bat"
		StrCmp $0 "0" 0 checkerror

		FileOpen $0 "$INSTDIR\logs\databases.txt" "r"
		FileRead $0 $RTMR_DATABASE_COUNT
		FileRead $0 $RTMR_DATABASE_COUNT
		FileRead $0 $RTMR_DATABASE_COUNT
		FileClose $0
		Delete "$INSTDIR\logs\databases.txt"
		${Trim} $RTMR_DATABASE_COUNT $RTMR_DATABASE_COUNT
		
		StrCmp $RTMR_DATABASE_COUNT "0" intalldb updatedb
		
		; Nouvelle installation
		intalldb:
		FileOpen $1 "$INSTDIR\sql\postgres\install_rtmr_database.bat" "w"
		FileWrite $1 "@ECHO OFF$\r$\n"
		FileWrite $1 "echo Installation of database $DB_SCHEMA_VALUE$\r$\n"
		FileWrite $1 "set PGPASSWORD=$PASSWORD_VALUE$\r$\n"
		FileWrite $1 "cd /D $\"$INSTDIR\sql\postgres$\"$\r$\n"
		FileWrite $1 "$\"$PSQL_EXEC_PATH$\" --host=$DB_HOST_VALUE --port=$DB_PORT_VALUE --username=$LOGIN_VALUE --set=var_dbname=$DB_SCHEMA_VALUE --file=install_database.sql  > $\"$INSTDIR\logs\install.log$\"$\r$\n"
		FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
		FileClose $1
		ExecWait '"$INSTDIR\sql\postgres\install_rtmr_database.bat"' $0
		Delete "$INSTDIR\sql\postgres\install_rtmr_database.bat"
		${if} $0 == "0"
			${WriteINIStrNS} $R0 "$INSTDIR\conf\netserver.conf" "DB_HOSTNAME" "$DB_HOST_VALUE"
			${WriteINIStrNS} $R0 "$INSTDIR\conf\netserver.conf" "DB_HOST_PORT" "$DB_PORT_VALUE"
			${WriteINIStrNS} $R0 "$INSTDIR\conf\netserver.conf" "DB_SCHEMA" "$DB_SCHEMA_VALUE"
			MessageBox MB_OK "Database $DB_SCHEMA_VALUE is now installed.$\r$\nThe netserver configuration file ($INSTDIR\conf\netserver.conf) has been updated."		
		${else}
			StrCpy $POSTGRES_LOG_FILE "$INSTDIR\logs\install.log"
			${ShowPostgresError} $POSTGRES_ERROR_MESSAGE $0  $POSTGRES_LOG_FILE
			MessageBox MB_OK "Installation program sends error $0.$\r$\n$POSTGRES_ERROR_MESSAGE"
		${EndIf}
		Goto done			

		; Mise � jour de la base de donn�es  
		updatedb:
		FileOpen $1 "$INSTDIR\sql\postgres\get_rtmr_database_version.bat" "w"
		FileWrite $1 "@ECHO OFF$\r$\n"
		FileWrite $1 "set PGPASSWORD=$PASSWORD_VALUE$\r$\n"
		FileWrite $1 "$\"$PSQL_EXEC_PATH$\" --no-psqlrc --host=$DB_HOST_VALUE --port=$DB_PORT_VALUE --username=$LOGIN_VALUE --dbname=$DB_SCHEMA_VALUE --command=$\"select database_version_number from database_version_table;$\"  > $\"$INSTDIR\logs\version.txt$\"$\r$\n"
		FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
		FileClose $1
		ExecWait '"$INSTDIR\sql\postgres\get_rtmr_database_version.bat"' $0
		Delete "$INSTDIR\sql\postgres\get_rtmr_database_version.bat"
		StrCmp $0 "0" 0 retrieve_db_version_error
		
		FileOpen $0 "$INSTDIR\logs\version.txt" "r"
		FileRead $0 $RTMR_DATABASE_VERSION_VALUE
		FileRead $0 $RTMR_DATABASE_VERSION_VALUE
		FileRead $0 $RTMR_DATABASE_VERSION_VALUE 12
		FileClose $0
		Delete "$INSTDIR\logs\version.txt"
		
		; Mise � jour de la base de donn�e
		${Trim} $RTMR_DATABASE_VERSION_VALUE $RTMR_DATABASE_VERSION_VALUE
		${if} $RTMR_DATABASE_VERSION_VALUE == "01.04.00.00"
			StrCpy $RTMR_DATABASE_UPGRADE_SCRIPT "1.4.0.0_to_1.5.0.0.sql"
		${else}
			${if} $RTMR_DATABASE_VERSION_VALUE == "01.05.00.00"
				MessageBox MB_OK "$DB_SCHEMA_VALUE database is already up-to-date (version is 1.5.0.0)."		
				Goto done
			${endif}
			Goto showdbversion
		${endif}
		
		FileOpen $1 "$INSTDIR\sql\postgres\update_rtmr_database.bat" "w"
		FileWrite $1 "@ECHO OFF$\r$\n"
		FileWrite $1 "echo Upgrade of database $DB_SCHEMA_VALUE$\r$\n"
		FileWrite $1 "set PGPASSWORD=$PASSWORD_VALUE$\r$\n"
		FileWrite $1 "cd /D $\"$INSTDIR\sql\postgres\upgrade$\"$\r$\n"
		FileWrite $1 "$\"$PSQL_EXEC_PATH$\" --no-psqlrc --host=$DB_HOST_VALUE --port=$DB_PORT_VALUE --username=$LOGIN_VALUE --dbname=$DB_SCHEMA_VALUE --file=$RTMR_DATABASE_UPGRADE_SCRIPT  > $\"$INSTDIR\logs\upgrade.log$\"$\r$\n"
		FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
		FileClose $1
		ExecWait '"$INSTDIR\sql\postgres\update_rtmr_database.bat"' $0
		Delete "$INSTDIR\sql\postgres\update_rtmr_database.bat"
		${if} $0 == "0"
			MessageBox MB_OK "Database $DB_SCHEMA_VALUE has been upgraded."
		${else}
			StrCpy $POSTGRES_LOG_FILE "$INSTDIR\logs\upgrade.log"
			${ShowPostgresError} $POSTGRES_ERROR_MESSAGE $0 $POSTGRES_LOG_FILE
			MessageBox MB_OK "Upgrade program sends error $0.$\r$\n$POSTGRES_ERROR_MESSAGE"
		${EndIf}
		Goto done			

		showdbversion:
		MessageBox MB_OK "$DB_SCHEMA_VALUE database version is $RTMR_DATABASE_VERSION_VALUE.$\r$\nThis version is not taken into account."		
		Goto done
				
		retrieve_db_version_error:
		StrCpy $POSTGRES_LOG_FILE ""
		${ShowPostgresError} $POSTGRES_ERROR_MESSAGE $0 $POSTGRES_LOG_FILE
		MessageBox MB_OK "Cannot retrieve current $DB_SCHEMA_VALUE database version (error $0).$\r$\n$POSTGRES_ERROR_MESSAGE"
		Goto done
								
		checkerror:
		StrCpy $POSTGRES_LOG_FILE "$INSTDIR\logs\upgrade.log"
		${ShowPostgresError} $POSTGRES_ERROR_MESSAGE $0 $POSTGRES_LOG_FILE
		MessageBox MB_OK "Check program sends error $0.$\r$\n$POSTGRES_ERROR_MESSAGE"

		done:
		
	FunctionEnd

  Page custom InstallDatabasePage

 
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "!R.T.M.R Server" MainSection

  SectionIn RO

  CreateDirectory "$INSTDIR\bin"
  CreateDirectory "$INSTDIR\conf"
  CreateDirectory "$INSTDIR\logs"
  CreateDirectory "$INSTDIR\sql"
  
  ReadRegStr $0 HKLM "Software\R.T.M.R Server" ""
  IfFileExists "$0\bin\netserver.exe" 0 +2
  ExecWait '"$0\bin\netserver.exe" -remove'

  IfFileExists "$0\conf\netserver.conf" 0 +2
  CopyFiles "$0\conf\netserver.conf" "$INSTDIR\conf"

  ReadRegStr $0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "NETSERVER_HOME"
  IfFileExists "$0\bin\netserver.exe" 0 +2
  ExecWait '"$0\bin\netserver.exe" -remove'
  
  IfFileExists "$0\conf\netserver.conf" 0 +2
  CopyFiles "$0\conf\netserver.conf" "$INSTDIR\conf"

  ;ADD YOUR OWN FILES HERE...
  SetOutPath "$INSTDIR\bin"
  File "..\..\..\netserver\Release\netserver.exe"
  File "comerr32.dll"
  File "gssapi32.dll"
  File "k5sprt32.dll"
  File "krb5_32.dll"
  File "libeay32.dll"
  File "libiconv-2.dll"
  File "libintl-8.dll"
  File "libpq.dll"
  File "msvcr100.dll"
  File "netserver_install.bat"
  File "netserver_start.bat"
  File "netserver_stop.bat"
  File "netserver_uninstall.bat"
  File "ssleay32.dll"
  
  IfFileExists "$INSTDIR\conf\netserver.conf" +3 0
  SetOutPath "$INSTDIR\conf"
  File "..\..\..\netserver\conf\netserver.conf"

  SetOutPath "$INSTDIR\sql"
  File /r "..\..\..\sql\*.*"

  ;Store installation folder
  WriteRegStr HKLM "Software\R.T.M.R Server" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$netserverFolder"
    CreateShortCut "$SMPROGRAMS\$netserverFolder\start netserver service.lnk" "$INSTDIR\bin\netserver_start.bat"
	push "$SMPROGRAMS\$netserverFolder\start netserver service.lnk"
	call ShellLinkSetRunAs
    CreateShortCut "$SMPROGRAMS\$netserverFolder\stop netserver service.lnk" "$INSTDIR\bin\netserver_stop.bat"
	push "$SMPROGRAMS\$netserverFolder\stop netserver service.lnk"
	call ShellLinkSetRunAs
	
	
	FileOpen $1 "$INSTDIR\bin\netserver_install.bat" "w"
	FileWrite $1 "@ECHO OFF$\r$\n"
	FileWrite $1 "net stop netserver$\r$\n"
	FileWrite $1 "$\"$INSTDIR\bin\netserver.exe$\"$\r$\n"
	FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
	FileClose $1
    CreateShortCut "$SMPROGRAMS\$netserverFolder\install netserver service.lnk" "$INSTDIR\bin\netserver_install.bat"
	push "$SMPROGRAMS\$netserverFolder\install netserver service.lnk"
	call ShellLinkSetRunAs

	FileOpen $1 "$INSTDIR\bin\netserver_uninstall.bat" "w"
	FileWrite $1 "@ECHO OFF$\r$\n"
	FileWrite $1 "net stop netserver$\r$\n"
	FileWrite $1 "$\"$INSTDIR\bin\netserver.exe$\" -remove$\r$\n"
	FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
	FileClose $1
    CreateShortCut "$SMPROGRAMS\$netserverFolder\uninstall netserver service.lnk" "$INSTDIR\bin\netserver_uninstall.bat"
	push "$SMPROGRAMS\$netserverFolder\uninstall netserver service.lnk"
	call ShellLinkSetRunAs
	
	FileOpen $1 "$INSTDIR\bin\netserver_debug.bat" "w"
	FileWrite $1 "@ECHO OFF$\r$\n"
	FileWrite $1 "net stop netserver$\r$\n"
	FileWrite $1 "$\"$INSTDIR\bin\netserver.exe$\" -d$\r$\n"
	FileWrite $1 "exit %ERRORLEVEL%$\r$\n"
	FileClose $1
    CreateShortCut "$SMPROGRAMS\$netserverFolder\netserver debug.lnk" "$INSTDIR\bin\netserver_debug.bat"
	push "$SMPROGRAMS\$netserverFolder\netserver debug.lnk"
	call ShellLinkSetRunAs
	
    CreateShortCut "$SMPROGRAMS\$netserverFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
	push "$SMPROGRAMS\$netserverFolder\Uninstall.lnk"
	call ShellLinkSetRunAs
  
  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

Function .onInstSuccess

	ExecWait '"$INSTDIR\bin\netserver.exe"'
	ExecWait 'net start netserver'
	
FunctionEnd


;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_MainSection ${LANG_ENGLISH} "Server netserver"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${MainSection} $(DESC_MainSection)
   !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ExecWait '"$INSTDIR\bin\netserver.exe" -remove'
    
  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r "$INSTDIR\bin"
  RMDir /r "$INSTDIR\sql"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $netserverFolder
    
  Delete "$SMPROGRAMS\$netserverFolder\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$netserverFolder"
  
  DeleteRegKey /ifempty HKLM "Software\R.T.M.R Server"

SectionEnd