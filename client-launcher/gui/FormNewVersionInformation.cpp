/*****************************************************************************
Copyright (C) 2012 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R.

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include "FormNewVersionInformation.h"
#include "ui_FormNewVersionInformation.h"

#include <QPushButton>

FormNewVersionInformation::FormNewVersionInformation(QString version, QString informations, QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::FormNewVersionInformation)
{
    m_ui->setupUi(this);

    m_ui->version->setText(tr("La version <b>%1</b> est disponible.").arg(version));

    if (informations.isEmpty())
    m_ui->version_informations->setVisible(false);

    m_ui->version_informations->setText(informations);

    connect(m_ui->buttonBox->button(QDialogButtonBox::Yes), SIGNAL(clicked()), this, SLOT(accept()));
    connect(m_ui->buttonBox->button(QDialogButtonBox::No), SIGNAL(clicked()), this, SLOT(reject()));
}


FormNewVersionInformation::~FormNewVersionInformation()
{
    delete m_ui;
}
