cmake_minimum_required(VERSION 3.7)

include (../common/CMakeFiles.common)

set (sources_CPP
    client_launcher.cpp
    gui/FormNewVersionInformation.cpp
    utils/ProcessUtils.cpp
    main.cpp
)

set (sources_H
    client_launcher.h
    gui/FormNewVersionInformation.h
    utils/ProcessUtils.h
)

set (sources_UI
    gui/FormNewVersionInformation.ui
)

set (sources_QRC
    ../client-app/resources.qrc
    client-launcher-resources.qrc
)

set (sources_RC
    ../client-app/resources.rc
)

IF (WIN32)
    set (EXTRA_LIBS psapi User32)
ELSE()
    set (EXTRA_LIBS procps X11)
ENDIF(WIN32)

add_definitions(-DAUTOMATION_LIB)
add_definitions(-DBINDIR=${CMAKE_INSTALL_BINDIR})

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
find_package(Qt5Widgets)
find_package(Qt5Network)
find_package(Qt5Gui)

qt5_wrap_ui(qt_sources_UI ${sources_UI})

add_executable (client-launcher
    ${sources_H}
    ${sources_CPP}
    ${sources_QRC}
    ${sources_RC}
    ${qt_sources_UI}
)

target_include_directories(client-launcher PUBLIC ${Qt5Widgets_INCLUDE_DIRS})

target_link_libraries (client-launcher
    ${EXTRA_LIBS}
    Qt5::Core
    Qt5::Widgets
    Qt5::Network
)
