/*****************************************************************************
Copyright (C) 2010 Emmanuel Jorge ejorge@free.fr

This file is part of R.T.M.R (Requirements and Tests Management Repository).

R.T.M.R is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

R.T.M.R is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with R.T.M.R.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#if (defined(_WINDOWS) || defined(WIN32))
	#include <winsock2.h>
	#ifdef _LDAP
		#include <winldap.h>
	#endif
#else
	#ifdef _LDAP
		#include <ldap.h>
	#endif
#endif

#include "org_ej_EJNativeLibrary.h"
#include "client.h"

// Directives de base de données
#ifdef _MYSQL
#include "../common/mysql/EJMysqlLibrary.h"
#else
#ifdef _POSTGRES
#include "../common/postgres/rtmrpostgreslibrary.h"
#else

#endif
#endif

#ifdef _LDAP
	#include "../common/ldap/EJLdapLibrary.h"
#endif

#include "../common/utilities.h"
#include "../common/entities.h"
#include "../common/constants.h"
#include "../common/errors.h"

#if defined (__APPLE__)
	#include <malloc/malloc.h>
#else
		#include <malloc.h>
#endif

#include <string.h>


static char* jni_arrays_strings_to_net_str(JNIEnv *in_java_env, entity_def *in_entity_def, jobjectArray in_columns_record, jobjectArray in_originals_columns_record, char *in_data_str)
{
	char							*tmp_data_ptr = in_data_str;
	unsigned int 		tmp_index = 0;
	unsigned int		tmp_diff_columns = 0;

	jstring				tmp_java_value = NULL;
	const char			*tmp_value = NULL;

	jstring				tmp_java_original_value = NULL;
	const char			*tmp_original_value = NULL;

	if (in_originals_columns_record != NULL)
	{
		// Clé primaire
		tmp_java_value = (*in_java_env)->GetObjectArrayElement(in_java_env, in_columns_record, 0);
		if (tmp_java_value != NULL)
			tmp_value = (*in_java_env)->GetStringUTFChars(in_java_env, tmp_java_value, 0);

		if (tmp_value != NULL)
			tmp_data_ptr = format_string(tmp_value, tmp_data_ptr);

		tmp_data_ptr += sprintf(tmp_data_ptr, "%c", SEPARATOR_CHAR);

		for (tmp_index = 1; tmp_index < in_entity_def->m_entity_columns_count; tmp_index++)
		{
			tmp_java_value = (*in_java_env)->GetObjectArrayElement(in_java_env, in_columns_record, tmp_index);
			if (tmp_java_value != NULL)
				tmp_value = (*in_java_env)->GetStringUTFChars(in_java_env, tmp_java_value, 0);
			else
				tmp_value = NULL;

			tmp_java_original_value = (*in_java_env)->GetObjectArrayElement(in_java_env, in_originals_columns_record, tmp_index);
			if (tmp_java_original_value != NULL)
				tmp_original_value = (*in_java_env)->GetStringUTFChars(in_java_env, tmp_java_original_value, 0);
			else
				tmp_original_value = NULL;

			if ((in_entity_def->m_primary_key != NULL &&  index_of_string_in_array(in_entity_def->m_entity_columns_names[tmp_index], in_entity_def->m_primary_key) >= 0)
						|| compare_values(tmp_value, tmp_original_value) != 0)
				{
					tmp_diff_columns++;
					if (is_empty_string(tmp_value))
					{
						tmp_data_ptr += sprintf(tmp_data_ptr, "''%c", SEPARATOR_CHAR);
					}
					else
					{
						tmp_data_ptr = format_string(tmp_value, tmp_data_ptr);
						tmp_data_ptr += sprintf(tmp_data_ptr, "%c", SEPARATOR_CHAR);
					}
				}
				else
					tmp_data_ptr += sprintf(tmp_data_ptr, "%c", SEPARATOR_CHAR);
		}

		// Aucun changement
		if (tmp_diff_columns == 0)
			return NULL;
	}
	else
	{
		for (tmp_index = 0; tmp_index < in_entity_def->m_entity_columns_count; tmp_index++)
		{
				tmp_java_value = (*in_java_env)->GetObjectArrayElement(in_java_env, in_columns_record, tmp_index);

				if (tmp_java_value != NULL)
				{
					tmp_value = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, tmp_java_value, 0);
					tmp_data_ptr = format_string(tmp_value, tmp_data_ptr);
					tmp_data_ptr += sprintf(tmp_data_ptr, "%c", SEPARATOR_CHAR);
				}
				else
					tmp_data_ptr += sprintf(tmp_data_ptr, "''%c", SEPARATOR_CHAR);
		}
	}

	return tmp_data_ptr;
}

static char* jni_array_strings_to_net_str(JNIEnv *in_java_env, unsigned int in_columns_count, jobjectArray in_columns_record, char *in_data_str)
{
	char				*tmp_data_ptr = in_data_str;
	unsigned int 		tmp_index = 0;
	jstring				tmp_java_value = NULL;
	const char			*tmp_value = NULL;

	if (in_columns_record != NULL)
	{
		for (tmp_index = 0; tmp_index < in_columns_count; tmp_index++)
		{
			tmp_java_value = (*in_java_env)->GetObjectArrayElement(in_java_env, in_columns_record, tmp_index);
			if (tmp_java_value != NULL)
			{
				tmp_value = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, tmp_java_value, 0);
				if (tmp_value != NULL)
					tmp_data_ptr = format_string(tmp_value, tmp_data_ptr);
			}
			tmp_data_ptr += sprintf(tmp_data_ptr, "%c", SEPARATOR_CHAR);
		}
	}

	return tmp_data_ptr;
}

static jobjectArray jni_parse_response_rows(JNIEnv *in_java_env, net_session *in_session, char *in_data_str, unsigned long *out_rows_count, unsigned long *out_columns_count)
{
	char							*tmp_data_ptr = in_data_str;
	char							*tmp_current_data_ptr = NULL;
	char							tmp_rows_count_str[16];
	char							tmp_columns_count_str[16];
	jobjectArray					tmp_rows_array = NULL;
	jobjectArray					tmp_columns_array = NULL;

	unsigned long		tmp_row_index = 0;
	unsigned long		tmp_column_index = 0;


	tmp_data_ptr = net_get_field(NET_MESSAGE_TYPE_INDEX+1, tmp_data_ptr, tmp_rows_count_str, SEPARATOR_CHAR);
	tmp_data_ptr = net_get_field(0, tmp_data_ptr, tmp_columns_count_str, SEPARATOR_CHAR);

	*out_rows_count = atoi(tmp_rows_count_str);
	*out_columns_count = atoi(tmp_columns_count_str);

	tmp_columns_array = (jobjectArray)(*in_java_env)->NewObjectArray(in_java_env, (*out_columns_count), (*in_java_env)->FindClass(in_java_env, "java/lang/String"), NULL);
	tmp_rows_array = (jobjectArray)(*in_java_env)->NewObjectArray(in_java_env, (*out_rows_count), (*in_java_env)->GetObjectClass(in_java_env, tmp_columns_array), 0);

	for(tmp_row_index = 0; tmp_row_index < (*out_rows_count); tmp_row_index++)
	{
		for(tmp_column_index = 0; tmp_column_index < (*out_columns_count); tmp_column_index++)
		{
			tmp_current_data_ptr = net_get_field(0, tmp_data_ptr, in_session->m_column_buffer, SEPARATOR_CHAR);
			if (is_empty_string(in_session->m_column_buffer) == FALSE)
				(*in_java_env)->SetObjectArrayElement(in_java_env, tmp_columns_array, tmp_column_index, (*in_java_env)->NewStringUTF(in_java_env, in_session->m_column_buffer));
			else
				(*in_java_env)->SetObjectArrayElement(in_java_env, tmp_columns_array, tmp_column_index, NULL);

			tmp_data_ptr = tmp_current_data_ptr;
		}

		(*in_java_env)->SetObjectArrayElement(in_java_env, tmp_rows_array, tmp_row_index, tmp_columns_array);
		if (tmp_row_index < (*out_rows_count) - 1)
			tmp_columns_array = (jobjectArray)(*in_java_env)->NewObjectArray(in_java_env, (*out_columns_count), (*in_java_env)->FindClass(in_java_env, "java/lang/String"), NULL);
	}

	return tmp_rows_array;
}



static int jni_parse_response_columns(JNIEnv *in_java_env, net_session *in_session, unsigned int in_columns_count, const char *in_data_str, jobjectArray in_out_columns_record)
{
	char							*tmp_data_ptr = (char*)in_data_str;
	char							*tmp_current_data_ptr = NULL;

	char							tmp_rows_count_str[16];
	char							tmp_columns_count_str[16];

	unsigned long		tmp_column_index = 0;

	unsigned int			tmp_row_count = 0;
	unsigned int			tmp_column_count = 0;


	tmp_data_ptr = net_get_field(NET_MESSAGE_TYPE_INDEX+1, tmp_data_ptr, tmp_rows_count_str, SEPARATOR_CHAR);
	tmp_data_ptr = net_get_field(0, tmp_data_ptr, tmp_columns_count_str, SEPARATOR_CHAR);

	tmp_row_count = atoi(tmp_rows_count_str);
	tmp_column_count = atoi(tmp_columns_count_str);

	if (tmp_row_count  == 1)
	{
		if (tmp_column_count == in_columns_count)
		{
			for (tmp_column_index = 0; tmp_column_index < tmp_column_count; tmp_column_index++)
			{
				tmp_current_data_ptr = net_get_field(0, tmp_data_ptr, in_session->m_column_buffer, SEPARATOR_CHAR);
				if (is_empty_string(in_session->m_column_buffer) == FALSE)
					(*in_java_env)->SetObjectArrayElement(in_java_env, in_out_columns_record, tmp_column_index, (*in_java_env)->NewStringUTF(in_java_env, in_session->m_column_buffer));
				else
					(*in_java_env)->SetObjectArrayElement(in_java_env, in_out_columns_record, tmp_column_index, (*in_java_env)->NewStringUTF(in_java_env, NULL));

				tmp_data_ptr = tmp_current_data_ptr;
			}
		}
		else
			return ENTITY_COLUMNS_COUNT_ERROR;
	}
	else if (tmp_row_count  > 1)
	{
		return DB_SQL_TOO_MUCH_ROWS_FOUND;
	}
	else
		return DB_SQL_NO_ROW_FOUND;

	return NOERR;
}


JNIEXPORT jlong JNICALL Java_org_ej_EJNativeLibrary_connect(JNIEnv *in_java_env, jclass in_class, jstring in_hostname, jint in_port, jstring in_username, jstring in_passwd)
{
	char		*tmp_hostname = NULL;
	char		*tmp_username = NULL;
	char		*tmp_passwd = NULL;
	net_session	*tmp_session = NULL;
	long		tmp_status = NOERR;

	// Vérifier les paramètres
	if (in_hostname == NULL)
	{
		fprintf(stderr, "*** JNI error (connect) : hostname is null\n");
		return EMPTY_OBJECT;
	}

	if (in_username == NULL)
	{
		fprintf(stderr, "*** JNI error (connect) : username is null\n");
		return EMPTY_OBJECT;
	}

	if (in_passwd == NULL)
	{
		fprintf(stderr, "*** JNI error (connect) : passwd is null\n");
		return EMPTY_OBJECT;
	}

	tmp_hostname = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_hostname, 0);
	tmp_username = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_username, 0);
	tmp_passwd = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_passwd, 0);

	tmp_status = cl_connect(&tmp_session, tmp_hostname, in_port, tmp_username, tmp_passwd, FALSE, NULL, NULL);
	if (tmp_status == NOERR)
	{
		if (tmp_session != NULL)
			tmp_status = (long)tmp_session;
		else
			tmp_status = NET_CONNEXION_ERROR;
	}

	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_hostname, tmp_hostname);
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_username, tmp_username);
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_passwd, tmp_passwd);

	return tmp_status;
}

JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_disconnect(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr)
{
	fprintf(stdout, "JNI  (die)\n");

	return cl_disconnect((net_session*)in_session_ptr);
}

JNIEXPORT jobjectArray JNICALL Java_org_ej_EJNativeLibrary_runSql(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jstring in_statement)
{
	int				tmp_status = 0;
	int				tmp_nb_bytes = 0;
	unsigned long	tmp_rows_count = 0;
	unsigned long	tmp_columns_count = 0;
	net_session		*tmp_session = (net_session*)in_session_ptr;
	const char		*tmp_statement = NULL;

	if (tmp_session == NULL || in_statement == NULL) return NULL;

	tmp_statement = (*in_java_env)->GetStringUTFChars(in_java_env, in_statement, 0);

	tmp_nb_bytes = sprintf(tmp_session->m_request, "%c%c%i%c%s%c%c", PAQUET_START_CHAR,
			SEPARATOR_CHAR, RUN_SQL,
			SEPARATOR_CHAR, tmp_statement,
			SEPARATOR_CHAR, PAQUET_STOP_CHAR);

	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_statement, tmp_statement);

	tmp_status = cl_send_request(tmp_session, tmp_nb_bytes);
	if (tmp_status != NOERR)
	{
		return NULL;
	}

	return jni_parse_response_rows(in_java_env, tmp_session, tmp_session->m_response, &tmp_rows_count, &tmp_columns_count);
}

JNIEXPORT jobjectArray JNICALL Java_org_ej_EJNativeLibrary_loadLdapEntries(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_entry_signature, jstring in_ldap_url, jstring in_distinguish_name, jstring in_credencial, jstring in_base_query, jstring in_filter_query )
{
#ifdef _LDAP
	char						*tmp_ldap_url = NULL;
	char						*tmp_distinguish_name = NULL;
	char						*tmp_credencial = NULL;
	char						*tmp_base_query = NULL;
	char						*tmp_filter_query = NULL;

	unsigned long	tmp_row_index = 0;
	unsigned long	tmp_columns_index = 0;
	unsigned long	tmp_rows_count = 0;
	unsigned long	tmp_columns_count = 0;

	char						***tmp_returned_rows = NULL;
	char						**tmp_attributes_names = NULL;

	jobjectArray			tmp_rows_array = NULL;
	jobjectArray			tmp_columns_array = NULL;

	int							tmp_ldap_return_code = 0;

	/* Vérifier les parametres */
	if (in_ldap_url == NULL || in_distinguish_name == NULL || in_credencial == NULL)
	{
		fprintf(stderr, "*** JNI error (loadLdapEntries) : ldap url, distinguish name or credencial is null\n");
		return NULL;
	}

	/* Chercher le définition de l'entités */
	if (get_ldap_entry_def(in_entry_signature, &tmp_attributes_names) < 0)
	{
		fprintf(stderr, "*** JNI error (loadLdapEntries) : ldap entry signature unknow\n");
		return NULL;
	}

	tmp_ldap_url = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_ldap_url, 0);
	tmp_distinguish_name = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_distinguish_name, 0);
	tmp_credencial = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_credencial, 0);

	if (is_empty_string(tmp_distinguish_name) == TRUE || is_empty_string(tmp_credencial) == TRUE)
	{
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_ldap_url, tmp_ldap_url);
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_distinguish_name, tmp_distinguish_name);
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_credencial, tmp_credencial);
		fprintf(stderr, "*** JNI error (loadLdapEntries) : distinguish name or credencial is empty\n");
		return NULL;
	}

	if (in_base_query != NULL)
		tmp_base_query = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_base_query, 0);

	if (in_filter_query != NULL)
		tmp_filter_query = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_filter_query, 0);


	/* Exécuter la requête LDAP */
	fprintf(stdout, "JNI  (loadLdapEntries) calling ldap_query...\n");

	tmp_returned_rows = ldap_query(tmp_ldap_url, tmp_distinguish_name, tmp_credencial, tmp_base_query, tmp_filter_query, tmp_attributes_names, &tmp_rows_count, &tmp_columns_count, &tmp_ldap_return_code);
	if (tmp_returned_rows)
	{
		tmp_columns_array = (jobjectArray)(*in_java_env)->NewObjectArray(in_java_env, tmp_columns_count, (*in_java_env)->FindClass(in_java_env, "java/lang/String"), (*in_java_env)->NewStringUTF(in_java_env, ""));
		tmp_rows_array = (jobjectArray)(*in_java_env)->NewObjectArray(in_java_env, tmp_rows_count, (*in_java_env)->GetObjectClass(in_java_env, tmp_columns_array), 0);

		fprintf(stdout, "JNI  (loadLdapEntries) treating ldap_query\n");

		for(tmp_row_index = 0; tmp_row_index < tmp_rows_count; tmp_row_index++)
		{
			for(tmp_columns_index = 0; tmp_columns_index < tmp_columns_count; tmp_columns_index++)
			{
				if (tmp_returned_rows[tmp_row_index][tmp_columns_index])
				{
					fprintf(stdout, "%s -> %s\n", tmp_attributes_names[tmp_columns_index], tmp_returned_rows[tmp_row_index][tmp_columns_index]);
					(*in_java_env)->SetObjectArrayElement(in_java_env, tmp_columns_array, tmp_columns_index, (*in_java_env)->NewStringUTF(in_java_env, tmp_returned_rows[tmp_row_index][tmp_columns_index]));
					free(tmp_returned_rows[tmp_row_index][tmp_columns_index]);
				}
			}
			free(tmp_returned_rows[tmp_row_index]);

			(*in_java_env)->SetObjectArrayElement(in_java_env, tmp_rows_array, tmp_row_index, tmp_columns_array);
			tmp_columns_array = (jobjectArray)(*in_java_env)->NewObjectArray(in_java_env, tmp_columns_count, (*in_java_env)->FindClass(in_java_env, "java/lang/String"), (*in_java_env)->NewStringUTF(in_java_env, ""));
		}
		free(tmp_returned_rows);
	}

	fprintf(stdout, "JNI  (loadLdapEntries) end ldap_query\n");

	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_ldap_url, tmp_ldap_url);
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_distinguish_name, tmp_distinguish_name);
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_credencial, tmp_credencial);

	if (tmp_base_query != NULL)
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_base_query, tmp_base_query);

	if (tmp_filter_query != NULL)
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_filter_query, tmp_filter_query);
	return tmp_rows_array;
#else
	return NULL;
#endif
}


JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_loadLdapEntry(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_entry_signature, jstring in_ldap_url, jstring in_distinguish_name, jstring in_credencial, jobjectArray in_out_columns_record, jstring in_base_query )
{
#ifdef _LDAP
	char						*tmp_ldap_url = NULL;
	char						*tmp_distinguish_name = NULL;
	char						*tmp_credencial = NULL;
	char						*tmp_base_query = NULL;
	char						*tmp_filter_query = NULL;

	unsigned long	tmp_row_index = 0;
	unsigned long	tmp_columns_index = 0;
	unsigned long	tmp_rows_count = 0;
	unsigned long	tmp_columns_count = 0;

	char						***tmp_returned_rows = NULL;
	char						**tmp_attributes_names = NULL;

	int							tmp_ldap_return_code = 0;

	/* Vérifier les parametres */
	if (in_ldap_url == NULL || in_distinguish_name == NULL || in_credencial == NULL)
	{
		fprintf(stderr, "*** JNI error (loadLdapEntry) : ldap url, distinguish name or credencial is null\n");
		return EMPTY_OBJECT;
	}

	/* Chercher le définition de l'entités */
	if (get_ldap_entry_def(in_entry_signature, &tmp_attributes_names) < 0)
	{
		fprintf(stderr, "*** JNI error (loadLdapEntry) : ldap entry signature unknow\n");
		return UNKNOW_LDAP_TYPE_ENTRY;
	}

	tmp_ldap_url = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_ldap_url, 0);
	tmp_distinguish_name = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_distinguish_name, 0);
	tmp_credencial = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_credencial, 0);

	if (is_empty_string(tmp_distinguish_name) == TRUE || is_empty_string(tmp_credencial) == TRUE)
	{
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_ldap_url, tmp_ldap_url);
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_distinguish_name, tmp_distinguish_name);
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_credencial, tmp_credencial);
		fprintf(stderr, "*** JNI error (loadLdapEntry) : distinguish name or credencial is empty\n");
		return EMPTY_OBJECT;
	}

	if (in_base_query != NULL)
		tmp_base_query = (char*)(*in_java_env)->GetStringUTFChars(in_java_env, in_base_query, 0);

	/* Exécuter la requête LDAP */
	fprintf(stdout, "JNI  (loadLdapEntry) calling ldap_query...\n");
	tmp_returned_rows = ldap_query(tmp_ldap_url, tmp_distinguish_name, tmp_credencial, tmp_base_query, tmp_filter_query, tmp_attributes_names, &tmp_rows_count, &tmp_columns_count, &tmp_ldap_return_code);

	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_ldap_url, tmp_ldap_url);
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_distinguish_name, tmp_distinguish_name);
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_credencial, tmp_credencial);

	if (tmp_base_query != NULL){
		fprintf(stdout, "JNI  (loadLdapEntry) release base query string...\n");
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_base_query, tmp_base_query);
	}

	if (tmp_returned_rows)
	{
		/* Il ne doit y avoir qu'un seul enregistrement correspondant à la clé primaire */
		if (tmp_rows_count == 1)
		{
			/* Alimenter le tableau des colonnes */
			for(tmp_columns_index = 0; tmp_columns_index < tmp_columns_count; tmp_columns_index++)
			{
				if (tmp_returned_rows[0][tmp_columns_index])
				{
					(*in_java_env)->SetObjectArrayElement(in_java_env, in_out_columns_record, tmp_columns_index, (*in_java_env)->NewStringUTF(in_java_env, tmp_returned_rows[0][tmp_columns_index]));
					fprintf(stdout, "JNI  (loadLdapEntry) free attr %li : %s...\n", tmp_columns_index, tmp_returned_rows[0][tmp_columns_index]);
					free(tmp_returned_rows[0][tmp_columns_index]);
				}
			}
			fprintf(stdout, "JNI  (loadLdapEntry) free row %i...\n", 0);
			free(tmp_returned_rows[0]);

			return NOERR;
		}
		/* Plusieurs enregistrements ont été trouvés : libérer la mémoire et renvoyer une erreur */
		else if (tmp_rows_count> 1)
		{
			fprintf(stderr, "*** JNI  (loadLdapEntry) trop de lignes renvoyées : %li\n", tmp_rows_count);

			for(tmp_row_index = 0; tmp_row_index < tmp_rows_count; tmp_row_index++)
			{
				for(tmp_columns_index = 0; tmp_columns_index < tmp_columns_count; tmp_columns_index++)
				{
					fprintf(stdout, "JNI  (loadLdapEntry) free attr %li...\n", tmp_columns_index);
					free(tmp_returned_rows[tmp_row_index][tmp_columns_index]);
				}
				fprintf(stdout, "JNI  (loadLdapEntry) free row %li...\n", tmp_row_index);
				free(tmp_returned_rows);
			}
			return LDAP_TOO_MUCH_ENTRIES_FOUND;
		}
		/* Aucun enregistrement a été trouvé */
		else
		{
			if (tmp_ldap_return_code != NOERR)
				return tmp_ldap_return_code;
		}
	}
#endif
	return LDAP_NO_ENTRY_FOUND;
}


JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_callSqlProcedure(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jstring in_procedure_name, jobjectArray in_out_parameters)
{
	jint						tmp_param_count = 0;

	int							tmp_status = NOERR;
	char						*tmp_ptr = NULL;

	net_session					*tmp_session = (net_session*)in_session_ptr;
	const char					*tmp_procedure_name = NULL;

	if (tmp_session == NULL || in_out_parameters == NULL || in_procedure_name == NULL) return EMPTY_OBJECT;

	tmp_procedure_name = (*in_java_env)->GetStringUTFChars(in_java_env, in_procedure_name, 0);

	// Calculer le nombre de parametres
	tmp_param_count = (*in_java_env)->GetArrayLength(in_java_env, in_out_parameters);
	fprintf(stdout, "JNI (callSqlProcedure) param count : %i\n", tmp_param_count);

	tmp_ptr = tmp_session->m_request;
	tmp_ptr += sprintf(tmp_ptr, "%c%c%i%c%s%c", PAQUET_START_CHAR,
			SEPARATOR_CHAR, CALL_SQL_PROC,
			SEPARATOR_CHAR, tmp_procedure_name,
			SEPARATOR_CHAR);

	tmp_ptr = jni_array_strings_to_net_str(in_java_env, tmp_param_count, in_out_parameters, tmp_ptr);
	tmp_ptr += sprintf(tmp_ptr, "%c", PAQUET_STOP_CHAR);

	// Liberation memoire
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_procedure_name, tmp_procedure_name);

	// Execution de la procedure
	//db_call_procedure(in_session, in_procedure_name, tmp_param_count, in_out_parameters);
	tmp_status = cl_send_request(tmp_session, tmp_ptr - tmp_session->m_request);
	if (tmp_status != NOERR)		return tmp_status;

	tmp_status = jni_parse_response_columns(in_java_env, tmp_session, tmp_param_count, tmp_session->m_response, in_out_parameters);

	return tmp_status;

}

JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_loadRecord(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_table_signature, jobjectArray in_out_columns_record, jstring in_primary_key_value)
{
	net_session				*tmp_session = (net_session*)in_session_ptr;
	const char				*tmp_primary_key_value = NULL;
	entity_def				*tmp_entity_def = NULL;

	int						tmp_status = NOERR;
	int						tmp_nb_bytes = 0;

	if (tmp_session == NULL || in_out_columns_record == NULL || in_primary_key_value == NULL)		return EMPTY_OBJECT;

	tmp_primary_key_value = (*in_java_env)->GetStringUTFChars(in_java_env, in_primary_key_value, 0);

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_nb_bytes = sprintf(tmp_session->m_request, "%c%c%i%c%i%c%s%c%c", PAQUET_START_CHAR,
			SEPARATOR_CHAR, LOAD_RECORD,
			SEPARATOR_CHAR, in_table_signature,
			SEPARATOR_CHAR, tmp_primary_key_value,
			SEPARATOR_CHAR, PAQUET_STOP_CHAR);

	// Liberation memoire
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_primary_key_value, tmp_primary_key_value);

	tmp_status = cl_send_request(tmp_session, tmp_nb_bytes);
	if (tmp_status != NOERR)		return tmp_status;

	tmp_status = jni_parse_response_columns(in_java_env, tmp_session, tmp_entity_def->m_entity_columns_count, tmp_session->m_response, in_out_columns_record);

	return tmp_status;
}

JNIEXPORT jobjectArray JNICALL Java_org_ej_EJNativeLibrary_loadRecords(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_table_signature, jstring in_where_clause, jstring in_order_by_clause)
{
	int				tmp_status = 0;
	int				tmp_nb_bytes = 0;
	unsigned long 	tmp_rows_count = 0;
	unsigned long 	tmp_columns_count = 0;

	net_session		*tmp_session = (net_session*)in_session_ptr;
	const char		*tmp_where_clause = NULL;
	const char		*tmp_order_by_clause = NULL;
	entity_def		*tmp_entity_def = NULL;

	if (tmp_session == NULL)	return NULL;

	if (in_where_clause == NULL)
	{
		fprintf(stderr, "*** JNI error (connect) : where clause is null\n");
		return NULL;
	}

	tmp_where_clause = (*in_java_env)->GetStringUTFChars(in_java_env, in_where_clause, 0);
	if (in_order_by_clause != NULL)
		tmp_order_by_clause = (*in_java_env)->GetStringUTFChars(in_java_env, in_order_by_clause, 0);

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return NULL;

	tmp_nb_bytes = sprintf(tmp_session->m_request, "%c%c%i%c%i%c%s%c%s%c%c", PAQUET_START_CHAR,
			SEPARATOR_CHAR, LOAD_RECORDS,
			SEPARATOR_CHAR, in_table_signature,
			SEPARATOR_CHAR, is_empty_string(tmp_where_clause) ? "" : tmp_where_clause,
			SEPARATOR_CHAR, is_empty_string(tmp_order_by_clause) ? "" : tmp_order_by_clause,
			SEPARATOR_CHAR, PAQUET_STOP_CHAR);

	// Liberation memoire
	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_where_clause, tmp_where_clause);
	if (in_order_by_clause != NULL)
		(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_order_by_clause, tmp_order_by_clause);

	tmp_status = cl_send_request(tmp_session, tmp_nb_bytes);
	if (tmp_status != NOERR)
	{
		return NULL;
	}

	return jni_parse_response_rows(in_java_env, tmp_session, tmp_session->m_response, &tmp_rows_count, &tmp_columns_count);
}

JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_insertRecord(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_table_signature, jobjectArray in_out_columns_record)
{
	int						tmp_status = NOERR;
	entity_def				*tmp_entity_def = NULL;
	char					tmp_pk_value_str[16];
	char					*tmp_ptr = NULL;
	net_session				*tmp_session = (net_session*)in_session_ptr;

	if (tmp_session == NULL || in_out_columns_record == NULL)		return EMPTY_OBJECT;

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_ptr = tmp_session->m_request;
	tmp_ptr += sprintf(tmp_ptr, "%c%c%i%c%i%c", PAQUET_START_CHAR, SEPARATOR_CHAR, INSERT_RECORD, SEPARATOR_CHAR, in_table_signature, SEPARATOR_CHAR);
	tmp_ptr = jni_array_strings_to_net_str(in_java_env, tmp_entity_def->m_entity_columns_count, in_out_columns_record, tmp_ptr);
	tmp_ptr += sprintf(tmp_ptr, "%c", PAQUET_STOP_CHAR);

	tmp_status = cl_send_request(tmp_session, tmp_ptr - tmp_session->m_request);
	if (tmp_status != NOERR)		return tmp_status;

	net_get_field(NET_MESSAGE_TYPE_INDEX+1, tmp_session->m_response, tmp_pk_value_str, SEPARATOR_CHAR);
	if (is_empty_string(tmp_pk_value_str) == FALSE && atoi(tmp_pk_value_str) > 0)
	{
		(*in_java_env)->SetObjectArrayElement(in_java_env, in_out_columns_record, 0, (*in_java_env)->NewStringUTF(in_java_env, tmp_pk_value_str));
	}

	return tmp_status;

}


JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_saveRecord(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_table_signature, jobjectArray in_out_columns_record, jobjectArray in_originals_columns_record)
{
	net_session				*tmp_session = (net_session*)in_session_ptr;
	entity_def				*tmp_entity_def = NULL;
	char					*tmp_ptr = NULL;

	if (tmp_session == NULL || in_out_columns_record == NULL)	return EMPTY_OBJECT;

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_ptr = tmp_session->m_request;
	tmp_ptr += sprintf(tmp_ptr, "%c%c%i%c%i%c", PAQUET_START_CHAR, SEPARATOR_CHAR, SAVE_RECORD, SEPARATOR_CHAR, in_table_signature, SEPARATOR_CHAR);
	tmp_ptr = jni_arrays_strings_to_net_str(in_java_env, tmp_entity_def, in_out_columns_record, in_originals_columns_record, tmp_ptr);
	// Aucun changement
	if (tmp_ptr == NULL)
		return NOERR;

	tmp_ptr += sprintf(tmp_ptr, "%c", PAQUET_STOP_CHAR);

	return cl_send_request(tmp_session, tmp_ptr - tmp_session->m_request);
}

JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_deleteRecord(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_table_signature, jobjectArray in_columns_record)
{
	net_session				*tmp_session = (net_session*)in_session_ptr;
	entity_def				*tmp_entity_def = NULL;
	char					*tmp_ptr = NULL;

	if (tmp_session == NULL || in_columns_record == NULL)	return EMPTY_OBJECT;

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_ptr = tmp_session->m_request;
	tmp_ptr += sprintf(tmp_session->m_request, "%c%c%i%c%i%c", PAQUET_START_CHAR, SEPARATOR_CHAR, DELETE_RECORD, SEPARATOR_CHAR, in_table_signature, SEPARATOR_CHAR);
	tmp_ptr = jni_array_strings_to_net_str(in_java_env, tmp_entity_def->m_entity_columns_count, in_columns_record, tmp_ptr);
	tmp_ptr += sprintf(tmp_ptr, "%c", PAQUET_STOP_CHAR);

	return cl_send_request(tmp_session, tmp_ptr - tmp_session->m_request);
}

/***********************************************************************************	*
*	Java_org_ej_EJNativeLibrary_deleteRecords																				*
*----------------------------------------------------------------------------------------------------------------------------	*
*	Supprimer plusieurs enregistrements d'une même table de la base de données					. 	*
*----------------------------------------------------------------------------------------------------------------------------	*
*					Arguments	|		in_java_env																									*
*											|		in_class																										*
*											|		in_table_signature																					*
*											|		in_where_clause																						*
*----------------------------------------------------------------------------------------------------------------------------	*
*							Retour	|																															*
* ************************************************************************************
*	Date			|	Auteur	|	Version	|	Description																				*
*----------------------------------------------------------------------------------------------------------------------------	*
*	20090209	|	EJO			|	1.0.0		|	Création																					*
*************************************************************************************/
JNIEXPORT jint JNICALL Java_org_ej_EJNativeLibrary_deleteRecords(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_table_signature, jstring in_where_clause)
{
	net_session				*tmp_session = (net_session*)in_session_ptr;
	const char				*tmp_where_clause = NULL;
	int						tmp_nb_bytes = 0;
	entity_def				*tmp_entity_def = NULL;

	if (tmp_session == NULL || in_where_clause == NULL)	return EMPTY_OBJECT;

	tmp_where_clause = (*in_java_env)->GetStringUTFChars(in_java_env, in_where_clause, 0);

	/* Chercher le definition de l'entite */
	if (get_table_def(in_table_signature, &tmp_entity_def) < 0)
		return UNKNOW_ENTITY;

	tmp_nb_bytes = sprintf(tmp_session->m_request, "%c%c%i%c%i%c%s%c%c", PAQUET_START_CHAR,
			SEPARATOR_CHAR, DELETE_RECORDS,
			SEPARATOR_CHAR, in_table_signature,
			SEPARATOR_CHAR, tmp_where_clause,
			SEPARATOR_CHAR, PAQUET_STOP_CHAR);

	(*in_java_env)->ReleaseStringUTFChars(in_java_env, in_where_clause, tmp_where_clause);

	return cl_send_request(tmp_session, tmp_nb_bytes);
}



JNIEXPORT jstring JNICALL Java_org_ej_EJNativeLibrary_getErrorMessage(JNIEnv *in_java_env, jclass in_class, jlong in_session_ptr, jint in_error_id)
{
	net_session				*tmp_session = (net_session*)in_session_ptr;
	char					*tmp_error_msg = cl_get_error_message(tmp_session, in_error_id);

	return (*in_java_env)->NewStringUTF(in_java_env, tmp_error_msg);
}
