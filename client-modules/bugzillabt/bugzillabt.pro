# -------------------------------------------------
# Project created by QtCreator 2011-04-25T21:32:39
# -------------------------------------------------
TARGET = bugzillabt
QT += xml network
TARGET_EXT = .cmo

TEMPLATE = lib
CONFIG += plugin

include (../../common/common.pri)

DEFINES += CLIENT_MODULE_LIBRARY

unix:target.path = $$EXTERNALS_MODULES_DIR

SOURCES += ../bugtracker.cpp \
    bugzillabt.cpp \
    bugzillamodule.cpp
HEADERS += ../bugtracker.h \
    bugzillabt.h \
    bugzillamodule.h
INCLUDEPATH += .. \
    ../../common \
    ../../client-lib \
    ../../client-app


equals(QT_MAJOR_VERSION, "5") {
    INCLUDEPATH += ../../third-party/qtxmlrpc
    LIBS += -L../../third-party/qtxmlrpc/lib/win32-msvc2010/5.5.0
    CONFIG(debug, debug|release) {
        LIBS += -lqtxmlrpcd
    }
    CONFIG(release, release|debug) {
        LIBS += -lqtxmlrpc
    }
}

equals(QT_MAJOR_VERSION, "4") {
    SOURCES += qxtxmlrpc/qxtxmlrpcresponseparser.cpp \
        qxtxmlrpc/qxtxmlrpcrequestcomposer.cpp \
        qxtxmlrpc/qxtxmlrpcclient.cpp

    HEADERS += qxtxmlrpc/qxtxmlrpcresponseparser.h \
        qxtxmlrpc/qxtxmlrpcrequestcomposer.h \
        qxtxmlrpc/qxtxmlrpcclient.h
}

OBJECTS_DIR = build

!mac {
    QMAKE_EXTENSION_SHLIB = cmo
}

#!win32:QMAKE_LFLAGS += --export-dynamic
#QMAKE_LN_SHLIB = :
