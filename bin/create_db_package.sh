#!/bin/bash

CLIENT_NAME=rtmr

WORKING_DIR=$(pwd)
BASH_DIR=$(dirname $0)
BASE_DIR="${CLIENT_NAME}_db_$(date +%Y%m%d)"
DEST_DIR=$WORKING_DIR/$BASE_DIR

mkdir -p $DEST_DIR/functions || exit 1

cd $BASH_DIR/..
cp sql/postgres/install_database.sql $DEST_DIR
cp sql/postgres/create_database.sql $DEST_DIR
cp sql/postgres/functions/copy_project_version.sql $DEST_DIR/functions
cp sql/postgres/functions/html_to_plain_text.sql $DEST_DIR/functions
cp sql/postgres/functions/create_test_from_requirement.sql $DEST_DIR/functions
cp sql/postgres/functions/create_test_from_test.sql $DEST_DIR/functions
cp sql/postgres/functions/create_requirement_from_requirement.sql $DEST_DIR/functions

cd $WORKING_DIR
zip ${CLIENT_NAME}_db.zip -r $BASE_DIR
rm -Rf $BASE_DIR
exit 0
