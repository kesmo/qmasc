(script-fu-register 
	"generate-rounded-shadowed-frame" ;func name
	"Text Box" ;menu label
	"Creates a simple text box, sized to fit\ around the user's choice of text,\ font, font size, and color." ;description
	"Michael Terry" ;author
	"copyright 1997, Michael Terry" ;copyright notice
	"October 27, 1997" ;date created
	"" ;image type that the script works on
	SF-STRING "Text:" "Text Box" ;a string variable
	SF-COLOR "Color:" '(0 0 0) ;color variable 
)
(generate-rounded-shadowed-frame "script-fu-text-box" "<Toolbox>/Xtns/Script-Fu/Text")
