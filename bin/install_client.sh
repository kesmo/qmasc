#!/bin/bash

CLIENT_NAME=rtmr

if [ ! -d sources ]; then
	echo "Le repertoire sources n'existe pas."
	exit 1
fi

if [ ! -d sources/client-lib ]; then
	echo "Le repertoire sources/client-lib n'existe pas."
	exit 1
fi

if [ ! -d sources/client ]; then
	echo "Le repertoire sources/client n'existe pas."
	exit 1
fi

QMAKE=$(which qmake)
if [ $? -ne 0 ]; then
	QMAKE=$(which qmake-qt4)
	if [ $? -ne 0 ]; then
		echo "qmake et qmake-qt4 sont introuvables."
		exit 2
	fi
fi

cd sources/client-lib
mkdir -p Release
make all

cd ../client

$QMAKE
make

cd ../..

uname -a | grep x86_64
if [ $? -ne 0 ]; then
	uname -a | grep ia64
	if [ $? -ne 0 ]; then
		LIB_DIR=/usr/lib
	else
		LIB_DIR=/usr/lib64
	fi
else
	LIB_DIR=/usr/lib64
fi

cp $(pwd)/sources/client-lib/Release/lib*.so $LIB_DIR

exit 0
