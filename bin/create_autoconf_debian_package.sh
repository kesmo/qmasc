#!/bin/bash

WORKING_DIR=$(pwd)

$(dirname $0)/create_autoconf_package.sh $@

if [ $? -ne 0 ]; then
	exit $?
fi

cd "$1-$2"
autoreconf --install -f
dh_make -n -s -a -e emmanuel.jorge@rtmr.net -c gpl3
dpkg-buildpackage -uc -us

cd $WORKING_DIR
rm -Rf "$1-$2"
