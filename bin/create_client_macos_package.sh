#!/bin/bash

CLIENT_NAME=rtmr
CLIENT_VERSION=1.11.0

WORKING_DIR=$(pwd)
BASH_DIR=$(dirname $0)

cd $BASH_DIR/../client-lib
make -f Makefile.macos clean
make -f Makefile.macos all

cd $BASH_DIR/../client
make clean
rm -f Makefile*
rm -Rf ../client-app/${CLIENT_NAME}app.app
rm -Rf ../client-launcher/${CLIENT_NAME}.app
qmake -spec macx-g++ client.pro
make
if [ $? -ne 0 ]; then
	echo "erreur de compilation"
	exit 1
fi

if [ ! -f ../client-lib/lib${CLIENT_NAME}.dylib ]; then
	echo "La librarie lib${CLIENT_NAME}.dylib n'existe pas."
	exit 2
fi

mkdir -p ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS/modules

cp ../client-modules/bugzillabt/libbugzillabt.dylib ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS/modules/libbugzillabt.dylib
cp ../client-modules/mantisbt/libmantisbt.dylib ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS/modules/libmantisbt.dylib
cp ../client-modules/jirabt/libjirabt.dylib ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS/modules/libjirabt.dylib
cp ../client-modules/redminebt/libredminebt.dylib ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS/modules/libredminebt.dylib
cp ../client-modules/std-automation/libstd-automation.dylib ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS/modules/libstd-automation.dylib
cp ../client-lib/lib${CLIENT_NAME}.dylib ../client-launcher/${CLIENT_NAME}.app/Contents/MacOS

cd ../client-launcher

macdeployqt ${CLIENT_NAME}.app
#cp -R /Library/Frameworks/QtGui.framework  ${CLIENT_NAME}.app/Contents/Frameworks/
#cp -R /Library/Frameworks/QtCore.framework ${CLIENT_NAME}.app/Contents/Frameworks/
#cp -R /Library/Frameworks/QtXml.framework ${CLIENT_NAME}.app/Contents/Frameworks/
#cp -R /Library/Frameworks/QtNetwork.framework ${CLIENT_NAME}.app/Contents/Frameworks/

install_name_tool -id @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/Frameworks/QtCore.framework/Versions/4/QtCore
install_name_tool -id @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/Frameworks/QtGui.framework/Versions/4/QtGui
install_name_tool -id @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/Frameworks/QtXml.framework/Versions/4/QtXml
install_name_tool -id @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/Frameworks/QtNetwork.framework/Versions/4/QtNetwork

install_name_tool -change Frameworks/QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/MacOS/${CLIENT_NAME}
install_name_tool -change Frameworks/QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/MacOS/${CLIENT_NAME}
install_name_tool -change Frameworks/QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/MacOS/${CLIENT_NAME}
install_name_tool -change Frameworks/QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/MacOS/${CLIENT_NAME}

install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/MacOS/modules/libbugzillabt.dylib
install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/MacOS/modules/libbugzillabt.dylib
install_name_tool -change QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/MacOS/modules/libbugzillabt.dylib
install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/MacOS/modules/libbugzillabt.dylib

install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/MacOS/modules/libmantisbt.dylib
install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/MacOS/modules/libmantisbt.dylib
install_name_tool -change QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/MacOS/modules/libmantisbt.dylib
install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/MacOS/modules/libmantisbt.dylib

install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/MacOS/modules/libjirabt.dylib
install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/MacOS/modules/libjirabt.dylib
install_name_tool -change QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/MacOS/modules/libjirabt.dylib
install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/MacOS/modules/libjirabt.dylib

install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/MacOS/modules/libredminebt.dylib
install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/MacOS/modules/libredminebt.dylib
install_name_tool -change QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/MacOS/modules/libredminebt.dylib
install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/MacOS/modules/libredminebt.dylib

install_name_tool -change QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}.app/Contents/MacOS/modules/libstd-automation.dylib
install_name_tool -change QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}.app/Contents/MacOS/modules/libstd-automation.dylib
install_name_tool -change QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}.app/Contents/MacOS/modules/libstd-automation.dylib
install_name_tool -change QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}.app/Contents/MacOS/modules/libstd-automation.dylib

cd ../client-app
macdeployqt ${CLIENT_NAME}app.app
#cp -R /Library/Frameworks/QtGui.framework  ${CLIENT_NAME}app.app/Contents/Frameworks/
#cp -R /Library/Frameworks/QtCore.framework ${CLIENT_NAME}app.app/Contents/Frameworks/
#cp -R /Library/Frameworks/QtXml.framework ${CLIENT_NAME}app.app/Contents/Frameworks/
#cp -R /Library/Frameworks/QtNetwork.framework ${CLIENT_NAME}app.app/Contents/Frameworks/

install_name_tool -id @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}app.app/Contents/Frameworks/QtCore.framework/Versions/4/QtCore
install_name_tool -id @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}app.app/Contents/Frameworks/QtGui.framework/Versions/4/QtGui
install_name_tool -id @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}app.app/Contents/Frameworks/QtXml.framework/Versions/4/QtXml
install_name_tool -id @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}app.app/Contents/Frameworks/QtNetwork.framework/Versions/4/QtNetwork

install_name_tool -change Frameworks/QtCore.framework/Versions/4/QtCore @executable_path/../Frameworks/QtCore.framework/Versions/4/QtCore ${CLIENT_NAME}app.app/Contents/MacOS/${CLIENT_NAME}app
install_name_tool -change Frameworks/QtGui.framework/Versions/4/QtGui @executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui ${CLIENT_NAME}app.app/Contents/MacOS/${CLIENT_NAME}app
install_name_tool -change Frameworks/QtXml.framework/Versions/4/QtXml @executable_path/../Frameworks/QtXml.framework/Versions/4/QtXml ${CLIENT_NAME}app.app/Contents/MacOS/${CLIENT_NAME}app
install_name_tool -change Frameworks/QtNetwork.framework/Versions/4/QtNetwork @executable_path/../Frameworks/QtNetwork.framework/Versions/4/QtNetwork ${CLIENT_NAME}app.app/Contents/MacOS/${CLIENT_NAME}app

cd ..
cp client-app/${CLIENT_NAME}app.app/Contents/MacOS/${CLIENT_NAME}app client-launcher/${CLIENT_NAME}.app/Contents/MacOS

cd client-launcher
hdiutil create -fs HFS+ -srcFolder ${CLIENT_NAME}.app ${CLIENT_NAME}_${CLIENT_VERSION}
mv ${CLIENT_NAME}_${CLIENT_VERSION}.dmg $WORKING_DIR
cd $WORKING_DIR

exit 0
