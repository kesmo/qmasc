#!/bin/bash
DATABASE_SCHEMA=rtmr
DB_CLIENT=psql

tmp_db_schema=
which $DB_CLIENT || exit 3
echo -n "Veuillez saisir le nom du schema d'installation de la base ? [default=$DATABASE_SCHEMA] : "
read tmp_db_schema
if [ -n "$tmp_db_schema" ]; then
	DATABASE_SCHEMA="$tmp_db_schema"
fi
$DB_CLIENT --username postgres --set var_dbname=$DATABASE_SCHEMA --file install_database.sql > install_database.log
exit 0
