# pour intialiser l'environnement rpmbuild (creation fichier de macros .rpmmacros et dossier rpmbuild
# $ rpmdev-setuptree
# Contenu du dossier rpmbuild cr�� :
#    $HOME/rpmbuild
#    $HOME/rpmbuild/BUILD (dossier temporaire utilis� pour la d�compression des archives et la construction)
#    $HOME/rpmbuild/RPMS (dossier contenant les RPM binaires construits)
#    $HOME/rpmbuild/SOURCES (dossier contenant les sources : archives, patches...)
#    $HOME/rpmbuild/SPECS (dossier contenant les fichiers .spec contenant les instructions de construction)
#    $HOME/rpmbuild/SRPMS (dossier contenant les RPM sources construits) 
# g�n�rer une cl� GPG
# $ gpg --gen-key
# $ gpg --export --armor >RPM-GPG-KEY-votrenom
# $ importer la cl� :
# $ sudo rpm --import ~builder/RPM-GPG-KEY-votrenom
# ajouter les macros �ventuelles :
#%vendor                 Votre Nom
#%packager               Plus d'information
#%dist                   .fc11
#%fedora                 11
#%_signature             gpg
#%_gpg_name              Votre Nom
#%_gpg_path              %(echo $HOME)/.gnupg
# construction d'un fichier spec
# $ rpmdev-newspec nom_du_spec
# construction des rpm � partir de fichier spec
# $ rpmbuild -ba rpmbuild/SPECS/less.spec
# signature des rpm :
# $ rpmsign --addsign rpmbuild/RPMS/i386/less-394-1.fc4.i386.rpm
# v�rifier la signature :
# $ rpmsign --checksig rpmbuild/RPMS/i386/less-394-1.fc4