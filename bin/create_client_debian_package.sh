#!/bin/bash

CLIENT_NAME=rtmr

WORKING_DIR=$(pwd)
BASH_DIR=$(dirname $0)

DEST_DIR=$WORKING_DIR/client_pck

CLIENT_DIR="${CLIENT_NAME}-1.11.0"
CLIENT_DEST_DIR=$DEST_DIR/sources/$CLIENT_DIR

rm -Rf $DEST_DIR
if [ $# -eq 1 ]; then
  if [ -d $1 ] && [ -d $1/common ] && [ -d $1/client ]; then
    mkdir -p $DEST_DIR/sources
    cp -R $1/client-modules $DEST_DIR/sources/client-modules
    cp -R $1/client-entities $DEST_DIR/sources/client-entities
    cp -R $1/common $DEST_DIR/sources/common
    cp -R $1/client-lib $DEST_DIR/sources/client-lib
    cp -R $1/client-launcher $DEST_DIR/sources/client-launcher
    cp -R $1/client-app $DEST_DIR/sources/client-app
    cp -R $1/client $CLIENT_DEST_DIR
  else
   echo "Dossier $1 inexistant"
   exit 1
  fi
else
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-modules $DEST_DIR/sources/client-modules
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/common $DEST_DIR/sources/common
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-lib $DEST_DIR/sources/client-lib
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-launcher $DEST_DIR/sources/client-launcher
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-app $DEST_DIR/sources/client-app
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-entities $DEST_DIR/sources/client-entities
  svn co --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client $CLIENT_DEST_DIR
fi

cd $CLIENT_DEST_DIR
qmake
dh_make -n -s -a -e emmanuel.jorge@rtmr.net -c gpl3
dpkg-buildpackage -uc -us

cd $WORKING_DIR
# rm -Rf $BASE_DIR
