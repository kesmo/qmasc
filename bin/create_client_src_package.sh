#!/bin/bash

CLIENT_NAME=rtmr

WORKING_DIR=$(pwd)
BASH_DIR=$(dirname $0)

CLIENT_DIR="${CLIENT_NAME}-1.11.0-src"

DEST_DIR=$WORKING_DIR/$CLIENT_DIR


rm -Rf $DEST_DIR
if [ $# -eq 1 ]; then
  if [ -d $1 ] && [ -d $1/common ] && [ -d $1/client ]; then
    mkdir -p $DEST_DIR/sources
    cp -R $1/client-modules $DEST_DIR/sources/client-modules
    cp -R $1/common $DEST_DIR/sources/common
    cp -R $1/client-lib $DEST_DIR/sources/client-lib
    cp -R $1/client-entities $DEST_DIR/sources/client-entities
    cp -R $1/client-app $DEST_DIR/sources/client-app
    cp -R $1/client-launcher $DEST_DIR/sources/client-launcher
    cp -R $1/client $DEST_DIR/sources/client
    cp vs2010-solution.sln  $1/client $DEST_DIR/sources/
    find $DEST_DIR -name ".svn" | xargs rm -Rf
  else
   echo "Dossier $1 inexistant"
   exit 1
  fi
else
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-modules $DEST_DIR/sources/client-modules
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-entities $DEST_DIR/sources/client-entities
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/common $DEST_DIR/sources/common
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-lib $DEST_DIR/sources/client-lib
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-app $DEST_DIR/sources/client-app
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client-launcher $DEST_DIR/sources/client-launcher
  svn export --force svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk/client $DEST_DIR/sources/client
fi



cd $WORKING_DIR
tar cfvz $CLIENT_DIR.tar.gz $CLIENT_DIR
rm -Rf $CLIENT_DIR

