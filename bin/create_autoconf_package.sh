#!/bin/bash

SVN_REPOSITORY=svn+ssh://ehouse/home/ejorge/Documents/qmasc/trunk
WORKING_DIR=$(pwd)
BASH_DIR=$(dirname $0)

if [ $# -lt 3 ]; then
	echo "usage $0 PACKAGE_NAME PACKAGE_VERSION MAIN_DIR [SOURCES_DIR]"
	exit 1
fi

PACKAGE_NAME=$1
PACKAGE_VERSION=$2
DEST_DIR="${PACKAGE_NAME}-${PACKAGE_VERSION}"
MAIN_DIR=$3

rm -Rf $DEST_DIR
if [ $# -gt 3 ]; then
  if [ -d $4 ] && [ -d $4/common ] && [ -d $4/${MAIN_DIR} ]; then
    mkdir -p $DEST_DIR/src
    cp -R $4/autoconf/${MAIN_DIR}/* $DEST_DIR
    cp -R $4/common $DEST_DIR/src/common
    cp -R $4/${MAIN_DIR} $DEST_DIR/src/${MAIN_DIR}
    find $DEST_DIR -name ".svn" | xargs rm -Rf
  else
   echo "Dossier $1 inexistant"
   exit 1
  fi
else
  svn export --force $SVN_REPOSITORY/autoconf/${MAIN_DIR} $DEST_DIR
  svn export --force $SVN_REPOSITORY/common $DEST_DIR/src/common
  svn export --force $SVN_REPOSITORY/${MAIN_DIR} $DEST_DIR/src/${MAIN_DIR}
fi

cd $DEST_DIR
autoreconf --install -f

