#!/bin/bash
BASH_PROFILE=
INSTALL_DIR=/usr/local
NETSERVER_HOME=$INSTALL_DIR/netserver
DAEMON_INSTALL_DIR=/etc/init.d

if [ -d $INSTALL_DIR ]; then
	[ -d $NETSERVER_HOME ] || mkdir -p $NETSERVER_HOME || exit 1
	[ -d $NETSERVER_HOME/bin ] || mkdir -p $NETSERVER_HOME/bin || exit 1
	[ -d $NETSERVER_HOME/conf ] || cp -R conf $NETSERVER_HOME || exit 1
	[ -d $NETSERVER_HOME/logs ] || mkdir -p $NETSERVER_HOME/logs || exit 1
else
	echo "$INSTALL_DIR does not exists."
	exit 2
fi

if [ -f /etc/bash.bashrc ];then
	BASH_PROFILE=/etc/bash.bashrc
else
	if [ -f /etc/bashrc ];then
		BASH_PROFILE=/etc/bashrc
	else
		if [ -f /etc/profile ];then
			BASH_PROFILE=/etc/profile
		else
			exit 10
		fi
	fi
fi

grep "NETSERVER_HOME" $BASH_PROFILE
if [ $? -ne 0 ]; then
	echo "NETSERVER_HOME=$INSTALL_DIR/netserver" >> $BASH_PROFILE
	echo "export NETSERVER_HOME" >> $BASH_PROFILE
fi


# Mac OS X
if [ $(uname -s) == "Darwin" ]; then

	# Arreter les eventuels processus netserver
	launchctl unload /System/Library/LaunchDaemons/net.nexp.netserver.plist

	cp bin/* $NETSERVER_HOME/bin/
	
	chmod +x $NETSERVER_HOME/bin/netserver

else

	cd src/netserver
	mkdir -p Release
	./configure
	[ $? -eq 0 ] || exit 4
	make
	[ $? -eq 0 ] || exit 5
	make install
	[ $? -eq 0 ] || exit 6
	
	# Arreter les eventuels processus netserver
	killall netserver

	#cp Release/netserver $NETSERVER_HOME/bin/netserver

fi


tmp_add_to_services=
tmp_auto_start=
tmp_start_netservice=
while [ "$tmp_add_to_services" != "o" ] && [ "$tmp_add_to_services" != "n" ]; do
	echo -n "Voulez-vous ajouter le service netserverd ? [o/n] : "
	read -n 1 tmp_add_to_services
	echo ""
	if [ "$tmp_add_to_services" = "o" ]; then
		
		# Mac OS X
		if [ $(uname -s) == "Darwin" ]; then
			
			cp net.nexp.netserver.plist /System/Library/LaunchDaemons

			while [ "$tmp_start_netservice" != "o" ] && [ "$tmp_start_netservice" != "n" ]; do
				echo -n "Voulez-vous demarrer le service netserverd ? [o/n] : "
				read -n 1 tmp_start_netservice
				echo ""
				if [ "$tmp_start_netservice" = "o" ]; then
					launchctl load /System/Library/LaunchDaemons/net.nexp.netserver.plist
				fi
			done

		# Linux
		else

			cd ../..
			sed "s#{INSTALL_DIR}#$INSTALL_DIR#g" bin/netserverd.template > $DAEMON_INSTALL_DIR/netserverd
			chmod +x $DAEMON_INSTALL_DIR/netserverd

			which chkconfig
			if [ $? -eq 0 ]; then
				chkconfig --add netserverd
			

				while [ "$tmp_auto_start" != "o" ] && [ "$tmp_auto_start" != "n" ]; do
					echo -n "Voulez-vous ajouter le service netserverd au demarrage du systeme ? [o/n] : "
					read -n 1 tmp_auto_start
					echo ""
					if [ "$tmp_auto_start" = "o" ]; then
						chkconfig --level 2345 netserverd on
					fi
				done
			fi

			while [ "$tmp_start_netservice" != "o" ] && [ "$tmp_start_netservice" != "n" ]; do
				echo -n "Voulez-vous demarrer le service netserverd ? [o/n] : "
				read -n 1 tmp_start_netservice
				echo ""
				if [ "$tmp_start_netservice" = "o" ]; then
					/etc/init.d/netserverd start
				fi
			done
			
		fi
	fi
done

