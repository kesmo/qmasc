#!/bin/bash

NETSERVER_VERSION=1.8.0

WORKING_DIR=$(pwd)
BASH_DIR=$(dirname $0)
BASE_DIR="netserver_${NETSERVER_VERSION}"
DEST_DIR=$WORKING_DIR/$BASE_DIR

mkdir -p $DEST_DIR || exit 1
mkdir -p $DEST_DIR/bin || exit 1
mkdir -p $DEST_DIR/conf || exit 1

cd $BASH_DIR
cd ..

# Mac OS X
if [ $(uname -s) == "Darwin" ]; then

	# Copie du fichier de lancement pour launchd
	cp netserver/net.nexp.netserver.plist $DEST_DIR
	
	# Copie de l'executable
	cp netserver/Release/netserver $DEST_DIR/bin
	chmod +x $DEST_DIR/bin/netserver
	
	# Copie de la librairie PostgreSQL
	cp /Library/PostgreSQL/8.4/lib/libpq.5.dylib $DEST_DIR/bin

else

	mkdir -p $DEST_DIR/src || exit 1
	mkdir -p $DEST_DIR/src/common || exit 1
	mkdir -p $DEST_DIR/src/common/postgres || exit 1
	mkdir -p $DEST_DIR/src/netserver || exit 1
	mkdir -p $DEST_DIR/src/netserver/conf || exit 1
	
	# Copie des sources
	cp common/*.c $DEST_DIR/src/common
	cp common/*.h $DEST_DIR/src/common

	cp common/postgres/*.c $DEST_DIR/src/common/postgres
	cp common/postgres/*.h $DEST_DIR/src/common/postgres

	cp netserver/*.c $DEST_DIR/src/netserver
	cp netserver/*.h $DEST_DIR/src/netserver
	cp netserver/LICENCE.txt $DEST_DIR/src/netserver
	cp netserver/Makefile.am $DEST_DIR/src/netserver
	cp netserver/configure.ac $DEST_DIR/src/netserver
	cp netserver/conf/netserver.conf $DEST_DIR/src/netserver/conf/netserver.conf

	# Copie du script modele pour init.d
	cp bin/netserverd.template $DEST_DIR/bin
fi

# Copie du dossier de configuration
cp netserver/conf/* $DEST_DIR/conf

# Copie de la licence
cp netserver/LICENCE.txt $DEST_DIR/

# Copie de l'installeur
cp bin/install_netserver.sh $DEST_DIR
chmod +x $DEST_DIR/install_netserver.sh

if [ $(uname -s) != "Darwin" ]; then
	cd $DEST_DIR/src/netserver
	autoreconf --install -f
fi

# Creation de l'archive
cd $WORKING_DIR
tar cfvz $BASE_DIR.tar.gz $BASE_DIR
rm -Rf $BASE_DIR
