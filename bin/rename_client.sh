#!/bin/bash

if [ $# -ne 2 ]; then

	echo "usage : $0 old_client_name new_client_name"
	exit 1
	
else

	OLD_CLIENT_NAME=$1
	NEW_CLIENT_NAME=$2
	
	for file in $(grep -R --exclude-dir=.svn -l -I "$OLD_CLIENT_NAME" .)
	do
		echo "Traitement $file"
		mv $file $file.old
		sed -e "s#${OLD_CLIENT_NAME}#${NEW_CLIENT_NAME}#g" $file.old > $file
		rm $file.old
	done
	
fi
